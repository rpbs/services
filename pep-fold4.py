#!/usr/bin/env python

"""
PEPFOLD main script
Will organise a script depending on options and launch series of runs.

Dependencies:

Command line examples:
cd test ; python pep-fold4.py -s 3e21.fst --shake_model 3e21.pdb --shake_mask 3e21.mask
cd test ; python pep-fold4.py -s 1AWR_lig.fst --receptor 1AWR_apo-clean.pdb --receptor_patch 1AWR_interface.txt

python pep-fold4.py --maxSimSze 210 --maxSeqSze 210 -s amyloid.fst -l test --nRuns 10 --generator fbt --sortKey sOPEP --mcSteps 30000
"""

import argparse
import sys
import os
import fnmatch
import shutil
import subprocess
import time
import cluster.cluster as cluster
import signal
import glob
import random

# Configuration 
from PEPFOLD4.Config import *

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PEPFOLD4','templates'))

# Dependencies
import PyPDB.PyPDB as PDB
import Fasta.Fasta as Fasta
from PEPFOLD4.startCrdsFromPatch import *
from PEPFOLD4.clusterTools import *


# check residues a continuous (ungapped)
# based on residue numbers + icode
def check_ungapped_pdb(pdb):
    rNum = pdb[0].rNum()
    riCode = pdb[0].riCode()
    for aRes in range(1, len(pdb)):
        rrNum   = pdb[aRes].rNum()
        rriCode = pdb[aRes].riCode()
        if (int(rrNum) != int(rNum) + 1):
            if ( int(rrNum) != int(rNum) ):
                # print rrNum, rNum
                return False
            if rriCode != ' ':
                # print rrNum, rNum, "\"",rriCode, "\""
                return False
        rNum = rrNum
        riCode = rriCode
    return True


def pmlScript(label, maxConf):

    rs = """
load protein.pdb, protein
show surface, protein
show cartoon, protein
hide lines, protein
spectrum b, blue_white_red, protein, 0, 100
set surface_color, white
set transparency, 0.5
set surface_quality, 1
"""
    
    for cnfNb in range(1, maxConf+1):
        for poseNb in range (1, 11):
            rs += "load %s-cnf%s_%.2d.pdb, cnf%s\n" % (label, cnfNb, poseNb, cnfNb)
    rs += "select all_cnf, cnf1"

    for cnfNb in range(2, maxConf+1):
        rs += " + cnf%s" % (cnfNb)
    
    rs += """
show cartoon, all_cnf
hide lines, all_cnf
set all_states, off
"""
    return rs


class PEPFOLD:

    def __init__(self):
        """
        @param options : a dictionary of the options among which 
                         some are valid.
        """
        self.version = "PEPFOLD"+ " v%s"%VERSION
        parser = self.__cmdLine()
        self.options = parser.parse_args()


    def __cmdLine(self, version = None):
        """
        Command line definition.
        These options are extra options over inherited PPP options.
        @return: command line options and arguments (optionParser object)
        """

        parser = argparse.ArgumentParser(prog = "PEPFOLD %s" % VERSION)

        parser.add_argument("--simul_type", action="store", dest="simType", type=str, default = "PEPMODEL",
                            help="Switch for simulation type")

        parser.add_argument("-s", "--iSeq", action="store", dest="seqFile", type=str, default = None,
                            help="amino acid sequence (fasta file)")

        parser.add_argument("-l", "--label", action="store", dest="label", type=str, default = PEPFOLD_DFLT_LABEL,
                            help="label for the run (%s)" % PEPFOLD_DFLT_LABEL)

        parser.add_argument("--SSBonds", action="store", dest="SSBonds", type=str, default = None,
                            help="disulfide bond definition")
        
        parser.add_argument("--Neighbours", action="store", dest="neighbours", type=str, default = None,
                            help="residue proximities")

        parser.add_argument("-n", "--nRuns", action="store", dest="nRuns", type=int, default = DFLT_MODEL_NUMBER,
                            help="number of greedy runs")

        parser.add_argument("--runsFrom", action="store", dest="runsFrom", type=int, default = 1,
                            help="number of first run")

        parser.add_argument("--maxSimSze", action="store", dest="maxSimSze", type=int, default = DFLT_MAX_SIM_SIZE,
                            help="maximal size to simulate (%s)" % str(DFLT_MAX_SIM_SIZE))

        parser.add_argument("--maxSeqSze", action="store", dest="maxSeqSze", type=int, default = DFLT_MAX_SEQ_SIZE,
                            help="maximal input sequence size  (%s)" % str(DFLT_MAX_SEQ_SIZE))

        parser.add_argument("--generator", action="store", dest="generator", type=str, default = None,
                            help="generator for trajectories  (fbt, ts3, ts4 and ts5 are allowed, default is None)")

        parser.add_argument("--script", action="store_true", dest="script", default = False,
                            help="do not run, but generate a script to run")

        parser.add_argument("--ppponly", action="store_true", dest="PPPOnly", default = False,
                            help="only run PPP")

        parser.add_argument("--combined", action="store_true", dest="combined", default = False,
                            help="generate multiple trajectories")

        parser.add_argument("--shortcombined", action="store_true", dest="shortcombined", default = False,
                            help="generate multiple trajectories")

        parser.add_argument("--asPEPFOLD1", action="store_true", dest="pepfold1", default = False,
                            help="run as pepfold1, i.e. constant bias")

        parser.add_argument("--medium", action="store_true", dest="medium", default = False,
                            help="generate multiple trajectories but use only sure bias")

        parser.add_argument("--greedyonly", action="store_true", dest="greedyOnly", default = False,
                            help="only run greedy")

        parser.add_argument("--clusteringonly", action="store_true", dest="clusteringOnly", default = False,
                            help="only run clustering")

        parser.add_argument("--posttreatmentonly", action="store_true", dest="postTreatmentOnly", default = False,
                            help="only run posttreatment")

        parser.add_argument("--parallelposttreatment", action="store_true", dest="sgePostTreat", default = False,
                            help="run posttreatment using SGE array (faster)")

        parser.add_argument("--autoFromTo", action="store_true", dest="autoFromTo", default = False,
                            help="Identify rigid core, perform 2 pass greedy on core, then on complete structure")

        parser.add_argument("--justEnrichClusters", action="store_false", dest="runClusters", default = True,
                            help="do not perform Apollo, just enrich clusters.txt")

        parser.add_argument("--refZones", action="store", dest="refZones", type=str, default = None,
                            help="Zones to define correspondance between ref and models")

        parser.add_argument("--fromTo", action="store", dest="simFromTo", type=str, default = None,
                            help="Simulate between from and to residues (included, from 1)")

        parser.add_argument("--uTrj", action="store", dest="userTrj", type=str, default = None,
                            help="user specified trajectory")

        parser.add_argument("--trjCnstr", action="store", dest="trjCnstr", type=str, default = None,
                            help="user specified constraining trajectory")

        parser.add_argument("--cs", action="store_true", dest="CS", default = False,
                            help="chemical shifts calculation for models generated")

        parser.add_argument("--csFile", action="store", dest="CSFile", type=str, default = None,
                            help="experimental chemical shifts")

        parser.add_argument("--sortKey", action="store", dest="sortKey", type=str, default = DFLT_SORT_KEY,
                            help="key to sort clusters, one of %s" % " ".join(DFLT_KEYS))

        parser.add_argument("--demo", action="store_true", dest="demo", default = None,
                            help="supersedes parameters with demo ones")

        parser.add_argument("--demo_shake", action="store_true", dest="demo_shake", default = None,
                            help="supersedes parameters with demo ones")

        parser.add_argument("--demo_complex", action="store_true", dest="demo_complex", default = None,
                            help="supersedes parameters with demo ones")

        parser.add_argument("--SVM2012", action="store_true", dest="SVM2012", default = False,
                            help="PPP using the 2012 SVM")

        parser.add_argument("--noPPP", action="store_false", dest="doPPP", default = True,
                            help="do not run PPP")

        parser.add_argument("--no3D", action="store_false", dest="do3D", default = True,
                            help="do not run 3D generation")

        parser.add_argument("--noPostTT", action="store_false", dest="doPostTT", default = False,
                            help="do not run post-treatment")

        parser.add_argument("--noClustering", action="store_false", dest="doClusters", default = False,
                            help="do not run clustering")

        parser.add_argument("--noMQA", action="store_false", dest="doMQA", default = False,
                            help="do not run model quality assessment")

        parser.add_argument("--noAddEnergy", action="store_false", dest="doAddEnergy", default = False,
                            help="do not add energies to clusters file")
                            
        parser.add_argument("--noHTMLFormatting", action="store_false", dest="doHTMLFormatting", default = True,
                            help="do not format HTML results")

        parser.add_argument("--ref_model", action="store", dest="reference",  type=str, default = None,
                            help="reference 3D model (to assess results)")

        parser.add_argument("--shake_model", action="store", dest="input_peptide_model",  type=str, default = None,
                            help="3D model to shake")

        parser.add_argument("--shake_mask", action="store", dest="input_shake_mask",  type=str, default = None,
                            help="Flexible positions in the mode to shake (uppercase to flexible, lowercase for fixed). The 4 first amino acids MUST HAVE THE SAME CASE (correspond to the first structural alphabet fragment). The sequence MUST MATCH THAT OF THE QUERY PEPTIDE AND THAT OF THE MODEL TO SHAKE")

        parser.add_argument("--receptor", action="store", dest="receptor",  type=str, default = None,
                            help="Protein receptor for protein-peptide docking.")

        parser.add_argument("--receptor_patch", action="store", dest="receptor_patch",  type=str, default = None,
                            help="Residues defining the patch on receptor surface on which the peptide will be folded.")

        parser.add_argument("--max-ppfld-centroids", action="store", dest="ppfld_centroids", type=int, default = DFLT_MAX_PEPFOLD_CENTROIDS,
                            help="number of PEPFOLD centroids used for docking (%d)" % DFLT_MAX_PEPFOLD_CENTROIDS)

        parser.add_argument("--nCores", action="store", dest="nCppCores", type=int, default = DFLT_PEPFOLD_CPPNCORES,
                            help="number of cores used during model generation (%d)" % DFLT_PEPFOLD_CPPNCORES)

        parser.add_argument("--nPPPCores", action="store", dest="nPPPCores", type=int, default = DFLT_PEPFOLD_PPPNCORES,
                            help="number of cores used during PyPPP generation (%d)" % DFLT_PEPFOLD_PPPNCORES)

        parser.add_argument("--2012", action="store_true", dest="model2012", default = False,
                            help="Use the 2012 model and blastpgp")

        parser.add_argument("--2006", action="store_true", dest="model2006", default = False,
                            help="Use the 2006 model and blastpgp")

        parser.add_argument("--2016", action="store_true", dest="model2016", default = False,
                            help="Use the 2016 model and psiblast_mod")

        parser.add_argument("--outModelOffset", action="store", dest="modelOffset", type=int, default = 0,
                            help="number of first model generated (default: 0)")
        
        parser.add_argument("--seed", action="store", dest="seed", type=int, default = -1,
                            help="pseudo random generator seed to synchronize threads (default: -1)")

        parser.add_argument("--sort_cls_by_size", action="store_true", dest="sortbysize", default = False,
                            help="sort clusters by size instead of sOPEP")
                            
        parser.add_argument("--mcSteps", action="store", dest="mcSteps", type=int, default = 30000,
                            help="number of MonteCarlo steps")
                
        parser.add_argument("--mcT", action="store", dest="mcT", type=float, default = 370,
                            help="MonteCarlo temperature")

        # === P. Tuffery 2021: Debye-Huckel contribution 
        parser.add_argument("--use_DH", action="store_true", dest="use_DH", default = False,
                            help="Switch ON Debye-Huckel contribution")

        parser.add_argument("--wDH", action="store", dest="DH_weight", type=float, default = 1.,
                            help="the weight of the Debye-Huckel contribution")

        parser.add_argument("--pH", action="store", dest="pH", type=float, default = 7.,
                            help="the pH of the simulation")
        
        parser.add_argument("--ionic_strength", action="store", dest="ionic_strength", type=float, default = 150.,
                            help="the ionic strength (NaCl concentration - mM) of the simulation")
        
        parser.add_argument("--epsilon", action="store", dest="epsilon", type=float, default = 78.,
                            help="the dielectric constant")
        
        parser.add_argument("--neutral_ext", action="store", dest="neutral_ext", type=str, default = "None",
                            help="consider neutral extremities (None, Both, Nter, Cter)")

        parser.add_argument("--user_pKa", action="store", dest="user_pKa", type=str, default = "None",
                            help="file specifying pKa for specific residues (one per line: index_from_1 pKa_value)")

        # === P. Tuffery 2021: Biasing profile using user defined secondary structures 
        parser.add_argument("--iPrfBias", action="store", dest="prfBias", default = None,
                            help="Bias profile using a sequence of '-','H','E'")
 
        # === P. Tuffery 2021: Pseudocount values to bias profile 
        parser.add_argument("--extBias", action="store", dest="extBias", type=float, default = 0.,
                            help="strength of bias towards extended")
 
        parser.add_argument("--hlxBias", action="store", dest="hlxBias", type=float, default = 0.,
                            help="strength of bias towards helical")

        # === P. Tuffery 2021: Uniform is exclusive of user defined 
        parser.add_argument("--uniBias", action="store", dest="uniBias", type=float, default = 0.,
                            help="strength of bias towards uniform distribution")

        # === P. Tuffery 2021: User profile 
        parser.add_argument("--iPrf", action="store", dest="probFile", default = None,
                            help="User defined probabilities")

        return parser


    def check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy

        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """

        # Check input data exists
        if self.options.seqFile:
            try:
                x = Fasta.fasta(self.options.seqFile)
                self.aaSeq = x[x.ids()[0]]
                # we write the sequence in a .seq file (seems mandatory for CppBuilder) 
                f = open("%s.seq" % self.options.label, "w")
                f.write("%s\n" % self.aaSeq.s())
                f.close()
            except:
                if verbose:
                    sys.stderr.write("PEP-FOLD 4: Failed to load sequence (tried %s).\n" % self.options.seqFile)
                return False
        else:
            if verbose:
                sys.stderr.write("PEP-FOLD 4: must specify sequence. --help for option detail.\n")
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: invalid input sequence or PDB file.", "./")
            return False

        self.options.runPPP           = True
        self.options.runGreedy        = True
        self.options.runClustering    = True
        self.options.runPostTreatment = True
        self.options.runClustering    = False
        self.options.runPostTreatment = False
        self.options.CSTabFile        = None
        if self.options.PPPOnly: 
             self.options.runPPP           = True
             self.options.runGreedy        = False
             self.options.runClustering    = False
             self.options.runPostTreatment = False
        if self.options.greedyOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = True
             self.options.runClustering    = False
             self.options.runPostTreatment = False
        if self.options.clusteringOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = False
             self.options.runClustering    = True
             self.options.runPostTreatment = False
        if self.options.postTreatmentOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = False
             self.options.runClustering    = False
             self.options.runPostTreatment = True


        if not self.aaSeq:
            sys.stderr.write("PEP-FOLD 4: Failed to read sequence to process.\nPlease check your input sequence.")
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: Invalid sequence.", "./")
            return False

        # Check seq length is OK.
        if len(self.aaSeq) < DFLT_MIN_SEQ_SIZE:
            sys.stderr.write("PEP-FOLD 4: Too short sequence (min is %d)\n" % DFLT_MIN_SEQ_SIZE)
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: Too short sequence (%d / %d)." % (len(self.aaSeq), DFLT_MIN_SEQ_SIZE), "./")
            return False
        if len(self.aaSeq) > self.options.maxSeqSze:
            sys.stderr.write("PEP-FOLD 4: Too long sequence (max is %d)\n" % self.options.maxSeqSze)
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: Too long sequence (%d / %d)." % (len(self.aaSeq), self.options.maxSeqSze), "./")
            return False

        # check only standard amino acids are in use.
        for aa in self.aaSeq:
            if aa not in "ACDEFGHIKLMNPQRSTVWY":
                sys.stderr.write("PEP-FOLD 4: unkown amino acid %s\n" % aa)
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: unkown amino acid  %s." % (aa), "./")
                return False
        
        # Check PDB reference is a PDB and is of correct length
        if self.options.reference is not None:
            try:
                x = PDB.PDB(self.options.reference)
                sys.stderr.write("Reference size is %d\n" % len(x))
            except:
                sys.stderr.write("Sorry: reference file does not seem a valid PDB. \n")
                self.options.reference = None
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Reference file does not seem a valid PDB.", "./")
                return False
            if not len(x):
                sys.stderr.write("Sorry: reference file does not seem a valid PDB. \n")
                self.options.reference = None
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Reference file does not seem a valid PDB.", "./")
                return False
                
        # Check SS Bonds correspond to CYS residues
        if self.options.SSBonds is not None:
           # This if for the PEPFOLD server
           self.options.pepfold1 = True         
           self.options.nRuns    = 100 # short combined
           if self.options.combined:
               self.options.nRuns = 200
           self.options.SSA = 10.
           # Now we check
           sslist =  self.options.SSBonds.split(":")
           for ss in sslist:
               aas = ss.split("-")
               if len(aas) != 2:
                   sys.stderr.write("PEP-FOLD 4: Incorrect SS bond specification. Incorrect syntax: %s.\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: Incorrect SS bond specification. Incorrect syntax: %s." % ss, "./")
                   return False
               if int(aas[0]) > int(aas[1]):
                   aas[0],aas[1] = aas[1], aas[0]
               if aas[0] == aas[1]:
                   sys.stderr.write("PEP-FOLD 4: Incorrect SS bond specification. Cannot make intra residue SS bond (%s).\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: Incorrect SS bond specification. Cannot make intra residue SS bond (%s)." % ss, "./")
                   return False
               if self.aaSeq[int(aas[0])-1] != "C":
                   sys.stderr.write("PEP-FOLD 4: Incorrect SS bond specification. Is residue %s a cysteine ?\n" % aas[0])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: Incorrect SS bond specification. Is residue %s a cysteine ?" % aas[0], "./")
                   return False
               if self.aaSeq[int(aas[1])-1] != "C":
                   sys.stderr.write("PEP-FOLD 4: Incorrect SS bond specification. Is residue %s a cysteine ?\n" % aas[1])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: incorrect SS bond specification. Is residue %s a cysteine ?" % aas[1], "./")
                   return False

        # Check neighbor specifications are OK.
        if self.options.neighbours is not None:
           self.options.NBA = 10.
           # Now we check
           sslist =  self.options.neighbours.split(":")
           for ss in sslist:
               aas = ss.split("-")
               if len(aas) != 2:
                   sys.stderr.write("PEP-FOLD 4: incorrect neighbor constraint specification. Incorrect syntax: %s.\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: incorrect neighbor constraint specification. Incorrect syntax: %s." % ss, "./")
                   return False
               if int(aas[0]) > int(aas[1]):
                   aas[0],aas[1] = aas[1],aas[0]
               if aas[0] == aas[1]:
                   sys.stderr.write("PEP-FOLD 4: incorrect neighbor constraint specification. Cannot make intra residue neighbor (%s).\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 4: incorrect neighbor constraint specification. Cannot make intra neighbor (%s)." % ss, "./")
                   return False

        # if peptide size < 20 aa, we set builder to forward back track
        self.builder = "ts"
        self.ts_size = 5
        if len(self.aaSeq) < 20:
          self.builder = "fbt"


        # If generator is specified, then we explicitely use it
        if self.options.generator is not None:
            if self.options.generator == "fbt":
                self.builder = "fbt"
            elif self.options.generator == "ts3":
                self.builder = "ts"
                self.ts_size = 3
            elif self.options.generator == "ts4":
                self.builder = "ts"
                self.ts_size = 4
            elif self.options.generator == "ts5":
                self.builder = "ts"
                self.ts_size = 5
          
        # Check chemical shifts specifications are OK.
        if self.options.CSFile:
            import PPMTools
            try:
                x = PPMTools.sxp_input(self.options.CSFile)
                if not len(x[0]):
                    sys.stderr.write("PEP-FOLD 4: Could not read any chemical shift data. Is format correct ?\n")
                    if REMOTE_SERVER:
                        cluster.progress("PEP-FOLD 4: could not read any chemical shift data. Is format correct ?", "./")
                    return False
                self.options.CSTabFile = "%s.tab" % self.options.CSFile
                rs = PPMTools.talosoutput(x, where=self.options.CSTabFile)
            except:
                sys.stderr.write("PEP-FOLD 4: Chemical Shift file input failed\n")
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: failed to read Chemical Shifts.", "./")
                return False
            self.options.CS = True

        # Remove banks in label
        self.options.label = self.options.label.replace(" ","_")

        # Checks related to model shake
        self.options.s4TrjFile     = None
        self.options.shakeMaskFile = None
        self.options.shakePos      = 0
        if (self.options.input_peptide_model is not None) and (not self.options.input_shake_mask):
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: BOTH Model to shake and shake mask must be specified. Cannot proceed.", "./")
                return False
 
        # shake mask: check expected sequence properties are met. 
        if self.options.input_shake_mask is not None:
            s = Fasta.fasta(self.options.input_shake_mask)
            self.aaMaskSeq = s[s.ids()[0]].s()

            if self.aaMaskSeq.upper() !=  self.aaSeq.s():
                cluster.progress("PEP-FOLD 4: Sequence of peptide to model (%s) and sequence to define shake mask (%s) do not match." % (self.aaMaskSeq.upper(), self.aaSeq.s()), "./")
                return False

            if self.aaMaskSeq[0].islower() and ( self.aaMaskSeq[1].isupper() or self.aaMaskSeq[2].isupper() or self.aaMaskSeq[3].isupper() ):
                cluster.progress("PEP-FOLD 4: Four first amino acids of shake mask MUST HAVE the same case. Got %s." % (self.aaMaskSeq[0:4]), "./")
                return False

            if self.aaMaskSeq[0].isupper() and ( self.aaMaskSeq[1].islower() or self.aaMaskSeq[2].islower() or self.aaMaskSeq[3].islower() ):
                cluster.progress("PEP-FOLD 4: Four first amino acids of shake mask MUST HAVE the same case. Got %s." % (self.aaMaskSeq[0:4]), "./")
                return False

            # Here we setup the shaking mask
            rs = ""
            if self.aaMaskSeq[0].isupper():
                rs = rs + "0"
            else:
                rs = rs + "1"
            for aa in self.aaMaskSeq[4:]:
                if aa.isupper():
                    rs = rs + "0"
                    self.options.shakePos += 1
                else:
                    rs = rs + "1"
            self.boolMask = rs
            if self.options.shakePos < 10:
                self.builder = "fbt"
            if len(self.boolMask) != (len(self.aaMaskSeq ) - 3):
                cluster.progress("PEP-FOLD 4: Some problem occurred while generating the shake mask. Please report this error.", "./")
                return False
            if verbose:
                sys.stderr.write("Shake mask : %s (aaMskSeq: %s)\n" % (self.boolMask, self.aaMaskSeq) )
            self.options.shakeMaskFile = "%s.shakemask" % self.options.label
            f = open(self.options.shakeMaskFile, "w")
            f.write("%s\n" % self.boolMask)
            f.close()

        # shake mask: check expected sequence properties are met. 
        if self.options.input_peptide_model is not None:
            # Check PDB model is valid
            isPDB, msg = PDB.isPDB(self.options.input_peptide_model)
            if (not isPDB):
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Model to shake does not seem a valid PDB.", "./")
                return False
            x = PDB.PDB(self.options.input_peptide_model, hetSkip=1, keepH=0)
            xaa = x.aaseq()
            x.out("%s.shake_pdb" % self.options.label)
            if xaa.upper() !=  self.aaSeq.s():
                cluster.progress("PEP-FOLD 4: Sequence of peptide (%s) to model and sequence of 3D model to shake (%s) do not match." % (self.aaSeq.s(), x.aaseq().upper()), "./")
                return False

            self.runPDBSAWithCppBuilder(verbose = verbose)


        # Checks related to protein receptor
        if (not self.options.receptor ) and (self.options.receptor_patch is not None):
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: BOTH receptor file and residues defining the patch must be specified. Cannot proceed.", "./")
                return False
        if (self.options.receptor  is not None) and (not self.options.receptor_patch):
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: BOTH receptor file and residues defining the patch must be specified. Cannot proceed.", "./")
                return False

        if self.options.receptor is not None:

            # Check PDB model is valid
            isPDB, msg = PDB.isPDB(self.options.receptor)
            if (not isPDB):
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Receptor file does not seem a valid PDB.", "./")
                return False
            x = PDB.PDB(self.options.receptor, hetSkip=1)
            x.out("%s.receptor_pdb" % self.options.label)

            if not check_ungapped_pdb(x):
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Receptor file seems to have missing residues, which is not accepted. Please check.", "./")
                return False

            # rList = self.options.receptor_patch.split(",")
            f = open(self.options.receptor_patch)
            rList = f.readlines()
            f.close()
            try:
                y = x.subPDBFromResList(rList)
            except: # 
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Some error occurred when checking receptor patch residues.", "./")
                    return False
            if (y is None) or (len(y) == 0):
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 4: Receptor patch residues do not seem to define a valid patch. Please check.", "./")
                    return False

            # Generate start points from patch
            out, outlines = startCrdsFromPatchResidues("%s.receptor_pdb" % self.options.label, rList)
            f = open("%s.start_crds" % self.options.label, "w")
            for l in outlines:
                f.write("%s\n" % l)
            f.close()

        if self.options.seed == -1:
            self.options.seed = random.randint(1, 1000000)

        if self.options.probFile is not None:
            with open(self.options.probFile) as f:
                pList = f.readlines()
            pList = [ pLine.split() for pLine in pList ]
            if len(pList) != (len(self.aaSeq) - 3):
                cluster.progress("PEP-FOLD 4: Length of peptide sequence and SA prediction profile do not match.", "./")
                return False
            uniform = True
            for pLine in pList:
                if len(pLine) != 27:
                    uniform = False
                try:
                    [ float(prob) for prob in pLine]
                except:
                    uniform = False
            if not uniform:
                cluster.progress("PEP-FOLD 4: SA prediction profile wrong format.", "./")
                return False


        # We determine the number of tasks to launch with Slurm    
        self.nTasks = int(round(float(self.options.nRuns)/self.options.nCppCores))

        return True


    def PDBfiles(self, wPath = ".", lbl = None, pass1 = False, verbose = 0):
        """
        @param wPath: directory to scan
        @param lbl  : file prefix to search for
        return a list of the names of the PDB files generated
        """

        # Label to search for
        if not lbl:
            lbl=self.options.label

        # List files of the current directory
        filesL = os.listdir( wPath )

        # PDB files to clusterize
        pdblist = []
        pattern = "%s*bestene1mc.pdb" % lbl

        # List them

        for aFile in filesL:
            if fnmatch.fnmatch( aFile, pattern ) and FileTools.checkFileStatus( aFile, verbose = verbose ) == 0:
                if aFile.count("pass1") and not pass1:
                    continue
                pdblist.append( aFile )

        # Return the list
        return pdblist


    def htmlResults(self, results, maxBest = 10, verbose = False):

        f = open(results)
        lines = f.readlines()
        f.close()

        rs, nclusters, nmodels = self.clusterHTMLize(lines, self.options.label)
        f = open("clusters.html", "w")
        f.write("".join(rs))
        f.close()


    def clusterHTMLize(self, lines, label = "", verbose = 0):
        """
        Append to lines in the clusters format the RMSd value
        @param lines   : the clusters.txt lines
        @param label   : the label as prefix to label-modelx.pdb
        @param verbose : switch verbose mode
        @return        : html page with link to the files and archives

        Test:

        import sys, os
        f = open("clusters.txt")
        lines = f.readlines()
        f.close()

        rs, nclusters, nmodels = clusterHTMLize(lines, "test")
        f = open("clusters.html", "w")
        f.write("".join(rs))
        f.close()
        """
        if verbose: 
            sys.stderr.write("Will htmlize the cluster retport\n")
        rs = ["<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n", "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" >\n", "<head>\n", "<title> PEP-FOLD clusters</title>\n","<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-15\" />\n", "</head>\n","<body style=\"font-family:monospace;font-size:11px;line-height:18px;\">\n","Download archive of all models <a href=\"allModels.tgz\"> here </a><br/>"]
        clusterfiles = []
        clusternumber = None
        nmodels = 0
        nclusters = 0
        allcmd = "tar czf allModels.tgz "

        for l in lines:
            if l[0] == '#':
                # if l.count("gdt      max"):
                if l.count("# file"):
                    it = l.split()
                    arraysize = len(it)
                    rs.append("<table>\n")
                    oit = ["<td>%s</td>" % x for x in it]
                    rs.append("<tr>%s   </tr>\n" % (" ".join(oit)))
                else:
                    if l.count("# cluster"):
                        if clusternumber and (len(clusterfiles) > 1):
                            cmd = "tar czf cluster%s.tgz %s" % (clusternumber, " ".join(clusterfiles))
                            if verbose: 
                                sys.stderr.write("%s\n" % cmd)
                            os.system(cmd)
                        nclusters += 1
                        clusterfiles = []
                        it = l.split()
                        clusternumber = str(int(it[2])+1)
                        modelnumber = 0
                        try:
                            cEff = int(it[-1])
                        except:
                            cEff = 1
                        if cEff > 1:
                            rs.append("<tr><td># <a href=\"./cluster%s.tgz\">cluster %s</a> %s </td></tr>\n" % (clusternumber, clusternumber, " ".join(it[3:])))
                        else:
                            rs.append("<tr><td># cluster %s %s </td></tr>\n" % (clusternumber, " ".join(it[3:])))
            else:
                it = l.split()
                if not len(it):
                    rs.append("<tr><td>%s  </td></tr>\n"% l[:-1])
                    continue
                nmodels += 1
                mfile = l.split()[0]
                clusterfiles.append(mfile)
                it = l.split()
                if modelnumber == 0:
                    modelname = "%s-model%s" % (label, clusternumber)
                else:
                    modelname = "%s-model%s.%d" % (label, clusternumber, modelnumber)
                cmd = "cp %s %s.pdb" % (mfile, modelname)
                allcmd = "%s %s.pdb" % (allcmd, modelname)
                if verbose: 
                    sys.stderr.write("%s\n" % cmd)
                os.system(cmd)
                # rs.append("<a href=\"%s\">%s</a> %s<br/>\n" % (it[0], modelname, " ".join(it[1:])))
                lit = ["<a href=\"./%s.pdb\">%s</a>" % (modelname, modelname)] + it[1:]
                oit = ["<td>%s</td>" % x for x in lit]
                rs.append("<tr><td>&nbsp;</td>%s   </tr>\n" % (" ".join(oit)))
                modelnumber += 1
                
        rs.append("</table><br/>\n")
        rs.append("</body>\n")
        rs.append("</html>\n")
        if clusternumber and (len(clusterfiles) > 1):
            cmd = "tar czf cluster%s.tgz %s" % (clusternumber, " ".join(clusterfiles))
            if verbose: 
                sys.stderr.write("%s\n" % cmd)
            os.system(cmd)
        os.system(allcmd)

        return rs, nclusters, nmodels


    def extractPeptideChain(self, pepChainLabel = "A"):
        if False:
            # Just a fake until gromacs minimizer works
            theList = glob.glob("*SC.pdb")
            for aFile in theList:
                shutil.copy2( aFile, aFile.replace("-SC.pdb", "-SC-min.pdb") )

        # The actual stuff
        theList = glob.glob("*SC-min.pdb")
        for aFile in theList:
            x = PDB.PDB(aFile)
            y = x[pepChainLabel]
            y.out( aFile.replace("-SC-min.pdb","-SC-min-pep.pdb" ) )

        # We also setup the receptor for visualization
        shutil.copy2( self.options.receptor, "receptor.pdb")


    def formatViewer(self, label = "PEPFOLD", wdir = "./"):

        html_file = 'bestModels.html'
        pdb_file = label + '-5bestmodels.pdb'
        with open(html_file, 'wt') as sink:
            template = jinja_env.get_template('pepfold4.vis.html')
            sink.write(template.render(pdb_file = pdb_file, wdir = wdir))


    def runPPP(self, verbose = False):

        cmd = "PyPPP3Exec"
        args = ["-s %s" % self.options.seqFile,
                "-l %s" % self.options.label,
                "-v"]

        if self.options.model2016:
            args.append("--2016")
        elif self.options.model2006:
            args.append("--2006")
        else :
            args.append("--2012") # default is 2012

        if verbose:
            sys.stderr.write("%s %s\n" % ( cmd , " ".join(args) ) )

        cluster.runTasks(cmd, args, job_opts = '-c %s' % self.options.nPPPCores, environment_module = "pyppp3-light/1.0-rpbs", log_prefix = "ppp")


    def runCppBuilderEnergy(self, verbose = False):

        files = glob.glob("%s-*ene1-mc_SC-min.pdb" % self.options.label)
        f = open("energy_list.txt","w")
        for file in files:
            f.write(" --i3D %s --oEne %s.ene\n" % (file, os.path.splitext(file)[0]))
        f.close()

        cmd = "CppBuilder"
        args = ["--objective sopep",
                "--action ecalc",
                "--generator %s" % self.builder,
                "--iW NONE --sOPEP1 --noTabEne",
                '$(sed -n "${TASK_ID}p" energy_list.txt)',
                ]

        if self.options.SSBonds is not None:
            args.append("--SS %s" % self.options.SSBonds)
        if self.options.neighbours is not None:
            args.append("--NB %s" % self.options.neighbours)

        if self.options.receptor:
            args.append("--iReceptor %s.receptor_pdb" % self.options.label)
            args.append("--iOriginCrdFile %s.start_crds" % self.options.label)
            args.append("--chPiv 1000")
            args.append("--outModelOffset %d" % self.options.modelOffset)
            if self.options.extBias:
                args.append("--extBias %f" % EXT_BIAS_VALUE)
            # args.append("--outPepOnly") # Better to split afterwards, since we want to return clean complexes

        if self.options.use_DH:
            args.append("--sOPEP_use_DH")
            args.append("--sOPEPwDH %f" % self.options.DH_weight)
            args.append("--sOPEPpH %f" % self.options.pH)
            args.append("--sOPEPionicstr %f" % self.options.ionic_strength)
            args.append("--sOPEPepsi %f" % self.options.epsilon)
            if self.options.neutral_ext == "Both":
                args.append("--sOPEPneutral_ext")
            elif self.options.neutral_ext == "Nter":
                args.append("--sOPEPneutral_Nter")
            elif self.options.neutral_ext == "Cter":
                args.append("--sOPEPneutral_Cter")

            if self.options.user_pKa is not None:
                args.append("--sOPEP_DH_user_pKa_file %s" % self.options.user_pKa)

        if verbose:
            sys.stderr.write("%s %s\n" % ( cmd , " ".join(args) ) )
            sys.stderr.write("nRuns: %d\n" % self.options.nRuns)
        cluster.runTasks(cmd, args, tasks = self.options.nRuns, job_opts = '-c 1', environment_module = "pepfold-core/1.0-rpbs", log_prefix = "cppbuilderenergy")


    def clusterUpdateEnergy(self, verbose = False):
        f = open("%s_clusters.txt" % self.options.label)
        lines = f.readlines()
        f.close()
        ll = clusterAppendCppBuilderEneFromEneFile(lines)
        ll2 = clustersSortBy(ll, "minsOPEP")
        shutil.copy("%s_clusters.txt" % self.options.label, "%s_clusters.txt.ori" % self.options.label)
        f = open("%s_clusters.txt" % self.options.label, "w")
        f.write("%s" % "".join(ll2))
        f.close()


    def PDBFilesUpdateEnergy(self, verbose = False):
        f = open("%s_clusters.txt" % self.options.label)
        lines = f.readlines()
        f.close()
        for l in lines:
            if l[0] == '#':
                continue

            it = l.split()
            if not len(it):
                continue

            # pfile = l.split()[0].replace(".pdb","_SC-min.pdb") # PDB file
            pfile = l.split()[0] # PDB file
            f=open(pfile)
            plines = f.readlines()
            f.close()

            # mfile = l.split()[0].replace(".pdb","_SC-min.ene") # ene file
            mfile = l.split()[0].replace(".pdb",".ene") # ene file
            f=open(mfile)
            flines = f.readlines()
            f.close()

            prs = []
            # first ATOM position
            pstart = 0
            while plines[pstart].startswith("ATOM") == False:
                pstart += 1

            prs.append("HEADER  %s\n" % pfile)
            is_open=False
            for fl in flines:
                if fl.count("sOPEP Energy:"):
                    prs.append("REMARK   %s\n" % fl.strip())
                    continue
                if fl.count("eCACA"):
                    prs.append("REMARK %s\n" % fl.strip())
                    is_open = True
                    continue
                if is_open:
                    prs.append("REMARK %s\n" % fl.strip())
            prs = prs + plines[pstart:]
            shutil.copy(pfile, pfile+".ori")
            f = open(pfile, "w")
            f.write("".join(prs))
            f.close()


    def runCppBuilder(self, verbose = False):

        cmd = "CppBuilder"
        args = ["--objective sopep",
                "--action pepbuild",
                "--generator %s" % self.builder,
                "--iPrf %s.svmi8.27.prob" % self.options.label,
                "--iSeq %s.seq" % self.options.label,
                "--label %s" % self.options.label,
                "--rndFrom 100",
                "--heapSze 300",
                "--mcSteps %d" % self.options.mcSteps,
                "--mcT %f" % self.options.mcT,
                "--nc %d" % self.options.nCppCores,
                "--nSim %s" % self.options.nRuns,
                "--prfFilter none",
                "--ts_sze %d" % self.ts_size,
                "--aa_models",
                "--iW NONE --sOPEP_version 2 --noTabEne", # "--sOPEP1" # "--sOPEP_version 1", 
                "--seed %d"   % self.options.seed]

        if self.options.SSBonds is not None:
            args.append("--SS %s" % self.options.SSBonds)
        if self.options.neighbours is not None:
            args.append("--NB %s" % self.options.neighbours)

        if self.options.s4TrjFile:
            args.append("--iS4Trj %s" % self.options.s4TrjFile)

        if self.options.trjCnstr:
            args.append("--iTrjCnstr %s" % self.options.trjCnstr)
        elif self.options.shakeMaskFile:
            args.append("--s4_shake")
            args.append("--iS4_shake_mask %s" % self.options.shakeMaskFile)

        if self.options.receptor:
            args.append("--iReceptor %s.receptor_pdb" % self.options.label)
            args.append("--iOriginCrdFile %s.start_crds" % self.options.label)
            args.append("--chPiv 1000")
            args.append("--outModelOffset %d" % self.options.modelOffset)
            if self.options.extBias:
                args.append("--extBias %f" % EXT_BIAS_VALUE)

        if self.options.use_DH:
            args.append("--sOPEP_use_DH --unbias")
            args.append("--sOPEPwDH %f" % self.options.DH_weight)
            args.append("--sOPEPpH %f" % self.options.pH)
            args.append("--sOPEPionicstr %f" % self.options.ionic_strength)
            if self.options.neutral_ext == "Both":
                args.append("--sOPEPneutral_ext")
                args.append("--add_ace")
                args.append("--add_nh2")
            elif self.options.neutral_ext == "Nter":
                args.append("--sOPEPneutral_Nter")
                args.append("--add_ace")
            elif self.options.neutral_ext == "Cter":
                args.append("--sOPEPneutral_Cter")
                args.append("--add_nh2")
            if self.options.user_pKa is not None:
                args.append("--sOPEP_DH_user_pKa_file %s" % self.options.user_pKa)

        if self.options.prfBias is not None:
            args.append("--iPrfBias %s" % self.options.prfBias) # check
            args.append("--extBias %s" % self.options.extBias)
            args.append("--hlxBias %s" % self.options.hlxBias)

        if self.options.uniBias > 0.000000001:
            args.append("--uniBias %s" % str(self.options.uniBias))

        if verbose or True:
            sys.stderr.write("%s %s\n" % ( cmd , " ".join(args) ) )
        cluster.runTasks(cmd, args, tasks = 1, job_opts = '-c %s' % self.options.nCppCores, environment_module = "pepfold-core/1.0-rpbs", log_prefix = "cppbuilder")


    def runOscarStar(self, verbose = False):

        files = glob.glob("%s-*ene1-mc.pdb" % self.options.label)
        f = open("oscar_list.txt","w")
        f.write("\n".join(files))
        f.close()

        cmd = "OscarExec"
        args = ["-i ", '$(sed -n "${TASK_ID}p" oscar_list.txt)',
                "-v"]
        if verbose:
            sys.stderr.write("%s %s\n" % ( cmd , " ".join(args) ) )
        cluster.runTasks(cmd, args, tasks = self.options.nRuns, environment_module = "oscar-star/1.0-rpbs", log_prefix = "oscar")


    def runPDBSAWithCppBuilder(self, verbose = False):
        """
        This  version makes possible to get SALetter.proto encoding.
        """

        cmd = "CppBuilder"
        args = ["--objective carmsd",
                "--action pepbuild",
                "--generator fbt",
                "--3D_dmn",
                "--iTemplate %s.shake_pdb" % self.options.label,
                "--iSeq %s.seq" % self.options.label,
                "--label %s-rebuild" % self.options.label,
                "--rndFrom 200",
                "--heapSze 600",
                "--mcSteps 10000",
                "--nc %d" % self.options.nCppCores,
                "--nSim 8",
                "--seed %d"   % self.options.seed]

        if verbose:
            sys.stderr.write("%s %s\n" % ( cmd , " ".join(args) ) )
        cluster.runTasks(cmd, args, tasks = 1, job_opts = '-c %s' % self.options.nCppCores, environment_module = "pepfold-core/1.0-rpbs", log_prefix = "pdbsa")

        # Identify best reconstruction
        f = open("pepfoldcore3.5.log")
        lines = f.readlines()
        f.close()
        ibestSim = int(lines[-1].split()[1].replace(":",""))
        bestSim = "%.5d" % ibestSim
        self.options.bestTrj = "%s-rebuild-%s_bestene1-mc.trj" % (self.options.label, bestSim)

        # Generate the --iTrjCnstr file
        f = open(self.options.shakeMaskFile)
        lines = f.readlines()
        f.close()
        mask = lines[0].split()[0]

        f = open(self.options.bestTrj)
        lines = f.readlines()
        f.close()

        if len(mask) != len(lines):
            sys.stderr.write("PEP-FOLD 4: Sorry: some problem occured during best approximation search.\nMask size (%d) incompatible with that of trajectory (%d)" % (len(mask), len(lines)))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 4: Sorry: some problem occured during best approximation search.\nMask size (%d) incompatible with that of trajectory (%d)" % (len(mask), len(lines)), "./")
            sys.exit(0)

        # Write the constraints.
        rs = []
        for pos in range(0, len(mask)):
            if mask[pos] == "1":
                rs.append(lines[pos])
        self.options.trjCnstr="%s-rebuild.cnstr.trj" % self.options.label
        f = open(self.options.trjCnstr, "w")
        f.write("".join(rs))
        f.close()


    def runPepFold(self, verbose = False):
        """
        This part to run PEPFOLD
        """

        self.start_time = time.time() # To track execution time

         # Local prediction
        if self.options.doPPP:
            if self.options.probFile is None:
                cluster.progress("PEP-FOLD 4: Local prediction", wdir = "./")
                self.runPPP(verbose = verbose)
            else:
                cluster.progress("PEP-FOLD 4: Using user input local prediction", wdir = "./")
                os.link(self.options.probFile, "%s.svmi8.27.prob" % self.options.label)

        # 3D generation
        if self.options.do3D:
            cluster.progress("PEP-FOLD 4: 3D generation", wdir = "./")
            self.runCppBuilder(verbose = verbose)
            if self.options.receptor:
                # For complexes, 2nd series of runs.
                # ~ self.options.extBias = True
                self.options.modelOffset = self.options.nRuns
                self.runCppBuilder(verbose = verbose)

        # Post Treatment
        if self.options.doPostTT:
            cluster.progress("PEP-FOLD 4: Post treatment", wdir = "./")
            cluster.progress("PEP-FOLD 4: Side chain positioning", wdir = "./")
            self.runOscarStar(verbose = verbose)
            cluster.progress("PEP-FOLD 4: Minimisation", wdir = "./")
            self.runGromacsMinimisation(verbose = verbose)
            cluster.progress("PEP-FOLD 4: Energy update", wdir = "./")
            self.runCppBuilderEnergy(verbose = verbose)
            self.clusterUpdateEnergy(verbose = verbose)
            self.PDBFilesUpdateEnergy(verbose = verbose)

        # MQA
        if self.options.doMQA and not self.options.receptor:
            cluster.progress("PEP-FOLD 4: Model quality assessment", wdir = "./")
            self.runMQA(verbose = verbose)

        # self.runClustersTools()
        cluster.progress("PEP-FOLD 4: Formatting results", wdir = "./")
        if self.options.doHTMLFormatting:
            self.htmlResults("%s_clusters.txt" % self.options.label, maxBest = 5)
            jobId = os.path.basename(os.getcwd())
            wdir = DFLT_MOBYLE_JOBS_PATH + "/" + jobId
            self.formatViewer(label = self.options.label, wdir = wdir)

        # we estimate the time it took
        self.elapsed_time = time.time() - self.start_time
        minutes = int(self.elapsed_time) / 60
        seconds = round(self.elapsed_time % 60)
        cluster.progress("PEP-FOLD 4: Completed (%dmin %dsec)" % (minutes, seconds), wdir = "./")


# =======================================================================
#
# General run switch
#
# =======================================================================

def main():
    """
    Launch the main purpose:
    - Parse command line
    - Launch predictions
    """

    # Parse arguments
    job = PEPFOLD()

    # Check command line and arguments are OK
    cmd_line_status = job.check_options(verbose = True) # Now we check
    if not cmd_line_status:
        sys.stderr.write("PEP-FOLD 4: Sorry, incorrect command line.\n")
        if True or REMOTE_SERVER:
            cluster.progress("PEP-FOLD 4: Failed when verifying input data. Please check carefully.")
        sys.exit(-1)

    # From here, ready to proceed
    job.runPepFold(verbose = True)
    if True or REMOTE_SERVER:
        cluster.progress("PEP-FOLD 4: Completed successfully.")

    return 

# GO !
if __name__ == "__main__":
    main()
# -------------------------
