#!/usr/bin/python

# FAF-QED wrapper to run on RPBS cluster
#

# Imports
import cluster, os, sys, cPickle

# Constants
dico_dump = "multi_params_dico.dump"
# Functions



if __name__ == "__main__":
    
    print '\n---> FAF-QED (April 2017) running ...'
    # 0 - Check the arguments
    argumentsToPass = ["--prepare"] + sys.argv[1:]
    #print(argumentsToPass)

    # 1 - Call FAFQED to prepare the bank
    print "---> Data curation step ..."
    cluster.progress("1/5 Preparing databank")
    cluster.runTasks("/usr/local/FAF-Drugs/bin/FAFQED.py", argumentsToPass, docker_img="fafqed")
   
    print "---> Preparing multithreads jobs ..."
 
    # 2 - Load parameters
    print "---> Loading parameters and options ..."
    cluster.progress("2/5 Loading parameters")
    options = cPickle.load(open(dico_dump))
    chunkList = options["list_of_chunks"]
    with open("list_of_dico_dump.dump","w") as dump_file:
        cPickle.dump([os.path.splitext(chunk)[0] + ".dump" for chunk in chunkList] , dump_file)

    ## exemple ["chunk1", "chunk2", "chunk3"]
    
    # 3 - Run FAFQED Loop
    print "---> Filtering step ..."
    cluster.progress("3/5 Running FAF-QED")
    cluster.runTasks("/usr/local/FAF-Drugs/bin/FAFQED.py", ["--fafonly", "fake.sdf"], map_list = chunkList, docker_img="fafqed")
    
    ### Ici le passage de map_list va lancer un pickle
    
    # 4 - Verify the results
    print "---> Checking threads outputs ..."
    cluster.progress("4/5 Checking outputs")
    for chunk in chunkList:
        # Check the dump
        dump = os.path.splitext(chunk)[0] + ".dump"
        if not os.path.isfile("%s" % (dump)):
            cluster.progress("FAF-QEDError : Chunk %s did not produce output" % dump)

    # 5 - Generate the outputs
    print "---> Generating global outputs ..."
    cluster.progress("5/5 Generating outputs")
    cluster.runTasks("/usr/local/FAF-Drugs/bin/FAFQED.py", ["--output", options['userfile']], docker_img="fafqed")

    print'---> FAF-QED Process Finished.\n     Lagorce D. et al\n'


