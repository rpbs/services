#!/usr/bin/env python2.7

"""
author: Yasaman Karami
12 December 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import sys
import os
from PyPDB import PyPDB as PDB
from PyPDB.Geo3DUtils import *
from optparse import OptionParser
import cluster
import argparse
import Fasta.Fasta as Fasta
import multiprocessing as mp
import numpy as np

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PEP-Cyclizer','templates'))

DFLT_WORK_PATH     = "./"
DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"
DFLT_DEMO_PATH     = "/service/env/PEPCyclizer/demo/" #"/scratch/user/karami/pep-cyclizer/demo/"
DFLT_NPROC         = 8
DFLT_NTHREADS      = 8
DFLT_ROOT          = "/src"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PDB_CLS_PATH  = DFLT_BANK + "/PDB_clusters/"
DFLT_PDB_PATH      = DFLT_BANK + "/pdbChainID/"
DFLT_PROF_PATH     = DFLT_BANK + "/PyPPP_profiles/"
DFLT_HOME          = DFLT_ROOT + "/pep-cyclizer/"
DFLT_HOME_PPP      = "/usr/local/PyPPP3/"
DFLT_PDB_NAME      = "pdb70"  #"pdbChainID"
DFLT_BC_MIN        = 0.8
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.
DFLT_MODE          = "machine"  #else "cluster"
PROT_DIR           = "pep_cyclizer_test"
DFLT_PDB_ID        = "none"
MAX_JOB            = 300
DFLT_LBL           = "cyclic"
AAs = "ARNDCEQGHILKMFPSTWYV"
AA3 = ("ALA","ARG","ASN","ASP","CYS","GLU","GLN","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL")
### running on a local computer
Docker_image = "/home/ykarami/Documents/Docker/pep-cyclizer/src/pep-cyclizer/" 
#Docker_image = "docker run -u $(id -u):$(id -g) -v $(pwd):$(pwd):z -w $(pwd) -v %s:%s:z %s %s" %(bank_local, DFLT_BANK, "dev-registry-v2.rpbs.univ-paris-diderot.fr/pep-cyclizer", DFLT_HOME)
gromacs_path       = "/media/internal_backup/Cyclic/script/Gromacs/gromacs_lib/gromacs_lib"
#gromacs_path      = "/media/internal_backup/Cyclic/script/Gromacs/gromacs_py-master/gromacs_py/"
oscar_star_docker  = "dev-registry-v2.rpbs.univ-paris-diderot.fr/oscar-star"
############################################################################
############################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def gro_machine(gromacs_path, model, name):
	command = "%s/minimize_cyclic_pep.py -f %s -n %s -dir . -keep" %(gromacs_path, model, name)
	os.system(command)
	return

def formatViewer(models, residue_list, output_file, wdir):        
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('pepcyclizer.vis.html')
        sink.write(template.render(models = models, residue_list = residue_list, wdir = wdir))

def modify_in_pdb(model_dir, targetAddr, target):
    '''
    check input pdb and prepare it for cyclization:
    read the first model, first chain, ignore water molecules and 
    convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    take care of alternate coordinates
    '''
    input_PDB = PDB.PDB(targetAddr + target)
    ### check if the PDB file is valid
    if len(input_PDB) == 0:
        cluster.progress("Error! The input PDB file is not valid!")
        sys.exit(0)

    if len(input_PDB) < 8:
        cluster.progress("Error! The input PDB file is too small, the peptide must have a minimum size of 8 residues!")
        sys.exit(0)

    ### read the first model and the first chain
    nb_models = input_PDB.nModels()
    if nb_models > 1:
        cluster.progress("Only the first model from the input PDB is considered!")
        input_PDB.setModel(1)

    all_chains = input_PDB.chnList()
    if len(all_chains) > 1:
        cluster.progress("Only the first chain from the input PDB is considered!")
        GappedPDB = input_PDB.chn(all_chains[0])
    else:
        GappedPDB = input_PDB

    ### clean the input PDB and remove water molecules
    GappedPDB.clean()
    start_solve = 0
    end_solve = 0
    for ii in range(len(GappedPDB)):
        if GappedPDB[ii].rType() == 'SOLVENT':
           if start_solve == 0:
              start_solve = ii
           if end_solve == 0:
              end_solve = ii
           elif end_solve == ii-1:
              end_solve = ii
           else:
              cluster.progress("Error! Please provide an input PDB without solvent molecules.")
              sys.exit(0)
    if start_solve > 0 and end_solve > 0:
        if end_solve == (len(GappedPDB) - 1):
           GappedPDB = GappedPDB[0:start_solve]
        else:
           cluster.progress("Error! Please provide an input PDB without solvent molecules.")
           sys.exit(0)

    ### ignore amide and carboxyle groups at the terminals
    refe_seq = GappedPDB.aaseq()
    start_ind = 0; end_ind = len(GappedPDB)
    if refe_seq[0] == 'X':
        start_ind = 1
    if refe_seq[len(GappedPDB) - 1] == 'X':
        end_ind = len(GappedPDB) - 1
    mod_GappedPDB = GappedPDB[start_ind : end_ind]
    refe_seq = mod_GappedPDB.aaseq()
    
    ### check for modified amino aids
    if 'X' in refe_seq:
        cluster.progress("Error! Please only provide standard amino acids in your PDB file!")
        sys.exit(0)

    ### check the maximum and minimum peptide size
    if len(mod_GappedPDB) < 8:
        cluster.progress("Error! The input PDB file is too small, the peptide must have a minimum size of 8 residues!")
        sys.exit(0)
    if len(mod_GappedPDB) > 50:
        cluster.progress("Error! The input PDB file is too large, the peptide must have a maximum size of 50 residues!")
        sys.exit(0)

    ### check for insertion codes
    mask_list = []
    mask_list2 = []
    for ll in range(len(mod_GappedPDB)):
        if mod_GappedPDB[ll].riCode() != ' ':
           mask_list.append(ll)
           mask_list2.append(mod_GappedPDB[ll].rNum())

    if len(mask_list) > 0:
        cluster.progress("ignoring residues with insertion code: %s" %mask_list2)
        start = 0; i = 0
        while mask_list[i] == start:
            start += 1
            i += 1
        sub_pdb = mod_GappedPDB[start : mask_list[i]]
        while i < (len(mask_list)-1):
            i += 1
            if (mask_list[i-1]+1) == mask_list[i]: continue
            sub_pdb += mod_GappedPDB[(mask_list[i-1]+1) : mask_list[i]]
        if mask_list[i] < (len(mod_GappedPDB)-1):
            sub_pdb += mod_GappedPDB[(mask_list[i]+1) : len(mod_GappedPDB)]
    else:
        sub_pdb = mod_GappedPDB

    ### check for alternate atoms
    '''
    flag_alt = sum(sub_pdb.hasAltAtms())
    if flag_alt > 0:
        for rr in sub_pdb:
            if sum(rr.hasAltAtms()) > 0:
                del_list = []
                for aa in rr:
                    if rr.alt() not in (" ", "A"):
                        del_list.append(rr.atmName())
                    else:
                        rr.alt(" ")
                rr.delete(del_list) 
    '''
    sub_pdb.out(model_dir + "modified_open_peptide.pdb", altCare = 1, OXTCare = 1)
    sub_pdb = PDB.PDB(model_dir + "modified_open_peptide.pdb")
    nb_BB, list_BB = sub_pdb.BBatmMiss()
    if nb_BB > 0:
        cluster.progress("Error! Backbone atoms are missing in your input PDB: %s" %list_BB)
        sys.exit(0)
    return

def check_aa_constraints(targetAddr, aa_type_file, Loop_size):
    aa_pos = np.zeros(Loop_size, dtype='int32')
    if os.path.exists(targetAddr + aa_type_file):
        aa_type_cons = open(targetAddr + aa_type_file , 'r').readlines()
	if len(aa_type_cons) != Loop_size:
            cluster.progress("Error! There is no match between the linker size (%d) and amino acid constraints (%d)!" %(Loop_size, len(aa_type_cons)))
            sys.exit(0)
        for ll in range(len(aa_type_cons)):
            lines = (aa_type_cons[ll].split("\n")[0]).split(":")
            ## check if the line is empty or field with other characters
            if len(lines) <= 1 :
                cluster.progress("Format Error! The amino acid constraints at line %d are wrong!" %(ll+1))
                sys.exit(0)
            linker_pos = int(lines[0])
            if aa_pos[linker_pos - 1] > 0:
                cluster.progress("Format Error! The constraints for linker position (%d) are defiend more than once!"  %(linker_pos))
                sys.exit(0)
            aa_pos[linker_pos - 1] = 1
            if linker_pos <= 0 or linker_pos >10 :
                cluster.progress("Format Error! The linker position (%d), is not within the specified linker size (%d)!" %(linker_pos, Loop_size))
                sys.exit(0)
            for jj in range(1,len(lines)):
                if lines[jj] not in AAs:
                   cluster.progress("Error! Please provide standard amino acids for the constraints! \"%s\" is not valid." %lines[jj])
                   sys.exit(0)
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


if __name__=="__main__":

	parser = argparse.ArgumentParser()
    

	parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
						action="store", default = None)

	parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
						action="store", default=DFLT_PDB_ID)
    
	parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
						action="store", default=DFLT_WORK_PATH)
    
	parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
						action="store", default=DFLT_NPROC)
    
	parser.add_argument("--nt", dest="nthreads", type = int, help="number of threads (%d)" % DFLT_NTHREADS,
						action="store", default=DFLT_NTHREADS)
    
	parser.add_argument("--loop_sze", dest="loop_sze", type = int, help="loop size",
						action="store", default=None)
    
	parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters (%s)" % DFLT_PDB_CLS_PATH,
						action="store", default=DFLT_PDB_CLS_PATH)
    
	parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB (%s)" % DFLT_PDB_PATH,
						action="store", default=DFLT_PDB_PATH)
    
	parser.add_argument("--pdb_name", dest="pdb_name", help="name of indexed PDB (%s)" % DFLT_PDB_NAME,
						action="store", default=DFLT_PDB_NAME)
    
	parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum) (%f)" % DFLT_BC_MIN,
						action="store", default=DFLT_BC_MIN)

	parser.add_argument("--loop_seq", dest="loop_seq", help="fasta file with the sequence of the missing loop",
						action="store", default="none")
    
	parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
						action="store", default=DFLT_RIGIDITY_MAX)
    
	parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
						action="store", default=DFLT_FLANK_SZE)
    
	parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
						action="store", default=DFLT_RMSD_MAX)

	parser.add_argument("--aa_type", dest="aa_type", help="List of desired amino acids at each position",
						action="store", default=None)
    
	parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"machine\")(%s)" % DFLT_MODE,
						action="store", default=DFLT_MODE) 

	parser.add_argument("--bank_local_path", dest="bank_local_path", help="path to data banks (%s)" %DFLT_BANK,
						action="store", default=DFLT_BANK)

	parser.add_argument("--ppp_path", dest="ppp_path", help="path to ppp3 (%s)" % DFLT_HOME_PPP,
						action="store", default=DFLT_HOME_PPP) 

	parser.add_argument("--prof_path", dest="prof_path", help="path to profiles (%s)" % DFLT_PROF_PATH,
						action="store", default=DFLT_PROF_PATH)

	parser.add_argument("--demo", dest="demo", help="selecting the demo mode for either \"Modeling the linker sequence\" or \"Modeling the linker conformation\"",
						action="store", default=None)

        parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                                                action="store", default=DFLT_LBL)


	options = parser.parse_args() # Do not pass sys.argv
	# print options.__dict__.keys()
	if options.work_path[-1] != "/":
		options.work_path = options.work_path + "/"
	if options.pdb_cls_path[-1] != "/":
		options.pdb_cls_path = options.pdb_cls_path + "/"
	if options.pdb_path[-1] != "/":
		options.pdb_path = options.pdb_path + "/"
	print print_options(options)

	
	jobId = os.path.basename(os.getcwd())
	serviceName = os.path.split(os.path.split(os.getcwd())[0])[1] #"PEP-Cyclizer"
	wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)
	

	# input
	targetAddr   = options.work_path
	target       = options.target
	pdbID        = options.pdb_id
        out_label    = options.Lbl
	NBPROCESS    = options.nproc
	nbThreads    = str(options.nthreads)
	clust_addr   = options.pdb_cls_path
	pdbdir       = options.pdb_path
	pdbName      = options.pdb_name
	BCth         = str(options.bc_min)
	RigidityTh   = str(options.rigidity)
	FLANKSIZE    = options.flank_sze
	loop_size    = options.loop_sze
	RMSDth       = str(options.rmsd_max)
	mode         = options.run_mod
	aa_type_file = options.aa_type
	LoopSeqFile  = options.loop_seq
	bank_local   = options.bank_local_path
	PyPPP3_dir   = options.ppp_path 
	Prof_dir     = options.prof_path
	select_demo  = options.demo

	targetAddr = os.getcwd() + "/" 
	model_dir = mk_dir(targetAddr + PROT_DIR)

	### DEMO mode
	if select_demo is not None:
		if select_demo == "Model_Sequence":
			target = "1mxn_m1.pdb"
			aa_type_file = "aa_cons.txt"
			loop_size = 6
			os.system("cp " + DFLT_DEMO_PATH + target + " " + targetAddr)
			os.system("cp " + DFLT_DEMO_PATH + aa_type_file + " " + targetAddr)

		elif select_demo == "Model_Structure":
			target = "1m2c_m1.pdb"
			LoopSeqFile = "1m2c_gap6.fasta"
			loop_size = 6
			os.system("cp " + DFLT_DEMO_PATH + target + " " + targetAddr)
			os.system("cp " + DFLT_DEMO_PATH + LoopSeqFile + " " + targetAddr)
		else:
			sys.stderr.write("Error! Wrong demo mode is selected!\n")

	### input linker sequence
	LoopSeqFileAddr = targetAddr + LoopSeqFile
	if os.path.isfile(LoopSeqFileAddr) and os.path.getsize(LoopSeqFileAddr) > 0 :
		seq = Fasta.fasta(LoopSeqFileAddr)
		LoopSeq = seq[seq.ids()[0]].data['s']
		loop_size = len(LoopSeq)
		for ii in range(loop_size):
			if LoopSeq[ii] not in AAs:
				cluster.progress("Error! Please provide standard amino acids for the linker sequence! %s is not valid." %LoopSeq[ii])
				sys.exit(0)
		RMSDth = "2.5"
		pdbName = "pdbChainID"
                nb_tot = 8
		nameing_pre = out_label + "_structure_guess_gap" + str(loop_size)
		name_vis = "structure_guess"
	else:
		if loop_size is None:
			cluster.progress("Error! Please select the Linker size")
			sys.exit(0)
		LoopSeq = None
		RMSDth = "3"
		pdbName = "pdb70"
                nb_tot = 9
		nameing_pre = out_label + "_sequence_guess_gap" + str(loop_size)
		name_vis = "sequence_guess"

	if select_demo is None and LoopSeq is None and aa_type_file is None:
		cluster.progress("Error! Please provide either the Linker sequence or amino acid constraints!")
		sys.exit(0)

        if aa_type_file is not None:
		check_aa_constraints(targetAddr, aa_type_file, loop_size)

	if mode == "cluster":
		######################################################################
		'''
		RUNNING ON THE CLUSTER USING DOCKER IMAGE
		'''
		######################################################################
		##### Prep Inputs and Remove Homologs (if needed) ####################
		cluster.progress("1/%d: Verifying input files" %nb_tot)
		### check the input pdb
		modify_in_pdb(model_dir, targetAddr, target)
		### prepare all the files
		cmd = DFLT_HOME + "Prep_input.py"
		args = ['--wpath', targetAddr,
			'--target', target,
			'--pdb_id', pdbID,
			'--cls_path', clust_addr,
			'--run_mod', mode,
			'--loop_sze', str(loop_size)]
		if LoopSeq is not None:
			args += ['--loop_seq', LoopSeq]
		cluster.runTasks(cmd, args, docker_img = "pep-cyclizer")
		##### BCLoopSearch ###################################################
		cluster.progress("2/%d: BCLoopSearch and clustering" %nb_tot)
		cmd = DFLT_HOME + "LoopSearch.py"
		args = ['--wpath', targetAddr,
			'--target', target,
			'--pdb_path', pdbdir,
			'--pdb_name', pdbName,
			'--bc_min' , BCth,
			'--rigidity', RigidityTh,
			'--rmsd_max', RMSDth,
			'--run_mod', mode,
			'--loop_sze', str(loop_size)]
		if LoopSeq is not None:
			args += ['--loop_seq', LoopSeq]
		### checking amino acid constraints
		if aa_type_file is not None:
			args += ['--aa_type', aa_type_file]
		cluster.runTasks(cmd, args, docker_img = "pep-cyclizer")
		### check if there was any hit found or not
		BCLoopRes = model_dir + "Gap" + str(loop_size) + "/BCSearch/BCLoopCluster_Final_BC.txt"
		if not os.path.isfile(BCLoopRes) or os.path.getsize(BCLoopRes) <= 0 :
			cluster.progress("Error! No candidate was found for your input file!")
			sys.exit(0)
		else:
			BCLoopResFile = open(BCLoopRes,'r').readlines()
			cluster.progress("Found %d hits (size %d)\n" %(len(BCLoopResFile),loop_size))
		##### PyPPP Prep ####################################################
		#verifying the existance of conformational profile for the target
		#Otherwise we call PyPPP to measure the profile
		# srun drun ppp3 PyPPP3Exec -s 2bbp6.fst -l 2bbp6 -v --2012
		cluster.progress("3/%d: Conformational profiles" %nb_tot)
		PPP_name = "target_seq"
		fst_name = PPP_name + ".fst"
		os.chdir(model_dir)
		cmd = "PyPPP3Exec"
		args = ['-s', fst_name,
			'-l', PPP_name,
			'-v',
			'--2012']
		cluster.runTasks(cmd, args, docker_img = "ppp3")
		os.chdir(targetAddr)
		##### Measuring JSD ###################################################		
		cluster.progress("4/%d: Measuring Jenson Shannon distances" %nb_tot)
		cmd = DFLT_HOME + "PyPPP_score.py"
		args = ['--ppp_path', Prof_dir,
			'--wpath', targetAddr,
			'--loop_sze', str(loop_size),
			'--target', target,
			'--flank_sze', str(FLANKSIZE),
			'--run_mod', mode]
		if LoopSeq is not None:
			args += ['--loop_seq', LoopSeq]
		out_dir = model_dir + "Gap" + str(loop_size) + "/BCSearch/"
		BCLoopRes = open(out_dir + "BCLoopCluster_Final_BC.txt", "r").readlines()
		nbCandid = min(len(BCLoopRes),MAX_JOB)
		cluster.runTasks(cmd, args, tasks = nbCandid, tasks_from = 1, docker_img = "pep-cyclizer")
		##### Candid Prep ####################################################
		cluster.progress("5/%d: Preparing the candidates" %nb_tot)
		cmd = DFLT_HOME + "Prep_candidates.py"
		args = ['--wpath', targetAddr,
			'--target', target,
			'--loop_sze', str(loop_size)]
		if LoopSeq is not None:
			args += ['--loop_seq', LoopSeq]
		cluster.runTasks(cmd, args, docker_img = "pep-cyclizer")
		### check if there was any hit found or not
		TopModels = model_dir + "Gap" + str(loop_size) + "/BCSearch/top_models.list"
		if not os.path.isfile(TopModels) or os.path.getsize(TopModels) <= 0 :
			cluster.progress("Error! No candidate was found for your input file!")
			sys.exit(0)
		##### Oscar-Star #####################################################
		cluster.progress("6/%d: positioning linker side chains" %nb_tot)
		cmd = "oscarstar"
		out_dir = model_dir + "Gap" + str(loop_size) + "/BCSearch/Candidates/"
		pdb_data = out_dir + "data"
		pdb_read = open(pdb_data, 'r').readlines()
		nbCandid = len(pdb_read)
		if nbCandid <= 1:
			cluster.progress("Error! No candidate was found!")
			sys.exit(0)
		os.chdir(out_dir)
		args = ['$(awk NR==$TASK_ID ' + pdb_data + ')', ]
		cluster.runTasks(cmd, args, tasks = nbCandid, tasks_from = 1, docker_img = "oscar")
		#### gromacs ########################################################
		os.chdir(targetAddr)
		cluster.progress("7/%d: minimization" %nb_tot)
		cmd = "minimize_pdb_and_cyclic.py"
		out_dir = model_dir + "Gap" + str(loop_size) + "/BCSearch/"
		gromacs_dir = out_dir + 'Candidates/'
		input_list = out_dir + "input_gromacs.list"
		name_list = out_dir + "gromacs_names.list"
		nbCandid = len(open(name_list, 'r').readlines())
		if nbCandid <= 1:
			candidate("No candidate was found!")
			sys.exit(0)
		os.chdir(gromacs_dir)
		args = ['-f', '$(awk NR==$TASK_ID ' + input_list + ')',
			'-n', '$(awk NR==$TASK_ID ' + name_list + ')',
			'-dir' , '.',
			'-keep', ' ',
			'-cyclic',]
		cluster.runTasks(cmd, args, tasks=nbCandid, tasks_from=1, docker_img="gromacs_py_bionic")
		##### Scoring #######################################################
		os.chdir(targetAddr)
		cluster.progress("8/%d: Scoring the candidates" %nb_tot)
		cmd = DFLT_HOME + "scoring.py"
		args = ['--wpath', targetAddr,
			'--target', target,
			'--l', out_label,
			'--loop_sze', str(loop_size)]
		if LoopSeq is not None:
			args += ['--loop_seq', LoopSeq]
		cluster.runTasks(cmd, args, docker_img = "pep-cyclizer")
		summary_file = targetAddr + nameing_pre + "_summary.txt"
		summary_file_lines = open(summary_file,'r').readlines()
		if len(summary_file_lines) <= 1:
			cluster.progress("Error! Failed to run minimization, please verify your input PDB")
			sys.exit(0)
		##### Logo ##########################################################
		if LoopSeq is None:
			cluster.progress("9/%d: Generating the logo" %nb_tot)
		cmd = DFLT_HOME + "create_logo.py"
		args = ['--wpath', targetAddr,
			'--l', out_label,
			'--loop_sze', str(loop_size)]
		if aa_type_file != None:
			args += ['--aa_type', aa_type_file]
		if LoopSeq is not None:
                        args += ['--loop_seq', LoopSeq]
		cluster.runTasks(cmd, args, docker_img = "pep-cyclizer")
		##### visulaization ##########################################################
		os.system("tar -czvf " + nameing_pre + "_models.tgz " + nameing_pre + "_model*.pdb")

		input_pdb = PDB.PDB(targetAddr + target)
		input_size = len(input_pdb)
		residue_list = [ (i + input_size + 1) for i in range(loop_size)]
		if os.path.isfile(summary_file):
			model_names = [i.split()[0] for i in open(summary_file) if not i.startswith("#")]
			formatViewer(model_names, residue_list, 'pepcyclizer_' + name_vis + '.vis.html', wdir)
		
		cluster.progress("Finished")
		


	else:
		######################################################################
		'''
		RUNNING ON A LOCAL COMPUTER AND NOT USING THE DOCKER IMAGE
		'''
		######################################################################		
		command = "%s/Prep_input.py --wpath %s --target %s --run_mod %s --loop_sze %d --pdb_id %s --cls_path %s" % (Docker_image, targetAddr, target, mode, loop_size, pdbID, clust_addr)   
		if LoopSeq is not None:
			command += " --loop_seq %s" %(LoopSeq)
		print command
		os.system(command)

		command = "%s/LoopSearch.py --wpath %s --target %s --nt %s --run_mod %s --loop_sze %d --pdb_path %s --pdb_name %s --bc_min %s --rigidity %s --rmsd_max %s" %(Docker_image, targetAddr, target, nbThreads, mode, loop_size, pdbdir, pdbName, BCth, RigidityTh, RMSDth)
		if LoopSeq is not None:
			command += " --loop_seq %s" %(LoopSeq)
		if aa_type_file != None:
			command += " --aa_type %s " %(aa_type_file)
		print command
		os.system(command)
		
		command = "%s/PyPPP_prep.py --wpath %s --target %s --pyppp_path %s" %(Docker_image, targetAddr, target, PyPPP3_dir)
		print command
		os.system(command)
		os.system("chmod +x " + targetAddr + "runPyPPP.sh")
		os.system(targetAddr + "runPyPPP.sh")
		os.system("rm " + targetAddr + "runPyPPP.sh")
		
		command = "%s/PyPPP_score.py --wpath %s --target %s --loop_sze %d --flank_sze %d --run_mod %s --np %d" %(Docker_image, targetAddr, target, loop_size, FLANKSIZE, mode, NBPROCESS)
		if LoopSeq is not None:
			command += " --loop_seq %s" %(LoopSeq)
		print command
		os.system(command)
	
	
		command = "%s/Prep_candidates.py --wpath %s --target %s --loop_sze %d" %(Docker_image, targetAddr, target, loop_size)
		print command
		os.system(command)

		Loop_dir = model_dir + "Gap" + str(loop_size) + "/BCSearch/" 
		BC_final_result = Loop_dir + "BCLoopCluster_Final_BC.txt"
		if os.path.exists(BC_final_result):
			out_dir =  Loop_dir + "Candidates/"
			if os.path.exists(out_dir):
				os.chdir(out_dir)
				command = "docker run -v %s:%s:z -w %s %s oscarstar" %(out_dir, out_dir, out_dir, oscar_star_docker)
				os.system(command)
		
		#### gromacs minimization
		out_dir = model_dir + "Gap" + str(loop_size) + "/BCSearch/"
		BC_final_result = out_dir + "BCLoopCluster_Final_BC.txt"
		if os.path.exists(BC_final_result):        
			gromacs_dir = out_dir + 'Candidates/'
			if os.path.exists(gromacs_dir):
				input_list_read = open(out_dir + "input_gromacs.list", "r").readlines()
				name_list_read = open(out_dir + "gromacs_names.list", "r").readlines()
				nbCandid = len(input_list_read)
				os.chdir(gromacs_dir)
				pool = mp.Pool()
				for i in range(nbCandid):
					model = input_list_read[i].split("\n")[0]
					name = name_list_read[i].split("\n")[0]
					pool.apply_async(gro_machine, args=(gromacs_path, model, name,))
				pool.close()
				pool.join()
		
		os.chdir(targetAddr) 
		command = "%s/scoring.py --wpath %s --target %s --run_mod %s --loop_sze %d --l %s" %(Docker_image, targetAddr, target, mode, loop_size, out_label)
		if LoopSeq is not None:
			command += " --loop_seq %s" %(LoopSeq)
		print command
		os.system(command)
		
		command = "%s/create_logo.py --wpath %s --loop_sze %d --l %s" %(Docker_image, targetAddr, loop_size, out_label)
                if LoopSeq is not None:
                        command += " --loop_seq %s" %(LoopSeq)
                if aa_type_file != None:
                        command += " --aa_type %s " %(aa_type_file)
		print command
		os.system(command)	
		
	'''	
	gap_dir = model_dir + "Gap" + str(loop_size)
	os.system("rm -rf " + gap_dir + "\n")
	os.system("rm -rf " + model_dir + "\n")
	'''

	
