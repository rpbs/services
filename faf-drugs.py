#!/usr/bin/env python

# FafDrugs wrapper to run on RPBS cluster
#

# Imports
import os, sys, cPickle
import cluster.cluster as cluster

# Constants
dico_dump = "multi_params_dico.dump"
# Functions



if __name__ == "__main__":
    
    print '\n---> FAF-Drugs4 (April 2017) running ...'
    # 0 - Check the arguments
    argumentsToPass = ["--prepare"] + sys.argv[1:]
    #print(argumentsToPass)

    # 1 - Call FAF to prepare the bank
    print "---> Data curation step ..."
    cluster.progress("1/5 Preparing databank")
    cluster.runTasks("FAFDrugs.py -D ", argumentsToPass, environment_module = "faf-drugs/4.1-rpbs", log_prefix = "curation")
   
    print "---> Preparing multithreads jobs ..."
 
    # 2 - Load parameters
    print "---> Loading parameters and options ..."
    cluster.progress("2/5 Loading parameters")
    options = cPickle.load(open(dico_dump))
    chunkList = options["list_of_chunks"]
    with open("list_of_dico_dump.dump","w") as dump_file:
        cPickle.dump([os.path.splitext(chunk)[0] + ".dump" for chunk in chunkList] , dump_file)

    ## exemple ["chunk1", "chunk2", "chunk3"]
    
    # 3 - Run FAF Loop
    print "---> Filtering step ..."
    cluster.progress("3/5 Running FAF-Drugs")
    cluster.runTasks("FAFDrugs.py -D ", ["--fafonly", "--chunkfile map_item", "fake.sdf"], map_list = chunkList, environment_module = "faf-drugs/4.1-rpbs", log_prefix = "filtering")
    
    ### Ici le passage de map_list va lancer un pickle
    
    # 4 - Verify the results
    print "---> Checking threads outputs ..."
    cluster.progress("4/5 Checking outputs")
    for chunk in chunkList:
        # Check the dump
        dump = os.path.splitext(chunk)[0] + ".dump"
        if not os.path.isfile("%s" % (dump)):
            cluster.progress("FAFError : Chunk %s did not produce output" % dump)

    # 5 - Generate the outputs
    print "---> Generating global outputs ..."
    cluster.progress("5/5 Generating outputs")
    cluster.runTasks("FAFDrugs.py -D ", ["--output", options['userfile']], environment_module="faf-drugs/4.1-rpbs", log_prefix = "outputs")

    print'---> FAF-Drugs4 Process Finished.\n     Lagorce D. et al\n'


