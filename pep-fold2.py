#!/usr/bin/env python

"""
PEP-FOLD2 main script
Will organise a script depending on options and launch series of runs, and MQA

Dependencies:
"""

import sys
import os
from optparse import OptionParser
import fnmatch
import shutil
import time
import glob
import random
import cluster.cluster as cluster

# Configuration 
from PEPFOLD2.Config import *

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PEPFOLD2','templates'))

# Dependencies
from PEPFOLD2.PyOptions import *
import PyPDB.PyPDB as PDB
import Fasta.Fasta as Fasta

import signal


class MINI_FOLD(Options):

    def __init__(self, args = ["PEPFOLD2"], options = None, version=None, verbose = False):
        """
        @param options : a dictionary of the options among which 
                         some are valid.
        """
        if version == None:
            version = "PEPFOLD2"+ " v%s"%VERSION
        if not hasattr(self,"clp"):
            Options.__init__(self, args = args, options = options, version=version, verbose = verbose)
        self.__cmdLine_options()
        # i_options = {"probFile" : "MINI_FOLD.svmi8.27.prob",
        #              "trj"      : "MINI_FOLD.trj",
        #              "zipMask"  : "MINI_FOLD.gmask",
        #              } 
        # self.add_options(i_options)

    def start(self, parse = True, check = True, verbose = False):
        """
        Effective options parsing
        on return, MINI_FOLD ready to run.
        """
        # This for demo mode
        if parse:
            self.parse()
        if verbose:
            self.options.verbose = verbose
        if check:
            self.cmd_line_status = self.__check_options(verbose = True)
            if verbose:
                sys.stderr.write("PEPFOLD2 Initial start: %s\n" % self.cmd_line_status)
        return

    def __repr__(self):
        """
        @return: a string describing the options setup for the Blast instance
        """

        try:
            rs = self.version
            for option in self.options.__dict__.keys():
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""

    def __cmdLine_options(self, version = None):
        """
        Command line definition.
        These options are extra options over inherited PPP options.
        @return: command line options and arguments (optionParser object)
        """

        self.clp.set_usage("usage: %prog [options] <sequence file> ")

    # OK from here
        self.clp.add_option("-n", "--nRuns", action="store", dest="nRuns", type="int", help="number of greedy runs", default = 100)
        self.clp.add_option("--runsFrom", action="store", dest="runsFrom", type="int", help="number of first run", default = 1)
        self.clp.add_option("--maxSimSze", action="store", dest="maxSimSze", type="int", help="maximal size to simulate (%s)" % str(DFLT_MAX_SIM_SIZE), default = DFLT_MAX_SIM_SIZE)
        self.clp.add_option("--maxSeqSze", action="store", dest="maxSeqSze", type="int", help="maximal input sequence size  (%s)" % str(DFLT_MAX_SEQ_SIZE), default = DFLT_MAX_SEQ_SIZE)
        self.clp.add_option("--script", action="store_true", dest="script", help="do not run, but generate a script to run", default = False)
        self.clp.add_option("--ppponly", action="store_true", dest="PPPOnly", help="only run PPP", default = False)
        self.clp.add_option("--combined", action="store_true", dest="combined", help="generate multiple trajectories", default = False)
        self.clp.add_option("--shortcombined", action="store_true", dest="shortcombined", help="generate multiple trajectories", default = False)
        self.clp.add_option("--asPEPFOLD1", action="store_true", dest="pepfold1", help="run as pepfold1, i.e. constant bias", default = False)
        self.clp.add_option("--medium", action="store_true", dest="medium", help="generate multiple trajectories but use only sure bias", default = False)
        self.clp.add_option("--greedyonly", action="store_true", dest="greedyOnly", help="only run greedy", default = False)
        self.clp.add_option("--clusteringonly", action="store_true", dest="clusteringOnly", help="only run clustering", default = False)
        self.clp.add_option("--posttreatmentonly", action="store_true", dest="postTreatmentOnly", help="only run posttreatment", default = False)
        self.clp.add_option("--parallelposttreatment", action="store_true", dest="sgePostTreat", help="run posttreatment using SGE array (faster)", default = False)
        self.clp.add_option("--autoFromTo", action="store_true", dest="autoFromTo", help="Identify rigid core, perform 2 pass greedy on core, then on complete structure", default = False)
        self.clp.add_option("--justEnrichClusters", action="store_false", dest="runClusters", help="do not perform Apollo, just enrich clusters.txt", default = True)
        self.clp.add_option("--refZones", action="store", dest="refZones", type="string", help="Zones to define correspondance between ref and models", default = None)
        self.clp.add_option("--fromTo", action="store", dest="simFromTo", type="string", help="Simulate between from and to residues (included, from 1)", default = None)
        self.clp.add_option("--uTrj", action="store", dest="userTrj", type="string", help="user specified trajectory", default = None)
        self.clp.add_option("--cs", action="store_true", dest="CS", help="chemical shifts calculation for models generated", default = False)
        self.clp.add_option("--csFile", action="store", dest="CSFile", type="string", help="experimental chemical shifts", default = None)
        self.clp.add_option("--sortKey", action="store", dest="sortKey", type="string", help="key to sort clusters, one of %s" % " ".join(DFLT_KEYS), default = DFLT_SORT_KEY)
        self.clp.add_option("--progressFile", action="store", dest="progressFile", type="string", help="Log file to put information about PEP-FOLD progress (%s)"%PEPFOLD_PROGRESS_FILE, default = PEPFOLD_PROGRESS_FILE)
        self.clp.add_option("--demo", action="store_true", dest="demo", help="supersedes parameters with demo ones", default = None)
        self.clp.add_option("--SVM2012", action="store_true", dest="SVM2012", help="PPP using the 2012 SVM", default = False)
        self.clp.add_option("--noPPP", action="store_false", dest="doPPP", help="do not run PPP", default = True)
        self.clp.add_option("--no3D", action="store_false", dest="do3D", help="do not run 3D generation", default = True)
        self.clp.add_option("--noPostTT", action="store_false", dest="doPostTT", help="do not run post-treatment", default = True)
        self.clp.add_option("--noClustering", action="store_false", dest="doClusters", help="do not run clustering", default = True)

    # PyPPP parameters

        self.clp.add_option("-l", "--label", action="store", dest="label", type="string", help="job label", default = DFLT_LABEL)
        self.clp.add_option("-s", "--iSeq", action="store", dest="seqFile", type="string", help="amino acid sequence (fasta file)", default = None)

    # Greedy parameters

        self.clp.add_option("--SSBonds", action="store", dest="SSBonds", type="string", help="Disulfide bond list (e.g.: x-y:k-l) from 1", default = None)
        self.clp.add_option("--Neighbours", action="store", dest="neighbours", type="string", help="Neighbour list (e.g.: x-y:k-l) from 1", default = None)
        self.clp.add_option("--from", action="store", dest="ffrom",   type="int", help="build from sequence position (0)", default = 1)
        self.clp.add_option("--to", action="store", dest="tto", type="int", help="build to sequence position (-1 == last)", default = -1)

    # DBClusters parameters

        self.clp.add_option("--threshold", action="store", dest="threshold", type="float", help="threshold ofr clustering (angstroms for RMSd)", default = DFLT_CLUSTERING_THRESHOLD)
        self.clp.add_option("--score", action="store", dest="score", type="string", help="Clustering score (one of %s)" % " ".join(PWSCORES.keys()), default = DFLT_PWSCORE)
        self.clp.add_option("--ref", action="store", dest="reference", type="string", help="Reference File", default = None)
        self.clp.add_option("--ncores", action="store", dest="np", type="int", help="# cores to use for multiprocessing", default = DFLT_NPROC)

    def __check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy

        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """

        # Check input data exists
        if (not self.options.seqFile) and (not self.options.pdb):
            if verbose:
                sys.stderr.write("PEP-FOLD 2: must specify either sequence or PDB file. --help for option detail.\n")
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: invalid input sequence or PDB file.")
            return False

        self.options.runPPP           = True
        self.options.runGreedy        = True
        self.options.runClustering    = True
        self.options.runPostTreatment = True
        self.options.CSTabFile        = None
        if self.options.PPPOnly: 
             self.options.runPPP           = True
             self.options.runGreedy        = False
             self.options.runClustering    = False
             self.options.runPostTreatment = False
        if self.options.greedyOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = True
             self.options.runClustering    = False
             self.options.runPostTreatment = False
        if self.options.clusteringOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = False
             self.options.runClustering    = True
             self.options.runPostTreatment = False
        if self.options.postTreatmentOnly: 
             self.options.runPPP           = False
             self.options.runGreedy        = False
             self.options.runClustering    = False
             self.options.runPostTreatment = True

        if self.options.seqFile:
            try:
                x = Fasta.fasta(self.options.seqFile)
                self.options.aaSeq = x[x.ids()[0]].s()
            except:
                if verbose:
                    sys.stderr.write("PyPPP: Failed to load sequence (tried %s).\n" % self.options.seqFile)
                return False

        if not self.options.aaSeq:
            sys.stderr.write("PEPFOLD 2:  Failed to read sequence to process.\nPlease check your input sequence.")
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: invalid sequence.")
            return False
        # Check seq length is OK.
        if len(self.options.aaSeq) < DFLT_MIN_SEQ_SIZE:
            sys.stderr.write("PEP-FOLD 2: Too short sequence (min is %d)\n" % DFLT_MIN_SEQ_SIZE)
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: too short sequence (%d / %d)." % (len(self.options.aaSeq), DFLT_MIN_SEQ_SIZE))
            return False
        # if len(self.options.aaSeq) > DFLT_MAX_SEQ_SIZE:
        #     sys.stderr.write("PEP-FOLD 2: Too long sequence (max is %d)\n" % DFLT_MAX_SEQ_SIZE)
        if len(self.options.aaSeq) > self.options.maxSeqSze:
            sys.stderr.write("PEP-FOLD 2: Too long sequence (max is %d)\n" % self.options.maxSeqSze)
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: too long sequence (%d / %d)." % (len(self.options.aaSeq), self.options.maxSeqSze))
            return False

        # Check sim length is OK.
        if self.options.simFromTo != None:
            try:
                it = self.options.simFromTo.split("-")
                ffrom = int(it[0])
                tto = int(it[1])
                self.options.ffrom = ffrom
                self.options.tto = tto
            except:
                sys.stderr.write("PEP-FOLD 2: could not parse simulation from - to range (%s).\n" % (len(self.options.simFromTo)))
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 2: failed to parse the 3D reconstruction limits. Please check.")
                return False

        if self.options.ffrom < 0:
            sys.stderr.write("PEP-FOLD 2: Incorrect start position for simulation (%d). (must be more or equal than 1)\n" % (self.options.ffrom + 1))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: incorrect limit for simulation start (must be >= 1) (%d)." % (self.options.ffrom + 1))
            return False

        if self.options.tto == -1:
            self.options.tto = len(self.options.aaSeq)
            simLen =  len(self.options.aaSeq)
        else:
            simLen =  self.options.tto
        # sys.stderr.write("Will simulate from %d to %d\n" % (self.options.ffrom, self.options.tto))
        if simLen > len(self.options.aaSeq):
            sys.stderr.write("PEP-FOLD 2: cannot simulate over sequence length (aske up to %d when sequence size is %d\n" % (simLen, len(self.options.aaSeq)))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: invalid limit of sequence to simulate (%d / %d)." % (simLen, len(self.options.aaSeq)))
            return False
        simLen = simLen - self.options.ffrom + 1
        # sys.stderr.write("Will simulate for simLen %d (from %d to %d)\n" % (simLen, self.options.ffrom, self.options.tto))
        # sys.exit(0)
        # if simLen > DFLT_MAX_SIM_SIZE:
        #     sys.stderr.write("PEP-FOLD 2: Maximum accepted length for simulation is %d. Asked for %d.\n" % (DFLT_MAX_SIM_SIZE, simLen))
        if simLen > self.options.maxSimSze:
            sys.stderr.write("PEP-FOLD 2: Maximum accepted length for simulation is %d. Asked for %d.\n" % (self.options.maxSimSze, simLen))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: too long sequence to simulate (%d / %d)." % (simLen, self.options.maxSimSze))
            return False

        if simLen < DFLT_MIN_SEQ_SIZE:
            sys.stderr.write("PEP-FOLD 2: Maximum accepted length for simulation is %d. Asked for %d.\n" % (DFLT_MIN_SEQ_SIZE, simLen))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: too short sequence to simulate (%d / %d)." % (simLen, DFLT_MIN_SEQ_SIZE))
            return False

        if self.options.tto > len(self.options.aaSeq):
            sys.stderr.write("PEP-FOLD 2: Incorrect stop position for simulation (%d). (cannot be longer than sequence specified of %d)\n" % (self.options.tto, len(self.options.aaSeq)))
            if REMOTE_SERVER:
                cluster.progress("PEP-FOLD 2: incorrect limit for simulation upper bound compared to sequence length (%d / %d)." % (self.options.tto, len(self.options.aaSeq)))
            return False

        self.options.aaSeq = self.options.aaSeq.upper()
        for aa in self.options.aaSeq:
            if aa not in "ACDEFGHIKLMNPQRSTVWY":
                sys.stderr.write("PEP-FOLD 2: unkown amino acid %s\n" % aa)
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 2: unkown amino acid  %s." % (aa))
                return False

        # Check PDB reference is a PDB and is of correct length
        if self.options.reference is not None:
            try:
                x = PDB.PDB(self.options.reference)
                sys.stderr.write("Reference size is %d\n" % len(x))
            except:
                sys.stderr.write("Sorry: reference file does not seem a valid PDB. \n")
                self.options.reference = None
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 2: reference file does not seem a valid PDB.")
                return False
            if not len(x):
                sys.stderr.write("Sorry: reference file does not seem a valid PDB. \n")
                self.options.reference = None
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 2: reference file does not seem a valid PDB.")
                return False
            if self.options.reference is not None:
                refaaseq = x.aaseq()
                if len(refaaseq) != (self.options.tto - self.options.ffrom +1):
                    sys.stderr.write("Sorry: reference file length is not compatible with input sequence size. (%d vs %d (%d - %d ))\n" % (len(refaaseq), (self.options.tto - self.options.ffrom +1), self.options.ffrom, self.options.tto ))
                    self.options.reference = None
                    if REMOTE_SERVER:
                        cluster.progress("PEP-FOLD 2: reference file size is not compatible with input sequence (%d / %d). Must be identical." % (len(refaaseq), (self.options.tto - self.options.ffrom +1)))
                    return False

        # Check SS Bonds correspond to CYS residues
        if self.options.SSBonds is not None:
           # This if for the PEPFOLD server
           self.options.pepfold1 = True
           self.options.nRuns    = 100 # short combined
           if self.options.combined:
               self.options.nRuns = 200
           self.options.SSA = 10.
           # Now we check
           sslist =  self.options.SSBonds.split(":")
           for ss in sslist:
               aas = ss.split("-")
               if len(aas) != 2:
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Incorrect syntax: %s.\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Incorrect syntax: %s." % ss)
                   return False
               if int(aas[0]) > int(aas[1]):
                   aas[0],aas[1] = aas[1], aas[0]
               if aas[0] == aas[1]:
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Cannot make intra residue SS bond (%s).\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Cannot make intra residue SS bond (%s)." % ss)
                   return False
               if int(aas[0]) < self.options.ffrom:
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Residue %s not within simulation range.\n" % aas[0])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Residue %s not is simulation range" % aas[0])
                   return False
               if int(aas[1]) > self.options.tto:
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Residue %s not within simulation range.\n" % aas[1])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Residue %s not is simulation range" % aas[1])
                   return False
               if self.options.aaSeq[int(aas[0])-1] != "C":
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Is residue %s a cysteine ?\n" % aas[0])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Is residue %s a cysteine ?" % aas[0])
                   return False
               if self.options.aaSeq[int(aas[1])-1] != "C":
                   sys.stderr.write("PEP-FOLD 2: incorrect SS bond specification. Is residue %s a cysteine ?\n" % aas[1])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect SS bond specification. Is residue %s a cysteine ?" % aas[1])
                   return False

        # Check neighbor specifications are OK.
        if self.options.neighbours is not None:
           # This if for the PEPFOLD server
           # self.options.pepfold1 = True         
           # self.options.nRuns    = 100 # short combined
           # if self.options.combined:
           #     self.options.nRuns = 200
           self.options.NBA = 10.
           # Now we check
           sslist =  self.options.neighbours.split(":")
           for ss in sslist:
               aas = ss.split("-")
               if len(aas) != 2:
                   sys.stderr.write("PEP-FOLD 2: incorrect neighbor constraint specification. Incorrect syntax: %s.\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect neighbor constraint specification. Incorrect syntax: %s." % ss)
                   return False
               if int(aas[0]) > int(aas[1]):
                   aas[0],aas[1] = aas[1],aas[0]
               if aas[0] == aas[1]:
                   sys.stderr.write("PEP-FOLD 2: incorrect neighbor constraint specification. Cannot make intra residue neighbor (%s).\n" % ss)
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect neighbor constraint specification. Cannot make intra neighbor (%s)." % ss)
                   return False
               if int(aas[0]) < self.options.ffrom:
                   sys.stderr.write("PEP-FOLD 2: incorrect neighbor constraint specification. Residue %s not within simulation range.\n" % aas[0])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect neighbor constraint specification. Residue %s not is simulation range" % aas[0])
                   return False
               if int(aas[1]) > self.options.tto:
                   sys.stderr.write("PEP-FOLD 2: incorrect neighbor constraint specification. Residue %s not within simulation range.\n" % aas[1])
                   if REMOTE_SERVER:
                       cluster.progress("PEP-FOLD 2: incorrect neighbor constraint specification. Residue %s not is simulation range" % aas[1])
                   return False

        # Check chemical shifts specifications are OK.
        if self.options.CSFile:
            import PPMTools
            try:
                x = PPMTools.sxp_input(self.options.CSFile)
                if not len(x[0]):
                    sys.stderr.write("PEP-FOLD 2: Could not read any chemical shift data. Is format correct ?\n")
                    if REMOTE_SERVER:
                        cluster.progress("PEP-FOLD 2: could not read any chemical shift data. Is format correct ?")
                    return False
                self.options.CSTabFile = "%s.tab" % self.options.CSFile
                rs = PPMTools.talosoutput(x, where=self.options.CSTabFile)
            except:
                sys.stderr.write("PEP-FOLD 2: Chemical Shift file input failed\n")
                if REMOTE_SERVER:
                    cluster.progress("PEP-FOLD 2: failed to read Chemical Shifts.")
                return False
            self.options.CS = True

        # Remove banks in label
        self.options.label = self.options.label.replace(" ","_")

        return True


    def PDBfiles(self, wPath = ".", lbl = None, pass1 = False, verbose = 0):
        """
        @param wPath: directory to scan
        @param lbl  : file prefix to search for
        return a list of the names of the PDB files generated
        """

        # Label to search for
        if not lbl:
            lbl=self.options.label

        # List files of the current directory
        filesL = os.listdir( wPath )

        # PDB files to clusterize
        pdblist = []
        pattern = "%s*bestene1mc.pdb" % lbl

        # List them

        for aFile in filesL:
            if fnmatch.fnmatch( aFile, pattern ) and FileTools.checkFileStatus( aFile, verbose = verbose ) == 0:
                if aFile.count("pass1") and not pass1:
                    continue
                pdblist.append( aFile )

        # Return the list
        return pdblist


    def linkToBestModels(self, results, maxBest = 10, doSnap = False, verbose = False):
        f = open(results)
        lines = f.readlines()
        f.close()

        isOpened = False
        models = []
        for l in lines:
            if l.count("# cluster "):
                clusterNumber = int(l.split()[2]) + 1
                isOpened = True
                continue
            if isOpened:
                isOpened = False
                fname = l.split()[0]
                os.system("cp %s model%d.pdb" % (fname, clusterNumber))
                maxBest = maxBest - 1
                if maxBest <= 0:
                    break
                continue


    def htmlResults(self, results, maxBest = 10, verbose = False):
        f = open(results)
        lines = f.readlines()
        f.close()
        rs, nclusters, nmodels = self.clusterHTMLize(lines, self.options.label)
        f = open("clusters.html", "w")
        f.write("".join(rs))
        f.close()


    def clusterHTMLize(self, lines, label = "", verbose = 0):
        """
        Append to lines in the clusters format the RMSd value
        @param lines   : the clusters.txt lines
        @param label   : the label as prefix to label-modelx.pdb
        @param verbose : switch verbose mode
        @return        : html page with link to the files and archives


        """
        if verbose:
            sys.stderr.write("Will htmlize the cluster retport\n")
        rs = ["<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n", "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" >\n", "<head>\n", "<title> PEP-FOLD clusters</title>\n","<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-15\" />\n", "</head>\n","<body style=\"font-family:monospace;font-size:11px;line-height:18px;\">\n","Download archive of all models <a href=\"allModels.tgz\"> here </a><br/>"]
        clusterfiles = []
        clusternumber = None
        nmodels = 0
        nclusters = 0
        allcmd = "tar czf allModels.tgz "
        for l in lines:
            if l[0] == '#':
                if l.count("gdt      max"):
                    it = l.split()
                    arraysize = len(it)
                    rs.append("<table>\n")
                    oit = map(lambda x : "<td>%s</td>" % x, it)
                    rs.append("<tr>%s   </tr>\n" % (" ".join(oit)))
                else:
                    if l.count("# cluster"):
                        if clusternumber and (len(clusterfiles) > 1):
                            cmd = "tar czf cluster%s.tgz %s" % (clusternumber, " ".join(clusterfiles))
                            if verbose:
                                sys.stderr.write("%s\n" % cmd)
                            os.system(cmd)
                        nclusters += 1
                        clusterfiles = []
                        it = l.split()
                        clusternumber = str(int(it[2])+1)
                        modelnumber = 0
                        try:
                            cEff = int(it[-1])
                        except:
                            cEff = 1
                        if cEff > 1:
                            rs.append("<tr><td># <a href=\"./cluster%s.tgz\">cluster %s</a> %s </td></tr>\n" % (clusternumber, clusternumber, " ".join(it[3:])))
                        else:
                            rs.append("<tr><td># cluster %s %s </td></tr>\n" % (clusternumber, " ".join(it[3:])))
            elif not l.count("-SC-min.pdb"):
                continue
                pass # The reference should not be in the clusters
                rs.append("%s  <br/>\n"% l[:-1])
            else:
                it = l.split()
                if not len(it):
                    rs.append("<tr><td>%s  </td></tr>\n"% l[:-1])
                    continue
                nmodels += 1
                mfile = l.split()[0]
                clusterfiles.append(mfile)
                it = l.split()
                if modelnumber == 0:
                    modelname = "%s-model%s" % (label, clusternumber)
                else:
                    modelname = "%s-model%s.%d" % (label, clusternumber, modelnumber)
                cmd = "cp %s %s.pdb" % (mfile, modelname)
                allcmd = "%s %s.pdb" % (allcmd, modelname)
                if verbose:
                    sys.stderr.write("%s\n" % cmd)
                os.system(cmd)
                # rs.append("<a href=\"%s\">%s</a> %s<br/>\n" % (it[0], modelname, " ".join(it[1:])))
                lit = ["<a href=\"./%s.pdb\">%s</a>" % (modelname, modelname)] + it[1:]
                oit = map(lambda x : "<td>%s</td>" % x, lit)
                rs.append("<tr><td>&nbsp;</td>%s   </tr>\n" % (" ".join(oit)))
                modelnumber += 1
        rs.append("</table><br/>\n")
        rs.append("</body>\n")
        rs.append("</html>\n")
        if clusternumber and (len(clusterfiles) > 1):
            cmd = "tar czf cluster%s.tgz %s" % (clusternumber, " ".join(clusterfiles))
            if verbose:
                sys.stderr.write("%s\n" % cmd)
            os.system(cmd)
        os.system(allcmd)
        return rs, nclusters, nmodels


    def runPPP(self):

        cluster.progress("PEP-FOLD 2 (1/5): Local prediction")

        cmd = "PyPPPExec"

        args = [ "-s %s" % self.options.seqFile,
                 "-l %s" % self.options.label,
                 "-v" ]

        if self.options.SVM2012:
           args.append("--2012")

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, job_opts='-c 4', log_prefix = "ppp")

        cmd = "SAProbsExec"

        args = [ "-i %s.svmi8.27.prob" % self.options.label,
                 "-l %s" % self.options.label,
                 "-v" ]

        if self.options.autoFromTo:
            args.append("--autoFromTo")

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "saprobs")


    def runGreedy(self):

        cluster.progress("PEP-FOLD 2 (2/5): 3D generation")

        job_list = []

        if self.options.combined:
            job_list = [[80, 'trj'], [80, 'HEnoSmooth.sure.trj'], [40, 'noFilter.trj']]

        if self.options.shortcombined:
            job_list = [[40, 'trj'], [40, 'HEnoSmooth.sure.trj'], [20, 'noFilter.trj']]

        if self.options.pepfold1:
            job_list = [[100, 'noFilter.trj']]

        njobs_total = 0

        for njobs, ext in job_list:

            cmd = "GreedyExec"

            args = [ "-s %s" % self.options.seqFile,
                     "-t %s.%s" % (self.options.label, ext),
                     "-l %s" % self.options.label,
                     "--zipMask %s.gmask" % self.options.label,
                     "--sgemap",
                     "--SSA 10.000000",
                     "--MCSS 0.000000",
                     "--NBA 10.000000",
                     "--MCNB 0.000000",
                     "--clusterize 3",
                     "--mcTemp 1250" ]

            if self.options.SSBonds is not None:
                args.append("--SSBonds %s" % self.options.SSBonds)

            if self.options.neighbours is not None:
                args.append("--Neighbours %s" % self.options.neighbours)

            if self.options.autoFromTo:
                with open("%s.autofromto" % self.options.label) as autoFromToFile:
                    line = autoFromToFile.readline()
                    autoFrom, autoTo = line.split()
                    if (int(autoFrom) != 0 or int(autoTo) != -1):
                        args.append("--autoFromToFile %s.autofromto" % self.options.label)

            cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, tasks = njobs, tasks_from = njobs_total + 1, log_prefix = "greedy")

            njobs_total += njobs

        self.options.njobs = njobs_total


    def runPostTT(self):
        """
        Greedy generates PEPFOLD-62_bestene0.pdb and PEPFOLD-62_bestene1mc.pdb files
        PostTreatment generates a PEPFOLD-62_bestene1mc-SC.pdb (side chains added)
        then a PEPFOLD-62_bestene1mc-SC-min.pdb (Gromacs minimization)
        """

        cluster.progress("PEP-FOLD 2 (3/5): PostTreatment")

        cmd = "POSTTTExec"

        args = [ "--mask *ene1mc.pdb",
                 "--oscar",
                 "--pattern *-%s_bestene1mc.pdb",
                 "--parallelposttreatment",
                 "--padding 3",
                 "--silent",
                 "-v" ]

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, tasks = self.options.njobs, log_prefix = "posttreatment")


    def runClustering(self):

        cluster.progress("PEP-FOLD 2 (4/5): Clustering")

        cmd = "DBClustersExec"

        args = [ "-l prefix",
                 "--mask *-*-SC-min.pdb",
                 "--odir MQA",
                 "--score %s" % self.options.score,
                 "--threshold %s" % self.options.threshold,
                 "--ncores %s" % self.options.np ]

        if self.options.reference:
            args.append("--ref %s" % self.options.reference)

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "clustering", job_opts = "-c %s" % self.options.np)

        cluster.progress("PEP-FOLD 2 (5/5): compiling results")

        shutil.copy2("./MQA/clusters.txt", "clusters.txt")

        cmd = "PPFLDClusterAppendEne"

        args = [ "--target clusters.txt",
                 "--sortKey %s" % self.options.sortKey ]

        if self.options.SSBonds is not None:
            args.append("--SSBonds %s" % self.options.SSBonds)

        if self.options.neighbours is not None:
            args.append("--neighbours %s" % self.options.neighbours)

        if self.options.reference is not None:
            args.append("--reference %s" % self.options.reference)

        if self.options.refZones is not None:
            args.append("--refZones %s" % self.options.refZones)

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "clusterappend")

        self.linkToBestModels("clusters.txt", maxBest = 10, doSnap = False)
        self.htmlResults("clusters.txt", maxBest = 5)


    def formatViewer(self, wdir = "./"):

        models = glob.glob("model*.pdb")
        data = [ ("Model %d" % i, '', '', 'model%d.pdb' % i) for i in range(1, len(models) + 1) ]
        with open('bestModels.html', 'wt') as sink:
            template = jinja_env.get_template('pepfold.vis.html')
            sink.write(template.render(data = data, wdir = wdir, color = 'color-succession'))


    def runPepFold(self, verbose = False):
        """
        New runner, preparing to mimick SGE behaviour on slurm, 
        but in a simpler manner than previously!
        """
        self.cwd = os.getcwd()

        doRun = True # Easy debug
        NFSDELAY = 2 # 2 secconds between each qsub
        self.start_time = time.time() # To track execution time

        # 1. Generate .prob, 3 .trj, .gmask and .autoFromTo Management
        if self.options.doPPP:
            self.runPPP()

        # 2. Now we run greedy (3D generation)
        if self.options.do3D:
            self.runGreedy()

        # 3. PostTreatment:
        # 3.1 We move from coarse grained to all atom
        # 3.2 We minimize using Gromacs
        if self.options.doPostTT:
            self.runPostTT()

        # 4. Clustering, MQA
        if self.options.doClusters:
            self.runClustering()

        jobId = os.path.basename(os.getcwd())
        serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]   # can be patchsearch or patchidentification
        wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

        self.formatViewer(wdir)


def main( args ):
    """
    Launch the main purpose:
    - Parse command line
    - Launch predictions
    """

    # Parse arguments
    job = MINI_FOLD(args = args)
    job.start(check = False, verbose = True) # Do not check yet

    # Manage demo before checking options validity
    if job.options.demo:
        sys.stderr.write("Will install demo files.\n")
        sys.stderr.write("Copying %s/%s as iSeq.data.\n" % (PEPFOLD_ROOT, DFLT_DEMO_SEQUENCE))
        os.system("cp %s/%s ." % (PEPFOLD_ROOT, DFLT_DEMO_SEQUENCE))
        os.system("cp %s/%s ." % (PEPFOLD_ROOT, DFLT_DEMO_PDB))
        job.options.label     = DFLT_DEMO_LABEL
        job.options.seqFile   = DFLT_DEMO_SEQUENCE
        job.options.reference = DFLT_DEMO_PDB
        if DFLT_DEMO_SSBONDS is not None:
            job.options.SSBonds  = DFLT_DEMO_SSBONDS
            job.options.pepfold1 = True
            job.options.SSA      = 10.
        # We must recheck options manually otherwise -s None will fail
        # job.start(parse=False, verbose = True)
    job.start(parse=False, verbose = True) # Now we check

    if not job.cmd_line_status:
        sys.stderr.write("PEP-FOLD 2: Sorry, incorrect command line.\n")
        if REMOTE_SERVER:
            cluster.progress("PEP-FOLD 2: failed when verifying input data. Please check carefully.")
        sys.exit(-1)
        # return
    # job.parse_specific()
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)
    # return

    job.runPepFold(job.options.verbose)
    # process
    # ret = process( argsDict )
    if REMOTE_SERVER:
        cluster.progress("PEP-FOLD 2: completed successfully.")

    return 

# GO !
if __name__ == "__main__":
    main(sys.argv)
# -------------------------
