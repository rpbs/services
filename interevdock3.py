#! /usr/bin/env python3
'''
Created on 20 mar. 2014

@author: Jinchao YU
modified 22/03/2018 JA & CQ: removed FCC
modified Nov 2020 RG, JA & CQ: complete restructuration of the code for version 3
modified Sep 2024 JR: fixed code for new architecture
'''
try:
    import os, sys
    import shutil
    import numpy as np
    import time
    import re
    import datetime
    import csv
    import optparse
    import glob
    import traceback
    import pkg_resources

    # for python version compatibility
    try:
        import ConfigParser
    except:
        import configparser as ConfigParser

    from jinja2 import Environment, PackageLoader

except ImportError:
    print("IMPORT ERROR")
    sys.exit(1)


# home-made functions and classes
try:
    import InterEvDock3.docking as docking
    import InterEvDock3.prepPDBs as prepPDBs
    import InterEvDock3.coMSAs as coMSAs
    import InterEvDock3.checkConstraints as checkConstraints
    import InterEvDock3.mapconservation as mapconservation
    import InterEvDock3.tools as tools
    import InterEvDock3.analyzeContactMap as analyzeContactMap
    import InterEvDock3.getInterface as getInterface
except ImportError:
    print("This script is accompanied by other scripts in the same package.")
    print("ImportError")
    sys.exit(1)

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))

#Constants
DFLT_MOBYLE_JOBS_PATH = config.get("paths","DFLT_MOBYLE_JOBS_PATH") # /data/jobs/ --> address on server (docs have to have this path in the end for visu)
DFLT_CLUSTER_JOBS_PATH = config.get("paths","DFLT_CLUSTER_JOBS_PATH") # /scratch/mobyle/jobs/ --> address on SlurmMaster

jobId = os.path.basename(os.getcwd())
serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]
MOBYLE_JOB_WDIR = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)
 
CMD_WRAPPER_DAREUS_OSCAR = pkg_resources.resource_filename("InterEvDock3",config.get("scripts","wrapper_ccm"))
PYTHON = config.get("scripts","python")

WRAPPER_GROMACS = pkg_resources.resource_filename("InterEvDock3",config.get("scripts","wrapper_min"))

# MTi cluster imports
import cluster.cluster as cluster
#import Fasta.Fasta as Fasta

import PyPDB.PyPDB as PDB

# Initialize templates

jinja_env = Environment(loader = PackageLoader('InterEvDock3', 'templates'))
TEMPLATE_TABLE = 'interevdock.table.html'
TEMPLATE_TABLE_CONS = 'interevdock.table_consensus.html'
TEMPLATE_VIS = 'interevdock.vis.html'
TEMPLATE_VIS_NO_CONS = 'interevdock.vis_no_cons.html'
TEMPLATE_CST = 'interevdock.constraints.html'
TEMPLATE_TEMPLTABLE = 'interevdock3.templates-table.html'
TEMPLATE_TEMPLTABLE_dic = {}
TEMPLATE_TEMPLTABLE_dic['protein_a'] = {}
TEMPLATE_TEMPLTABLE_dic['protein_b'] = {}
TEMPLATE_TEMPLTABLE_dic['protein_a']['full'] = 'interevdock3.protein_a.templates-table.html'
TEMPLATE_TEMPLTABLE_dic['protein_b']['full'] = 'interevdock3.protein_b.templates-table.html'

PYMOL_SCRIPTS_DIR = pkg_resources.resource_filename("InterEvDock3",config.get("templates","pymol"))
README = pkg_resources.resource_filename("InterEvDock3",config.get("templates","readme"))

def prepare_inputs_for_docking(workdir,protein_a,protein_b=None,dockingmethod='frodock',is_coevol_map=False):

    chainA_output_subdir = os.path.join(workdir,'data_chainA') # sub dirs that will contain the translation tables and individual fastas
    chainB_output_subdir = os.path.join(workdir,'data_chainB')
    protein_a_pdb_for_dock = os.path.join(workdir,'protein_a_for_dock.pdb') # clean and monomeric transformed pdbs that will be used in docking
    protein_b_pdb_for_dock = os.path.join(workdir,'protein_b_for_dock.pdb')

    # load prep tools
    oPrep = prepPDBs.PrepPDBs()

    # check pdb A -> is it a pdb, number of residues >10 & <6000
    status, msg = oPrep.checkPDB(protein_a)
    if not status:
        raise Exception(msg)

    # clean partner A pdb and create new clean output protein_a_clean (rmk: protein_a remains unchanged)
    protein_a_clean = oPrep.cleanPDB(protein_a, chainA_output_subdir)

    # create residue translation tables and re-adjusted monomeric outputs from mulimeric inputs 
    # for docking in prot A subdir
    oPrep.Oligo2Mono(chainA_output_subdir, protein_a_clean, protein_a_pdb_for_dock, 'A', use_frodock=(dockingmethod=='frodock'), is_coevol_map=is_coevol_map)

    if protein_b:
        # heteromeric case
        # check pdb B -> is it a pdb, number of residues >10 & <6000
        status, msg = oPrep.checkPDB(protein_b)
        if not status:
            raise Exception(msg)

        # clean partner B pdb and create new clean output protein_b_clean (rmk: protein_b remains unchanged)
        protein_b_clean = oPrep.cleanPDB(protein_b, chainB_output_subdir)

        # create residue translation tables and re-adjusted monomeric outputs from mulimeric inputs 
        # for docking in prot B subdir
        oPrep.Oligo2Mono(chainB_output_subdir, protein_b_clean, protein_b_pdb_for_dock, 'B', use_frodock=(dockingmethod=='frodock'), is_coevol_map=is_coevol_map)

        # input for coMSA search
        chains = ['A','B']
        data_chain_subdirs = [os.path.abspath(chainA_output_subdir),os.path.abspath(chainB_output_subdir)]

    else:
        # homodimeric case, we can save time by copying files from partner A
        # [RG]->@QUESTION: Does this option still runs as expected in IED3 ?
        # [CQ] not tested in this context but in the IED3 input form, you can't run the docking with only a structure in A
        os.system("cp -rp "+chainA_output_subdir+" "+chainB_output_subdir)
        protein_b_clean = os.path.join(chainB_output_subdir,os.path.basename(protein_a_clean))

        # create residue translation tables and re-adjusted monomeric outputs from mulimeric inputs 
        # for docking in prot B subdir
        oPrep.Oligo2Mono(chainB_output_subdir, protein_b_clean, protein_b_pdb_for_dock, 'B', use_frodock=(dockingmethod=='frodock')) # coevol map option can't be used there

        # input for coMSA search
        chains = ['A']
        data_chain_subdirs = [os.path.abspath(chainA_output_subdir)]

    return protein_a_pdb_for_dock, protein_b_pdb_for_dock, chainA_output_subdir, chainB_output_subdir, chains, data_chain_subdirs


def create_coalignments_for_oligomer(workdir, data_chain_subdirs, out_MSAs, chains=['A','B']):
    """
    This script generates coMSAs for oligomers. If len(data_chain_subdirs) == 1 and len(chains) == 1,
    we're in the scenario of a homomer so outputs are duplicated ready for docking (i.e. len(out_MSAs) has to be equal to 2)

    INPUT:
    chains: chain names as renamed by the PrepPDB class i.e. should always be ['A', 'B']
    out_MSAs: list of 2 output fasta MSA names
    data_chain_subdirs: list of subdirs for each protein partner with chainX_representative_chain.out files in them

    IMPORTANT OUTPUTS:
    Both coMSAs are generated in the 2 files named in out_MSAs with the query sequences added at the top of each fasta.

    QUESTIONS
    Do we use blast or full length ali (i.e. use_Full_length_seq=True) ? -> for now, we use blast length as in IED2
    """
    # make sure we have abspath here, otherwise we have problems
    data_chain_subdirs = [os.path.abspath(subdir) for subdir in data_chain_subdirs] 
    
    curdir = os.path.abspath(os.getcwd())
    os.chdir(workdir)

    # Analyze each oligomer
    oligomer_definition = {}
    dic_chain2index = {}
    list_fasta_fname = []
    indexchain = 1

    # We collect the data required to derive the way we should encode the arguments for "co_alignment_vs_UP_oligomer" function equivalent
    for chain, subdir in zip(chains,data_chain_subdirs):
        oligomer_definition[chain] = []
        fchain = open(subdir+"/chain%s_representative_chain.out"%(chain))
        for l in fchain.readlines():
            s = l.split("\t") # e.g. chainB_pdbextract_chain_A.fasta chainB_pdbextract_chain_A.fasta protein 106
            realchain = s[0]
            representative = s[1]
            mol_type = s[2]
            length = s[3]
            if mol_type != 'protein':
                continue
            if realchain == representative:
                dic_chain2index[realchain] = indexchain
                list_fasta_fname.append(os.path.join(subdir,realchain)) # we absolutely need abspath here
                indexchain += 1
            else:
                index_rep = dic_chain2index[representative]
                dic_chain2index[realchain] = index_rep
            oligomer_definition[chain].append(str(dic_chain2index[realchain]))

    # We create the input for option -g 1,1:2,3,4 for "co_alignment_vs_UP_oligomer"   
    if len(chains) == 1:
        # homodimer
        combinedAB_code = ",".join(oligomer_definition[chains[0]])
    else:
        combinedAB_code = ":".join([','.join(oligomer_definition[ch]) for ch in chains])

    # SETP1 Run the blast for every single sequence and recover the alignments
    # + STEP2 Concatenate the multiple sequence alignments      
    print('Generating coMSAs')
    # rmk: class GenerateCoMSAs is based on co_alignment_vs_UP_oligomer.py
    coMSAs.GenerateCoMSAs(list_fasta_fname,redundancy_filter=90,group_MSA=combinedAB_code,if_do_separate_MSA=False, use_Full_length_seq=False,parallelise=True,verbose=True, coverage=75, seq_id=30)

    # STEP3 Add the input seq on top of the alignment
    # these are the default output file names for GenerateCoMSAs and are created in the current workdir:
    fasta1 = "regrouped_INPUTSEQ1.fasta" 
    fasta2 = "regrouped_INPUTSEQ2.fasta"
    outfile1 = "regrouped_MSA1.fasta"
    outfile2 = "regrouped_MSA2.fasta"

    headers = tools.read_fasta(outfile1)[0]
    if len(headers) == 0:
        # If no sequences in resulting MSA, add query sequence -> at least we have one mono sequence alignment available
        cluster.progress("... Warning: no sequences in coMSAs")
        print('No sequences left in coMSAs => query sequences only')
        shutil.copy(fasta1, out_MSAs[0])
        if len(chains)==1:
            shutil.copy(fasta1, out_MSAs[1])
        else:
            shutil.copy(fasta2, out_MSAs[1])
    else:
        # Add the input sequence on top of the alignment
        # use mafft --seed
        # rmk: we use mafft --seed now instead of merge_MSA.py in which first 
        # sequences were aligned in a pairwise fashion and used to align 2 MSAs
        # This was not ideal as extra regions in query seq were not well realigned onto the MSA
        print('Adding query sequence to MSAs')
        coMSAs.MergeMSAs([fasta1], [outfile1], out_MSAs[0])
        if len(chains)==1:
            shutil.copy(out_MSAs[0], out_MSAs[1])
        else:
            coMSAs.MergeMSAs([fasta2], [outfile2], out_MSAs[1])
        if len(headers) < 10:
            cluster.progress("... Warning: less than 10 sequences in coMSAs")
            print("Warning: less than 10 sequences in coMSAs")


    os.chdir(curdir)
    return len(headers)


def get_interface_residues(list_of_pdbs, chainsA=None, chainsB=None):
    """
    If we want to only show the docked interface, chainsA and chainsB have to be specified 
    as a string (e.g. "ABC"); if not specified, residues for all interfaces will be outputted    
    """
    threshold = 5 # 5A is the contact threshold between any heteroatoms of 2 different chains

    dico_interface_residues_for_visu = {}
    for pdb_complex in list_of_pdbs:
        oInter = getInterface.GetInterface(pdb_complex,threshold,chainsA,chainsB)
        dico_interface_residues_for_visu[pdb_complex]=oInter.lst_contacts

    print("dico_interface_residues_for_visu",dico_interface_residues_for_visu)
    return dico_interface_residues_for_visu


def format_output(methods=[],d_scorefiles={},d_names={},d_paths={},wdir='./',IEDfile=None,labels=[],collectedinfo={},collectedinfosmall={}):
    """
    Here, we prepare the output score table and the output visualisation html
    """
    headers_for_score_table = {
        'Consensus': ['Final rank', 'Consensus models'],
        'IES':       ['IES rank', 'IES Score (higher is better)'],
        'SPP':       ['SPP rank', 'SPP score (higher is better)'],
        'FRODOCK2.1':['FRODOCK2 rank', 'FRODOCK2 score (higher is better)'],
        'IESh':      ['IESh rank', 'IESh Score (higher is better)'],
        'SPPh':      ['SPPh rank', 'SPPh score (higher is better)'],
        'ISCh':      ['ISCh rank', 'ISCh score (lower is better)'],
        'ISC':       ['ISC rank', 'ISC score (lower is better)'],
        'CMAPnb':    ['CMAP NbContacts models', 'Nb of Satisfied Contacts (higher is better)'],
        'CMAPsco':   ['CMAP ScoreContacts models', 'Score of Satisfied Contacts (higher is better)'],
    }
    # traditional free docking -> visu with IED only and a modified score table with rank/consensus model/equivalent IES rank/IES score/etc.
    if IEDfile and os.path.exists(IEDfile):
        # header = ['Final rank', 'Consensus models', 'IES rank', 'IES score' etc.] i.e. just a list
        # data_count = len(top10IED) i.e. number of top models -> max 10
        # data = [['1','Complex1','5','60.568','10','-105.51',etc.]]
        # dico_for_visu = {"10best":[['Complex1',path/to/complex1], etc.]}

        d_top50 = {}
        for m in methods:
            corr = 1 if "ISC" in m else -1
            f=open(d_scorefiles[m])
            scores={l.split()[0]:float(l.split()[1]) for l in f if not l.startswith("#") and l.strip()}
            f.close()
            d_top50[m] = scores
            d_top50[m]['order'] = sorted(scores,key=lambda k: scores[k]*corr)

        os.makedirs("Consensus_models")
        
        # read top 10 InterEvDock consensus
        data = []
        dico_for_visu = {"10best":[]}
        f=open(IEDfile)
        i=1
        for l in f:
            if not l.strip() or l.startswith("#"):
                continue
            decoy=l.split()[1]
            m,num=re.search(r"Complex_(\w+[^\d])(\d+)",decoy).groups()
            m = m.replace('FRODOCK',"FRODOCK2.1")
            original_decoy_name = d_top50[m]['order'][int(num)-1] # e.g. complex.1.pdb

            shutil.copy2(os.path.join(d_paths[m],decoy),"Consensus_models/Consensus_{}.pdb".format(i))
            dico_for_visu["10best"].append(["Consensus_"+str(i),"Consensus_models/Consensus_{}.pdb".format(i)])
            lst_scores = [i,"Consensus_"+str(i),"Consensus_models/Consensus_{}.pdb".format(i)] # CQ: rename to Consensus_1, Consensus_2 etc.

            for mm in methods:
                if original_decoy_name in d_top50[mm]:
                    rank = str(d_top50[mm]['order'].index(original_decoy_name)+1)
                    score = "{:.3f}".format(d_top50[mm][original_decoy_name])
                else:
                    rank = '-'
                    score = '-'
                lst_scores += [rank, score]
            data.append(lst_scores) 
            i+=1
        f.close()

        dico_interface_residues_for_visu = get_interface_residues(glob.glob(os.path.join("Consensus_models","*pdb")), chainsA=None, chainsB=None)
        for k in dico_for_visu:
            for i in range(len(dico_for_visu[k])):
                dico_for_visu[k][i].append(dico_interface_residues_for_visu.get(dico_for_visu[k][i][1],[]))

        data_count = len(data)
        header = headers_for_score_table['Consensus']
        for m in methods:
            header += headers_for_score_table[m]

        # individual score table
        title = 'Top 10 models'
        sink = open(TEMPLATE_TABLE, 'w')
        template = jinja_env.get_template(TEMPLATE_TABLE_CONS)
        sink.write(template.render(data = data, data_count = data_count, header = header, title = title, wdir = MOBYLE_JOB_WDIR+"/"))
        sink.close()                

        # Format visualization
        title = 'Visualisation of top-scoring models'
        sink = open(TEMPLATE_VIS, 'w')
        template = jinja_env.get_template(TEMPLATE_VIS)
        sink.write(template.render(data = dico_for_visu, wdir = MOBYLE_JOB_WDIR+"/"))
        sink.close()


    # coevol map case -> visu wo IED consensus and traditional score table
    elif "CMAPsco" in methods:
        # table = {methodname: [list of top 50 scores]}
        # data = {methodname: ['Compelx_XXXN.pdb',score,fullpath]}
        # data_count = 10 -> top 10 decoys per score
        # dico_for_visu = {"CMAPsco":[['Complex_CMAPsco1',path/to/Complex_CMAPsco1.pdb], etc.], "CMAPnb": [[]]}

        dico_for_visu = {}
        data = {}
        data_count = 0
        for m in methods:
            # read score file
            f = open(d_scorefiles[m])
            scores = [float(l.split()[1]) for l in f if not l.startswith("#") and l.strip()]
            f.close()
            top10 = sorted(scores,reverse=True)[:10]
            data_count = max(data_count,len(top10))
            selection = [[d_names[m].format(i + 1), top10[i], os.path.join(d_paths[m], d_names[m].format(i + 1))] for i in range(len(top10))]
            data[m] = selection
            dico_for_visu[m] = [[os.path.splitext(s[0])[0],s[2]] for s in selection]

        dico_interface_residues_for_visu = get_interface_residues([pdb for folder in glob.glob("*_models/") for pdb in glob.glob(os.path.join(folder,"*pdb"))], chainsA=None, chainsB=None)
        for k in dico_for_visu:
            for i in range(len(dico_for_visu[k])):
                dico_for_visu[k][i].append(dico_interface_residues_for_visu.get(dico_for_visu[k][i][1],[]))

        # individual score table
        title = 'Top %d models' % data_count
        sink = open(TEMPLATE_TABLE, 'w')
        template = jinja_env.get_template(TEMPLATE_TABLE)
        sink.write(template.render(data = data, data_count = data_count, header = {m:headers_for_score_table[m] for m in methods}, methods = methods, title = title, wdir = MOBYLE_JOB_WDIR+"/"))
        sink.close()

        # Format visualization
        title = 'Visualisation of top-scoring models'
        sink = open(TEMPLATE_VIS, 'w')
        template = jinja_env.get_template(TEMPLATE_VIS_NO_CONS)
        sink.write(template.render(data = dico_for_visu, wdir = MOBYLE_JOB_WDIR+"/"))
        sink.close()

    # TBM case -> visu but no table
    else:   
        dico_for_visu = {"TBM-models": [["Model_{}".format(i),d_names['TBM'][i-1]] for i in range(1,len(d_names['TBM'])+1) if os.path.exists(d_names['TBM'][i-1])]}
        if dico_for_visu["TBM-models"]:
            dico_interface_residues_for_visu = get_interface_residues(d_names['TBM'], chainsA=None, chainsB=None)
            for k in dico_for_visu:
                for i in range(len(dico_for_visu[k])):
                    dico_for_visu[k][i].append(dico_interface_residues_for_visu.get(dico_for_visu[k][i][1],[]))
            title = 'Visualisation of top-scoring template based model'
            sink = open(TEMPLATE_VIS, 'w')
            template = jinja_env.get_template(TEMPLATE_VIS_NO_CONS)
            sink.write(template.render(data = dico_for_visu, wdir = MOBYLE_JOB_WDIR+"/"))
            sink.close()

    # display templates if TBM was performed
    if labels:
        title = 'Possible templates'
        for label in labels:
            sink = open(TEMPLATE_TEMPLTABLE_dic[label]['full'], 'w')
            template = jinja_env.get_template(TEMPLATE_TEMPLTABLE)
            sink.write(template.render(data = collectedinfo[label]))
            sink.close()
            #sink = open(TEMPLATE_TEMPLTABLE_dic[label]['short'], 'w')
            #template = jinja_env.get_template(TEMPLATE_TEMPLTABLE)
            #sink.write(template.render(data = collectedinfosmall[label]))
            #sink.close()


# def format_output_for_template_based(names,labels,collectedinfo,collectedinfosmall):
#     # Format visualization for template-based modeling case
    
#     dico_for_visu = {"TBM-models": [["Model_TBD{}".format(i),names[i-1]] for i in range(1,len(names)+1)]}

#     title = 'Visualisation of top-scoring template based model'
#     sink = open(TEMPLATE_VIS, 'w')
#     template = jinja_env.get_template(TEMPLATE_VIS)
#     sink.write(template.render(data = dico_for_visu, wdir = MOBYLE_JOB_WDIR+"/"))
#     sink.close()

#     title = 'Possible templates'
#     for label in labels:
#         sink = open(TEMPLATE_TEMPLTABLE_dic[label]['full'], 'w')
#         template = jinja_env.get_template(TEMPLATE_TEMPLTABLE)
#         sink.write(template.render(data = collectedinfo[label]))
#         sink.close()
#         #sink = open(TEMPLATE_TEMPLTABLE_dic[label]['short'], 'w')
#         #template = jinja_env.get_template(TEMPLATE_TEMPLTABLE)
#         #sink.write(template.render(data = collectedinfosmall[label]))
#         #sink.close()

def format_constraints(constraints_file, title = "User constraints"):
  
    data = []
    headers = ["Original constraint", "Status", "Value / Reason"]
    with open(constraints_file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter="\t")
        for row in reader:
            data.append(row)
    sink = open(TEMPLATE_CST, 'w')
    template = jinja_env.get_template(TEMPLATE_CST)
    sink.write(template.render(data = data, headers = headers, title = title))
    sink.close()    

def best_dsv_sorted(tsvfile):
    regex = re.compile(r"sequence(\d+):(\w+) \((\d+)\)")
    d = []
    with open(tsvfile,"r") as f:
        for l in f:
            t = l.strip().split("\t")
            if t[0]=="Template":
                continue
            pdb = t[0]
            id_percent = float(t[2])
            coverage = float(t[3])
            m=re.findall(regex,t[4])
            method = t[7]
            resol = t[8]
            d.append([pdb,id_percent,coverage,len(m),m,t[4],method,resol])
    # all coverage <25% get pushed to the end. Then sort by decreasing id first, then by decreasing coverage.
    sorted_d = sorted(d, key= lambda x:(x[2]<25,x[1]<30,-x[2]))
    # then we have to re-sort the part of the list where id<30%, by decreasing (id+cov)
    idx=[i for (i,x) in enumerate(sorted_d) if ((x[1]<30) & (x[2]>=25))]
    if idx:
        first = idx[0]
        last = idx[-1]
        # when cov is >=25%, second sorting criterion is identity
        sorted_d[first:last+1] = sorted(sorted_d[first:last+1],key = lambda x:(-(x[1]+x[2]),-x[1]))
        # when cov is <25%, second sorting criterion is coverage
        sorted_d[last+1:] = sorted(sorted_d[last+1:],key = lambda x:(-(x[1]+x[2]),-x[2]))
    return sorted_d



def run_dareus_oscar(inputs=[],labels=[],workdirs=[],outputs=[],min_loop_sze=2,max_loop_sze=50,max_loop_num=150):
    """
    To run Pierre's wrapper for comparative modelling
    inputs are a list of whether query sequence fastas or template pdb id+(optional jobid) or a mix
    only basenames of inputs are used in this script so make sure the inputs are in workdirs
    labels are the output labels for each model and workdirs are the folders where everything will be created (temp files included)
    """
    print(inputs,labels,workdirs,outputs)

    for i in range(len(inputs)):

        if inputs[i][0]:

            cmd = CMD_WRAPPER_DAREUS_OSCAR

            args = [ "-l %s" % labels[i],
                     "-q %s" % os.path.basename(inputs[i][0]),
                     "--min_loop_sze %s" % min_loop_sze,
                     "--max_loop_sze %s" % max_loop_sze,
                     "--max_loop_num %s" % max_loop_num,
                     "--master_progress %s" % os.path.abspath(os.getcwd()),
                     "--verbose",
                   ]

            if inputs[i][1]:

                args.append("--t_id %s" % inputs[i][1])

            if inputs[i][2]:

                args.extend([" --session_id %s" % inputs[i][2],
                             " --p3dn_path %s" % DFLT_CLUSTER_JOBS_PATH,
                             " --noProteo3Dnet"
                            ])

            cmd = "python %s %s" % (CMD_WRAPPER_DAREUS_OSCAR, " ".join(args))
            os.chdir(workdirs[i])
            os.system(cmd)
            os.chdir("..")

    worked = [os.path.exists(os.path.join(workdirs[i],labels[i]+'-ccm-model_SC-min.pdb')) for i in range(len(workdirs))]

    for i in range(len(workdirs)):
        if worked[i]:
            # remove H prior to copying output PDB model
            tmp_prot_a_clean = tools.make_tempfile(parent_dir='.',prefix_str='protein_a',suffix_str='.pdb')
            p = PDB.PDB('{}/{}-ccm-model_SC-min.pdb'.format(workdirs[i],labels[i]),keepH=0)
            p.out(tmp_prot_a_clean)
            os.system('cp {} {}'.format(tmp_prot_a_clean,outputs[i]))

    # copy important outputs to workdir
    info = []
    collectedinfo = {}
    collectedinfosmall = {}
    chosentemplates = {}
    for it,(inp,wd) in enumerate(zip(inputs,workdirs)):
        if not worked[it]:
            info.append([])
            cluster.progress("... Error for Partner "+['A','B'][it]+": Comparative modeling step failed, there are usually three main causes for this:\nthe template might only be available in mmcif format,\nthere might be too many unknown residues (X) or\nthere might be discrepencies between the input sequence(s) and the sequences in the potential templates.")
            print("Error: Comparative modeling step failed, there are usually three main causes for this:\nthe template might only be available in mmcif format,\nthere might be too many unknown residues (X) or\nthere might be discrepencies between the input sequence(s) and the sequences in the potential templates.")
            # continue

        template_id, identity, coverage = None, None, None
        if inp[0] and not inp[1] and not inp[2]:
            # query seq given
            bn = os.path.basename(os.path.splitext(inp[0])[0])
            if os.path.exists(wd+'/'+bn+'-hhexpand/'+bn+".hhdb"):
                f=open(wd+'/'+bn+'-hhexpand/'+bn+".hhdb")
                data=f.read()
                f.close()
                f=open(wd+"/"+bn+".hhdb","w")
                f.write("#query\ttemplate_chain\t-\tid%\t-\tmatched%\t-\tcov%\n") #sequence1   5XYN_D  -   38  -   202 -   81
                f.write(data)
                f.close()
            os.system('cp '+wd+'/'+bn+'-hhsearch/*hhsearch.out'+' '+wd+'/'+bn+'-hhsearch/*hhsearch.html'+' '+wd+'/'+bn+'-hhsearch/*hhsearch.png'+' '+wd)

        if inp[0] and worked[it]:
            bn = os.path.basename(os.path.splitext(inp[0])[0])
            # read what template was used and id + coverage
            f=open(wd+"/.progress.txt","r")
            data = f.read()
            f.close()
            myregex = re.compile(r"Considering template (\w{4})")
            # -d c001-protein_a_5XYN_3.3_86.0_33.0.dsv
            try:
                #data = data.split()
                #tmp = data[-1*(data[::-1].index("-d"))].strip(".dsv").split("_") # to get the last occurrence
                match = re.findall(myregex,data)
                template_id = match[-1]
                #template_id, res, coverage, identity = tmp[-4],tmp[-3],tmp[-2],tmp[-1]
                #info.append([template_id, identity, coverage, res])
                chosentemplates[bn] = {}
                chosentemplates[bn][template_id] = {}
                #f=os.popen('grep ">{}" {}/*hhsearch.out | grep "Proba"'.format(template_id,wd))
                #for line in f:
                #    res = re.search(">(\w{4}_\w).*; Probability=([\d\.\-]+); Identities=([\d\.\-]+)%; Resolution=([\d\.\-]+)A; MatchedRegionTemplate=[\d\-\.]+;(.*)$",line)
                #    # 0_protein_a_00_hhsearch.out:>5XYN_D:AUTOPDB; Probability=100.0; Identities=36%; Resolution=3.3A; MatchedRegionTemplate=4-226; Platinum sensitivity protein 3, Chromosome; Comlex, REPLICATION, DNA BINDING PROTEIN; {Saccharomyces cerevisiae (strain ATCC 204508 / S288c)}
                #    if res:
                #        info[-1].append(res.groups()) # tpl_chain, proba, id, res, description
                #f.close()
            except:
                pass
            # *_<bn>_00_hhsearch.html
            # *_<bn>_00_hhsearch.out
            # *_<bn>_00_hhsearch.png

        if inp[0]:
            bn = os.path.basename(os.path.splitext(inp[0])[0])
            # try to extract relevant info from tsv files, they should be sorted by decreasing number of sequences
            printc = 3
            printd = 3
            longesttsv = 0
            tsvlst = sorted(glob.glob(wd+"/*.tsv"))
            # c001 should always be there and contains the template used for modeling
            collectedinfo[bn] = {}
            collectedinfosmall[bn] = {}
            for ite,t in enumerate(tsvlst):
                tsvinfo = best_dsv_sorted(t)
                longesttsv = max(longesttsv, len(tsvinfo))
                for j in range(len(tsvinfo)):
                    collectedinfo[bn]['g{:03}t{:03}'.format(ite+1,j+1)] = tsvinfo[j]
                    collectedinfo[bn]['g{:03}t{:03}'.format(ite+1,j+1)].append('g{:03}'.format(ite+1))
                    collectedinfo[bn]['g{:03}t{:03}'.format(ite+1,j+1)].append('t{:03}'.format(j+1))
            print("collectedinfo")
            print(bn)
            print(collectedinfo[bn])
            with open(wd+"/"+bn+".short_table_templates.txt","w") as o:
                o.write("#"+"\t".join(["templ","PDB","avg_id","avg_cov","num_seqs","which_seqs","method","resol"])+"\n")
                for i in range(printc):
                    for j in range(printd):
                        tid = 'g{:03}t{:03}'.format(i+1,j+1)
                        if tid in collectedinfo[bn]:
                            cinfo = collectedinfo[bn][tid]
                            whichseqs = " ".join(["s%s:%s(%s%%)"%(x[0],x[1],x[2]) for x in cinfo[4]])
                            o.write("\t".join([tid,cinfo[0],str(cinfo[1]),str(cinfo[2]),str(cinfo[3]),whichseqs,cinfo[6],cinfo[7]])+"\n") 
                            collectedinfosmall[bn][tid] = cinfo 
                            if cinfo[0] in chosentemplates[bn].keys():
                                for x in cinfo[4]:
                                    chosentemplates[bn][cinfo[0]][x[0]] = [x[1],x[2]] 

            with open(wd+"/"+bn+".full_table_templates.txt","w") as o:
                o.write("#"+"\t".join(["templ","PDB","avg_id","avg_cov","num_seqs","which_seqs","method","resol"])+"\n")
                for i in range(len(tsvlst)):
                    for j in range(longesttsv):
                        tid = 'g{:03}t{:03}'.format(i+1,j+1)
                        if tid in collectedinfo[bn]:
                            cinfo = collectedinfo[bn][tid]
                            whichseqs = " ".join(["s%s:%s(%s%%)"%(x[0],x[1],x[2]) for x in cinfo[4]])
                            o.write("\t".join([tid,cinfo[0],str(cinfo[1]),str(cinfo[2]),str(cinfo[3]),whichseqs,cinfo[6],cinfo[7]])+"\n")
 
        else:
            # pass for now because not tested...
            # we'll have to see later on what's important to keep here
            # a priori, c'est la même info qu'on peut récupérer...
            pass

    return info,collectedinfo,collectedinfosmall,chosentemplates


def run_final_minimization(docking=True):
    """Minimise InterEvDock output structures from *_model files
    Option docking is to say if the models were generated without coevol map (ie. decoys are the same from one score to another) 
    so that we can reduce minimisation time by removing redundancy
    """
    isok = True
    folders = glob.glob("*_models/")

    if docking:
        total = 0
        scorefiles = glob.glob("scores_*.txt")
        d_equivalence = {}
        d_equivalence_rev = {}
        for scorefile in scorefiles:
            scorename = re.search(r"scores_(\w+).txt",os.path.basename(scorefile)).group(1)
            coeff = 1
            if not 'ISC' in scorefile:
                coeff = -1
            f=open(scorefile)
            d_scores = {int(l.split()[0].split(".")[1]):float(l.split()[1])*coeff for l in f if l.strip() and not l.startswith("#")}
            f.close()
            # [decoy number] -> score (lower = better)
            top50 = sorted(d_scores,key=lambda k:(d_scores[k],k))[:50]
            total += len(top50)
            for rank,decoy in enumerate(top50):
                complexe = scorename.replace('FRODOCK',"frodock")+"_models/Complex_"+scorename+str(rank+1)+".pdb"
                if decoy not in d_equivalence:
                    d_equivalence[decoy] = complexe
                    d_equivalence_rev[complexe] = []
                else:
                    d_equivalence_rev[d_equivalence[decoy]].append(complexe)

        lst_pdbs = list(d_equivalence.values())+glob.glob('Consensus_models/*pdb')
        print("Using decoy equivalence to accelerate the minimisation step (reduced number from {} to {}".format(total,len(lst_pdbs)))
        for rep in sorted(d_equivalence_rev):
            print(rep,d_equivalence_rev[rep])

    else:
        lst_pdbs = []
        for folder in folders:
            lst_pdbs += glob.glob(folder+"/*")

    # split over 15 jobs, each job has a list of pdb files to minimise on 6 cpus each (sequentially) i.e. 90 cpus used total to minimise 150-250 structures
    split = 15
    lg = len(lst_pdbs)
    for i in range(split):
        f=open("input_{}.txt".format(i+1),"w")
        for j in range(i,lg,split):
            f.write(lst_pdbs[j]+"\n")
        f.close()

    cluster.runTasks("python3 "+WRAPPER_GROMACS,["-f input_${SLURM_ARRAY_TASK_ID}.txt"],tasks=split,tasks_from=1)

    for i in range(split):
       os.remove("input_{}.txt".format(i+1))

    for folder in folders:
        try:
            shutil.rmtree(folder+"/.cache")
            shutil.rmtree(folder+"/.config")
        except:
            pass

    if os.path.exists("mini_failed.txt"):
        isok = False
        cluster.progress("Minimization failed, this might be due to non-standard atoms")
        print("... minimization failed, this might be due to non-standard atoms")

    if docking:
        # copy decoys that were minimised across
        for complexe_rep, lst_complexe in d_equivalence_rev.items():
            for complexe in lst_complexe:
                shutil.copy(complexe_rep,complexe)

    return isok

def main(workdir='./',protein_a='',protein_b='',alig_a='',alig_b='',constraints_file='',contact_map_file='',nodes=3,cpus=8,runISC=False,runExplicit=True,runTBD=True,dock_noconstraint=None,nr_decoys=10000,dockingmethod='frodock',frodock_version=2.1,seq10=False,protein_a_fasta='',protein_b_fasta='',protein_a_alignment='',protein_a_template_pdb='',protein_b_alignment='',protein_b_template_pdb='',exit_after_template_search=False,exit_after_modeling=False,runMini=False,job_session=False,runMiniTop50=False,NdecoyCovariationMap=300000,dthresh_contact=8.0,redundantContactsWindowSize=2,frodock_type_option='O',min_loop_sze=2,max_loop_sze=50,max_loop_num=150):
# def main(workdir='./',protein_a='',protein_b='',alig_a='',alig_b='',constraints_file='',contact_map_file='',nodes=1,cpus=1,runISC=False,runExplicit=True,runTBD=True,dock_noconstraint=None,nr_decoys=10000,dockingmethod='frodock',frodock_version=2.1,seq10=False,protein_a_fasta='',protein_b_fasta='',protein_a_alignment='',protein_a_template_pdb='',protein_b_alignment='',protein_b_template_pdb='',exit_after_template_search=False,exit_after_modeling=False,runMini=False,job_session=False,runMiniTop50=False,NdecoyCovariationMap=300000,dthresh_contact=8.0,redundantContactsWindowSize=2,frodock_type_option='O',min_loop_sze=2,max_loop_sze=50,max_loop_num=150):
    """
    cpus options is not used in any mpi runs (frodock) but is used to calculate the total number of jobs Rosetta ISC is parallelised on
    """
    if not os.path.exists(workdir):
        os.makedirs(workdir)

    curdir = os.path.abspath(os.getcwd())
    os.chdir(workdir)
    labels = []
    collectedinfo = {}
    collectedinfosmall = {}

    # copy the README 
    os.system('cp %s .' % (README))
    archive = [os.path.basename(README),'.progress']

    if runMiniTop50:
        # Options to trigger minimization
        cluster.progress(" ... minimising structures")
        print(" ... minimising structures")
        state = run_final_minimization(docking=True)
        archive += glob.glob("dock*.dat") + glob.glob("protein_*.pdb") + glob.glob("*_models") + glob.glob("score*txt")
        archive += ['scripts'] 
        archive += glob.glob("start_analysis*")
        archive += glob.glob("InterEvDock_Top*.txt")
        archive += glob.glob("consensus*.txt")
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
        if state:
            cluster.progress("Done. You can download your minimized structures in the new archive below")
        os.chdir(curdir)
        return

    inputs = []
    outputs = []            
    labels = []
    workdirs = []
    # if not pdb for partner A, check for sequence or template code
    if not protein_a:
        if protein_a_template_pdb:
            # model from template
            # for now, template isn't a pdb but a pdb code
            if job_session:
                inputs.append([protein_a_fasta,protein_a_template_pdb,job_session])
            else:
                print("Error: Cannot force a template without having a reference to a previous job id")
                cluster.progress("Cannot force a template without having a reference to a previous job id")
                sys.exit()
                # inputs.append([protein_a_fasta,protein_a_template_pdb,''])
            labels.append('protein_a')
            workdirs.append('protein_a/')
            outputs.append('protein_a.pdb')
            archive += [protein_a_template_pdb,'protein_a.pdb']
            # not sure what files are generated here... to add to archive...
        elif protein_a_fasta:
            # model from sequence and search for template
            if not os.path.exists("protein_a/protein_a-ccm-model_SC-min.pdb"):
                inputs.append([protein_a_fasta,'',''])
                labels.append('protein_a')
                workdirs.append('protein_a/')
                outputs.append('protein_a.pdb')
            else:
                # from hot restart
                shutil.copy("protein_a/protein_a-ccm-model_SC-min.pdb",'protein_a.pdb')
            # add hhsearch output to archive
            archive += [protein_a_fasta]
            archive += ['protein_a/*hhsearch.out','protein_a/*hhsearch.html','protein_a/*hhsearch.png','protein_a.pdb','protein_a/protein_a.hhdb']
        else:
            # we have a problem
            print("No inputs for partner A")
            cluster.progress("No inputs for partner A. Stopping here.")
            return

        protein_a = 'protein_a.pdb'

    # if not pdb for partner B, check for sequence or sequence alignment with template
    stop = False
    if not protein_b:
        if protein_b_template_pdb:
            # model from sequence/template alignment
            if job_session:
                inputs.append([protein_b_fasta,protein_b_template_pdb,job_session])
            else:
                print("Error: Cannot force a template without having a reference to a previous job id")
                cluster.progress("Cannot force a template without having a reference to a previous job id")
                sys.exit()
                # inputs.append([protein_b_fasta,protein_b_template_pdb,''])
            labels.append('protein_b')
            workdirs.append('protein_b/')
            outputs.append('protein_b.pdb')
            archive += [protein_b_template_pdb,'protein_b.pdb']
        elif protein_b_fasta:
            # model from sequence and search for template
            if not os.path.exists("protein_b/protein_b-ccm-model_SC-min.pdb"):
                inputs.append([protein_b_fasta,'',''])
                labels.append('protein_b')
                workdirs.append('protein_b/')
                outputs.append('protein_b.pdb')
            else:
                # from hot restart
                shutil.copy("protein_b/protein_b-ccm-model_SC-min.pdb",'protein_b.pdb')
            # add hhsearch output to archive
            archive += [protein_b_fasta]
            archive += ['protein_b/*hhsearch.out','protein_b/*hhsearch.html','protein_b/*hhsearch.png','protein_b.pdb','protein_b/protein_b.hhdb']

        protein_b = 'protein_b.pdb'

    collectedinfo = {}
    if workdirs:
        if len(workdirs)==1:
            # Only box partner A (or B) was filled
            print("Running comparative modeling of input sequence(s)")
            cluster.progress("Running comparative modeling of input sequence(s)")
        else:
            # Both A and B were filled
            print("0/4 Running comparative modeling of input sequence(s)")
            cluster.progress("0/4 Running comparative modeling of input sequence(s)")
        info, collectedinfo, collectedinfosmall, chosentemplates = run_dareus_oscar(inputs,labels,workdirs,outputs,min_loop_sze,max_loop_sze,max_loop_num)
        for bn in collectedinfo:
            archive += ['%s/%s.short_table_templates.txt'%(bn,bn),'%s/%s.full_table_templates.txt'%(bn,bn)]
        print("... templates used:")
        #cluster.progress("... template(s) used:")
        f_log_template=open("templates_used.txt","w")
        #for ite,line in enumerate(info):
        for bn in sorted(chosentemplates.keys()):
            if len(chosentemplates[bn].keys())==1:
                t = list(chosentemplates[bn].keys())[0]
                for k in collectedinfo[bn]:
                    if collectedinfo[bn][k][0] == t:
                        i = collectedinfo[bn][k][1]
                        c = collectedinfo[bn][k][2]
                        r = collectedinfo[bn][k][7]
                #t,i,c,r = line[:4]
                print("... template: {}, identity: {}%, coverage: {}%, resolution: {}".format(t,i,c,r))
                f_log_template.write("... template: {}, identity: {}%, coverage: {}%, resolution: {}\n".format(t,i,c,r))
                if bn=="protein_a":
                    cluster.progress("... template used for Partner A:")
                elif bn=="protein_b":
                    cluster.progress("... template used for Partner B:")
                cluster.progress("... template: {}, average identity: {}%, overall average coverage: {}%, resolution: {}".format(t,i,c,r))
                #for supp in line[4:]:
                #    # tpl_chain, proba, id, res, description
                #    print("...    >{}; HH-search Probability: {}; HH-search Identity: {}%; Resolution: {}\n                         Description: {}".format(*supp))
                #    f.write(">{}; HH-search Probability: {}; HH-search Identity: {}%; Resolution: {}\n Description: {}\n".format(*supp))
                #    # cluster.progress("...    >{}; HH-search Probability: {}; HH-search Identity: {}%; Resolution {};\n                         Description: {}".format(*supp))
                # t is the template for labels[ite]
                biounit = {}
                if not os.path.exists("%s/%s_biounit_equivalence.txt"%(bn,t)):
                    continue
                with open("%s/%s_biounit_equivalence.txt"%(bn,t),"r") as f:
                    for l in f:
                        #ASSEMBLY2PDB A -> A
                        if l[0:12] != "ASSEMBLY2PDB":
                            continue
                        biounit[l.strip().split()[3]] = l.split()[1]
                for seq in sorted(chosentemplates[bn][t].keys()):
                    chain,id = chosentemplates[bn][t][seq]
                    if chain in biounit:
                        cluster.progress("...    >sequence {} is modeled as chain {} (biounit identifier) from chain {} in PDB ({}% identity)".format(seq,biounit[chain],chain,id))
                    else:
                        cluster.progress("...    >sequence {} is modeled from chain {} in PDB ({}% identity)".format(seq,chain,id))
        f_log_template.close()
        archive.append("templates_used.txt")

    elif not (os.path.exists(protein_a) and os.path.exists(protein_b)):
        # protein_a and protein_b are the expected pdb inputs
        print("No input for modeling. Stopping here.")
        return

    if exit_after_modeling:
        print("User-selected breakpoint after modeling: stopping the run.")
        cluster.progress("User-selected breakpoint after modeling: stopping the run.")
        cluster.progress("YOU CAN NOW VISUALIZE YOUR MODELS BY CLICKING ON THE NGL VIEWER BUTTON BELOW.")
        cluster.progress("Then you can proceed to docking by copying or uploading the PDB coordinates of the models to the InterEvDock3 input page.")
        # Pack the results
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
        # Visualization of resulting PDB(s)
        cluster.progress("Finished comparative modeling")
        # format_output_for_template_based(outputs,labels,collectedinfo,collectedinfosmall)
        format_output(d_names={"TBM":outputs},labels=labels,collectedinfo=collectedinfo,collectedinfosmall=collectedinfosmall)

        # (disabled for now for debugging)
        #os.system("rm -rf README.txt protein_a protein_b")
        return

    if sum([os.path.exists(protein_a),os.path.exists(protein_b)]) == 1:
        print("No inputs for partner B, only performed comparative modeling of protein A")
        cluster.progress("No inputs for partner B, only performed comparative modeling of partner A")
        #cluster.progress("YOU CAN NOW VISUALIZE YOUR MODELS BY CLICKING ON THE NGL VIEWER BUTTON BELOW.")
        #cluster.progress("Then you can proceed to docking by copying or uploading the PDB coordinates of the models to the InterEvDock3 input page.")
        # Pack the results
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
        # Visualization of resulting PDB(s)
        cluster.progress("Finished comparative modeling")
        format_output(d_names={"TBM":outputs},labels=labels,collectedinfo=collectedinfo,collectedinfosmall=collectedinfosmall)
        # (disabled for now for debugging)
        #os.system("rm -rf README.txt protein_a protein_b")
        return

    if sum([os.path.exists(protein_a),os.path.exists(protein_b)]) == 0:
        # no model generated, still output table of templates and exit cleanly
        # Pack the results
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
        # Visualization of resulting PDB(s)
        cluster.progress("No models were generated")
        format_output(d_names={"TBM":outputs},labels=labels,collectedinfo=collectedinfo,collectedinfosmall=collectedinfosmall)
        return

    # Define the flag used to know whether we should use the contact_mpa/coevol_map pipeline specific options
    if contact_map_file:
        is_coevol_map = True
    else:
        is_coevol_map = False

    # keep a the original input with a generic name
    try:
        shutil.copy(protein_a,'protein_a_ori_input.pdb')
    except:
        pass
    try:
        shutil.copy(protein_b,'protein_b_ori_input.pdb')
    except:
        pass


    ################## STEP 1 PREPARE INPUT STRUCTURES

    protein_a_pdb_for_dock, protein_b_pdb_for_dock, chainA_output_subdir, chainB_output_subdir, chains, data_chain_subdirs = prepare_inputs_for_docking(workdir,protein_a,protein_b,dockingmethod=dockingmethod,is_coevol_map=is_coevol_map)

    ####
    # At this stage, important variables are:
    #  -> clean and monomeric reformated input pdbs for docking: protein_a_pdb_for_dock and protein_b_pdb_for_dock
    #  -> the subdirectories per pdb partner: chainA_output_subdir and chainB_output_subdir 
    #          (they have the translation tables that will be used to revert the residue and chain indexing)
    #  -> protein_a and protein_b: the original user inputs remain unchanged
    #  -> chains and data_chain_subdirs are only there to avoid performing twice the same BLAST search in the homodimer case
    ####


    ################## STEP 2 GENERATE COMSAS IF NOT ALREADY GIVEN
    reorder = True
    if dock_noconstraint:
        reorder = False
        # because we're restarting from a previous job where sequences were already reordered

    # If coevo contact map provided we want to skip coMSA generation
    # We thus add the elif to prevent entering in any of the co_ali options... Hope it has no side-effects...

    if not (alig_a and os.path.exists(alig_a) and alig_b and os.path.exists(alig_b)) and not is_coevol_map:
        alig_a = os.path.join(workdir,"protein_a_coMSA.fasta")
        alig_b = os.path.join(workdir,"protein_b_coMSA.fasta")
        cluster.progress("1/4 Generating coMSAs...")
        print("1/4 Generating coMSAs...")
        reorder = False
        num_homologs = create_coalignments_for_oligomer(workdir, data_chain_subdirs, [alig_a, alig_b], chains=chains)
        # this returns the number of homologs excluding query sequence
        if num_homologs <= 1: #== 0:
            # no sequences in MSAs except input query sequence so we'll revert to non-explicit modelling i.e. IES = 23bB
            runExplicit = False
    elif is_coevol_map:
        runExplicit = False
        cluster.progress("1/4 no need for coMSAs, skipping generation step...")
        print("1/4 no need for coMSAs, skipping generation step...")
        # alig_a and alig_b are kept empty strings
        # May be we'll have to instantiate them for compatibility with other operations in the pipeline
    else:
        num_homologs = len(tools.read_fasta(alig_a)[0])-1
        if num_homologs <= 1:
            # no sequences in MSAs except input query sequence so we'll revert to non-explicit modelling i.e. IES = 23bB
            runExplicit = False
        cluster.progress("1/4 coMSAs were provided with %i sequences, skipping generation step..."%num_homologs)
        print("1/4 coMSAs were provided with %i sequences, skipping generation step..."%num_homologs)

        # keep original input under a generic name
        try:
            shutil.copy(alig_a,os.path.join(workdir,"protein_a_coMSA.fasta"))
        except:
            pass
        try:
            shutil.copy(alig_b,os.path.join(workdir,"protein_b_coMSA.fasta"))
        except:
            pass

    alig_a_ori = alig_a # in order to keep a trace of the original alignments before reducing them
    alig_b_ori = alig_b

    if runExplicit and num_homologs > 10:
        # to run the explicit modelling scoring pipeline, we have to reduce the number of sequences to max 40 to save time
        # cut at 100, use hhfilter iteratively to reduce to about 30 sequences (max allowed: 40)
        cluster.progress("... reducing coMSAs for homology scoring")
        print("... reducing coMSAs for homology scoring")
        oReduceMSA = coMSAs.ReduceCoMSA(workdir, alig_a, alig_b, diff=30, top_N=100, max_N=40, reorder=reorder, verbose=True)
        oReduceMSA.run()
        # don't have to reorder sequences per seq id if it's our input because they're already ordered by seq id
        if seq10:
            alig_a = oReduceMSA.output_eq10A
            alig_b = oReduceMSA.output_eq10B
        else:
            alig_a = oReduceMSA.output_hhfilterA
            alig_b = oReduceMSA.output_hhfilterB


    ####
    # At this stage, important variables are:
    #  -> input or generated coMSAs: alig_a and alig_b
    ####

    cluster.progress("2/4 Docking step...")
    print("2/4 Docking step...")

    ################## STEP 3 CHECK AND FORMAT CONSTRAINTS
    ## VARIABLES
    frodock_constraints = os.path.join(workdir,"frodock_constraints.txt")   # frodock-compatible constraint file
    frodock_usr_constraints = os.path.join(workdir,"user_constraints.txt")  # user-friendly constraint output file
    frodock_tab_constraints = os.path.join(workdir,"table_constraints.txt") # InterEvDock format constraint table

    if constraints_file and os.path.exists(constraints_file):
        oCheckRes = checkConstraints.checkResConstraints(pdbA=protein_a_pdb_for_dock, pdbB=protein_b_pdb_for_dock, constraintfile=constraints_file,\
                        conversionfileA=chainA_output_subdir+"/chainA_residues_translation_table.out", conversionfileB=chainB_output_subdir+"/chainB_residues_translation_table.out", \
                        output=frodock_constraints, user_output=frodock_usr_constraints, table_output=frodock_tab_constraints, renumber=False, \
                        threshold = 1.0, default_dist_single = 8.0, default_dist_pair = 11.0)
        frodock_constraints = oCheckRes.output # will be equal to None if no constraints are left
        pass
    else:
        frodock_constraints = None

    ## VARIABLES
    frodock_cmap_seqA = os.path.join(workdir, "cmap_seqA.fasta")  # frodock-coevo seqA file
    frodock_cmap_seqB = os.path.join(workdir, "cmap_seqB.fasta")  # frodock-coevo seqB file
    frodock_cmap_coevo = os.path.join(workdir, "cmap_coevo.txt")  # frodock-coevo cmap file
    if is_coevol_map and os.path.exists(contact_map_file):
        oExtractCmap = analyzeContactMap.analyzeContactMap(contact_map_file=contact_map_file,frodock_cmap_seqA=frodock_cmap_seqA,frodock_cmap_seqB=frodock_cmap_seqB,frodock_cmap_coevo=frodock_cmap_coevo,range_index_redundancy_group=redundantContactsWindowSize)
        d_frodock_sco_cmap = oExtractCmap.output # dic which contains the path to the useful files for frodock_sco : 'path_sequence_A', 'path_sequence_B', 'path_cmap'
        nr_decoys = NdecoyCovariationMap
        archive+=[os.path.basename(frodock_cmap_seqA),os.path.basename(frodock_cmap_seqB),os.path.basename(frodock_cmap_coevo)]
    else:
        d_frodock_sco_cmap = None

    ################## STEP 4 PERFORM DOCKING
    try:
        oDock = docking.FrodockRigidBodyDocking(protein_a=protein_a_pdb_for_dock, protein_b=protein_b_pdb_for_dock, \
                                    alignment_a=alig_a, alignment_b=alig_b, \
                                    workdir=workdir, constraints=frodock_constraints, d_coevol_cmap=d_frodock_sco_cmap, nodes=nodes, \
                                    cpus=cpus, runISC=runISC, dockdat=dock_noconstraint, \
                                    runExplicitHomology=runExplicit, frodock_version=frodock_version, \
                                    topN=nr_decoys,dthresh_contact=dthresh_contact, frodock_type_option=frodock_type_option)
    except Exception as e:
        cluster.progress(" ... docking failed due to exception %s"%e)
        print(" ... docking failed due to exception %s"%e)
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)

    cluster.progress("3/4 Formating results")
    print("3/4 Formating results")

    ################## STEP 5 COLLECT PDB PATHS, NAMES and SCOREFILE NAMES FOR OUTPUT PROCESSING
    # # 
    # methods=[]
    # scorefiles=[]
    # names=[]
    # paths=[]
    # if runExplicit:
    #     methods = ['IESh','SPPh','FRODOCK2.1']
    #     scorefiles = [os.path.join(workdir,oDock.IEShscores),os.path.join(workdir,oDock.SPPhscores),os.path.join(workdir,oDock.FDscores)]
    #     names = [oDock.top50IEShnames,oDock.top50SPPhnames,oDock.top50FDnames]
    #     paths = [os.path.relpath(oDock.top50IEShdir),os.path.relpath(oDock.top50SPPhdir),os.path.relpath(oDock.top50FDdir)]
    #     if runISC:
    #         methods += ['ISCh','ISC']
    #         scorefiles += [os.path.join(workdir,oDock.ISChscores),os.path.join(workdir,oDock.ISCscores)]
    #         names += [oDock.top50ISChnames,oDock.top50ISCnames]
    #         paths += [os.path.relpath(oDock.top50ISChdir),os.path.relpath(oDock.top50ISCdir)]
    # if d_frodock_sco_cmap:
    #     methods = ['CMAPnb', 'CMAPsco']
    #     scorefiles = [os.path.join(workdir,oDock.CMAPnbscores),os.path.join(workdir,oDock.CMAPscoscores)]
    #     names = [oDock.top10CMAPnbnames,oDock.top10CMAPsconames]
    #     paths = [os.path.relpath(oDock.top10CMAPnbdir),os.path.relpath(oDock.top10CMAPscodir)]
    # else:
    #     methods = ['IES','SPP','FRODOCK2.1']
    #     scorefiles = [os.path.join(workdir,oDock.IESscores),os.path.join(workdir,oDock.SPPscores),os.path.join(workdir,oDock.FDscores)]
    #     names = [oDock.top50IESnames,oDock.top50SPPnames,oDock.top50FDnames]
    #     paths = [os.path.relpath(oDock.top50IESdir),os.path.relpath(oDock.top50SPPdir),os.path.relpath(oDock.top50FDdir)]
    #     if runISC:
    #         methods += ['ISC']
    #         scorefiles += [os.path.join(workdir,oDock.ISCscores)]
    #         names += [oDock.top50ISCnames]
    #         paths += [os.path.relpath(oDock.top50ISCdir)]
    # centralise the paths to the various score files
    d_paths      = {'SPPh':os.path.relpath(oDock.top50SPPhdir),'SPP':os.path.relpath(oDock.top50SPPdir),
                    'IESh':os.path.relpath(oDock.top50IEShdir),'IES':os.path.relpath(oDock.top50IESdir),
                    'ISCh':os.path.relpath(oDock.top50ISChdir),'ISC':os.path.relpath(oDock.top50ISCdir),
                    'FRODOCK2.1':os.path.relpath(oDock.top50FDdir),
                    'CMAPnb':os.path.relpath(oDock.top10CMAPnbdir),'CMAPsco':os.path.relpath(oDock.top10CMAPscodir)}
    d_scorefiles = {'SPPh':os.path.join(workdir,oDock.SPPhscores),'SPP':os.path.join(workdir,oDock.SPPscores),
                    'IESh':os.path.join(workdir,oDock.IEShscores),'IES':os.path.join(workdir,oDock.IESscores),
                    'ISCh':os.path.join(workdir,oDock.ISChscores),'ISC':os.path.join(workdir,oDock.ISCscores),
                    'FRODOCK2.1':os.path.join(workdir,oDock.FDscores),
                    'CMAPnb':os.path.join(workdir,oDock.CMAPnbscores),'CMAPsco':os.path.join(workdir,oDock.CMAPscoscores)}
    d_names      = {'SPPh':oDock.top50SPPhnames,'SPP':oDock.top50SPPnames,
                    'IESh':oDock.top50IEShnames,'IES':oDock.top50IESnames,
                    'ISCh':oDock.top50ISChnames,'ISC':oDock.top50ISCnames,
                    'FRODOCK2.1':oDock.top50FDnames,
                    'CMAPnb':oDock.top10CMAPnbnames,'CMAPsco':oDock.top10CMAPsconames}

    # take note of the methods that were used in this run
    methods=[]
    if d_frodock_sco_cmap:
        methods = ['CMAPnb', 'CMAPsco']
    elif runExplicit:
        methods = ['IESh','SPPh','FRODOCK2.1']
        if runISC:
            methods += ['ISCh','ISC']
    else:
        methods = ['IES','SPP','FRODOCK2.1']
        if runISC:
            methods += ['ISC']


    os.chdir(workdir) # make sure we're in workdir


    ################## STEP 6 RUN MINIMISATION ON FINAL OUTPUT STRUCTURES (TOP 50 OF EACH SCORE)
    # Analyse conservation with complete alignments and add conservation index to b factor column of all existing top 50 directories
    if not d_frodock_sco_cmap and runMini:
        cluster.progress(" ... minimising best structures")
        print(" ... minimising best structures")
        run_final_minimization(docking=True)


    ################## STEP 7 PERFORM CONSERVATION ANALYSIS BASED ON COMPLETE COMSAS
    # Analyse conservation with complete alignments and add conservation index to b factor column of all existing top 50 directories
    if not d_frodock_sco_cmap:
        cluster.progress(" ... assessing conservation with rate4site")
        print(" ... assessing conservation with rate4site")
        oCons = mapconservation.AnalyseConservation([d_paths[m] for m in methods],[alig_a,alig_b],['A','B']) # CQ: use reduced MSAs for R4S to save time instead of [alig_a_ori,alig_b_ori]


    ################## STEP 8 PUT ALL RESULTS TOGETHER AND FORMAT
    cluster.progress(" ... formatting visualization")
    print(" ... formatting visualization")

    # if d_frodock_sco_cmap:
    #     format_output(methods,scorefiles,names,paths,wdir=workdir)
    # else:
    #     format_output([], [], [], [], wdir=workdir, top10IED=lsttop10, labels=labels, collectedinfo=collectedinfo)

    if constraints_file and os.path.exists(constraints_file):
        format_constraints(frodock_tab_constraints)

    # copy inputs
    os.system('cp %s %s .' % (protein_a,protein_b))

    # copy visu scripts
    if not d_frodock_sco_cmap:
        pymol_script = 'scripts/_analysis_top30.pml'
        os.system('cp -r %s/* .' % (PYMOL_SCRIPTS_DIR))
    else:
        pymol_script = 'scripts/_analysis_coevomap_top10.pml'
        os.system('cp -r %s/* .' % (PYMOL_SCRIPTS_DIR))



    # In case of oligomer docking -> convert back chains, residue indexes
    # In some cases chain names will have to be changed in the docked models with respect to original if input A and B had the same chains in the input.
    translation_file_for_A = os.path.join(chainA_output_subdir,'chainA_residues_translation_table.out')
    translation_file_for_B = os.path.join(chainB_output_subdir,'chainB_residues_translation_table.out')

    cluster.progress(" ... packing the results")
    print(" ... packing the results")
    
    oPrepPDB = prepPDBs.PrepPDBs()
    if d_frodock_sco_cmap:
        oPrepPDB.Mono2Oligo(translation_file_for_A, translation_file_for_B, [d_paths[m] for m in methods], None, pymol_script, workdir, dockingmethod=dockingmethod, is_coevol_map=is_coevol_map, d_coevol_map=d_frodock_sco_cmap)
        # Pack the results
        archive += [os.path.basename(protein_a), os.path.basename(protein_b),'start_analysis_cmap.pml', 'scripts']
        archive += [os.path.relpath(d_paths[m]) for m in methods]
        archive += [os.path.relpath(d_scorefiles[m]) for m in methods]
        # There are two dat files because they were created at the clustering step
        archive.append(os.path.basename(oDock.dockclustdat_cmapnb))
        archive.append(os.path.basename(oDock.dockclustdat_cmapsco))
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
    
    else:        
        oPrepPDB.Mono2Oligo(translation_file_for_A, translation_file_for_B, [d_paths[m] for m in methods], oDock.top10cons_res_txt, pymol_script, workdir)
        # Pack the results
        archive += [os.path.basename(protein_a), os.path.basename(protein_b), os.path.basename(alig_a_ori), os.path.basename(alig_b_ori), os.path.basename(oDock.top10cons_struct_txt), os.path.basename(oDock.top10cons_res_txt), 'start_analysis.pml', 'scripts']
        archive += [os.path.relpath(d_paths[m]) for m in methods]+['Consensus_models/']
        archive += [os.path.relpath(d_scorefiles[m]) for m in methods]
        archive.append(os.path.basename(oDock.dockclustdat))
        if constraints_file:
            if os.path.exists('frodock_constraints.txt'):
                archive.append('frodock_constraints.txt')
            if os.path.exists('user_constraints.txt'):
                archive.append('user_constraints.txt')
        os.system('tar -czf results.tar.gz %s' % ' '.join(archive))
    
    # Cleanup
    # (disabled for now for debugging)
    #os.system("rm -rf scripts start_analysis.pml README.txt decoys* oscar* regrouped_MSA*.fasta regrouped_INPUTSEQ*.fasta *UP.pbl NR_org*fasta seq*.fst seq*_UP_alignhits.fasta combined_MSA.fasta filtered_combined_MSA.fasta final_MSA*.fasta")

    # Generate HTML page with PDB viewer and best models
    format_output(methods=methods, d_scorefiles=d_scorefiles, d_names=d_names, d_paths=d_paths, wdir=workdir, IEDfile=oDock.top10cons_struct_txt if oDock.top10cons_struct_txt else None, labels=labels, collectedinfo=collectedinfo, collectedinfosmall=collectedinfosmall)

    cluster.progress("You can now visualize your models and download the output files below.")
    if not d_frodock_sco_cmap:
        cluster.progress("If not already done, you can minimize your structures with Gromacs with the hot restart function of InterEvDock3 by specifying this job id, the same options as this job and by additionnaly activating the minimization option")
    cluster.progress("4/4 done")
    print("4/4 done")
    os.chdir(curdir)


def cmdLine():

    USAGE = "A generic pipeline for docking 2 PDB files containing one or multiple chains, with or without input co-MSAs\n"+\
            "1. simple case:\n"+\
            "InterEvDock3.py -R protein_a.pdb -L protein_b.pdb -d workdir\n"
    
    parser = optparse.OptionParser(usage = USAGE)

    
    parser.add_option('-d','--dir',
                      action="store",
                      dest='workdir',
                      type='string',
                      help="Docking work directory",
                      default=None)

    parser.add_option('--mobyle_job_id',
                      action="store",
                      dest='mobyle_job_id',
                      help="Mobyle job id to fetch frodock docking file from. Used for hotstarting", 
                      default=None)


    # Protein inputs 
    parser.add_option('-R','--protein_a_pdb',
                      action="store",
                      dest='protein_a_pdb',
                      type='string',
                      help="Protein A PDB file path",
                      default=None)

    parser.add_option('--protein_a_sequence',
                      action="store",
                      dest='protein_a_fasta',
                      help="Fasta file of protein A sequence",
                      default=None)
    
    parser.add_option('-L','--protein_b_pdb',
                      action="store",
                      dest='protein_b_pdb',
                      type='string',
                      help="Protein B PDB file path. None by default meaning the program predicts homodimeric interface of protein A PDB",
                      default=None)

    parser.add_option('--protein_b_sequence',
                      action="store",
                      dest='protein_b_fasta',
                      help="Fasta file of protein B sequence",
                      default=None)
    

    # CCM options
    parser.add_option('--templatePDB_a',
                      action="store",
                      dest='protein_a_template_pdb',
                      help="PDB code of protein A template",
                      default=None)

    parser.add_option('--templatePDB_b',
                      action="store",
                      dest='protein_b_template_pdb',
                      help="PDB code of protein B template",
                      default=None)

    parser.add_option('--loop',
                       action="store",
                       dest='loop',
                       type='int',
                       help="maximum number of residues that can be modeled in a loop",
                       default=50)

    parser.add_option('--Nter',
                       action="store",
                       dest='Nter',
                       type='int',
                       help="maximum number of residues to model as Nter tail",
                       default=0)

    parser.add_option('--Cter',                       
                       action="store",                       
                       dest='Cter',                       
                       type='int',                       
                       help="maximum number of residues to model as Cter tail",                       
                       default=0)     


    # Docking parameters
    parser.add_option('-D','--nr_decoys',
                      action="store",
                      dest='nr_decoys',
                      type='int', 
                      help="Number of decoys to generate for single run",
                      default = 10000)

    parser.add_option('-r','--protein_a_msa',
                      action="store",
                      dest='protein_a_msa',
                      help="Custom protein A MSA for IES analysis (implies -N flag is set).")
    
    parser.add_option('-l','--protein_b_msa',
                      action="store",
                      dest='protein_b_msa',
                      help="Custom protein B MSA for IES analysis (implies -N flag is set).")
        
    parser.add_option('-F','--frodock',
                      action="store_true",
                      dest='use_frodock',
                      help="use FRODOCK instead of ZDOCK",
                      default=False)

    parser.add_option('--frodock_version',
                      action="store",
                      dest='frodock_version',
                      type='float', 
                      help="Frodock version",
                      default = 2.1)    

    parser.add_option('-I','--runISC',
                      action="store_true",
                      dest='runISC',
                      help="use Rosetta Interface Score",
                      default=False)
    
    parser.add_option('-H','--runExplicitHomology',
                      action="store_true",
                      dest='runExplicit',
                      help="use explicit homology scoring pipeline",
                      default=False)
 
    parser.add_option('-C', '--constraint_file',
                      action="store",
                      dest='constraint_file',
                      help="File containing constraints. Each line is a pair of residues like 11A:20B", 
                      default=None)

    parser.add_option('--dock_noconstraint_dat',
                      action="store",
                      dest='dock_noconstraint_dat',
                      help="Docking file output by Frodock (before any constraint filtering or clustering). Used for hotstarting", 
                      default=None)

    parser.add_option('-S','--short',
                       action="store_true",
                       dest='seq10',
                       help="If you want to reduce the MSAs to 10 sequences instead of 40 in homology scoring",
                       default=False) 

    parser.add_option('--frodock_type_option',
                      action="store",
                      type='string',
                      dest='frodock_type_option',
                      help="Protein type for frodock docking. Should be O (Other), A (Antibody-Antigen) or E (Enzyme-substrate)", 
                      default="O")


    # coevol options
    parser.add_option('--distanceCoevoThresh',
                      action="store",
                      type="float",
                      dest='distanceCoevoThresh',
                      help="To modify the distance threshold to count contacts between heavy atoms in coevolution-map analysis (default : 8A)",
                      default=8)

    parser.add_option('--redundantContactsWindowSize',
                      action="store",
                      type="int",
                      dest='redundantContactsWindowSize',
                      help="To modify the window size to group contacts between neighbouring residues (default : 2)",
                      default=2)

    parser.add_option('-M', '--contact_map',
                      action="store",
                      dest='contact_map_file',
                      help="File containing contact_map. Each line is a coupling between residues with format <ires1> <ires2> <coupling_val> <redundancy_group> <highest_coupling_val_in_grp>."
                           "Format can also be limited as <ires1> <ires2> <coupling_val> and the 5-cols will be generated subsequently"
                           "On top of the file sequences of the input can be added as a fasta format (4 lines)",
                      default=None)

    parser.add_option('--NdecoyCovariationMap',
                      action="store",
                      type="int",
                      dest='NdecoyCovariationMap',
                      help="number of decoys to take into account in the coevol map mode",
                      default=300000)


    # breakpoints
    parser.add_option('--exit_after_template_search',
                      action="store_true",
                      dest='exit_after_template_search',
                      help="Exit after template search to visualize HHsearch results (when box is ticked in advanced options)",
                      default=False)

    parser.add_option('--exit_after_modeling',
                      action="store_true",
                      dest='exit_after_modeling',
                      help="Exit after modeling to visualize models (when box is ticked in advanced options)",
                      default=False)


    # run minimisation
    parser.add_option('--minimize_output',
                      action="store",
                      dest='runMini',
                      help="Run minimisation with gromacs on all top 50 output models at the end of the docking pipeline",
                      default='False')

    parser.add_option('--minimize_restart_top50',
                      action="store",
                      dest='runMini_top50',
                      help="Run minimisation with gromacs on all top 50 output models from a previous job id",
                      default='False')


    # useless options ...
    parser.add_option('-N','--no_orthology_analysis',
                       action="store_true",
                       dest='no_orthology_analysis',
                       help="If you want use own MSA for IES analysis and skip the orthology analysis, name the MSA(s) to: protein_a.alig_headersp.fst (and protein_b.alig_headersp.fst). The first case is for prediction of homodimeric interface (for protein_a), while the second is for heterodimeric interaction prediction.",
                       default=False) 

    parser.add_option('--protein_a_alignment',
                      action="store",
                      dest='protein_a_alignment',
                      help="Alignment file of protein A sequence & template",
                      default=None)

    parser.add_option('--protein_b_alignment',
                      action="store",
                      dest='protein_b_alignment',
                      help="Alignment file of protein B sequence & template",
                      default=None)
   
    parser.add_option('-T','--runTemplateBased',
                      action="store_true",
                      dest='runTBD',
                      help="use template based docking pipeline",
                      default=False)

    return parser


def checkArgValues(options):

    status = True

    options.runMini = (options.runMini == "True")
    options.runMini_top50 = (options.runMini_top50 == "True")

    def readargs(lst):
        option_args_bool = ["--runExplicitHomology","--runISC","--runTemplateBased","--short"]
        option_args_val = ["--redundantContactsWindowSize","--distanceCoevoThresh"]
        i=0
        listed_args=[0,0,0,0,0,0]
        while i < len(lst):
            if lst[i].startswith("-"):
                #option
                if lst[i] in option_args_bool:
                    listed_args[option_args_bool.index(lst[i])]=1
                if lst[i] in option_args_val:
                    idx=option_args_val.index(lst[i])+4
                    i+=1
                    listed_args[idx] = float(lst[i])
            i+=1
        return listed_args

    if options.runMini and options.mobyle_job_id:
        print("mini top50")
        print(os.path.join(DFLT_CLUSTER_JOBS_PATH, options.mobyle_job_id))
        sys.stdout.flush()
        if re.match(r"^[A-Z]{1}\d{14}$", options.mobyle_job_id) and os.path.isdir(os.path.join(DFLT_CLUSTER_JOBS_PATH, options.mobyle_job_id)):
            mobyle_job_dir = os.path.join(DFLT_CLUSTER_JOBS_PATH, options.mobyle_job_id)
            if not (options.protein_a_template_pdb or options.protein_b_template_pdb) and \
               not (options.protein_a_msa or options.protein_b_msa) and \
               not (options.constraint_file or options.contact_map_file):
                try:
                    f=open("%s/.command"%mobyle_job_dir)
                    line=f.read().split()
                    f.close()
                    print(line)
                    args_oldjob = readargs(line)
                    args_thisjob = readargs(sys.argv)
                    print(args_oldjob)
                    print(args_thisjob)
                    if args_oldjob == args_thisjob:
                        options.runMini_top50 = True
                except Exception as e:
                    print(e)
                    pass

    if options.workdir is None:
        options.workdir = os.getcwd()
    else:
        try:
            os.makedirs(options.workdir)
        except:
            pass

    # generic names
    protein_a_pdb = "%s/protein_a.pdb"%options.workdir
    protein_b_pdb = "%s/protein_b.pdb"%options.workdir
    protein_a_msa = "%s/protein_a_coMSA.fasta"%options.workdir
    protein_b_msa = "%s/protein_b_coMSA.fasta"%options.workdir

    # Option for hotstart from a dock_noconstraint.dat file or hotrestart for modelling with different template
    if options.mobyle_job_id is not None:
        if not re.match(r"^[A-Z]{1}\d{14}$", options.mobyle_job_id):
            cluster.progress("Invalid session Id for hot restart : %s" % options.mobyle_job_id)
            print("Invalid session Id for hot restart : %s" % options.mobyle_job_id)
            status = False
        elif not os.path.isdir(os.path.join(DFLT_CLUSTER_JOBS_PATH, options.mobyle_job_id)):
            cluster.progress("Session %s does not exist. Cannot perform hot restart." % options.mobyle_job_id)
            print("Session %s does not exist. Cannot perform hot restart." % options.mobyle_job_id)
            status = False
        else:
            mobyle_job_dir = os.path.join(DFLT_CLUSTER_JOBS_PATH, options.mobyle_job_id)
            if not os.path.isfile("%s/dock.dat" % mobyle_job_dir):
                if not options.protein_a_template_pdb and not options.protein_b_template_pdb:
                    cluster.progress("Could not retrieve Frodock docking file from session %s." % options.mobyle_job_id)
                    if options.protein_a_fasta and os.path.isfile("%s/protein_a/protein_a-ccm-model_SC-min.pdb" % mobyle_job_dir):
                        os.mkdir('%s/protein_a'%options.workdir)
                        shutil.copy2("%s/protein_a/protein_a-ccm-model_SC-min.pdb" % mobyle_job_dir,"%s/protein_a/protein_a-ccm-model_SC-min.pdb"%options.workdir)
                    if options.protein_b_fasta and os.path.isfile("%s/protein_b/protein_b-ccm-model_SC-min.pdb" % mobyle_job_dir):
                        os.mkdir('protein_b')
                        shutil.copy2("%s/protein_b/protein_b-ccm-model_SC-min.pdb" % mobyle_job_dir,"%s/protein_b/protein_b-ccm-model_SC-min.pdb"%options.workdir)
                    if not (options.protein_a_fasta and os.path.isfile("%s/protein_a/protein_a-ccm-model_SC-min.pdb" % mobyle_job_dir)) and not (options.protein_b_fasta and os.path.isfile("%s/protein_b/protein_b-ccm-model_SC-min.pdb" % mobyle_job_dir)):
                        print("Could not retrieve Frodock docking file from session %s." % options.mobyle_job_id)
                        status = False

                else:
                    # we're re-running this to choose a different template

                    # check template input format is correct
                    if options.protein_a_template_pdb and not re.match(r"^\w{4}$",options.protein_a_template_pdb):
                        print("Template id for Partner A not recognised {}".format(options.protein_a_template_pdb))
                        cluster.progress("Template id for Partner A not recognised {}. Please enter a 4-letter code".format(options.protein_a_template_pdb))
                        sys.exit()
                    elif options.protein_a_template_pdb:
                        options.protein_a_template_pdb = options.protein_a_template_pdb.upper()
                    if options.protein_b_template_pdb and not re.match(r"^\w{4}$",options.protein_b_template_pdb):
                        print("Template id for Partner B not recognised {}".format(options.protein_b_template_pdb))
                        cluster.progress("Template id for Partner B not recognised {}. Please enter a 4-letter code".format(options.protein_b_template_pdb))
                        sys.exit()
                    elif options.protein_b_template_pdb:
                        options.protein_b_template_pdb = options.protein_b_template_pdb.upper()

                    if not os.path.isfile("%s/protein_a_sequence.data" % mobyle_job_dir) and not os.path.isfile("%s/protein_b_sequence.data" % mobyle_job_dir):
                        cluster.progress("Could not retrieve sequence input file from session %s." % options.mobyle_job_id)
                    else:
                        if os.path.isfile("%s/protein_a_sequence.data" % mobyle_job_dir):
                            os.mkdir("%s/protein_a"%options.workdir)
                            os.system("cp %s/protein_a_sequence.data %s/protein_a/protein_a.fst"% (mobyle_job_dir,options.workdir))
                            options.protein_a_fasta = '%s/protein_a/protein_a.fst'%options.workdir
                        if os.path.isfile("%s/protein_b_sequence.data" % mobyle_job_dir):
                            os.mkdir("%s/protein_b"%options.workdir)
                            os.system("cp %s/protein_b_sequence.data %s/protein_b/protein_b.fst"% (mobyle_job_dir,options.workdir))
                            options.protein_b_fasta = '%s/protein_b/protein_b.fst'%options.workdir
                    cluster.progress("User provided job id {} and template(s) {}. Will re-run template-based modeling from previous job.".format(options.mobyle_job_id," ".join([p for p in [options.protein_a_template_pdb,options.protein_b_template_pdb] if p])))

            else:
                # Hot restart is working 

                if options.runMini_top50:
                    # we're not re-running the scoring, just the minimisation on the top 50 models of a previous job
                    # we need a copy of the dock.dat, input proteins, model folders, score files and consensus files
                    cluster.progress("User-provided session id %s: Minimising the final output models from previous job." % options.mobyle_job_id)
                    print("User-provided session id %s: Minimising the final output models from previous job." % options.mobyle_job_id)
                    try:
                        shutil.copy2("%s/dock.dat" % mobyle_job_dir, "%s/dock.dat"%options.workdir)
                        shutil.copy2("%s/dockclust.dat" % mobyle_job_dir, "%s/dockclust.dat"%options.workdir)
                        # retrieve PDB from previous job
                        shutil.copy2("%s/protein_a_ori_input.pdb" % mobyle_job_dir, "%s/protein_a.pdb"%options.workdir)
                        options.protein_a_pdb = "%s/protein_a.pdb"%options.workdir
                        # retrieve PDB from previous job
                        options.protein_b_pdb = "%s/protein_b.pdb"%options.workdir
                        shutil.copy2("%s/protein_b_ori_input.pdb" % mobyle_job_dir, "%s/protein_b.pdb"%options.workdir)
                        os.system("cp -r %s/*_models %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp %s/score*txt %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp -rp %s/scripts %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp %s/start_analysis* %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp %s/interevdock*html %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp %s/InterEvDock_Top*.txt %s/" % (mobyle_job_dir,options.workdir))
                        os.system("cp %s/consensus*.txt %s/" % (mobyle_job_dir,options.workdir))
                    except Exception as e:
                        print(e)
                        print("Error in minimisation hot-restart: Could not copy all relevant files from previous job.")
                        cluster.progress("Error: Could not copy all relevant files from previous job.")
                        sys.exit()

                else:
                    # retrieve docking file as well as PDB files and coMSAs from previous job
                    cluster.progress("User-provided session id %s: restarting from the docking result of a previous job." % options.mobyle_job_id)
                    print("User-provided session id %s: restarting from the docking result of a previous job." % options.mobyle_job_id)
                    shutil.copy2("%s/dock.dat" % mobyle_job_dir, "%s/dock.dat"%options.workdir)
                    options.dock_noconstraint_dat = '%s/dock.dat'%options.workdir
                    if options.protein_a_pdb is None:
                        if os.path.isfile("%s/protein_a_ori_input.pdb" % mobyle_job_dir):
                            # retrieve PDB from previous job
                            options.protein_a_pdb = "%s/protein_a_ori_input.pdb" % mobyle_job_dir
                        else:
                            cluster.progress("No protein A pdb structure provided although a dock.dat file (FRODOCK2 docking output) from session %s was provided: cannot continue." % options.mobyle_job_id)
                            print("No protein A pdb structure provided although a dock.dat file (FRODOCK2 docking output) from session %s was provided: cannot continue." % options.mobyle_job_id)
                            status = False
                    if options.protein_b_pdb is None:
                        if os.path.isfile("%s/protein_b_ori_input.pdb" % mobyle_job_dir):
                            # retrieve PDB from previous job
                            options.protein_b_pdb = "%s/protein_b_ori_input.pdb" % mobyle_job_dir
                        else:
                            cluster.progress("No protein B pdb structure provided although a dock.dat file (FRODOCK2 docking output) from session %s was provided: cannot continue." % options.mobyle_job_id)
                            print("No protein B pdb structure provided although a dock.dat file (FRODOCK2 docking output) from session %s was provided: cannot continue." % options.mobyle_job_id)
                            status = False

                    # Retrieve previously computed coMSAs unless they are provided by the user
                    if options.protein_a_msa is None and os.path.isfile(os.path.join(mobyle_job_dir,os.path.basename(protein_a_msa))):
                        options.protein_a_msa = os.path.join(mobyle_job_dir,os.path.basename(protein_a_msa))
                    if options.protein_b_msa is None and os.path.isfile(os.path.join(mobyle_job_dir,os.path.basename(protein_b_msa))):
                        options.protein_b_msa = os.path.join(mobyle_job_dir,os.path.basename(protein_b_msa))

    else:
        if options.protein_a_pdb is not None:
            if os.path.abspath(protein_a_pdb) != os.path.abspath(options.protein_a_pdb):
                try:
                    shutil.copy2(options.protein_a_pdb, protein_a_pdb)
                except: pass
            options.protein_a_pdb = protein_a_pdb
        elif options.protein_a_fasta is not None:
            cluster.progress("No protein A pdb structure provided: will build structure from partner A sequence instead.")
            os.mkdir("%s/protein_a"%options.workdir)
            shutil.copy2(options.protein_a_fasta, '%s/protein_a/protein_a.fst'%options.workdir)
            options.protein_a_fasta = '%s/protein_a/protein_a.fst'%options.workdir
        elif options.protein_a_alignment:
            # in case the query-template alignment is provided, get fasta sequence as first sequence (query) in alignment
            os.mkdir("%s/protein_a"%options.workdir)
            foundFasta = tools.get_fasta_from_ali(options.protein_a_alignment, '%s/protein_a/protein_a.fst'%options.workdir)
            if not foundFasta:
                cluster.progress("Query sequence could not be automatically detected from query-template alignment for partner A: cannot continue.")
            else:
                options.protein_a_fasta = '%s/protein_a/protein_a.fst'%options.workdir
        else:
            cluster.progress("No fasta sequence provided for partner A: cannot continue.")
            print("No input protein!")
            status = False
    
        if options.protein_b_pdb is not None:
            if os.path.abspath(protein_b_pdb) != os.path.abspath(options.protein_b_pdb):
                try:
                    shutil.copy2(options.protein_b_pdb, protein_b_pdb)
                except: pass
            options.protein_b_pdb = protein_b_pdb
        elif options.protein_b_fasta is not None:
            cluster.progress("No protein B pdb structure provided: will build structure from partner B sequence instead.")
            os.mkdir("%s/protein_b"%options.workdir)
            shutil.copy2(options.protein_b_fasta, '%s/protein_b/protein_b.fst'%options.workdir)
            options.protein_b_fasta = '%s/protein_b/protein_b.fst'%options.workdir
        elif options.protein_b_alignment:
            # in case the query-template alignment is provided, get fasta sequence as first sequence (query) in alignment
            os.mkdir("%s/protein_b"%options.workdir)
            foundFasta = tools.get_fasta_from_ali(options.protein_b_alignment, '%s/protein_b/protein_b.fst'%options.workdir)
            if not foundFasta:
                cluster.progress("Query sequence could not be automatically detected from query-template alignment for partner B: cannot continue.")
            else:
                options.protein_b_fasta = '%s/protein_b/protein_b.fst'%options.workdir
        elif options.protein_a_fasta:
            cluster.progress("No input for partner B provided, will only perform comparative modeling of protein A.")
            print("No input for partner B provided, will only perform comparative modeling of protein A.")
        else:
            cluster.progress("No fasta sequence provided for partner A and B: cannot continue.")
            print("No input proteins!")
            status = False

    if options.constraint_file is not None:
        constraint_file = "%s/constraint_list.data"%options.workdir
        if os.path.abspath(options.constraint_file) != os.path.abspath(constraint_file):
            shutil.copy2(options.constraint_file, constraint_file)
            options.constraint_file = os.path.abspath(constraint_file)

    if options.contact_map_file is not None:
        contact_map_file = "%s/coevol_map.input"%options.workdir
        if os.path.abspath(options.contact_map_file) != os.path.abspath(contact_map_file):
            shutil.copy2(options.contact_map_file, contact_map_file)
            options.contact_map_file = os.path.abspath(contact_map_file)


    # Check multifasta sizes for -N
    if options.protein_a_msa is not None and options.protein_b_msa is not None:
        # Copy custom files
        try:
            shutil.copy2(options.protein_a_msa, protein_a_msa)
        except: pass # Ok if the files are exactly the same
        try:
            shutil.copy2(options.protein_b_msa, protein_b_msa)
        except: pass # Ok if the files are exactly the same
        if len(tools.read_fasta(protein_a_msa)[0]) < 10 or len(tools.read_fasta(protein_b_msa)[0]) < 10:
            cluster.progress('Warning: There are not enough sequences in the co-alignments. InterEvScore is dedicated to cases with at least 10 sequences in alignments.')
            print('Warning: There are not enough sequences in the co-alignments. InterEvScore is dedicated to cases with at least 10 sequences in alignments.')

    options.protein_a_msa = protein_a_msa
    options.protein_b_msa = protein_b_msa

    if options.frodock_type_option not in "AOE":
        cluster.progress("Unrecognized protein type %s. Setting to 'Other' by default."%options.frodock_type_option)
        options.frodock_type_option="O"

    if not status:
        sys.exit(0)


if __name__=="__main__":

    cluster.progress("Starting job: "+datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    parser = cmdLine()
    (options, args) = parser.parse_args(sys.argv)
    print(options.__dict__)
    checkArgValues(options)

    if options.use_frodock:
        dockingmethod = 'frodock'
    else:
        dockingmethod = None

    ## CQ 26/03/21: force max loop size to 50 while we're waiting for Julien to update the default value in the IED3 form
    #options.loop = 50

    main(workdir=options.workdir,protein_a=options.protein_a_pdb,protein_b=options.protein_b_pdb,
        alig_a=options.protein_a_msa,alig_b=options.protein_b_msa,
        constraints_file=options.constraint_file,
        contact_map_file=options.contact_map_file,
        runISC=options.runISC,runExplicit=options.runExplicit,
        runTBD=options.runTBD,
        dock_noconstraint=options.dock_noconstraint_dat,
        nr_decoys=options.nr_decoys,seq10=options.seq10,
        frodock_version=options.frodock_version,dockingmethod=dockingmethod,
        protein_a_fasta=options.protein_a_fasta,protein_b_fasta=options.protein_b_fasta,
        protein_a_alignment=options.protein_a_alignment,protein_a_template_pdb=options.protein_a_template_pdb,
        protein_b_alignment=options.protein_b_alignment,protein_b_template_pdb=options.protein_b_template_pdb,
        exit_after_template_search=options.exit_after_template_search,
        exit_after_modeling=options.exit_after_modeling,runMini=options.runMini,job_session=options.mobyle_job_id,
        runMiniTop50=options.runMini_top50,NdecoyCovariationMap=options.NdecoyCovariationMap,
        dthresh_contact=options.distanceCoevoThresh,redundantContactsWindowSize=options.redundantContactsWindowSize,
        frodock_type_option=options.frodock_type_option,max_loop_sze=options.loop)
