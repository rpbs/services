#!/usr/bin/env python

import sys, os, re, time, fcntl
import shutil, random, glob, subprocess, itertools
from multiprocessing import Queue, Process, cpu_count

import Fasta.Fasta as Fasta
import PyPDB.PyPDB as PDB
import drmaa
import cluster

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('Wrapper_HHkbestModQmean','templates'))

from optparse import OptionParser
from optparse import OptionGroup
from optparse import IndentedHelpFormatter
import textwrap


PROG           = "Wrapper_HHkbestModQmean.py"
DEFAULT_WDIR   = "."
DEFAULT_KBEST  = 500 #50 #500
DEFAULT_DAG    = "autodag"
DEFAULT_MAXMEM = 15
DEFAULT_MODELS1 = 20 #2 #20
DEFAULT_MODELS2 = 50 #5 #50
DEFAULT_MAXBEST = 32
DEFAULT_MAXMODEL = 50
DEFAULT_CPU_PER_NODE = 12
DEFAULT_MAX_RUNNING = 200
DEFAULT_DOMFINDER_CPUS = 8
DEFAULT_LOCAL_JOB = False
MODELLER_LKEY  = "MODELIRANJE"

OUT            = "reshhk"
TARCH          = "template_PDB.tar.gz"
QJAL_ARCH      = "qjalarch.zip"
QBESTPIR       = "qbest_ali.pir"
QBESTPIR5      = "qbest_ali5.pir"
QBESTTXT       = "Qmean_best_models.txt"
QBESTALI       = "Qmean_best_ali.txt"


def cmdLine():
    
    class MyFormatter(IndentedHelpFormatter):
        def format_epilog(self, epilog):
                return "\n" + epilog + "\n\n"
        
        def format_option(self, option):
            result = []
            opts = self.option_strings[option]
            opt_width = self.help_position - self.current_indent - 2
            if len(opts) > opt_width:
                opts = "%*s%s\n" % (self.current_indent, "", opts)
                indent_first = self.help_position
            else:                       
                opts = "%*s%-*s  " % (self.current_indent, "", \
                        opt_width, opts)
                indent_first = 0
            result.append(opts)
            if option.help:
                help_text = self.expand_default(option)
                help_lines =[]
                for l in help_text.split('\n'):
                    help_lines += textwrap.wrap(l, self.help_width)
                result.append("%*s%s\n" % (indent_first, "", \
                        help_lines[0]))
                result.extend(["%*s%s\n" % (self.help_position, "", \
                        line) for line in help_lines[1:]])
            elif opts[-1] != "\n":
                result.append("\n")
            return "".join(result)

    Usage  = "%s [options]" %PROG
    Epilog = \
"""
Example: %s -i 4F67.fasta -t 2BJD_A.pdb -k 300 -n 50 --qmean --glob -K MODELLER_KEY
""" %PROG
    
    parser = OptionParser(formatter=MyFormatter(), usage = Usage, epilog = Epilog )
    
    parser.add_option("-w", "--working-directory", dest="wDir",
                      action="store", default=DEFAULT_WDIR, type="string",
                      help="Working directory (def=%default)",
                      metavar="<string>")
    parser.add_option("-v", "--verbose", dest="verbose", 
                      action="store_true", default=False,
                      help="verbose mode")
    
    groupHHkbest  = OptionGroup(parser, 'HHalign_kbest options')
    groupModeller = OptionGroup(parser, 'Modeller options')
    groupQmean    = OptionGroup(parser, 'Qmean options')
    
    
    groupHHkbest.add_option("-i", "--query-input", dest="query_input",
                      action="store", default=None, type="string",
                      help=
"""
input query alignment (fasta/a2m/a3m) or HMM file (.hhm) *
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--ifasta", dest="query_input",
                      action="store", default=None, type="string",
                      help=
"""
input query alignment (fasta or multi fasta)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--ia3m", dest="query_inpout",
                      action="store", default=None, type="string",
                      help=
"""
input query alignment (a3m)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--ihhm", dest="query_input",
                      action="store", default=None, type="string",
                      help=
"""
input query alignment (.hhm)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option("-t", "--template-input", dest="template_input",
                      action="store", default=None, type="string",
                      help=
"""
input template alignment  (fasta/a2m/a3m) or HMM file (.hhm) or PDB file *
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--tfasta", dest="template_input",
                      action="store", default=None, type="string",
                      help=
"""
input template alignment (fasta or multifasta)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--ta3m", dest="template_input",
                      action="store", default=None, type="string",
                      help=
"""
input template alignment (a3m)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--thhm", dest="template_input",
                      action="store", default=None, type="string",
                      help=
"""
input template alignment (hhm)
""",
                      metavar="<file>")
    
    groupHHkbest.add_option(      "--tpdb", dest="template_input",
                      action="store", default=None, type="string",
                      help=
"""
input template PDB file
""",
                      metavar="<file>")
    
    groupHHkbest.add_option("-k", "--kbest", dest="kbest",
                      action="store", default=DEFAULT_KBEST, type="int",
                      help=
"""Number of suboptimal HMM-HMM alignments to be generated (def=%default).
In suboptimal alignment mode, the k-best Viterbi algorithm is used.
The highest available MAXK depends on both the HMMs lengths and computer RAM.
  Ex: MAXK ~ 800 for two HMMs of size 250 with 3 GB of available memory when using the standard (non-DAG-based) k-best Viterbi.
If this number is set too high with respect to the maximum available memory, it will automatically be replaced by the largest and most suitable number.
""",
                      metavar='[1,MAXK]')
    
    groupHHkbest.add_option("-d", "--dag-option", dest="dag",
                      action="store", default=DEFAULT_DAG, type="choice",
                      choices=['nodag', 'dag', 'autodag'], 
                      help=
"""
DAG-based suboptimal alignment mode:
 -nodag: do not use DAG-based mode (def=%default, very fast)
 -dag: use DAG-based mode (3 times less memory usage but 10 times longer execution time)
 -autodag: automatically set by computer, according to HMM lengths, RAM, and the argument --kbest.
The option -autodag favors first the number of suboptimal alignments , then computation speed.
DAG-based mode only works for global suboptimal alignments, therefore --glob should be added when -dag or -autodag is set.
""",
                      metavar='nodag/dag/autodag')
    
    groupHHkbest.add_option("-m", "--maxmem", dest="maxmem",
                      action="store", default=DEFAULT_MAXMEM, type="float",
                      help=
"""
Maximum available memory in GB (def=%default).
Caution: Please do NOT set an argument exceeding your real computer RAM.
""",
                      metavar='<float>')
    
    groupHHkbest.add_option(      "--glob", dest="ali_global",
                      action="store_true", default=False,
                      help="global alignment mode")
    
    groupHHkbest.add_option(      "--local", dest="ali_local",
                      action="store_true", default=False,
                      help="local alignment mode (default)")
    
    groupModeller.add_option(     "--nmodel1", dest="nModels1", 
                      action="store", default=DEFAULT_MODELS1, type="int",
                      help="number of models to generate - first turn (default: %default)",
                      metavar='<int>')
                      
    groupModeller.add_option(     "--nmodel2", dest="nModels2", 
                      action="store", default=DEFAULT_MODELS2, type="int",
                      help="number of models to generate - second turn (default: %default)",
                      metavar='<int>')
                      
    groupModeller.add_option(     "--nBest", dest="nBest", 
                      action="store", default=DEFAULT_MAXBEST, type="int",
                      help="number of best models to keep - first turn (default: %default)",
                      metavar='<int>')
    
    groupModeller.add_option("-K", "--modellerKey", dest="lKey", 
                      action="store", default=None, type="string", 
                      help="MODELLER LICENCE KEY *",
                      metavar='<string>')
    
    groupQmean.add_option(         "--qmean", dest="qmean", 
                      action="store_true", default=False,
                      help="Launch Qmean evaluation")
    
    parser.add_option_group(groupHHkbest)
    parser.add_option_group(groupModeller)
    parser.add_option_group(groupQmean)
    
    return parser

def run_service(service, args, jobs = 1, max_running = None, map = None, workdir = '.', verbose = 0, local = DEFAULT_LOCAL_JOB):
    # No upper bound, cap at maximum number of available cores
    #if max_running == None:
    #    max_running = min(jobs, DEFAULT_MAX_RUNNING)
    # Check number of jobs
    if jobs < 1:
        raise Exception("0 jobs requested for: %s %s" % (service, args))
    # Run locally or dispatch a batch job
    cluster.runTasks(service, args, tasks = jobs, docker_img = "qmean_precise_amd64", map_list = map, log_prefix = "slurm")

def hhalign_kbest(query="", template="", kbest=10, dag="autodag", 
                  maxmem=3, alignment=0, wdir=".", verbose=0):

    args = [query, template, str(kbest), dag, str(maxmem), str(alignment), str(int(verbose)), OUT]

    run_service("HHalign_kbest_service", args, workdir = wdir, verbose = verbose)
    
    label = query.split(".")[0]
    tlabel = query.split(".")[0]
    
    fFAS = open(OUT+".fas", "rt")
    linesFAS = fFAS.readlines()
    fFAS.close()
    
    # rename template FAS
    resFAS = []
    cpt    = 0
    qseq   = ""
    cptAli = 1
    while cpt < len(linesFAS):
        if linesFAS[cpt][0] == ">" and label in linesFAS[cpt]:
            cpt += 1
            while cpt < len(linesFAS) and linesFAS[cpt][0] != ">":
                qseq += linesFAS[cpt].rstrip()
                cpt += 1
            resFAS.append(">%s\n" %label)
            resFAS.append("%s\n" %qseq)
            
        elif linesFAS[cpt][0] == ">":
            resFAS.append(">ali_%s\n" %cptAli)
            cpt += 1
            cptAli += 1
            seq = ""
            while cpt < len(linesFAS) and linesFAS[cpt][0] != ">":
                seq += linesFAS[cpt].rstrip()
                cpt += 1
            resFAS.append("%s\n" %seq)
    
    op_File = open(OUT+".hhr", "rt")
    li_out = op_File.readlines()
    op_File.close()
    dico = {}
    regex_tag = re.compile("^\s*([0-9]+)\s+k=[0-9]+,")
    for l in li_out:
        match = regex_tag.search(l)
        if match:
            dico[match.group(1)] = {}
            dico[match.group(1)]["pval"] = "%.1E" %float(l[48:57])
            dico[match.group(1)]["qbeg"] = int(l[76:85].split("-")[0])
            dico[match.group(1)]["qend"] = int(l[76:85].split("-")[1])
            dico[match.group(1)]["tbeg"] = int(l[87:94].split("-")[0])
            dico[match.group(1)]["tend"] = int(l[87:94].split("-")[1])
    
    #FAS2PIR
    fFAS = open(OUT+".fas", "wt")
    tag = 0
    cptAli = 1
    for rF in resFAS:
        fFAS.write(rF)
    fFAS.close()
    
    remove_gapFas(OUT+".fas")
    

    f_mali = open(OUT+".pir", "rt")
    mali_tab =  f_mali.readlines()
    f_mali.close()
    
    all_ali = []
    al_tmp  = ""
    al_tmp_tab = []  # to remove the identical sequence
    all_ali_tab = []# to remove the identical sequence
    
    for elem in mali_tab:
        if elem[0] != "\n":
            al_tmp += elem
            if elem[0] == ">" and label not in elem:
                tlabel = elem.rstrip()[4:]
            if elem[0] != "s" and elem[0] != ">":
                al_tmp_tab.append(elem)
        else:
            all_ali.append(al_tmp)
            if al_tmp_tab not in all_ali_tab:
                all_ali_tab.append(al_tmp_tab)
            else:
                all_ali_tab.append("None")
            al_tmp_tab = []
            al_tmp  = ""
    
    
    ff_mali = open(OUT+".fas", "rt")
    fali_tab =  ff_mali.readlines()
    ff_mali.close()
    fasDico = {}
    cpt = 0
    while cpt < len(fali_tab):
        if fali_tab[cpt][0] == ">":
            k = fali_tab[cpt]
            fasDico[k] = ""
            cpt += 1
            while cpt < len(fali_tab) and fali_tab[cpt][0] != ">":
                fasDico[k] += fali_tab[cpt]
                cpt += 1
    
    
    f_mali = open(OUT+".pir", "wt")
    fpirNR = open(OUT+"_nr.pir", "wt")
    ffasNR = open(OUT+"_nr.fas", "wt")
    ffasNR.write(">%s\n%s\n" %(label, qseq))
    for i,elem in enumerate(all_ali):
        if all_ali_tab[i] != "None": # to remove the identical sequence
            f_OneAli = open("ali_it%d.pir" %(i+1), "wt")
            f_OneAli.write(removeGap(elem.replace("%s_%d" %(label, (i+1)), label).replace(tlabel, "ali_%d" %(i+1))))
            f_OneAli.close()
            fpirNR.write(removeGap(elem.replace("%s_%d" %(label, (i+1)), label).replace(tlabel, "ali_%d" %(i+1))))
            fpirNR.write("\n")
            ffasNR.write(">ali_%d\n" %(i+1))
            ffasNR.write(fasDico[">ali_%d\n" %(i+1)])
        f_mali.write(removeGap(elem.replace("%s_%d" %(label, (i+1)), label).replace(tlabel, "ali_%d" %(i+1))))
        f_mali.write("\n")
    ffasNR.close()
    fpirNR.close()
    f_mali.close()
    
    remove_gapFas(OUT+"_nr.fas")
    
    ali_it = glob.glob("ali_it*.pir")
    ali_it = sorted(ali_it, key=lambda x: int(x.split(".")[0].split("ali_it")[-1]))
    
    return ( ali_it, len(all_ali_tab) )


def removeGap (bloc=""):
    tab = bloc.split("\n")
    qseq = tab[2]
    tseq = tab[5]
    
    rqseq = ""
    rtseq = ""
    for i, aa in enumerate(qseq):
        if (aa == "-") and (tseq[i] == "-"):
            continue
        else:
            rqseq += aa
            rtseq += tseq[i]
    
    res = tab[:2] + [rqseq] + tab[3:5] + [rtseq] + tab[6:]
    
    return "\n".join(res)

def remove_gapFas(file):
    f = open(file, "rt")
    lines = f.readlines()
    f.close()

    res = ""
    tag = 0
    tab = []

    for l in lines:
        if l[0] == ">":
            res += l
            tag = 1
        elif tag == 1:
            res += l
            tag = 0
            tab.append(res)
            res =""
    seq = []
    res = []
    for t in tab:
        tmp = t.split("\n")
        seq.append(tmp[1])
        res.append("")


    for i in range(len(seq[0])):
        tag = 0
        for s in seq:
            try:
                if s[i] != "-":
                    tag = 1
            except IndexError:
                continue
        if tag == 1:
            for j,s in enumerate(seq):
                try:
                    res[j] += s[i]
                except IndexError:
                    continue

    f = open(file, "wt")
    for i, r in enumerate(res):
        tmp = tab[i].split("\n")[0]
        f.write("%s\n%s\n" %(tmp, r) )
    f.close()

def modeller(label="", tlabel="", nbMod=5, subAli=[], dirRes="./ModellerPDB", wdir=".", nbTurn="first",verbose=0):
    return run_service("Modeller_service", [label, tlabel, str(nbMod), TARCH, nbTurn, str(int(verbose)), dirRes], map = subAli[0], jobs = len(subAli[0]))

def calcBestAli(all_res=[], max_best=DEFAULT_MAXMODEL, wdir=".", out=QBESTALI):
    ftop_tab = []
    for ar in all_res:
        ftop = open(ar, "rt")
        ftop_tab += ftop.readlines()
        ftop.close()
    
    dict_best_mod = {}
    for i in ftop_tab:
        name    = i.split()[0]
        score = float(i.rstrip().split()[-1])
        num    = i.split()[0].split(".")[0].split("_")[-1]
        if num not in dict_best_mod.keys():
            dict_best_mod[num] = {"name": name, "score": score, 
                                  "cpt": 1, "mean": 0.0}
        else:
            dict_best_mod[num]["score"] += score
            dict_best_mod[num]["cpt"]    += 1
    
    bKeys = dict_best_mod.keys()
    bKeys.sort()
    
    for i in dict_best_mod.keys():
        score = dict_best_mod[i]["score"]
        cpt    = dict_best_mod[i]["cpt"]
        dict_best_mod[i]["mean"] = str(score/cpt)
        
    best_sorted = sorted([[num, dict_best_mod[num]["mean"]] for num in dict_best_mod.keys()], key=lambda x: float(x[-1]), reverse=True)
    ftop = open("%s/%s" %(wdir, out), "wt")
    ftop.write("Alignment\tScore\n")
    ftop.write( "\n".join(["ali_%s_rank%d\t%s"%(elem[0], (i+1), elem[-1]) for i, elem in enumerate(best_sorted)]) + "\n" )
    ftop.close()
    
    
    if len(best_sorted) >= max_best:
        best_mod = [["ali_it%s.pir" %elem[0], elem[-1]] for elem in best_sorted[:max_best] ]
    else:
        best_mod = [["ali_it%s.pir" %elem[0], elem[-1]] for elem in best_sorted ]
    
    
    return best_mod


def calcBestModel(all_res=[], max_best=DEFAULT_MAXMODEL, wdir=".", out=QBESTTXT):
    # take the name of the 50 first best models with Qmean
    
    ftop_tab = []
    for ar in all_res:
        ftop = open(ar, "rt")
        ftop_tab += ftop.readlines()
        ftop.close()
    
    dict_best_mod = {}
    for i in ftop_tab:
        nom    = i.split()[0]
        score = i.rstrip().split()[-1]
        num    = i.split()[0].split(".")[0].split("_")[-1]
        if num not in dict_best_mod.keys():
            dict_best_mod[num] = {"nom": nom, "score": score}
        else:
            if float(score) > float(dict_best_mod[num]["score"]):
                dict_best_mod[num] = {"nom": nom, "score": score}
    
    best_sorted = sorted([[dict_best_mod[num]["nom"], dict_best_mod[num]["score"]] for num in dict_best_mod.keys()], key=lambda x: float(x[-1]), reverse=True)
    ftop = open("%s/%s" %(wdir, out), "wt")
    ftop.write("Models\tScore\n")
    ftop.write( "\n".join(["model_ali_%s_rank%d\t%s"%(elem[0].split(".")[0].split("_")[-1], (i+1), elem[-1]) for i, elem in enumerate(best_sorted)]) + "\n")
    ftop.close()
    
    if len(best_sorted) >= max_best:
        best_mod = [["ali_it%s.pir" %elem[0].split(".")[0].split("_")[-1]] + elem for elem in best_sorted[:max_best] ]
    else:
        best_mod = [["ali_it%s.pir" %elem[0].split(".")[0].split("_")[-1]] + elem for elem in best_sorted ]
    
    return best_mod


def qmean( dirPDB="./ModellerPDB", label="", num_ali=[], wdir=".",
           max_best=DEFAULT_MAXMODEL, nbTurn="first", verbose=0 ):
    
    # progress log
    cluster.progress("(5/6) model quality assessment (Qmean) - %s run" %nbTurn)

    mask = label + "*.pdb"
    outDir = "QmeanRes_%s" % nbTurn
    out = "%s/%s" % (wdir, outDir)
    os.mkdir(out)
    file_list = glob.glob(dirPDB + "/" + mask)
    file_list = cluster.group_map_list(file_list, 20)

    run_service("Qmean_service", [dirPDB, outDir], jobs = len(file_list), map = file_list)
    time.sleep(1)

    # Get the score into a single file
    all_files_zscore = glob.glob("%s/*_Zscore.csv" % out)
    tab_tmp = []

    for a in all_files_zscore:
        f = open(a, "rt")
        res = f.readlines()
        f.close()

        if len(res) > 0 and ("Zscore_QMEAN" in res[1]):
            tab_tmp.append((os.path.basename(a).split("_Zscore.csv")[0],res[1].rstrip().split()[-1]))
        else:
            print "Error in %s: no Zscore found!!" %a

    tab_tmp = sorted(tab_tmp, key=lambda x: float(x[-1]), reverse=True)
    fout = open("%s/Qmean_top_alignment_model.txt" % out, "wt")
    for i in tab_tmp:
        fout.write("%s\t%s\n" %(i[0], i[1]))
    fout.close()

    all_res = glob.glob("%s/Qmean_top_alignment_model.txt" % out)
    if nbTurn == "first":
        best_modQmean = calcBestAli(all_res, max_best, wdir, QBESTALI)
    else:
        best_modQmean = calcBestModel(all_res, max_best,wdir, QBESTTXT)

    #cleaning
    return best_modQmean
    to_rem = glob.glob(dirPDB + "/" +mask)
    
    for tr in to_rem:
        ttr = os.path.basename(tr)
        if os.path.isfile(ttr + "_Zscore.csv"):
            os.unlink(ttr + "_Zscore.csv")
        if os.path.isfile("QMEAN_scores_" + ttr + ".csv"):
            os.unlink("QMEAN_scores_" + ttr + ".csv")
    
    to_rem = list(set([os.path.dirname(elem) for elem in all_res ]))
    
    for tr in to_rem:
        if os.path.isdir(tr):
            shutil.rmtree(tr)
    
    return best_modQmean



def run_Qmean(label="", tlabel="", dirResfirst="", mask="*.pdb", wdir=".", 
               tuple_ali=(), max_best=DEFAULT_MAXBEST, 
               nbMod2=DEFAULT_MODELS2, verbose=0, q=None):
    
    
    num_ali = [elem.split("ali_it")[-1].split(".")[0] for elem in tuple_ali[0]]
    
    best_modQmean1st = qmean(dirPDB=dirResfirst, label=label, 
                                num_ali=num_ali, wdir=".",
                                max_best=max_best, nbTurn="first",
                                verbose=verbose)
    
    tuple_ali_2nd = ([elem[0] for elem in best_modQmean1st],tuple_ali[-1])
    
    num_bestAli = [elem.split(".")[0].split("ali_it")[-1] for elem in tuple_ali_2nd[0]]
    
    f = open(OUT+"_nr.fas")
    lFasnr = f.readlines()
    f.close()
    
    
    resFAS = []
    cpt    = 0
    qseq   = ""
    resQuery = ""
    while cpt < len(lFasnr):
        if lFasnr[cpt][0] == ">" and label in lFasnr[cpt]:
            cpt += 1
            while cpt < len(lFasnr) and lFasnr[cpt][0] != ">":
                qseq += lFasnr[cpt].rstrip()
                cpt += 1
            resQuery += ">%s\n%s\n" %(label,qseq)
            resFAS.append(resQuery)
        elif lFasnr[cpt][0] == ">":
            tag = 0
            resTemp = ""
            for r, num in enumerate(num_bestAli):
                if lFasnr[cpt] == ">ali_%s\n" %num:
                    resTemp += ">ali_%s_rank%d\n" %(num,(r+1))
                    tag = 1
                    break
        
            cpt += 1
            seq = ""
            while cpt < len(lFasnr) and lFasnr[cpt][0] != ">":
                seq += lFasnr[cpt].rstrip()
                cpt += 1
            if tag == 1:
                resTemp +="%s\n" %seq
                resFAS.append(resTemp)
    
    resFAS = [resFAS[0]] + sorted(resFAS[1:], key=lambda x: int(x.split("\n")[0].split("rank")[-1]))
    f = open(OUT+"_bestAli.fas", "wt")
    for rF in resFAS:
        f.write(rF)
    f.close()
    
    remove_gapFas(OUT+"_bestAli.fas")
    
    f = open(OUT+"_bestAli.pir", "wt")
    for i, ta in enumerate(tuple_ali_2nd[0]):
        fpir = open(ta, "rt")
        lines = fpir.readlines()
        fpir.close()
        res = []
        for l in lines:
            if ">P1;ali_" in l:
                res.append(l.rstrip() + "_rank" + str(i+1) + "\n")
            elif "ali_" in l:
                tmp = l.split(":")
                res.append(":".join([tmp[0]] + [tmp[1] + "_rank" + str(i+1)] + tmp[2:]))
            else:
                res += l
        f.write("".join(res))
        f.write("\n")
            
    f.close()
    
    dirRes = "Qmean_ModellerPDB_2nd"
    os.mkdir(dirRes);
    
    modeller(label=label, tlabel=tlabel, nbMod=nbMod2,
                subAli=tuple_ali_2nd, wdir=wdir, dirRes=dirRes,
                nbTurn="2nd", verbose=verbose)
    time.sleep(1)
    
    num_ali2 = [elem.split("ali_it")[-1].split(".")[0] for elem in tuple_ali_2nd[0]]
    
    best_modQmean2nd = qmean(dirPDB=dirRes, label=label,
                                num_ali=num_ali2, wdir=".",
                                max_best=DEFAULT_MAXMODEL, nbTurn="2nd",
                                verbose=verbose)
    
    
    best_modQmean5 = selectBest_5Models(resPDB=dirRes, resPir=QBESTPIR,
                                        resPir5=QBESTPIR5, wdir=wdir,
                                        best_mod=best_modQmean2nd,
                                        label=label, method="Qmean")

    # progress log
    cluster.progress("(6/6) formatting output (Qmean)")

    iSuperpose(best_mod=best_modQmean5, wdir=wdir,
                verbose=verbose)

    time.sleep(1)

    iSupMod = glob.glob("model*.pdb")
    iSupMod = sorted(iSupMod, key=lambda x: int(x.split("rank")[-1].split(".")[0]))
    # iSupMod5 = iSupMod[:5]
    iSupMod5 = iSupMod[:10]

    # for mobyle
    for i,ism in enumerate(iSupMod5):
        if not os.path.isfile("model%s.pdb" %(i+1)):
            shutil.copy(ism, "model%s.pdb" %(i+1))
        if not os.path.isfile("model%s.pir" %(i+1)):
            shutil.copy(ism.replace(".pdb",".pir"), "model%s.pir" %(i+1))
        if not os.path.isfile("model%s.fas" %(i+1)):
            shutil.copy(ism.replace(".pdb",".fas"), "model%s.fas" %(i+1))

    os.system("LANG=C && zip %s %s %s_5bestModels.fas > /dev/null" %(QJAL_ARCH, " ".join(iSupMod5), OUT))
    os.system("LANG=C && zip HHalign_kbest_best_models.zip query.hhm template.hhm %s.hhr model[1-9].pdb model10.pdb model[1-9].pir model10.pir model[1-9].fas model10.fas %s_bestModels.pir %s_bestModels.fas %s > /dev/null" %(OUT, OUT, OUT, QBESTTXT))

    #to_rem = glob.glob("iSup_model*.pdb")
    #for tr in to_rem:
    #    os.unlink(tr)

    if q :
        q.put(best_modQmean5)

    return best_modQmean5

def iSuperpose(best_mod=[], wdir=".", verbose=0):
    file_list = [os.path.basename(m) for m in best_mod]
    if len(file_list) < 2:
        return # Nothing to superpose
    return run_service("iSuperpose_service", [file_list[0]], jobs = len(file_list) - 1, map = file_list[1:])

def selectBest_5Models(resPDB="./ModellerPDB", resPir=QBESTPIR, resPir5=QBESTPIR5, wdir=".", best_mod=[], label="", method="Qmean"):
    best_mod5 = []
    maxLen = 50 if len(best_mod) >= 50 else len(best_mod)
    
    pref = ""
    id = "q"

    f_pir  = open("%s%s_bestModels.pir" %(pref, OUT), "wt")
    f_pir5 = open("%s%s_5bestModels.pir"%(pref, OUT), "wt")
    for i in range(maxLen):
        num = best_mod[i][0].split("ali_it")[-1].split(".")[0]
        if not os.path.isfile("%s/%smodel_ali_%s_rank%d.pdb" %(wdir, pref, num, (i+1))):
            shutil.copy("%s/%s" %(resPDB, best_mod[i][1]), "%s/%smodel_ali_%s_rank%d.pdb" %(wdir, pref, num, (i+1)))

        best_mod5.append("%s/%smodel_ali_%s_rank%d.pdb" %(wdir, pref, num, (i+1)))

        if not os.path.isfile("%s/%smodel_ali_%s_rank%d.pir" %(wdir, pref, num, (i+1))):
            shutil.copy(best_mod[i][0], "%s/%smodel_ali_%s_rank%d.pir" %(wdir, pref, num, (i+1)))

        f_tmp = open("%s/%smodel_ali_%s_rank%d.pir" %(wdir, pref, num, (i+1)), "rt")
        l_tmp = f_tmp.readlines()
        f_tmp.close()

        res = []
        for l in l_tmp:
            if ">P1;ali_" in l:
                res.append(l.rstrip().replace("ali_", "%smodel_ali_" %pref) + "_rank" + str(i+1) + "\n")
            elif "ali_" in l:
                tmp = l.split(":")
                res.append(":".join([tmp[0]] + [tmp[1].replace("ali_", "%smodel_ali_" %pref) + "_rank" + str(i+1)] + tmp[2:]))
            else:
                res += l

        f_tmp = open("%s/%smodel_ali_%s_rank%d.pir" %(wdir, pref, num, (i+1)), "wt")
        f_tmp.write("".join(res))
        f_tmp.close()

        f_pir.write("".join(res))
        f_pir.write("\n")
        if i < 5:
            f_pir5.write("".join(res))
            f_pir5.write("\n")

    f_pir5.close()
    f_pir.close()

    fasnr = open(pref+ OUT+"_bestAli.fas", "rt")
    lines = fasnr.readlines()
    fasnr.close()

    resFAS = []
    cpt    = 0
    qseq   = ""
    resQuery = ""
    num_bestMod = [ elem[0].split("ali_it")[-1].split(".")[0] for elem in best_mod ]

    while cpt < len(lines):
        if lines[cpt][0] == ">" and label in lines[cpt]:
            cpt += 1
            while cpt < len(lines) and lines[cpt][0] != ">":
                qseq += lines[cpt].rstrip()
                cpt += 1
            resQuery += ">%s\n%s\n" %(label,qseq)
            resFAS.append(resQuery)
        elif lines[cpt][0] == ">":
            tag = 0
            resTemp = ""
            for r, num in enumerate(num_bestMod):
                if ">ali_%s_" %num in lines[cpt]:
                    resTemp += ">ali_%s_rank%d\n" %(num,(r+1))
                    tag = 1
                    break

            cpt += 1
            seq = ""
            while cpt < len(lines) and lines[cpt][0] != ">":
                seq += lines[cpt].rstrip()
                cpt += 1
            if tag == 1:
                resTemp +="%s\n" %seq
                resFAS.append(resTemp)

    resFAS = [resFAS[0]] + sorted(resFAS[1:], key=lambda x: int(x.split("\n")[0].split("rank")[-1]))

    f_fas  = open("%s%s_bestModels.fas" %(pref, OUT), "wt")
    f_fas5 = open("%s%s_5bestModels.fas"%(pref, OUT), "wt")
    for i, rF in enumerate(resFAS):
        f_fas.write(rF)
        if i < 6:
            f_fas5.write(rF)
    f_fas.close()
    f_fas5.close()

    remove_gapFas("%s%s_bestModels.fas" %(pref, OUT))
    remove_gapFas("%s%s_5bestModels.fas" %(pref, OUT))

    # Create individual files for model[1-5].fas, model[1-5].pir
    for rank in range(1,11):
        try:
            fname = glob.glob("model_ali*_rank%s.pir" % rank)[0]
            if not os.path.isfile("model%s.pir" %(rank)):
                shutil.copy(fname, "model%s.pir" %(rank))
            f_tmp = open("model%s.pir" %(rank), "rt")
            l_tmp = f_tmp.readlines()
            f_tmp.close()
            f = open("model%s.fas" %(rank), "wt")
            for ll in l_tmp:
                if ll[0] == ">":
                    f.write(ll.replace("P1;",""))
                elif ll.split(":")[0] == "sequence":
                    continue
                elif ll.split(":")[0] == "structure":
                    continue
                else:
                    f.write(ll.replace("*",""))
            f.close()
        except:
            # print fname, model%s.pdb
            pass
    
    return best_mod5


def isHHM(file="", verbose=0):
    """
    check if a file is a FASTA file
    return 1 if it is a FASTA file, 0 otherwise
    """
    
    if verbose:
        sys.stderr.write("Checking if the file %s is in HHM format...\n"\
                        %file)
    
    f = open(file, "rt")
    lines = f.readlines()
    f.close()
    
    regexHHM1 = re.compile("^NULL +([0-9]+\t){20}")
    regexHHM2 = re.compile("^HMM +([A-Z]\t){20}")
    regexHHM3 = re.compile("^ +M->M\tM->I\tM->D\tI->M\tI->I\tD->M\tD->D\tNeff\tNeff_I\tNeff_D")
    
    cpt = 0
    while cpt < (len(lines)-1):
        if regexHHM1.search(lines[cpt]):
            cpt += 1
            if regexHHM2.search(lines[cpt]):
                cpt += 1
                if regexHHM3.search(lines[cpt]):
                    return 1
        cpt += 1
    
    return 0

def isFASTA(file="", verbose=0):
    """
    check if a file is a FASTA file
    return 2 if it is a multi fasta file, 1 if it is a FASTA file, 
    0 otherwise
    """
    
    if verbose:
        sys.stderr.write("Checking if the file %s is in FASTA format...\n"\
                        %file)
    
    f = open(file, "rt")
    lines = f.readlines()
    f.close()
    
    tag_fasta = 0
    cpt = 0
    regexFasta = re.compile("[A-Za-z-]+")
    
    if lines[0][0] == ">" and regexFasta.search(lines[1]):
        tag_fasta = 1
    else:
        return tag_fasta
    
    for l in lines:
        if l[0] == ">":
            cpt += 1
        if cpt > 1:
            tag_fasta = 2
            break
    
    return tag_fasta


def isPDB(file="", verbose=0):
    """
    check if a file is a PDB file
    return 1 if it is a PDB file, 0 otherwise
    """
    
    if verbose:
        sys.stderr.write("Checking if the file %s is in PDB format...\n"\
                        %file)
    
    f = open(file, "rt")
    lines = f.readlines()
    f.close()
    
    regexPDB = re.compile("^(ATOM|HETATM) *[0-9]+ +[A-Z0-9]+ *[A-Z]+ +[A-Z0-9] *[0-9]+ *[A-Z0-9] +[0-9\.\- ]+ +[A-Z0-9 ]+")
    
    for l in lines:
        if regexPDB.search(l):
            return 1
    
    return 0
    


def Fasta2HHM(file="", wdir=".", pref="fst", multi=False, verbose=0, q=None):
    # create HHM from FASTA sequence
    
    my_fst = Fasta.fasta(file)
    
    if multi:
        li = []
        f = open(file, "rt")
        tmp = f.readline()
        li.append(tmp)
        tmp = f.readline()
        
        while(tmp[0] != ">"):
            li.append(tmp)
            tmp = f.readline()
        
        f.close()
        
        my_fst.parse(li,0)
        
    id  = my_fst.ids()[0]
    seq = my_fst[id].data["s"].replace("X", "-")
    
    new_fst = Fasta.sfasta(cmt="", fid=id, seq=seq)
    new_fst.write(id+".fst")
    
    if multi:
        if not os.path.isfile("%s/%s.fas" %(wdir, id)):
            shutil.copy(file, "%s/%s.fas" %(wdir, id))
    
    run_service("HHmakeExec_service", [id, "fas" if multi else "a3m", str(int(verbose))])
    
    if q :
        q.put(id)
    
    return id


def PDB2HHM(file="", wdir=".", verbose=0, q=None):
    # create HHM from PDB file
    
    #name = os.path.basename(file).split(".")[0]
    pdb = PDB.PDB( file, hetSkip=2 )
    
    if pdb.nChn() == 0 :
        err = "Error : No chain found in PDB file.\n The program will stop now."
        cluster.progress(err)
        sys.exit(-1)
    elif pdb.nChn() > 1 :
        err = "More than one chain found in PDB file.\n The program will take the first one."
        cluster.progress(err)
    
    nt  = pdb.id
    nc  = pdb.chnList()[0]
    pdb = PDB.PDB( file, nc, hetSkip=2 )
    pdb.clean()
    pdb.out( nt + nc + ".pdb" )
    pdb.out( "template.pdb" )
    
    os.system( "tar -czf template_PDB.tar.gz %s%s.pdb" %(nt, nc) )
    
    aas = pdb.aaseq()
    Fasta.sfasta(fid="%s_%s" %(nt, nc), seq=aas).write("%s_%s.fst" %(nt, nc))
    
    new_name = Fasta2HHM("%s_%s.fst" %(nt, nc), wdir, "pdb", False, verbose)
    
    if q :
        q.put(new_name)
    
    return new_name

def format_visualization(results, output_file):
    """ Format HTML file with output visualization. """
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('hhalign.vis.html')
        sink.write(template.render(data = results, data_count = len(results)))


def main( args ):
    """
    The main entry point. Wraps calls to hhsearch etc using 
    either sge or os.system.
    """
        
    clp = cmdLine()
    (options, argument) = clp.parse_args(args)
    
    start = time.time()

    workDir = os.getcwd()
                
    ##### Input validation
    # progress log
    cluster.progress("(1/6) input validation")
 
    if options.query_input is None:
        cluster.progress("      No sequence input specified. Aborting.")
        sys.exit(-1)

    if options.template_input is None:
        cluster.progress("      No template specified. Attempting to identify one using HHsearch.")
        curDir = os.getcwd()
        if not os.path.exists("%s/domainFinder" % curDir):
            os.mkdir("%s/domainFinder" % curDir)
        # print cmd
        args = ['-i', options.query_input, '-r', '3', '-w', './domainFinder', '-l', 'dfinder']
        cluster.runTasks("DOMFinderExec", args, docker_img = 'qmean_precise_amd64')
        # Now we identify the best template proposed
        try:
            fname = glob.glob("./domainFinder/resPDB/*_cov15.hhr")[0]
        except:
            cluster.progress("      Aborting: could not identify any template.")
            sys.exit(-1)
        if not os.path.exists("templates.hhr"):
            shutil.copy(fname,"templates.hhr")
        f = open(fname)
        lines = f.readlines()
        f.close()
        isTemplate = False
        template = None
        for l in lines:
            # print l,
            if l.count(" No Hit"):
                headerLine = l
                isTemplate = True
                continue
            if isTemplate:
                it = l.split()
                if len(it) < 2:
                    break
                it = it[1]
                if not os.path.isfile("./domainFinder/%s.pdb" % it):
                    continue
                theLine = l
                template = "%s.pdb" % it
                templateId = it
                shutil.copy("./domainFinder/%s" % template, options.wDir)
                options.template_input = template
                fname = glob.glob("./domainFinder/resPDB/*_cov15.png")[0]
                shutil.copy(fname, "%s/%s" % (options.wDir, "templates.png"))
                break

        if not template:
            cluster.progress("      Aborting: could not identify any PDB template.")
            # sys.stderr.write("\n\ncould not identify any template.\n\n")
            sys.exit(-1)
        else:
            seq_identity = None
            hhblistProba = None
            infoLine = "\n"
            # print "looking for >%s " % templateId
            for i, l in enumerate(lines):
                if l.count(">%s " % templateId):
                    # print l, lines[i+1]
                    infoLine = lines[i+1]
                    seq_identity = lines[i+1].split()[4].split("=")[1].replace("%","")
                    hhblits_proba = lines[i+1].split()[0].split("=")[1].replace("%","")
                    break
            cluster.progress("      Will use template: %s (%s%% sequence identity)" %
(options.template_input, seq_identity))
            # sys.stderr.write("\n\nWill use template: %s\n\n" % options.template_input)
            f = open("templates.txt","w")
            f.write("Template used (PDB code): %s\nHHsearch probability: %s\nSequence Identity: %s\n\n" % (templateId, hhblits_proba, seq_identity))
            f.close()
            try:
                if float(seq_identity) > 35.:
                    cluster.progress("      Sequence identity >35%. Only 1 alignement will be considered.")
                    options.kbest = 1
                    f = open("templates.txt","a")
                    f.write("Analyzing you query sequence in automatic mode (no template specified), a template with significant identity was identified by hhsearch so that hhalign-kbest was not used (the optimal alignment is probably already precise enough to generate correct models).\nHowever, if you wish to run hhalign-kbest to explore sub-optimal alignment even for that case, you can re-run the hhalign-kbest server and specify as template the PDB code and chain of interest.\n\n")
                    f.close()
            except:
                pass

        # sys.exit(0)
    else:
        f = open("templates.info","w")
        f.write("User specified its own template: Template name : %s\n\n" % (options.template_input))
        f.close()
    
    if (options.dag == "dag" or options.dag == "autodag") \
            and not options.ali_global:
        sys.stderr.write("\n\nOption %s requires option --global\n\n"\
                        %options.dag )
        clp.print_help()
        sys.exit(-1)
    
    if options.ali_local and options.ali_global:
        sys.stderr.write("\n\nYou have to choose between local and global option\n\n")
        clp.print_help()
        sys.exit(-1)
    
    curDir = os.getcwd()
    
    if not os.path.isfile( options.wDir + "/" + os.path.basename(options.query_input)):
        shutil.copy(options.query_input, options.wDir)
    if not os.path.isfile( options.wDir + "/" + os.path.basename(options.template_input)):
        shutil.copy(options.template_input, options.wDir)
    
    if (options.wDir != "." and options.wDir != "./"):
        if not os.path.isfile(options.wDir):
            shutil.copy(options.query_input, options.wDir)
        if not os.path.isfile(options.wDir):
            shutil.copy(options.template_input, options.wDir)
        os.chdir(options.wDir)
        
    options.query_input    = os.path.basename(options.query_input)
    options.template_input = os.path.basename(options.template_input)
    
   
    resQ_isHHM   = isHHM(options.query_input, options.verbose)
    resQ_isFASTA = isFASTA(options.query_input, options.verbose)
    resT_isPDB   = isPDB(options.template_input, options.verbose)

    if resQ_isFASTA:
        my_fst = Fasta.fasta(options.query_input)
        testLabel  = my_fst.ids()[0]
        if testLabel.count(":"):
            cluster.progress("Aborting: query label (%s) cannot contain \":\". Please edit your input accordingly" % (testLabel))
            sys.exit(-1)
    
    if resQ_isHHM == 0 and resQ_isFASTA == 0 :
        err = "Error : Query file is not in FASTA format nor in HHM format.\nThe program will stop now."
        cluster.progress(err)
        sys.exit(-1)
    
    if resT_isPDB == 0 :
        err = "Error : Template file is not in PDB format.\nThe program will stop now."
        cluster.progress(err)
        sys.exit(-1)
    
    if resQ_isHHM == 2 :
        cluster.progress("Warning : Query input is in multiFASTA format\n. The query file name must match to the identifier of the first sequence.")
    
    Modtag = 1
    if options.lKey != MODELLER_LKEY:
        cluster.progress("Warning : Incorrect MODELLER key!! The program will not launch Modeller!!")
        Modtag = 0

##### HHM creation
    # progress log
    cluster.progress("(2/6) HHMs creation")
    if resQ_isHHM > 0:
        f = open(options.query_input, "rt")
        tmp = f.readlines()
        f.close()
        cptH = 0

        while(tmp[cptH][:4] != "NAME" and cptH != (len(tmp)-1)):
            cptH += 1
        
        if "NAME" in tmp[cptH]:
            qLabel = tmp[cptH].rstrip().split()[1]
        else:
            err = "No name found in HHM file."
            # sys.stderr.write(err)
            cluster.progress(err)
            sys.exit(-1)
        
        if not os.path.isfile("%s/%s.hhm" %(workDir, qLabel)):
            shutil.copy(options.query_input, "%s/%s.hhm" %(workDir, qLabel))
        
        while(tmp[cptH][1:len(qLabel)+1] != qLabel and cptH != (len(tmp)-1)):
            cptH += 1
        
        if qLabel not in tmp[cptH]:
            err = "No label '%s' found in HHM file." %qLabel
            # sys.stderr.write(err)
            cluster.progress(err)
            sys.exit(-1)
        cptH += 1
        seq = ""
        
        while(tmp[cptH][0] != ">" and cptH != (len(tmp)-1)):
            seq += tmp[cptH].rstrip()
            cptH += 1
        
        if not os.path.isfile("%s/%s.fst" %(workDir, qLabel)):
            Fasta.sfasta(cmt="", fid=qLabel, seq=seq).write("%s/%s.fst" %(workDir, qLabel))
        
        tLabel = PDB2HHM(file=options.template_input, 
                        wdir=workDir, verbose=options.verbose)
    elif resQ_isFASTA > 0 and resT_isPDB > 0 :
        if resQ_isFASTA == 1 :
            qLabel = Fasta2HHM(options.query_input, workDir, "fst", False, options.verbose, None)
        elif resQ_isFASTA == 2 :
            qLabel = Fasta2HHM(options.query_input, workDir, "fst", True, options.verbose, None)
        tLabel = PDB2HHM(options.template_input, workDir, options.verbose, None)
        
        # print "QUERY LABEL: %s ========================================" % (qLabel)
        # print "TEMPLATE LABEL: %s ========================================" % (tLabel)
        
    if qLabel == None or tLabel == None :
        sys.exit(-1)


##### HHalign_kbest
    # progress log
    cluster.progress("(3/6) sub optimal alignements generation")
    
    ali = 0
    if options.ali_global:
        ali = 1
    
    tuple_ali = hhalign_kbest(query=qLabel+".hhm", 
                template=tLabel+".hhm", 
                kbest=options.kbest, dag=options.dag, 
                maxmem=options.maxmem, alignment=ali, wdir=workDir, 
                verbose=options.verbose)

    # print "tuple_ali: %s ========================================" % (str(tuple_ali))
    
    # for mobyle
    if not os.path.isfile("query.hhm"):
        shutil.copy(qLabel+".hhm", "query.hhm")
    if not os.path.isfile("template.hhm"):
        shutil.copy(tLabel+".hhm", "template.hhm")
    
    if Modtag == 0:
        # progress log
        cluster.progress("Finished")
        sys.exit(0)

##### Modeller 1st turn
    # progress log
    cluster.progress("(4/6) 3D models generation")
    
    dirResfirst = "ModellerPDB_first"
    os.mkdir(dirResfirst);

    tLabel = "".join(tLabel.split("_"))
    
    modeller(label=qLabel, tlabel=tLabel, nbMod=options.nModels1, subAli=tuple_ali, 
             wdir=workDir, dirRes=dirResfirst, nbTurn="first", verbose=options.verbose)
    time.sleep(1)

    mask = qLabel + "*.pdb"
    
 ##### Qmean
    best_modQmean10  = []
    if options.qmean:
        best_modQmean10 = run_Qmean(label=qLabel, tlabel=tLabel, dirResfirst=dirResfirst,
                                    mask=mask, wdir=workDir, tuple_ali=tuple_ali,
                                    max_best=options.nBest, nbMod2=options.nModels2,
                                    verbose=options.verbose)

    # Append to list (name, model, score, filename)
    model_id = 0
    results = []
    with open('Qmean_best_models.txt', 'r') as res_file:
        for line in res_file:
            if line.startswith('Models'):
                continue
            line = line.split()
            if len(line) < 2:
                break
            model_id += 1
            if model_id <= 10:
                results.append( (line[0], str(model_id), line[1], 'model%d.pdb' % model_id
))
    #### HTML creation
    if options.qmean:
        format_visualization(results, 'hhalign_kbest.vis.html')
    
#### Remove unused files
    to_rem = glob.glob("iSup_*.log") + glob.glob("ali_it*.pir")
    for tr in to_rem:
        if os.path.isfile(tr):
            os.unlink(tr)
    
    if os.path.isdir(dirResfirst):
        shutil.rmtree(dirResfirst)
    if os.path.isdir("Qmean_ModellerPDB_2nd"):
        shutil.rmtree("Qmean_ModellerPDB_2nd")
    if os.path.isdir("QmeanRes_first"):
        shutil.rmtree("QmeanRes_first")
    if os.path.isdir("QmeanRes_2nd"):
        shutil.rmtree("QmeanRes_2nd")
    
    os.chdir(curDir)
    
    # progress log
    cluster.progress("Finished")


if __name__ == "__main__":
    
    main(sys.argv)
    sys.exit(0)
