#!/usr/bin/env python

from setuptools import setup, find_packages
import glob

setup(name='rpbs_services',
      description='RPBS services for python 2.7',
      author='Julien Rey',
      author_email='julien.rey@univ-paris-diderot.fr',
      url='bioserv.rpbs.univ-paris-diderot.fr',
      packages=find_packages('env'),
      package_dir={'': 'env'},
      package_data={'': ['templates/*.html', 'config/*.ini', 'doc/*', 'demo/*', 'pymol/*']},
      scripts=glob.glob('*.py'),
)
