#!/usr/bin/env python

import argparse
import re
import os
import sys
import shutil
import glob
import json
import string
import csv
from collections import defaultdict

import cluster.cluster as cluster
import PyPDB.PyPDB as PDB

class myPDB(PDB.PDB):

    def compound(self):
        """
        PDB.compound
        @return: the nature of the file (string)
        """
        title=''
        for Line in self.info:
          if Line[:6]=='COMPND':
            items = string.split(Line[6:])
            for aItem in items:
              if title != '':
                title = title + " "
              title = title + aItem
        return title

#Constants
DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"
DFLT_PDB_BANK = "/shared/banks/pdb/data/structures/all/pdb/"
DFLT_PDBQT_BANK = "/shared/banks/pdbqt"
DFLT_BS_BANK = "/shared/banks/binding_sites/"
ENVIRONMENT_MODULE = "patchsearch/2.0-rpbs"

jobId = os.path.basename(os.getcwd())
serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]   # can be patchsearch or patchidentification
wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PepiT', 'templates'))


def cmdLine():

    parser = argparse.ArgumentParser(description=__doc__)
    
    parser.add_argument("-p", "--pdb", dest="pdb_file",
                        action="store", default=None,
                        help="PDB file from which extract patch around ligand.")

    parser.add_argument("-q", "--qchain", dest="query_chain",
                        action="store", default=None,
                        help="Chain of protein query.")   
    
    parser.add_argument("-l", "--lchain", dest="ligand_chain",
                        action="store", default=None,
                        help="Chain of peptide to define patch around.")
 
    parser.add_argument("-i", "--pdbidlist", dest="pdbid_list",
                        action="store", default=None,
                        help="List of pdbs to browse (pdb ids).")
                        
    parser.add_argument("-a", "--pdbbanklist", dest="pdbbank_list",
                        action="store", default=None,
                        help="Bank of pdbs to browse (pdb ids with chain).")
                        
    parser.add_argument("-s", "--bsbank", dest="bs_bank",
                        action="store_true", default=False,
                        help="Bank of binding sites (pdb files).")

    parser.add_argument("-m", "--max_hits", dest="max_hits",
                        action="store", default=1, type=int,
                        help="Number of returned hits on a target.")
                        
    parser.add_argument("-b", "--nr_best_hits", dest="nr_best_hits",
                        action="store", default=50, type=int,
                        help="Number of best hits.")

    parser.add_argument("-g", "--scoring_method", dest="scoring_method",
                        action="store", default="vinardo", choices=["dkoes_scoring", "vinardo"],
                        help="Scoring method.") 

    return parser


def checkArgValues(args):

    # Input complex file is PDB

    if not args.pdb_file:
        msg = "No pdb from which to extract patch file specified."
        return False, msg

    if (args.query_chain == "None"):
        args.query_chain = None
    
    if (args.ligand_chain == "None"):
        args.ligand_chain = None

    status, msg = PDB.isPDB(args.pdb_file)
    if not status:
        return False, msg
        
    x = PDB.PDB(args.pdb_file)
    if args.query_chain and not x.hasChn(args.query_chain):
        msg = "Input PDB has no '%s' chain" % args.query_chain
        return False, msg
        
    if args.ligand_chain and not x.hasChn(args.ligand_chain):
        msg = "Input PDB has no peptide '%s' chain" % args.ligand_chain
        return False, msg

    pdb_file = os.path.splitext(os.path.basename(args.pdb_file))[0] + ".pdb"

    if args.pdb_file != pdb_file:
        try:
            shutil.copy2(args.pdb_file, pdb_file)
        except IOError, e:
            msg = "Could not copy input pdb file to current directory."
            return False, msg
        args.pdb_file = pdb_file
     
    if not args.pdbid_list and not args.bs_bank and not args.pdbbank_list:
        msg = "No pdb ids list nor binding sites / pdb bank provided."
        return False, msg
        
    if args.pdbid_list:
        if os.path.split(args.pdbid_list)[0]:
            try:
                shutil.copy2(args.pdbid_list, "./" + os.path.basename(args.pdbid_list))
                args.pdbid_list = os.path.basename(args.pdbid_list)
            except IOError, e:
                msg = "Could not copy pdb ids list to current directory."
                return False, msg

    return True, ""


def readAligFile(alig_file, ligand_num):

    alig_num = 0

    scores = {}

    with open(alig_file, "r") as alig_file_handle:
        alig_file_content = alig_file_handle.readlines()
        for i, line in enumerate(alig_file_content):
            if line.startswith(">"):
                alig_num += 1
                if alig_num == ligand_num:
                    score_line = line.split()[4:]
                    scores["match_len"] = score_line[0]
                    scores["rmsd"] = "%0.3f" % float(score_line[1])
                    scores["coverage"] = "%0.3f" % float(score_line[2])
                    scores["mean_distorsion"] = "%0.3f" % float(score_line[3])
                    scores["score"] = "%0.3f" % float(score_line[4])
                    query_atom_list = [ "%s:%s.%s" % tuple([atom.split(":")[j] for j in (0,3,2)]) for atom in alig_file_content[i+1].split() ]
                    query_residue_list = [ atom.split(".")[0] for atom in query_atom_list ]
                    query_chain_list = list(set([ residue.split(":")[1] for residue in query_residue_list ]))
                    hit_atom_list = [ "%s:%s.%s" % tuple([atom.split(":")[j] for j in (0,3,2)]) for atom in alig_file_content[i+2].split() ]
                    hit_residue_list = [ atom.split(".")[0] for atom in hit_atom_list ]
                    hit_chain_list = list(set([ residue.split(":")[1] for residue in hit_residue_list ]))
                    scores["query_atom_list"] = query_atom_list
                    scores["query_residue_list"] = query_residue_list
                    scores["query_chain_list"] = query_chain_list
                    scores["hit_atom_list"] = hit_atom_list
                    scores["hit_residue_list"] = hit_residue_list
                    scores["hit_chain_list"] = hit_chain_list

    return scores
    

def readPDBInfo(pdb_file, chains):

    pdb = myPDB(pdb_file)

    compound_lines = pdb.compound()
    source_lines = pdb.source()

    match_molecule = re.findall("MOL_ID:\s+(\d+);\s+\d+\s+MOLECULE:\s+([^;]*);\s+\d+\s+CHAIN:\s+([A-Z, ]+)", compound_lines)
    match_organism = re.findall("MOL_ID:\s+(\d+);\s+\d+\s+ORGANISM_SCIENTIFIC:\s([^;]*);", source_lines)

    mol_ids = defaultdict(dict)

    if len(match_molecule):
        for m in match_molecule:
            mol_ids[m[0]].update({
                                  "molecule" : m[1], 
                                  "chain" : [x.strip() for x in m[2].split(",")]
                                 })
    if len(match_organism):
        for m in match_organism:
            mol_ids[m[0]].update({
                                  "organism" : m[1]
                                 })

    molecules = []
    organisms = []
    
    if bool(mol_ids):
        for mol_id in mol_ids.keys():
            if "chain" in mol_ids[mol_id].keys() and len(list(set(mol_ids[mol_id]["chain"]).intersection(set(chains)))):
                if "molecule" in mol_ids[mol_id].keys():
                    molecules.append(mol_ids[mol_id]["molecule"])
                if "organism" in mol_ids[mol_id].keys():
                    organisms.append(mol_ids[mol_id]["organism"])

    if not len(molecules):
        molecules.append("N/A")
    if not len(organisms):
        organisms.append("N/A")

    return molecules, organisms


def formatScoresTable(data, output_file, wdir="./"):

    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('patchsearch2.table.html')
        sink.write(template.render(data = data))


def formatViewer(pdb_file, data, template, output_file, wdir = "./"):
        
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template(template)
        sink.write(template.render(pdb_file = pdb_file, data = json.dumps(data), wdir = wdir))


def formatPatchesViewer(model_file, patches, output_file, wdir = "./"):
  
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('patchidentification.vis.html')
        sink.write(template.render(model_file = model_file, patches = patches, wdir = wdir))    


def main():

    parser = cmdLine()
    p_args = parser.parse_args()
    status, msg = checkArgValues(p_args)

    if not status:
        cluster.progress(msg)
        sys.exit(0)

    # we prepare the protein-peptide complex
    
    cmd = "SimpleAtomTyping.R"
    s_args = [ "%s" % p_args.pdb_file,
               "%s" % p_args.pdb_file ]
    
    cluster.progress("Atom Typing.")
    cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "simple_atom_typing")

    # we extract the patch(es) around the peptide if chains are specified
    
    if (p_args.query_chain and p_args.ligand_chain):

        ps_input_file = os.path.splitext(p_args.pdb_file)[0] + "-bs.pdb"

        cmd = "getBindingSite.R"
        s_args = [ "%s" % p_args.pdb_file,
                   "--qchain %s" % p_args.query_chain,
                   "--lchain %s" % p_args.ligand_chain,
                   "--add calpha",
                   "--outfile %s" % ps_input_file ]

        cluster.progress("Query chain '%s' and peptide chain '%s' were specified : extraction of binding site around peptide." % (p_args.query_chain, p_args.ligand_chain))
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "get_binding_site")
        
    else:

        ps_input_file = p_args.pdb_file
        
        cmd = "pdb2pdbqt"
        s_args = [ "%s" % ps_input_file,
                   "%s.pdbqt" % os.path.splitext(ps_input_file)[0] ]
        
        cluster.progress("Peptide chain was not specified : a search for binding sites on the protein will be performed.")
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "pdb2pdbqt")

        if not os.path.exists("%s.pdbqt" % os.path.splitext(ps_input_file)[0]):
            cluster.progress("Could not convert input pdb file to pdbqt.")
            sys.exit(0)

    # we search for homologous patches in a list of pdbs

    if (p_args.query_chain and p_args.ligand_chain):
    
        if p_args.pdbid_list:

            pdbid_list = []
            with open(p_args.pdbid_list, "r") as pdbid_list_handle:
                for line in pdbid_list_handle:
                    currentline = line.rstrip().lower().replace(" ", "").split(",")
                    pdbid_list += currentline
            
            pdbid_list = list(set(pdbid_list))                              # we remove duplicates

            for pdbid in pdbid_list:
                if not re.match("\d[a-z0-9]{3}", pdbid):
                    msg = "Invalid pdb id : %s" % pdbid
                    cluster.progress(msg)
                    sys.exit(0)
                if not os.path.isfile(os.path.join(DFLT_PDB_BANK, "pdb%s.ent.gz" % pdbid)):
                    msg = "the pdb structure could not be processed, because of problems with the coordinates of the protein: %s" % pdbid
                    cluster.progress(msg)
                 # we must add something here to check that the pdb exists   
            
            if not os.path.isfile(ps_input_file):
                msg = "Problem when extracting patch from input pdb : %s does not exist." % ps_input_file
                cluster.progress(msg)
                sys.exit(0)

            targets_list = pdbid_list

            cmd = "PS2vsprotein_service"
            s_args = [ "map_item",
                       "%s" % ps_input_file,
                       "%s" % DFLT_PDB_BANK ]

            
        elif p_args.pdbbank_list:
            
            targets_list = []

            with open(p_args.pdbbank_list, "r") as pdbbank_list_handle:
                targets_list = [ line.strip().split()[0] for line in pdbbank_list_handle.readlines()[1:] ]

            cmd = "PS2vsproteinchain_service"
            s_args = [ "map_item",
                       "%s" % ps_input_file,
                       "%s" % DFLT_PDB_BANK ]
                    

        elif p_args.bs_bank:

            targets_list = glob.glob(os.path.join(DFLT_BS_BANK, "*-*[!+pep].pdb"))

            cmd = "PS2vsbindingsite_service"
            s_args = [ "map_item",
                       ps_input_file ]

    else:
        
        targets_list = glob.glob(os.path.join(DFLT_BS_BANK, "*-*[!+pep].pdb"))
        
        cmd = "BindingsitevsPS2_service"
        s_args = [ "map_item",
                   ps_input_file ]
        if p_args.query_chain:
            s_args.append("%s" % p_args.query_chain) 

    cluster.progress("Peptide binding site searching.")
    cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, map_list = targets_list, log_prefix = "patchsearch")


    # We look for all alig file produced and only keep targets with hits

    alig_files = glob.glob("*.alig.txt")
    pdbid_list = []

    with open("alignments.txt", "w") as alig_outfile:
        for alig_file in alig_files:
            size = 0
            with open(alig_file) as alig_filehandle:
                for line in alig_filehandle:
                    size += 1
                    alig_outfile.write(line)
            if size > 1:
                pdbid_list.append(alig_file.split(".alig.txt")[0])


    # we re-extract the patch(es) around the peptide but we include the peptide with it

    if (p_args.query_chain and p_args.ligand_chain):

        cmd = "getBindingSite.R"
        s_args = [ "%s" % p_args.pdb_file,
                   "--qchain %s" % p_args.query_chain,
                   "--lchain %s" % p_args.ligand_chain,
                   "--add calpha",
                   "--outfile %s" % ps_input_file,
                   "--ligand" ]

        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "patch_extract_with_ligand")

    # We align peptide with targets

        cmd = "SiteAlign_service"
        s_args = [ "map_item",
                   "%s" % p_args.ligand_chain,
                   "%s" % p_args.scoring_method,
                   "%s" % DFLT_PDB_BANK,
                   "%s" % DFLT_PDBQT_BANK ]
                   
        cluster.progress("Aligning input peptide with target.")
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, map_list = pdbid_list, log_prefix = "sitealign")

    else:
        
        cmd = "SiteAlignReverse_service"
        s_args = [ "map_item",
                   "%s.pdbqt" % os.path.splitext(ps_input_file)[0],
                   "%s" % p_args.scoring_method,
                   "%s" % DFLT_BS_BANK ]

        cluster.progress("Aligning binding sites bank peptides with protein query.")
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, map_list = pdbid_list, log_prefix = "sitealign")
        

    # We read the scores
    
    ligand_files = glob.glob("*-ligand-*.pdb")

    scores_tab = {}

    for ligand_file in ligand_files:
        m = re.match("(\S+)-ligand-(\d+)\.pdb", ligand_file)
        pdbid, ligandnum = m.groups()
        ligandnum = int(ligandnum)
        alig_file = "%s.alig.txt" % pdbid
        
        with open(ligand_file) as ligand_filehandle:
            affinity = ligand_filehandle.readline().split()[2]
            scores = readAligFile(alig_file, ligandnum)
            scores["ligand_file"] = ligand_file
            scores["affinity"] = "%0.3f" % float(affinity)
        
        scores_tab[pdbid] = scores


    # We put all the scores in a csv table

    with open("scores.csv", "w") as csv_file:
        writer = csv.writer(csv_file, delimiter = "\t")
        writer.writerow(["PDB Id", "Chains", "Hit Patch Length (# atoms)", "RMSD", "Coverage", "Mean distorsion", "Score", "Affinity"])
        for pdbid, scores in scores_tab.items():
            writer.writerow([pdbid, ",".join(scores["hit_chain_list"]), scores["match_len"], scores["rmsd"], scores["coverage"], scores['mean_distorsion'], scores["score"], scores["affinity"]])


    # We read the info in the PDB files and put them in the best scores table

    cluster.progress("Results analysis.")

    best_hits = dict(sorted(scores_tab.items(), key=lambda x: float(x[1]["affinity"]), reverse = False)[:100])

    # Add info to scores table
    
    for pdbid, scores in best_hits.items():

        pdbid = re.split('[_-]', pdbid)[0]
        pdbfile = os.path.join(DFLT_PDB_BANK, "pdb%s.ent.gz" % pdbid)
        if (p_args.query_chain and p_args.ligand_chain):
            chains = scores["hit_chain_list"]
        else:
            chains = scores["query_chain_list"]
        try:
            molecules, organisms = readPDBInfo(pdbfile, chains)
            scores["molecules"] = " + ".join(molecules)
            scores["organisms"] = " + ".join(organisms)
        except:
            scores["molecules"] = "could not retrieve information"
            scores["organisms"] = "could not retrieve information"

    # Format html table and ngl viewer

    cluster.progress("Formatting results.")

    formatScoresTable(best_hits, "pepit.table.html", wdir)

    if (p_args.query_chain and p_args.ligand_chain):
        
        formatViewer(p_args.pdb_file, best_hits, "patchsearch2.vis.html", "pepit.vis.html", wdir)
        
    else:
        
        formatViewer(p_args.pdb_file, best_hits, "patchsearch2.vis2.html", "pepit.vis.html", wdir)


if __name__ == "__main__":
    
    main()
    sys.exit(0)
