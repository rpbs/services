#!/bin/sh
set -ue

python /service/attract/download-attractfile.py $1 $2 > attract-downloaded.web

opt="-v $(pwd):$(pwd) -v /service:/service -v /dev/shm:/dev/shm -v /scratch:/scratch"

#local docker command
#docker run $opt attract /bin/bash -c "cd $(pwd);bash -x /service/attract/run-attract.sh"

#docker command on RPBS:
docker run -u $UID -e HOST_UID=$UID -h $(hostname) --add-host="slurmmaster:10.0.1.51" --privileged=true  $opt --rm docker-registry.rpbs.univ-paris-diderot.fr/attract \
/bin/bash -c "cd $(pwd);bash -x /service/attract/run-attract.sh"

# Reformat visualization
python << EOF
import sys
from jinja2 import Template
models = [l.strip() for l in open("result.list").read().split("\n") if len(l.strip())]
data = []
for i in range(len(models)):
    data.append(('Model %d' % (i+1), '', '', models[i]))
with open('bestModels.html', 'wt') as sink:
    template = Template(open('/service/templates/attract.vis.html').read())
    sink.write(template.render(data = data, data_count = len(data), color = 'color-succession'))
EOF