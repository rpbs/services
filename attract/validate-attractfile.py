import sys, os
guidir = os.environ["ATTRACTDIR"] + "/../gui"
sys.path.insert(0, guidir)
import spyder, Spyder
import attractmodel

inputfile = sys.argv[1]
inputdata = open(inputfile).read()
inputdir = os.path.split(inputfile)[0]
if len(inputdir): os.chdir(inputdir)
spydertypename, spyderdict = spyder.core.parse(inputdata)
spydertype = getattr(Spyder, spydertypename)
model = spydertype.fromdict(spyderdict)

model = model.convert(Spyder.AttractModel)

assert len(model.partners) == 2
assert model.search != "custom"
if model.search == "random":
  assert model.structures <= 100000
assert len(model.grids)
model.np = 8


#enable this for a quick test:
"""
model.search = "random"
model.structures = 100
model.grids = []
model.partners[0].gridname = None
model.validate()
"""

model.tofile(sys.argv[2])