#!/usr/bin/env python

import sys

cmd = sys.argv[1]

# MTi cluster imports

import cluster

cluster.runTasks(cmd, [], job_opts = '-c 12', partition = "bigmemory", qos = "onejobpernode", docker_img = "attract")
