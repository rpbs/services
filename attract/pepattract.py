"""
Command-line front-end for pepATTRACT
Requires ATTRACT to be installed
"""

import sys
import os
assert "ATTRACTDIR" in os.environ and "ATTRACTTOOLS" in os.environ and "ATTRACTGUI" in os.environ #need to run the script inside a docker container with ATTRACT installed
sys.path.append(os.environ["ATTRACTGUI"])
import spyder, Spyder #need to have Spyder installed
from attractmodel import AttractPeptideModel

import argparse
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--seq",help="Peptide sequence")
parser.add_argument("--pdb",help="Protein PDB file")
parser.add_argument("--iattract",help="Perform iATTRACT refinement", action="store_true")

args = parser.parse_args()
assert os.path.split(args.pdb)[0] == "", args.pdb #PDB must be in same directory
# TODO: validate contents of PDB
model = AttractPeptideModel(
  args.pdb, 
  args.seq,   
  clustering = True, 
  analyze_interface = True,
  use_iattract = False,
  np = 8, #8 cores  
)
if args.iattract:
  model.use_iattract = True
os.system("cat /dev/null > pepattract.sh")
model.tofile("pepattract.web")
os.system("python $ATTRACTTOOLS/gui-generate.py pepattract.web")
os.system("./pepattract.sh")

    