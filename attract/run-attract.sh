python /service/attract/validate-attractfile.py attract-downloaded.web attract-validated.web
python $ATTRACTTOOLS/gui-deploy.py attract-validated.web . > attract.web
python $ATTRACTTOOLS/gui-generate.py attract.web
./attract.sh >& attract.out
$ATTRACTDIR/shm-clean
python $ATTRACTTOOLS/splitmodel result.pdb > result.list
rm -f *.grid *.gridheader
tar -czf result.tgz *.*
