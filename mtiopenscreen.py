#!/usr/bin/env python

import os, sys, shutil, glob, time
import argparse
import sqlite3
import fileinput
import random
import pkg_resources

import PyPDB.PyPDB as PDB
import Mol2.Mol2 as Mol2
import sdf.sdf as sdf

import cluster.cluster as cluster

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('MTiOpenScreen','templates'))

# Constants
DFLT_MOBYLE_JOBS_PATH = "/data/jobs"
DFLT_GRID_CENTER_CRDS = None
DFLT_GRID_CENTER_RES  = None
DFLT_GRID_SPACING     = 1
DFLT_GRID_PTS         = (19, 13, 18)
DFLT_MAX_ATOMS        = 300 # including H is present
ENVIRONMENT_MODULE    = "opendocking/1.0-rpbs"
SUBDIR_COUNT          = 256 # Number of subdirectories for file distribution
PDB_OUTDIR            = 'pdb_out' # Output directory for PDB files
VINA_INFILE           = 'vina.inp' # Vina input file
VINA_OUTDIR           = 'vina_out' # Directory with intermediate results
VINA_NUM_MODES        = 3 # Number of models to generate
VINA_EXHAUSTIVENESS   = 8 # Vina option
VINA_CPUS_PER_JOB     = 4 # NR of CPUs per vina run
VINA_TABLE_ROWS       = 100 # Maximum number of ligands in the results table
VINADB_MAX_SELECTED   = 11000 # Maximum number of processed ligands
USERDB_MAX_COMPOUNDS  = 5000 # Maximum number in user-specified compound database
USERDB_DIR            = 'ligand_db'   # User-specified compound database directory

jobId = os.path.basename(os.getcwd())
serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]
wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

# Databanks
BANKS_MTIOPENSCREEN = '/shared/banks/mtiopenscreen/'
BANKS_DB = os.path.join(BANKS_MTIOPENSCREEN, 'ligand.sqlite')


def get_arg_parser():
    """
    Create command line arguments parser.
    """

    parser = argparse.ArgumentParser(description='MTiOpenScreen parser')

    # Global parameters

    parser.add_argument("-v", "--verbose", dest="verbose",
                        action="store_true", default=False, help="verbose mode")

    parser.add_argument("-r", "--receptor", dest="receptor_file", action="store", default=None,
                        help="input receptor file (pdb)",
                        metavar="FILE")

    parser.add_argument("--receptorFormat", dest="receptor_format",
                        action="store", default="pdb", choices=["mol2", "pdb"],
                        help="input receptor file format (mol2)")

    parser.add_argument("-b", "--built-in-db", dest="built_in_db",
                        action="store", default=None, help="built-in database")
                        
    parser.add_argument("-d", "--user-db", dest="user_db",
                        action="store", default=None, help="custom user database")                        

    parser.add_argument("-l", "--limit", dest="limit",
                        action="store", default=VINADB_MAX_SELECTED, help="limit the number of ligands")

    # Ligand database filters

    groupFilters = parser.add_argument_group('Database filters')

    groupFilters.add_argument("--isLeadLike", dest="filter_isleadlike",
                              action="store", default=None, choices=['Y', 'N'],
                              help="Is the compound lead-like?")

    groupFilters.add_argument("--library", dest="filter_library",
                              action="store", default=None, choices=['iPPI-lib', 'diverse-lib'],
                              help="Only compounds from selected library.")

    groupFilters.add_argument("--HBA", dest="filter_hba", action="store", default=None, metavar='<numeric>',
                              help="Only compounds with the number of H acceptors < (value)")

    groupFilters.add_argument("--HBD", dest="filter_hbd", metavar='<numeric>', action="store", default=None,
                              help="Only compounds with the number of H donors < (value)")

    groupFilters.add_argument("--LogP", dest="filter_logp", action="store", default=None, metavar='<numeric>',
                              help="Only compounds with the LogP value < (value)")

    groupFilters.add_argument("--MW", dest="filter_mw", action="store", default=None, metavar='<numeric>',
                              help="Only compounds with the molecular weight < (value)")

    groupFilters.add_argument("--nRot", dest="filter_nrot", action="store", default=None, metavar='<numeric>',
                              help="Only compounds with the number of rotatable bonds < (value)")

    groupFilters.add_argument("--TPSA", dest="filter_tpsa", action="store", default=None, metavar='<numeric>',
                              help="Only compounds with the topological polar surface area < (value)")
    # Grid specification

    groupAutogrid = parser.add_argument_group('Pocket options')

    groupAutogrid.add_argument("--gridpts", dest="gridpts",
                               action="store", default=DFLT_GRID_PTS,
                               help="number of steps for the grid",
                               metavar="x,y,z")

    groupAutogrid.add_argument("--gridspacing", dest="gridspacing",
                               action="store", default=DFLT_GRID_SPACING,
                               help="grid spacing (%f)" % DFLT_GRID_SPACING,                         
                               metavar="x")

    groupAutogrid.add_argument("--gridcentercrds", dest="gridcenter_coordinates",
                              action="store", default=DFLT_GRID_CENTER_CRDS,
                              help="grid center coordinates",
                              metavar="x,y,z")

    groupAutogrid.add_argument("--gridcenterres", dest="gridcenter_residues",
                              action="store", default=DFLT_GRID_CENTER_RES,
                              help="residue list for grid center calculation",
                              metavar="_A_THR_1__,_A_THR_2__,_A_ILE_7__")

    return parser


def check_args(args):
    """
    Check command line arguments for validity.
    """

    if (not args.receptor_file):
        raise Exception("No receptor_file specified")

    # Copy the receptor to the working directory
    receptor_dest = 'receptor.%s' % args.receptor_format
    if not os.path.isfile(receptor_dest):
        shutil.copy(args.receptor_file, receptor_dest)
        args.receptor_file = receptor_dest

    # Receptor is MOL2, convert it to PDB
    if args.receptor_format == "mol2":
        mol22pdb([args.receptor_file])
        args.receptor_format = 'pdb'
        args.receptor_file = '%s.%s' % (os.path.splitext(args.receptor_file)[0], args.receptor_format)
    else:
        # Remove heterogroups
        pdb_file = PDB.PDB(args.receptor_file, hetSkip=1)
        pdb_file.out(args.receptor_file, info=1)

    # Receptor is PDB
    status, msg = PDB.isPDB(args.receptor_file)
    if not status:
        raise Exception(msg)

    # User-specified database format must be valid mol2 or sdf
    userdb_dest = 'ligands.%s'
    if args.user_db != None:
        data = Mol2.mol2_set(args.user_db)
        if not data.num_compounds:
            # Convert from SDF to MOL2
            shutil.copy(args.user_db, userdb_dest % 'sdf')
            sdf2mol2(userdb_dest % 'sdf', split = False)
            args.user_db = userdb_dest % 'mol2'
            data = Mol2.mol2_set(args.user_db)
        else:
            # Valid MOL2, copy as ligands.mol2
            shutil.copy(args.user_db, userdb_dest % 'mol2')
            args.user_db = 'ligands.mol2'

        # Check the MOL2 file
        if not data.num_compounds:
            raise Exception("Could not convert user database (%s) to MOL2.", args.user_db)
        if data.num_compounds > USERDB_MAX_COMPOUNDS:
            raise Exception("User database (%s) has %u compounds (less than %u compounds is allowed)" % (args.user_db, data.num_compounds, USERDB_MAX_COMPOUNDS))
        for cmpnd in data.compounds.keys():
            if data.compounds[cmpnd].num_atoms > DFLT_MAX_ATOMS:
                raise Exception("Ligand number of atoms (%d) cannot exceed (%d)" % (data.compounds[cmpnd].num_atoms, DFLT_MAX_ATOMS))
        # Not it's valid MOL2 file
        args.user_db_format = 'mol2'


def subdirs_create(path, nr_dirs = SUBDIR_COUNT):
    """
    Create a directory with N subdirectories in it, ordered from 0..(nr_dirs - 1)
    """

    os.mkdir(path)
    for i in range(0, nr_dirs):
        os.mkdir("%s/%d" % (VINA_OUTDIR, i))


def setupGrid(args):
    """
    Setting up grid from parameters
    """

    MAXGRIDPOINTS = 200 
    if not args.gridcenter_coordinates:
        if args.receptor_format == "mol2":
            # We need to convert into pdb
            args.receptor_file = mol22pdb([args.receptor_file])
            # x = None
            x = PDB.PDB(args.receptor_file)
        if args.receptor_format == "pdb":
            x = PDB.PDB(args.receptor_file)
        if x is not None:
            if args.gridcenter_residues is not None:
                rList = args.gridcenter_residues.split(",")
                try:
                    y = x.subPDBFromResList(rList)
                    if (y is None) or (len(y) == 0):
                        pass
                    else:
                        x = y
                except IndexError:
                    raise IndexError("Invalid list of residues: '%s', see help." % str(rList))
            args.gridcenter_coordinates = x.BC()
            dx, dy, dz = x.gridSize()
            # print dx, dy, dz
            dx = (dx + 3.) * 2.
            dy = (dy + 3.) * 2.
            dz = (dz + 3.) * 2.
            # print dx, dy, dz
            nx = int(dx / args.gridspacing) + 1
            ny = int(dy / args.gridspacing) + 1
            nz = int(dz / args.gridspacing) + 1
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                args.gridspacing = 0.6
                nx = int(dx / args.gridspacing) + 1
                ny = int(dy / args.gridspacing) + 1
                nz = int(dz / args.gridspacing) + 1           
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                args.gridspacing = 0.8
                nx = int(dx / args.gridspacing) + 1
                ny = int(dy / args.gridspacing) + 1
                nz = int(dz / args.gridspacing) + 1
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                # ERROR: WE EXIT
                msg = "Sorry: too big protein for blind docking. Please specify manually either grid coordinates or residues."
                cluster.progress(msg)
                sys.exit(0)
            if float(args.gridspacing) >= 0.6:
                msg = "Warning: too big protein for blind docking using default spacing of 0.375. Increased up to %s." % args.gridspacing
                cluster.progress(msg)

            args.gridpts = (nx, ny, nz)
            # print nx, ny, nz

            args.gridpts = ",".join(map(str,args.gridpts))
            args.gridcenter_coordinates = ",".join(map(str,args.gridcenter_coordinates))

    else:
        # We have user specified grid center and steps
        pass


def mol22mol2(mol2_file, split = True):
    """
    Splits a mol2 file to mol2 files
    """

    command = "babel"
    args = []
    if split:
        args.append("-m")
    args += ["-i mol2 %s" % (mol2_file), "-o mol2 %s" % (mol2_file)]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "mol22mol2")


def sdf2mol2(sdf_file, split = True):
    """
    Splits and converts a sdf file to mol2 files
    """

    cluster.progress("Converting and splitting ligand file from sdf to mol2")

    command = "babel"
    (label, ext) = os.path.splitext(sdf_file)
    mol2_file = label + ".mol2"
    args = []
    if split:
        args.append("-m")
    args += ["-i sdf %s" % (sdf_file), "-o mol2 %s" % (mol2_file)]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "sdf2mol2")


def prepareLigandFile(ligand_label, tasks):
    """
    Converts a mol2 ligand file to pdbqt
    """

    command = "Prepare_ligand"
    args = [ligand_label]

    cluster.runTasks(command, args, tasks, environment_module = ENVIRONMENT_MODULE, log_prefix = "prepare_ligand")


def prepareReceptorFile(receptor_file):
    """
    Converts a pdb receptor file to pdbqt
    """

    command = "Prepare_receptor"
    (label, ext) = os.path.splitext(receptor_file)
    pdbqt_file = label + ".pdbqt"
    args = [receptor_file]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "prepare_receptor")

    return pdbqt_file


def write_vina_input(args, file_name):
    """
    Generate vina input file from parameters. 
    """

    # Vina accepts grid in [A]
    #ssize = float(args.gridspacing)
    #x,y,z = args.gridpts
    #grid_box = (x * ssize, y * ssize, z * ssize)
    grid_box = args.gridpts
    with open(file_name, "wt") as vina_inp:
        vina_inp.write("receptor = %s\n" % args.receptor_file)
        vina_inp.write("center_x = %f\ncenter_y = %f\ncenter_z = %f\n" % args.gridcenter_coordinates)
        vina_inp.write("size_x = %d\nsize_y = %d\nsize_z = %d\n" % grid_box)
        vina_inp.write("num_modes = %d\n" % VINA_NUM_MODES)
        vina_inp.write("exhaustiveness = %d\n" % VINA_EXHAUSTIVENESS)


def read_vina_score(file_name):
    """
    Read vina model name/energy from a pdbqt file.
    """

    scores = []
    with open(file_name, 'r') as vina_out:
        line = vina_out.readline()
        while len(line) > 0:
            if line.startswith('MODEL'):
                model  = line.split()[1]      # MODEL ID
                result = vina_out.readline()  # REMARK VINA RESULT
                name   = vina_out.readline()  # REMARK VINA NAME
                tors   = vina_out.readline()  # REMARK N active torsions:
                scores.append((name.split()[2], model, float(result.split()[3]), tors.split()[1]))
            line = vina_out.readline()
    return scores


def prepare_user_db(args):
    """
    Convert user-provided database in mol2 format to splitted pdbqt files.
    """

    # Make local ligand database

    label = 'ligand'
    dbfile = '%s/%s.%s' % (USERDB_DIR, label, args.user_db_format)
    os.mkdir(USERDB_DIR)
    shutil.copy(args.user_db, dbfile)

    # Split mol2 user database to files

    if args.user_db_format == 'mol2':
        mol22mol2(dbfile)
    else:
        sdf2mol2(dbfile)

    # Identify split ligands

    os.remove(dbfile)
    ligands = glob.glob('%s/%s*.mol2' % (USERDB_DIR, label))
    if len(ligands) == 0:
        raise Exception("User compound database, failed to split compounds.")

    # Convert to pdbqt and hydrogenate

    cluster.progress('... converting %u ligands' % len(ligands))
    cwd = os.getcwd()
    os.chdir(USERDB_DIR)
    prepareLigandFile(label, len(ligands))
    os.chdir(cwd)

    return glob.glob('%s/*.pdbqt' % USERDB_DIR)


def prepare_filter_query(args):
    """
    Prepare ligand database SQL query filters based on the parameters.
    """

    sql_where = []
    sql_param = []
    if args.filter_library != None:
        sql_where.append('Library = ?')
        sql_param.append(args.filter_library)
    if args.filter_isleadlike != None:
        sql_where.append('IsLeadLike = ?')
        sql_param.append(args.filter_isleadlike)
    else:
        # IsLeadLike excludes other parameters
        if args.filter_hba != None:
            sql_where.append('HBA < ?')
            sql_param.append(args.filter_hba)
        if args.filter_hbd != None:
            sql_where.append('HBD < ?')
            sql_param.append(args.filter_hbd)
        if args.filter_logp != None:
            sql_where.append('LogP < ?')
            sql_param.append(args.filter_logp)
        if args.filter_mw != None:
            sql_where.append('MW < ?')
            sql_param.append(args.filter_mw)
        if args.filter_nrot != None:
            sql_where.append('nRot < ?')
            sql_param.append(args.filter_nrot)
        if args.filter_tpsa != None:
            sql_where.append('TPSA < ?')
            sql_param.append(args.filter_tpsa)

    # Not empty filters
    sql_where = ' AND '.join(sql_where)
    if len(sql_where) > 0:
        sql_where = 'WHERE ' + sql_where

    return (sql_where, tuple(sql_param))


def select_ligands(args):
    """
    Select a list of ligand files either from user-provided database or filtered ligand database.
    """

    ligands = []

    # Convert user database

    if args.user_db != None:
        ligands = prepare_user_db(args)

    elif args.built_in_db != None:
        ligands = glob.glob('%s/%s/*.pdbqt' % (BANKS_MTIOPENSCREEN, args.built_in_db))

    else:
        # Filter our database
        (sql_where, sql_param) = prepare_filter_query(args)
        conn = sqlite3.connect(BANKS_DB)
        for row in conn.execute('SELECT IDCompound FROM descriptors ' + sql_where, sql_param):
            subdir = str(row[0] % SUBDIR_COUNT)
            ligands.append('%s/pdbqt/%s/%s.pdbqt' % (BANKS_MTIOPENSCREEN, subdir, str(row[0])))
        conn.close()

    # Random choice of maximum N ligands

    if len(ligands) > int(args.limit):
        ligands = random.sample(ligands, int(args.limit))

    return ligands


def run_openscreen_map(args):
    """
    Map ligand files to cluster jobs and start screening.
    """

    ligands = select_ligands(args)
    cluster.progress('... selected %u ligands' % len(ligands))

    if len(ligands) == 0:
        raise Exception("No ligands matching the filters found, at least 1 ligand is required.")

    # Create vina input file

    write_vina_input(args, VINA_INFILE)
    cluster.progress('... written vina.inp file')

    # Create the output directories

    subdirs_create(VINA_OUTDIR)

    # Start the jobs
    cluster.progress('... starting %u vina jobs' % len(ligands))
    args = [VINA_INFILE, args.receptor_file, str(SUBDIR_COUNT), "map_item"]
    cluster.runTasks("Vina_service", args, map_list=ligands, environment_module = ENVIRONMENT_MODULE, job_opts='-c %d' % VINA_CPUS_PER_JOB, log_prefix = "vina")


def run_openscreen_reduce(output_file_label, args, nr_ligands=1000):
    """
    Sort the ligand ranking and assemble X best configurations of Y best ligands.
    """

    # Extract the ranking

    ranking = {}
    ranking_files_pdbqt = []
    for root, _, files in os.walk(VINA_OUTDIR):
        for file in files:
            file = os.path.join(root, file)
            ranking[file] = read_vina_score(file)
            if len(ranking[file]) == 0:
                del ranking[file]
            else:
                ranking_files_pdbqt.append(file)

    # Sort ligands based on the energy of their best model

    ranking_files_pdbqt.sort(key=lambda x: ranking[x][0][2])
    cluster.progress('... sorted %u ranked files' % len(ranking_files_pdbqt))

    # Extract N-best ligands

    if len(ranking_files_pdbqt) > nr_ligands:
        ranking_files_pdbqt = ranking_files_pdbqt[0 : nr_ligands]
    if len(ranking_files_pdbqt) == 0:
        raise Exception("No ranked models found.")

    # Merge results into a PDBQT and mol2 output file

    cluster.progress('... merging results')

    model_id = 1
    pdbqt_file_FH = None
    pdbqt_files_names = []
    with open(output_file_label + ".pdbqt", 'wt') as sink:
        for line in fileinput.input(ranking_files_pdbqt):
            if line.startswith('MODEL'):
                try:
                    pdbqt_file_FH.close()
                except:
                    pass
                pdbqt_file_name = os.path.join(VINA_OUTDIR, "pose" + str(model_id) + ".pdbqt")
                pdbqt_file_FH = open(pdbqt_file_name, "w")
                pdbqt_files_names.append(pdbqt_file_name)
                line = 'MODEL %d\n' % model_id
                model_id += 1
            sink.write(line)
            pdbqt_file_FH.write(line)
        pdbqt_file_FH.close()

    pdb_files_names = pdbqt2pdb(pdbqt_files_names)
    mol2_files_names = pdb2mol2(pdb_files_names)

    with open(output_file_label + ".mol2", 'wt') as sink:
        for fname in mol2_files_names:
            if not os.path.isfile(fname):
                continue
            with open(fname) as finput:
                for line in finput:
                    sink.write(line)

    # Produce a results table
    conn = sqlite3.connect(BANKS_DB)
    conn.row_factory = sqlite3.Row
    table_file = '%s.table.csv' % output_file_label

    # Create a header

    header = ['nRot']

    if args.user_db is None and args.built_in_db is None:
        # Using our database, provide all descriptors
        header += ['Library', 'isLeadLike', 'HBA', 'HBD', 'LogP', 'MW', 'TPSA']

    with open(table_file, 'wt') as sink:
        sink.write(','.join(['Compound', 'Model ID', 'Energy'] + header) + '\n')
        for file in [] + ranking_files_pdbqt:
            # Take only the best model for each file
            (name, model, energy, tors) = ranking[file][0]
            fields = [name, model, str(energy)]
            if args.user_db is None and args.built_in_db is None:
                id_compound = int(os.path.splitext(os.path.basename(file))[0])
                row = conn.execute('SELECT * FROM descriptors WHERE IDCompound = ?', (id_compound,)).fetchone()
                [fields.append(str(row[key])) for key in header]
            else:
                fields.append(str(tors))
            sink.write(','.join(fields) + '\n')
    conn.close()

    # Extract K-best ligands

    if len(ranking_files_pdbqt) > VINA_TABLE_ROWS:
        ranking_files = ranking_files_pdbqt[0 : VINA_TABLE_ROWS]

    # Produce PDBQT files for K-best ligands

    model_id = 1
    args.pdb_out = []
    ligands_pdbqt = []
    os.mkdir(PDB_OUTDIR)
    for file_name in ranking_files_pdbqt:
        model_label = '%s/model%d' % (PDB_OUTDIR, model_id)
        with open(model_label+".pdbqt", 'wt') as sink:
            with open(file_name, 'r') as source:
                for line in source:
                    sink.write(line)
                    # Only the first model of each file
                    if line.startswith('ENDMDL'):
                        break
                # Append to list (name, model, energy, filename)
                (name, model, energy, tors) = ranking[file_name][0]
                args.pdb_out.append( (name, model_id, energy, model_label) )
                ligands_pdbqt.append(model_label+".pdbqt")
                # 1 model per file
                model_id += 1
                
    # Convert from pdbqt to pdb            

    ligands_pdb = pdbqt2pdb(ligands_pdbqt)
        
    #Convert from pdb to mol2 for download and then from mol2 to pdb to get connectivities for visualization

    ligands_mol2 = pdb2mol2(ligands_pdb)
    mol22pdb(ligands_mol2)

    # Remove partial result files
    cluster.progress('... cleaning up')
    if os.path.isdir(VINA_OUTDIR):
        shutil.rmtree(VINA_OUTDIR)
    if os.path.isdir(USERDB_DIR):
        shutil.rmtree(USERDB_DIR)


def pdbqt2pdb (ligands):

    command = "PDBQT2PDB_service"
    args = ["map_item"]
  
    cluster.runTasks(command, args, map_list=ligands, environment_module = ENVIRONMENT_MODULE, log_prefix = "pdbqt2pdb")

    return [os.path.splitext(ligand)[0] + ".pdb" for ligand in ligands]


def pdb2mol2 (ligands):

    command = "Babel_service"
    args = ["pdb", "mol2", "map_item"]

    cluster.runTasks(command, args, map_list=ligands, environment_module = ENVIRONMENT_MODULE, log_prefix = "pdb2mol2")
    
    return [os.path.splitext(ligand)[0] + ".mol2" for ligand in ligands]


def mol22pdb (ligands):

    command = "Babel_service"
    args = ["mol2", "pdb", "map_item"]

    cluster.runTasks(command, args, map_list=ligands, environment_module = ENVIRONMENT_MODULE, log_prefix = "mol22pdb")

    return [os.path.splitext(ligand)[0] + ".pdb" for ligand in ligands]


def format_table(table, order = '[2,"asc"]', title = "Tabulated ligands", tpl = 'mtiopenscreen.table.html', limit = 0, wdir = "./"):
    """
    Format CSV file as an HTML table.
    """

    html_file = '%s.html' % os.path.splitext(table)[0]
    with open(html_file, 'wt') as sink:
        # Read CSV data
        lineno = 0
        data = []
        header = []
        with open(table, 'r') as csv:
            for line in csv:
                if lineno == 0:
                    header = line.split(',')
                else:
                    data.append(line.split(','))
                if limit > 0 and lineno >= limit:
                    break
                lineno += 1
        # Generate output table
        template = jinja_env.get_template(tpl)
        sink.write(template.render(data = data, data_count = len(data), order = order, header = header, title = title, wdir = wdir))


def format_visualization(args, output_file, wdir = "./"):
    """
    Format HTML file with output visualization.
    """

    file_label = os.path.splitext(output_file)[0]
    html_file = '%s.html' % file_label
    with open(html_file, 'wt') as sink:
        template = jinja_env.get_template('mtiopenscreen.vis.html')
        sink.write(template.render(receptor = args.orig_receptor_file, data = args.pdb_out, data_count = len(args.pdb_out), wdir = wdir))


def main():
    # Parse options
    cluster.progress('1/5 input processing')
    parser = get_arg_parser()
    args = parser.parse_args()
    check_args(args)

    # We need to setup grid
    setupGrid(args)
    if not isinstance(args.gridpts, tuple):
        args.gridpts = tuple([int(pt) for pt in args.gridpts.split(',')])
    if not isinstance(args.gridcenter_coordinates, tuple):
        args.gridcenter_coordinates = tuple([float(pt) for pt in args.gridcenter_coordinates.split(',')])

    # Convert receptor file from pdb to pdbqt and add hydrogens
    cluster.progress('2/5 converting receptor file')
    prepareReceptorFile(args.receptor_file)
    args.orig_receptor_file = args.receptor_file
    args.receptor_file = '%s.pdbqt' % os.path.splitext(args.receptor_file)[0]

    # Perform the screening
    cluster.progress('3/5 screening')
    run_openscreen_map(args)

    # Collect/merge the results
    cluster.progress('4/5 merging results')
    run_openscreen_reduce('output', args, nr_ligands = 1500)

    # Format output
    cluster.progress("5/5 formatting output")

    if (args.built_in_db == "UMR8601"):
        shutil.copy2("%s/umr8601.png" % pkg_resources.resource_filename("MTiOpenScreen", "doc"), "umr8601.png")

    format_table('output.table.csv', wdir = wdir)
    format_visualization(args, 'output.vis.pdb', wdir = wdir)

    cluster.progress("finished")

if __name__ == "__main__":
    main()
    sys.exit(0)
