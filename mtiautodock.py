#!/usr/bin/env python

import os, sys, csv, re, glob, shutil
import argparse
import cluster.cluster as cluster

import PyPDB.PyPDB as PDB
import Mol2.Mol2 as Mol2
import sdf.sdf as sdf

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('MTiAutoDock','templates'))


# Constants
DFLT_WDIR             = "."
DFLT_MOBYLE_JOBS_PATH = "/data/jobs"
DFLT_GRID_CENTER_CRDS = None # (0,0,0)
DFLT_GRID_CENTER_RES  = None
DFLT_GRID_SPACING     = 0.375
DFLT_GRID_PTS         = None # (126,126,126)
DFLT_MAX_COMPOUNDS    = 10
DFLT_MAX_ATOMS        = 300 # including H is present
ENVIRONMENT_MODULE    = "opendocking/1.0-rpbs"

jobId = os.path.basename(os.getcwd())
serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]
wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)


def argValues(args):
    """
    @return: a string describing the options 
    """
    try:
        rs = "%s\n" % (sys.argv[0])
        for option in args.__dict__.keys():
            rs += ("  %-15s: %s\n") % (str(option),getattr(args,option))
        return rs
    except:
        return ""


def cmdLine():
    
    parser = argparse.ArgumentParser(description='MTiAutodock parser')
    
    parser.add_argument("-w", "--working-directory", dest="wdir",
                        action="store", default=DFLT_WDIR, 
                        help="Working directory (def=%s)" % DFLT_WDIR,
                        metavar="DIR")

    parser.add_argument("-l", "--ligand", dest="ligand_file",
                        action="store", default=None,
                        help="input ligand file (mol2)",
                        metavar="FILE")

    parser.add_argument("--ligandFormat", dest="ligand_format",
                        action="store", default="mol2", choices=["mol2","sdf"],
                        help="input ligand file format (mol2)",
                        metavar="FILE")

    parser.add_argument("-r", "--receptor", dest="receptor_file",
                        action="store", default=None,
                        help="input receptor file (pdb)",
                        metavar="FILE")

    parser.add_argument("--receptorFormat", dest="receptor_format",
                        action="store", default="pdb", choices=["mol2","pdb"],
                        help="input receptor file format (mol2)")

    groupAutogrid = parser.add_argument_group('Pocket options')

    groupAutogrid.add_argument("--gridpts", dest="gridpts",
                               action="store", default=DFLT_GRID_PTS,
                               help="number of points for the grid",                         
                               metavar="x,y,z")

    groupAutogrid.add_argument("--gridspacing", dest="gridspacing",
                               action="store", default=DFLT_GRID_SPACING,
                               help="grid spacing (%f)" % DFLT_GRID_SPACING,                         
                               metavar="x")

    groupAutogrid.add_argument("--gridcentercrds", dest="gridcenter_coordinates",
                              action="store", default=DFLT_GRID_CENTER_CRDS,
                              help="grid center coordinates",
                              metavar="x,y,z")

    groupAutogrid.add_argument("--gridcenterres", dest="gridcenter_residues",
                              action="store", default=DFLT_GRID_CENTER_RES,
                              help="grid center coordinates",
                              metavar="_A_THR_1__,_A_THR_2__,_A_ILE_7__")

    return parser


def checkArgValues(args):

    status = True
    msgList = []
    if (not args.receptor_file):
        msg = "No receptor_file specified"
        msgList.append(msg)
        status = False
        return False, msg

    if (not args.ligand_file):
        msg = "No ligand_file specified"
        return False, msg

    # Copy the receptor to the working directory
    receptor_dest = 'receptor.%s' % args.receptor_format
    if not os.path.isfile(receptor_dest):
        shutil.copy(args.receptor_file, receptor_dest)
        args.receptor_file = receptor_dest

    # Receptor is MOL2, convert it to PDB
    if args.receptor_format == "mol2":
        mol22pdb(args.receptor_file)
        args.receptor_format = 'pdb'
        args.receptor_file = '%s.%s' % (os.path.splitext(args.receptor_file)[0], args.receptor_format)
    else:
        # Remove heterogroups
        pdb_file = PDB.PDB(args.receptor_file, hetSkip=1)
        pdb_file.out(args.receptor_file, info=1)

    # Receptor is PDB
    status, msg = PDB.isPDB(args.receptor_file)
    if not status:
        return False, msg

    # Convert from SDF to MOL2
    ligand_dest = 'ligands_in.%s'
    if args.ligand_format == "sdf":
        shutil.copy(args.ligand_file, ligand_dest % 'sdf')
        sdf2mol2(ligand_dest % 'sdf', split = False)
    else:
        shutil.copy(args.ligand_file, ligand_dest % 'mol2')
    args.ligand_format = "mol2"
    args.ligand_file = ligand_dest % 'mol2'

    # Ligand is mol2
    data = Mol2.mol2_set(args.ligand_file)
    if not data.num_compounds:
        return False, "Ligand does not seem a valid mol2 file"
    if data.num_compounds > DFLT_MAX_COMPOUNDS:
        return False, "Ligand number (%d) exceeds maximum of %d" % (data.num_compounds, DFLT_MAX_COMPOUNDS)
    for cmpnd in data.compounds.keys():
        # print data.compounds[cmpnd].__dict__
        if data.compounds[cmpnd].num_atoms > DFLT_MAX_ATOMS:
            return False, "Ligand number of atoms (%d) cannot exceed (%d)" % (data.compounds[cmpnd].num_atoms, DFLT_MAX_ATOMS)

    return True, ""


def setupGrid(args):
    """
    Setting up grid from parameters
    """
 
    MAXGRIDPOINTS = 200 
    if not args.gridcenter_coordinates:
        if args.receptor_format == "mol2":
            # We need to convert into pdb
            args.receptor_file = mol22pdb(args.receptor_file)
            # x = None
            x = PDB.PDB(args.receptor_file)
        if args.receptor_format == "pdb":
            x = PDB.PDB(args.receptor_file)
        if x is not None:
            if args.gridcenter_residues is not None:
                rList = args.gridcenter_residues.split(",")
                try:
                    y = x.subPDBFromResList(rList)
                    if (y is None) or (len(y) == 0):
                        pass
                    else:
                        x = y
                except IndexError:
                    raise IndexError("Invalid list of residues: '%s', see help." % str(rList))
            args.gridcenter_coordinates = x.BC()
            dx, dy, dz = x.gridSize()
            # print dx, dy, dz
            dx = (dx + 3.) * 2.
            dy = (dy + 3.) * 2.
            dz = (dz + 3.) * 2.
            # print dx, dy, dz
            nx = int(dx / args.gridspacing) + 1
            ny = int(dy / args.gridspacing) + 1
            nz = int(dz / args.gridspacing) + 1
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                args.gridspacing = 0.6
                nx = int(dx / args.gridspacing) + 1
                ny = int(dy / args.gridspacing) + 1
                nz = int(dz / args.gridspacing) + 1           
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                args.gridspacing = 0.8
                nx = int(dx / args.gridspacing) + 1
                ny = int(dy / args.gridspacing) + 1
                nz = int(dz / args.gridspacing) + 1
            if (nx > MAXGRIDPOINTS) or (ny > MAXGRIDPOINTS) or (nz > MAXGRIDPOINTS):
                # ERROR: WE EXIT
                msg = "Sorry: too big protein for blind docking. Please specify manually either grid coordinates or residues."
                cluster.progress(msg)
                sys.exit(0)
            if float(args.gridspacing) >= 0.6:
                msg = "Warning: too big protein for blind docking using default spacing of 0.375. Increased up to %s." % args.gridspacing
                cluster.progress(msg)

            args.gridpts = (nx, ny, nz)
            # print nx, ny, nz

            args.gridpts = ",".join(map(str,args.gridpts))
            args.gridcenter_coordinates = ",".join(map(str,args.gridcenter_coordinates))


    else:
        # We have user specified grid center and steps
        pass


def sdf2mol2(sdf_file, split = True):
    """
    Splits and converts a sdf file to mol2 files
    """
    cluster.progress("Converting and splitting ligand file from sdf to mol2")

    command = "babel"
    (label, ext) = os.path.splitext(sdf_file)
    mol2_file = label + ".mol2"
    args = []
    if split:
        args.append("-m")
    args += ["-i sdf %s" % (sdf_file), "-o mol2 %s" % (mol2_file)]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "sdf2mol2")


def mol22mol2(mol2_file, split = True):
    """
    Splits a mol2 file to mol2 files
    """

    command = "babel"
    args = []
    if split:
        args.append("-m")
    args += ["-i mol2 %s" % (mol2_file), "-o mol2 %s" % (mol2_file)]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "mol22mol2")

def mol22pdb(mol2_file):
    """
    Converts a mol2 file to pdb file
    """
    cluster.progress("Converting receptor file from mol2 to pdb")

    command = "babel"
    (label, ext) = os.path.splitext(mol2_file)
    pdb_file = label + ".pdb"
    args = ["-i mol2 %s" % (mol2_file), "-o pdb %s" % (pdb_file)]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "mol22pdb")

    return pdb_file


def prepareLigandFile(ligand_label, tasks):
    """
    Converts a mol2 ligand file to pdbqt
    """

    command = "Prepare_ligand"
    args = [ligand_label]

    cluster.runTasks(command, args, tasks, environment_module = ENVIRONMENT_MODULE, log_prefix = "prepare_ligand")


def prepareReceptorFile(receptor_file):
    """
    Converts a pdb receptor file to pdbqt
    """
    
    command = "Prepare_receptor"
    (label, ext) = os.path.splitext(receptor_file)
    pdbqt_file = label + ".pdbqt"
    args = [receptor_file]

    cluster.runTasks(command, args, environment_module = ENVIRONMENT_MODULE, log_prefix = "prepare_receptor")

    return pdbqt_file


def runOpenDocking(ligand_label, receptor_file, gridpts, gridcenter, gridspacing, tasks):
    """
    Runs the docking
    """
    
    command = "Open_docking"
    
    args = [ligand_label, receptor_file, gridpts, str(gridspacing), gridcenter]
    
    cluster.runTasks(command, args, tasks, environment_module = ENVIRONMENT_MODULE, log_prefix = "opendocking")


def formatTable(dict_ligands, wdir = "./"):
  
    html_file = 'best_poses.html'
    with open(html_file, 'wt') as sink:
        template = jinja_env.get_template('mtiautodock.table.html')
        sink.write(template.render(dict_ligands = dict_ligands, wdir = wdir))


def formatViewer(dict_ligands, receptor_file, wdir = "./"):
  
    html_file = 'poses_explorer.html'
    with open(html_file, 'wt') as sink:
        template = jinja_env.get_template('mtiautodock.vis.html')
        sink.write(template.render(dict_ligands = dict_ligands, receptor_file = receptor_file, wdir = wdir))


def formatCSV(dict_ligands):
    
    csv_file = 'best_poses.csv'
    with open(csv_file, 'wt') as sink:
        sink.write("ligand\tpose\tenergy\ttorsions\n")
        for ligand, data in dict_ligands.items():
            for poses in data['poses']:
                sink.write(ligand + "\t" + poses['pose'] + "\t" + poses['energy'] + "\t" + data['torsions'] + "\n")

        
def main( args ):
    
    parser = cmdLine()
    args = parser.parse_args()

    status, msg = checkArgValues(args)
    
    # We split ligands

    cluster.progress("(1/6) Splitting ligand file")

    if not status:
        print msg
        sys.exit(0)
    if args.ligand_format == "mol2":
        mol22mol2(args.ligand_file)

    # We need to setup grid

    cluster.progress("(2/6) Setting up grid")

    setupGrid(args)

    print argValues(args)

    # We get ligand label needed for ARRAY JOBS

    (args.ligand_label, ext) = os.path.splitext(args.ligand_file)
    args.nLigands = len(glob.glob('%s*.mol2' % args.ligand_label)) - 1;

    # We convert ligand files from mol2 to pdbqt

    cluster.progress("(3/6) Preparing ligand files")

    prepareLigandFile(ligand_label = args.ligand_label,
                      tasks        = args.nLigands)

    # We convert receptor file from pdb to pdbqt and add hydrogens
    
    cluster.progress("(4/6) Preparing receptor file")
    args.orig_receptor_file = args.receptor_file
    args.receptor_file = prepareReceptorFile(args.receptor_file)

    # We lauch individual docking experiments (using docker, drmaa, etc)

    cluster.progress("(5/6) Running Open_docking program")

    runOpenDocking(ligand_label  = args.ligand_label,
                   receptor_file = args.receptor_file,
                   gridpts       = args.gridpts,
                   gridcenter    = args.gridcenter_coordinates,
                   gridspacing   = args.gridspacing,
                   tasks         = args.nLigands)

    # We collect/merge the results
    
    cluster.progress("(6/6) Formatting output")

    dict_ligands={}
    multi_ligands_file_pdbqt = open("all_ligands.pdbqt", "wt")
    multi_ligands_file_mol2 = open("all_ligands.mol2", "wt")

    for i in range(1, args.nLigands + 1):

        ligand_name = args.ligand_label + str(i)

        receptor_name = os.path.splitext(args.receptor_file)[0]
        dock_log_file = ligand_name + "/" + ligand_name + "_vs_" + receptor_name + ".dlg"
        dict_ligands[ligand_name] = {}
        dict_ligands[ligand_name]['poses'] = []

        try:
            with open(dock_log_file, "r") as dock_log_file_FH:
                dock_log_file_content = dock_log_file_FH.read()
                m = re.search("(\d+) torsions", dock_log_file_content)
                if bool(m):
                    dict_ligands[ligand_name]['torsions'] = m.group(1)
                else:
                    dict_ligands[ligand_name]['torsions'] = "0"

            with open(ligand_name + "/" + ligand_name + "_energy.csv", "r") as energies_file:
                energies = csv.DictReader(energies_file, fieldnames=("pose", "energy"), delimiter=" ")
                for row in energies:
                    dict_ligands[ligand_name]['poses'].append(row)

            with open(ligand_name + "/" + ligand_name + "_all_poses.pdbqt", "r") as multi_poses_file_pdbqt:
                multi_ligands_file_pdbqt.write(multi_poses_file_pdbqt.read())

            with open(ligand_name + "/" + ligand_name + "_all_poses.mol2", "r") as multi_poses_file_mol2:
                multi_ligands_file_mol2.write(multi_poses_file_mol2.read())

            #~ shutil.copy(ligand_name + '_all_poses.pdbq', ligand_name + '_all_poses.pdb')

        except IOError as e:
            #~ cluster.progress("Docking of " + ligand_name + " has failed.")
            pass

    multi_ligands_file_pdbqt.close()
    multi_ligands_file_mol2.close()

    # We format output

    formatTable(dict_ligands, wdir)
    formatCSV(dict_ligands)
    formatViewer(dict_ligands, args.orig_receptor_file, wdir)


if __name__ == "__main__":
    
    main(sys.argv)
    sys.exit(0)
