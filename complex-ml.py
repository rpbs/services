#!/usr/bin/env python
import subprocess, csv, sys, os, glob
import PyPDB.PyPDB as PDB
import Fasta.Fasta as Fasta

# Constants
BCSEARCH_CMD = '/service/BCSearch.py'
DOMFINDER_CMD = 'drun complex-ml DOMFinderExec'
DOREALIGN_CMD = 'drun complex-ml DoRealignExec'
ALIGNTEMPL_CMD = 'drun complex-ml align_template.py'
MODELTEMPL_CMD = 'drun complex-ml model_template.py'

def dprint(*args):
    if 'DEBUG' in os.environ:
        for arg in args: print(arg)

def domains(seq_file):
    subprocess.check_call('%s -i %s -r 1 -w .' % (DOMFINDER_CMD, seq_file), shell = True)
    reader = csv.DictReader(open('ResRank1_infos.txt'), delimiter='\t')
    results = []
    for row in reader:
        if ',' in row['PDB']:
            row['PDB'] = row['PDB'].split(',')
        else:
            row['PDB'] = [row['PDB']]
        results.append(row)
    return results

def realign(seq_id, seq_file):
    result = 'alnfile.pir'
    subprocess.check_call('%s --input-label=%s  --input-fst=%s --input-hhrfst=resPDB/%s.pdb.hhrfas --fr=%s' % (DOREALIGN_CMD, seq_id, seq_file, seq_id, result), shell = True)
    return result

def cut_template(pir, model, seq_id, templates):
    # 1. read the PIR file, first alignment
    # @todo: why only first alignment?
    subprocess.check_call('%s %s %s %s' % (ALIGNTEMPL_CMD, pir, seq_id, templates[0]), shell = True)
    # 2. construct gap mask
    fst = Fasta.fasta('gaps.fst')
    fst_ids = fst.ids()
    mask = fst[fst_ids[0]].s()
    for i, c in enumerate(fst[fst_ids[1]].s()):
        if c == '-':
            mask[i] = '-'
    # 3. align the gaps on the sequence
    print 'mask',mask
    print 'model',model
    pdb = PDB.PDB(model)
    gapped = pdb.mask(mask=mask)
    gapped.out(model)
    return model

def fill_template(fasta, gapped, search_list = 'pdb', min_bc = 0.75, max_rigidity = 1.0):
    # 1. call bcloopsearch
    # out: list of pdb loops, full sequence
    cmd = '%s -p %s -s %s --search_list %s --max_Rigid %f --min_BCscore %f' % (BCSEARCH_CMD, gapped, fasta, search_list, min_bc, max_rigidity)
    subprocess.check_call(cmd, shell = True)
    # 2. pdbsa generate .hmm.27-2 for every fragment
    # 3. no fragment from bcloopsearch, call cppbuilder
    # in: fasta, .prob from ppp3
    # out: series of pdb fragments
    # 4. cluster them and select N candidate (@todo we dont know how many here)
    pass

def modeller(pir, seq_id, templates):
    subprocess.check_call('%s %s %s %s' % (MODELTEMPL_CMD, pir, seq_id, ' '.join(templates)), shell = True)
    # @todo: retrieve model name from modeller API
    return os.path.basename(glob.glob('%s.*1.pdb' % seq_id)[0])

def main(seq_file):
    # in: fasta, out: reformatted fasta
    # @note: this program works with only 1 sequence
    fasta = Fasta.fasta(seq_file)
    seq_file_orig = seq_file
    seq_id = fasta.ids()[0]
    seq_file = '%s.fas' % seq_id
    dprint('reformat "%s" => "%s"' % (seq_file_orig, seq_file))
    # @note: bug in Fasta.py (there are 2 definitions of "def out()" in fasta class, depending on version)
    # fasta.out(open(seq_file, 'w'))
    fasta.data[seq_id].out(f = open(seq_file, 'w'))
    if not os.path.isfile(seq_file):
        raise Exception('couldn\'t created Fasta file %s from input' % seq_file)
    # @todo: user can provide gapped PDB
    # in: fasta, out: domains (sets of PDBs)
    dprint('domains for "%s"' % (seq_id))
    domain_list = domains(seq_file)
    # in: fasta + template, out: pir alignment
    dprint('realigning "%s" with "%s"' % (seq_id, [d['PDB'] for d in domain_list]))
    pir = realign(seq_id, seq_file)
    # in: alignment, out: model
    domain_pdbs = []
    for domain in domain_list:
        for pdb_id in domain['PDB']:
            for f in glob.glob('%s_PDB_files/%s*.pdb' % (seq_id, pdb_id.translate(None,'_'))):
                domain_pdbs.append(os.path.splitext(os.path.basename(f))[0])
            break
    dprint('modelling "%s" with "%s"' % (pir, domain_pdbs))
    model = modeller(pir, seq_id, domain_pdbs)
    # in: model, out: gapped model
    dprint('cutting gap regions "%s"' % (pir))
    gapped = cut_template(pir, model, seq_id, domain_pdbs)
    # in: gapped model, out: filled model
    dprint('filling loops in "%s"' % (gapped))
    filled = fill_template(seq_file, gapped)

if __name__ == '__main__':
    if len(sys.argv) < 2 or '-h' in sys.argv:
        print('Usage: %s <seq_file>', sys.argv[0])
        sys.exit(1)
    main(sys.argv[1])