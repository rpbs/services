#!/bin/bash

# Main script to launch in one shot all the different subparts
# These subparts are launched sequentially

# 1 - genere_peptide_aleat.pl
# 2 - check_solubility.pl
# 3 - create_model.py

### To adjust for mobyle
# Be sure that system path include
# path for genere_peptide_aleat.pl, 
# path for Solubility.pm
# path create_model.py (adjust path line 170)

function container_exec()
{
  if which drun > /dev/null; then
  	drun genopept $@
  else
	/bin/bash -c "$@"
  fi
  return $?
}

function print_usage()
{
  echo "Usage: `basename $0` (-c) (-d) (-k) (-l) (-n) (-p) (-q)"
  echo ""
  echo "This program is meant to predict a series of peptides and to filter them according to solubility rules."
  echo ""
  echo "-c : keep peptides able to form disulfide bridges"
  echo "-d : demo mode"
  echo "-k : provide the MODELLER key for 3D conformations generation"
  echo "-l : indicate the lenght of the peptides of interest"
  echo "-n : indicate the number of peptides to obtain"
  echo "-p : predict the three dimensional conformation for a given peptide"
  echo "-q : transform the generated pdb file into the autodock/vina initial configuration file (noop)"
  echo ""
  exit 1;
}

[ $# -eq 0 ] && print_usage && exit;

cysbridge=0
demo=0
check=0
key=""
long=0
pepnum=0
pdb=0
pdbqt=0
solubility=0

export PATH=${PATH}:/usr/local/Genopept

while getopts "tcdk:l:n:pqs" opt ; do
    case $opt in
        c ) cysbridge=1 ;;
        d ) demo=1 ;;
        t ) check=1 ;;
        k ) key=$OPTARG ;;
        l ) long=$OPTARG ;;
        n ) pepnum=$OPTARG ;;
        p ) pdb=1 ;;
        q ) pdbqt=1 ;;
        s ) solubility=1 ;;
        * ) print_usage ;;
    esac
done

[ $long -eq 0 -o $pepnum -eq 0 ] && echo "Provide the number of peptides to predict" && exit 1

# Step 1 - initial sequence generation

if [ $demo -eq 1 ]; then

key="MODELIRANJE"
echo "Generating the initial random peptide list of peptides"

cat << EOPEPDATA > genopept-pep.fasta
>pep-10AA-f705024eb7db458b8c2ce6df493b9f4e
SYLMITCGTP
>pep-10AA-3f055afa0cc847379695a4a55c1329c9
NAWSRDNMQC
>pep-10AA-9e78928017174c97bffefc30af9ba2b6
QQDDMPNHKI
>pep-10AA-16fd8e845fb344a3b695f8f17ce884b3
QCSVDTKFPG
>pep-10AA-14039158484611E2920067873B1F0AA1
NFGQTVEIRF
>pep-10AA-140629F4484611E2920067873B1F0AA1
IERHTVATRF
>pep-10AA-1408ADD2484611E2920067873B1F0AA1
EESHSADDMH
>pep-10AA-140B3AE8484611E2920067873B1F0AA1
HNHCPKKRPG
>pep-10AA-140DC92A484611E2920067873B1F0AA1
PVRAIYEHNH
>pep-10AA-14105802484611E2920067873B1F0AA1
SIKWADRTPK
EOPEPDATA
else

    if [ $solubility -eq 1 ]; then
        container_exec genere_peptide_aleat -l $long -n $pepnum -o ./ -s
    else
        container_exec genere_peptide_aleat -l $long -n $pepnum -o ./
    fi

    if [ $cysbridge -eq 1 ]; then
        cat peptides_aleat.fasta peptides_disu.fasta > genopept-pep.fasta
    else
        cat peptides_aleat.fasta > genopept-pep.fasta
    fi

fi

# creation weblogo
container_exec "weblogo --format PNG --size large --yaxis 1 --aspect-ratio 2 --stack-width 300 --show-xaxis NO --show-yaxis NO --color-scheme auto --reverse-stacks NO < genopept-pep.fasta > genopept-pep.png"

# 2 - Solubility check, used directly in #1 outside the demo mode

if [ $demo -eq 1 ]; then

# Returns in demo mode :

cat << EOF > genopept-test.expect
pep-10AA-f705024eb7db458b8c2ce6df493b9f4e soluble
pep-10AA-3f055afa0cc847379695a4a55c1329c9 soluble
pep-10AA-9e78928017174c97bffefc30af9ba2b6 soluble
pep-10AA-16fd8e845fb344a3b695f8f17ce884b3 soluble
pep-10AA-14039158484611E2920067873B1F0AA1 soluble
pep-10AA-140629F4484611E2920067873B1F0AA1 not soluble
pep-10AA-1408ADD2484611E2920067873B1F0AA1 not soluble
pep-10AA-140B3AE8484611E2920067873B1F0AA1 not soluble
pep-10AA-140DC92A484611E2920067873B1F0AA1 not soluble
pep-10AA-14105802484611E2920067873B1F0AA1 not soluble
EOF

container_exec "check_solubility -i genopept-pep.fasta -o genopept-solubility.log -of genopept-solubility.fasta" | tee genopept-test.log
tail -n +2 genopept-test.log | diff -u genopept-test.expect -
[ $? -ne 0 ] && exit 1
[ $check -eq 1 ] && exit 0
fi

# creation script SGE

if [ $pdb -eq 1 ]; then

#     if [ $key != "MODELIRANJE" ]; then

# 	echo "Sorry: could not generate PDB. Incorrect Modeller license key"
# 	exit 0

#     fi

# job synchrone
python /service/env/cluster.py ${pepnum} genopept perl /usr/local/Genopept/gen_pep_conf.pl genopept-pep.fasta 0

# archive zip
tar -czf genopept.tgz *.pdb $(ls *.pdbqt)

fi
