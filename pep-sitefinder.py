#!/usr/bin/env python

"""
PyToolsPepDock main script
Will organise a script depending on options and launch series of runs.

Dependencies:
"""

import sys
import os
from optparse import OptionParser
import fnmatch
import shutil
import subprocess
import time
import cluster.cluster as cluster

# Configuration
from PEPSiteFinder.Config import *

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PEPSiteFinder','templates'))

# Dependencies
from PEPSiteFinder.PyOptions import *
import PyPDB.PyPDB as PDB
import Fasta.Fasta as Fasta

import signal

import glob


def reduceFile(filein, fileout, attract2=False):

    cmd = "Reduce"

    args = [ filein,
             fileout ]

    if attract2:
        args.append("--attract2")

    cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "reduce")


def pmlScript(label, maxConf):
    rs = """
load protein.pdb, protein
show surface, protein
show cartoon, protein
hide lines, protein
spectrum b, blue_white_red, protein, 0, 100
set surface_color, white
set transparency, 0.5
set surface_quality, 1
"""

    for cnfNb in range(1, maxConf+1):
        for poseNb in range (1, 11):
            rs += "load %s-cnf%s_%.2d.pdb, cnf%s\n" % (label, cnfNb, poseNb, cnfNb)
    rs += "select all_cnf, cnf1"

    for cnfNb in range(2, maxConf+1):
        rs += " + cnf%s" % (cnfNb)

    rs += """
show cartoon, all_cnf
hide lines, all_cnf
set all_states, off
"""
    return rs


class PepDock(Options):

    def __init__(self, args = ["PepDock"], options = None, version=None, verbose = False):
        """
        @param options : a dictionary of the options among which
                         some are valid.
        """
        if version == None:
            version = "PepDock"+ " v%s"%VERSION
        if not hasattr(self,"clp"):
            Options.__init__(self, args = args, options = options, version=version, verbose = verbose)
        self.__cmdLine_options()
        # i_options = {"probFile" : "MINI_FOLD.svmi8.27.prob",
        #              "trj"      : "MINI_FOLD.trj",
        #              "zipMask"  : "MINI_FOLD.gmask",
        #              }
        # self.add_options(i_options)


    def start(self, parse = True, check = True, verbose = False):
        """
        Effective options parsing
        on return, MINI_FOLD ready to run.
        """
        # This for demo mode
        if parse:
            self.parse()
        if verbose:
            self.options.verbose = verbose
        if check:
            self.cmd_line_status = self.__check_options(verbose = True)
            if verbose:
                sys.stderr.write("PepDock Initial start: %s\n" % self.cmd_line_status)
        return

    def __repr__(self):
        """
        @return: a string describing the options setup for the Blast instance
        """

        try:
            rs = self.version
            for option in self.options.__dict__.keys():
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""

    def __cmdLine_options(self, version = None):
        """
        Command line definition.
        These options are extra options over inherited PPP options.
        @return: command line options and arguments (optionParser object)
        """

        self.clp.set_usage("usage: %prog [options] <sequence file> ")

    # OK from here
        self.clp.add_option("-n", "--nRuns", action="store", dest="nRuns", type="int", help="number of greedy runs", default = 100)
        self.clp.add_option("-3", "--iPDB", action="store", dest="pdb", type="string", help="protein PDB file", default = None)
        self.clp.add_option("--iPep", action="store", dest="peppdb", type="string", help="peptide PDB file", default = None)
        self.clp.add_option("--iPepSeq", action="store", dest="pepseq", type="string", help="peptide sequence file", default = None)
        self.clp.add_option("--focus", action="store", dest="focus", type="string", help="focus docking around specified residue", default = None)
        self.clp.add_option("--radius", action="store", dest="radius", type="float", help="radius around focus to start docking", default = PEPDOCK_DFLT_RADIUS)
        self.clp.add_option("-l", "--label", action="store", dest="label", type="string", help="label for the run (%s)" % PEPDOCK_DFLT_LABEL, default = PEPDOCK_DFLT_LABEL)

        self.clp.add_option("--script", action="store_true", dest="script", help="do not run, but generate the scripts", default = False)
        self.clp.add_option("--dockonly", action="store_true", dest="dockOnly", help="only run greedy", default = False)
        self.clp.add_option("--clusterizeonly", action="store_true", dest="clusteringOnly", help="only run clustering", default = False)
        self.clp.add_option("--posttreatmentonly", action="store_true", dest="postTreatmentOnly", help="only run posttreatment", default = False)
        self.clp.add_option("--parallelposttreatment", action="store_true", dest="sgePostTreat", help="run posttreatment using SGE array (faster)",    default = False)
        self.clp.add_option("--progressFile", action="store", dest="progressFile", type="string", help="Log file to put information about PEP-FOLD progress (%s)"%PEPDOCK_PROGRESS_FILE, default = PEPDOCK_PROGRESS_FILE)
        self.clp.add_option("--demo", action="store_true", dest="demo", help="supersedes parameters with demo ones", default = None)

        self.clp.add_option("--density", action="store", dest="density",  type="float", help = "remove starting points if their relative distance is < density (%s A)" % str(DFLT_PEP_SITEFINDER_DENSITY), default = None)
        self.clp.add_option("--distance-to-receptor", type="str", dest="distance_to_receptor", help="minimum distance (in A) between starting points and the receptor surface, default is the ligand radius. If the distance ends with 'x' then the distance to the receptor will be the ligand radius multiplied by this input value (%s)" % DFLT_PEP_SITEFINDER_DISTANCE, default = DFLT_PEP_SITEFINDER_DISTANCE)
        self.clp.add_option("--noattract2", action='store_false', dest="attract2", default=True, help="use attract1 forcefield instead of version 2")
        self.clp.add_option("--nclusters", action="store", dest="nclusters", type="int", help="number of structures to cluster, an increase of this value will increase significantly the time processing", default = DFLT_NCLUSTERS)
        self.clp.add_option("--max_prop_models", action="store", dest="max_prop_models", type="int", help="number of models used to define interaction propensities (%d)." % DFLT_MAX_PROPENSITY_MODELS, default = DFLT_MAX_PROPENSITY_MODELS)
        self.clp.add_option("--max-ppfld-centroids", action="store", dest="ppfld_centroids", type="int", help="number of PEPFOLD centroids used for docking (%d)" % DFLT_MAX_PEPFOLD_CENTROIDS, default = DFLT_MAX_PEPFOLD_CENTROIDS)
        self.clp.add_option("--parameter-file", action="store", type="string", dest="param_file", help="use a custom forcefield parameter file (by default parmw.par) (only for attract2)", default="%s/PyAttract/parmw.par" % PTOOLS_PATH)
        self.clp.add_option("--contact-cutoff", action="store", dest="contact_cutoff", type="float", default=7.0, help="use a custom cutoff for considering contacted residues. Default is 7.0A (for CG), and should be 5.0 for all-atoms")
        self.clp.add_option("--cluster-cutoff", action="store", dest="cluster_cutoff", type="float", default=5.0, help="set a custom cutoff for clustering docked peptides (default 5.0A)")

        self.clp.add_option("--ncores", action="store", dest="np", type="int", help="# cores to use for multiprocessing", default = DFLT_NPROC)

    def __check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy

        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """

        # Check input data exists
        if (not self.options.pdb):
            if verbose:
                sys.stderr.write("PEP-SiteFinder: must specify a target PDB file to dock the peptide. --help for option detail.\n")
            if True or REMOTE_SERVER:
                cluster.progress("PEP-SiteFinder : invalid input protein file.")
            return False

        if (not self.options.peppdb) and (not self.options.pepseq):
            if verbose:
                sys.stderr.write("PEP-SiteFinder: must specify either a PDB file for peptide to dock, or a peptide sequence. --help for option detail.\n")
            if True or REMOTE_SERVER:
                cluster.progress("PEP-SiteFinder : invalid input peptide file.")
            return False

        self.options.runDock          = True
        self.options.runPostTreatment = True
        self.options.runClustering    = True
        if self.options.dockOnly:
             self.options.runDock          = True
             self.options.runClustering    = False
             self.options.runPostTreatment = False
        if self.options.clusteringOnly:
             self.options.runDock          = False
             self.options.runClustering    = True
             self.options.runPostTreatment = False
        if self.options.postTreatmentOnly:
             self.options.runDock          = False
             self.options.runClustering    = False
             self.options.runPostTreatment = True

        # Check protein PDB is a PDB and is of correct length
        if self.options.pdb is not None:
            try:
                x = PDB.PDB(self.options.pdb)
                sys.stderr.write("Protein size is %d\n" % len(x))
            except:
                sys.stderr.write("Sorry: protein file does not seem a valid PDB. \n")
                self.options.pdb = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : protein file does not seem a valid PDB.")
                return False
            if not len(x):
                sys.stderr.write("Sorry: protein file does not seem a valid PDB. \n")
                self.options.reference = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : protein file does not seem a valid PDB.")
                return False

        # Check peptide PDB is a PDB and is of correct length
        if self.options.peppdb is not None:
            try:
                x = PDB.PDB(self.options.peppdb)
                sys.stderr.write("Peptide size is %d\n" % len(x))
            except:
                sys.stderr.write("Sorry: peptide file (%s) does not seem a valid PDB . \n" % self.options.peppdb)
                self.options.peppdb = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide file does not seem a valid PDB.")
                return False
            if not len(x):
                sys.stderr.write("Sorry: peptide file (%s) does not seem a valid PDB. \n" % self.options.peppdb)
                self.options.reference = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide file does not seem a valid PDB.")
                return False

        if self.options.pepseq is not None:
            try:
                x = Fasta.fasta(self.options.pepseq)
                sys.stderr.write("Peptide size is %d\n" % len(x[x.ids()[0]]))
            except:
                sys.stderr.write("Sorry: peptide file (%s) does not seem a valid sequence. \n" % self.options.pepseq)
                self.options.pepseq = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide file does not seem a valid PDB.")
                return False
            if not len(x[x.ids()[0]]):
                sys.stderr.write("Sorry: peptide sequence file does not seem a valid Fasta. \n")
                self.options.reference = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide file does not seem a valid Fasta.")
                return False

            if len(x[x.ids()[0]]) > 36:
                sys.stderr.write("Sorry: peptide is too long. \n")
                self.options.reference = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide too long.")
                return False

            if len(x[x.ids()[0]]) < 5:
                sys.stderr.write("Sorry: peptide is too short. \n")
                self.options.reference = None
                if True or REMOTE_SERVER:
                    cluster.progress("PEP-SiteFinder : peptide too short.")
                return False
            f = open("%s.seq" % self.options.label, "w")
            f.write("%s\n" % x[x.ids()[0]].s())
            f.close()

        # Remove banks in label
        self.options.label = self.options.label.replace(" ","_")

        return True


    def PDBfiles(self, wPath = ".", lbl = None, pass1 = False, verbose = 0):
        """
        @param wPath: directory to scan
        @param lbl  : file prefix to search for
        return a list of the names of the PDB files generated
        """

        # Label to search for
        if not lbl:
            lbl=self.options.label

        # List files of the current directory
        filesL = os.listdir( wPath )

        # PDB files to clusterize
        pdblist = []
        pattern = "%s*bestene1mc.pdb" % lbl

        # List them

        for aFile in filesL:
            if fnmatch.fnmatch( aFile, pattern ) and FileTools.checkFileStatus( aFile, verbose = verbose ) == 0:
                if aFile.count("pass1") and not pass1:
                    continue
                pdblist.append( aFile )

        # Return the list
        return pdblist


    def formatViewer(self, tab_poses, wdir = "./"):

        html_file = 'poses_explorer.html'
        with open(html_file, 'wt') as sink:
            template = jinja_env.get_template('pepsitefinder.vis.html')
            sink.write(template.render(tab_poses = tab_poses, wdir = wdir))


    def formatTable(self, wdir = "./"):

        html_file = 'download.html'
        with open(html_file, 'wt') as sink:
            template = jinja_env.get_template('pepsitefinder.table.html')
            sink.write(template.render(wdir = wdir))

# =======================================================================
#
# PEPFOLD simulations to get peptide conformations to dock
# Then series of PEP-SiteFinder
#
# =======================================================================

    def runPepFold(self, verbose = False):
        """
        This part to run PEPFOLD
        """

        self.start_time = time.time() # To track execution time

        reRun = True

        if reRun:

            shutil.copy2(self.options.pdb, 'protein.pdb')

            self.cwd = os.getcwd()
            if self.options.progressFile is not None and self.options.progressFile[0] != "/":
                self.options.progressFile = "%s/%s" % (self.cwd, self.options.progressFile)

            if not os.path.exists("PEPFOLD"):
                os.makedirs("PEPFOLD")
            shutil.copy2(self.options.pepseq, 'PEPFOLD/%s' % self.options.pepseq)
            shutil.copy2("%s.seq" % self.options.label, 'PEPFOLD/%s.seq' % self.options.label)
            os.chdir("PEPFOLD")

            cluster.progress("PEP-SiteFinder (1/4): Running peptide 3D generation", wdir = "../")

            # local prediction

            cmd = "PyPPPExec"

            args = ["-s %s" % self.options.pepseq,
                    "-l %s" % self.options.label,
                    "--2012",
                    "-v"]

            cluster.progress("    PepFold (1/5): Local prediction", wdir = "../")
            cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, job_opts='-c 4', log_prefix = "ppp")

            # 3D generation

            cmd = "CppBuilder"

            args = ["-iPrf %s.svmi8.27.prob" % self.options.label,
                    "-iSeq %s.seq" % self.options.label,
                    "-fuzzyFirst",
                    "-mode 2",
                    "-rndFrom 100",
                    "-heapSze 300",
                    "-clusterStep -1",
                    "-maxCls 3",
                    "-rndZip",
                    "-mc 30",
                    "-MpT 700",
                    "-o3D %s" % self.options.label,
                    "-expg",
                    "-nIter 100",
                    "-nc 8"]

            cluster.progress("    PepFold (2/5): 3D generation", wdir = "../")
            cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "cppbuilder")

            # post treatment

            cmd = "POSTTTExec"

            args = ["--mask *ene1-mc.pdb",
                    "--oscar",
                    "--pattern *-ID_bestene1-mc.pdb",
                    "--parallelposttreatment",
                    "--padding 3",
                    "--silent",
                    "-v"]

            cluster.progress("    PepFold (3/5): PostTreatment", wdir = "../")
            cluster.runTasks(cmd, args, tasks = 100, environment_module = DOCKER_IMG, log_prefix = "posttreatment")

            # clustering

            cmd = "DBClustersExec"

            args = ["-l prefix",
                    "--mask *-*-SC-min.pdb",
                    "--tmpPATH clusters",
                    "--odir MQA",
                    "--score BCscore",
                    "--threshold 0.90",
                    "--noMQA",
                    "-v",
                    "--ncores %s" % self.options.np]

            cluster.progress("    PepFold (4/5): Clustering", wdir = "../")
            cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "clustering", job_opts = "-c %s" % self.options.np)

            # Generate information of best clusters

            cmd = "PPFLDClusterAnalyze"

            args = ["%d" % self.options.ppfld_centroids]

            cluster.progress("    PepFold (5/5): Cluster analysis", wdir = "../")
            cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "clusteranalyze")

            lines = open("./MQA/clusters.txt.20-1-best.Ids")
            theList = []
            for i,l in enumerate(lines):
                fname = l.split()[0]
                modelname = "%s-cnf%d.pdb" % (self.options.label, i+1)
                theList.append("%s-cnf%d" % (self.options.label, i+1))
                shutil.copy2(fname, '../%s' % modelname)

            os.chdir("..")


            # Prepare directories with everything for PEP-SiteFinder

            cluster.progress("PEP-SiteFinder (2/4):")

            for i, aConformation in enumerate(theList):
                if not os.path.exists(aConformation):
                    os.mkdir(aConformation)
                shutil.copy2("%s.pdb" % aConformation, aConformation)
                shutil.copy2(self.options.pdb, aConformation)
                os.chdir(aConformation)
                cluster.progress("    Docking conformation: %d/%d" % (i+1, len(theList)), wdir = "../")
                # WE RUN THE DOCKING HERE !!!!!!!!!!!!
                self.onePepDock(self.options.pdb, "%s.pdb" % aConformation, label = aConformation, verboseProgress = verbose, verbose = verbose)
                os.chdir("..")


            # poses clustering

            cmd = "Clustering"

            args = [ "output.dat",
                     "l.red",
                     "%s" % self.options.nclusters,
                     "map_item",
                     "clusterized.out" ]

            cluster.progress("PEP-SiteFinder (3/4): clustering poses", wdir = "./")

            cluster.runTasks(cmd, args, map_list = theList, environment_module = DOCKER_IMG, log_prefix = "posesclustering")

            #extract the NBESTMODELS (10) best structures

            for directory in theList:
                f=open(os.path.join(directory, "clusterized.out"), "r")
                lines = f.readlines()
                os.chdir(directory) #go into the corresponding simulation directory
                for i,l in enumerate(lines[1:NBESTMODELS+1]): # Change here to move for larger number of results
                    lspl   = l.split()
                    trans  = lspl[1]
                    rot    = lspl[2]
                    ene    = lspl[3]
                    weight = lspl[6]
                    label = directory
                    recname = self.options.pdb

                    cmd = "Extract"

                    args = [ "output.dat",
                             "%s.pdb" % directory,
                             "%s" % trans,
                             "%s" % rot,
                             label,
                             "%02i" % (i+1) ]

                    cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "extract")

                    os.system("cat %s %s_%02i.pdb > to_optimize_%02i.pdb"%(recname, label, i+1, i+1) )
                    shutil.copy2("%s_%02i.pdb" % (label, i+1), "..")
                    os.system("echo %s_%02i.pdb %s %s >> ../poses.txt" % (label, i+1, ene, weight))
                os.chdir("..") #go back to the main directory


        # Now we present the results...

        cluster.progress("PEP-SiteFinder (4/4): compiling results", wdir = "./")

        cmd = "sort poses.txt -k2 -n > poses.ranked.txt"
        os.system(cmd)

        f = open("poses.ranked.txt")
        lines = f.readlines()
        f.close()

        cmd = "echo 'HEADER  PEPTIDE POSES RANKED BY ATTRACT2 ENERGY' >> poses-ranked.pdb"
        status = os.system(cmd)

        tab_poses = []

        for i,l in enumerate(lines):
            it = l.split()
            aPDB = it[0]
            tab_poses.append(aPDB)
            cmd = "echo MODEL %2d >> poses-ranked.pdb" % (i+1)
            status = os.system(cmd)
            cmd = "grep ATOM %s >> poses-ranked.pdb" % aPDB
            status = os.system(cmd)
            cmd = "echo ENDMDL >> poses-ranked.pdb"
            status = os.system(cmd)    

        cmd = "echo END >> poses-ranked.pdb" 
        status = os.system(cmd)


        cmd = "echo 'HEADER  PEPTIDE POSES RANKED BY ATTRACT2 ENERGY' > poses-10bestranked.pdb"
        status = os.system(cmd)
        for i,l in enumerate(lines):
            if i == 10:
                break
            it = l.split()
            aPDB = it[0]
            cmd = "echo MODEL %2d >> poses-10bestranked.pdb" % (i+1)
            status = os.system(cmd)
            cmd = "grep ATOM %s >> poses-10bestranked.pdb" % aPDB
            status = os.system(cmd)
            cmd = "echo ENDMDL >> poses-10bestranked.pdb"
            status = os.system(cmd)

        cmd = "echo END >> poses-10bestranked.pdb"
        status = os.system(cmd)


        cmd = "echo 'HEADER  PEPTIDE POSES RANKED BY ATTRACT2 ENERGY' > poses-50bestranked.pdb"
        status = os.system(cmd)
        for i,l in enumerate(lines):
            if i == 50:
                break
            it = l.split()
            aPDB = it[0]
            cmd = "echo MODEL %2d >> poses-50bestranked.pdb" % (i+1)
            status = os.system(cmd)
            cmd = "grep ATOM %s >> poses-50bestranked.pdb" % aPDB
            status = os.system(cmd)
            cmd = "echo ENDMDL >> poses-50bestranked.pdb"
            status = os.system(cmd)

        cmd = "echo END >> poses-50bestranked.pdb"
        status = os.system(cmd)


        #
        # Calculate the propensities
        #
        f = open("poses.ranked.txt")
        lines = f.readlines()
        f.close()

        nLines = 0
        theList = []
        for i,l in enumerate(lines):
            if i == self.options.max_prop_models:
                break
            it = l.split()
            theList.append(it[0])
            nLines += 1

        cmd = "Contacted"

        args = [ "%s" % self.options.pdb,
                 "map_item",
                 "%f" % self.options.contact_cutoff,
                 "patchPropensity.rawdata" ]

        cluster.runTasks(cmd, args, map_list = theList, environment_module = DOCKER_IMG, log_prefix = "contacted")

        #
        # Generate a PDB file for which the TFac are replaced by the propensities
        #
        x = PDB.PDB(self.options.pdb, hetSkip=2)

        if False:
            rStart = int(x[0].rNum())
            rStop  = int(x[-1].rNum())
            rs = {}
            for aRes in range(rStart, rStop+1):
                rs[aRes] = 0

            f = open("patchPropensity.rawdata")
            lines = f.readlines()
            f.close()

            for l in lines:
                it = l.split()
                for aIt in it:
                    rs[int(aIt)] += 1

            # nLines = len(lines)

            for aRes in range(rStart, rStop+1):
                rs[aRes] = 100. * float(rs[aRes]) / float(nLines)
                if rs[aRes] > 99.99:
                    rs[aRes] = 99.99

            # Now we output the propensities and setup the BFac values in the PDB file
            f = open("patchPropensity.txt", "w")
            for aRes in range(rStart, rStop+1):
                for i, aResx in enumerate(x):
                    if int(aResx.rNum()) == aRes:
                        outstr = "%s %3d : %f\n" % (aResx.rName(), int(aResx.rNum()), rs[aRes])
                        x[i].tFac(rs[aRes])
                        f.write(outstr)
                        break
            f.close()
            x.out("protein.pdb")
        else:
            chIds = x.chnList()
            rs = {}
            for aChId in chIds:
                # rs[aId] = {}
                c = x[aChId]
                if aChId == ' ':
                    aChId = ""
                for aRes in c:
                    # riCode = aRes.riCode()
                    rNum = aRes.rNum()
                    rLbl = "%s_%s" % (rNum,aChId)
                    rs[rLbl] = 0

            f = open("patchPropensity.rawdata")
            lines = f.readlines()
            f.close()

            for l in lines:
                it = l.split()
                for aIt in it:
                    rs[aIt] += 1

            nLines = len(lines)
            for aRLbl in rs.keys():
                rs[aRLbl] = 100. * float(rs[aRLbl]) / float(nLines)
                if rs[aRLbl] > 99.99:
                    rs[aRLbl] = 99.99

            # Now we output the propensities and setup the BFac values in the PDB file
            f = open("patchPropensity.txt", "w")
            for i, aRes in enumerate(x):
                x[i].tFac(0.)
                rNum  = aRes.rNum()
                aChId = aRes.chnLbl()
                if aChId == ' ':
                    aChId = ""
                rLbl = "%s_%s" % (rNum,aChId)
                outStr = "%s %3d %s: %f\n" % (aRes.rName(), int(aRes.rNum()), aChId, rs[rLbl])
                f.write(outStr)
                x[i].tFac(rs[rLbl])

            f.close()
            x.out("protein.pdb")

        # we add connectivities to pose pdb files for pv viewer (actually pv still doesn't show them)

        pose_files = glob.glob('./*-cnf*_*.pdb')

        cmd = "Babel_service"
        args = ["pdb", "pdb", "map_item"]

        cluster.runTasks(cmd, args, environment_module = BABEL_DOCKER_IMG, map_list = pose_files)


        # we create an archive of all poses

        cmd = "tar czf poses.tgz %s %s-cnf*.pdb" % (self.options.pdb, self.options.label)
        status = os.system(cmd)


        # we format output

        jobId = os.path.basename(os.getcwd())
        serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]   # can be patchsearch or patchidentification
        wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

        self.formatViewer(tab_poses, wdir)
        self.formatTable(wdir)


        # we create pymol script (very dirty, to be enhanced)

        rs = pmlScript(self.options.label, len(theList))
        f = open("Pep-SiteFinderVisu-allposesbyconf.pml", "w")
        f.write("".join(rs))
        f.close()

        rs = """
load protein.pdb, protein
show surface, protein
show cartoon, protein
hide lines, protein
spectrum b, blue_white_red, protein, 0, 100
set surface_color, white
set transparency, 0.5
set surface_quality, 1
load poses-10bestranked.pdb, ranked
show cartoon, ranked
hide lines, ranked
split_states ranked
"""
        f = open("Pep-SiteFinderVisu-10bestranked.pml", "w")
        f.write(rs)
        f.close()

        rs =  """
load protein.pdb, protein
show surface, protein
show cartoon, protein
hide lines, protein
spectrum b, blue_white_red, protein, 0, 100
set surface_color, white
set transparency, 0.5
set surface_quality, 1
load poses-50bestranked.pdb, ranked
show cartoon, ranked
hide lines, ranked
split_states ranked
"""
        f = open("Pep-SiteFinderVisu-50bestranked.pml", "w")
        f.write(rs)
        f.close()

        rs = """
load protein.pdb, protein
show surface, protein
show cartoon, protein
hide lines, protein
spectrum b, blue_white_red, protein, 0, 100
set surface_color, white
set transparency, 0.5
set surface_quality, 1
load poses-ranked.pdb, ranked
show cartoon, ranked
hide lines, ranked
split_states ranked
"""
        f = open("Pep-SiteFinderVisu-allposesranked.pml", "w")
        f.write(rs)
        f.close()


        # we estimate the time it took

        self.elapsed_time = time.time() - self.start_time
        minutes = int(self.elapsed_time) / 60
        seconds = round(self.elapsed_time % 60)
        cluster.progress("PEP-SiteFinder : completed (%dmin %dsec)" % (minutes, seconds), wdir = "./")


# =======================================================================
#
# PEP-SiteFinder simulations to dock ONE peptide conformation on protein
#
# =======================================================================

    def onePepDock(self, proteinFile, peptideFile, label="best", verboseProgress = False, verbose = False):

        recname = proteinFile
        ligname = peptideFile

        if verboseProgress:
            cluster.progress("        PepDock (1/3): starting", wdir = "../")

        #reduce ligand and receptor

        if verbose:
            sys.stderr.write("%s\n" %  "*******reducing receptor")
        reduceFile(recname, "r.red", self.options.attract2)

        if verboseProgress:
            cluster.progress("        PepDock (2/3): reduced input to coarse grained representation", wdir = "../")

        if verbose:
            sys.stderr.write("%s\n" %  "*******reducing ligand")
        reduceFile(ligname, "l.red", self.options.attract2)

        #generate the translation file

        cmd = "Translate"
        args = [ "r.red",
                 "l.red",
                 "translation.dat" ]
        if self.options.density:
            args.append('--density %s' % self.options.density)

        if self.options.distance_to_receptor:
            args.append('--distance-to-receptor %s' % self.options.distance_to_receptor)

        cluster.runTasks(cmd, args, environment_module = DOCKER_IMG, log_prefix = "translate")

        with open("translation.dat") as translate:
          splitted = translate.readlines()
        trans_size = len([i for i in splitted if "ATOM" in i] )  #number of translations

        #send docking commands over Slurm

        cmd = "Docking"

        if self.options.attract2:
            args = [ "r.red",
                     "l.red",
                     "translation.dat",
                     "--attract2",
                     "--parameter-file %s" % self.options.param_file ]
        else:
            args = [ "r.red",
                     "l.red",
                     "translation.dat" ]

        if verboseProgress:
            cluster.progress("        PepDock (3/3): running %d docking simulations" % (trans_size), wdir = "../")

        cluster.runTasks(cmd, args, tasks = trans_size, environment_module = DOCKER_IMG, log_prefix = "docking")

        #merging results

        for i in range(trans_size):
            os.system("cat o_%i.out >> output.dat" % (i+1))
        os.system("rm o_*.out")

        return


# =======================================================================
#
# General run switch
#
# =======================================================================

    def run(self, verbose = False):
        """
        Organize complete processing
        """

        if self.options.pepseq is not None:
            self.runPepFold(verbose = verbose)


def main( args ):
    """
    Launch the main purpose:
    - Parse command line
    - Launch predictions
    """

    # Parse arguments
    job = PepDock(args = args)

    # Check command line and arguments are OK
    job.start(check = False, verbose = True) # Do not check yet

    # Manage demo before checking options validity
    if job.options.demo:
        sys.stderr.write("Will install demo files.\n")
        sys.stderr.write("Copying %s/%s as protein.\n" % (PEPDOCK_DEMO, DFLT_DEMO_PDB))
        os.system("cp %s/%s ." % (PEPDOCK_DEMO, DFLT_DEMO_PDB))
        sys.stderr.write("Copying %s/%s as peptide.\n" % (PEPDOCK_DEMO, DFLT_DEMO_PEPSEQ))
        os.system("cp %s/%s ." % (PEPDOCK_DEMO, DFLT_DEMO_PEPSEQ))
        job.options.label     = DFLT_DEMO_LABEL
        # We must recheck options manually otherwise -s None will fail
        # job.start(parse=False, verbose = True)
        job.options.pdb    = DFLT_DEMO_PDB
        job.options.pepseq = DFLT_DEMO_PEPSEQ
        job.options.peppdb = None

    # Re-check command line and arguments are OK after demo setup
    job.start(parse = False, verbose = True) # Now we check
    if not job.cmd_line_status:
        sys.stderr.write("PEP-SiteFinder: Sorry, incorrect command line.\n")
        if True or REMOTE_SERVER:
            cluster.progress("PEP-SiteFinder : failed when verifying input data. Please check carefully.")
        sys.exit(-1)

    # Display options
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)

    # From here, ready to proceed
    #~ job.run(job.options.verbose)
    job.run(verbose = True)
    if True or REMOTE_SERVER:
        cluster.progress("PEP-SiteFinder : completed successfully.")

    return

# GO !
if __name__ == "__main__":
    main(sys.argv)
# -------------------------
