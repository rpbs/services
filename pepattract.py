#!/usr/bin/env python

import sys
import os
import glob
import argparse
import re
import shutil

# MTi cluster imports

import cluster
import Fasta.Fasta as Fasta
import PyPDB.PyPDB as PDB

# Constants

DFLT_MOBYLE_JOBS_PATH = "/data/jobs/pepATTRACT"
DFLT_PEPTIDE_MAX_SIZE = 20

# Initialize templates

from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('pepATTRACT','templates'))


def formatViewer(output_file, wdir = "./"):
        
    data = []
    
    pdb_files = glob.glob("result*.pdb")
    
    for i in enumerate(pdb_files):
        data.append(('Model %d' % (i[0] + 1), i[1]))

    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('pepattract.vis.html')
        sink.write(template.render(data = data, color = 'color-succession', wdir = wdir))


def formatInterfacesTable(table_file, output_file, wdir = "./"):

    with open(output_file, 'wt') as sink:
        # Read CSV data
        title = 'Interfaces'
        order = '[1, "desc"]'
        headers = ['Residue', 'Contacts']
        data = []
        with open(table_file, 'r') as csv:
            next(csv)
            for line in csv:
                if line.split()[0] == '1':
                    data.append(line.split()[2:])
        # Generate output table
        template = jinja_env.get_template('pepattract.table.html')
        sink.write(template.render(title = title, order = order, headers = headers, data = data))


def formatEnergiesTable(energies_file, output_file, wdir = "./"):

    with open(output_file, 'wt') as sink:
        # Read CSV data
        title = 'Energies'
        order = '[1, "asc"]'
        headers = ['Model', 'Energy']
        data = []
        with open(energies_file, 'r') as dat:
            i = 0
            for line in dat:
                m = re.match("## Energy: (\S+)", line)
                if m:
                  i += 1
                  data.append(('<a href="%s/result-%s.pdb" target="blank">Model %s</a>' % (wdir, i, i), m.groups()[0]))
        # Generate output table
        template = jinja_env.get_template('pepattract.table.html')
        sink.write(template.render(title = title, order = order, headers = headers, data = data))



def cmdLine():

    parser = argparse.ArgumentParser(description=__doc__)
    
    parser.add_argument("--seq", help="Peptide sequence")
    
    parser.add_argument("--pdb", help="Protein PDB file")
    
    parser.add_argument("--iattract", help="Perform iATTRACT refinement", action="store_true")

    return parser


def checkArgValues(args):

    if not args.pdb:
        msg = "No protein file specified."
        return False, msg

    if os.path.split(args.pdb)[0]:
        shutil.copy(args.pdb, "./")
        args.pdb = os.path.split(args.pdb)[1]

    status, msg = PDB.isPDB(args.pdb)
    
    if not status:
        return False, msg

    if not args.seq:
        msg = "No sequence file specified."
        return False, msg
        
    try:
        seqs = Fasta.fasta(args.seq)
        pepsize = len(seqs[seqs.ids()[0]])
        if pepsize > DFLT_PEPTIDE_MAX_SIZE:
            msg = "Peptide size should not exceed %s." % DFLT_PEPTIDE_MAX_SIZE
            return False, msg
    except IOError:
        msg = "%s is not a fasta file" % args.seq
        return False, msg
        
    args.seq = seqs[seqs.ids()[0]].s()

    return True, ""


def main():

    parser = cmdLine()
    p_args = parser.parse_args()
    status, msg = checkArgValues(p_args)

    if not status:
        cluster.progress(msg)
        sys.exit(0)

    cmd = "pepattract.py"
    s_args = ["--seq %s" % p_args.seq,
              "--pdb %s" % p_args.pdb]
      
    if p_args.iattract:
      s_args.append("--iattract")
      
    cluster.runTasks(cmd, s_args, job_opts = '-c 12', partition = "bigmemory", qos = "onejobpernode", docker_img = "attract")

    if p_args.iattract:
      cmd = "Minimize_Amber"
      s_args = []
      pose_files = glob.glob('./result-iattract-*.pdb')
      cluster.runTasks(cmd, s_args, docker_img = "post-treatment", map_list = pose_files)

    jobId = os.path.basename(os.getcwd())
    
    wdir = DFLT_MOBYLE_JOBS_PATH + "/" + jobId

    formatViewer("pepattract.vis.html", wdir)
    formatInterfacesTable("result.interface", "pepattract.interfaces.html", wdir)
    formatEnergiesTable("result.dat", "pepattract.energies.html", wdir)

if __name__ == "__main__":
    
    main()
    sys.exit(0)
