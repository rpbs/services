
#!/usr/bin/env python

# import PyGreedy.PyGreedy

# ------------------------- Globals
VERSION = "1.0"
# -------------------------

PEPFOLD_ROOT      = "/usr/local/PEPFOLD2"

PEPFOLD_PROGRESS_FILE = ".progress.txt"

DOCKER_IMG        = "pep-sitefinder/1.0-rpbs"

DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"

REMOTE_SERVER     = True  # If True, you can specify the remote python interpreter by setting the IPYTHON value
REMOTE_PYTHON     = "/usr/local/python2.6/bin/python"
SYNCHRONOUS       = False # If true, the process will not exit after sge submit, but wait cfor completion (required for Mobyle)

# The fst and pdb files should be copied into the PEPFOLD_ROOT
DFLT_DEMO_SEQUENCE = "1mmc.fst"
DFLT_DEMO_PDB      = "1mmc.pdb"
DFLT_DEMO_LABEL    = "1mmc"
DFLT_DEMO_SSBONDS  = "4-15:9-20:14-28" #  None is valid

DFLT_DEMO_SEQUENCE = "1jbl.fst"
DFLT_DEMO_PDB      = "1jbl.pdb"
DFLT_DEMO_LABEL    = "1jbl"
DFLT_DEMO_SSBONDS  = "3-11"

DFLT_WPATH        = "."
DFLT_MAX_LETTERS  = 8
DFLT_LABEL        = "MINI_FOLD"
DFLT_BIN_PATH     = "%s/bin" % PEPFOLD_ROOT
DFLT_PRG          = ""
DFLT_BLAST_DBPATH = "%s/lib/data/blast/2006-04-27" % PEPFOLD_ROOT
DFLT_BLAST_BANK   = "uniref90"

DFLT_MIN_SEQ_SIZE = 9
DFLT_MAX_SEQ_SIZE = 50 # 50
DFLT_MAX_SIM_SIZE = 36 # 36

DFLT_KEYS         = ["sOPEP", "RMSd","TMalign"]
DFLT_SORT_KEY     = "sOPEP" # one of "sOPEP", or, if reference specified, "RMSd" or "TMalign"

# Oscar-star library (if using oscar-star)
MULUL="/usr/local/OSCAR/library/z/" # oscar-star library path

# Pymol color palette
PML_COL = ["marine","magenta","limegreen","dash","br8",
            "lightblue","cyan","lightpink","purpleblue","sulfur",
            "oxygen","deepteal","chocolate","br4","lightteal"]

DFLT_CLUSTERING_THRESHOLD = 0.60
DFLT_PWSCORE   = "TMscore"
PWSCORES   = {'cRMSd':'cRMSd',
              'TMscore':'TMscore',
              'GDT_TS':'GDT_TS',
              'BCscore':'BCscore',
              'TMscoreRMSd': 'TMscoreRMSd'}
DFLT_NPROC = 8
