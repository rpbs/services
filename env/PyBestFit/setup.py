#!/usr/bin/env python
from distutils.core import setup

setup(name='PyBestFit',
      version='1.0',
      description='A basic tool to superimpose 2 proteins',
      long_description="""\
PyHHsearch : a python class to run QBestFit to superimpose 2 PDBs.

Written (2008-2011) by P. Tuffery, INSERM, France

This class is used in production since 2010 at the RPBS structural
bioinformatics platform. 

""",
      author='P. Tuffery',
      author_email=['pierre.tuffery@univ-paris-diderot.fr'],
      url='http://bioserv.rpbs.univ-paris-diderot.fr',
      scripts = ['doBestFit'],
      packages = ['PyBestFit'],
      # py_modules=['PyBlast'],
      classifiers=['License :: OSI Approved :: GNU General Public License (GPL)',
                   'Operating System :: Unix',
                   'Programming Language :: Python',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
      license='GNU General Public License (GPL)'
     )

