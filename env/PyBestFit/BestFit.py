#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# HMM-SA tools upon the PDB class
#
# Written (2001-2004) by P. Tuffery, INSERM, France
#
# No warranty of any kind is provided
#
"""
La classe BestFit: a wrapper over sert a encoder dans HMM-SA
Les quelques globales ci-dessous peuvent etre editees
selon la machine
"""

import string
import sys
import os
import copy
import math
import gzip
import types
import popen2
import subprocess

import PyPDB.PyPDB as PDB

from Config import *
GDFLTQBESTFITBIN      = DFLT_PRG
GDFLTBINPATH          = DFLT_BESTFIT_BIN_PATH


def shellExec_pb( cmd, stdinl = [], ignerr = False, verbose = 0 ):
    """
    Execute a shell command, with pipelines to stdin, stdout, and stderr.
    
    @param cmd    : the command line
    @param stdinl : string list to pipe as input
    @param ignerr : ignore stderr process content
    @param verbose: verbose mode

    @return: a tuple as (stdout lines list,stderr lines list)
    """

    if verbose:
        print >> sys.stderr, "ShellExec command: %s" % cmd

    if len( stdinl ):
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdin=subprocess.PIPE, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
        # Prepare stdin
        iStr = ""
        for aIn in stdinl:
            iStr += "%s\n" % aIn
        # Write to stdin
        p.stdin.write( "%s" % iStr )
        # Close pipe
        p.stdin.close()
    else:
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
    
    # Read stdout/err
    stdoutl = []
    stderrl = []
    while True:
        aStdout = p.stdout.readline()
        aStderr = p.stderr.readline()
        if verbose >1:
            print >> sys.stderr, aStdout,
        if verbose >1:
            print >> sys.stderr, aStderr,
        if aStdout:
            stdoutl.append( aStdout )
        if aStderr:
            stderrl.append( aStderr )
        if not aStdout and not aStderr:
            break

    if len(stderrl) and not ignerr:
        writeList( sys.stderr, stderrl )

    if verbose:
        print >> sys.stderr, ""

    return (stdoutl,stderrl)


def shellExec( cmd, stdinl = [], ignerr = False, fake = False, verbose = 0 ):
    """
    Execute a shell command, with pipelines to stdin, stdout, and stderr.

    @param cmd    : the command line
    @param stdinl : string list to pipe as input
    @param ignerr : ignore stderr process content
    @param fake   : only prints the command line
    @param verbose: verbose mode

    @return: a tuple as (stdout lines list,stderr lines list)
    """

    if verbose:
        print >> sys.stderr, "ShellExec command: %s" % cmd

    if fake:
        print >> sys.stdout, "[FAKE]\n%s" % cmd
    
    p = subprocess.Popen(cmd,
                         shell=True,
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         close_fds=True)
    if len( stdinl ):
        # Prepare stdin
        iStr = ""
        for aIn in stdinl:
            iStr += "%s\n" % aIn
    else:
        iStr = None

    (stdoutl,stderrl) = p.communicate( input=iStr )
    
    if verbose:
        print >> sys.stderr, "Command done."

    return (stdoutl.splitlines(),stderrl.splitlines())



def writeList( handle, lines, ret = 0, verbose = 0 ):
    """
    Write a string (list) to an open file handle

    @param handle : the file handle
    @param lines  : line(s) (list) to write
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """

    if isinstance( lines, str ):
        if ret:
            aStr = '%s\n' % lines
        else:
            aStr = '%s' % lines
        try:
            handle.write( aStr )
        except:
            print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % lines
            sys.exit(0)
    elif isinstance( lines, list ):
        for line in lines:
            if ret:
                aStr = '%s\n' % line
            else:
                aStr = '%s' % line
            try:
                handle.write( aStr )
            except:
                print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % line
                sys.exit(0)
    try:
        handle.flush()
    except:
        pass
    return

#
# This is a simple wrapper of QBestFit
# but, it (will) can iterate to refine the fit.
#
class bestFit:
    """
    @author: P. Tuffery

    the bestFit class is basically a wrapper over the QBestFit and TM programs
    """

    def __init__(self, ncrd = 0, qxyz = [], txyz = [], method = GDFLTQBESTFITBIN, binPath = GDFLTBINPATH, niter = 1, maxRMSd = 3.):
        """
        bestFit.__init__:

        @param ncrd : size of xyz arrays
        @param qxyz : query xyz array    (undergo transformation)
        @param txyz : template xyz array (do not undergo transformation)
        @param method: QBestFit or equivalent binary
        @param binPath: path to binaries

        NOT USED:
        @param niter: 
        @param maxRMSd: these are left in prevision of iterative 3D alignment.

        """
        self.binPath  = binPath
        self.method   = method

        self.niter    = niter
        self.maxRMSd  = maxRMSd
        self.orirmsd  = 0.
        self.bestrmsd = 0.
        self.ncrd     = ncrd
        self.qxyz     = qxyz
        self.txyz     = txyz
        self.ocrd     = []
        self.M1       = ""
        self.M2       = ""
        self.M3       = ""
        self.M4       = ""

    #
    # Call to best fit calculation
    # The two sets of corresponding crds are specified.
    # crd1 and crd2 are ncrd lines of 3 crds (x,y,z) for each point.
    # This will return : ori and best fit RMSd, the transformation matrix,
    # and if required, the crd1 crds superimpoed onto crd2.
    #
    def bestFit_old(self, getFit = 0):
        """
        bestFit: will call QBestFit
        On return: gets rmsd values and the transformation matrix
        Optionally gets the best fit crds.
        @param:
        getFit: Boolean. Optionally return best fit crds (the default is [])
        @return: 
        rmsd_before_fit, rmsd_after_fit, 4 lines of TM matrix, best fit crds

        """
        oristdout = sys.stdout
        cmd = self.binPath+"/"+self.method+" "
        if getFit:
            cmd += "-bfxyz "
        cmd += "2> /dev/null"
        
        fin, sys.stdout = popen2.popen2(cmd)

        # We send data to method
        print self.ncrd
        for i in self.txyz:
            print i
        for i in self.qxyz:
            print i
        sys.stdout.close()
        sys.stdout = oristdout

        # we get the results
        self.orirmsd  = fin.readline(),
        self.bestrmsd = fin.readline(),
        self.M1 = fin.readline()
        self.M2 = fin.readline()
        self.M3 = fin.readline()
        self.M4 = fin.readline()
        
        self.ocrd = []
        if getFit:
            for i in range(0,self.ncrd):
                self.ocrd.append(fin.readline()[:-1])
        fin.close()

        # return the result
        return string.split(self.orirmsd[0])[1], string.split(self.bestrmsd[0])[1], [self.M1, self.M2, self.M3, self.M4], self.ocrd

    def bestFit(self, getFit = 0, verbose = False):
        """
        bestFit: will call QBestFit
        On return: gets rmsd values and the transformation matrix
        Optionally gets the best fit crds.
        @param:
        getFit: Boolean. Optionally return best fit crds (the default is [])
        @return: 
        rmsd_before_fit, rmsd_after_fit, 4 lines of TM matrix, best fit crds

        """
        cmd = self.binPath+"/"+self.method+" "
        if getFit:
            cmd += "-bfxyz "
        cmd += "# 2> /dev/null"
        
        # fin, sys.stdout = popen2.popen2(cmd)

        stdinl = []
        stdinl.append("%d" % self.ncrd)
        for i in self.txyz:
            stdinl.append("%s" % str(i))
        for i in self.qxyz:
            stdinl.append("%s" % str(i))
        # We send data to method
        if verbose:
            for l in stdinl:
                sys.stderr.write("%s\n" % l)
            
        (cOut, cErr) = shellExec( cmd, stdinl = stdinl, ignerr = False, verbose = verbose )
        if verbose:
            sys.stderr.write("%s\n" % cOut)

        # we get the results
        self.orirmsd  = cOut[0][:-1]
        self.bestrmsd = cOut[1][:-1]
        self.M1 = cOut[2][:-1]
        self.M2 = cOut[3][:-1]
        self.M3 = cOut[4][:-1]
        self.M4 = cOut[5][:-1]
        
        self.ocrd = []
        if getFit:
            for i in range(0,self.ncrd):
                self.ocrd.append(cOut[6+i][:-1])

        # return the result
        return string.split(self.orirmsd)[1], string.split(self.bestrmsd)[1], [self.M1, self.M2, self.M3, self.M4], self.ocrd



    #
    # transform the crds of one PDB instance
    #
    def PDBTM(self, x, TM, ffrom = None, tto = None, BINPATH=GDFLTBINPATH, verbose = 0):
        """
        @param x : the PDB instance
        @param TM: the 4x4 transformation matrix to apply using TM
        @param from : start of region to fit. If None: 0
        @param tto  : en dof region to fit (excluded NOTE: -1 won't work!). If None: len(x)
        @param BINPATH: the path to the TM binary

        @return x[ffrom:tto+1] transformed
        """
        if ffrom == None:
            ffrom = 0
        if tto == None:
            tto = len(x)
        # print ffrom, tto
        # return

        z   = PDB.atmList(x[ffrom:tto].atms)
        rs  = PDB.atmList(x[ffrom:tto].atms)
        crds = z.crds()

        stdinl = []
        stdinl.append("%d" % len(z))
        for i in crds:
            stdinl.append(i)
        stdinl.append(TM[0])
        stdinl.append(TM[1])
        stdinl.append(TM[2])
        stdinl.append(TM[3])

        cmd = BINPATH+"/TM"        
        (cOut, cErr) = shellExec( cmd, stdinl = stdinl, ignerr = False, verbose = verbose )
        if verbose:
            sys.stderr.write("%s\n" % cOut)

        if verbose:
	    sys.stderr.write("Will setup new crds for %d atoms\n" % len(rs))
        for i, lrs  in enumerate(cOut):
            crds = lrs.split()
            if verbose:
		sys.stderr.write("%d : %f %f %f\n" % (i,float(crds[0]), float(crds[1]), float(crds[2])) )
            rs[i].setcrds(float(crds[0]), float(crds[1]), float(crds[2]) )
        return PDB.PDB(rs.list, id = x.id, verbose = 0)

    #
    # transform the crds of one PDB instance
    #
    def PDBTM_old(self, x, TM, ffrom = None, tto = None, BINPATH=GDFLTBINPATH, verbose = 0):
        """
        @param x : the PDB instance
        @param TM: the 4x4 transformation matrix to apply using TM
        @param from : start of region to fit. If None: 0
        @param tto  : en dof region to fit (excluded NOTE: -1 won't work!). If None: len(x)
        @param BINPATH: the path to the TM binary

        @return x[ffrom:tto+1] transformed
        """
        if ffrom == None:
            ffrom = 0
        if tto == None:
            tto = len(x)
        # print ffrom, tto
        # return

        oristdout = sys.stdout
        fin, sys.stdout = popen2.popen2(BINPATH+"/TM")

        z   = PDB.atmList(x[ffrom:tto].atms)
        rs  = PDB.atmList(x[ffrom:tto].atms)
        crds = z.crds()
        print len(z)
        for i in crds:
            print i
        print TM[0]
        print TM[1]
        print TM[2]
        print TM[3]
        sys.stdout.close()
        sys.stdout = oristdout

        if verbose:
	    sys.stderr.write("Will setup new crds for %d atoms\n" % len(rs))
        for i in range(0,len(rs)):
            lrs= fin.readline()
            crds = lrs.split()
            if verbose:
		sys.stderr.write("%d : %f %f %f\n" % (i,float(crds[0]), float(crds[1]), float(crds[2])) )
            rs[i].setcrds(float(crds[0]), float(crds[1]), float(crds[2]) )
        return PDB.PDB(rs.list, id = x.id, verbose = 0)

    def PDBBestFit(self, template, query, outFit = True, verbose = False):
        """
        @param template: a PDB instance of template (do not move)
        @param query   : a PDB instance of query (undergo the fit)

        the Two PDBs must be the same lenght

        @return:
        (orirmsd, bestfitrmsd, TM, a PDB instance where query is best fit onto template)
        """
        q = PDB.atmList(query.select(awhat=["CA"]).atms)
        t = PDB.atmList(template.select(awhat=["CA"]).atms)
        if len(q) != len(t):
            sys.stderr.write("BestFit.PDBBestFit : query and template have different lengths.\n")
            if verbose:
                sys.stderr.write("query:\n")
                sys.stderr.write("%s" % q)
                sys.stderr.write("template:\n")
                sys.stderr.write("%s" % t)
            return None, None, None, None
        self.qxyz = q.crds()
        self.txyz = t.crds()
        self.ncrd = len(q)
        
        # We get the transforamtion matrix adn the cRMSd
        # orirmsd, bfrmsd, TM1, TM2, TM3, TM4, ocrds = self.bestFit()
        orirmsd, bfrmsd, TM, ocrds = self.bestFit()
        
        # Now we produce best fit PDB crds (apply TM to query PDB)
        # rs = self.PDBTM(query, [TM1, TM2, TM3, TM4])
        rs = None
        if outFit:
            rs = self.PDBTM(query, TM)
        # return orirmsd, bfrmsd, [TM1, TM2, TM3, TM4], rs
        return orirmsd, bfrmsd, TM, rs
	
    def TM2(self, crdList, M1, M2, M3, M4, BINPATH="/data/bin/"):
        """
        @param:
        crdList: a list of xyz lines
        M1,M2,M3,M4: the 4 lines of the TM matrix
        BINPATH: path to the TM binary

        @return:
        modified crdList
        """
        oristdout = sys.stdout
        fin, sys.stdout = popen2.popen2(BINPATH+"/TM")
        print len(crdList)
        for i in crdList:
            print i
        print M1
        print M2
        print M3
        print M4
        sys.stdout.close()
        sys.stdout = oristdout

##     orirmsd = string.split(orirmsd[0])[2]
##     rmsd = string.split(rmsd[0])[1]
        rs = []
        for i in range(0,len(crdList)):
            lrs= fin.readline()
            rs.append(lrs[:-1])
        # print rs
        # sys.exit(0)
        return rs

