#!/usr/bin/env python

# ------------------------- Globals
VERSION = 1.0

BESTFIT_ROOT          = "/nfs/freetown/user/tuffery/prgs/PEPFOLD2_sge4slurmm/PyBestFit"
BESTFIT_ROOT          = "/service/env/PyBestFit"
# BESTFIT_ROOT          = "/nfs/freetown/user/tuffery/prgs/PyBestFit"
DFLT_BESTFIT_BIN_PATH  = "%s/bin" % BESTFIT_ROOT
DFLT_PRG              = "QBestFit"
