#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define DtFloat		double	        /* Le type flottant.	        */
typedef DtFloat         DtPoint3[3];
typedef DtFloat         DtPoint4[4];
typedef DtFloat         DtMatrix3x3[3][3];
typedef DtFloat         DtMatrix4x4[4][4];


#define boucle(i,min,max) for((i)=(min);(i)<(max);(i)++)
#define max(a,b) (a<b?b:a)
#define min(a,b) (a<b?a:b)
#define sign(a,b) ((b)>=0.0 ? fabs(a) : -fabs(a))
#define EPS 1.e-10


/* ===============================================================
 * ---------- Transformation d'une coordonnee par rmat. ----------
 * on est dans la forme (x,y,z,1) (M00 M01 M02 0)
				  (M10 M11 M12 0)
				  (M20 M21 M22 0)
				  (Tx  Ty  tZ  1)
 * =============================================================== */

void crdRotate(DtPoint3 p,DtMatrix4x4 rmat)
{
  double x,y,z;

  x = p[0] * rmat[0][0] + p[1] * rmat[1][0] + p[2] * rmat[2][0] + rmat[3][0]; 
  y = p[0] * rmat[0][1] + p[1] * rmat[1][1] + p[2] * rmat[2][1] + rmat[3][1]; 
  z = p[0] * rmat[0][2] + p[1] * rmat[1][2] + p[2] * rmat[2][2] + rmat[3][2]; 
  p[0] = x;
  p[1] = y;
  p[2] = z;
}


int main(int argc, char *argv[])
{
  DtPoint3 *c1, *c2;
  DtMatrix4x4 M;
  double squared_rmsd;
  int i,j;
  int n;
  
  fscanf(stdin,"%d",&n);
  c1 = calloc(n,sizeof(DtPoint3));

  for (i=0; i<n; i++) {
    j = i*3 + 1;
    fscanf(stdin,"%lf%lf%lf",&c1[i][0],&c1[i][1],&c1[i][2]);
  }
  fscanf(stdin,"%lf%lf%lf%lf",&M[0][0],&M[0][1],&M[0][2],&M[0][3]);
  fscanf(stdin,"%lf%lf%lf%lf",&M[1][0],&M[1][1],&M[1][2],&M[1][3]);
  fscanf(stdin,"%lf%lf%lf%lf",&M[2][0],&M[2][1],&M[2][2],&M[2][3]);
  fscanf(stdin,"%lf%lf%lf%lf",&M[3][0],&M[3][1],&M[3][2],&M[3][3]);

  for (i=0; i<n; i++) {
    crdRotate(c1[i],M);
    fprintf(stdout,"%8.3lf%8.3lf%8.3lf\n",c1[i][0],c1[i][1],c1[i][2]);
  }

/*   for (i=0; i<n; i++) { */
/*     fprintf(stdout,"ATOM  %5d  %.3s%c%.3s %c%4d%c   %8.3lf%8.3lf%8.3lf\n", */
/* 	    i, "CA", ' ', "ALA", ' ',i,' ',c2[i][0],c2[i][1],c2[i][2]); */
/*   } */

}
