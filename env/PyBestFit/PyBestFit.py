#!/usr/bin/env python

import sys
sys.path.append("/nfs/eurydice/PyTools/Classes")
sys.path.append("/nfs/eurydice/PyTools/Classes/HMMSA")

import BestFit
import PyPDB.PyPDB as PDB

def printUsage(exitCode):

    print >> sys.stderr, "Usage: superposePDBs.py [OPTIONS] -r [PDB file] -m [PDB file]"

    print >> sys.stderr, ""
    print >> sys.stderr, " -r : reference PDB file"
    print >> sys.stderr, " -m : mobile PDB file"

    print >> sys.stderr, ""
    print >> sys.stderr, "OPTIONS:"
    print >> sys.stderr, " -o : output fitted PDB file (stdout)"
    print >> sys.stderr, " -z : specify zones to fit"
    print >> sys.stderr, "      ref1from:ref1to::mobi1from:mobi1to|ref2from:ref2to::mobi2from:mobi2to"
    print >> sys.stderr, " -h : prints help"
    print >> sys.stderr, " -v : verbose mode"
    
    sys.exit(exitCode)

def argsParse( argList, verbose = 0):

    if verbose:
        print >> sys.stdout, "argsParse: Will parse arguments ..."

    # the arguments dictionnary
    argsDict = {}
    argsDict['outPDBFN'] = None
    # -------- HELP
    try:
        argList.index("-h")
        help = 1
    except:
        help = 0

    if help:
        printUsage(1)
        
    # -------- VERBOSE
    try:
        argList.index("-v")
        argsDict['verbose'] = 1
    except:
        argsDict['verbose'] = 0

    # -------- REFERENCE PDB FILE
    try:
        refFNR = argList.index("-r")
        argsDict['refFN'] = argList[refFNR +1]
    except:
        print >> sys.stderr, "[ERROR] No reference pdb file precized !"
        printUsage(0)

    # -------- MOBILE PDB FILE
    try:
        mobiFNR = argList.index("-m")
        argsDict['mobiFN'] = argList[mobiFNR +1]
    except:
        # You can ommit mobile PDB files in case of
        # models in refererence file
        try:
            tmpPDB = loadPDBfile( argsDict['refFN'] )
        except:
            print >> sys.stderr, "[ERROR] Invalid reference pdb file format !"

        if not tmpPDB.nModels():                
            print >> sys.stderr, "[ERROR] No mobile pdb file precized !"
            printUsage(0)
            
        del(tmpPDB)
        argsDict['mobiFN'] = None
            
    # -------- OUTPUT PDB file
    try:
        outPDBFNR = argList.index("-o")
        argsDict['outPDBFN'] = argList[outPDBFNR +1]
    except:
        print >> sys.stderr, "No output file precized for PDB, will use stdout ..."
        argsDict['outPDBFN'] = None

    # -------- restrict superposition to zones
    try:
        znDef = argList.index("-z")
        argsDict['zones'] = parseUsrDefZn( argList[ znDef  +1], argsDict['verbose'] )
    except Exception:
        argsDict['zones'] = {}
        argsDict['zones']['ref']  = [[0,-1]]
        argsDict['zones']['mobi'] = [[0,-1]]
    return argsDict

def zonesOverlap( theZones ):
    """
    We test if zones overlaps.
    """

    for aZone1 in range( 0, len(theZones)-1, 1 ):
        for aZone2 in range( aZone1+1, len(theZones), 1 ):
            if ( theZones[ aZone1 ][0] >= theZones[ aZone2 ][0] and theZones[ aZone1 ][0] <= theZones[ aZone2 ][1] ) or ( theZones[ aZone1 ][1] >= theZones[ aZone2 ][0] and theZones[ aZone1 ][1] <= theZones[ aZone2 ][1] ):
                return True

    return False

def parseUsrDefZn( znDef, verbose = 0 ):
    """
    Parse user defined zones.
    
    syntax is : 
         zone1|zone2|...|zoneN
    with zoneX    = RefZoneX::MobiZoneX
         RefZoneX = from-to
    exemple:
         12:18::13:19|24:30::25:31
    """
    if verbose:
        print >> sys.stderr, "parseUsrDefZn: will parse user zone definition: %s " % znDef

    zones = []
    a = znDef.split("|")
    for b in a:
        c = b.split("::")
        tmp1 = []
        for d in c:
            e = d.split(":")
            tmp2 = []
            for f in e:
                try:
                    tmp2.append( int(f) )
                except:
                    print >> sys.stderr, "Error: Invalid pattern for zones !"
                    sys.exit(0)
            tmp1.append(tmp2)
        zones.append(tmp1)

    # Correctly represents selections
    # Ref  = index 0
    # Mobi = index 1
    sZones = {}
    sZones['ref']  = []
    sZones['mobi'] = []

    for aZone in zones:
        sZones['ref'].append( aZone[0] )
        if len(aZone) > 1:
            sZones['mobi'].append( aZone[1] )
        else:
            print >> sys.stderr, "Error: %d zone(s) defined for mobile and %d for reference structure !" % (len(sZones['mobi']), len(sZones['ref']))
            sys.exit(0)

        # Check zones length
        if ( aZone[0][1] - aZone[0][0] ) != ( aZone[1][1] - aZone[1][0] ):
            print >> sys.stderr, "Error: zones lengths differs: [%d-%d] vs [%d-%d] !" % ( aZone[0][0], aZone[0][1], aZone[1][0], aZone[1][1] )
            sys.exit(0)

    if verbose:
        print >> sys.stderr, " -> ", sZones

    # check lists overlapping
    if zonesOverlap( sZones['ref'] ):
        print >> sys.stderr, "Error: zones definition must not overlap ! Reference:", sZones['ref']
        sys.exit(0)
    if zonesOverlap( sZones['mobi'] ):
        print >> sys.stderr, "Error: zones definition must not overlap ! Mobile:", sZones['mobi']
        sys.exit(0)
        
    return sZones

def PDBZones( iPDB, zones = None, verbose = 0 ):
    """
    Extract a PDB giving zones definition
    """
    import math

    c = 0
    if zones:
        for aZone in zones:
            # negative index compatibility
            if aZone[0] < 0:
                aZone[0] = len(iPDB) + aZone[0]
            if aZone[1] < 0:
                aZone[1] = len(iPDB) + aZone[1]

            theSze = int( math.fabs( aZone[1] - aZone[0] ) ) +1
            if c == 0:
                oPDB = iPDB.mask( aZone[0], aZone[1]+1, mask = "X" * theSze )
            else:
                oPDB += iPDB.mask( aZone[0], aZone[1]+1, mask = "X" * theSze )
            c += 1
    else:
        oPDB = iPDB

    return oPDB

def pairRMSd(refFN, mobiFN, refZones = None, mobiZones = None, outFN = None, verbose = 0):
    x = PDB.PDB(refFN, altCare = 1)
    y = PDB.PDB(mobiFN, altCare = 1)
    
    xz = PDBZones(x, refZones)
    yz = PDBZones(y, mobiZones)

    # We check residue lists are ok.
    if len(xz) != len(yz):
        return "NaN"

    if verbose:
        sys.stderr.write("pairRMSd: will match lenghts %d - %d\n" %( len(xz), len(yz)) )
    # sys.exit(0)
    bf = BestFit.bestFit()
    
    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(xz,yz, outFit = False)
    rs = bf.PDBTM(y, TM)

    if verbose:
        sys.stderr.write("%s %s %s\n" % (refFN, mobiFN, str(bfrmsd)))

    if outFN:
        rs.out(outFN)
    return bfrmsd
    
def main( argv):
    # Parse arguments
    argsDict = argsParse( argv )

    x = PDB.PDB(argsDict['refFN'])
    y = PDB.PDB(argsDict['mobiFN'])
    # print len(x), len(y)

    xz = PDBZones(x, argsDict["zones"]['ref'])
    yz = PDBZones(y, argsDict["zones"]['mobi'])
    # print len(xz), len(yz)

    # sys.exit(0)

    bf = BestFit.bestFit()

    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(xz,yz)
    rs = bf.PDBTM(y, TM)

    print argsDict['refFN'], argsDict['mobiFN'], bfrmsd

    sys.exit(0)
    if argsDict['outPDBFN']:
        rs.out(argsDict['outPDBFN'])


if __name__ == "__main__":

    main (sys.argv)
