#!/usr/bin/env python
"""
This script is used to read the contact map submitted to score coevolution/deep learning constraints.
First we check if the beginning of the file contains the sequences used to generated the coevolution alignment.
Otherwise the sequences extracted from the pdb file are used as default.
The script returns as output the contact map file, the sequence files for A and B.
If A and B are 'None' they will be copied from protein_a/protein_a.fst, etc

HELP
Type 'python analyzeContactMap.py' for usage info
Refer to paper and/or help file for more details
"""

### LOADING LIBRARIES
try:
    # Standard libraries
    import sys
    import os
    import re
    import copy
    from operator import itemgetter
    from collections import defaultdict

    # for python version compatibility
    try:
        import ConfigParser
    except:
        import configparser as ConfigParser
except:
    print("ERROR LIBRARY IMPORTATION")
    print("CHECK YOUR PATHS AND THE PROGRAMS REQUIRED")
    sys.exit(1)

try:
    import cluster.cluster as cluster
except:
    print("ERROR IMPORTING cluster")
    print("THIS PROGRAM MAY NOT RUN PROPERLY ON RPBS SERVER")

def is_float(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def is_integer(value):
  try:
    int(value)
    return True
  except ValueError:
    return False


class analyzeContactMap():

    def __init__(self, contact_map_file="", cmap_output='', sequence_a='', sequence_b='',
                 range_index_redundancy_group=0, is_value_proba=True, frodock_cmap_seqA='', frodock_cmap_seqB='',
                 frodock_cmap_coevo=''):
        """
        This class formats the information included in the contact coevolution map input from the web page

        @param contact_map_file: file obtained directly from the web server (may contain sequences input in header)
        @param cmap_output: contact map once validated with redundancy groups added
        @param sequence_a: sequence a will be used if not available in the header of contact_map_file
        @param sequence_b: sequence b will be used if not available in the header of contact_map_file
        @param range_index_redundancy_group: on how many position redundancy should be calculated
        @param is_value_proba: True if contact value reports a proba or a coupling strength (higher=better). False if it a distance (lower=better)
        @param frodock_cmap_seqA: path to the file sequence_a
        @param frodock_cmap_seqB: path to the file sequence_b
        @param frodock_cmap_coevo: path to the file cmap_output

        @return: self.output dict with keys: 'cmap_output', 'sequence_a', 'sequence_b', 'frodock_cmap_seqA', 'frodock_cmap_seqB', 'frodock_cmap_coevo'
        """
        # The input provided in the server contains the 2 fasta sequences used to generate the alignment followed by the contact list
        self.contact_map_file = contact_map_file  # contact_map as given by the user in the InterEvDock3 server

        self.cmap_output = cmap_output  # frodock-compatible txt output ready for frodock_sco3.2 scoring of coevolution
        self.cmap_seqA = sequence_a  # the first sequence in the input (fasta format) corresponding to the receptor
        self.cmap_seqB = sequence_b  # the second sequence in the input (fasta format) corresponding ot the ligand

        self.is_redundancy_group_added = False  # A variable used to flag whether user imposed redundancy group index or not
        # If 5-cols in self.cmap_output set to true in self.check_format()
        self.is_constraint_type_proba = True  # A variable used to know whether proba or distance were given by user

        # read contraint file, extracts sequences and list of contacts as txt
        self.cmap_seqA, self.cmap_seqB, self.cmap_output , self.d_param = self.extract_information()
        if 'REDUNDANCY' in self.d_param:
            self.is_redundancy_group_added = True
            range_index_redundancy_group = int(self.d_param['REDUNDANCY'])
        if 'DIST' in self.d_param:
            pass
        if 'MODE' in self.d_param:
            if self.d_param['MODE'] == 'PROBA':
                self.is_constraint_type_proba = True
            if self.d_param['MODE'] == 'DIST':
                self.is_constraint_type_proba = False

        # =====================================
        # Check the format of the constraints
        # =====================================
        # Set self.redundancy_group_added True or False depending on the number of cols in self.cmap_output
        self.cmap_output, self.is_redundancy_group_added = self.check_format(self.cmap_output)
        # lines which do not fulfill the expected format were removed

        # ======================================================
        # Append at the end of each line a group of redundancy
        # ======================================================
        if not self.is_redundancy_group_added:
            self.cmap_output = self.append_redundancy_group(self.cmap_output, range_index_redundancy_group,
                                                            is_value_proba)
        # =================================================================
        # If sequences were not provided, get them from the PDB sequences
        # =================================================================
        if self.cmap_seqA == '' or self.cmap_seqB == '':
            # We have to decipher if the input was given as a sequence which was subsequently modeled or if it was given as a pdb.
            self.cmap_seqA = self.read_sequence_from_seq_or_pdb_inputs('A')
            self.cmap_seqB = self.read_sequence_from_seq_or_pdb_inputs('B')

        # clean up
        # The output is defined as a dictionary
        self.output = {}
        self.output["sequence_a"] = self.cmap_seqA
        self.output["sequence_b"] = self.cmap_seqB
        self.output["cmap"] = self.cmap_output

        # write frodock friendly constraints file
        if frodock_cmap_seqA:
            self.write_sequence(self.cmap_seqA, frodock_cmap_seqA)
        if frodock_cmap_seqB:
            self.write_sequence(self.cmap_seqB, frodock_cmap_seqB)
        if frodock_cmap_coevo:
            self.write_cmap(self.cmap_output, frodock_cmap_coevo)

        # The output is defined as a dictionary
        self.output = {}
        self.output["sequence_a"] = self.cmap_seqA  # content as a str
        self.output["sequence_b"] = self.cmap_seqB  # content as a str
        self.output["cmap"] = self.cmap_output  # content as a str
        self.output["path_sequence_a"] = frodock_cmap_seqA  # name of the files
        self.output["path_sequence_b"] = frodock_cmap_seqB
        self.output["path_cmap"] = frodock_cmap_coevo
        self.output["parameters"] = copy.deepcopy(self.d_param)

        print(">> Coevolution Contact Map analyzed : Contact Map file created under: \n %s" % self.cmap_output)

    def extract_information(self):
        """
        Parse the contact_map file and if sequences are detected in fasta format on top of the file returns the sequences for partner A and partner B
        @return: <str_fasta_partnerA>,<str_fasta_partnerB>,<str with a contact at each line>
                or '','',<str with a contact at each line> if no fasta sequences in header
        """
        l_sequence = []
        l_header = []
        contact_map = ''
        fasta_a = ''
        fasta_b = ''
        sequence_section = False
        contact_section = False
        count_sequence = 0
        d_param = dict()
        pat = re.compile("#(\w+)=(\S+)")
        with open(self.contact_map_file) as fcmap:
            for l in fcmap:
                if len(l) < 1:
                    continue
                s = l.split()
                if len(s)==0:
                    continue
                if s[0].isdigit():
                    contact_section = True
                    sequence_section = False
                    contact_map += l
                if l[0] == ">":
                    sequence_section = True
                    count_sequence += 1
                    l_header.append(l)
                    l_sequence.append("")
                elif l[0] == "#":
                    m = pat.search(l)
                    if m:
                       d_param[m.group(1)]=m.group(2)
                elif sequence_section and not contact_section:
                    l_sequence[-1] += l.strip()
        if len(l_header) == 2 and len(l_sequence) == 2:
            fasta_a = l_header[0] + l_sequence[0] + "\n"
            fasta_b = l_header[1] + l_sequence[1] + "\n"

        return fasta_a, fasta_b, contact_map, d_param

    def check_format(self, cmap_output):
        """
        Validate that the format of the contact map is one of the two expected :
        - A list of 3 columns with <index_residue_A> <index_residue_B> <a distance or a probability>
        or
        - A list of 5 columns is interpretated as  <index_residue_A> <index_residue_B> <a distance or a probability>
                                                    <redundancy group index> <best value (dist|proba) for the group>
        Toggle self.is_redundancy_group_added False -> True if 5 columns are found
        @param cmap_output:
        @return: recover a list of contacts containing only the contacts validated by format check
        """
        is_at_least_one_3_col = False  # Means that in at least one case we don't have the redundancy group. So we consider that the redundancy has to be recomputed for all.
        valid_lines = []
        for ll in cmap_output.split('\n'):
            format_3col_error_message = "Warning: checkContactMapFormat: ContactMap Line %s isn't written in proper format - " \
                                        "it will be ignored " \
                                        "(should have at least 3 cols : <receptor_res_index> <ligand_res_index> [<dist>|<proba>])" % (
                                            ll)
            format_5col_error_message = "Warning: checkContactMapFormat: ContactMap Line %s isn't written in proper format - " \
                                        "it will be ignored " \
                                        "(should have an integer in 4th col : <receptor_res_index> <ligand_res_index> [<dist>|<proba>]) <redundancy_group_index > [group <dist>|<proba>]" % (
                                            ll)
            x = ll.split()
            if len(x)==0:
                continue
            if len(x) > 0 and len(x) < 3:
                print(format_3col_error_message)
                if 'cluster' in sys.modules:
                    cluster.progress(format_3col_error_message)
            elif not is_integer(x[0]) or not is_integer(x[1]):
                print(format_3col_error_message)
                if 'cluster' in sys.modules:
                    cluster.progress(format_3col_error_message)
            elif len(x) == 5:
                if not is_integer(x[3]):
                    print(format_5col_error_message)
                    if 'cluster' in sys.modules:
                        cluster.progress(format_5col_error_message)
                elif is_integer(x[4]) or is_float(x[4]):
                    valid_lines.append(ll)
                else:
                    print(format_5col_error_message)
                    if 'cluster' in sys.modules:
                        cluster.progress(format_5col_error_message)
            elif len(x) in [3,4]:
                if is_float(x[2]):
                    valid_lines.append(ll)
                else:
                    print(format_3col_error_message)
                    if 'cluster' in sys.modules:
                        cluster.progress(format_3col_error_message)
            if len(x) < 5 and not is_at_least_one_3_col:
                is_at_least_one_3_col = True

        if len(valid_lines) == 0:
            error_message = "Warning: checkContactMapFormat: None of the ContactMap Lines are written in proper format - " \
                            "contact map is not scored " \
                            "(should have at least 3 cols : <receptor_res_index> <ligand_res_index> [<dist>|<proba>])"
            return '', False
        valid_cmap_output = "\n".join(valid_lines)

        if not is_at_least_one_3_col:  # None of the contacts were found with less than 5 columns
            is_redundancy_group_added = True
            print(
                "> Coevolution Contact Map contains 5 columns with the 2 last columns interpreted as defining groups of redundant contacts.")
        else:
            is_redundancy_group_added = False

        return valid_cmap_output, is_redundancy_group_added

    def append_redundancy_group(self, cmap_output, range_index_redundancy_group, is_value_proba):
        """
        Change the file only if the contact map is 3-columns.
        Grouping by equivalent contacts can be done with respect to the amplitude of  redundancy_grouping value
        @param cmap_output :
        @param range_index_redundancy_group :
        @param is_value_proba :

        @return:
        """
        list_contacts = []
        dic_groups = {}
        newlines = []
        coupling_group = 0
        # For every residue index resi, residues in the index range [resi-AMPLITUDE,resi+AMPLITUDE]  will be assigned the best value for the contact
        AMPLITUDE = range_index_redundancy_group
        for ll in cmap_output.split('\n'):
            x = ll.split()
            list_contacts.append([eval(x[2]), int(x[0]), int(x[1])])

        if is_value_proba:
            # typical case is when the 3rd column of the contact map file contains coupling intensity or contact probability (higher-> better)
            list_contacts.sort(reverse=True, key=itemgetter(0))
        else:
            # typical case is when the 3rd column of the contact map file contains distances (lower-> better)
            list_contacts.sort(key=itemgetter(0))

        for ii, sp in enumerate(list_contacts):
            # sp = ll.split()
            idxAi = sp[1]
            idxBj = sp[2]
            value_contact = sp[0]
            if "%d_%d" % (idxAi, idxBj) not in dic_groups:
                coupling_group += 1
                for add_i in range(-AMPLITUDE, AMPLITUDE + 1):
                    for add_j in range(-AMPLITUDE, AMPLITUDE + 1):
                        elt = "%d_%d" % (idxAi + add_i, idxBj + add_j)
                        if elt not in dic_groups:
                            dic_groups[elt] = {}
                            dic_groups[elt]['value'] = value_contact
                            dic_groups[elt]['group_index'] = coupling_group

        for ii, sp in enumerate(list_contacts):
            idxAi = sp[1]
            idxBj = sp[2]
            value_contact = sp[0]
            key_residues = "%d_%d" % (idxAi, idxBj)
            newline = "%d %d %.6f %d %.6f" % (
                idxAi, idxBj, value_contact, dic_groups[key_residues]['group_index'], dic_groups[key_residues]['value'])
            newlines.append(newline)
        cmap_output_with_group = "\n".join(newlines)

        return cmap_output_with_group

    def write_sequence(self, sequence, file):
        """

        @param sequence: list made of a header and a sequence
        @param file:
        @return:
        """
        with (open(file, "w")) as fout:
            for l in sequence:
                fout.write(l)
        fout.close()

    def write_cmap(self, cmap, file):
        """
        @param sequence: str, lines of contacts
        @param file:
        @return:
        """
        with (open(file, "w")) as fout:
            fout.write(cmap)
        fout.close()

    def read_sequence_from_seq_or_pdb_inputs(self, partner):
        """
        @param partner: Either 'A' or 'B'
        """

        if os.path.isdir("protein_{}".format(partner.lower())):
            # Check if sequence was used as input by analysing protein_a or _b directory.
            # If template based modeling was used to model partner => We will use this sequence as reference for the coevolution map.
            print(
            "Reading the sequence used to derive the coevolution contact map from the sequence contained in protein_{}/protein_{}-sketch-chunk1.fst".format(partner.lower(),partner.lower()))

            first_seq_fasta = 'protein_{}-sketch-chunk1.fst'.format(partner.lower())
            fasta_str = open(os.path.join("protein_{}".format(partner.lower()), first_seq_fasta)).read()
        else:
            print("Reading the sequence used to derive the coevolution contact map from the sequence of the pdb of {}".format(partner))
            fref = open(os.path.join("data_chain{}".format(partner), "chain{}_representative_chain.out".format(partner))).readlines()
            fasref = fref[0].split()[1]
            fasta_str = open(os.path.join("data_chain{}".format(partner), fasref)).read()
        return fasta_str


def usage():
    print("""analyzeContactMap.py
    USAGE : python analyzeContactMap.py
                                 -cmap <file set in the input>
                                 [-r width of window for indexes to be grouped into sets of redundant contacts default=0]

          """)
    sys.exit(1)
    return True


def main():
    # get arguments
    try:
        contact_map_file = os.path.abspath(sys.argv[sys.argv.index('-cmap') + 1])
    except:
        print("\nERROR: Coevolution contact map file missing\n")
        usage()
        sys.exit(-1)
    try:
        range_index_redundancy_group = int(sys.argv[sys.argv.index('-r') + 1])
        print("Applying a grouping of residue indexes neighbours by less than {} residues".format(range_index_redundancy_group))
    except:
        range_index_redundancy_group = 0
    try:
        seqA = os.path.abspath(sys.argv[sys.argv.index('-seqA') + 1])
    except:
        seqA = ''
    try:
        seqB = os.path.abspath(sys.argv[sys.argv.index('-seqB') + 1])
    except:
        seqB = ''
    try:
        sys.argv.index('-dist')
        is_value_proba = False
    except:
        is_value_proba = True

    aCM = analyzeContactMap(contact_map_file=contact_map_file, cmap_output='', sequence_a=seqA, sequence_b=seqB,
                            range_index_redundancy_group=range_index_redundancy_group, is_value_proba=is_value_proba,
                            frodock_cmap_seqA='frodock_cmap_seqA.fasta', frodock_cmap_seqB='frodock_cmap_seqB.fasta',
                            frodock_cmap_coevo='frodock_cmap_coevo.txt')


""" MAIN """

if __name__ == "__main__":
    main()
