#! /usr/bin/env python

import os, sys
import shutil
import numpy as np
import tempfile
import time
import InterEvDock3.consensus
import InterEvDock3.frodock
import optparse
import pkg_resources

# for python version compatibility
try:
    import ConfigParser
except:
    import configparser as ConfigParser

# our own tools
import InterEvDock3.tools as tools
import InterEvDock3.threadingOSCAR as threadingOSCAR
import InterEvDock3.rosettaScoring as rosettaScoring

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))

import PyPDB.PyPDB as PDB

import cluster.cluster as cluster


class FrodockRigidBodyDocking():
    """
    A class to perform the rigid body docking part of the InterEvDock pipeline
    i.e. from input structures and coMSAs, it runs frodock, frodock clustering,
    IES and SPP homology scoring and Rosetta ISC if option set to True, as well 
    as calculating the final top 10 consensus and top 10 residues
    """

    def __init__(self, protein_a='', protein_b='', alignment_a='', alignment_b='', workdir='', constraints={}, d_coevol_cmap={}, nodes=1, cpus=1, runISC=False, dockdat=False, runExplicitHomology=True,
                 frodock_version=2.1, frodock_type_option='O', topN=10000, noscoring=False, dthresh_contact=8.0):
        """
        ### INPUT VARIABLES ###
        protein_a & protein_b : receptor and ligand proteins (should be robust to multimeric inputs except for threading script with OSCAR)
        alignment_a  & alignment_b : receptor and ligand coMSAs (should be reduced to max 40 prior to this script if running runExplicitHomology)
        dockdat : previous dock.dat file if starting from another job
        workdir : folder in which all output will be generated
        runISC : bool, run ISC ?
        runExplicitHomology : bool, run Explicit Homology Scoring pipeline ?
        nodes & cpus : number of nodes parallel processes wil be run on and number of cpus per node
        constraints : str or dict, to apply AND contraints. If str: name of the constraint file in frodock-compatible format. If dict: {'receptor_constraints':[(ch,resnum,restype,dist)], 'ligand_constraints':[(ch,resnum,restype,dist)], 'pairwise_constraints':[(ch,resnum,restype,ch,resnum,restype,dist)]} 
        d_coevol_cmap :dic containing the path of the files sequence and cmap
        frodock_version : version of frodock for sampling (2.1 or 3.1)
        frodock_type_option : str, frodock type (A: Antibody/Antigen, E: Enzyme/Ligand, O: Other) [default=O]
        topN : maximum number of decoys to score
        noscoring : bool, stop after sampling    
        dthresh_contact : int, contact map threshold to use in coevol map scoring

        ### OTHER VARIABLES DEFINED THROUGHOUT THIS CLASS ###
        dockdat : frodock docking output without constraints or clustering
        dockclustdat : frodock docking output after clustering
        dockcstdat : frodock docking output after applying constraints
        cstfile : constraints for frodock
        
        full_path_basename_models_partner_a : basenames for homologs of partner A
        full_path_basename_models_partner_b : basenames for homologs of partner B
        homolog_list_partner_a : file with names of homologs of partner A
        homolog_list_partner_b : file with names of homologs of partner B

        FDscores : file with decoy scores for FRODOCK
        IESscores : file with decoy scores for homology or query InterEvScore
        SPPscores : file with decoy scores for homology or query SOAP-PP 
        ISCscores : file with decoy scores for query Rosetta Interface Score
        ISChscores : file with decoy scores for homology Rosetta Interface Score
        
        top50FD : list of top 50 decoy numbers (as in frodock i.e. numbering starts with 1) for FRODOCK
        top50IES : list of top 50 decoy numbers (as in frodock i.e. numbering starts with 1) for homology or query InterEvScore
        top50SPP : list of top 50 decoy numbers (as in frodock i.e. numbering starts with 1) for homology or query SOAP-PP 
        top50ISC : list of top 50 decoy numbers (as in frodock i.e. numbering starts with 1) for query Rosetta Interface Score
        top50ISCh : list of top 50 decoy numbers (as in frodock i.e. numbering starts with 1) for homology Rosetta Interface Score
        top150 : list of 3xtop50 decoy numbers of FD, IES and SPP for rosetta
        
        top50FDdir : path to folder with top 50 decoys for FRODOCK 
        top50IESdir : path to folder with top 50 decoys for homology or query InterEvScore
        top50SPPdir : path to folder with top 50 decoys for homology or query SOAP-PP 
        top50ISCdir : path to folder with top 50 decoys for query Rosetta Interface Score
        top50ISChdir : path to folder with top 50 decoys for homology Rosetta Interface Score

        top10cons_struct_html : file with top 10 consensus structures
        top10cons_struct_txt : file with top 10 consensus structures
        top10cons_res_html : file with top 10 consensus residues
        top10cons_res_txt : file with top 10 consensus residues

        """
        # set all the variables
        self.workdir = os.path.abspath(workdir)        # str, working directory
        self.protein_a = os.path.abspath(protein_a)
        self.protein_b = os.path.abspath(protein_b)
        self.noScoring = noscoring
        self.dthresh_contact = dthresh_contact

        self.CMAPnbscores = "scores_CMAPnb.txt"
        self.CMAPscoscores = "scores_CMAPscore.txt"
        self.FDscores = "scores_FRODOCK.txt"
        self.IESscores = "scores_IES.txt"
        self.SPPscores = "scores_SPP.txt"
        self.IEShscores = "scores_IESh.txt"
        self.SPPhscores = "scores_SPPh.txt"
        self.ISCscores = "scores_ISC.txt"
        self.ISChscores = "scores_ISCh.txt"

        self.top10CMAPnb = list(range(1,11)) # top decoys are already sorted
        self.top10CMAPsco = list(range(1,11))
        self.top50FD = list(range(1,51))
        self.top50IES = [] 
        self.top50SPP = []
        self.top50IESh = [] 
        self.top50SPPh = []
        self.top50ISC = [] 
        self.top50ISCh = []

        self.top10CMAPnbdir = os.path.abspath("CMAPnb_models/")
        self.top10CMAPscodir = os.path.abspath("CMAPscore_models/")
        self.top50FDdir = os.path.abspath("frodock_models/")
        self.top50IESdir = os.path.abspath("IES_models/")
        self.top50SPPdir = os.path.abspath("SPP_models/")
        self.top50IEShdir = os.path.abspath("IESh_models/")
        self.top50SPPhdir = os.path.abspath("SPPh_models/")
        self.top50ISCdir = os.path.abspath("ISC_models/")
        self.top50ISChdir = os.path.abspath("ISCh_models/")

        self.top10CMAPnbnames = "Complex_CMAPnb{}.pdb"
        self.top10CMAPsconames = "Complex_CMAPscore{}.pdb"
        self.top50FDnames = "Complex_FRODOCK{}.pdb"
        self.top50IESnames = "Complex_IES{}.pdb"
        self.top50SPPnames = "Complex_SPP{}.pdb"
        self.top50IEShnames = "Complex_IESh{}.pdb"
        self.top50SPPhnames = "Complex_SPPh{}.pdb"
        self.top50ISCnames = "Complex_ISC{}.pdb"
        self.top50ISChnames = "Complex_ISCh{}.pdb"
        self.top150 = [] # 3xtop50 of FD, IES and SPP for rosetta

        self.full_path_basename_models_partner_a = 'oscar_homologs_partner_a/homolog_partner_a' # basenames for homologs of partner A
        self.full_path_basename_models_partner_b = 'oscar_homologs_partner_b/homolog_partner_b' # basenames for homologs of partner B
        self.homolog_list_partner_a = self.full_path_basename_models_partner_a+'_list.txt' # file with names of homologs of partner A
        self.homolog_list_partner_b = self.full_path_basename_models_partner_b+'_list.txt' # file with names of homologs of partner B

        self.top10cons_output_basename = os.path.abspath("InterEvDock3")
        self.top5cons_output_basename = os.path.abspath("consensus")
        self.top10cons_struct_html = self.top10cons_output_basename+"_Top10Consensus.html" # not generated anymore because not used
        self.top10cons_struct_txt = self.top10cons_output_basename+"_Top10Consensus.txt"
        self.top10cons_res_html = self.top5cons_output_basename+"_top5_residues.html" # still generated but not used
        self.top10cons_res_txt = self.top5cons_output_basename+"_top5_residues.txt"


        # if MSA no given, we just use the input sequence
        if (not alignment_a or alignment_a==None) and not self.noScoring:
            alignment_a = os.path.join(self.workdir,os.path.basename(self.protein_a))+".fasta"
            p = PDB.PDB(self.protein_a)
            f=open(alignment_a,"w")
            f.write(">protein_a\n"+p.aaseq()+'\n')
            f.close()
        if (not alignment_b or alignment_b==None) and not self.noScoring:
            alignment_b = os.path.join(self.workdir,os.path.basename(self.protein_b))+".fasta"
            p = PDB.PDB(self.protein_b)
            f=open(alignment_b,"w")
            f.write(">protein_b\n"+p.aaseq()+'\n')
            f.close()

        if not self.noScoring:
            self.alignment_a = os.path.abspath(alignment_a)
            self.alignment_b = os.path.abspath(alignment_b)
        self.topN = int(topN)
        if dockdat and os.path.exists(dockdat):
            dockdat = os.path.abspath(dockdat)
        self.runISC = runISC                           # bool, run Rosetta ISC or not?
        self._nodes = nodes                            # int, number of nodes for parallel tasks
        self._cpus = cpus                              # int, number of cpus per node
        self.constraints = constraints                 # file or dict, {'receptor_constraints':[(ch,resnum,restype,dist)], 'ligand_constraints':[(ch,resnum,restype,dist)], 'pairwise_constraints':[(ch,resnum,restype,ch,resnum,restype,dist)]}        
        self.d_coevol_map = d_coevol_cmap              # dict with the path of the file required to run frodock3v2
        if self.d_coevol_map:
            self.cmap = True
        else:
            self.cmap = False
        self.runExplicitHomology = runExplicitHomology # bool, run Explicit Homolog scoring or not?
        self.frodock_version = frodock_version         # frodock version used for sampling : default is 2.1 but scoring will be performed with frodock3.1
        self.frodock_type_option = frodock_type_option # str, frodock type (A: Antibody/Antigen, E: Enzyme/Ligand, O: Other) [default=O]

        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir)
        curdir= os.getcwd()
        os.chdir(self.workdir)

        ############# DOCKING
        self.dockdat = os.path.join(self.workdir,'dock.dat')           # frodock docking output without constraints or clustering
        self.dockclustdat = os.path.join(self.workdir,'dockclust.dat') # frodock docking output after clustering
        self.dockcstdat = os.path.join(self.workdir,'dockcst.dat')     # frodock docking output after applying constraints
        self.cstfile = os.path.join(self.workdir,'cst.txt')            # constraints for frodock

        # For coevo map we run scoring before clustering. (May be we should improve this but for now we do it that way...)
        # After scoring, clustering will be done twice, on the CMAPnb and on the CMAPsco...
        # With clustering the decoys are ranked so we can get the top10 best.
        # Alternatively, (faster) May be we could rather cluster and only after run the frodock_sco on the clustered file and sort the solution in the same way as we do for SPP &co
        # Can be implemented later that way
        self.dockcmapdat = os.path.join(self.workdir, 'dockcmap')  # frodock docking rootname output after applying cmap scoring
        self.dockclustdat_cmapnb = os.path.splitext(self.dockclustdat)[0] + '_CMAPnb.dat'
        self.dockclustdat_cmapsco = os.path.splitext(self.dockclustdat)[0] + '_CMAPsco.dat'

        # RUN THE SAMPLING STEP OF DOCKING !!
        self.run_docking(dockdat)

        if self.noScoring:
            return

        ### SPECIAL OUTPUT TREATMENT IN CASE WE CONSIDER COEVOLUTION-BASED CONTACT MAPS
        if d_coevol_cmap:
            self.cmap = True

            self.generate_top10_decoys(self.dockclustdat_cmapnb, self.top10CMAPnb, self.top10CMAPnbdir, decoyname=self.top10CMAPnbnames)
            self.generate_top10_decoys(self.dockclustdat_cmapsco, self.top10CMAPsco, self.top10CMAPscodir, decoyname=self.top10CMAPsconames)

            oFro = frodock.Frodock(workdir=self.workdir, receptor=self.protein_a, ligand=self.protein_b, nodes=self._nodes, cpus=self._cpus)
            scoresCMAPnb = oFro.read_frodock_output(self.dockclustdat_cmapnb, version=3.1, output=self.CMAPnbscores)
            scoresCMAPsco = oFro.read_frodock_output(self.dockclustdat_cmapsco, version=3.1, output=self.CMAPscoscores)


            cluster.progress("Finished docking")
            print("Finished docking")

            os.chdir(curdir)
            
            return

        ############# GENERATING HOMOLOGS FOR SCORING
        cluster.progress("Scoring decoys...")
        print("Scoring decoys...")

        if self.runExplicitHomology:
            cluster.progress(" ... generating homolog models")
            print(" ... generating homolog models")
            self.run_threading(self.alignment_a,self.protein_a,self.full_path_basename_models_partner_a,self.homolog_list_partner_a)
            self.run_threading(self.alignment_b,self.protein_b,self.full_path_basename_models_partner_b,self.homolog_list_partner_b)

        ############# SCORING WITH IES AND SPP

        if self.runExplicitHomology:
            self.run_explicit_homology_scoring()
            if self.runISC:
                self.run_ISC_scoring()
        else:
            self.run_traditional_scoring() # "traditional" InterEvDock but with distance version IES and Frodock's SPP and priority order: SPP, then IES, then FD
            if self.runISC:
                self.run_ISC_scoring()

        ############# GENERATE TOP 50 DECOYS

        self.generate_top50_decoys(self.top50FD,self.top50FDdir,decoyname=self.top50FDnames)
        if self.runExplicitHomology:
            self.generate_top50_decoys(self.top50IESh,self.top50IEShdir,decoyname=self.top50IEShnames)
            self.generate_top50_decoys(self.top50SPPh,self.top50SPPhdir,decoyname=self.top50SPPhnames)
        else:
            self.generate_top50_decoys(self.top50IES,self.top50IESdir,decoyname=self.top50IESnames)
            self.generate_top50_decoys(self.top50SPP,self.top50SPPdir,decoyname=self.top50SPPnames)
        if self.runISC:
            print(self.top50ISC)
            self.generate_top50_decoys(self.top50ISC,self.top50ISCdir,decoyname=self.top50ISCnames)
            if self.runExplicitHomology:
                print(self.top50ISCh)
                self.generate_top50_decoys(self.top50ISCh,self.top50ISChdir,decoyname=self.top50ISChnames)

        ############# CALCULATE CONSENSUS

        self.run_consensus_scoring()
        cluster.progress("Finished docking")
        print("Finished docking")

        os.chdir(curdir)


    def run_docking(self, dockdat):
        """
        run frodockgrid and frodock if dat file not already given, frodockonstraints if constraints and frodockcluster
        """
        oFro = frodock.Frodock(workdir=self.workdir, receptor=self.protein_a, ligand=self.protein_b, nodes=self._nodes, cpus=self._cpus)

        # SPECIAL CASE OF DOCKING WITH COEVOLUTION MAPS
        # If we need to score coevolution maps, we switch to both docking and scoring using version 2 of frodock3
        # Otherwise, frodock docking step might be upset by the format of symmetric assemblies inputs...
        if self.cmap:
            self.frodock_version = 3.2

        if dockdat:
            # If the dockdat was generated with a frodock2.1 version, we cannot use it for scoring coevolution maps anymore...
            # -> No hot start from a standard run to a coevo map one (to be added in the help section)
            # TODO: Add a checker which analyses with which version the dock.dat was generated. Adapt the options subsequently for coevomap scoring
            cluster.progress("Starting from previous docking output...")
            print("Starting from previous docking output...")
            if os.path.abspath(dockdat) != os.path.abspath(self.dockdat):
                shutil.copy(dockdat,self.dockdat)
        else:
            # run frodockgrid and frodock 
            cluster.progress("Performing the sampling...")
            print("Performing the sampling...")
            oFro.run_frodock_sampling(self.dockdat, version=self.frodock_version, type_option=self.frodock_type_option)

        if self.constraints:
            if isinstance(self.constraints,str) and os.path.exists(self.constraints):
                # constraint file already formated for frodock
                self.cstfile = self.constraints
            elif isinstance(self.constraints,dict):
                # dictionary given as input, has to be converted into a txt output
                oFro.write_frodock_constraints(self.cstfile, **self.constraints)
            else:
                # format not recognised
                print("Warning: input constraints format not recognised, proceeding without constraints...")
                oFro.run_frodock_clustering(self.dockdat, self.dockclustdat, version=self.frodock_version)
                return
            oFro.run_frodock_constraints(self.dockdat, self.dockcstdat, self.cstfile, version=self.frodock_version)
            oFro.run_frodock_clustering(self.dockcstdat, self.dockclustdat, version=self.frodock_version)

        elif self.cmap:
            cluster.progress("... using user co-evolution constraints")
            print("... using user co-evolution constraints")

            # The input dat is in 2.1 or 3.1 frodock format. But with cmap the output is necessarily in frodock3 format
            # Hence we have to switch version between scoring and clustering to ensure clustering is running on the proper format
            # TODO: [RG] dthresh_contact is set as a fixed value so far. Add it as an optional tunable parameter from the webpage
            oFro.run_frodock_coevolmap_scoring(self.dockdat, self.dockcmapdat, self.d_coevol_map, version=self.frodock_version, dthresh_contact=self.dthresh_contact, extra_args=['-r 1-'+str(self.topN)])
            # Output should be dockcmap_CMAPnb.dat and dockcmap_CMAPsco.dat

            self.dockcmapdat_cmapnb = self.dockcmapdat+"_CMAPnb.dat"
            self.dockcmapdat_cmapsco = self.dockcmapdat+"_CMAPsco.dat"
            self.dockcmapdat_SPPsco = self.dockcmapdat + "_SPPsco.dat"

            # We choose to cluster only on the number of respected restraints

            oFro.run_frodock_clustering_jobarray([self.dockcmapdat_cmapnb,self.dockcmapdat_cmapsco], [self.dockclustdat_cmapnb,self.dockclustdat_cmapsco] , version=self.frodock_version)
            # Output should be dockclust_CMAPnb.dat and dockclust_CMAPsco.dat
            # test with job_array we can omit oFro.run_frodock_clustering(self.dockcmapdat_cmapsco, self.dockclustdat_cmapsco, version=self.frodock_version)
            # Output should be dockclust_CMAPsco.dat
            # check if there are still decoys left after clustering
            num_decoys = oFro.check_size(self.dockclustdat_cmapnb, version=self.frodock_version)
            if num_decoys == 0:
                cluster.progress("No decoys after FRODOCK sampling and clustering, stopping the run.")
                raise Exception('No decoys after FRODOCK sampling and clustering in dockclust_CMAPnb.dat')
            else:
                cluster.progress("... %i decoys left after FRODOCK sampling and clustering"%num_decoys)
            return

        else:
            oFro.run_frodock_clustering(self.dockdat, self.dockclustdat, version=self.frodock_version)
            
        # check if there are still decoys left after clustering
        num_decoys = oFro.check_size(self.dockclustdat, version=self.frodock_version)
        if num_decoys == 0:
            cluster.progress("No decoys after FRODOCK sampling and clustering, stopping the run.")
            raise Exception('No decoys after FRODOCK sampling and clustering for %s and %s' %(msa,pdb))
        else:
            cluster.progress("... %i decoys left after FRODOCK sampling and clustering"%num_decoys)

    def run_threading(self,msa,pdb,output,homolog_file_list):
        """
        Run homology modelling of homologs in MSA of a protein
        Inputs:
        msa: fasta multiple sequence alignment
        pdb: protein used as template for threading
        output: full path output basename e.g. homolog_pdbs/homolog_chainA -> outputs will be indexed _1_SC.pdb, _2_SC.pdb etc. according to order in the MSA
        homolog_file_list: full path to the file that will contain a list of the homologs that are 
        """
        oThread = threadingOSCAR.runOSCAR(output=output,realign=True,verbose=False,parallelise=True)
        oThread.load_inputs(in_alignment=[msa],in_template=[pdb])
        oThread.run(output_homologs=homolog_file_list)
        if not os.path.exists(os.path.dirname(output)) or not os.path.exists(output+"_1_SC.pdb"): # there is always at least 1 homolog i.e. the query sequence
            cluster.progress('Error: No threaded models generated for scoring.')
            raise Exception('OSCAR threading failed for %s and %s' %(msa,pdb))


    def run_traditional_scoring(self):
        """
        Run IES (23bBE with MSAs at residue level) and SPP on query models only --> InterEvDock2 version
        Outputs 3 text files with IES, SPP and Frodock scores
        """
        cluster.progress(" ... scoring with InterEvScore and SOAP-PP")
        print(" ... scoring with InterEvScore and SOAP-PP")
        oFro = frodock.Frodock(workdir=self.workdir, receptor=self.protein_a, ligand=self.protein_b, nodes=self._nodes, cpus=self._cpus)
        oFro.run_frodock_scoring(self.dockclustdat, os.path.splitext(self.dockclustdat)[0], alig_rec=self.alignment_a, alig_lig=self.alignment_b, version=self.frodock_version, extra_args=['-r 1-{}'.format(self.topN)])

        scoresIES = oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+'_IES.dat',output=self.IESscores, version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
        scoresSPP = oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+'_SPP.dat',output=self.SPPscores, version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
        _ = oFro.read_frodock_output(self.dockclustdat, output=self.FDscores, version=self.frodock_version, topN=self.topN)
        try:
            os.remove(os.path.splitext(self.dockclustdat)[0]+'_SPP.dat')
            os.remove(os.path.splitext(self.dockclustdat)[0]+'_IES.dat')
            os.remove(os.path.splitext(self.dockclustdat)[0]+'_Tobi.dat')
            os.remove(os.path.splitext(self.dockclustdat)[0]+'_fnat.dat')
        except:
            pass

        self.top50IES = np.argsort(scoresIES)[::-1][:50]+1
        self.top50SPP = np.argsort(scoresSPP)[::-1][:50]+1 # frodock_sco 3.1 repo also outputs spp in reverse order -> the higher the better
        self.top50FD = np.arange(1,min(51,len(self.top50IES)+1),dtype=int)


    def run_explicit_homology_scoring(self):
        """
        Run IES (2bB) and SPP on query and homolog models --> explicit evolution scoring version
        """
        def write_scoring_output(scores,output):
            f=open(output,"w")
            f.write("#decoys scores\n")
            f.write("\n".join(['complex.{}.pdb {:.4f}'.format(i+1,s) for i,s in enumerate(scores)])+"\n")
            f.close()

        # get list of homologs
        f=open(self.homolog_list_partner_a)
        homologs_partner_a = [l.strip() for l in f if l.strip() and not l.startswith("#")]
        f.close()
        f=open(self.homolog_list_partner_b)
        homologs_partner_b = [l.strip() for l in f if l.strip() and not l.startswith("#")]
        f.close()

        cluster.progress(" ... scoring query and homolog models with InterEvScore and SOAP-PP")
        print(" ... scoring query and homolog models with InterEvScore and SOAP-PP")
        # query scoring
        oFro = frodock.Frodock(workdir=self.workdir)

        lst_rec = [self.protein_a]+homologs_partner_a
        lst_lig = [self.protein_b]+homologs_partner_b
        oFro.run_frodock_scoring_jobarray(self.dockclustdat, os.path.splitext(self.dockclustdat)[0], receptor=lst_rec, ligand=lst_lig, alig_rec=self.alignment_a, alig_lig=self.alignment_b, version=self.frodock_version, extra_args=['-r 1-{}'.format(self.topN), '--scoreIES 2bb'],jobarray=True)

        total_scores_IES = []
        total_scores_SPP = []
        for i in range(len(lst_rec)):
            if i == 0:
                total_scores_IES = oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_IES.dat', version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
                total_scores_SPP = oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_SPP.dat', version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
            else:
                total_scores_IES += oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_IES.dat', version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
                total_scores_SPP += oFro.read_frodock_output(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_SPP.dat', version=3.1, topN=self.topN)['score'] # output version of frodock_sco is always 3.1
            try:
                os.remove(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_IES.dat')
                os.remove(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_Tobi.dat')
                os.remove(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_fnat.dat')
                os.remove(os.path.splitext(self.dockclustdat)[0]+"_"+str(i+1)+'_SPP.dat')
            except:
                pass

        # write scoring output files
        write_scoring_output(total_scores_IES/(len(homologs_partner_a)+1),self.IEShscores) # IES
        write_scoring_output(total_scores_SPP/(len(homologs_partner_a)+1),self.SPPhscores) # SPP
        _ = oFro.read_frodock_output(self.dockclustdat,output=self.FDscores, version=self.frodock_version, topN=self.topN) # FD

        self.top50IESh = np.argsort(total_scores_IES)[::-1][:50]+1
        self.top50SPPh = np.argsort(total_scores_SPP)[::-1][:50]+1 # frodock_sco 3.1 repo also outputs spp in reverse order -> the higher the better
        self.top50FD = np.arange(1,min(51,len(self.top50IESh)+1), dtype=int)


    def run_ISC_scoring(self):
        """
        Genereate top 150 decoys for query and homologs and score with Rosetta
        """
        def write_scoring_output(decoys,scores,output):
            f=open(output,"w")
            f.write("#decoys scores\n")
            f.write("\n".join(['complex.{}.pdb {:.4f}'.format(i,s) for i,s in zip(decoys,scores)])+"\n")
            f.close()

        if self.runExplicitHomology:
            top150_decoys = sorted(list(set(self.top50FD).union(self.top50IESh).union(self.top50SPPh)))
        else:
            top150_decoys = sorted(list(set(self.top50FD).union(self.top50IES).union(self.top50SPP)))

        cluster.progress(" ... running rosetta ISC scoring")
        print(" ... running rosetta ISC scoring")
        outdir = tools.make_tempdir(prefix_str='decoys_isc',parent_dir=self.workdir)
        oFro = frodock.Frodock(workdir=self.workdir)
        decoy_names = ['complex.{}.query.pdb'.format(d) for d in top150_decoys]

        # rosetta needs pdbs without ligands or hetatm to avoid bugs
        tmp_prot_a_clean = tools.make_tempfile(parent_dir=outdir,prefix_str='protein_a',suffix_str='.pdb')
        p = PDB.PDB(self.protein_a,hetSkip=1)
        p.out(tmp_prot_a_clean)
        tmp_prot_b_clean = tools.make_tempfile(parent_dir=outdir,prefix_str='protein_b',suffix_str='.pdb')
        p = PDB.PDB(self.protein_b,hetSkip=1)
        p.out(tmp_prot_b_clean)
        oFro.create_frodock_decoys(self.dockclustdat, outdir, receptor=tmp_prot_a_clean, ligand=tmp_prot_b_clean, version=self.frodock_version, decoys=top150_decoys, decoy_names=decoy_names)
        all_decoy_names = [os.path.join(self.workdir,outdir,'complex.{}.query.pdb'.format(d)) for d in top150_decoys]

        num_homologs = 1
        if self.runExplicitHomology:
            # get list of homologs
            f=open(self.homolog_list_partner_a)
            homologs_partner_a = [l.strip() for l in f if l.strip() and not l.startswith("#")]
            f.close()
            f=open(self.homolog_list_partner_b)
            homologs_partner_b = [l.strip() for l in f if l.strip() and not l.startswith("#")]
            f.close()

            num_seq = len(homologs_partner_a)
            divisor = min(num_seq,10)
            q = num_seq//divisor
            r = num_seq%divisor
            idx_eq10 = list(range(0, (q+1)*r, q+1)) + list(range((q+1)*r,num_seq+1,q))
            homologs_partner_a = [homologs_partner_a[i-1] for i in idx_eq10[1:]]
            homologs_partner_b = [homologs_partner_b[i-1] for i in idx_eq10[1:]]

            for homologA, homologB in zip(homologs_partner_a,homologs_partner_b):
                decoy_names = ['complex.{}.{}.pdb'.format(d,os.path.splitext(os.path.basename(homologA))[0]) for d in top150_decoys]
                oFro.create_frodock_decoys(self.dockclustdat, outdir, receptor=homologA, ligand=homologB, version=self.frodock_version, decoys=top150_decoys, decoy_names=decoy_names)
                all_decoy_names += [os.path.join(self.workdir,outdir,'complex.{}.{}.pdb'.format(d,os.path.splitext(os.path.basename(homologA))[0])) for d in top150_decoys]
            
            num_homologs = len(homologs_partner_a)+1

        oRosetta = rosettaScoring.RosettaISC(list_pdb=all_decoy_names,output="",parallelise=self._nodes*self._cpus,debug=False)

        ISCscores = oRosetta.final_scores

        # combine homolog scores
        d_ISCh10 = {} # [decoy num] -> ISCh10 score
        d_ISCq = {} # [decoy num] -> ISCq score
        ISCscores['I_sc'][np.where(ISCscores['I_sc']==float('inf'))] = np.max(ISCscores['I_sc'][np.where(ISCscores['I_sc']!=float('inf'))])
        for decoy_num in top150_decoys:
            d_ISCh10[decoy_num]=0.
            for line in range(len(ISCscores)):
                try:
                    name = os.path.basename(ISCscores['description'][line]).decode("utf-8") # might be a byte instead of a string
                except:
                    name = os.path.basename(ISCscores['description'][line])
                if name.startswith("complex.{}.".format(decoy_num)):
                    d_ISCh10[decoy_num]+=ISCscores['I_sc'][line]/float(num_homologs)
                if name.startswith("complex.{}.query".format(decoy_num)):
                    d_ISCq[decoy_num]=ISCscores['I_sc'][line]

        # write scores to file and get top 50 decoys
        if self.runExplicitHomology:
            write_scoring_output(top150_decoys,[d_ISCh10[d] for d in top150_decoys],self.ISChscores)
            self.top50ISCh = sorted(d_ISCq,key= lambda k: d_ISCq[k])[:50] 
        write_scoring_output(top150_decoys,[d_ISCq[d] for d in top150_decoys],self.ISCscores)
        self.top50ISC = sorted(d_ISCh10,key= lambda k: d_ISCh10[k])[:50]

        try:
            shutil.rmtree(outdir)
        except:
            # OSError: [Errno 16] Device or resource busy: '.nfs0000000093fbc9450000163f'
            time.sleep(5)
            try:
                # try again and if not, leave dir... .nfsXXX files are tmpfiles created when reading files that are on a different disk to the execution one
                # they stay there until the process ends -> might be linked to reading the ISC npz output...
                shutil.rmtree(outdir)
            except Exception as e:
                print(e)
                pass


    def generate_top50_decoys(self,top50_decoys,outdir,decoyname="Complex_{}.pdb"):
        """
        Generate the traditional top 50 decoys directory for a given score
        """
        print("Generating top 50 (max) decoys in {}".format(os.path.basename(outdir)))
        oFro = frodock.Frodock(workdir=self.workdir, receptor=self.protein_a, ligand=self.protein_b)
        oFro.create_frodock_decoys(self.dockclustdat, outdir, version=self.frodock_version, decoys=top50_decoys, decoy_names=[decoyname.format(d) for d in range(1,min(len(top50_decoys)+1,51))])

    def generate_top10_decoys(self,dockclustdat,top10_decoys,outdir,decoyname="Complex_{}.pdb"):
        """
        Generate the traditional top 10 decoys directory for a given score
        """
        print("Generating top 10 (max) decoys in {}".format(os.path.basename(outdir)))
        oFro = frodock.Frodock(workdir=self.workdir, receptor=self.protein_a, ligand=self.protein_b)
        oFro.create_frodock_decoys_for_coevomap(dockclustdat, outdir, version=self.frodock_version, decoys=top10_decoys, decoy_names=[decoyname.format(d) for d in range(1,11)])

    def run_consensus_scoring(self):
        """
        Get top 10 consensus structures and 2*top5 consensus residues
        """
        def write_pdb_list(output,decoyname,top50):
            f=open(output,"w")
            f.write("\n".join([decoyname.format(i+1)+" "+str(d) for i,d in enumerate(top50)])+'\n')
            f.close()

        cluster.progress(" ... calculating consensus")
        print(" ... calculating consensus")
        # get chain names
        p = PDB.PDB(self.protein_a)
        chainsA = p.chnList()
        p = PDB.PDB(self.protein_b)
        chainsB = p.chnList()

        _, output = tempfile.mkstemp(prefix='top50decoys_')
        os.remove(output)

        # generate input list
        if self.runExplicitHomology:
            write_pdb_list(output+'_SPP.txt',os.path.join(self.top50SPPhdir,self.top50SPPhnames),self.top50SPPh)
            write_pdb_list(output+'_IES.txt',os.path.join(self.top50IEShdir,self.top50IEShnames),self.top50IESh)
        else:
            write_pdb_list(output+'_SPP.txt',os.path.join(self.top50SPPdir,self.top50SPPnames),self.top50SPP)
            write_pdb_list(output+'_IES.txt',os.path.join(self.top50IESdir,self.top50IESnames),self.top50IES)
        write_pdb_list(output+'_FD.txt',os.path.join(self.top50FDdir,self.top50FDnames),self.top50FD)
        if self.runISC:
            write_pdb_list(output+'_ISCq.txt',os.path.join(self.top50ISCdir,self.top50ISCnames),self.top50ISC)
            if self.runExplicitHomology:
                write_pdb_list(output+'_ISCh.txt',os.path.join(self.top50ISChdir,self.top50ISChnames),self.top50ISCh)
                top50files = [output+'_ISCh.txt',output+'_SPP.txt',output+'_IES.txt',output+'_ISCq.txt',output+'_FD.txt']
            else:
                top50files = [output+'_SPP.txt',output+'_IES.txt',output+'_ISCq.txt',output+'_FD.txt']
        else:
            top50files = [output+'_SPP.txt',output+'_IES.txt',output+'_FD.txt']

        # get structural consensus
        oCons_Struct = consensus.GetConsensus(top50files,self.top10cons_output_basename,chainsB)

        # get residue consensus
        oCons_Res = consensus.GetResInterface(top50files,self.top5cons_output_basename,chainsA,chainsB)



if __name__=="__main__":

    USAGE = "A generic pipeline for docking 2 PDB files, each of which contains only one chain\n"+\
            "this_script.py -R protein_a.pdb -L protein_b.pdb -d workdir\n"+\
            "or this_script.py -R protein_a.pdb -d workdir for homodimeric docking\n"+\
            "\nUse option -h or --help for more usage information\n"
    
    parser = optparse.OptionParser(usage = USAGE)
    
    parser.add_option('-R','--protein_a_pdb',
                      action="store",
                      dest='protein_a_pdb',
                      type='string',
                      help="Protein A PDB file path")
   
    parser.add_option('-L','--protein_b_pdb',
                      action="store",
                      dest='protein_b_pdb',
                      type='string',
                      help="Protein B PDB file path. None by default meaning the program predicts homodimeric interface of protein A PDB",
                      default=None)
    
    parser.add_option('-D','--nr_decoys',
                      action="store",
                      dest='nr_decoys',
                      type='int', 
                      help="Number of decoys to generate for single run",
                      default = 10000)

    parser.add_option('-n','--nodes',
                      action="store",
                      dest='nodes',
                      type='int', 
                      help="Number of nodes for parallisation (default: %default)",
                      default = 1)

    parser.add_option('-p','--cpus_per_node',
                      action="store",
                      dest='cpus',
                      type='int', 
                      help="Number of cpus per node (default: %default)",
                      default = 1)

    parser.add_option('--version',
                      action="store",
                      dest='frodock_version',
                      type='float', 
                      help="Frodock version for sampling (default: %default)",
                      default = 2.1)

    parser.add_option('-d','--dir',
                      action="store",
                      dest='workdir',
                      type='string',
                      help="Docking work directory",
                      default=os.getcwd())
    
    parser.add_option('-r','--protein_a_msa',
                      action="store",
                      dest='protein_a_msa',
                      help="Custom protein A MSA for IES analysis.",
                      default=None)
    
    parser.add_option('-l','--protein_b_msa',
                      action="store",
                      dest='protein_b_msa',
                      help="Custom protein B MSA for IES analysis.",
                      default=None)
    
    parser.add_option('-C', '--constraint_file',
                      action="store",
                      dest='constraint_file',
                      help="File containing constraints. Each line is a constraint in this format: ch1.resnum1.restype1:ch2.resnum2.restype2:dist. e.g. A.75.THR:B.231.SER:8 or A.75.THR::8 etc.", 
                      default=None)

    parser.add_option('--dock_noconstraint_dat',
                      action="store",
                      dest='dock_noconstraint_dat',
                      help="Docking file output by Frodock (before any constraint filtering or clustering). Used for hotstarting", 
                      default=None)

    parser.add_option('--runISC',
                      action="store_true",
                      dest='runISC',
                      help="Use this option to use Rosetta's ISC scoring")

    parser.add_option('--runExplicit',
                      action="store_true",
                      dest='runExplicit',
                      help="Use this option to use explicit homology scoring")

    parser.add_option('--noScoring',
                      action="store_true",
                      dest='noScoring',
                      help="Use this option to only perform sampling")

    if len(sys.argv) == 1:
        print(USAGE)
        print("Type -h or --help for more help information")
        sys.exit(1)

    (options, args) = parser.parse_args(sys.argv)

    options.workdir = os.path.abspath(options.workdir)
    
    if not options.protein_a_pdb:
        print(USAGE)
        sys.exit(1)

    if not os.path.exists(options.protein_a_pdb):
        print("Error: cannot find pdb input {}".format(options.protein_a_pdb))
        sys.exit(1)

    options.protein_a_pdb = os.path.abspath(options.protein_a_pdb)
    if not options.protein_a_msa or options.protein_a_msa == None:
        print("Warning: no msa input for pdb A")
    elif not os.path.exists(options.protein_a_msa):
        print("Error: cannot find msa input {}".format(options.protein_a_msa))
        sys.exit(1)
    else:
        options.protein_a_msa = os.path.abspath(options.protein_a_msa)     
    
    if options.protein_b_pdb == None:
        options.protein_b_pdb = "_L".join(os.path.splitext(options.protein_a))
        options.protein_b_msa = "_L".join(os.path.splitext(options.protein_a))
        shutil.copy(options.protein_a_pdb,options.protein_b_pdb)
        shutil.copy(options.protein_a_msa,options.protein_b_msa)
        print("Will dock homodimer")
    elif not os.path.exists(options.protein_b_pdb):
        print("Error: cannot find pdb input {}".format(options.protein_b_pdb))
        sys.exit(1)
    else:
        options.protein_b_pdb = os.path.abspath(options.protein_b_pdb)
        if options.protein_b_msa == None:
            print("Warning: no msa input for pdb B")
        elif not os.path.exists(options.protein_b_msa):
            print("Error: cannot find msa input {}".format(options.protein_b_msa))
            sys.exit(1)
        else:
            options.protein_b_msa = os.path.abspath(options.protein_b_msa)     

    if options.dock_noconstraint_dat != None and os.path.exists(options.dock_noconstraint_dat):
        options.dock_noconstraint_dat = os.path.abspath(options.dock_noconstraint_dat)
    else:
        options.dock_noconstraint_dat = None

    constraints = {}
    if options.constraint_file != None and os.path.exists(options.constraint_file):
        constraints={'receptor_constraints':[], 'ligand_constraints':[], 'pairwise_constraints':[]} 
        f=open(options.constraint_file)
        for l in f:
            if l.strip() and not l.startswith("#"):
                try:
                    l=l.split(":")
                    receptor_constraints = l[0].split('.')
                    ligand_constraints = l[1].split('.')
                    distance = l[2]
                    if receptor_constraints and ligand_constraints:
                        if distance:
                            try:
                                distance = float(distance)
                            except:
                                distance = 12.
                        else:
                            distance = 12.
                        constraints['pairwise_constraints'] = [receptor_constraints+ligand_constraints+[distance]]
                    else:
                        if distance:
                            try:
                                distance = float(distance)
                            except:
                                distance = 8.
                        else:
                            distance = 8.
                        if ligand_constraints:
                            constraints['ligand_constraints'] = [ligand_constraints+[distance]]
                        if receptor_constraints:
                            constraints['receptor_constraints'] = [receptor_constraints+[distance]]

                except Exception as e:
                    print(e)
        f.close()


    oFroDocking = FrodockRigidBodyDocking(protein_a=options.protein_a_pdb, protein_b=options.protein_b_pdb, \
                                    alignment_a=options.protein_a_msa, alignment_b=options.protein_b_msa, \
                                    workdir=options.workdir, constraints=constraints, nodes=options.nodes, \
                                    cpus=options.cpus, runISC=options.runISC, dockdat=options.dock_noconstraint_dat, \
                                    runExplicitHomology=options.runExplicit, frodock_version=options.frodock_version, \
                                    topN=options.nr_decoys, noscoring=options.noScoring)
