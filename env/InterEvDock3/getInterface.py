#!/usr/bin/env python
'''
Created on 2016-01-20

@author: Jinchao Yu, Jessica Andreani
@author: adapted by Chloe Quignot in 2018 for distance calculation purposes
'''
import os
import sys
import re
from collections import defaultdict
import pickle as pk

class GetInterface():

    def __init__(self,pdb,threshold,chainsA,chainsB):

        self.lst_contacts = self.find_res_in_contact(pdb, threshold, chainsA, chainsB)

    def get_pdb_file_list_in_folder(self, folder, if_abspath=False):
        """Get the list of file names ending at ".pdb" in the given directory.
        If if_abspath=True, return file absolute paths instead of file names
        """
        pdb_file_list = []
        for filename in os.listdir(folder):
            res_search = re.search("\.pdb$",filename)
            if res_search:
                if if_abspath:
                    file_abspath = os.path.abspath(os.path.join(folder,filename))
                    pdb_file_list.append(file_abspath)
                else:
                    pdb_file_list.append(filename)
        return pdb_file_list

    def if_pdb_atom_line(self, line):
        """if this line (in PDB file) begins by "ATOM  ", this is a valid line for occupancy calculation
        """
        res_match_ATOM = re.match("ATOM  ",line)
        if res_match_ATOM:
            return True
        else:
            return False
        
    def if_pdb_coor_line(self, line):
        """if this line (in PDB file) begins by "ATOM  " or "HETATM", this is a coor line
        """
        res_match_ATOM = re.match("ATOM  ",line)
        res_match_HETATM = re.match("HETATM",line)
        if res_match_ATOM or res_match_HETATM:
            return True
        else:
            return False

             
    def get_chainID_from_pdb_line(self, line_string):
        """get pdb line[21] if this is a correct PDB coor line; if not return None
        """
        try:
            ch = line_string[21]
            if not ch.strip():
                #use segid
                ch = line_string[72:76]
            return ch
        except:
            print("Error: Not valid pdb coor line")
            return None


    def get_resnum_int_from_pdb_line(self, line_string):
        """get pdb line[22:26] if this is a correct PDB coor line; if not return None
        """
        try:
            return int(line_string[22:26])
        except:
            print("Error: Not valid pdb coor line")
            return None

    def get_restype_from_pdb_line(self, line_string):
        """get the one-letter residue type
        the three-letter type is given by pdb line[17:20] if this is a correct PDB coor line; if not return None
        Replaces unknown residue types with a gap.
        """

        one_letter = {}
        one_letter["ALA"]="A"
        one_letter["CYS"]="C";one_letter["CME"]="C";one_letter["CSE"]="C";one_letter["CSD"]="C";one_letter["CSO"]="C";one_letter["CSS"]="C";one_letter["CCS"]="C";one_letter["P1L"]="C";one_letter["CMT"]="C";one_letter["CSZ"]="C";one_letter["CAS"]="C"
        one_letter["ASP"]="D"
        one_letter["GLU"]="E";one_letter["PCA"]="E"
        one_letter["PHE"]="F"
        one_letter["GLY"]="G";one_letter["GLZ"]="G"
        one_letter["HIS"]="H";one_letter["DDE"]="H";one_letter["HIC"]="H" ;one_letter["NEP"]="H"; one_letter["HSD"]="H"
        one_letter["ILE"]="I"
        one_letter["LYS"]="K";one_letter["KCX"]="K";one_letter["MLY"]="K";one_letter["KCX"]="K";one_letter["LLP"]="K";one_letter["LYZ"]="K"
        one_letter["LEU"]="L"
        one_letter["MET"]="M";one_letter["MSE"]="M";one_letter["CXM"]="M";one_letter["FME"]="M";
        one_letter["ASN"]="N";one_letter["MEN"]="N"
        one_letter["PRO"]="P"
        one_letter["GLN"]="Q"
        one_letter["ARG"]="R";one_letter["ARO"]="R"
        one_letter["SER"]="S";one_letter["SEP"]="S";one_letter["PN2"]="S"
        one_letter["THR"]="T";one_letter["TPO"]="T";
        one_letter["VAL"]="V"
        one_letter["TRP"]="W";one_letter["TRF"]="W";one_letter["TRQ"]="W"
        one_letter["TYR"]="Y";one_letter["PTR"]="Y";one_letter["PAQ"]="Y"
         
        try:
            type = line_string[17:20]
            try:
                return one_letter[type]
            except:
                print("Unknown residue type %s"%type)
                return "X"
        except:
            print("Error: Not valid pdb coor line")
            return None

    def get_atomname_from_pdb_line(self, line,space_stripped=True,ignoreH=True):
        """In a pdb coor line, 13-16
        Return (x,y,z) in float
        """
        try:
            if ignoreH:
                if re.match("H|\dH",re.sub(" ", "",line[12:16])):
                    return None
            if space_stripped:
                return re.sub(" ", "",line[12:16])
            else:
                return line[12:16]
        except:
            print("Error: Not valid pdb coor line")
            return None
        
                
    def get_coor_from_pdb_line(self, line):
        """In a pdb coor line, 31-38, 39-46, 47-54 are x, y, z
        Return (x,y,z) in float
        """
        try:
            return (float(line[30:38]),float(line[38:46]),float(line[46:54]))
        except:
            print("Error: Not valid pdb coor line")
            return None

       
    def squared_eucl_dist(self, lcoor1,lcoor2):
        """ find euclidean distance between coordinates in lcoor1 and coordinates in lcoor2
        return distance as a float
        """
        return (lcoor1[0] - lcoor2[0])**2 + (lcoor1[1] - lcoor2[1])**2 + (lcoor1[2] - lcoor2[2])**2
        

    def find_all_pdb_coor(self, input_PDB):
        """ Extract all coordinates from input_PDB
        """
        
        FHi = open(input_PDB, 'r')

        coor_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))
        res_types = defaultdict(lambda: defaultdict(lambda: "X"))
        chains = []

        for line in FHi:
            if self.if_pdb_atom_line(line):
                c = self.get_chainID_from_pdb_line(line) # if not, segid
                r = self.get_resnum_int_from_pdb_line(line)
                t = self.get_restype_from_pdb_line(line)
                a = self.get_atomname_from_pdb_line(line)
                if a==None:
                    continue
                x, y, z = self.get_coor_from_pdb_line(line)
                coor_dict[c][r][a] = [x,y,z]
                res_types[c][r] = t
                if c not in chains:
                    chains.append(c)
        FHi.close()

        return coor_dict, res_types, chains

    def find_contact(self, coorA, coorB, thresh):
        """ Find if two residues are in contact (at least one atom in rA within thresh (distance) of one atom in rB)
        """

        # Prefilter for speed
        if "CA" in coorA and "CA" in coorB:
            if self.squared_eucl_dist(coorA["CA"], coorB["CA"]) >= 400.0:
                return False
        
        dist=[]
        for aA in coorA:
            for aB in coorB:
                d = self.squared_eucl_dist(coorA[aA], coorB[aB])
                if d <= thresh:
                    dist.append(d)
        if dist: 
            return min(dist)
        return False

    def find_res_in_contact(self, input_PDB, thresh, chainsA, chainsB):
        """ Find lists of residues in contact
        """

        thresh = thresh**2

        coor_dict, _, chains = self.find_all_pdb_coor(input_PDB)

        if chainsA == None and chainsB == None:
            chainsA = chains
            chainsB = chains

        res_lst_interface = set()

        for chainA in chainsA:
            for rA in coor_dict.get(chainA,[]):
                for chainB in chainsB:
                    if chainB != chainA:
                        for rB in coor_dict.get(chainB,[]):
                            d=self.find_contact(coor_dict[chainA][rA], coor_dict[chainB][rB], thresh)
                            if d:
                                res_lst_interface.add("{}:{}".format(rA,chainA))
                                res_lst_interface.add("{}:{}".format(rB,chainB))

        return sorted(list(res_lst_interface))


def usage():
    print("Usage: python get_interface_residues_as_defined_by_CAPRI.py -pdb <pdb file> -ch <ch1:ch2> [-t <distance threshold>]")
    print("       -pdb pdb file with at least 2 chains")
    print("       -ch the 2 chains for which we want to know the interface")
    print("       -t max distance between 2 non-hydrogen atoms to consider them in contact (default = 5.0 A)")
    print("NB: Interface residues are recognised using the min distance between any atom of one chain against any atom of the other.")
    print("    Max distance to define a contact between 2 residues is 5.0 A by default as it is for CAPRI.")

def main():
    """
    """
    if '-h' in sys.argv:
        print("**Help message**")
        usage()
        sys.exit(-1)

    try:
        try:
            chains = sys.argv[sys.argv.index('-ch')+1] # ChainA:ChainB
            chainsA, chainsB = chains.split(':')
        except:
            chainsA, chainsB = None, None
        pdb = os.path.abspath(sys.argv[sys.argv.index('-pdb')+1]) # structure of complex
        try:
            threshold = float(sys.argv[sys.argv.index('-t')+1]) # distance threshold
        except:
            threshold = 5.0

    except Exception as e:
        print("**Not enough or incorrect input arguments**")
        usage()
        sys.exit(-1)

    
    oInter = GetInterface(pdb,threshold,chainsA,chainsB)
    print(oInter.lst_contacts)


if __name__ == '__main__':
    main()
