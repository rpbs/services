#!/usr/bin/env python
'''
Created on 2016-01-26
Updated 2016-03-09 to include iterative alignment shortening in order to avoid empty output

@author: Jessica Andreani, Jinchao Yu
Latest update: CQ Nov 2020 for InterEvDock3 (minor structural changes) and renamed from analyze_conservation.py to mapconservation.py
'''
### LOADING LIBRARIES
try:
    # Standard libraries
    import sys
    import os
    import tempfile
    import re
    import glob
    import optparse
    import pkg_resources
    from collections import defaultdict
    # for python version compatibility
    try:
        import ConfigParser
    except:
        import configparser as ConfigParser
except:
    print("ERROR LIBRARY IMPORTATION")
    print("CHECK YOUR PATHS AND THE PROGRAMS REQUIRED")
    sys.exit(1)

import InterEvDock3.tools as tools

import cluster.cluster as cluster

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))

# programmes
RATE4SITE = config.get("programs","r4s")
MAFFT = config.get("programs","mafft")
HHFILTER = config.get("programs","hhfilter")

# docker locations
DOCKER_R4S = config.get("environments","r4s")
DOCKER_MAFFT = config.get("environments","mafft")
DOCKER_HHSUITE = config.get("environments","hhsuite")

#
# block: tempfile
#
LOCAL_TMP_DIR_PATH = os.getcwd()
if not os.path.exists(LOCAL_TMP_DIR_PATH):
    try:
        os.mkdir(LOCAL_TMP_DIR_PATH)
    except:
        print("The program needs to create a tmp dir at $HOME/tmp/.")
        print("Creation failed.")
        raise IOError
if not os.access(LOCAL_TMP_DIR_PATH, os.W_OK):
    print("No writing right in %s"% LOCAL_TMP_DIR_PATH)
    raise IOError  



class AnalyseConservation():

    def __init__(self,list_dirs,list_alig,list_chains):
        """
        This script takes a list of directories containing decoy pdbs, the list of chains in these pdbs and their
        corresponding coMSA and transforms the pdb b_factor field into a conservation score using Rate4Site

        INPUTS:
        list_dirs: a list of folders (containing pdbs)
        list_alig: a list of alignments (one per chain in the pdbs)
        list_chains: a list of chains in the pdbs

        No output, pdbs files are changed directly
        """

        pdb_list = []
        for folder in list_dirs:
            if os.path.exists(folder):
                pdb_list += self.get_pdb_file_list_in_folder(folder, if_abspath=True)

        if not pdb_list:
            print("Error: no pdb files to convert")
            raise Exception("Error: No pdb files in given folders")

        # a pdb example for r4s mapping
        pdbused = pdb_list[0]

        # first find the bigger chain of the list
        pdbseq = {}
        reslist = {}
        pdbdic = {}
        for chain in list_chains:
            # Extract one chain from the PDB
            tmpdb = tools.make_tempfile()
            self.get_pdbfile_containing_given_chains(pdbused, [chain], tmpdb)
            # load PDB sequence
            pdbseq[chain], reslist[chain], pdbdic[chain] = self.get_pdb_seq(tmpdb)
            os.remove(tmpdb)

        # reorder chain list per chain size
        idx = sorted(range(len(list_chains)),key=lambda k: len(reslist[list_chains[k]]),reverse=True)
        orderedchains = [list_chains[i] for i in idx]
        orderedaligs = [list_alig[i] for i in idx]

        top_n = 100 # start by filtering at 100 seqs (then possibly reduce in steps of 10 seqs until R4S succeeds)
        new_top_n = top_n
        filtering = False   

        for chain,alifile in zip(orderedchains,orderedaligs):

            # Extract one chain from the PDB (receptor or ligand)
            tmpdb = tools.make_tempfile(parent_dir=LOCAL_TMP_DIR_PATH)
            self.get_pdbfile_containing_given_chains(pdbused, [chain], tmpdb)
            
            # load PDB sequence
            pdbseq[chain], reslist[chain], pdbdic[chain] = self.get_pdb_seq(tmpdb)
            os.remove(tmpdb)

            # First match the alignment to the exact PDB sequence
            # If the alignment contains more than top_n sequences, take only the first top_n sequences
            # (to avoid Rate4Site failure)
            # top_n is 100 for the bigger chain but might be reduced more if r4s fails, in that case, we can directly use the new topn for the smaller chains
            if not filtering:
                tmpoutfile = tools.make_tempfile()
                self.get_reduced_alignment(top_n, alifile, tmpoutfile)
                tmpgrade, tmptree, tmpunnorm = self.R4S_processing(tmpoutfile, pdbseq, reslist, pdbdic, chain)

            else:
                # For the smaller protein:
                # take only the first top_n sequences
                tmpoutfile = tools.make_tempfile()
                self.get_reduced_alignment(top_n, alifile, tmpoutfile)
                # then keep the same sequence indices as for the bigger protein
                tmpoutfilefilt = tools.make_tempfile()
                self.extract_seqs_in_multifasta_by_lst_index(tmpoutfile, lst_index, tmpoutfilefilt)

                tmpgrade, tmptree, tmpunnorm = self.R4S_processing(tmpoutfilefilt, pdbseq, reslist, pdbdic, chain)
                
            if os.path.exists(tmpgrade) and os.stat(tmpgrade).st_size > 0:
                r4s = self.computeConservationScores(tmpgrade, pdbseq, reslist, chain)
            else:
                if chain!=orderedchains[0]:
                    # this is unexpected - Rate4Site has worked for the bigger chain, it should also work for the smaller one(s)!
                    print("Unexpected Rate4Site failure for smaller chain(s) - filling bfact column with zeros")
                    # fill the bfact column with zeros
                    print("No grade file")
                    r4s = {}
                    r4s["score"] = {}
                    
                else:
                    # If rate4site has failed to create the grade file for some reason, try to further reduce the alignment and rerun R4S (for the biggest of the two chains)
                    while (not (os.path.exists(tmpgrade) and os.stat(tmpgrade).st_size > 0) and new_top_n>0):
                        new_top_n = new_top_n - 10
                        tmpoutfilefilt = tools.make_tempfile()
                        nb_seqs, redundancy_filter = self.use_hhfilter_by_nb_seqs(tmpoutfile, tmpoutfilefilt, new_top_n, 95)

                        #print(nb_seqs, redundancy_filter)

                        if not nb_seqs or nb_seqs <= 1 or redundancy_filter < 30:
                            print("Breaking while loop")
                            break
                        
                        filtering = new_top_n
                        # find the indices of the sequences in the last filtered alignment
                        lst_index = self.read_indices_seqs_in_multifasta(tmpoutfilefilt, tmpoutfile)
                        
                        tmpgrade, tmptree, tmpunnorm = self.R4S_processing(tmpoutfilefilt, pdbseq, reslist, pdbdic, chain)
                        
                    if os.path.exists(tmpgrade) and os.stat(tmpgrade).st_size > 0:
                        # R4S successful: compute conservation scores
                        r4s = self.computeConservationScores(tmpgrade, pdbseq, reslist, chain)
                    else:
                        # this is unexpected - we have reached 0 sequences and R4S still hasn't produced an output file
                        print("Unexpected Rate4Site failure for biggest chain - filling bfact column with zeros")
                        # fill the bfact column with zeros
                        print("No grade file")
                        r4s = {}
                        r4s["score"] = {}
                    
                    
            os.remove(tmpoutfile)
            if filtering and os.path.exists(tmpoutfilefilt):
                os.remove(tmpoutfilefilt)
            if os.path.exists(tmpgrade):
                os.remove(tmpgrade)
            if os.path.exists(tmptree):
                os.remove(tmptree)
            if os.path.exists(tmpunnorm):
                os.remove(tmpunnorm)


            # Overwrite PDB files, replacing the bfact field with the conservation score
            for pdbfile in pdb_list:
                # write output PDB file containing the conservation score in the bfact field
                self.createPDBFromR4S(r4s, chain, pdbfile)


    def get_pdb_file_list_in_folder(self, folder, if_abspath=False):
        """Get the list of file names ending at ".pdb" in the given directory.
        If if_abspath=True, return file absolute paths instead of file names
        """
        pdb_file_list = []
        for filename in os.listdir(folder):
            res_search = re.search("\.pdb$",filename)
            if res_search:
                if if_abspath:
                    file_abspath = os.path.abspath(os.path.join(folder,filename))
                    pdb_file_list.append(file_abspath)
                else:
                    pdb_file_list.append(filename)
        return pdb_file_list

    def get_pdbfile_containing_given_chains(self, PDB_file, chains, outfile=None):
        """
        chains (in string): 'AB', ' ', etc., '_' represents ' ' (void ID).
        For example, 'AB_A' or 'A B' both represent chains 'A', 'B' and ' '
        if outfile is None, no file will created. Outfile content will always be returned.
        Return outfile content
        """
        # no chain ID is given
        if len(chains)<1 or chains=='all':
            if outfile:
                shutil.copy(PDB_file, outfile)
            FHi = open(PDB_file, 'r')
            out_content = FHi.read()
            FHi.close()
            return out_content
        
        try:
            FH = open(PDB_file,"r")
        except:
            print("can not open the PDB file: " + PDB_file)
            return 1
        chain_set = set(chains)
        if '_' in chain_set:
            chain_set.discard('_')
            chain_set.add(' ')
        out_content = ""
        for line in FH:
            res_match_ATOM = re.match("ATOM  ",line)
            res_match_HETATM = re.match("HETATM",line)
            
            if res_match_ATOM or res_match_HETATM:
                try:
                    if line[21] in chain_set:
                        out_content += line
                except:
                    print("Abnormal PDB format detected")
                    print(line)
            else:
                res_match_END = re.match("END",line)
                if res_match_END:
                    out_content += line
        FH.close()
        if outfile:
            FHo = open(outfile,'w')
            FHo.write(out_content)
            FHo.close()
        return out_content

    def read_multi_fasta(self, multi_fasta_file):
        """ read multi fasta file and return the list of headers and the list of seqs
        Return (list_header,list_seq)
        """
        headers = []
        seqs = []
        FHi = open(multi_fasta_file)
        
        lines = FHi.readlines()
        index_seq = -1
        for line in lines:
            line = line.rstrip('\n')
            res = re.match(">(.*)",line)
            if res:
                headers.append(res.group(1))
                index_seq += 1
                seqs.append("")
            else:
                try:
                    seqs[index_seq] += re.sub("[\t ]","",line)
                except:
                    print("Fasta format error.")
        FHi.close()
        return (headers,seqs)

    def write_multi_fasta_file(self, header_list,seq_list,outfile):
        """write_multi_fasta_file(header_list,seq_list,outfile)
        header_list: list of headers
        seq_list: list of sequences (string)
        outfile: output multi-fasta file path
        """
        if len(header_list) != len(seq_list):
            print("Error: cannot write fasta file. Header number: %d, seq number: %d"%(len(header_list),
                                                                                       len(seq_list)))
            sys.exit(1)
        FHo = open(outfile, 'w')
        for i in range(len(header_list)):
            FHo.write('>%s\n'%header_list[i])
            FHo.write("%s\n"%seq_list[i])
        FHo.close()
            

    def get_reduced_alignment(self, top_n, alifile, outfile):
        """ Create alignment containing only the top top_n sequences from alifile
        """

        list_header,list_seq = self.read_multi_fasta(alifile)
        if len(list_header) > top_n:
            self.write_multi_fasta_file(list_header[0:top_n], list_seq[0:top_n], outfile)
        else:
            self.write_multi_fasta_file(list_header, list_seq, outfile)

        return outfile

    def use_hhfilter(self, in_fasta,out_fasta,redundancy_filter):
        """Return the nb of sequences in the out_fasta according to its log file
        Normally hhfilter takes a3m as input and output formats, however, a fasta file 
        is like a format a3m, just like we do "reformat fasta a3m -M 100"
        """
        tmp_log = tools.make_tempfile("tmp_hhfilter.log")
        args = ['-i '+in_fasta,
                '-o '+out_fasta,
                '-id %.2f'%redundancy_filter,
                '> '+ tmp_log]
        cluster.runTasks(HHFILTER, args, environment_module = DOCKER_HHSUITE, log_prefix = "analyze_conservation")

        FHi = open(tmp_log, 'r')
        nb_seqs = None
        for line in FHi:
            res = re.match("(\d+) out of .* sequence.*filter",line)
            if res:
                nb_seqs = int(res.group(1))
                break
        FHi.close()
        if os.path.exists(tmp_log):
            os.remove(tmp_log)
        if not nb_seqs:
            print("Error: hhfilter failed.")
        return nb_seqs
        
    def use_hhfilter_by_nb_seqs(self, in_fasta,out_fasta, nb_seqs_max, start_redundancy_filter=90):
        """return nb_seqs, redundancy_filter
        Use "use_hhfilter" in a loop by decreasing the redundancy option from 
        start_redundancy_filter by 5(%) each time until the number of sequences in the output 
        fasta file is no more than nb_seqs_max
        """
        redundancy_filter = start_redundancy_filter
        nb_seqs = self.use_hhfilter(in_fasta,out_fasta,redundancy_filter)
        while nb_seqs and nb_seqs > nb_seqs_max:
            redundancy_filter = redundancy_filter - 5 
            nb_seqs = self.use_hhfilter(in_fasta,out_fasta,redundancy_filter)
            if redundancy_filter < 30:
                print("Error: something is wrong")
                break
            
        return nb_seqs, redundancy_filter

    def read_indices_seqs_in_multifasta(self, in_multifasta, ref_multifasta):
        """read seqs from in_multifasta, find these seqs in ref_multifasta by the headers and
        return their indices
        """
        in_headers = self.read_multi_fasta(in_multifasta)[0]
        ref_headers = self.read_multi_fasta(ref_multifasta)[0]
        lst_index = []
        try:
            for in_head in in_headers:
                lst_index.append(ref_headers.index(in_head))
        except:
            print("Error: Make sure the header in the input fasta file " +\
                  "%s is present in the %s"%(in_multifasta, ref_multifasta))
        #print(lst_index)
        return lst_index   
        
    def extract_seqs_in_multifasta_by_lst_index(self, in_multifasta, lst_index, out_multifasta):
        """
        """
        in_headers, in_seqs = self.read_multi_fasta(in_multifasta)
        try:
            out_headers, out_seqs = [], []
            for i in lst_index:
                out_headers.append(in_headers[i])
                out_seqs.append(in_seqs[i])
            self.write_multi_fasta_file(out_headers,out_seqs,out_multifasta)
            
        except:
            print("Error: the demanded index is greater than the total number of seqs")

    def load_alignment(self, ali):
        """ get first sequence in alignment
        """
        alidic = {}
        idlist = []

        FH = open(ali,'r')
        ite = 0
        for line in FH:
            if line[0]==">":
                ite += 1
                alidic[ite] = ""
                idlist.append(ite)
            else:
                alidic[ite] += line.strip()
        FH.close()
        
        return alidic, idlist

    def if_pdb_coor_line(self, line):
        """if this line (in PDB file) begins by "ATOM  " or "HETATM", this is a coor line
        """
        res_match_ATOM = re.match("ATOM  ",line)
        res_match_HETATM = re.match("HETATM",line)
        if res_match_ATOM or res_match_HETATM:
            return True
        else:
            return False

    def get_chainID_from_pdb_line(self, line_string):
        """get pdb line[21] if this is a correct PDB coor line; if not return None
        """
        try:
            return line_string[21]
        except:
            print("Error: Not valid pdb coor line")
            return None

    def get_resnum_int_from_pdb_line(self, line_string):
        """get pdb line[22:26] if this is a correct PDB coor line; if not return None
        """
        try:
            return int(line_string[22:26])
        except:
            print("Error: Not valid pdb coor line")
            return None

    def get_restype_from_pdb_line(self, line_string):
        """get the one-letter residue type
        the three-letter type is given by pdb line[17:20] if this is a correct PDB coor line; if not return None
        Replaces unknown residue types with a gap.
        """

        one_letter = {}
        one_letter["ALA"]="A"
        one_letter["CYS"]="C";one_letter["CME"]="C";one_letter["CSE"]="C";one_letter["CSD"]="C";one_letter["CSO"]="C";one_letter["CSS"]="C";one_letter["CCS"]="C";one_letter["P1L"]="C";one_letter["CMT"]="C";one_letter["CSZ"]="C";one_letter["CAS"]="C"
        one_letter["ASP"]="D"
        one_letter["GLU"]="E";one_letter["PCA"]="E"
        one_letter["PHE"]="F"
        one_letter["GLY"]="G";one_letter["GLZ"]="G"
        one_letter["HIS"]="H";one_letter["DDE"]="H";one_letter["HIC"]="H" ;one_letter["NEP"]="H"
        one_letter["ILE"]="I"
        one_letter["LYS"]="K";one_letter["KCX"]="K";one_letter["MLY"]="K";one_letter["KCX"]="K";one_letter["LLP"]="K";one_letter["LYZ"]="K"
        one_letter["LEU"]="L"
        one_letter["MET"]="M";one_letter["MSE"]="M";one_letter["CXM"]="M";one_letter["FME"]="M";
        one_letter["ASN"]="N";one_letter["MEN"]="N"
        one_letter["PRO"]="P"
        one_letter["GLN"]="Q"
        one_letter["ARG"]="R";one_letter["ARO"]="R"
        one_letter["SER"]="S";one_letter["SEP"]="S";one_letter["PN2"]="S"
        one_letter["THR"]="T";one_letter["TPO"]="T";
        one_letter["VAL"]="V"
        one_letter["TRP"]="W";one_letter["TRF"]="W";one_letter["TRQ"]="W"
        one_letter["TYR"]="Y";one_letter["PTR"]="Y";one_letter["PAQ"]="Y"
         
        try:
            type = line_string[17:20]
            try:
                return one_letter[type]
            except:
                #print("Unknown residue type %s"%type)
                return "-"
        except:
            print("Error: Not valid pdb coor line")
            return None

    def get_pdb_seq(self, input_PDB):
        """ get sequence from PDB file
        """

        pdbdic = {}
        reslist = []
        # PDB structure
        FHi = open(input_PDB, 'r')

        for line in FHi:
            if self.if_pdb_coor_line(line):
                r = self.get_resnum_int_from_pdb_line(line)
                t = self.get_restype_from_pdb_line(line)
                if r not in reslist:
                    # new residue number
                    reslist.append(r)
                    pdbdic[r] = t
                    
        FHi.close()

        pdbseq = ""
        for r in reslist:
            pdbseq += pdbdic[r]
                
        return pdbseq, reslist, pdbdic

    def align_sequences(self, aliseq, pdbseq, input, output):
        """ align the first sequence in the alignment with the PDB sequence
        """
        aliseq = aliseq.replace("-","")
        FHo = open(input, 'w')
        FHo.write(">aliseq\n%s\n>pdbseq\n%s\n"%(aliseq, pdbseq))
        FHo.close()

        args = ["--auto",
                input,
                "> "+ output,
                "2> /dev/null"]
        cluster.runTasks(MAFFT, args, environment_module = DOCKER_MAFFT, log_prefix = "analyze_conservation")
        

    def get_ali_positions(self, aliseq, pdbseq):
        """ get alignment positions to retain (only those contained in the PDB sequence)
        """
        finalalignogap = [] # contains positions to retain in the alignment and "-" when there is an amino acid in the PDB that is not present in the alignment
        itepdb = 0
        itenogap = -1
        
        # go through first sequence in alignment
        for ite, i in enumerate(aliseq):
            
            # get amino acid from PDB
            j =  pdbseq[itepdb]
            
            if i != "-":
                itenogap += 1 # itenogap is an iterator going through non-gapped positions
                
            if j != "-" and i !="-":
                # if none of the two is a gap
                if i!=j:
                    print("LOG -> Warning AA PDB/alig different %s %s"%(i, j))
                    
                # sometimes there are small sequence variations between the PDB sequence and the alignment
                # here we trust the alignment
                finalalignogap.append(itenogap) # keep the position from the alignment
                itepdb += 1 # increase position in PDB sequence
                
            if  i == "-" and j != "-":
                # the PDB contains an amino acid that is not in the alignment
                finalalignogap.append("-") # the gap indicates that the alignment does not contain one amino acid from the PDB
                itepdb += 1
                print("LOG -> AA not found in alignment, introduction of a gap in alignement %s %s"%(i, j))
                continue
                
            if j == "-" and i != "-":
                # the PDB has a gap and the alignment does not
                # here we do not select this position in the alignment
                itepdb+=1
                continue

        return finalalignogap

    def get_sub_alignment(self, alidic, idlist):
        """ retrieve total alignment selecting only non-gapped positions in the first sequence
        """

        subfasta = {}
        for ite, i in enumerate(alidic[1]):
            if i != "-":
                for id in idlist:
                    if id not in subfasta:
                        subfasta[id] = []
                    subfasta[id].append(alidic[id][ite])
                    
        return subfasta

    def write_modified_alignment(self, idlist, subfasta, finalalignogap, reslist, pdbdic, output):
        """ write alignment matching exactly PDB sequence to file
        """

        fout = open(output, "w")
        for id in idlist:
            fout.write(">%i\n"%(id))
            ite = 0 # ite serves as an iterator on reslist
            
            # go through positions of the PDB/first seq in alignment
            for i in finalalignogap:

                pospdb = reslist[ite]
                
                # follow PDB numbering: if the residue is not known, insert a gap
                if pdbdic[pospdb] == "-":
                    fout.write("-")
                    ite += 1

                if i != "-":
                    fout.write("%s"%(subfasta[id][i]))
                else:
                    # if the PDB contains an amino acid not present in the sequence
                    fout.write("-")
                ite += 1
            fout.write("\n")
        fout.close()



    def computeR4S(self, ali, chain, resultgrade, resulttree, resultunnorm):
        """ compute rate4site results for receptor and ligand
        and write them to directory resultdir
        """
        args = ['-s '+ali,
                '-o '+resultgrade,
                '-x '+resulttree,
                '-y '+resultunnorm,
                '> /dev/null',
                '2> /dev/null']
        cluster.runTasks(RATE4SITE, args, environment_module = DOCKER_R4S, log_prefix = "analyze_conservation")

        # remove parasite r4s.res file created in the curr directory by rate4site
        if os.path.exists(os.path.join(os.getcwd(), "r4s.res")):
            os.remove(os.path.join(os.getcwd(), "r4s.res"))
        
    def getScoreFromGradeFileR4S(self, ad_r4s):
        """ get R4S scores from the grade R4S file.
        Also catch the sequence that will be used later
        """
        f_r4s = open(ad_r4s,"r")
        l_r4s = f_r4s.readlines()
        f_r4s.close()
        
        score = {}
        res_order = []
        gradeseq = ""
            
        for line in l_r4s:
            if (re.search("#",line) == None) and (re.search("[0-9]",line)):
                res_number = line.split()[0]
                score[res_number] = float(line.split()[2])
                res_order.append(res_number)

                gradeseq += line.split()[1]
                
        return res_order, score, gradeseq

    def computeConservationScoreR4S(self, res_order,score):
        """ Compute the conservation score for each residue
        """
        
        r4s_value = {}
        r4s_value["order"] = []
        r4s_value["score"] = {}
        
        val_score = score.values()
        amplitude = max(val_score) - min(val_score)
        bfact_min = min(val_score)

        for res in res_order:

            r4s_value["order"].append(res)
            
            bfact          = 100. * (1 - (score[res] - bfact_min) / (amplitude))
                
            r4s_value["score"][res] =  bfact
            if r4s_value["score"][res] > 99:
                r4s_value["score"][res] = 99.99
            if r4s_value["score"][res] < 0:
                r4s_value["score"][res] = 1.0
        return r4s_value


    def reindex_r4s(self, r4s, gradeseq, pdbseq, reslist, input, output):
        """ Reindex the r4s dictionary so that the residue numbers match the PDB numbering
        """   
        FHo = open(input, 'w')
        FHo.write(">gradeseq\n%s\n>pdbseq\n%s\n"%(gradeseq, pdbseq))
        FHo.close()

        args = ["--auto",
                input,
                "> "+ output,
                "2> /dev/null"]
        cluster.runTasks(MAFFT, args, environment_module = DOCKER_MAFFT, log_prefix = "analyze_conservation")

        matchdic, _ = self.load_alignment(output)

        localgradeseq = matchdic[1]
        localpdbseq = matchdic[2]

        r4s_corres = {}
        r4s_corres["order"] = []
        r4s_corres["score"] = {}

        ite_index = 0  # iterator of reslist (true number in the pdb file)
        ite_r4s   = 1  # iterator of the r4s_value["bin"] number in the r4s file
            
        for spdb,sr4s in zip(localpdbseq, localgradeseq):
                
            if spdb != "-":
                cur_index_pdb = reslist[ite_index]
                ite_index += 1 # we go to the next index   
                    
                if sr4s != "-":
                    r4s_corres["score"][cur_index_pdb] = r4s["score"][str(ite_r4s)]
                    r4s_corres["order"].append(cur_index_pdb)
                    
                    ite_r4s += 1
                else:
                    r4s_corres["score"][cur_index_pdb] = 0.0
                    r4s_corres["order"].append(cur_index_pdb)
                    
            else:
                if sr4s != "-":
                    ite_r4s += 1
            
        return r4s_corres

        

    def createPDBFromR4S(self, r4s_value, chain, input_PDB):
        """ Create an output PDB file essentially identical to the input,
        except that it contains R4S-derived conservation scores in the bfact field
        (or zero if no R4S score has been calculated for this residue/PDB)
        """
            
        # PDB structure
        FHi = open(input_PDB, 'r')
        lines = FHi.readlines()
        FHi.close()

        FHo = open(input_PDB, 'w')
        for line in lines:
            s1 = line.split()
            if s1 != [] and s1[0]=="ATOM":
                ch = self.get_chainID_from_pdb_line(line)

                if ch!=chain:
                    # change only the lines corresponding to the current chain
                    FHo.write(line)
                    continue
                
                res_number = self.get_resnum_int_from_pdb_line(line)
                
                if res_number in r4s_value["score"]:
                    FHo.write(line[:60]+"%6.2f"%(r4s_value["score"][res_number])+line[66:])
                else:
                    FHo.write(line[:60]+"%6.2f"%(0.0)+line[66:])
            elif s1 != [] and s1[0]=="HETATM":
                FHo.write(line[:60]+"%6.2f"%(0.0)+line[66:])
            else:
                # 
                FHo.write(line)

        FHo.close()


    def R4S_processing(self, tmpoutfile, pdbseq, reslist, pdbdic, chain):
        """ Steps of processing for Rate4Site computation
        """
        alidic, idlist = self.load_alignment(tmpoutfile)
                
        # Compare the first (non-gapped) sequence in the alignment to the PDB sequence
        # first align the sequences
        tmpinput = tools.make_tempfile()
        tmpalig = tools.make_tempfile()
        self.align_sequences(alidic[1], pdbseq[chain], tmpinput, tmpalig)
        
        # load the (match) alignment
        matchdic, _ = self.load_alignment(tmpalig)
        
        os.remove(tmpinput)
        os.remove(tmpalig)
        
        # get alignment positions to retain
        finalalinogap = self.get_ali_positions(matchdic[1], matchdic[2])
        
        # Retrieve sub-alignment (from original alignment) containing only non-gapped positions in the first sequence
        subfasta = self.get_sub_alignment(alidic, idlist)
        
        # Write modified alignment matching PDB sequence numbering to file
        tmpcomsa = tools.make_tempfile()
        self.write_modified_alignment(idlist, subfasta, finalalinogap, reslist[chain], pdbdic[chain], tmpcomsa)
        
        # Compute R4S scores
        tmpgrade = tools.make_tempfile()
        tmptree = tools.make_tempfile()
        tmpunnorm = tools.make_tempfile()
        self.computeR4S(tmpcomsa, chain, tmpgrade, tmptree, tmpunnorm)
        os.remove(tmpcomsa)
        return tmpgrade, tmptree, tmpunnorm

    def computeConservationScores(self, tmpgrade, pdbseq, reslist, chain):
        """ Compute conservation scores from grade files
        """
        # Compute conservation scores from grade files
        res_order, scores, gradeseq = self.getScoreFromGradeFileR4S(tmpgrade)
        r4s = self.computeConservationScoreR4S(res_order,scores)
        # We need to reindex the data so that the residue numbers match the PDB numbering (because rate4site ignores unknown residues)
        tmpinput = tools.make_tempfile()
        tmpalig = tools.make_tempfile()
        r4s = self.reindex_r4s(r4s, gradeseq, pdbseq[chain], reslist[chain], tmpinput, tmpalig)
        os.remove(tmpinput)
        os.remove(tmpalig)
        os.remove(tmpgrade)

        return r4s


def main():
    """
    """
    
    # if '-h' in sys.argv or len(sys.argv)!=2:
    #     usage()
    #     sys.exit(-1)

    USAGE = "Usage: python mapconservation.py -d directoryname1,directoryname2[,...] -a coMSA_chB,coMSA_chB[,...] -c A,B[,...]\n"+\
            "Directories directorynameX should contain decoys in PDB format.\n"+\
            "This script will overwrite all models in the directories, replacing the bfact column with values based on conservation.\n"+\
            "One coMSA sould be provided per chain in these decoys and specified with -a (corresponding chains should be specified with the -c option)\n"+\
            "\nUse option -h or --help for more usage information\n"
    
    parser = optparse.OptionParser(usage = USAGE)
    
    parser.add_option('-d','--list_dirs',
                      action="store",
                      dest='list_dirs',
                      type='string',default='',
                      help="List of folders that contain pdbs e.g. InterEvScore_models,frodock_models,soap_pp_models")

    parser.add_option('-a','--list_alig',
                      action="store",
                      dest='list_alig',
                      type='string',default='',
                      help="list of coMSAs (1 per chain in the pdbs) e.g. coMSA_chA.fasta,coMSA_chB.fasta")

    parser.add_option('-c','--list_chains',
                      action="store",
                      dest='list_chains',
                      type='string',default='',
                      help="list of chains listed in the same order as in --list_alig e.g. A,B")

    if len(sys.argv) == 1:
        print(USAGE)
        print("Type -h or --help for more help information")
        sys.exit(1)

    (options, args) = parser.parse_args(sys.argv)

    list_dirs = [os.path.abspath(d) for d in options.list_dirs.split(",") if os.path.exists(d)]
    if not list_dirs:
        print("No readable pdb folders")
        print(USAGE)
        sys.exit(1)

    list_alig = [os.path.abspath(a) for a in options.list_alig.split(",") if os.path.exists(a)]
    list_chains = options.list_chains.split(",")
    if not list_alig:
        print("No readable alignment files")
        print(USAGE)
        sys.exit(1)

    if len(list_alig) != len(list_chains):
        print("number of alignment files doesn't match number of given chains")
        print(USAGE)
        sys.exit(1)


    AnalyseConservation(list_dirs, list_alig, list_chains)


if __name__ == '__main__':
    main()
