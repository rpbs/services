#!/usr/bin/env python

"""
author: Yasaman Karami
7 November 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
from __future__ import print_function
import sys
import os
import argparse
import multiprocessing as mp
import Fasta.Fasta as Fasta
import json
from multiprocessing import Process
from PyPDB import PyPDB as PDB
from PyPDB.Geo3DUtils import *
from optparse import OptionParser
import numpy as np


DFLT_WORK_PATH     = "./"
DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"
DFLT_DEMO_PATH     = "/service/env/DaReUS_Loop/demo/" #"/scratch/user/karami/DaReUS-Loop/demo/"
DFLT_NPROC         = 1
DFLT_FLANK_SZE     = 4
DFLT_CLASH_SZE     = 3.
DFLT_MIN_LOOP_SZE  = 2
DFLT_MAX_LOOP_SZE  = 30
DFLT_MAX_LOOP_NUM  = 150 # CQ: used to be 20
DFLT_BC            = 0.0
DFLT_RIGIDITY      = 3.0
DFLT_RMSD          = 4.0
DFLT_MINIMIZE      = 1
DFLT_KORP          = 1
DFLT_MODE          = "local"  #else "cluster" or "mobyle"
DFLT_DOCKER_IMAGE  = "dareus-loop/1.0-rpbs"
PROT_DIR           = "DaReUS_Loop"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_HOME_PPP      = "/usr/local/PyPPP3/"
DFLT_ROOT          = "/src/"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_HOME          = DFLT_ROOT + "/DaReUS_Loop/"
DFLT_PDB_CLS_PATH  = DFLT_BANK + "/PDB_clusters/"
DFLT_PDB_PATH      = DFLT_BANK + "/pdbChainID/"
DFLT_PROF_PATH     = DFLT_BANK + "/PyPPP_profiles/"
MAX_JOB            = 300
DFLT_LBL           = "dareus"
DFLT_ADV           = 0
AAs = "ARNDCEQGHILKMFPSTWYV*-arndceqghilkmfpstwyv"
AAs2 = "ARNDCEQGHILKMFPSTWYV"
AAs3 = "arndceqghilkmfpstwyv"

DOCKER_GROMACS = "gromacs/2020.4.gromacs-py-rpbs"
DOCKER_PYPPP3 = "pyppp3-light/1.0-rpbs"
DOCKER_OSCAR = "oscar-star/1.0-rpbs"
DOCKER_KORP = "korp/1.22-rpbs"

### running on a local computer
run_docker         = os.path.abspath(os.path.dirname(__file__)) + "/src/DaReUS_Loop"
gromacs_path       = "/media/internal_backup/Cyclic/script/Gromacs/gromacs_lib/gromacs_lib"
#gromacs_path      = "/media/internal_backup/Cyclic/script/Gromacs/gromacs_py-master/gromacs_py/"
######################################
def formatViewer(models, residue_list, output_file, wdir):
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('dareusloop.vis.html')
        sink.write(template.render(models = json.dumps(models), residue_list = json.dumps(residue_list), wdir = wdir))

#def formatTables(data, output_file, wdir):
#    with open(output_file, 'wt') as sink:
#        template = jinja_env.get_template('dareusloop.table.html')
#        #sink.write(template.render(data = json.dumps(data)), wdir = wdir) # FIXME Remove wdir?
#        #sink.write(template.render(data = data, wdir = wdir)) # FIXME Remove wdir?
#        sink.write(template.render(data = data)) # FIXME Remove wdir?

def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def modify_in_pdb(model_dir, targetAddr, target):
    '''
    check input pdb and prepare it for remodeling:
    read the first model, first chain, ignore water molecules and
    convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    take care of alternate coordinates
    '''
    input_PDB = PDB.PDB(targetAddr + target)
    ### check if the PDB file is valid
    if len(input_PDB) == 0:
        progress("Error! The input PDB file is not valid!")
        sys.exit(1)

    ### read the first model and the first chain
    nb_models = input_PDB.nModels()
    if nb_models > 1:
        progress("Warning! Only the first model from the input PDB is considered!")
        input_PDB.setModel(1)

    all_chains = input_PDB.chnList()
    if len(all_chains) > 1:
        progress("Warning! Only the first chain from the input PDB is considered!")
        GappedPDB = input_PDB.chn(all_chains[0])
    else:
        GappedPDB = input_PDB

    ### clean the input PDB and
    ### check for insertion codes
    GappedPDB.clean()
    mask_list2 = []
    for ll in range(len(GappedPDB)):
        if GappedPDB[ll].riCode() != ' ':
            mask_list2.append(GappedPDB[ll].rNum())
    if len(mask_list2) > 0:
        progress("Error! Please decide for the insertion codes: %s" %mask_list2)
        sys.exit(1)

    ### remove water molecules
    '''
    start_solve = 0
    end_solve = 0
    for ii in range(len(GappedPDB)):
        if GappedPDB[ii].rType() == 'SOLVENT':
            if start_solve == 0:
                start_solve = ii
            if end_solve == 0:
                end_solve = ii
            elif end_solve == ii-1:
                end_solve = ii
            else:
                progress("Error! Please provide an input PDB without solvent molecules.")
                sys.exit(1)
    if start_solve > 0 and end_solve > 0:
        if end_solve == (len(GappedPDB) - 1):
            GappedPDB = GappedPDB[0:start_solve]
        else:
            progress("Error! Please provide an input PDB without solvent molecules.")
            sys.exit(1)
    '''
    has_solvent = 0
    ii = 0
    while ii < len(GappedPDB) and has_solvent == 0 :
        if GappedPDB[ii].rType() == 'SOLVENT':
            has_solvent = 1
        ii += 1
    if has_solvent == 1:
        progress("Warning! All the solvent molecules and hetatoms have been removed from the input PDB file!\n")
    GappedPDB.out(model_dir + "modified_input_protein1.pdb", altCare = 1, OXTCare = 1, hetSkip = 1)

    ### remove HETATMs
    out_modif_no_het_pdb = open(model_dir + "modified_input_protein.pdb", "w")
    read_modif_pdb = open(model_dir + "modified_input_protein1.pdb", "r").readlines()
    for i in range(len(read_modif_pdb)):
        if read_modif_pdb[i].startswith("HETATM"): continue
        out_modif_no_het_pdb.write(read_modif_pdb[i])
    out_modif_no_het_pdb.close()

    ### check for modified amino aids
    GappedPDB = PDB.PDB(model_dir + "modified_input_protein.pdb")
    refe_seq = GappedPDB.aaseq()
    if 'X' in refe_seq:
        progress("Error! Please only provide standard amino acids in your PDB file!")
        sys.exit(1)

    ### check for missing atoms   #alternate atoms
    nb_BB, list_BB = GappedPDB.BBatmMiss()
    if nb_BB > 0:
        progress("Error! Backbone atoms are missing in your input PDB: %s" %list_BB)
        sys.exit(1)
    return


def find_gap(GappedSeq, NativeSeq, modeling_mode, flank_size):
    before_gap = 1; nbLoop = 0
    gap_start = []; gap_end = []; before_gap_start_ind = []; Loop_seq = []
    len_seq = len(NativeSeq)
    ind_nat = 0; no_gap = 0
    while ind_nat < len_seq:
        this_seq_pos = GappedSeq[ind_nat]
        if this_seq_pos == '-':
            Rstart = ind_nat
            while ind_nat < len_seq and GappedSeq[ind_nat] == '-' :
                ind_nat += 1
            looplength = ind_nat - Rstart
            if Rstart - before_gap + 1 >= flank_size:
               before_gap = ind_nat + 1
               gap_start.append(Rstart + 1)
               gap_end.append(ind_nat )
               nbLoop += 1
               Loop_seq.append(NativeSeq[Rstart : ind_nat])
               if modeling_mode == "Modeling":
                   before_gap_start_ind.append(no_gap - 1)
               else:
                   before_gap_start_ind.append(Rstart - 1)
            else:
               progress("Error! There must be at least 4 residues between any two gaps. Please modify the inputs!")
               sys.exit(1)
        ind_nat += 1
        no_gap += 1
    return gap_start, gap_end, before_gap_start_ind, Loop_seq, nbLoop

def parsing_seq(Model, Model_seq, seq1):
    '''
    parsing the gapped sequence and initial model (pdb) to find the loop positions
    any lower case or special character on the sequence is regarded as gap
    checking if there is any inconsistency between the sequence and pdb
    '''
    AA1 = "ACDEFGHIKLMNPQRSTVWY"
    seq2 = ''
    first_ind = int(Model[0].rNum())
    last_ind = int(Model[len(Model)-1].rNum())
    len_model = last_ind - first_ind + 1
    '''
    if len_model != len(seq1):
        progress("Error! There is a mismatch between the sequence and the structure!") #The sequence does not contain all the residues available on the structure! Or the numbering does not start from 1!")
        sys.exit(1)
    '''
    native_len = max(len_model, len(seq1))
    prot_seq = np.chararray(native_len)
    for i in range(native_len): prot_seq[i] = '-'
    model_to_seq = {}
    for i in range(len(Model)):
        str_pos = int(Model[i].rNum())
        ind = str_pos - first_ind #1
        prot_seq[ind] = Model_seq[i]
        model_to_seq[ind] = i

    for i in range(len(seq1)):
        gap = False
        if seq1[i] in AA1: # or seq1[i].upper() in AA1:
            if seq1[i] != prot_seq[i]:
                seqpos = model_to_seq[i]
                str_pos = Model[seqpos].rNum()
                progress("Error! There is a mismatch between the residue %s%d on the sequence and the residue %s%s on the input structure!" % (seq1[i], (i+1), prot_seq[i], str_pos))
                sys.exit(1)
        elif seq1[i] == '-' or seq1[i].upper() in AA1:
            gap = True
        else:
            progress("Error! The gapped sequence/structure contains an unknown amino acid '%s' at residue %d!" % (seq1[i] ,(i+1)) )
            sys.exit(1)

        if not gap:
            seq2 += seq1[i].upper()
        else:
            seq2 += '-'
    return seq2

def parsing_str(Model, Model_seq, NativeSeq, flank_size):
    '''
    parsing the gapped pdb and full sequence to find the loop positions
    checking if there is any inconsistency between the sequence and pdb
    '''
    first_ind = int(Model[0].rNum())
    last_ind = int(Model[len(Model)-1].rNum())
    len_model = last_ind - first_ind + 1
    '''
    if len_model != len(NativeSeq):
        progress("Error! There is a mismatch between the sequence and the structure!") #Or the numbering does not start from 1!")
        sys.exit(1)
    '''
    str_first_four = Model_seq[0:flank_size]
    seq_first_four = NativeSeq[0:flank_size]
    if str_first_four in seq_first_four and int(Model[0].rNum()) > 1:
        progress("Error! The input structure has to start from residue 1!")
        sys.exit(1)

    native_len = max(last_ind, len(NativeSeq))
    seq = np.chararray(native_len)
    for i in range(native_len): seq[i] = '-'
    for i in range(len(Model)):
        ind = int(Model[i].rNum()) - 1  #first_ind
        seq[ind] = Model_seq[i]
    seq_ini = "".join(seq)

    '''
    seq_Nter=''
    start_seq = seq_ini[0:flank_size]
    start_ind = NativeSeq.find(start_seq)
    if start_ind > 0:
        for i in range(start_ind):
            seq_Nter += '-'
    seq_Nter += seq_ini

    seq_Nter_Cter = seq_Nter
    end_seq = seq_ini[(len(seq_ini)-flank_size):len(seq_ini)]
    end_ind = NativeSeq.find(end_seq)
    if end_ind > 0 :
        cter_size = len(seq_ini) - end_ind - flank_size
        for i in range(cter_size):
            seq_Nter_Cter += '-'

    for i in range(native_len):
        if seq_Nter_Cter[i] != '-':
            if seq_Nter_Cter[i] != NativeSeq[i]:
                str_pos = (i+1) + first_ind + start_ind
                progress("Error! There is a mismatch between the amino acid %s at position %d on the sequence and the residue %s%d on the input structure!" % (NativeSeq[i], (i+1), seq_Nter_Cter[i], str_pos))
                sys.exit(1)
    '''
    for i in range(native_len):
        if seq_ini[i] != '-':
            if seq_ini[i] != NativeSeq[i]:
                progress("Error! There is a mismatch between the amino acid %s at position %d on the sequence and the residue %s%d on the input structure!" % (NativeSeq[i], (i+1), seq_ini[i], (i+1)))
                sys.exit(1)

    return seq_ini

def check_renumber(NativeSeq, gappedSeq, Model, flank_size):
    '''
    checking the C- and N-terminals, cannot be modeled by DaReUS-Loop
    in case of terminal missing residues, the sequence and pdb are shrinked to the
    first set of consecutive 4 residues at each side with both structure and sequence
    '''
    Nter = 0; Cter = len(gappedSeq)-1
    while gappedSeq[Nter] == '-':
        Nter += 1
    while gappedSeq[Cter] == '-':
        Cter -= 1

    if Nter > 0 :
        progress("Warning! Ignoring the N-terminal gap of size %d." %Nter)

    if (len(gappedSeq) - Cter - 1) > 0:
        progress("Warning! Ignoring the C-terminal gap of size %d." %(len(gappedSeq) - Cter - 1))

    gaped_seq = gappedSeq[Nter : (Cter+1)]
    ungaped_seq = NativeSeq[Nter : (Cter+1)]

    if '-' in gaped_seq[0:flank_size]:
        progress("Error! The first %d residues must be present in the PDB!" %flank_size)
        sys.exit(1)

    if '-' in gaped_seq[(len(gaped_seq) - flank_size) : len(gaped_seq)]:
        progress("Error! The last %d residues must be present in the PDB!" %flank_size)
        sys.exit(1)

    gapped_str = Model
    start_num = int(gapped_str[0].rNum())
    if start_num != 1:
        for i in range(len(gapped_str)):
            ind_ini = int(Model[i].rNum())
            gapped_str[i].rNum(str(ind_ini - start_num + 1))

    return ungaped_seq, gaped_seq, gapped_str


## prepare input files
def prep_files(model_dir, protSeq, modeling_mode, flank_size, def_min_loop_size, def_max_loop_size, def_max_loop_num):
    ModelPDB = PDB.PDB(model_dir + "modified_input_protein.pdb")
    ### find loop positions
    if modeling_mode == "Modeling":
        Model_seq = ModelPDB.aaseq()
        NativeSeq = protSeq
        gappedSeq = parsing_str(ModelPDB, Model_seq, protSeq, flank_size)
    else:
        NativeSeq = ModelPDB.aaseq()
        gappedSeq = parsing_seq(ModelPDB, NativeSeq, protSeq)
    ### verifying the input sequence and structure ############
    '''
    if len(NativeSeq) != len(gappedSeq):
        progress("Error! There is a mismatch between the sequence and the structure you provided! Please make sure of the input data!")
        sys.exit(1)
    '''
    ### check the terminals and renumber the pdb if needed ####
    ungaped_seq, gaped_seq, gapped_str = check_renumber(NativeSeq, gappedSeq, ModelPDB, flank_size)
    ##### identifying all the loops
    Gap_Start, Gap_End, before_gap, LoopSeq, nbLoops = find_gap(gaped_seq, ungaped_seq, modeling_mode, flank_size)
    if nbLoops < 1:
        if modeling_mode == "Modeling":
           progress("Error! There is no gap in the input PDB file!")
        else:
           progress("Error! There is no gap in the input sequence file!")
        # sys.exit(1)
        return 0

    if nbLoops > def_max_loop_num:
        progress("Error! There are more than %d loops to be modeled!" %def_max_loop_num)
        # sys.exit(1)
        return nbLoops

    for j in range(nbLoops):
        ## ignoring gaps of size 2
        if len(LoopSeq[j]) < def_min_loop_size:
            progress("Warning! Minimum loop size is %d. Ignoring one loop of size %d." %(def_min_loop_size, len(LoopSeq[j])))
            #sys.exit(1)
        ## ignoring to model long loops with more than 30 residues
        if len(LoopSeq[j]) > def_max_loop_size:
            progress("Warning! Maximum loop size is %d. Ignoring one loop of size %d." %(def_max_loop_size, len(LoopSeq[j])))
            #sys.exit(1)
    return nbLoops

def check_seq(targetAddr, target_seq):
    seq_file = targetAddr + target_seq
    seqq = Fasta.fasta(seq_file)
    if len(seqq) == 0 :
       progress("Error! Please provide a standard fasta file!")
       sys.exit(1)

    model_seq = seqq[seqq.ids()[0]].data['s']
    count_lowercase = 0
    gaps = 0
    for ii in range(len(model_seq)):
        if model_seq[ii] in AAs3:
            count_lowercase += 1
        if model_seq[ii] not in AAs2:
            gaps += 1
        if model_seq[ii] not in AAs:
            progress("Error! Please provide standard amino acids for the sequence! %s is not valid. (Note that only \'-\' and lower-case letters in the sequence are interpreted as gaps)" %model_seq[ii])
            sys.exit(1)

    if count_lowercase == len(model_seq) and gaps == len(model_seq):
        progress("Warning! The sequence file contains only lower-case letters and no gaps. They are automatically converted to upper-case letters!")
        model_seq = model_seq.upper()

    elif count_lowercase > 0:
        progress("Warning! lower-case letters in the sequence are interpreted as gaps!")

    if (gaps - count_lowercase) == len(model_seq):
        progress("Error! The sequence file contains only gaps! (Note that lower-case letters in the sequence are interpreted as gaps)")
        sys.exit(1)

    return model_seq

def func1(DFLT_HOME,targetAddr,target,pdbdir,pdbName,BCth,RigidityTh,RMSDth,rmode,out_label,nb_all_dp,docker_img):
        ##### BCLoopSearch ###################################################
    progress("2/%d: BCLoopSearch" % nb_all_dp)
    cmd = DFLT_HOME + "BCLoopSearch.py"
    args = ['--wpath', targetAddr,
            '--target', target,
            '--pdb_path', pdbdir,
            '--pdb_name', pdbName,
            '--bc_min' , str(BCth),
            '--rigidity', str(RigidityTh),
            '--rmsd_max', str(RMSDth),
            '--l', out_label,
            '--run_mod', run_mod]
    Loop_info = loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    nbLoop = len(Loop_info) - 1
    if nbLoop <= 0:
        progress("No gap is detected!")
        sys.exit(1)
    cluster.runTasks(
        cmd, args, tasks = nbLoop, tasks_from = 1, job_opts = "-c 8",
        environment_module = docker_img, job_name="bcloopsearch", log_prefix="bcloopsearch"
    )
    try:
        os.system("cat bcloopsearch.*.log > log_bcloopsearch.all.log")
        os.system("rm bcloopsearch.*.log")
    except:
        pass

    ##### Clustering ###################################################
    progress("3/%d: clustering" % nb_all_dp)
    cmd = DFLT_HOME + "Clustering.py"
    cluster.runTasks(cmd, args, tasks = nbLoop, tasks_from = 1, environment_module = docker_img, job_name="clustering", log_prefix="wrapper_dareus_all")
    try:
        os.system("cat clustering.*.log > log_clustering.all.log")
        os.system("rm clustering.*.log")
    except:
        pass
    return

def func2(target,model_dir,nb_all_dp):
    PPP_name = "target_seq"
    fst_name = model_dir + PPP_name + ".fst"
    progress("4/%d: Conformational profiles" % nb_all_dp)
    cmd = "PyPPP3Exec"
    args = ['-s', fst_name,
            '-l', PPP_name,
            '-v',
            '--2012']
    os.chdir(model_dir)
    cluster.runTasks(cmd, args, environment_module = "DOCKER_PYPPP3", log_prefix = "wrapper_dareus_all")
    try:
        os.system("cat ppp3.*.log > log_ppp3.all.log")
        os.system("rm ppp3.*.log")
    except:
        pass

    os.chdir(targetAddr)
    return

def JSD_score(DFLT_HOME,Prof_dir,targetAddr,LoopSeq,target,flank_size,run_mod,loop_dir, Loop_name,docker_img):
    ##### Measuring JSD ###################################################
    #progress("5/10: Measuring Jenson Shannon distances")
    cmd = DFLT_HOME + "PyPPP_score.py"
    args = ['--ppp_path', Prof_dir,
            '--wpath', targetAddr,
            '--loop_seq', LoopSeq,
            '--target', target,
            '--loop_path', loop_dir,
            '--flank_sze', str(flank_size),
            '--run_mod', run_mod]
    BCLoopResFile = loop_dir + "/BCSearch/BCLoopCluster_Final_BC.txt"
    if not os.path.isfile(BCLoopResFile) or os.path.getsize(BCLoopResFile) <= 0 :
        progress("Warning! The database does not contain any protein fragment where the flanks are compatible with the loop (%s) that is to be modeled!\n" %Loop_name)
    else:
        BCLoopRes = open(BCLoopResFile, "r").readlines()
        nbCandid = min(MAX_JOB, len(BCLoopRes))
        job_name = "PyPPP-" + Loop_name
        cluster.runTasks(cmd, args, tasks = nbCandid, tasks_from = 1, environment_module = docker_img, job_name=job_name, log_prefix="wrapper_dareus_all")
    return

#####  run_mode == local ####################################################
def PyPPP_local(run_docker, targetAddr, target, flank_size, nbCPU, loop_dir, Prof_dir, LoopSeq):
    command = "%s/PyPPP_score.py --wpath %s --target %s --flank_sze %d --run_mod local --np %d --loop_path %s --ppp_path %s --loop_seq %s" %(run_docker, targetAddr, target, flank_size, nbCPU, loop_dir, Prof_dir, LoopSeq)
    os.system(command)

def oscar_local(candid_dir, data):
    command = "docker run -v $(pwd):$(pwd):z -w $(pwd) oscar oscarstar %s" %(data,)
    os.system(command)
    return

def gro_local(gromacs_path, candid_dir, model, name):
    # command = "%s/minimize_pdb_and_cyclic.py -f %s -n %s -dir . -keep" %(gromacs_path, model, name)
    command = "%s/minimize_cyclic_pep.py -f %s -n %s -dir . -keep" %(gromacs_path, model, name)
    os.system(command)
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                                            action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--mode", dest="mode", help="Modeling or Advanced or Re-Modeling",
                                            action="store", default = None)

    parser.add_argument("--model_str", dest="model_str", help="Modeling: gaped structure, Remodeling: initial model",
                                            action="store", default=None)

    parser.add_argument("--model_seq", dest="model_seq", help="Modeling: full structure, Remodeling: gapped sequence",
                                            action="store", default=None)

    parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
                                            action="store", default="none")

    parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
                                            action="store", default=DFLT_NPROC)

    parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters (%s)" % DFLT_PDB_CLS_PATH,
                                            action="store", default=DFLT_PDB_CLS_PATH)

    parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB (%s)" % DFLT_PDB_PATH,
                                            action="store", default=DFLT_PDB_PATH)

    parser.add_argument("--pdb_name", dest="pdb_name", help="name of indexed PDB (%s)" % DFLT_PDB_NAME,
                                            action="store", default=DFLT_PDB_NAME)

    parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum)",
                                            action="store", default=DFLT_BC)

    parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum)",
                                            action="store", default=DFLT_RIGIDITY)

    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                                            action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)",
                                            action="store", default=DFLT_RMSD)

    parser.add_argument("--min_loop_sze", dest="min_loop_sze", type = int, help="Minimal loop size",
                                            action="store", default=DFLT_MIN_LOOP_SZE)

    parser.add_argument("--max_loop_sze", dest="max_loop_sze", type = int, help="Maximal loop size",
                                            action="store", default=DFLT_MAX_LOOP_SZE)

    parser.add_argument("--max_loop_num", dest="max_loop_num", type = int, help="Maximal number of loops",
                                            action="store", default=DFLT_MAX_LOOP_NUM)

    parser.add_argument("--minimize", dest="minimize", type = int, help="Run Gromacs minimization, 0 or 1",
                                            action="store", default=DFLT_MINIMIZE)

    parser.add_argument("--korp", dest="korp", type = int, help="Calculate KORP scores, 0 or 1",
                                            action="store", default=DFLT_KORP)

    parser.add_argument("--docker_img", dest="docker_img", type = str, help="Name of the DaReUS-Loop Docker image, for drun",
                                            action="store", default=DFLT_DOCKER_IMAGE)

    parser.add_argument("--run_mod", dest="run_mode", help='running mode: one of "mobyle", "cluster", "local" or "dry")(%s)' % DFLT_MODE,
                                            action="store", default=DFLT_MODE)

    parser.add_argument("--prof_path", dest="prof_path", help="path to profiles (%s)" % DFLT_PROF_PATH,
                                            action="store", default=DFLT_PROF_PATH)

    parser.add_argument("--ppp_path", dest="ppp_path", help="path to ppp3 (%s)" % DFLT_HOME_PPP,
                                            action="store", default=DFLT_HOME_PPP)

    parser.add_argument("--bank_local_path", dest="bank_local_path", help="path to data banks (%s)" %DFLT_BANK,
                                            action="store", default=DFLT_BANK)

    parser.add_argument("--demo", dest="demo", help="selecting the demo mode for either \"ReModeling\", \"Modeling\" or \"advance_Modeling\"",
                                            action="store", default=None)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                                            action="store", default=DFLT_LBL)

    #parser.add_argument("--advanced", dest="advanced", type = int, help="Modeling loops independently!",
    #                                        action="store", default=DFLT_ADV)

    options = parser.parse_args()
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    if options.pdb_cls_path[-1] != "/":
        options.pdb_cls_path = options.pdb_cls_path + "/"
    if options.pdb_path[-1] != "/":
        options.pdb_path = options.pdb_path + "/"
    print(print_options(options))


    # input
    targetAddr        = options.work_path
    modeling_mode     = options.mode
    target            = options.model_str
    target_seq        = options.model_seq
    pdbID             = options.pdb_id
    run_mode          = options.run_mode
    docker_img        = options.docker_img
    out_label         = options.Lbl
    select_demo       = options.demo
    def_min_loop_size = options.min_loop_sze
    def_max_loop_size = options.max_loop_sze
    def_max_loop_num  = options.max_loop_num
    ### for running on a local computer
    NBPROCESS         = options.nproc
    clust_addr        = options.pdb_cls_path
    pdbdir            = options.pdb_path
    pdbName           = options.pdb_name
    Prof_dir          = options.prof_path #"/home/ykarami/Documents/banks/PyPPP/PyPPP/"
    PyPPP3_dir        = options.ppp_path  #"/home/ykarami/Documents/SAFrag/PyPPP3/"
    bank_local        = options.bank_local_path
    ### advanced options
    #advanced_run     = options.advanced
    BCth              = options.bc_min
    RigidityTh        = options.rigidity
    flank_size        = options.flank_sze
    RMSDth            = options.rmsd_max

    ### visualization
    jobId = os.path.basename(os.getcwd())
    serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]
    wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

    ### reading the input sequences
    targetAddr = os.getcwd() + "/"
    model_dir = mk_dir(targetAddr + PROT_DIR)

    ### DEMO mode
    if select_demo is not None:
            if select_demo == "ReModeling":
                    target = "model-9999-0001.pdb"
                    target_seq = "model_gap.fasta"
                    modeling_mode = "ReModeling"

            elif select_demo == "Modeling":
                    target = "model-gapped.pdb"
                    target_seq = "model_no_gap.fasta"
                    advanced_run = 0
                    modeling_mode = "Modeling"

            elif select_demo == "advance_Modeling":
                    target = "model-gapped.pdb"
                    target_seq = "model_no_gap.fasta"
                    advanced_run = 1
                    modeling_mode = "Modeling"
            else:
                    sys.stderr.write("Error! Wrong demo mode is selected!\n")
            pdbID = "4q5t"
            os.system("cp " + DFLT_DEMO_PATH + target + " " + targetAddr)
            os.system("cp " + DFLT_DEMO_PATH + target_seq + " " + targetAddr)

    ### modeling options
    if modeling_mode == "Advanced":
       advanced_run = 1
       modeling_mode = "Modeling"
    else:
       advanced_run = 0

    '''
    if advanced_run == 1 and modeling_mode == "ReModeling":
        progress("Error! With advanced modeling enabled, the modeling mode must be 'Modeling', not 'ReModeling'")
        sys.exit(1)
    '''

    if advanced_run == 1 or modeling_mode == "ReModeling":
        nb_all_dp = 14
    else:
        nb_all_dp = 15

    if run_mode in ("cluster", "mobyle"):
        ######################################################################
        '''
        RUNNING ON THE CLUSTER USING DOCKER IMAGE
        '''
        ######################################################################
        import cluster.cluster as cluster
        progress = cluster.progress
        if run_mode == "mobyle":
            # Initialize templates
            from jinja2 import Environment, PackageLoader
            jinja_env = Environment(loader = PackageLoader('DaReUS-Loop','templates'))
    else:
        def progress(*args):
            print(*args, file=sys.stderr)


    ##### Prep Inputs and Remove Homologs ################################
    progress("Please bookmark this page to have access to your results!")
    progress("1/%d: Verifying input files" % nb_all_dp)
    ### check the input pdb

    model_seq = check_seq(targetAddr, target_seq)
    modify_in_pdb(model_dir, targetAddr, target)
    nbLoops = prep_files(model_dir, model_seq, modeling_mode, flank_size, def_min_loop_size, def_max_loop_size, def_max_loop_num)

    if (nbLoops < 1) or (nbLoops > def_max_loop_num):
        os.system("cp %s %s_final_models.pdb" % (target, out_label))
        sys.exit(0)

    if run_mode == "dry":
        progress("Input was verified successfully, exiting now...")
        sys.exit(0)

    if run_mode in ("cluster", "mobyle"):
        run_mod = "cluster"

        cmd = DFLT_HOME + "Prep_input.py"
        args = ['--wpath', targetAddr,
                '--flank_sze', str(flank_size),
                '--model_str', target,
                '--model_seq', model_seq,
                '--mode', modeling_mode,
                '--pdb_id', pdbID,
                '--l', out_label,
                '--run_mod', run_mod,
                '--cls_path', clust_addr,
                '--min_loop_sze', str(def_min_loop_size),
                '--max_loop_sze', str(def_max_loop_size) ]
        cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
        if not os.path.isfile(targetAddr + out_label + "_loops.log"):
            progress("There is a mismatch between the sequence and the structure you provided! Please make sure of the input data!")
            sys.exit(1)
        ##### BCLoopSearch and PyPPP in parallel #############################
        func1(DFLT_HOME,targetAddr,target,pdbdir,pdbName,BCth,RigidityTh,RMSDth,run_mod,out_label,nb_all_dp,docker_img)
        func2(target,model_dir,nb_all_dp)
        '''
        threads = []
        p = mp.Process(target = func1, args = (DFLT_HOME,targetAddr,target,pdbdir,pdbName,BCth,RigidityTh,RMSDth,run_mod,out_label,))
        p.start()
        threads.append(p)
        p = mp.Process(target = func2, args = (target,model_dir,))
        p.start()
        threads.append(p)
        for t in threads:
                t.join()
        '''
        ##### Measuring JSD ##################################################
        Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
        nbLoop = len(Loop_info) - 1
        if nbLoop <= 0:
            progress("No gap is detected!")
            sys.exit(1)
        nbHit = 0
        for k in range(nbLoop):
            LoopFileLine = Loop_info[k+1].split()
            Loop_name = "Loop" + LoopFileLine[0] + "_" + LoopFileLine[1]
            BCLoopRes = model_dir + Loop_name + "/BCSearch/BCLoopCluster_Final_BC.txt"
            if not os.path.isfile(BCLoopRes) or os.path.getsize(BCLoopRes) <= 0 :
                progress("Warning! The database does not contain any protein fragment where the flanks are compatible with the loop (%s) that is to be modeled!\n" %Loop_name)
            else:
                BCLoopResFile = open(BCLoopRes,'r').readlines()
                progress("Found %d hits for %s" %(len(BCLoopResFile), Loop_name))
                nbHit += len(BCLoopResFile)
        if nbHit == 0:
            sys.exit(1)

        progress("5/%d: Measuring Jensen Shannon distances" % nb_all_dp)
        threads = []
        for k in range(nbLoop):
            LoopFileLine = Loop_info[k+1].split()
            LoopSeq = LoopFileLine[1]
            Loop_name = "Loop" + LoopFileLine[0] + "_" + LoopSeq
            Loop_dir = model_dir + Loop_name + "/"
            p = mp.Process(target=JSD_score, args=(DFLT_HOME,Prof_dir,targetAddr,LoopSeq,target,flank_size,run_mod,Loop_dir,Loop_name,docker_img))
            p.start()
            threads.append(p)
        for t in threads:
            t.join()
        ### chosing top candidates independetly or with respect to other loops
        nb_step = 6
        if advanced_run == 1 or modeling_mode == "ReModeling":
            ###>>>independet
            ###### top 50 candidates #####################################################
            progress("%d/%d: selecting top candidates per loop" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "select_top.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label,
                    '--run_mod', run_mod]
            Loop_info = loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
            nbLoop = len(Loop_info) - 1
            cluster.runTasks(cmd, args, tasks = nbLoop, tasks_from = 1, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
            ###### Prepare candidates for minimization ###################################
            progress("%d/%d: preparing top candidates for minimization" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "Prep_candidates.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label,
                    '--run_mod', run_mod]
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
        else:
            ###>>>dependet
            ###### top 100 candidates ####################################################
            progress("%d/%d: selecting first set of candidates per loop" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "select_candidates.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label,
                    '--top 100'
                    ###'--run_mod', run_mod
            ]
            Loop_info = loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
            ### find clashes between top candidates ############################
            progress("%d/%d: clash detection between top loop candidates" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "detect_clashes.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label]
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
            ### choose a reference structure #####################################
            progress("%d/%d: choosing a reference structure" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "find_consensus.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label]
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
            ##### Candid Prep ####################################################
            progress("%d/%d: selecting top candidates per loop" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "select_top_10_per_Loop.py"
            args = ['--wpath', targetAddr,
                    '--flank_sze', str(flank_size),
                    '--l', out_label]
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
            nb_step += 1
        ##### Oscar-Star #####################################################
        progress("%d/%d: positioning linker side chains" %(nb_step,nb_all_dp))
        cmd = "eval oscarstar"
        out_dir = model_dir + "Candidates/"
        pdb_data = out_dir + "data"
        pdb_read = open(pdb_data, 'r').readlines()
        nbCandid = len(pdb_read)
        if nbCandid <= 1:
            progress("Error! No candidate was found!")
            sys.exit(1)
        os.chdir(out_dir)
        args = ['$(awk NR==$SLURM_ARRAY_TASK_ID ' + pdb_data + ')', ]
        cluster.runTasks(cmd, args, tasks = nbCandid, tasks_from = 1, environment_module = "oscar-star/1.0-rpbs", log_prefix = "wrapper_dareus_all")
        nb_step += 1
        #### gromacs ########################################################
        if options.minimize > 0:
            os.chdir(targetAddr)
            progress("%d/%d: minimization" %(nb_step,nb_all_dp))
            cmd = "eval minimize_pdb_and_cyclic.py"
            gromacs_dir = model_dir + 'Candidates/'
            input_list = gromacs_dir + "input_gromacs.list"
            name_list = gromacs_dir + "gromacs_names.list"
            nbCandid = len(open(name_list, 'r').readlines())
            if nbCandid <= 1:
                candidate("Error! No candidate was found!")
                sys.exit(1)
            os.chdir(gromacs_dir)
            args = ['-f', '$(awk NR==$SLURM_ARRAY_TASK_ID ' + input_list + ')',
                    '-n', '$(awk NR==$SLURM_ARRAY_TASK_ID ' + name_list + ')',
                    '-dir' , '.',
                    '-nt', str(2),
                    #'-m_steps', str(500),
                    '-keep', ' ']
            cluster.runTasks(cmd, args, tasks = nbCandid, tasks_from = 1, environment_module = DOCKER_GROMACS, log_prefix = "wrapper_dareus_all")
            nb_step += 1
        ##### Scoring ########################################################
        os.chdir(targetAddr)
        progress("%d/%d: Scoring the candidates" %(nb_step,nb_all_dp))
        cmd = DFLT_HOME + "scoring.py"
        args = ['--wpath', targetAddr,
                '--l', out_label,
                '--advanced', str(advanced_run),
                '--mode', modeling_mode,
                '--run_mod', run_mod]
        Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
        nbLoop = len(Loop_info) - 1
        cluster.runTasks(cmd, args, tasks = nbLoop, tasks_from = 1, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
        nb_step += 1
        ##### KORP ############################################################
        if options.korp > 0:
            progress("%d/%d: Measuring energy values (KORP potential)" %(nb_step,nb_all_dp))
            cmd = "eval korp.sh"
            loops_name = targetAddr + "input_model_name.log"
            input_dareus_model = open(loops_name,"w")
            Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
            nbLoop = len(Loop_info) - 1
            for ii in range(nbLoop):
                CandidFileLine = Loop_info[ii+1].split()
                Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
                input_dareus_model.write("%s\n" %(targetAddr + out_label + "_" + Loop_name + "_models.pdb"))
            input_dareus_model.close()
            args = [ '$(awk NR==$SLURM_ARRAY_TASK_ID ' + loops_name + ')',]
            cluster.runTasks(cmd, args, tasks = nbLoop, tasks_from = 1, environment_module = DOCKER_KORP, log_prefix = "wrapper_dareus_all")
            nb_step += 1
        ##### final reports ####################################################
        progress("%d/%d: Preparing the final energy report" %(nb_step,nb_all_dp))
        cmd = DFLT_HOME + "scoring_energy.py"
        args = ['--wpath', targetAddr,
                '--l', out_label,
                '--advanced', str(advanced_run),
                '--mode', modeling_mode,
                '--run_mod', run_mod]
        Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
        nbLoop = len(Loop_info) - 1
        cluster.runTasks(cmd, args, tasks = nbLoop, tasks_from = 1, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
        nb_step += 1
        ##### Generating combinatorial models ################################
        progress("%d/%d: Generating combinatorial models" %(nb_step,nb_all_dp))
        cmd = DFLT_HOME + "combine_models.py"
        args = ['--wpath', targetAddr,
                '--model_str', target,
                '--l', out_label,
                '--advanced', str(advanced_run),
                '--mode', modeling_mode,
                '--run_mod', run_mod]
        cluster.runTasks(cmd, args, tasks = 1, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
        nb_step += 1
        ##### Detecting clashes ##############################################
        if advanced_run == 1 or modeling_mode == "ReModeling":
            progress("%d/%d: detecting possible clashes between candidates" %(nb_step,nb_all_dp))
            cmd = DFLT_HOME + "Final_clashes.py"
            args = ['--wpath', targetAddr,
                    '--l', out_label]
            cluster.runTasks(cmd, args, environment_module = docker_img, log_prefix = "wrapper_dareus_all")
        #####################################################################
        nameing = out_label #targetAddr
        os.system("tar -czvf " + nameing + "_models.tgz " + nameing + "_Loop*_models.pdb")
        ##########
        model_names = []; residue_list = []
        Conf_file = open(targetAddr + out_label + "_Loop_Confidence.txt", "w")
        Conf_file.write("#Loop_name confidence_value confidence_level \n")
        Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
        nbLoop = len(Loop_info) - 1
        for ii in range(nbLoop):
            CandidFileLine = Loop_info[ii+1].split()
            Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
            summary_file = targetAddr + out_label + "_" + Loop_name + "_summary.txt"
            summary_file_read = open(summary_file,'r').readlines()
            topp_file = model_dir + "Candidates/" + Loop_name + "_top_models.list"
            top_file_read = open(topp_file,'r').readlines()
            if len(top_file_read) > 1 and len(summary_file_read) <= 1:
                progress("Error! Failed to generate results for %s! Please verify your input PDB format for GROMACS compatibility!\n" %(Loop_name))
                print("Error! Failed to generate results for %s! Please verify your input PDB format for GROMACS compatibility!\n" %(Loop_name))
                print(top_file_read)
                print(summary_file_read)
                sys.stdout.flush()
                #sys.exit(1)
            this_name = out_label + "_" + Loop_name + "_models.pdb"
            model_names.append(this_name)
            this_loop_size = int(CandidFileLine[2])
            Loop_start = int(CandidFileLine[3])
            residue_list.append([ (i + Loop_start) for i in range(this_loop_size)])
            this_loop_JSD = [float(i.split()[9]) for i in open(summary_file) if not i.startswith("#")]
            if len(this_loop_JSD) > 0:
                min_JSD = min(this_loop_JSD)
                if min_JSD <= 0.2:
                   conf_val = "high"
                else:
                   conf_val = "low"
            else:
                min_JSD = "-"
                conf_val = "no_candidates"
            Conf_file.write("%s %s %s\n" %(Loop_name, str(min_JSD), conf_val))
        Conf_file.close()

        if len(residue_list) > 0:
            if run_mode == "mobyle":
                formatViewer(model_names, residue_list, 'dareusloop.vis.html', wdir)

                # XXX Create a concatenated score file for raw display
                os.popen('cat *_scores.txt | head -n 1 && cat *_scores.txt | grep -v "^#" > all_loop_scores.txt')

#                # XXX *_summary.txt
#                summary_files = targetAddr + out_label + "_*_summary.txt" # * is for any loop name
#                summary_str = os.popen('cat %s' % summary_files).read()
#                summary_list = summary_str.split('\n')
#                summary_dict = {}
#                for line in summary_list:
#                    #if line[0] != '#':
#                    data_list = line.split(' ')
#                    summary_dict[data_list[0]] = data_list
#                formatTables(summary_dict, 'dareusloop.table.html', wdir)
#                # XXX


            else:
                for model_name, res_list in zip(model_names, residue_list):
                    print(model_name, res_list)

        ##### Convert .txt files to HTML tables ################################
        cmd = "/home/dsv2datatables.pl"
        separator = "space"

        # *_Loop_Confidence.txt
        args = [ "--dareus_output_dir %s" % targetAddr,
                 "--dareus_output_type Confidence",
                 "--output %s_Loop_Confidence.html" % out_label,
                 "--title %s Loop Confidence" % out_label,
                 "--separator %s" % separator,
                 "--project dareus",
                 "--label %s" % out_label,
               ]
        cluster.runTasks(cmd, args, tasks = 1, environment_module = "dsv2datatables", log_prefix = "wrapper_dareus_all")

        # *_scores.txt
        args = [ "--dareus_output_dir %s" % targetAddr,
                 "--dareus_output_type scores",
                 "--output %s_scores.html" % out_label,
                 "--title %s scores" % out_label,
                 "--separator %s" % separator,
                 "--project dareus",
                 "--label %s" % out_label,
               ]
        cluster.runTasks(cmd, args, tasks = 1, environment_module = "dsv2datatables", log_prefix = "wrapper_dareus_all")

        # *_summary.txt
        args = [ "--dareus_output_dir %s" % targetAddr,
                 "--dareus_output_type summary",
                 "--output %s_summary.html" % out_label,
                 "--title %s summary" % out_label,
                 "--separator %s" % separator,
                 "--project dareus",
                 "--label %s" % out_label,
               ]
        cluster.runTasks(cmd, args, tasks = 1, environment_module = "dsv2datatables", log_prefix = "wrapper_dareus_all")

        #####################################################################
        progress("Finished")

    elif run_mode == "local":
        ######################################################################
        '''
        RUNNING ON A LOCAL COMPUTER AND NOT USING THE DOCKER IMAGE
        '''
        ######################################################################
        ### prepare input data and remove homologs ###########################
        run_mod = run_mode

        command = "%s/Prep_input.py --wpath %s --model_str %s --model_seq %s --flank_sze %d --mode %s --pdb_id %s --cls_path %s --l %s --run_mod %s" % (run_docker, targetAddr, target, model_seq, flank_size, modeling_mode, pdbID, clust_addr, out_label, run_mod)
        os.system(command)

        ### run BCLoopSearch #################################################
        command = "%s/BCLoopSearch.py --wpath %s --target %s --run_mod %s --pdb_path %s --pdb_name %s --bc_min %f --rigidity %f --rmsd_max %f --l %s" %(run_docker, targetAddr, target, run_mod, pdbdir, pdbName, BCth, RigidityTh, RMSDth, out_label)
        os.system(command)

        ### run clustering ###################################################
        command = "%s/Clustering.py --wpath %s --target %s --run_mod %s --pdb_path %s --pdb_name %s --bc_min %f --rigidity %f --rmsd_max %f --l %s" %(run_docker, targetAddr, target, run_mod, pdbdir, pdbName, BCth, RigidityTh, RMSDth, out_label)
        os.system(command)

        ### run PyPPP for the target sequence ################################
        command = "%s/PyPPP_prep.py --wpath %s --target %s --pyppp_path %s" %(run_docker, targetAddr, target, PyPPP3_dir)
        os.system(command)
        os.system("chmod +x " + targetAddr + "runPyPPP.sh")
        os.system(targetAddr + "runPyPPP.sh")
        os.system("rm " + targetAddr + "runPyPPP.sh")

        ### measure JSD for all the hits #####################################
        Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
        nbLoop = len(Loop_info) - 1
        if nbLoop <= 0:
            sys.stderr.write("No gap is detected!")
            sys.exit(1)
        threads = []
        for k in range(nbLoop):
            nbCPU = int(NBPROCESS / nbLoop)
            LoopFileLine = Loop_info[k+1].split()
            LoopSeq = LoopFileLine[1]
            loop_dir = model_dir + "Loop" + LoopFileLine[0] + "_" + LoopSeq + "/"
            p = mp.Process(target=PyPPP_local, args=(run_docker,targetAddr,target,flank_size,nbCPU,loop_dir,Prof_dir,LoopSeq,))
            p.start()
            threads.append(p)
        for t in threads:
            t.join()

        if advanced_run == 1 or modeling_mode == "ReModeling":
            ###>>>independent
            ###### top 50 candidates ##################################################
            command = "%s/select_candidates.py --wpath %s --flank_sze %d --l %s  --top 50" %(run_docker, targetAddr, flank_size, out_label)
            os.system(command)

            ###### Prepare candidates for minimization ################################
            command = "%s/Prep_candidates.py --wpath %s --flank_sze %d --l %s --run_mod %s" %(run_docker, targetAddr, flank_size, out_label, run_mod)
            os.system(command)
        else:
            ### select top 100 candidates per loop ####################################
            command = "%s/select_candidates.py --wpath %s --flank_sze %d --l %s --top 100 --check_clashes" %(run_docker, targetAddr, flank_size, out_label)
            os.system(command)

            ### find clashes between those candidates ############################
            command = "%s/detect_clashes.py --wpath %s --flank_sze %d --l %s " %(run_docker, targetAddr, flank_size, out_label)
            os.system(command)

            ### choose a reference structure #####################################
            command = "%s/find_consensus.py --wpath %s --flank_sze %d --l %s " %(run_docker, targetAddr, flank_size, out_label)
            os.system(command)

            ### chose top 20 candidates for each loop that do not have clashes with the reference structure
            command = "%s/select_top_10_per_Loop.py --wpath %s --flank_sze %d --l %s" %(run_docker, targetAddr, flank_size, out_label)
            os.system(command)

        #### oscar-star over the top candidates ##############################
        candid_dir = model_dir + "Candidates/"
        oscar_list = open(candid_dir + "data", "r").readlines()
        nbCandid = len(oscar_list)
        os.chdir(candid_dir)
        pool = mp.Pool()
        for i in range(nbCandid):
            data = oscar_list[i].split("\n")[0]
            pool.apply_async(oscar_local, args=(candid_dir, data,))
        pool.close()
        pool.join()

        #### gromacs minimization ############################################
        if options.minimize > 0:
            candid_dir = model_dir + "Candidates/"
            input_list_read = open(candid_dir + "input_gromacs.list", "r").readlines()
            name_list_read = open(candid_dir + "gromacs_names.list", "r").readlines()
            nbCandid = len(input_list_read)
            os.chdir(candid_dir)
            pool = mp.Pool()
            for i in range(nbCandid):
                model = input_list_read[i].split("\n")[0]
                name = name_list_read[i].split("\n")[0]
                pool.apply_async(gro_local, args=(gromacs_path, candid_dir, model, name,))
            pool.close()
            pool.join()

        ### final selection of top 10 candidates per loop ####################
        os.chdir(targetAddr)
        command = "%s/scoring.py --wpath %s --l %s --run_mod %s --advanced %d --mode %s" %(run_docker, targetAddr, out_label, run_mod, advanced_run, modeling_mode)
        os.system(command)

        ### clashes between candidates ######################################
        if advanced_run == 1 or modeling_mode == "ReModeling":
            command = "%s/Final_clashes.py --wpath %s --l %s --run_mod %s" %(run_docker, targetAddr, out_label)
        os.system(command)
    else:
        raise Exception("Unknown mode %s" % run_mode)

    ### clear the intermediate files  generated by DaReUS-Loop
    #os.system("rm -rf " + model_dir + "\n")
