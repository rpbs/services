#! /usr/bin/env python
"""
Adapted from co_alignment_vs_UP_oligomer.py
Created on 2016-10-27
Updated on 2017-12-10

@author: Jinchao Yu

Classes to search for MSAs and to reduce the number of sequences in PPI4DOCK's coMSAs to max 40 
sequences and 10 sequences
Last modified: CQ Nov. 2020
"""
import os
import sys
import re
import tempfile
import shutil
import optparse
import time
import pkg_resources
from collections import defaultdict

# for python version compatibility
try:
    import commands
except:
    import subprocess as commands
try:
    import ConfigParser
except:
    import configparser as ConfigParser

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini")) # config file in the same folder as this script

import InterEvDock3.tools # general functions
import cluster.cluster as cluster

# dockers
DOCKER_MAFFT = config.get('environments','mafft')
DOCKER_HH = config.get('environments','hhsuite')
DOCKER_BLAST = config.get('environments','blast')

# database
BLAST_UP_DB = config.get('databases','blast_up_db') # "/scratch/banks/uniprot_sprot/current/blast/uniprot_sprot /scratch/banks/uniprot_trembl/current/blast/uniprot_trembl"

# programmes
REFORMAT = config.get('programs','reformat')
ALIGNHITS = config.get('programs','alignhits')
HHFILTER = config.get('programs','hhfilter')
MAFFT = config.get('programs','mafft')

# variable defaults
DEF_COV = config.get('defaults','DEF_COV') # 75 % min coverage
DEF_ID = config.get('defaults','DEF_ID') # 30 % min id
DEF_TOPN = config.get('defaults','DEF_TOPN') # 100 number of top sequences in alignments to be kept

# BLAST_UP_PARSE_IDS_DB is not required for InterEvDock implementation
# BLAST_UP_PARSE_IDS_DB = "'/home/jy239873/databases/uniprot_blastdb_parse_seqids_2016-10-13/sp "+\
#                        "/home/jy239873/databases/uniprot_blastdb_parse_seqids_2016-10-13/tr'"
BLAST_UP_PARSE_IDS_DB = None


#
# block: tempfile
#
LOCAL_TMP_DIR_PATH = os.getcwd()
if not os.path.exists(LOCAL_TMP_DIR_PATH):
    try:
        os.mkdir(LOCAL_TMP_DIR_PATH)
    except:
        print("The program needs to create a tmp dir at $HOME/tmp/.")
        print("Creation failed.")
        raise IOError
if not os.access(LOCAL_TMP_DIR_PATH, os.W_OK):
    print("No writing right in %s"% LOCAL_TMP_DIR_PATH)
    raise IOError  

def make_tempdir(suffix_str='',prefix_str='',parent_dir=LOCAL_TMP_DIR_PATH):
    """ create a tempdir in parent_dir (def = $HOME/tmp). Return the created tempdir absolute path.
    Arguments:
    1. suffix_str: string for tempdir name's suffix part (def = '')
    2. prefix_str: string for tempdir name's prefix part (def = '')
    3. parent_dir: parent directory path (def = $HOME/tmp)
    Return:
    the created tempdir absolute path
    """
    tmpfile = tempfile.mkdtemp(suffix=suffix_str,prefix=prefix_str,dir=parent_dir)
    return os.path.abspath(tmpfile)

def make_tempfile(suffix_str='',prefix_str='',parent_dir=LOCAL_TMP_DIR_PATH):
    """ create a tempfile in parent_dir (def = $HOME/tmp). Return the created tempfile absolute path.
    File handle problem has been solved.
    Arguments:
    1. suffix_str: string for tempfile name's suffix part (def = '')
    2. prefix_str: string for tempfile name's prefix part (def = '')
    3. parent_dir: parent directory path (def = $HOME/tmp)
    Return:
    the created tempdir absolute absolute path
    """
    fd, tmpfile = tempfile.mkstemp(suffix=suffix_str,prefix=prefix_str,dir=parent_dir)
    os.close(fd)
    return os.path.abspath(tmpfile)

############### end block: tempfile ###############


class GenerateCoMSAs():

    def __init__(self,lst_fasta_abspath,redundancy_filter=90,group_MSA=None,if_do_separate_MSA=False, use_Full_length_seq=False,parallelise=False,verbose=False, coverage=75, seq_id=30):
        """following steps are executed successively:
        1. blast for each sequence against the database uniprot (sp+tr)
        2. reform blast results to alignment (MSA)
        3. get common non-redundant species
        4. re-align each MSA by mafft
        5. combine MSAs
        6. filter combined MSA by HHfilter using redundancy_filter (default: 90 (%))
        7. separate MSAs
        8. If group_MSA is given, re-combine final MSAs into groups
        """
        print(lst_fasta_abspath,redundancy_filter,group_MSA,if_do_separate_MSA, use_Full_length_seq,parallelise,verbose, coverage, seq_id)
        if not verbose:
            curdir = os.getcwd()
            tmp_workdir = make_tempdir("_co_ali_wd")
            os.chdir(tmp_workdir)
           
        nb_input = len(lst_fasta_abspath)    
        # 1 - blast
        
        for fasta_index in range(1,nb_input+1):
            shutil.copy(lst_fasta_abspath[fasta_index-1], "seq%d.fst" % fasta_index)

        if parallelise:
            cmd = "blastpgp"
            args = ["-i seq${SLURM_ARRAY_TASK_ID}.fst",
                    "-o seq${SLURM_ARRAY_TASK_ID}_UP.pbl",
                    "-j 1",
                    "-v 50000",
                    "-b 50000",
                    "-d \"%s\"" % BLAST_UP_DB,
                    "-a 8"]
            print(cmd+ " "+ " ".join(args))
            cluster.runTasks(cmd, args, job_opts = '-c 8', tasks = nb_input, tasks_from = 1, environment_module = DOCKER_BLAST, log_prefix = "generate_co_msas") # job_opts to specify other slurm commands
                                                                                                                   # here, we're using job arrays to execute all sequences at the same time and each on 8 ppns
            
            lst_out_pbl_path = [] # seq1_UP.pbl, seq2_UP.pbl ...
            for i in range(nb_input):
                lst_out_pbl_path.append("seq%d_UP.pbl"%(i+1))
              
        else:
            for i in range(nb_input):
                in_fasta_path = lst_fasta_abspath[i]
                out_pbl_path = "seq%d_UP.pbl"%(i+1) # such as seq1_UP.pbl, seq2_UP.pbl
                lst_out_pbl_path.append(out_pbl_path)
                cmd = 'blastpgp'
                args = ["-i "+ in_fasta_path,
                        "-o "+ out_pbl_path,
                        "-j 1",
                        "-v 50000",
                        "-b 50000",
                        "-d \"%s\"" % BLAST_UP_DB,
                        "-a 1"]
                print("blastpgp local using 1 CPU: "+cmd+" "+" ".join(args))
                cluster.runTasks(cmd, args, tasks_from = 1, environment_module = DOCKER_BLAST, log_prefix = "generate_co_msas")


        # 2 - blast to alignment
        lst_blast_fasta_file = [] # seq1_UP_alignhits.fasta, seq2_UP_alignhits.fasta ...
        lst_dict_UP_NR_org_id_cov_seq = [] # dict1_UP_NR_org_id_cov_seq, dict2_UP_NR_org_id_cov_seq
        # dict_UP_NR_org_id_cov_seq:
        # key: Up (uniprot code) 
        # value: [NR_org, id, cov, seq] # NR_org: organism names are non-redundant
        
        for i in range(nb_input):
            blast_fasta_file = "seq%d_UP_alignhits.fasta"%(i+1)
            lst_blast_fasta_file.append(blast_fasta_file)
            args = ['-i '+lst_out_pbl_path[i],
                    '-o '+blast_fasta_file,
                    '-e 1E-4',
                    '-cov %.1f'%(coverage),
                    '-best',
                    '-qid %.1f'%(seq_id),
                    '-fas']
            print(ALIGNHITS + " " + " ".join(args))
            cluster.runTasks(ALIGNHITS, args, tasks_from = 1, environment_module = DOCKER_HH, log_prefix = "generate_co_msas")

            # read_UP_dict_fasta method assures that organism names are non-redundant
            dict_UP_NR_org_id_cov_seq = self.read_UP_dict_fasta(blast_fasta_file)
            lst_dict_UP_NR_org_id_cov_seq.append(dict_UP_NR_org_id_cov_seq)
        
            print("For seq%d, %d sequences (organism non-redundant) are found by blast"%(i+1,
                                                 len(lst_dict_UP_NR_org_id_cov_seq[i].keys())))
        
        # if use_Full_length_seq, then replace fasta sequences by FL sequences
        if use_Full_length_seq:
            try:
                new_lst_dict_UP_NR_org_id_cov_seq = [] #
                for i in range(nb_input):
                    tmp_multifasta = make_tempfile("tmp%d.fasta"%(i+1))
                    self.extract_seqs_from_blastdb_with_entry_adding_id_cov(lst_dict_UP_NR_org_id_cov_seq[i], tmp_multifasta, BLAST_UP_PARSE_IDS_DB)
                    # mafft
                    mafft_realigned_MSA = "mafft_realigned_MSA_%d.fasta"%(i+1)
                    args = ['--auto',
                            '--anysymbol',
                            tmp_multifasta,
                            "> " + mafft_realigned_MSA]
                    cluster.runTasks(MAFFT, args, tasks_from = 1, environment_module = DOCKER_MAFFT, log_prefix = "generate_co_msas")

                    new_dict_UP_NR_org_id_cov_seq = self.read_UP_dict_fasta(mafft_realigned_MSA)
                    new_lst_dict_UP_NR_org_id_cov_seq.append(new_dict_UP_NR_org_id_cov_seq)
                    print("Using full-length sequences...")
                    if os.path.exists(tmp_multifasta):
                        os.remove(tmp_multifasta)
                lst_dict_UP_NR_org_id_cov_seq = new_lst_dict_UP_NR_org_id_cov_seq
            except:
                pass
        
        # 3 - get common NR organisms
        # these lists are sorted by id% in descending order
        lst_lst_sorted_keys = self.get_common_org_keys_from_two_NR_org_dict(lst_dict_UP_NR_org_id_cov_seq)

        print("For %d input sequences, %d co-sequences exist."%(nb_input,
                                                                len(lst_lst_sorted_keys[0])))
        
        lst_mafft_NR_fasta_file = []
        for i in range(nb_input):
            NR_fasta_file = "NR_org_%d.fasta"%(i+1) # ex: NR_org_2.fasta
            mafft_NR_fasta_file = "NR_org_mafft_%d.fasta"%(i+1) # ex: "NR_org_mafft_1.fasta"
            lst_mafft_NR_fasta_file.append(mafft_NR_fasta_file)
            self.write_multiple_fasta_file(NR_fasta_file,lst_dict_UP_NR_org_id_cov_seq[i],lst_lst_sorted_keys[i])    
            args = ['--auto',
                    '--anysymbol',
                    NR_fasta_file,
                    "> " + mafft_NR_fasta_file]
            cluster.runTasks(MAFFT, args, tasks_from = 1, environment_module = DOCKER_MAFFT, log_prefix = "generate_co_msas")
        
        # 5 - combine
        # This is important for redundancy filtering
        combined_MSA_file = "combined_MSA.fasta"
        self.combine_some_MSA(lst_mafft_NR_fasta_file, combined_MSA_file)
        
        # 6 - redundancy-filter for the combined MSA
        filtered_combined_MSA_file = "filtered_combined_MSA.fasta"
        # here create an empty file
        FHo = open(filtered_combined_MSA_file,'w')
        FHo.write("")
        FHo.close()
        
        h,s = tools.read_fasta(combined_MSA_file)
        if len(h) == 0:
            shutil.copy(combined_MSA_file,filtered_combined_MSA_file)
        else:
            self.use_hhfilter(combined_MSA_file,filtered_combined_MSA_file, redundancy_filter)
        
        
        print("After redundancy filter, %d co-sequences exist after a filter by %.2f%% id."%(len(tools.read_fasta(filtered_combined_MSA_file)[0]),
                                                                                     redundancy_filter))
        ### 7 - separate the MSAs
        lst_final_out_MSA = [] # final_MSA1.fasta, final_MSA2.fasta, ...
        for i in range(nb_input):
            out_MSA_file = "final_MSA%d.fasta"%(i+1) # ex: final_MSA1.fasta
            lst_final_out_MSA.append(out_MSA_file)
        self.extract_separate_MSA_from_combined(filtered_combined_MSA_file, lst_final_out_MSA)
        
        for MSA in lst_final_out_MSA:
            self.extract_topN_seqs_in_multifasta(MSA, DEF_TOPN, MSA)
        
        ### 8 - combine final MSAs into several group is the option group_MSA is given
        lst_outfile_grouped_MSA = []
        if group_MSA:
            # Reorganize to parallelize this step !
            lst_groups = re.split(":",group_MSA)
            ind_gr = 0
            for gr in lst_groups:
                ind_gr += 1
                lst_final_MSA_file_in_group = []
                lst_final_INPUTSEQ_file_in_group = []
                for term in re.split(",",gr):
                    if len(term)>0:
                        try:
                            if int(term)>0 and int(term)<=nb_input:
                                lst_final_MSA_file_in_group.append(lst_final_out_MSA[int(term)-1])
                                lst_final_INPUTSEQ_file_in_group.append(lst_fasta_abspath[int(term)-1])
                            else:
                                raise IOError ("Error: Invalid group number: %s"%term)
                        except:
                            raise IOError ("Error: Invalid group number: %s"%term)
                out_regrouped_MSA_path = "regrouped_MSA%d.fasta"%ind_gr
                out_regrouped_INPUTSEQ_path = "regrouped_INPUTSEQ%d.fasta"%ind_gr
                print("%d-th group: "%ind_gr + ",".join(lst_final_MSA_file_in_group))
                self.combine_some_MSA(lst_final_MSA_file_in_group,out_regrouped_MSA_path)
                self.combine_some_MSA(lst_final_INPUTSEQ_file_in_group,out_regrouped_INPUTSEQ_path)
                lst_outfile_grouped_MSA.append(out_regrouped_MSA_path)
                lst_outfile_grouped_MSA.append(out_regrouped_INPUTSEQ_path)

        lst_separate_MSA_files = []
        
        if if_do_separate_MSA:
            # get only the most probable ortholog sequence by blast significance value
            # use dict1_UP_NR_org_id_cov_seq and dict2_UP_NR_org_id_cov_seq for separate MSA files
            for i in range(nb_input):
                separate_MSA_outfile = "separate_MSA%d.fasta"%(i+1)
                lst_separate_MSA_files.append(separate_MSA_outfile)
                tmp_sorted_fasta_file = make_tempfile("sorted_orgNR%d.fasta"%(i+1))
                tmp_filtered_fasta_file = make_tempfile("sorted_orgNR_NR_%d.fasta"%(i+1))
                dict_info = lst_dict_UP_NR_org_id_cov_seq[i]
                sorted_lst_key = sorted(dict_info.keys(),
                                        key = lambda x:(float(dict_info[x][1]),
                                                        float(dict_info[x][2])),
                                        reverse=True)
                
                self.write_multiple_fasta_file(tmp_sorted_fasta_file,dict_info,sorted_lst_key)
                 
                self.use_hhfilter(tmp_sorted_fasta_file,tmp_filtered_fasta_file, redundancy_filter)
                args = ['--auto',
                        '--anysymbol',
                        tmp_filtered_fasta_file,
                        "> " + separate_MSA_outfile]
                cluster.runTasks(MAFFT, args, tasks_from = 1, environment_module = DOCKER_MAFFT, log_prefix = "generate_co_msas")
                
        if not verbose:
            shutil.copy2(filtered_combined_MSA_file, curdir)
            for file_path in lst_final_out_MSA:
                shutil.copy2(file_path, curdir)
            for file_path in lst_outfile_grouped_MSA:
                shutil.copy2(file_path, curdir)
            for file_path in lst_separate_MSA_files:
                shutil.copy2(file_path, curdir)
            os.chdir(curdir)
            shutil.rmtree(tmp_workdir)
    
    def extract_topN_seqs_in_multifasta(self,infile_multifasta, topN, outfile_multifasta):
        """
        infile_multifasta: type str, input multifasta file path
        topN: type int, the number of top sequences to be extracted
        outfile_multifasta: type str, output multifasta file path. If it is the same with infile_multifasta,
                            then the input file will be overwritten.
        """
        topN = int(topN)
        (list_header,list_seq) = tools.read_fasta(infile_multifasta)
        if len(list_header) > topN:
            tools.write_fasta(list_header[:topN],list_seq[:topN],outfile_multifasta)
        else:
            if outfile_multifasta != infile_multifasta:
                shutil.copy(infile_multifasta, outfile_multifasta)

    def read_UP_dict_fasta(self,UP_fasta_file):
        """Input: Uniprot multiple fasta file 
        Return: dict of Uniprot name --> [org,identity,cov,sequence], this dict is organism non-redundant 
        """
        UP_name = []
        org_lst = []
        identity = []
        cov = []
        seqs = []
        FHi = open(UP_fasta_file, 'r')
        
        lines = FHi.readlines()
        index_seq = -1
        for line in lines:
            line = line.rstrip('\n')
            res = re.match(">.*",line)
            
            if res:
                res = re.match(">.*\|.*\|(\w+_(\w+))\(.*id=([\d\.]+)% cov=([\d\.]+)%",line)
                if res:
                    UP_name.append(res.group(1))
                    org_lst.append(res.group(2))
                    identity.append(res.group(3))
                    cov.append(res.group(4))
                    index_seq += 1
                    seqs.append("")
                else:
                    print("No UP name detected.",line)
            else:
                try:
                    seqs[index_seq] += re.sub("[\t ]","",line)
                except:
                    print("Fasta format error in coMSA read_UP_dict_fasta.")
        FHi.close()
        
        dict_org = defaultdict(list) # org -> list of ind
        for i in range(len(org_lst)):
            dict_org[org_lst[i]].append(i)
        # print(len(dict_org.keys()))
        # print(len(org_lst))
        NR_org_ind_lst = []
        for key_org in dict_org.keys():
            if len(dict_org[key_org]) == 1:
                NR_org_ind_lst.append(dict_org[key_org][0])
            else:
                best_ind = dict_org[key_org][0]
                for sub_ind in dict_org[key_org]:
                    better_id_cond = float(identity[sub_ind]) > float(identity[best_ind])
                    better_cov_cond = float(identity[sub_ind]) == float(identity[best_ind]) and\
                                      float(cov[sub_ind]) > float(cov[best_ind])
                    if better_id_cond or better_cov_cond:
                        best_ind = sub_ind
                NR_org_ind_lst.append(best_ind)
        # print(len(NR_org_ind_lst))
        dict_NR_org_id_cov_seq = defaultdict(list)
        for i in range(len(UP_name)):
            if i in NR_org_ind_lst:
                dict_NR_org_id_cov_seq[UP_name[i]] = [org_lst[i],identity[i],cov[i],seqs[i]]
        return dict_NR_org_id_cov_seq

    def get_common_org_keys_from_two_NR_org_dict(self,lst_dict_UP_NR_org_id_cov_seq):
        """get keys (Uniprot code) of common organisms sorted by average id% in descending order
        In the special case where only one alignment is concerned, keys of all organisms sorted by id%.
        Return:
        [sorted_common_key1, sorted_common_key2, ...]
        """
        nb_input_seq = len(lst_dict_UP_NR_org_id_cov_seq)
        if nb_input_seq == 1:
            # return [lst_sorted_keys] # sorted by id% in descending order
            return [[key for key in sorted(lst_dict_UP_NR_org_id_cov_seq[0],key=lambda x:lst_dict_UP_NR_org_id_cov_seq[0][x][1],reverse=True)]]
        else:
            set_comm_org = None
            for dict_UP_NR_org_id_cov_seq in lst_dict_UP_NR_org_id_cov_seq:
                set_org = set()
                for key in dict_UP_NR_org_id_cov_seq.keys():
                    set_org.add(dict_UP_NR_org_id_cov_seq[key][0])
                if set_comm_org is None:
                    set_comm_org = set_org
                else:
                    set_comm_org = set_comm_org & set_org
                    
            # key: organism name
            # value: list of keys (Uniprot code in each alignment)
            dict_k_org_v_lst_keys = defaultdict(list) 
            
            # key: organism name
            # value: average id
            dict_k_org_v_average_id = {}
            for org in set_comm_org:
                average_seq_id = 0
                for dict_UP_NR_org_id_cov_seq in lst_dict_UP_NR_org_id_cov_seq:
                    for key in dict_UP_NR_org_id_cov_seq.keys():
                        if dict_UP_NR_org_id_cov_seq[key][0] == org:
                            dict_k_org_v_lst_keys[org].append(key)
                            seq_id = dict_UP_NR_org_id_cov_seq[key][1]
                            average_seq_id += float(seq_id)
                            # when organism name is found, we can go to the next dict
                            break
                average_seq_id = float(average_seq_id)/float(nb_input_seq)
                dict_k_org_v_average_id[org] = average_seq_id
            
            lst_sorted_org = sorted(dict_k_org_v_average_id.keys(), 
                                    key=lambda x:dict_k_org_v_average_id[x],
                                    reverse=True)
            
            lst_sorted_comm_keys_for_each_MSA = []


            for i in range(nb_input_seq):
                sorted_keys_for_each_MSA = []
                for org in lst_sorted_org:
                    sorted_keys_for_each_MSA.append(dict_k_org_v_lst_keys[org][i])
                lst_sorted_comm_keys_for_each_MSA.append(sorted_keys_for_each_MSA)
                
            return lst_sorted_comm_keys_for_each_MSA

    def write_multiple_fasta_file(self,outfile, dict_org_id_cov_seq, key_lst):
        """This method is for local use for the function co_alignment_vs_UP
        """
        FHo = open(outfile,'w')
        for key in key_lst:
            FHo.write(">%s id=%s%% cov=%s%%\n"%(key,dict_org_id_cov_seq[key][1],dict_org_id_cov_seq[key][2]))
            FHo.write("%s\n"%dict_org_id_cov_seq[key][3])
        FHo.close()

    def combine_some_MSA(self,lst_fasta_MSAs, combined_fasta_MSA):
        """Combine some MSAs (multi-fasta format) into one fasta MSA, of which the new i-th sequence is 
        the fusion of all i-th sequences from input fasta MSAs.
        """
        lst_lst_headers = []
        lst_lst_seqs = []
        nb_MSA = len(lst_fasta_MSAs)
        for i in range(nb_MSA):
            lst_headers, lst_seqs = tools.read_fasta(lst_fasta_MSAs[i])
            lst_lst_headers.append(lst_headers)
            lst_lst_seqs.append(lst_seqs)
        
        # a small check for the nb of seqs
        nb_seqs_in_MSA1 = len(lst_lst_headers[0])
        if nb_MSA > 1:
            for i in range(1, nb_MSA):
                if len(lst_lst_headers[i]) != nb_seqs_in_MSA1:
                    print("Error: cannot combine. MSA sizes differ.")
                    return None
        
        FHo = open(combined_fasta_MSA,'w')
        for i in range(nb_seqs_in_MSA1):
            combined_header = ">"
            combined_seq = ""
            for j in range(nb_MSA):
                shortened_header = re.sub(",", " ", lst_lst_headers[j][i])
                if len(shortened_header)>10:
                    shortened_header = shortened_header[:30]
                combined_header += "%s MSA%d-seq%d (gapped length %d),"%(shortened_header, j+1,i+1,len(lst_lst_seqs[j][i]))
                combined_seq += lst_lst_seqs[j][i]
            combined_header += "\n"
            combined_seq += "\n"
            FHo.write(combined_header)
            FHo.write(combined_seq)
        FHo.close()

    def extract_separate_MSA_from_combined(self,combined_MSA, lst_out_MSA_file):
        """Only used for this module but not for generic purpose
        """
        headers, seqs = tools.read_fasta(combined_MSA)
        nb_MSAs = len(lst_out_MSA_file)
        if len(headers) == 0:
            for out_MSA_file in lst_out_MSA_file:
                FHo = open(out_MSA_file, 'w')
                FHo.write("")
                FHo.close()
        else:
            lst_len = [] # ex: 50,60,70 means 50 columns for the 1st MSA, 60 for the second, etc.
            res = re.search("gapped length", headers[0])
            if res:
                lst_split_headers = re.split(",",headers[0])
                for small_header in lst_split_headers:
                    res_small = re.search("\(gapped length (\d+)\)",small_header)
                    if res_small:
                        lst_len.append(int(res_small.group(1)))
            
            lst_FHo = []
            for j in range(nb_MSAs):
                FHo_each = open(lst_out_MSA_file[j], 'w')
                lst_FHo.append(FHo_each)
            
            # nb of MSAs
            for j in range(nb_MSAs):
                # nb of seqs in one MSA (same for all MSAs)   
                for i in range(len(headers)):
                    lst_headers_of_MSA = re.split(",",headers[i])
                    
                    # for seq j,
                    # start: len[0] + len[1] + .. + len[j-1]
                    # end: len[0] + len[1] + .. + len[j]
                    seq_start_index = 0
                    for x in range(j):
                        seq_start_index += lst_len[x]
                    seq_end_index = seq_start_index + lst_len[j]

                    lst_FHo[j].write(">%s\n"%lst_headers_of_MSA[j])
                    lst_FHo[j].write("%s\n"%seqs[i][seq_start_index:seq_end_index])
                
            for j in range(nb_MSAs):
                lst_FHo[j].close()

    def extract_seqs_from_blastdb_with_entry_adding_id_cov(self,dict_UP_NR_org_id_cov_seq, out_multifasta, database=BLAST_UP_PARSE_IDS_DB):
        """Caution: database should be built with the option parse_seqids if it is not a NCBI standard database 
        (ex: built from Uniprot fasta sequences). This database may be not useful for blasting because of bugs 
        of Blast software.
        """
        # get uniprot id in the order of BLAST id% and cov%
        lst_entry = sorted(dict_UP_NR_org_id_cov_seq.keys(),
                           key = lambda x:(float(dict_UP_NR_org_id_cov_seq[x][1]),
                                           float(dict_UP_NR_org_id_cov_seq[x][2])),
                           reverse=True)
        lst_seqid = [dict_UP_NR_org_id_cov_seq[up_id][1] for up_id in lst_entry]
        lst_cov = [dict_UP_NR_org_id_cov_seq[up_id][2] for up_id in lst_entry]
        cmd = "blastdbcmd"
        args = ["-db " + database,
                "-entry " + ",".join(lst_entry),
                "-out " + out_multifasta]
        print(cmd+" "+" ".join(args))
        cluster.runTasks(cmd, args, environment_module = DOCKER_BLAST, log_prefix = "generate_co_msas") 

        # add blast id% and cov%
        FHi = open(out_multifasta)
        lines = FHi.readlines()
        FHi.close()
        
        FHo = open(out_multifasta, 'w')
        index_seq = -1
        for line in lines:
            line = line.rstrip('\n')
            if line[0] == ">":
                index_seq += 1
                # only the first 160 characters are kept to avoid too long header
                FHo.write("%s id=%d%% cov=%d%%\n"%(line[:160],
                                                   int(lst_seqid[index_seq]),
                                                   int(lst_cov[index_seq])))
            else:
                FHo.write("%s\n"%line)
        FHo.close()
        
    def use_hhfilter(self,in_fasta,out_fasta,redundancy_filter):
        """Normally hhfilter takes a3m as input and output formats, however, a fasta file 
        is like a format a3m, just like we do "reformat fasta a3m -M 100"
        """
        # run hhfilter
        args = ["-i {}".format(in_fasta),
                "-o {}".format(out_fasta),
                "-id {:.2f}".format(redundancy_filter)]

        cluster.runTasks(HHFILTER, args, environment_module=DOCKER_HH, log_prefix = "generate_co_msas")
        if not os.path.exists(out_fasta):
          print("Error in GenerateCoMSAs: hhfilter failed for {}".format(in_fasta))
          raise Exception("Error in GenerateCoMSAs: hhfilter failed for {}".format(in_fasta))


class MergeMSAs():

    def __init__(self, list_seq_a, list_MSA_b, MSA_out):
        """
        Add a sequence to the top of a coMSA using Mafft --seed
        """
        list_seq_a = [os.path.abspath(seq_a) for seq_a in list_seq_a]
        list_MSA_b = [os.path.abspath(MSA_b) for MSA_b in list_MSA_b]
            
        if len(list_seq_a) > 1:
            # We concatenate together the list of MSA_as to get a single fasta file
            single_file_seq_a = make_tempfile(suffix = '_seqa.fst', prefix = 'tmp_')
            tools.combine_MSAs(list_seq_a, single_file_seq_a, add_gapped_lg=True)
            tmpa = True
        else:
            single_file_seq_a = list_seq_a[0]
            tmpa = False
            
        if len(list_MSA_b) > 1:
            # We concatenate together the list of MSA_bs to get a single fasta file       
            single_file_MSA_b = make_tempfile(suffix = '_MSAb.fst', prefix = 'tmp_')
            tools.combine_MSAs(list_MSA_b, single_file_MSA_b, add_gapped_lg=True)
            tmpb = True
        else:
            single_file_MSA_b = list_MSA_b[0]
            tmpb = False
        
        self.add_first_sequence(single_file_seq_a, single_file_MSA_b, MSA_out)
        
        if tmpa:
            os.system("rm %s"%(single_file_seq_a))
        if tmpb:
            os.system("rm %s"%(single_file_MSA_b))

    def add_first_sequence(self, first_seq_fasta, MSA_fasta, output):
        """
        Add a sequence to the top of the coMSA using Mafft --seed

        Input:
        first_seq_fasta:  str, fasta file of the sequence that needs to be added
        MSA_fasta:        str, coMSA fasta file

        Output:
        (headers,seqs):   ([str],[str]), a tuple with a list of the sequence headers 
                and a list of corresponding sequences
        """
        tmpout = make_tempfile()
        tmpin = False
        h,s = tools.read_fasta(MSA_fasta)
        if len(h) == 0:
            print("Error in MergeMSAs: given MSA is empty")
            raise Exception("Error in MergeMSAs: given MSA is empty")
        elif len(h) == 1:
            # run mafft without --seed
            tmpin = make_tempfile()
            h1,s1 = tools.read_fasta(first_seq_fasta)
            tools.write_fasta([h[0],h1[0]],[s[0],s1[0]],tmpin,ignore_gaps=False)
            args = ["--auto",
                    tmpin,
                    "> {}".format(tmpout)]
        else:
            args = ["--auto",
                    "--seed {}".format(MSA_fasta),
                    first_seq_fasta,
                    "> {}".format(tmpout)]
                
        cluster.runTasks(MAFFT, args, environment_module=DOCKER_MAFFT, log_prefix = "merge_msas")
        try:
          h,s = tools.read_fasta(tmpout)
        except:
          print("Error in ReduceCoMSA: mafft didn't work on {} and {}".format(MSA_fasta,first_seq_fasta))
          raise Exception("Error in ReduceCoMSA: mafft didn't work on {} and {}".format(MSA_fasta,first_seq_fasta))
        os.remove(tmpout)
        tools.write_fasta([hh.replace("_seed_","") for hh in h[-1:]+h[:-1]], s[-1:]+s[:-1], output)
        if tmpin:
            try:
                os.remove(tmpin)
            except:
                pass


class ReduceCoMSA():

  def __init__(self, workdir, MSAA, MSAB, outMSAA=None, outMSAB=None, diff=30, top_N=100, max_N=40, reorder=True, fastaA="", fastaB="", verbose=False):
    """
    Class to reduce co-MSAs using hhfilter (this script assumes that the first sequence in both coMSAs corresponds to the query 
    sequence and that sequences are ordered in decreasing sequence id with the query).

    workdir     str, name of working directory where files are created by default
    MSAA        str, name of fasta file containing the coMSA of partner A
    MSAB        str, name of fasta file containing the coMSA of partner B

    diff        int, starting diff parameter for hhfilter (default: 30)
    top_N       int, cutoff for the number of sequences in the MSAs i.e. only
                the top "topN" sequences are used (default: 100)
    max_N       int, cutoff for the number of sequences desired (default: 40)
    reorder     bool, if True, sequence are reordered according to general seq id with the first sequences in the coMSAs
    fastaA      str, fasta file with sequence that needs adding to the top of msaA (using mafft --seed)
    fastaB      str, fasta file with sequence that needs adding to the top of msaB
    """
    self.workdir = workdir
    self.verbose = verbose
    self.MSAA = MSAA
    self.MSAB = MSAB
    self.fastaA = fastaA
    self.fastaB = fastaB
    self.reorder = reorder
    self.diff = diff
    self.top_N = top_N
    self.max_N = max_N

    if not os.path.exists(self.workdir):
      os.makedirs(self.workdir)

    if outMSAA and outMSAB:
      outMSAA = os.path.join(self.workdir,"{}".join(os.path.splitext(
                                  os.path.basename(outMSAA))))
      outMSAB = os.path.join(self.workdir,"{}".join(os.path.splitext(
                                  os.path.basename(outMSAB))))
    else:
      outMSAA = os.path.join(self.workdir,"{}".join(os.path.splitext(
                                  os.path.basename(self.MSAA))))
      outMSAB = os.path.join(self.workdir,"{}".join(os.path.splitext(
                                  os.path.basename(self.MSAB))))

    self.output_hhfilterA = outMSAA.format('_hhfiltered')
    self.output_hhfilterB = outMSAB.format('_hhfiltered')
    self.output_eq10A = outMSAA.format('_hhfiltered_eq10')
    self.output_eq10B = outMSAB.format('_hhfiltered_eq10')

  def run(self):
    """
    Run reduce MSAs
    """
    # add first sequence if given
    if self.fastaA and self.fastaB and os.path.exists(self.fastaA) and os.path.exists(self.fastaB):
      h_iniA, s_iniA = self.add_first_sequence(self.fastaA, self.MSAA)
      h_iniB, s_iniB = self.add_first_sequence(self.fastaB, self.MSAB)
    else:
      h_iniA, s_iniA = tools.read_fasta(self.MSAA)
      h_iniB, s_iniB = tools.read_fasta(self.MSAB) 

    if self.reorder:
      (h_iniA, s_iniA), (h_iniB, s_iniB) = self.reorder_seq((h_iniA, s_iniA), (h_iniB, s_iniB))
    elif self.top_N:
      (h_iniA, s_iniA), (h_iniB, s_iniB) = (h_iniA[:self.top_N], s_iniA[:self.top_N]), (h_iniB[:self.top_N], s_iniB[:self.top_N])
    initial_lg = len(s_iniA)

    # if we already have a low number of sequences, no need to filter out sequences
    if initial_lg <= 11:
      if self.verbose: 
        print("{} and {}: Number of sequences is already <= 11 -> {}".format(self.MSAA,self.MSAB,initial_lg)+\
              ", no need for hhfilter or eq10 selection")
      shutil.copy(self.MSAA,self.output_hhfilterA)
      shutil.copy(self.MSAB,self.output_hhfilterB)
      shutil.copy(self.MSAA,self.output_eq10A)
      shutil.copy(self.MSAB,self.output_eq10B)
      if self.verbose: 
        print("output MSAs after hhfilter: {} and {}".format(self.output_hhfilterA,
                                self.output_hhfilterB))
        print("output MSAs after hhfilter and selection of 10 sequences: {} and {}".format(
                                self.output_eq10A,self.output_eq10B))
      return

    folder = tempfile.mkdtemp(dir=self.workdir)

    # combine MSAs to use hhfilter on both MSAs at the same time
    combined_MSA = os.path.join(folder,"combined_MSA.fasta")
    tools.combine_MSAs([[h_iniA, s_iniA],[h_iniB, s_iniB]],combined_MSA)

    filtered_MSA = os.path.join(folder,"filtered_diff_combined_MSA.fasta")

    # if we already have a low number of sequences, no need to filter out sequences
    if initial_lg < self.max_N:
      if self.verbose:
        print("{} and {}: Number of sequences is already low enough -> {}".format(self.MSAA,self.MSAB,initial_lg)+\
            ", no need for hhfilter")
      shutil.copy(combined_MSA,filtered_MSA)

    else:
      initial_diff = self.diff

      self.use_hhfilter(combined_MSA,filtered_MSA,self.diff)

      # if desired number of sequences max_N is specified, try tuning diff option to get closer
      # to it
      if self.max_N:
        h,s = tools.read_fasta(filtered_MSA)
        reduced_lg = len(h)
        while reduced_lg > self.max_N+1: # max_N + 1 to take query into account
          # continue reducing
          if self.diff != initial_diff:
            os.remove(filtered_MSA)
          self.diff-=2
          self.use_hhfilter(combined_MSA,filtered_MSA,self.diff)
          h,s = tools.read_fasta(filtered_MSA)
          reduced_lg = len(h)

    # extract from hhfiltered combined MSA the separate MSAs
    h_filtered, _  = tools.read_fasta(filtered_MSA)
    idx = [i for i in range(len(h_iniA)) if h_iniA[i]+'|||'+h_iniB[i] in h_filtered]
    tools.write_fasta([h_iniA[i] for i in idx],[s_iniA[i] for i in idx],
                                          self.output_hhfilterA)
    tools.write_fasta([h_iniB[i] for i in idx],[s_iniB[i] for i in idx],
                                          self.output_hhfilterB)

    # extract from hhfiltered combined MSA 10 equally distributed sequences into
    # separate MSAs
    num_seq = len(h_filtered)-1
    divisor = min(num_seq,10)
    q = num_seq//divisor
    r = num_seq%divisor
    idx_eq10 = list(range(0, (q+1)*r, q+1)) + list(range((q+1)*r,num_seq+1,q))

    tools.write_fasta([h_iniA[i] for i in idx_eq10],[s_iniA[i] for i in idx_eq10],
                                                self.output_eq10A)
    tools.write_fasta([h_iniB[i] for i in idx_eq10],[s_iniB[i] for i in idx_eq10],
                                                self.output_eq10B)

    if self.verbose:
      print("output MSAs after hhfilter: {} and {}".format(
                      self.output_hhfilterA,self.output_hhfilterB))
      print("output MSAs after hhfilter and selection of 10 sequences: {} and {}".format(
                      self.output_eq10A,self.output_eq10B))

    shutil.rmtree(folder)


  def use_hhfilter(self,in_fasta,out_fasta,diff):
    """
    Run HH-filter

    Input:
    in_fasta:    str, name of the input fasta file
    out_fasta:   str, name of the output fasta file
    diff: int, value for hhfilter -diff option

    Output:
    None
    """
    # remove first sequence (i.e. query sequence)
    h,s = tools.read_fasta(in_fasta)
    temp_file = tools.make_tempfile(prefix_str='tmp',suffix_str='.fst',parent_dir=self.workdir)
    tools.write_fasta(h[1:],s[1:],temp_file)

    # run hhfilter
    args = ["-i {}".format(temp_file),
            "-o {}".format(out_fasta),
            "-diff {}".format(diff)]

    cluster.runTasks(HHFILTER,args,environment_module=DOCKER_HH, log_prefix = "reduce_co_msa")
    if not os.path.exists(out_fasta):
      print("Error in ReduceCoMSA: hhfilter failed for {}".format(in_fasta))
      raise Exception("Error in ReduceCoMSA: hhfilter failed for {}".format(in_fasta))
    os.remove(temp_file)

    # get index of selected sequences and re-insert first sequence
    ho,so = tools.read_fasta(out_fasta)
    idx=[0]+[i for i in range(1,len(h)) if h[i] in ho]

    tools.write_fasta([h[i] for i in idx], # headers
                [s[i] for i in idx], # sequences
                out_fasta,
                ignore_gaps=False)


  def reorder_seq(self,msaA,msaB):
    """
    Paste 2 MSAs files together
    
    Input:
    msaA:     ([str],[str]), a tuple with a list of the sequence headers 
               and a list of corresponding sequences for the first coMSA
    msaB:     ([str],[str]), a tuple with a list of the sequence headers 
               and a list of corresponding sequences for the second coMSA

    Output:
    msaA:     ([str],[str]), a tuple with a list of the sequence headers 
               and a list of corresponding sequences for the first coMSA
    msaB:     ([str],[str]), a tuple with a list of the sequence headers 
               and a list of corresponding sequences for the second coMSA
    """
    def calc_seqid(seq1,seq2):
      """
      Calculate symmetric id between 2 sequences normalised by alignment length

      Input:
      seq1:   str, aligned amino acid sequence 1
      seq2:   str, aligned amino acid sequence 2

      Output:
      None
      """
      id_count = 0.
      lg = 0.
      for i in range(len(seq1)):
        if seq1[i]==seq2[i]!='-':
          id_count += 1
        if seq1[i]!="-" or seq2[i]!="-":
          lg += 1
      return id_count/lg

    h1,s1 = msaA
    h2,s2 = msaB

    seq_ids = [0. for i in range(len(h1)-1)]
    for i in range(1,len(h1)):
      seq_ids[i-1] = calc_seqid(s1[0]+s2[0],s1[i]+s2[i])
    new_order = sorted(range(len(h1)-1),key=lambda k: seq_ids[k])[::-1]
    h1 = h1[:1] + [h1[i+1] for i in new_order]
    s1 = s1[:1] + [s1[i+1] for i in new_order]
    h2 = h2[:1] + [h2[i+1] for i in new_order]
    s2 = s2[:1] + [s2[i+1] for i in new_order]

    if self.top_N:
      new_lg = min(len(h1),self.top_N)
    else:
      new_lg = len(h1)

    return (h1[:new_lg],s1[:new_lg]),(h2[:new_lg],s2[:new_lg])

  def add_first_sequence(self, first_seq_fasta, MSA_fasta):
      """
      Add a sequence to the top of the coMSA using Mafft --seed

      Input:
      first_seq_fasta:  str, fasta file of the sequence that needs to be added
      MSA_fasta:        str, coMSA fasta file

      Output:
      (headers,seqs):   ([str],[str]), a tuple with a list of the sequence headers 
              and a list of corresponding sequences
      """
      tmpout = make_tempfile(parent_dir=self.workdir,prefix_str='tmp',suffix_str='.fst')
      tmpin = False
      h,s = tools.read_fasta(MSA_fasta)
      if len(h) == 0:
          print("Error in MergeMSAs: given MSA is empty")
          raise Exception("Error in MergeMSAs: given MSA is empty")
      elif len(h) == 1:
          # run mafft without --seed
          tmpin = make_tempfile()
          h1,s1 = tools.read_fasta(first_seq_fasta)
          tools.write_fasta([h[0],h1[0]],[s[0],s1[0]],tmpin,ignore_gaps=False)
          args = ["--auto",
                  tmpin,
                  "> {}".format(tmpout)]
      else:
          args = ["--auto",
                  "--seed {}".format(MSA_fasta),
                  first_seq_fasta,
                  "> {}".format(tmpout)]
              
      cluster.runTasks(MAFFT, args, environment_module=DOCKER_MAFFT, log_prefix = "reduce_co_msa")
      try:
        h,s = tools.read_fasta(tmpout)
      except:
        print("Error in ReduceCoMSA: mafft didn't work on {} and {}".format(MSA_fasta,first_seq_fasta))
        raise Exception("Error in ReduceCoMSA: mafft didn't work on {} and {}".format(MSA_fasta,first_seq_fasta))
      os.remove(tmpout)
      if tmpin:
          try:
              os.remove(tmpin)
          except:
              pass
      return [hh.replace("_seed_","") for hh in h[-1:]+h[:-1]], s[-1:]+s[:-1]
                
def main():
    """
    """
    USAGE = "Usage: coMSAs.py -i 1.fasta,2.fasta,3.fasta,4.fasta [-g 1,1:2,3,4 -r 90 -s -F -P -v]\n"+\
            "Script to generate coMSAs, reduce coMSAs or merge coMSAs\n"+\
            "\n1. Generate coMSAs:\n"+\
            "Output: output files will be generated in curdir:\n"+\
            "  final_MSAx.fasta (x=1,2,3...): co-MSA for each sequence\n"+\
            "  regrouped_MSAx.fasta (x=1,2,3...): regrouped MSA according to -g option\n"+\
            "Ex: coMSAs.py -i A.fasta,B.fasta,C.fasta -g 1,1:2,3\n"+\
            "  regrouped_MSA1.fasta is for a homodimer of A and regrouped_MSA2.fasta is for BC fusion "+\
            "\n2. Merge 2 MSAs:\n"+\
            "Description: Merge two fasta format MSA by aligning the a-th sequence in MSA_a and b-th sequence in MSA_b."+\
            "Ex: coMSAs.py -i MSA_a.fasta,MSA_b.fasta -n <int>,<int> -o MSA_out.fasta\n"

    # options
    parser = optparse.OptionParser(usage=USAGE)
    parser.add_option('-i','--input_list_fasta',action="store",dest='input_list_fasta',type='string',
                      help="lst of fasta sequences delimited by commas, ex: 1.fasta,2.fasta,3.fasta or single.fasta")
    parser.add_option('-o','--output_fasta',action="store",dest='output_fasta',type='string',
                      help="output fasta",default=None)
    parser.add_option('-F','--use_Full_length_seq',action="store_true",dest='use_Full_length_seq',
                      help="Use FL sequences instead of BLAST hit sequences", default=False)
    parser.add_option('-s','--if_do_separate_MSA',action="store_true",dest='if_do_separate_MSA',
                      help="separate MSA files will be also generated", default=False)
    # parser.add_option('--reduceMSAs',action="store_true",dest='if_reduce_coMSAs',
    #                   help="to reduce coMSAs that are generated", default=False)
    parser.add_option('-g','--group_MSA',action="store",dest='group_MSA',
                      help="group final MSAs, ex: 1,2:3,4,5 means MSA1 and 2 will group together,"+\
                            " 3,4,5 is in another group", default=None)
    parser.add_option('-P','--parallel',action="store_true",dest='in_parallel',
                      help="Parallel mode (def=False, only one cpu is used): "+\
                      "use 8 cpu for blast", default=False)
    parser.add_option('-r','--redundancy_filter',action="store",dest='redundancy_filter',type='float',
                      help="redundancy filter by how much identity (in percentage, def=%d)"%90, default=90)
    parser.add_option('-C','--seq_Coverage',action="store",dest='seq_Coverage',type='float',
                      help="the threshold of sequence coverage (def=%.1f) in percentage"%75, default=75)
    parser.add_option('-I','--seq_ID',action="store",dest='seq_ID',type='float',
                      help="the threshold of sequence identity (def=%.1f) in percentage"%30, default=30)
    parser.add_option('-n','--idx_seq',action="store",dest='idx_seq',type='str',
                      help="n-th sequence numbers that will be aligned in each MSA to merge them", default=None)
    parser.add_option('--topN',action="store",dest='topN',type='int',
                      help="Cutoff for number of sequences if reducing coMSAs", default=None)
    parser.add_option('--maxN',action="store",dest='maxN',type='int',
                      help="Max number of sequences you wish to acquire if reducing coMSAs", default=None)
    parser.add_option('-v','--verbose',action="store_true",dest='verbose',
                      help="verbose mode (def=False): all files will be generated and kept in curdir", default=False)
    if len(sys.argv) == 1:
        print(USAGE)
        print("Type -h or --help for more help information")
        sys.exit(1)
    (options, args) = parser.parse_args(sys.argv)
    if len(args) > 1: # sys.argv[0] is the name of this program
        print("Leftover arguments:" + str(args[1:]))
    lst_fasta = re.split(",",options.input_list_fasta)
    lst_fasta_abspath = [os.path.abspath(fasta_file) for fasta_file in lst_fasta]

    if (not options.idx_seq or options.idx_seq == None):
        print("Generating coMSAs")
        GenerateCoMSAs(lst_fasta_abspath,
                       options.redundancy_filter,
                       options.group_MSA,
                       options.if_do_separate_MSA,
                       options.use_Full_length_seq,
                       options.in_parallel,
                       options.verbose,
                       options.seq_Coverage,
                       options.seq_ID)

    elif options.idx_seq and options.output_fasta:
        print("Merge MSAs")
        if options.group_MSA:
            lst_idx = options.group_MSA.split(':')
            list_MSA_a = [lst_fasta_abspath[int(i)-1] for i in lst_idx[0].split(',')]
            list_MSA_b = [lst_fasta_abspath[int(i)-1] for i in lst_idx[1].split(',')]
        else:
            list_MSA_a = [lst_fasta_abspath[0]]
            list_MSA_b = [lst_fasta_abspath[1]]
        MergeMSAs(list_MSA_a, list_MSA_b, int(options.idx_seq.split(',')[0]), int(options.idx_seq.split(',')[1]), options.output_fasta)

    else:
        print("Reducting coMSAs")
        MSAA = lst_fasta_abspath[0]
        MSAB = lst_fasta_abspath[1]
        ReduceCoMSA(os.path.abspath(os.getcwd()), MSAA, MSAB, outMSAA=None, outMSAB=None, diff=options.redundancy_filter, top_N=options.topN, max_N=options.maxN, reorder=True, fastaA="", fastaB="", verbose=options.verbose)

if __name__ == '__main__':
    main()
