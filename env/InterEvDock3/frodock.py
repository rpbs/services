#! /usr/bin/env python
"""
Docking and scoring module with frodock2.1 or our frodock3.1_repo
Last modified: CQ Nov. 2020
"""
import os, sys
import tempfile
import shutil
import numpy as np
import optparse
import time
import glob
import pkg_resources

# for python version compatibility
try:
    import ConfigParser
except:
    import configparser as ConfigParser

import InterEvDock3.tools as tools

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini")) # config file in the same folder as this script

import PyPDB.PyPDB as PDB

import cluster.cluster as cluster

# defined docker files where frodock2.1 and frodock3.1 are installed
DOCKER21 = config.get('environments','frodock21')
DOCKER31 = config.get('environments','frodock31')
DOCKER32 = config.get('environments','frodock32') # for coevo map scoring

# defined conda environments for openmpi
OPENMPI1 = config.get('environments','openmpi1')
OPENMPI2 = config.get('environments','openmpi2')

# defined extensions for frodock bins
FD21_EXT = config.get('programs','frodock21_ext')
FD31_EXT = config.get('programs','frodock31_ext')
FD32_EXT = config.get('programs','frodock32_ext') # for coevo map scoring

FRODOCKGEN = config.get('scripts','GenerateDecoys')

class Frodock():

    def __init__(self, receptor="", ligand="", workdir="./", nodes=1, cpus=1):
        """
        Module for docking
        Inputs: 
        receptor: str, receptor pdb
        ligand: str, ligand pdb
        """
        self.workdir = os.path.abspath(workdir)
        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir)
        self.receptor = receptor
        self.ligand = ligand
        self._nodes = nodes
        self._cpus = cpus
        if self._nodes == self._cpus == 1:
            self._parallel = False
        else:
            self._parallel = True
        self.frodockbin21 = config.get('programs','frodock21')
        self.frodockbin31 = config.get('programs','frodock31')
        self.frodockbin32 = config.get('programs','frodock32')# for coevo map scoring

    def run_frodock_sampling(self, output_dat, receptor="", ligand="", workdir="", version=2.1, type_option="O", np=4, th=10.0, extra_args=[]):
        """
        Run Frodock grid and frodock in parallel

        Inputs: 
        output_dat: str, name of output docking (.dat) file
        version: int, frodock version (2.1 or 3.1)
        type_option: str, frodock type (A: Antibody/Antigen, E: Enzyme/Ligand, O: Other) [default=O]
        th: electrostatic threshold for sampling
        np: top number of rotations per position around the receptor
        extra_args: list of extra frodock inputs e.g. ["-E 0.5", "-W 15", "--wS 0.5", "--wT 15"]
        """
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if not workdir:
            workdir = self.workdir
        tmpdir = tools.make_tempdir(parent_dir=workdir)
        shutil.copy(receptor,tmpdir)
        shutil.copy(ligand,tmpdir)
        curdir = os.path.abspath(os.getcwd())
        os.chdir(tmpdir)
        tmpoutput = os.path.basename(output_dat)
        receptor = os.path.basename(receptor)
        ligand = os.path.basename(ligand)

        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            ext = 'ccp4'
            gcc = FD21_EXT
            environment_module = [OPENMPI1,DOCKER21]
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            ext = 'mrc'
            gcc = FD31_EXT
            soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')
            extra_args = list(set(extra_args+['-s '+soap_bin]))
            environment_module = [OPENMPI2,DOCKER31]
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            ext = 'mrc'
            gcc = FD32_EXT
            soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')
            extra_args = list(set(extra_args+['-s '+soap_bin]))
            environment_module = [OPENMPI2,DOCKER32]
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))

        if self._parallel:   
            FRODOCKGRID = os.path.join(FRODOCKBIN,'frodockgrid_mpi'+gcc)
            FRODOCK = os.path.join(FRODOCKBIN,'frodock_mpi'+gcc)
        else:
            FRODOCKGRID = os.path.join(FRODOCKBIN,'frodockgrid'+gcc)
            FRODOCK = os.path.join(FRODOCKBIN,'frodock'+gcc)

        # Creation of receptor vdw potential map
        args = [receptor,
                "-o {}_W.{}".format(os.path.splitext(receptor)[0],ext),
                "-m 0"]#,
                #"-t {}".format(type_option)] # JA: option not necessary for this operation
        os.chdir(curdir)
        cluster.progress(' ... creating VDW potential map')
        os.chdir(tmpdir)        
        print(' ... creating VDW potential map')
        if self._parallel:   
            cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        else:
            cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        if not os.path.exists("{}_W.{}".format(os.path.splitext(receptor)[0],ext)):
            os.chdir(curdir)        
            cluster.progress('Warning: unknown HETATM atoms detected by frodockgrid. All HETATM lines will be removed for the receptor structure.')
            os.chdir(tmpdir)        
            print('Warning: unknown HETATM atoms detected by frodockgrid. All HETATM lines will be removed for the receptor structure.')
            x = PDB.PDB(receptor, hetSkip = 1)
            x.out(receptor)
            if self._parallel:   
                cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
            else:
                cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
            if not os.path.exists("{}_W.{}".format(os.path.splitext(receptor)[0],ext)):
                os.chdir(curdir)
                cluster.progress('Error: Frodock cannot read input receptor pdb file.')
                raise Exception('frodockgrid failed for %s' % receptor)            

        # Creation of the receptor electrostatic potential map
        args = [receptor,
                "-o {}_E.{}".format(os.path.splitext(receptor)[0],ext),
                "-m 1",
                "-t {}".format(type_option)]
        os.chdir(curdir)        
        cluster.progress(' ... creating EP potential map')
        os.chdir(tmpdir)        
        print(' ... creating EP potential map')
        if self._parallel:   
            cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        else:
            cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        if not os.path.exists("{}_E.{}".format(os.path.splitext(receptor)[0],ext)):
            os.chdir(curdir)
            cluster.progress('Error: Frodock cannot read input receptor pdb file.')
            raise Exception('frodockgrid failed for %s' % receptor)            

        # Creation of the receptor desolvation potential map
        args = [receptor,
                "-o {}_DS.{}".format(os.path.splitext(receptor)[0],ext),
                "-m 3"]#, 
                #"-t {}".format(type_option)] # JA: option not necessary for this operation
        os.chdir(curdir)        
        cluster.progress(' ... creating desolvation potential maps')
        os.chdir(tmpdir)
        print(' ... creating desolvation potential maps')
        if self._parallel:   
            cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        else:
            cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        if not os.path.exists("{}_DS.{}".format(os.path.splitext(receptor)[0],ext)):
            os.chdir(curdir)
            cluster.progress('Error: Frodock cannot read input receptor pdb file.')
            raise Exception('frodockgrid failed for %s' % receptor)            

        # Creation of the ligand desolvation potential map
        args = [ligand,
                "-o {}_DS.{}".format(os.path.splitext(ligand)[0],ext),
                "-m 3"]#,
                #"-t {}".format(type_option)] # JA: option not necessary for this operation
        if self._parallel:   
            cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        else:
            cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        if not os.path.exists("{}_DS.{}".format(os.path.splitext(ligand)[0],ext)):
            cluster.progress('Warning: unknown HETATM atoms detected by frodockgrid. All HETATM lines will be removed for the ligand structure.')
            print('Warning: unknown HETATM atoms detected by frodockgrid. All HETATM lines will be removed for the ligand structure.')
            x = PDB.PDB(ligand, hetSkip = 1)
            x.out(ligand)
            if self._parallel:
                cluster.runMPITasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
            else:
                cluster.runTasks(FRODOCKGRID, args, environment_module = environment_module, log_prefix = "frodock_sampling")
            if not os.path.exists("{}_DS.{}".format(os.path.splitext(ligand)[0],ext)):
                os.chdir(curdir)
                cluster.progress('Error: Frodock cannot read input ligand pdb file.')
                raise Exception('frodockgrid failed for %s' % ligand)            

        # Performing the docking
        args = ["_ASA".join(os.path.splitext(receptor)),
                "_ASA".join(os.path.splitext(ligand)),
                "-w {}_W.{}".format(os.path.splitext(receptor)[0],ext),
                "-e {}_E.{}".format(os.path.splitext(receptor)[0],ext),
                "--th {}".format(th), # electrostatic map threshold
                "--np {}".format(np),
                "-d {}_DS.{},{}_DS.{}".format(os.path.splitext(receptor)[0],ext,os.path.splitext(ligand)[0],ext),
                "-t {}".format(type_option),
                "-o {}".format(tmpoutput)] + extra_args
        os.chdir(curdir)
        cluster.progress(' ... docking with FRODOCK type_option %s'%type_option)
        os.chdir(tmpdir)
        print(' ... docking with FRODOCK type_option %s'%type_option)
        if self._parallel:
            cluster.runMPITasks(FRODOCK, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        else:
            cluster.runTasks(FRODOCK, args, environment_module = environment_module, log_prefix = "frodock_sampling")
        if not os.path.exists(tmpoutput):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not generate .dat.')
            raise Exception('frodock failed for %s and %s' %(receptor,ligand))

        os.chdir(curdir)
        shutil.copy(os.path.join(tmpdir,tmpoutput),output_dat)
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)


    def run_frodock_clustering(self, input_dat, output_dat, workdir="", ligand="", version=2.1, extra_args=['-d 4']):
        """
        Run Frodock clustering

        Inputs: 
        input_dat: str, input .dat file
        output_dat: str, output .dat file
        version: int, frodock version (2.1, 3.1 or 3.2)
        extra_args: extra frodockcluster args e.g. ['-d 4']
        """
        if not ligand:
            ligand = self.ligand
        if not workdir:
            workdir = self.workdir
        tmpdir = tools.make_tempdir(parent_dir=workdir)
        shutil.copy(ligand,tmpdir)
        shutil.copy(input_dat,tmpdir)
        curdir = os.path.abspath(os.getcwd())
        os.chdir(tmpdir)
        tmpoutput = os.path.basename(output_dat)
        ligand = os.path.basename(ligand)
        input_dat = os.path.basename(input_dat)

        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            gcc = FD21_EXT
            environment_module= DOCKER21
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            gcc = FD31_EXT
            environment_module= DOCKER31
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            gcc = FD32_EXT
            environment_module= DOCKER32
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))
        FRODOCKCLUSTER = os.path.join(FRODOCKBIN,'frodockcluster'+gcc)

        # Clustering of predictions
        args = ["{}".format(input_dat),
                "{}".format(ligand),
                "-o {}".format(tmpoutput)] + extra_args
        os.chdir(curdir)        
        cluster.progress(' ... clustering decoys')
        os.chdir(tmpdir)        
        print(' ... clustering decoys')
        cluster.runTasks(FRODOCKCLUSTER, args, environment_module = environment_module, log_prefix = "frodock_clustering")
        if not os.path.exists(tmpoutput):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not cluster .dat.')
            raise Exception('frodockcluster failed for %s and %s' %(input_dat, ligand))
 
        os.chdir(curdir)
        shutil.copy(os.path.join(tmpdir,tmpoutput),output_dat)
        shutil.rmtree(tmpdir)


    def run_frodock_constraints(self, input_dat, output_dat, constraints, workdir='', receptor="", ligand="", version=2.1, xorconstraints=[]):
        """
        Run Frodock constraints

        Inputs: 
        input_dat: str, input .dat file
        output_dat: str, output .dat file
        constraints: str, constraints file
        version: int, frodock version (2.1 or 3.1)
        xorconstraints: [str], list of supplementary constraints files applied as OR (only available with frodock3.1)
        """
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if not workdir:
            workdir = self.workdir
        tmpdir = tools.make_tempdir(parent_dir=workdir)
        shutil.copy(receptor,tmpdir)
        shutil.copy(ligand,tmpdir)
        shutil.copy(input_dat,tmpdir)
        shutil.copy(constraints,tmpdir)
        curdir = os.path.abspath(os.getcwd())
        for c in xorconstraints:
            shutil.copy(c,tmpdir)
        os.chdir(tmpdir)
        tmpoutput = os.path.basename(output_dat)
        receptor=os.path.basename(receptor)
        ligand=os.path.basename(ligand)
        input_dat=os.path.basename(input_dat)
        constraints=os.path.basename(constraints)

        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            environment_module= DOCKER21
            if len(xorconstraints):
                print("Warning: xor constraints not possible with frodock2.1, they will be ignored")
                xorconstraints=[]
            gcc = FD21_EXT
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            environment_module= DOCKER31
            gcc=FD31_EXT
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            environment_module= DOCKER32
            gcc=FD32_EXT
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))

        FRODOCKCONSTRAINTS = os.path.join(FRODOCKBIN,'frodockonstraints'+gcc)

        # Apply constraints on predictions
        args = ["{}".format(input_dat),
                "{}".format(receptor),
                "{}".format(ligand),
                "{}".format(constraints),
                "-o {}".format(tmpoutput)]
        args += ["--xor {}".format(os.path.basename(c)) for c in xorconstraints]
        os.chdir(curdir)        
        cluster.progress(' ... applying user constraints')
        os.chdir(tmpdir)        
        print(' ... applying user constraints')
        cluster.runTasks(FRODOCKCONSTRAINTS, args, environment_module = environment_module, log_prefix = "frodock_clustering")
        if not os.path.exists(tmpoutput):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not apply constraints.')
            raise Exception('frodockonstraints failed for %s and %s' %(input_dat, ",".join([constraints]+xorconstraints)))
        if self.read_frodock_output(tmpoutput, workdir=workdir, version=version).shape[0] == 0:
            os.chdir(curdir)
            cluster.progress('No decoys left after applying constraints.')
            raise Exception('frodockonstraints filtered out all decoys for %s and %s' %(input_dat, ",".join([constraints]+xorconstraints)))

        os.chdir(curdir)
        shutil.copy(os.path.join(tmpdir,tmpoutput),output_dat)
        shutil.rmtree(tmpdir)


    def write_frodock_constraints(self, output, receptor_constraints=[], ligand_constraints=[], pairwise_constraints=[]):
        """
        Write Frodock constraint file

        Inputs: 
        output: str, output constraint file
        receptor_constraints: [(ch,resnum,restype,dist)], single receptor constraints
        ligand_constraints: [(ch,resnum,restype,dist)], single ligand constraints
        pairwise_constraints: [(ch,resnum,restype,ch,resnum,restype,dist)], receptor-ligand constraints
        """
        f=open(output,"w")
        f.write("RECEPT_____     LIGAND_____ D__\n")
        f.write("-------------------------------\n")
        for ch,resnum,restype,dist in receptor_constraints:
            f.write("{:3s} {} {:>4d}      --- -    -1 {:>2d}\n".format(restype,ch,int(resnum),dist))
        for ch,resnum,restype,dist in ligand_constraints:
            f.write("--- -   -1      {:3s} {} {:>5d} {:>2d}\n".format(restype,ch,int(resnum),dist))
        for chA,resnumA,restypeA,chB,resnumB,restypeB,dist in pairwise_constraints:
            f.write("{:3s} {} {:>4d}      {:3s} {} {:>5d} {:>2d}\n".format(restypeA,chA,int(resnumA),restypeB,chB,int(resnumB),dist))
        f.close()


    def write_frodock_contraints_v1(self, output, receptor_constraints=[], ligand_constraints=[], pairwise_constraints=[]):
        """
        XXXXXX I'm leaving this here for historical purposes
        On standby -> not sure how frodockv1 deals with chains
        Writes a frodockv1-friendly constraints file
        INPUT
        name of the output file
        constraints dictionary
        dictionary of max distances that defines a contact between 2 residues
        OUTPUT
        None
        """
        f=open(output,"w")
        f.write("RECEPT______LIGAND_____DIST\n")
        f.write("---------------------------\n")
        for ch,resnum,restype,dist in receptor_constraints:
            f.write("{:<12}{:<10}{:>5}\n".format(restype,'-1',dist))
        for ch,resnum,restype,dist in ligand_constraints:
            f.write("{:<12}{:<10}{:>5}\n".format('-1',restype,dist))
        for chA,resnumA,restypeA,chB,resnumB,restypeB,dist in pairwise_constraints:
            f.write("{:<12}{:<10}{:>5}\n".format(restypeA,restypeB,dist))
        f.close()


    def read_frodock_output(self, input_dat, workdir="", version=2.1, output=None, topN=0):
        """
        Read frodock output directly in python (pyIVS-style)

        Inputs: 
        input_dat: str, input .dat file
        version: int, frodock version (2.1 or 3.1)
        output: str, output file with decoy numbers and corresponding scores, if not, 
                    results are returned as a 2D array (column 1 = decoy number, 
                    column 2 = score)
        topN: int, top number of decoys

        Output:
        if output file not specified, array of scores
        """
        rectype = np.dtype([('psi', np.float32),
                            ('theta', np.float32),
                            ('phi', np.float32),
                            ('x', np.float32),
                            ('y', np.float32),
                            ('z', np.float32),
                            ('score', np.float32)])

        f = open(input_dat, "rb")
        if version>=3:
            f.seek(4*4, os.SEEK_SET) #Skips the origin of the receptor for the new files
        else:
            f.seek(4, os.SEEK_SET)
        data = np.fromfile(f, dtype=rectype)
        f.close()

        if topN:
            data=data[:topN]

        if output:
            f=open(output,'w')
            f.write('#decoy score\n')
            for i,line in enumerate(data):
                f.write('complex.{}.pdb {:.4f}\n'.format(i+1,line[-1]))
            f.close()
        
        return data


    def combine_scores(self, input_list_dat, version=2.1, extra_args=[]):
        """
        Combine average scores over several frodock .dat outputs

        Inputs: 
        input_list_dat: [str], list of input .dat files
        version: int, frodock version (2.1 or 3.1)
        output: str, output file, if not, results are returned as an array
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])

        Output:
        if output file not specified, array of scores
        """
        total_scores = None
        div = 0
        for input_dat in input_list_dat:
            scores = self.read_frodock_output(input_dat, version=version, output=None, extra_args=extra_args)['score'] # we assume decoys are ordered in the same way
            try:
                total_scores+=scores
                div+=1
            except:
                total_scores=scores
                div+=1
        return total_scores/div


    def check_size(self, input_dat, workdir="", version=2.1):
        """
        Check number of decoys in the dock.dat file
        """
        if not workdir:
            workdir = self.workdir
        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            gcc = FD21_EXT
            environment_module= DOCKER21
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            gcc=FD31_EXT
            environment_module= DOCKER31
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            gcc=FD32_EXT
            environment_module= DOCKER32
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))
        FRODOCKVIEW = os.path.join(FRODOCKBIN,'frodockview'+gcc)
        
        stdoutfile = tools.make_tempfile(parent_dir=workdir)
        args = ["{}".format(os.path.basename(input_dat)),
                "--size",
                '> {}'.format(stdoutfile),
                '2>/dev/null'] # always returns a status 1
        cluster.runTasks(FRODOCKVIEW, args, environment_module = environment_module, log_prefix = "frodock_decoys")
        try:
            f=open(stdoutfile)
            size=int(f.read().strip())
            f.close()
            os.remove(stdoutfile)
            return size
        except:
            cluster.progress('Error: Frodock could not read .dat')
            raise Exception('frodockview failed reading %s' %(input_dat))


    def create_frodock_decoys(self, input_dat, outdir, receptor="", ligand="", version=2.1, decoys=list(range(1,10001)), decoy_names=[], extra_args=[]):
        """
        Run Frodock view to generate the decoys but use the wrapper instead of frodockview directly (1 job instead of N for N decoys)

        Inputs: 
        input_dat: str, input .dat file
        version: int, frodock version (2.1, 3.1 or 3.2)
        output: str, output file, if not, results are returned as an array
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])

        Output:
        if output file not specified, array of scores
        """
        print("Starting create_frodock_decoys")
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            environment_module= DOCKER21
            gcc = FD21_EXT
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            environment_module= DOCKER31
            gcc = FD31_EXT
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            environment_module= DOCKER32
            gcc = FD32_EXT
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))


        if not len(decoy_names):
            decoy_names = ['complex.{}.pdb'.format(n) for n in decoys]

        if len(decoy_names) != len(decoys):
            print("Error: specified decoy output names not the same length as specified decoy list to generate")
            print(decoy_names[:10])
            print(decoys[:10])
            raise Exception("Error: specified decoy output names not the same length as specified decoy list to generate")

        outdir = os.path.abspath(outdir)
        try:
            os.mkdir(outdir)
        except:
            pass

        def regroup_list(lst):
            """
            function to regroup decoy numbers in consecutive batches
            e.g. [1,2,3,6,7,8,9] -> ['1-3', '6-9']
            """
            last_elt = lst[0]
            first_elt = lst[0]
            txt = str(first_elt)
            if len(lst) > 1:
                for elt in lst[1:]:
                    if elt != last_elt + 1:
                        if last_elt == first_elt:
                            txt+=","+str(elt)
                        else:
                            txt+='-'+str(last_elt)+","+str(elt)
                        first_elt = elt
                        last_elt = elt
                    else:
                        last_elt = elt
                if not txt.endswith(str(elt)):
                    txt+='-'+str(elt)
            return txt

        # run frodockview in batches when possible
        try:
            shutil.copy(receptor,outdir)
        except:
            pass
        try:
            shutil.copy(ligand,outdir)
        except:
            pass
        try:
            shutil.copy(input_dat,outdir)
        except:
            pass
        curdir = os.path.abspath(os.getcwd())
        os.chdir(outdir)
        decoy_str = regroup_list(decoys)
        args = ["{}".format(os.path.basename(input_dat)),
                "-p {}".format(os.path.basename(ligand)),
                "-r {}".format(decoy_str)]
        if gcc:
            args += ['-ext '+gcc]
        args += extra_args
        cluster.runTasks(FRODOCKGEN, args, environment_module = environment_module, log_prefix = "frodock_decoys")

        # write complex pdbs because frodockview only generates the ligand poses
        f=open(os.path.basename(receptor))
        data_rec = [line for line in f if line.strip() and (line.startswith("ATOM") or line.startswith("HETATM") or line.startswith("TER"))]
        f.close()
        skipped = 0
        for decoy,name in zip(decoys,decoy_names):
            if not os.path.exists(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand)))):
                print(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand))))
                skipped += 1
                continue
            fo=open(name,"w")
            fo.write("".join(data_rec))
            fi=open(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand))))
            for line in fi:
                if line.strip() and (line.startswith("ATOM") or line.startswith("HETATM") or line.startswith("TER")):
                    fo.write(line)
            fo.close()
            fi.close()
            os.remove(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand))))

        if curdir != outdir:
            os.remove(os.path.basename(receptor))
            os.remove(os.path.basename(ligand))
            os.remove(os.path.basename(input_dat))

        os.chdir(curdir)
        to_rm = " ".join([l for l in glob.glob(outdir+"/*") if not l.endswith(".pdb")])

        if to_rm:
            os.system("rm "+to_rm)

        if skipped == len(decoys):
            cluster.progress('Error: Frodock could not generate decoys.')
            raise Exception('frodockview failed generating decoys for %s' %(input_dat))

    def create_frodock_decoys_for_coevomap(self, input_dat, outdir, receptor="", ligand="", version=2.1, decoys=list(range(1,10001)), decoy_names=[], extra_args=[]):
        """
        Run Frodock view to generate the decoys but use the wrapper instead of frodockview directly (1 job instead of N for N decoys)

        Inputs:
        input_dat: str, input .dat file
        version: int, frodock version (2.1, 3.1 or 3.2)
        output: str, output file, if not, results are returned as an array
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])

        Output:
        if output file not specified, array of scores
        """

        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if version==2.1:
            FRODOCKBIN = self.frodockbin21
            environment_module= DOCKER21
            gcc = FD21_EXT
        elif version==3.1:
            FRODOCKBIN = self.frodockbin31
            environment_module= DOCKER31
            gcc = FD31_EXT
        elif version==3.2:
            FRODOCKBIN = self.frodockbin32
            environment_module= DOCKER32
            gcc = FD32_EXT
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))
        FRODOCKVIEW = os.path.join(FRODOCKBIN, 'frodockview' + gcc)

        print(FRODOCKVIEW)

        if not len(decoy_names):
            decoy_names = ['complex.{}.pdb'.format(n) for n in decoys]
        print(decoy_names)

        if len(decoy_names) != len(decoys):
            print("Error: specified decoy output names not the same length as specified decoy list to generate")
            print(decoy_names[:10])
            print(decoys[:10])
            raise Exception("Error: specified decoy output names not the same length as specified decoy list to generate")

        outdir = os.path.abspath(outdir)
        try:
            os.mkdir(outdir)
        except:
            pass

        def regroup_list(lst):
            """
            function to regroup decoy numbers in consecutive batches
            e.g. [1,2,3,6,7,8,9] -> ['1-3', '6-9']
            """
            last_elt = lst[0]
            first_elt = lst[0]
            txt = str(first_elt)
            if len(lst) > 1:
                for elt in lst[1:]:
                    if elt != last_elt + 1:
                        if last_elt == first_elt:
                            txt+=","+str(elt)
                        else:
                            txt+='-'+str(last_elt)+","+str(elt)
                        first_elt = elt
                        last_elt = elt
                    else:
                        last_elt = elt
                if not txt.endswith(str(elt)):
                    txt+='-'+str(elt)
            return txt

        # run frodockview in batches when possible
        try:
            shutil.copy(receptor,outdir)
        except:
            pass
        try:
            shutil.copy(ligand,outdir)
        except:
            pass
        try:
            shutil.copy(input_dat,outdir)
        except:
            pass
        curdir = os.path.abspath(os.getcwd())
        os.chdir(outdir)

        print(outdir)
        decoy_str = regroup_list(decoys)

        args = ["{}".format(os.path.basename(input_dat)),
                "-p {}".format(os.path.basename(ligand)),
                "-r {}".format(decoy_str)]
        args += extra_args
        print(args)
        cluster.runTasks(FRODOCKVIEW, args, environment_module = environment_module, log_prefix = "frodock_decoys")

        # write complex pdbs because frodockview only generates the ligand poses
        f=open(os.path.basename(receptor))
        data_rec = [line for line in f if line.strip() and (line.startswith("ATOM") or line.startswith("HETATM") or line.startswith("TER"))]
        f.close()
        skipped = 0
        for decoy,name in zip(decoys,decoy_names):
            if not os.path.exists(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand)))):
                skipped += 1
                continue
            fo=open(name,"w")
            fo.write("".join(data_rec))
            fi=open(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand))))
            for line in fi:
                if line.strip() and (line.startswith("ATOM") or line.startswith("HETATM") or line.startswith("TER")):
                    fo.write(line)
            fo.close()
            fi.close()
            os.remove(("_{}".format(decoy)).join(os.path.splitext(os.path.basename(ligand))))

        if curdir != outdir:
            os.remove(os.path.basename(receptor))
            os.remove(os.path.basename(ligand))
            os.remove(os.path.basename(input_dat))

        os.chdir(curdir)
        to_rm = " ".join([l for l in glob.glob(outdir+"/*") if not l.endswith(".pdb")])

        if to_rm:
            os.system("rm "+to_rm)

        if skipped == len(decoys):
            cluster.progress('Error: Frodock could not generate decoys.')
            raise Exception('frodockview failed generating decoys for %s' %(input_dat))

    def run_frodock_scoring(self, input_dat, output_dat, workdir="", receptor="", ligand="", alig_rec="", alig_lig="", version=2.1, extra_args=['-r 1-10000']):
        """
        Run Frodock_sco 3.1 with mpi on decoys

        Inputs: 
        input_dat: str, input .dat file
        output_dat: str, output .dat file basename (<bn>_IES.dat, <bn>_Tobi.dat, <bn>_SPP.dat, <bn>_Fnat.dat will be created)
        alig_rec: str, coMSA for receptor
        alig_lig: str, coMSA for ligand
        version: int, frodock version (2.1 or 3.1)
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])
        """
        START = time.time()
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if version==2.1:
            extra_args += ['--old'] 
            extra_args = list(set(extra_args))
        if not workdir:
            workdir = self.workdir
        tmpdir = tools.make_tempdir(parent_dir=workdir)
        shutil.copy(receptor,tmpdir)
        shutil.copy(ligand,tmpdir)
        shutil.copy(input_dat,tmpdir)
        curdir = os.path.abspath(os.getcwd())
        tmpoutput = os.path.basename(output_dat)
        receptor=os.path.basename(receptor)
        ligand=os.path.basename(ligand)
        input_dat=os.path.basename(input_dat)

        if alig_rec and alig_lig:
            shutil.copy(alig_rec,tmpdir)
            shutil.copy(alig_lig,tmpdir)
            alig_rec=os.path.basename(alig_rec)
            alig_lig=os.path.basename(alig_lig)
        else:
            alig_rec = ""
            alig_lig = ""
        os.chdir(tmpdir)

        FRODOCKBIN = self.frodockbin31
        environment_module= DOCKER31
        FRODOCKSCO = os.path.join(FRODOCKBIN,'frodock_sco_gcc')
        soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')
        body3_mat = os.path.join(FRODOCKBIN,'3body.mat')
        body2_mat = os.path.join(FRODOCKBIN,'2body.mat')
        VdW_dat = os.path.join(FRODOCKBIN,'VdW.dat')

        args = ['-d {}'.format(input_dat),
                '-o {}'.format(tmpoutput),
                '{}'.format(receptor),
                '{}'.format(ligand),
                '-s {}'.format(soap_bin),
                '--pairwise {}'.format(body2_mat),
                '--triangle {}'.format(body3_mat),
                '--vdwIES {}'.format(VdW_dat)]
        if alig_lig and alig_rec:
            args += ['--fR {}'.format(alig_rec),
                     '--fL {}'.format(alig_lig)]
        args += extra_args
        args += ['2> /dev/null']

        if self._parallel:   
            cluster.runMPITasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_scoring")
        else:
            cluster.runTasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_scoring")
        if not os.path.exists(os.path.join(tmpdir,tmpoutput+'_IES.dat')):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not score decoys.')
            raise Exception('frodock_sco failed to score decoys for %s' %(input_dat))

        os.chdir(curdir)
        shutil.copy(os.path.join(tmpdir,tmpoutput+'_IES.dat'),os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir,tmpoutput+'_SPP.dat'),os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir,tmpoutput+'_Tobi.dat'),os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir,tmpoutput+'_fnat.dat'),os.path.dirname(os.path.abspath(output_dat)))
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)
        print("frodock_sco "+receptor+" "+ligand+" time: {:.0f} s".format(time.time()-START))


    def run_frodock_scoring_jobarray(self, input_dat, output_dat, workdir="", receptor=[], ligand=[], alig_rec="", alig_lig="", version=2.1, extra_args=['-r 1-10000'],jobarray=False):
        """
        Run Frodock_sco 3.1 and parallelise on cases using jobarrays

        Inputs: 
        input_dat: str, input .dat file
        output_dat: str, output .dat file basename (<bn>_IES.dat, <bn>_Tobi.dat, <bn>_SPP.dat, <bn>_Fnat.dat will be created)
        alig_rec: str, coMSA for receptor
        alig_lig: str, coMSA for ligand
        version: int, frodock version (2.1 or 3.1)
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])
        """
        START = time.time()
        curdir = os.path.abspath(os.getcwd())
        if not receptor or not ligand:
            receptor = [self.receptor]
            ligand = [self.ligand]
        if isinstance(receptor,str):
            receptor = [receptor]
            ligand = [ligand]
        if not workdir:
            workdir = self.workdir
        if version==2.1:
            extra_args += ['--old'] 
            extra_args = list(set(extra_args)) # remove redundant args

        tmpdir = tools.make_tempdir(parent_dir=workdir)
        for i,(lig,rec) in enumerate(zip(receptor,ligand)):
            shutil.copy(rec,tmpdir)
            shutil.copy(lig,tmpdir)
            receptor[i]=os.path.basename(receptor[i])
            ligand[i]=os.path.basename(ligand[i])
        shutil.copy(input_dat,tmpdir)

        tmpoutput = os.path.basename(output_dat)+'_{}'
        input_dat=os.path.basename(input_dat)

        if alig_rec and alig_lig:
            shutil.copy(alig_rec,tmpdir)
            shutil.copy(alig_lig,tmpdir)
            alig_rec=os.path.basename(alig_rec)
            alig_lig=os.path.basename(alig_lig)
        else:
            alig_rec = ""
            alig_lig = ""

        os.chdir(tmpdir)

        FRODOCKBIN = self.frodockbin31
        environment_module= DOCKER31
        FRODOCKSCO = os.path.join(FRODOCKBIN,'frodock_sco_gcc')
        soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')
        body3_mat = os.path.join(FRODOCKBIN,'3body.mat')
        body2_mat = os.path.join(FRODOCKBIN,'2body.mat')
        VdW_dat = os.path.join(FRODOCKBIN,'VdW.dat')

        if jobarray:
            for i,(rec,lig) in enumerate(zip(receptor,ligand)):
                args = ['-d {}'.format(input_dat),
                        '-o {}'.format(tmpoutput.format(i+1)),
                        '{}'.format(rec),
                        '{}'.format(lig),
                        '-s {}'.format(soap_bin),
                        '--pairwise {}'.format(body2_mat),
                        '--triangle {}'.format(body3_mat),
                        '--vdwIES {}'.format(VdW_dat)]
                if alig_lig and alig_rec:
                    args += ['--fR {}'.format(alig_rec),
                             '--fL {}'.format(alig_lig)]
                args += extra_args
                #args += ['2> /dev/null']
                f=open("input_{}.txt".format(i+1),"w")
                f.write(" ".join(args)+"\n")
                f.close()
            args = ["$(cat input_${SLURM_ARRAY_TASK_ID}.txt)"]
            print("jobarray "+FRODOCKSCO+" "+" ".join(args))
            cluster.runTasks(FRODOCKSCO, args, tasks = len(receptor), tasks_from = 1, environment_module = environment_module, log_prefix = "frodock_scoring")

        else:
            for i,(rec,lig) in enumerate(zip(receptor,ligand)):            
                args = ['-d {}'.format(input_dat),
                        '-o {}'.format(tmpoutput.format(i+1)),
                        '{}'.format(rec),
                        '{}'.format(lig),
                        '-s {}'.format(soap_bin),
                        '--pairwise {}'.format(body2_mat),
                        '--triangle {}'.format(body3_mat),
                        '--vdwIES {}'.format(VdW_dat)]
                if alig_lig and alig_rec:
                    args += ['--fR {}'.format(alig_rec),
                             '--fL {}'.format(alig_lig)]
                args += extra_args
                args += ['2> /dev/null']
                if self._parallel:
                    cluster.runMPITasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_scoring")
                else:
                    cluster.runTasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_scoring")
                print("sequentiel "+FRODOCKSCO+" "+" ".join(args))

        if sum([os.path.exists(os.path.join(tmpdir,tmpoutput.format(str(i+1)+'_IES.dat'))) for i in range(len(receptor))]) != len(receptor):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not score decoys.')
            raise Exception('frodock_sco failed to score decoys for %s' %(input_dat))

        os.chdir(curdir)
        for i in range(len(receptor)):
            shutil.copy(os.path.join(tmpdir,tmpoutput.format(str(i+1)+'_IES.dat')),os.path.dirname(os.path.abspath(output_dat)))
            shutil.copy(os.path.join(tmpdir,tmpoutput.format(str(i+1)+'_SPP.dat')),os.path.dirname(os.path.abspath(output_dat)))
            shutil.copy(os.path.join(tmpdir,tmpoutput.format(str(i+1)+'_Tobi.dat')),os.path.dirname(os.path.abspath(output_dat)))
            shutil.copy(os.path.join(tmpdir,tmpoutput.format(str(i+1)+'_fnat.dat')),os.path.dirname(os.path.abspath(output_dat)))
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)
        print("frodock_sco on "+str(len(receptor))+" pairs, time: {:.0f} s".format(time.time()-START))


    def run_pyIVS_scoring(self, input_dat, output_dat, receptor="", ligand='', alig_rec="", alig_lig="", version=2.1, extra_args=['-topN 10000']):
        """
        Run pyIVS --> function for us

        Inputs: 
        input_dat: str, input .dat file
        output_dat: str, output .dat file basename (<bn>_IES.dat, <bn>_SPP.dat will be created)
        alig_rec: str, coMSA for receptor
        alig_lig: str, coMSA for ligand
        version: int, frodock version (2.1 or 3.1)
        extra_args: [str], extra arguments for frodockview (e.g. ['-topN 10000'])
        """
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        python = config.get('scripts','python')
        pyIVS = config.get('scripts','pyIVS')

        if version==2.1:
            extra_args += ['-old'] 
            extra_args = list(set(extra_args))  

        args = ['-rec {}'.format(receptor),
                '-lig {}'.format(ligand),
                '-a1 {}'.format(alig_rec),
                '-a2 {}'.format(alig_lig),
                '-dat {}'.format(input_dat),
                '-out {}'.format(output_dat)]
        args += extra_args

        cluster.runTasks(python+" "+pyIVS, args)


    def run_frodock_coevolmap_scoring(self, input_dat, output_dat, d_coevol_map, workdir="", receptor="", ligand="", alig_rec="", alig_lig="", version=3.2, extra_args=['-r 1-10000000'], dthresh_contact=8.0):
        """
        Run Frodock_sco 3.2 only for scoring decoys with coevolution map with mpi on decoys
            much faster that using jobarrays...
        Inputs:
        input_dat: str, input .dat file
        output_dat: str, output .dat file basename (<bn>_CMAPnb.dat, <bn>_CMAPsco.dat, <bn>_Tobi.dat, <bn>_SPP.dat, <bn>_Fnat.dat will be created)
        alig_rec: str, coMSA for receptor
        alig_lig: str, coMSA for ligand
        version: int, frodock version (3.2 exclusively)
        contact_map_constraints: str, coevol_map_constraint file, written without any fasta sequence on top
        extra_args: [str], extra arguments for frodockview (e.g. ['-r 1-10000'])
        dthresh_contact : interatomic threshold distance to consider a contact respected
        """
        START = time.time()
        curdir = os.path.abspath(os.getcwd())
        if not receptor:
            receptor = self.receptor
        if not ligand:
            ligand = self.ligand
        if version==2.1:
            extra_args += ['--old']
            extra_args = list(set(extra_args))
        if not workdir:
            workdir = self.workdir

        path_sequence_a = d_coevol_map["path_sequence_a"]
        path_sequence_b = d_coevol_map["path_sequence_b"]
        path_cmap = d_coevol_map["path_cmap"]

        tmpdir = tools.make_tempdir(parent_dir=workdir)

        shutil.copy(receptor,tmpdir)
        shutil.copy(ligand,tmpdir)
        shutil.copy(input_dat,tmpdir)

        tmpoutput = os.path.basename(output_dat)
        receptor=os.path.basename(receptor)
        ligand=os.path.basename(ligand)
        input_dat=os.path.basename(input_dat)

        if alig_rec and alig_lig:
            shutil.copy(alig_rec,tmpdir)
            shutil.copy(alig_lig,tmpdir)
            alig_rec=os.path.basename(alig_rec)
            alig_lig=os.path.basename(alig_lig)
        else:
            alig_rec = ""
            alig_lig = ""
        os.chdir(tmpdir)

        FRODOCKBIN = self.frodockbin32
        environment_module= DOCKER32
        FRODOCKSCO = os.path.join(FRODOCKBIN,'frodock_sco_gcc')
        soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')

        args = ['-d {}'.format(input_dat),
                '-o {}'.format(tmpoutput),
                '{}'.format(receptor),
                '{}'.format(ligand),
                '-s {}'.format(soap_bin),
                '--fR {}'.format(path_sequence_a),
                '--fL {}'.format(path_sequence_b),
                '--cmap {}'.format(path_cmap),
                '--dthresh {}'.format(dthresh_contact)]

        args += extra_args
        #args += ['2> /dev/null']

        if self._parallel:
            cluster.runMPITasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_coevolmap_scoring")
        else:
            cluster.runTasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_coevolmap_scoring")
        print("Parallel run " + FRODOCKSCO + " " + " ".join(args))

        if not os.path.exists(os.path.join(tmpdir,tmpoutput+'_CMAPnb.dat')):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not score decoys.')
            raise Exception('frodock_sco failed to score decoys for %s' %(input_dat))

        os.chdir(curdir)
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_CMAPnb.dat'), os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_CMAPsco.dat'), os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_SPP.dat'), os.path.dirname(os.path.abspath(output_dat)))
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)
        print("frodock_sco "+receptor+" "+ligand+" time: {:.0f} s".format(time.time()-START))


    def run_frodock_coevolmap_scoring_jobarray(self, input_dat, output_dat, d_coevol_map, workdir="", receptor=[], ligand=[], fasta_rec="", fasta_lig="", version=3.2, extra_args=['-r 1-10000000'],dthresh_contact=8.0,jobarray=False):
        """
        USELESS METHOD NOW
        Run Frodock_sco 3.2 only for scoring decoys with coevolution map and parallelise on cases using jobarrays
            Originally encoded as a jobarray but erroneously... very slow (job array is best adapted for explicit running to prevent getting into the queue server several times)
            --> use the method without jobarray rather !!!
        Inputs:
        input_dat: str, input .dat file
        output_dat: str, output .dat file basename (<bn>_CMAPnb.dat, <bn>_CMAPsco.dat, <bn>_Tobi.dat, <bn>_SPP.dat, <bn>_Fnat.dat will be created)
        fasta_rec: str, fasta file for receptor
        fasta_lig: str, fasta file for ligand
        version: int, frodock version (3.2 exclusively)
        contact_map_constraints: str, coevol_map_constraint file, written without any fasta sequence on top
        extra_args: [str], extra arguments for frodockview -> by default set very large to include all the decoys ( ['-r 1-10000000'])
        dthresh_contact : interatomic threshold distance to consider a contact respected
        """
        START = time.time()
        curdir = os.path.abspath(os.getcwd())
        if not receptor or not ligand:
            receptor = [self.receptor]
            ligand = [self.ligand]
        if isinstance(receptor,str):
            receptor = [receptor]
            ligand = [ligand]
        if not workdir:
            workdir = self.workdir
        if version==2.1:
            extra_args += ['--old']
            extra_args = list(set(extra_args)) # remove redundant args
        path_sequence_a = d_coevol_map["path_sequence_a"]
        path_sequence_b = d_coevol_map["path_sequence_b"]
        path_cmap = d_coevol_map["path_cmap"]

        tmpdir = tools.make_tempdir(parent_dir=workdir)
        for i,(lig,rec) in enumerate(zip(receptor,ligand)):
            shutil.copy(rec,tmpdir)
            shutil.copy(lig,tmpdir)
            receptor[i]=os.path.basename(receptor[i])
            ligand[i]=os.path.basename(ligand[i])
        shutil.copy(input_dat,tmpdir)
        shutil.copy(path_sequence_a,tmpdir)
        shutil.copy(path_sequence_b,tmpdir)
        shutil.copy(path_cmap,tmpdir)

        tmpoutput = os.path.basename(output_dat)
        input_dat=os.path.basename(input_dat)

        os.chdir(tmpdir)

        # Switch to frodock3.2 (containing the coevol map module calculation and the symmetry aware code)
        FRODOCKBIN = self.frodockbin32
        environment_module= DOCKER32
        FRODOCKSCO = os.path.join(FRODOCKBIN,'frodock_sco_gcc')
        soap_bin = os.path.join(FRODOCKBIN,'soap30N.bin')

        if jobarray:
            for i,(rec,lig) in enumerate(zip(receptor,ligand)):
                if i == 0:
                    tag = ""
                else:
                    tag = "_{}".format(i+1)
                args = ['-d {}'.format(input_dat),
                        '-o {}'.format(tmpoutput+tag),
                        '{}'.format(rec),
                        '{}'.format(lig),
                        '-s {}'.format(soap_bin),
                        '--fR {}'.format(path_sequence_a),
                        '--fL {}'.format(path_sequence_b),
                        '--cmap {}'.format(path_cmap),
                        '--dthresh {}'.format(dthresh_contact)]

                args += extra_args
                #args += ['2> /dev/null']
                f=open("input_{}.txt".format(i+1),"w")
                f.write(" ".join(args)+"\n")
                f.close()
            args = ["$(cat input_${SLURM_ARRAY_TASK_ID}.txt)"]
            print("jobarray "+FRODOCKSCO+" "+" ".join(args))
            cluster.runTasks(FRODOCKSCO, args, tasks = len(receptor), tasks_from = 1, environment_module = environment_module, log_prefix = "frodock_coevolmap_scoring")
        else:
            for i,(rec,lig) in enumerate(zip(receptor,ligand)):
                if i == 0:
                    tag = ""
                else:
                    tag = "_{}".format(i+1)
                args = ['-d {}'.format(input_dat),
                        '-o {}'.format(tmpoutput+tag),
                        '{}'.format(rec),
                        '{}'.format(lig),
                        '-s {}'.format(soap_bin),
                        '--fR {}'.format(path_sequence_a),
                        '--fL {}'.format(path_sequence_b),
                        '--cmap {}'.format(path_cmap),
                        '--dthresh {}'.format(dthresh_contact)]
                args += extra_args
                args += ['2> /dev/null']
                if self._parallel:
                    cluster.runMPITasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_coevolmap_scoring")
                else:
                    cluster.runTasks(FRODOCKSCO, args, environment_module = environment_module, log_prefix = "frodock_coevolmap_scoring")
                print("sequentiel "+FRODOCKSCO+" "+" ".join(args))

        if not os.path.exists(os.path.join(tmpdir,tmpoutput+'_CMAPnb.dat')):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not score decoys.')
            raise Exception('frodock_sco failed to score decoys for %s' %(input_dat))

        os.chdir(curdir)
        #for i in range(len(receptor)):
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_CMAPnb.dat'),os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_CMAPsco.dat'),os.path.dirname(os.path.abspath(output_dat)))
        shutil.copy(os.path.join(tmpdir, tmpoutput + '_SPP.dat'), os.path.dirname(os.path.abspath(output_dat)))
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)
        print("frodock_sco on "+str(len(receptor))+" pairs, time: {:.0f} s".format(time.time()-START))

    def run_frodock_clustering_jobarray(self, list_input_dat, list_output_dat, workdir="", ligand="", version=2.1, extra_args=['-d 4'],jobarray=False):
        """
        Run Frodock clustering using job array

        Inputs:
        list_input_dat: list of str, input .dat file
        list_output_dat: str, output .dat file
        version: int, frodock version (2.1, 3.1 or 3.2)
        extra_args: extra frodockcluster args e.g. ['-d 4']
        """
        START = time.time()
        if not ligand:
            ligand = self.ligand
        if not workdir:
            workdir = self.workdir


        tmpdir = tools.make_tempdir(parent_dir=workdir)
        for i, (idat,odat) in enumerate(zip(list_input_dat,list_output_dat)):

            #ltmp.append(tmpdir)
            shutil.copy(ligand, tmpdir)
            shutil.copy(idat, tmpdir)
            list_input_dat[i] = os.path.basename(list_input_dat[i])
            list_output_dat[i] = os.path.basename(list_output_dat[i])
        curdir = os.path.abspath(os.getcwd())
        os.chdir(tmpdir)

        ligand = os.path.basename(ligand)

        if version == 2.1:
            FRODOCKBIN = self.frodockbin21
            gcc = FD21_EXT
            environment_module = DOCKER21
        elif version == 3.1:
            FRODOCKBIN = self.frodockbin31
            gcc = FD31_EXT
            environment_module = DOCKER31
        elif version == 3.2:
            FRODOCKBIN = self.frodockbin32
            gcc = FD32_EXT
            environment_module = DOCKER32
        else:
            print("Error: frodock version {} unknown".format(version))
            raise Exception("Error: frodock version {} unknown".format(version))

        FRODOCKCLUSTER = os.path.join(FRODOCKBIN, 'frodockcluster' + gcc)

        os.chdir(curdir)
        cluster.progress(' ... clustering decoys')
        os.chdir(tmpdir)
        print(' ... clustering decoys')

        if jobarray:
            for i, (idat, odat) in enumerate(zip(list_input_dat, list_output_dat)):
                # Clustering of predictions
                args = ["{}".format(idat),
                        "{}".format(ligand),
                        "-o {}".format(odat)] + extra_args
                f = open("input_{}.txt".format(i + 1), "w")
                f.write(" ".join(args) + "\n")
                f.close()
            args = ["$(cat input_${SLURM_ARRAY_TASK_ID}.txt)"]
            print("jobarray "+FRODOCKCLUSTER+" "+" ".join(args))
            cluster.runTasks(FRODOCKCLUSTER, args, tasks = len(list_input_dat), tasks_from = 1, environment_module = environment_module, log_prefix = "frodock_clustering")
        else:
            for i, (idat, odat) in enumerate(zip(list_input_dat, list_output_dat)):
                # Clustering of predictions
                args = ["{}".format(idat),
                        "{}".format(ligand),
                        "-o {}".format(odat)] + extra_args

                cluster.runTasks(FRODOCKCLUSTER, args, environment_module = environment_module, log_prefix = "frodock_clustering")

        if sum([os.path.exists(os.path.join(tmpdir,list_output_dat[i])) for i in range(len(list_input_dat))]) != len(list_input_dat):
            os.chdir(curdir)
            cluster.progress('Error: Frodock could not cluster .dat.')
            raise Exception('frodockcluster failed for %s and %s' % (list_input_dat, ligand))

        for i in range(len(list_input_dat)):
            shutil.copy(os.path.join(tmpdir, list_output_dat[i]),curdir)

        os.chdir(curdir)
        try:
            shutil.rmtree(tmpdir)
        except:
            # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
            # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
            time.sleep(2)
            shutil.rmtree(tmpdir)

        print("frodock_cluster on "+str(len(list_input_dat))+" frodock .dat files, time: {:.0f} s".format(time.time()-START))

