#! /usr/bin/env python3
'''
Created in Feb 2021, CQ: mini wrapper for gromacs_py based on PT's function in ccm
'''
try:
    import os, sys
    import shutil
    import optparse
    import glob
    from string import ascii_uppercase, ascii_lowercase
    import numpy as np
    import InterEvDock3.tools
    from collections import defaultdict
    # for python version compatibility
    try:
        import ConfigParser
    except:
        import configparser as ConfigParser

except ImportError:
    print("IMPORT ERROR")
    sys.exit(1)

chain_alphabet = ascii_uppercase+ascii_lowercase+"".join(map(str,range(10)))+"_"

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.dirname(os.path.abspath(__file__)),"config.ini"))

import PyPDB.PyPDB as PDB

# MTi cluster imports
import cluster.cluster as cluster

DOCKER_GROMACS = config.get('environments','gromacs')

def read_line(line):
    line = line.strip()
    try:
        header=line[0:6]
        atmnum=int(line[6:11])
        atmnam=line[12:16]
        altloc=line[16:17]
        resnam=line[17:20]
        ch=line[21:22]
        resnum=int(line[22:26])
        resadd=line[26:27]
        x=float(line[30:38])
        y=float(line[38:46])
        z=float(line[46:54])
    except:
        print("WEIRD PDB FORMAT")
        print(line)
        sys.exit()

    try:
        occ=float(line[54:60])
    except:
        occ=0.

    try:
        bfac=float(line[60:66])
    except:
        bfac=0.

    try:
        segid=line[72:76]
    except:
        segid=ch

    try:
        elt=line[76:78]
    except:
        elt=""

    try:
        chrg=line[78:80]
    except:
        chrg=""
    return (header,atmnum,atmnam,altloc,resnam,ch,resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg)

def read_pdb(pdbfile,exclude_hetatm=True):
    """This function reads and stores the initial chainID and residue numbers
    So that 3D gaps aren't closed by gromacs, any 2 residues with non-sequential residue numbers
    are given a new chainID
    Since Gromacs doesn't handle non-canonical atoms very well, we have to remove hetatms before minimisation with exclude_hetatm=True
    """
    d_pdb = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: [])))
    f=open(pdbfile)
    data=f.readlines()
    f.close()

    f=open(pdbfile,'w')
    prevresnum = ""
    newresnum = ""
    newchainidx = -1
    prevchain = ""
    COM = np.zeros(3)
    count = 0
    keep_hetatm = []
    for line in data:
        if not line.startswith("ATOM") and not line.startswith("HETATM"):
            f.write(line)
            continue

        if line.startswith("HETATM") and exclude_hetatm:
            keep_hetatm.append(line)
            continue
        header,atmnum,atmnam,altloc,resnam,ch,resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg = read_line(line)

        if ch != prevchain:
            prevchain = ch
            prevresnum = ""
            newchainidx += 1
        else:
            if prevresnum and resnum != prevresnum:
                # new residue, check if more than 1 appart
                if resnum-prevresnum > 1:
                    # new chain
                    newchainidx += 1
            prevresnum = resnum

        if atmnam.strip() == "CA":
            COM += [x,y,z] # centre of mass, only calculated on CA atoms that are present in d_pdb
            count += 1.
        d_pdb[chain_alphabet[newchainidx]][(resnum,resadd)][atmnam] = [header,atmnum,atmnam,altloc,resnam,ch,resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg]
        f.write("{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}      {:<4s}{:>2s}{:2s}\n".format(header,atmnum,atmnam,altloc,resnam,chain_alphabet[newchainidx],resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg))
    f.close()
    # return dict [chain] -> [(resnum,resadd)] -> [atom type] = [whole pdb line] and centre of mass on CA's
    return {k:{kk:vv for kk,vv in v.items()} for k,v in d_pdb.items()}, COM/count, keep_hetatm


def revert_to_original(d_pdb,pdbfile,COM,keep_hetatm=[]):
    """To get back from gromcas output to initial pdb output with recentralisation, renaming chains and adding missing hetatms
    """
    f=open(pdbfile)
    data = f.readlines()
    f.close()
    newCOM = np.zeros(3)
    count = 0
    #f=open(pdbfile+"tmp3.pdb","w")
    for line in data:
        if not line.startswith("ATOM") and not line.startswith("HETATM"):
            continue
        try:
            header,atmnum,atmnam,altloc,resnam,ch,resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg = read_line(line)
            newline = d_pdb[ch][(resnum,resadd)][atmnam][:8]+[x,y,z]+d_pdb[ch][(resnum,resadd)][atmnam][11:]
            #f.write("{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}      {:<4s}{:>2s}{:2s}\n".format(*newline))
            if atmnam.strip() == "CA":
                newCOM += [x,y,z]
                count += 1.
        except:
            # hydrogen atoms
            pass
    #f.close()
    newCOM = newCOM/count
    translation = COM - newCOM
    # to translate back to original centre: x,y,z - newCOM + COM
    f=open(pdbfile,"w")
    for line in data:
        if not line.startswith("ATOM") and not line.startswith("HETATM"):
            continue
        try:
            header,atmnum,atmnam,altloc,resnam,ch,resnum,resadd,x,y,z,occ,bfac,segid,elt,chrg = read_line(line)
            x,y,z = translation + [x,y,z]
            newline = d_pdb[ch][(resnum,resadd)][atmnam][:8]+[x,y,z]+d_pdb[ch][(resnum,resadd)][atmnam][11:]
            f.write("{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}      {:<4s}{:>2s}{:2s}\n".format(*newline))
        except:
            # hydrogen atoms
            pass
    for line in keep_hetatm:
        f.write(line)
    f.close()
    return


def run_final_minimization(pdb,output_mini,gro_dir_bin,verbose=True):
    """
    usage: minimize_pdb_and_cyclic.py [-h] -f F -n NAME [-dir OUT_DIR]
                                  [-m_steps MIN_STEPS] [-keep] [-cyclic]
                                  [-nt NT] [-ntmpi NTMPI] [-gpu_id GPUID]

    Minimize a cyclic peptide structure in 2 steps, the first step without bonds
    constraints and the second step with bonds constraints

    optional arguments:
      -h, --help          show this help message and exit
      -f F                Input PDB file
      -n NAME             Output file name
      -dir OUT_DIR        Output directory for intermediate files
      -m_steps MIN_STEPS  Minimisation nsteps, default=1000
      -keep               Flag to keep temporary files (without flag output
                          directory will be delete
      -cyclic             Flag to indicate if the peptide/protein is cyclic
      -nt NT              Total number of threads to start, default=0
      -ntmpi NTMPI        Number of thread-MPI threads to start, default=0
      -gpu_id GPUID       List of GPU device id-s to use, default=""
    """
    gro_dir_bin = os.path.abspath(gro_dir_bin)
    if not os.path.exists(gro_dir_bin):
        try:
                os.makedirs(gro_dir_bin)
        except:
                pass

    shutil.copy(pdb,gro_dir_bin)
    curdir = os.getcwd()
    output_mini = os.path.abspath(output_mini)
    os.chdir(gro_dir_bin)
    label = os.path.splitext(os.path.basename(pdb))[0]

    cmd = "minimize_pdb_and_cyclic.py"
    args = ["-f", "%s.pdb" % label,
            "-n", "%s-min" % label,
            "-nt 6" ]
    if verbose:
        sys.stdout.write("%s %s\n" % (cmd, " ".join(args)))

    count = 0
    while (count < 2) and (not os.path.exists("%s-min.pdb" % label)):
       cluster.runTasks(cmd, args, tasks = 1, tasks_from = 1, job_opts = "-c 6", environment_module = DOCKER_GROMACS; log_prefix = "wrapper_gromacs")
       count += 1

    try:
      os.rename("%s-min.pdb" % label,output_mini)
    except Exception as e:
      print(e)
      print("No output for minimisation - double check you don't have any atoms unknown to Gromacs")
      os.chdir(curdir)
      return 0
      
    os.chdir(curdir)
    return 1

if "__main__" == __name__:

    USAGE = "A small wrapper to run gromacs on a folder of pdbs (it overwrites existing structures with minimised ones)\n"+\
            "Example: this_script.py -d pdb_dir [or -f file_list_pdb.txt]\n"

    parser = optparse.OptionParser(usage = USAGE)
    
    parser.add_option('-d','--pdb_dir',
                      action="store",
                      dest='pdb_dir',
                      type='string',
                      help="Folder containing pdb files")

    parser.add_option('-f','--pdb_list',
                      action="store",
                      dest='pdb_list',
                      type='string',
                      help="File containing a list of pdb files")

    (options, args) = parser.parse_args(sys.argv)

    files = []
    if options.pdb_dir and os.path.exists(options.pdb_dir):
      files = glob.glob(options.pdb_dir+"/*pdb")
    elif options.pdb_list and os.path.exists(options.pdb_list):
      f=open(options.pdb_list)
      files=[l.strip() for l in f if l.strip() and not l.startswith("#") and os.path.exists(l.strip())]
      f.close()
    else:
      print("Unknown input")
      print(USAGE)
      sys.exit()


    # p = PDB.PDB(files[0],hetSkip=2)
    # chains = p.chnList()

    bin_dir = tools.make_tempdir()
    for pdb in files:
      #os.system("cp "+pdb+" "+pdb+"tmp0.pdb")
      d_pdb, COM, keep_hetatm = read_pdb(pdb)
      #os.system("cp "+pdb+" "+pdb+"tmp1.pdb")
      status = run_final_minimization(pdb,pdb,bin_dir)
      #os.system("cp "+pdb+" "+pdb+"tmp2.pdb")
      revert_to_original(d_pdb,pdb,COM,keep_hetatm)
      if status == 0:
        os.system("touch mini_failed.txt")
        # minimisation failed, it will fail for all the decoys, stop here to avoid wasting time for nothing
        break
      # p = PDB.PDB(pdb,hetSkip=2)
      # p.chnRename(p.chnList()+":"+chains)
      # p.out(pdb,hetSkip=2,HSkip=1)
    os.system("cp {}/*gro* ./".format(bin_dir))
    shutil.rmtree(bin_dir)
#    if status == 0:
#      sys.exit(1) # signal abnormal termination
