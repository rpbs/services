#!/usr/bin/env python
'''
Created on 8 dec. 2017

@author: Raphael GUEROIS
'''

import os
import sys
import tempfile
import shutil
import re
import csv
import optparse
import pkg_resources

# for python version compatibility
try:
    import ConfigParser
except:
    import configparser as ConfigParser
config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini")) # config file in the same folder as this script
import PyPDB.PyPDB as PDB

try:
    import InterEvDock3.tools as tools
except ImportError as import_error:
    print("This script is accompanied by other scripts in the same package.")
    print("ImportError: ", import_error)
    sys.exit(1)

def add_path_to_filename_if_required(workingdir,filename):
    if filename is None:
        return filename
    if os.path.dirname(filename) == '':
        output_filename = os.path.join(workingdir, os.path.basename(filename))
    else:
        output_filename = filename
    return output_filename


class ConvertMultimer():
    """
    A class to convert multichains PDB into a single chain renumbered PDB + fasta files + residues translation table
    """
    def __init__(self, input_pdb = None, output_pdb = None, output_chain = None, \
                 workdir = './', output_fasta = None, \
                 input_lres = None, output_lres = None, filename_resout = None, \
                 input_translation_table = None, output_translation_table = None, \
                 input_backward = None, output_backward = None, list_exclude = None, output_same_sequences = None,
                 dockingmethod="frodock", coevolmap = False):
        
        self.input_pdb         = input_pdb
        self.output_pdb        = output_pdb
        self.output_chain      = output_chain
        self.workdir           = workdir
        self.output_fasta      = output_fasta
        self.input_lres        = input_lres
        self.output_lres       = output_lres
        self.filename_resout   = filename_resout
        self.input_translation_table  = input_translation_table
        self.output_translation_table = output_translation_table
        self.input_backward    = input_backward # pdb file, typically a docking decoy that we need to convert back for users
        self.output_backward   = output_backward
        self.list_exclude      = list_exclude
        self.output_same_sequences = output_same_sequences
        self.dockingmethod     = dockingmethod
        self.coevolmap         = coevolmap

        #####################################################
        # Analyze which protocol should be followed
        # PROTOCOL 1
        # Running Protocol 1 : Standard docking
        #  - Check which chains are identical in the input_pdb. Store the info in self.dic_identical_chains.
        #  - File is ready for docking afterwards
        if self.input_pdb is not None and self.output_pdb is not None and self.output_chain is not None:
            self.FLAG_prepare_docking = True
        else:
            self.FLAG_prepare_docking = False
        # PROTOCOL 2
        # Running Protocol 2:
        #  - Convert residue indexes from the multi-chains input reference to the single chain docking input one
        if self.input_lres is not None: 
            self.FLAG_convert_lres_multi2single = True
            self.FLAG_convert_lres_single2multi = False
        # PROTOCOL 3
        # Running Protocol 3:
        #  - Convert residue indexes from the single chain docking input to the multi-chains input reference
        elif self.output_lres is not None:
            self.FLAG_convert_lres_multi2single = False
            self.FLAG_convert_lres_single2multi = True
        else:
            self.FLAG_convert_lres_multi2single = False
            self.FLAG_convert_lres_single2multi = False
        # PROTOCOL 4
        # Running Protocol 4:
        # - Useful to convert the selected decoys back to the original chain
        if self.input_backward:
            self.FLAG_convert_pdb_single2multi = True
        else:
            self.FLAG_convert_pdb_single2multi = False
        ######################################################


        # GLOBAL VARIABLES INSTANTIATED LATER ################
        self.dic_translation             = {}    # a dic instantiated either by reading pdb or the transinput table.
        self.dic_translation["forward"]  = {}
        self.dic_translation["backward"] = {}
        self.dic_substitute_chains        = {}    # a dic used when we want to change the chains at the reversion step
        self.dic_identical_chains        = {}    # a dic to check identical chains not to run the same blast twice
        self.dic_sequences               = {}    # a dic to store the sequences to the chains 
        self.set_chains_to_turn_hetatm   = set() # a set to recognize the chains that should be turned to HETATM such as DNA and so on
        
        ######################################################
        
        # METHODS CALLING

        ### INITIALIZATION STEP ##############################          
        
        # EITHER General method enumerating the required operations in case there is a pdb file input
        if self.input_pdb is not None:
            print("Detected input type: pdb file")
            if self.coevolmap:
                print("Running pdb file conversion with the coevolmap option: "
                      " -> Input pdb will be converted in a format using chains and segids "
                      "to enable symmetry aware account of constraints")
            self.read_pdb(use_coevolmap_format=self.coevolmap)
            
        # OR General method enumerating the required operations in case there is a translation table input
        elif self.input_translation_table is not None:
            print("Detected input type: translation table")
            self.read_translation_table()
            
        ######################################################
        
        if self.list_exclude:
            # In case we don't want certain chains id 
            #   to be used upon backward reversion of pdb 
            #   (cause they are redundant with the chains in the partner).
            self.define_new_chains()
        
        ##################
        ### PROTOCOL 1 ###
        ##################
        
        if self.FLAG_prepare_docking:
            """
            Standard protocol for docking with multimeric assemblies
            """
            print("Running Protocol 1: Prepare the structures and files for docking")
            # 1 - Check which chains are identical in the input_pdb. Store the info in self.dic_identical_chains.
            #     -> Create self.dic_identical_chains, such that dic[chain] -> set(redundant chains)
            self.dic_identical_chains = self.check_homomer()
            self.write_identical_sequence()
            shutil.copy(self.path_pdb_renum_singlechain, self.output_pdb)

            #tools.clean_subunit_pdb_for_docking(self.path_pdb_renum_singlechain, self.output_pdb, keepHOH = False, force_hetatm = set_chain_hetatm)
                
            # 4 - Run the docking

        ##################
        ### PROTOCOL 2 ###
        ##################
        
        if self.FLAG_convert_lres_multi2single:
            """
            Useful to translate docking constraints
            """
            print("Running Protocol 2: Convert residue indexes from the multi-chains input reference to the single chain docking input one")
            self.translate_forward()
            
        ##################
        ### PROTOCOL 3 ###
        ##################

        if self.FLAG_convert_lres_single2multi:
            """
            Useful to translate the consensus residues part of the interface
            """
            print("Running Protocol 3: Convert residue indexes from the single chain docking input to the multi-chains input reference")
            self.translate_backward()
        
        
        ##################
        ### PROTOCOL 4 ###
        ##################
        
        if self.FLAG_convert_pdb_single2multi:
            """
            Useful to convert the selected decoys back to the original chain
            """
            if not self.coevolmap:
                print("Running Protocol 4: Convert pdb file from single chain docking input to the multi-chains input reference")
                self.convert_pdb_back(self.input_backward,self.output_backward)
            else:
                print(
                    "Running Protocol 4: Convert pdb file from single chain input (with the format using segids for coevolution)"
                    " to the multi-chains input reference")
                self.convert_pdb_back_using_coevolmap(self.input_backward,self.output_backward)

        # print("Process finished")

    def read_pdb(self, use_coevolmap_format = False):
        """
        Analyse the input pdb
            1. Analyze the chains in input_pdb
            2. For every chains, dump their sequence in fasta format (self.write_fasta())
            3. Identify the identical chains to limit blast search
        If use_coevolmap_format is True, the last command calls a specific method to handle symmetries in the case of the use of coevolution map
        """
        # 1 - Analyze the pdb. Store chains in self.list_chains.
        # p = PDB.PDB(self.input_pdb,hetSkip=2)
        # self.list_chains = [c for c in p.chnList()]
        self.list_chains = tools.get_chains_and_exclude_chains_only_solvent(self.input_pdb)
        print("List of detected chains: ",self.list_chains)
        for chain in self.list_chains:
            # chains name are conserved except if list_exclude is used to define a list of chains to avoid in the reverse process
            self.dic_substitute_chains[chain] = chain
            
        # 2 - Extract sequences from the pdb input file. Dump as fasta
        #     -> Create self.dic_sequences
        print("Analyzing the sequences")
        self.analyze_sequence(use_coevolmap_format=use_coevolmap_format)
        
        # 3 - Turn to HETATM the chains that contains 90 % of X residues in their sequence
        #        Generally is for DNA
        for chain in self.list_chains:
            if self.dic_sequences[chain]['chain_type'] == 'hetatm' and self.dockingmethod == 'frodock':
                print("Chain %s will be turned into HETATM"%(chain))
                self.set_chains_to_turn_hetatm.add(chain)
        
        # 4 - Generate translation table from original indexing to renumbered single chain indexing
        #       Create the transouput file
        #      + self.dic_translation with keys 'forward' and 'backward' subdic to run conversions with format <AAtype>.<resindex>.<chain>
        print("Generates the pdbinput and the translation table")
        if use_coevolmap_format:
            self.dic_translation, self.path_pdb_renum_singlechain = self.generate_translation_table_and_pdb_for_docking_using_coevolmap()
        else:
            self.dic_translation, self.path_pdb_renum_singlechain = self.generate_translation_table_and_pdb_for_docking()

    def read_translation_table(self):
        """
        Instead of using a pdb to define the conversion between pdbs, we use here the translation table generated at an initial step 
            and use it to : 1 - convert a list of residues
                            2 - convert a docked single chain pdb back into a multi-chain renumbered pdb 
        """
        fin = open(self.input_translation_table).readlines()
        self.list_chains = []
        for l in fin:
            s = l.strip('\n').split('\t')
            self.dic_translation["forward"][s[0]] = s[1]
            self.dic_translation["backward"][s[1]] = s[0]
            s0short = '.'.join(s[0].split('.')[1:])
            s1short = '.'.join(s[1].split('.')[1:])
            self.dic_translation["forward"][s0short] = s[1]
            self.dic_translation["backward"][s1short] = s[0]
            chain = s[0].split('.')[2]
            if chain not in self.list_chains:
                self.list_chains.append(chain)
                self.dic_substitute_chains[chain] = chain
            self.output_chain = s[1].split('.')[2]

    def analyze_sequence(self, use_coevolmap_format = False):
        """
        Get the sequences in the chains of self.input_pdb.
            -> Create self.dic_sequences to store the infos as lists 
               + Write the fasta files
        """
        # p=PDB.PDB(self.input_pdb)
        if use_coevolmap_format:
            use_biopython = False
        else:
            use_biopython = True
        for chain in self.list_chains:
            self.dic_sequences[chain] = {}
            # AA_seq_list = p[chain].aaseq()
            # resi_list = p[chain].resNumList()

            AA_seq_list, resi_list = tools.get_chain_res_list_and_resid_list(self.input_pdb, given_chain=chain, ignore_hetatm=False, use_biopython=use_biopython)
            full_path_fasta = self.write_fasta(AA_seq_list, chain)
            self.dic_sequences[chain]['path_fasta'] = full_path_fasta
            self.dic_sequences[chain]['list_AA'] = AA_seq_list
            self.dic_sequences[chain]['list_resi'] = resi_list
            
            # Analyse if seq is pure DNA
            seqAA = "".join(AA_seq_list)
            nbX = seqAA.count("X")
            if nbX > len(AA_seq_list)*.9:
                self.dic_sequences[chain]['chain_type'] = "hetatm"
            else:
                self.dic_sequences[chain]['chain_type'] = "protein"
                    
    def write_fasta(self, list_AA, suffix):
        """
        Write a fasta file from a list of single letter amino acids.
             - Suffix is added to the out_filename
             - Full path of the created fasta file is returned
        """
        out_fasta_filename = "%s_chain_%s.fasta" %(self.output_fasta,suffix)
        path_out_fasta_filename = out_fasta_filename # os.path.join(self.workdir,out_fasta_filename)
        fout = open(path_out_fasta_filename,"w")
        header = "> %s|chain_%s\n"%(os.path.basename(self.input_pdb),suffix)
        sequence = "".join(list_AA) + "\n"
        fout.write(header)
        fout.write(sequence)
        fout.close()
        return path_out_fasta_filename
    
    def write_identical_sequence(self):
        """
        Write a file listing in the first column  : every sequence fasta filename 
                             in the second column : the ref sequence that should be used for blast
        """
        fout_redundant = open(self.output_same_sequences,"w")
        for chain in self.list_chains:
            chain_filename = self.dic_sequences[chain]['path_fasta']
            chain_type = self.dic_sequences[chain]['chain_type']
            chain_length = len(self.dic_sequences[chain]['list_AA'])
            if chain in self.dic_identical_chains:
                representative_chain_filename = self.dic_sequences[self.dic_identical_chains[chain]]['path_fasta']
                fout_redundant.write("%s\t%s\t%s\t%d\n"%(chain_filename,representative_chain_filename,chain_type, chain_length))
            else:
                fout_redundant.write("%s\t%s\t%s\t%d\n"%(chain_filename,chain_filename,chain_type, chain_length))
        fout_redundant.close()

    def check_homomer(self):
        """
        Detects identical chains to run only once the blast
        Returns results in dic_identical_chains
        """

        dic_is_represented_by = {}
        chain_treated = set()

        dic_ref_redundant = {}
        # HERE THINK HOW TO BEST ENCODE REDUNDANCY
        for ii, ch1 in enumerate(self.list_chains):
            is_singleton = True
            if ch1 in chain_treated:
                continue
            chain_treated.add(ch1)
            for ch2 in self.list_chains[ii+1:]:
                # So far we impose strict identity but we couldimagine that we are more tolerant with coverage
                # We could have 100% seq id over at least 50 AA to decide that they are redundant
                if self.dic_sequences[ch1]['list_AA'] == self.dic_sequences[ch2]['list_AA']:
                    print("Redundancy: chain %s and %s were detected as identical in %s"%(ch1,ch2,self.input_pdb))
                    chain_treated.add(ch2)
                    dic_is_represented_by[ch2] = ch1
                    dic_is_represented_by[ch1] = ch1
                    is_singleton = False
            if is_singleton == True:
                dic_is_represented_by[ch1] = ch1
                    
        return dic_is_represented_by

    def generate_translation_table_and_pdb_for_docking(self):
        """
        Writes the pdb file used in docking changing the pdb input in a single chain  :
            - All the residues have the same chain.
            - Residue indexes are renumbered from 1 to N
        Creates a file containing 2 columns, each line is for an AA,
        col1: AA index from the input (format <AAtype>.<resindex>.<chain>)
        col2: AA index in renumbered/single_chain pdb used in docking (format <AAtype>.<resindex>.<chain>)
        """
        fout = open(self.output_translation_table,"w")
        str_chains = "".join(self.list_chains)
        dic_translation = {}
        dic_translation['forward'] = {}
        dic_translation['backward'] = {}
        
        # To create the translation Table we generate the target pdb and then map to each other the residue list of the original and target file
        
        # 1 - Renumber the pdb from 1 to N whatever the chains
        pdb_renumbered = os.path.join(self.workdir,os.path.basename(self.input_pdb)+'_ren.pdb')
        tools.renumber_pdb_from_one_to_last_res(self.input_pdb, pdb_renumbered, ignore_hetatm=False)
        
        # 2 - Switch some chains to HETATM if required
        print("The pdb will be formatted for compatibility with %s" %(self.dockingmethod))

        if self.dockingmethod == 'zdock':
            set_chain_hetatm = False
        else:
            if len(self.set_chains_to_turn_hetatm) == 0:
                set_chain_hetatm = False
            else:
                set_chain_hetatm = self.set_chains_to_turn_hetatm            
        tools.clean_subunit_pdb_for_docking(pdb_renumbered, pdb_renumbered, keepHOH = False, force_hetatm = set_chain_hetatm)        
        # we use (keepHOH = False) so that it behaves the same as in the clean_subunit_pdb_for_docking function in one_click_InterEvDock2 for monomer
        
        # Third, turn it into a single chain
        str_newchain = "%s"%(self.output_chain)*len(self.list_chains)
        chain_conversion = "%s:%s"%(str_chains,str_newchain)
        path_pdb_renum_singlechain = os.path.join(self.workdir,os.path.basename(self.input_pdb)+'_ren_sc.pdb')
        pdb_renumbered_singlechain = tools.change_chainID(pdb_renumbered,chain_conversion,out_pdb = path_pdb_renum_singlechain)
        
        # Get the lists for the converted pdb
        listAA_pdbsc, listresi_pdbsc = tools.get_chain_res_list_and_resid_list(path_pdb_renum_singlechain, given_chain = self.output_chain, ignore_hetatm=False)
        
        # Map the 2 residue lists together
        absolute_index = 0
        ii = -1
        for chain in self.list_chains:
            absolute_index += ii+1
            for ii,AA in enumerate(self.dic_sequences[chain]["list_AA"]):
                res_index = self.dic_sequences[chain]["list_resi"][ii]
                original_residue = "%s.%s.%s" %(AA, res_index, chain)
                translated_residue = "%s.%s.%s" %(listAA_pdbsc[absolute_index+ii], listresi_pdbsc[absolute_index+ii], self.output_chain)
                original_residue_short = "%s.%s" %(res_index, chain)
                translated_residue_short = "%s.%s" %(listresi_pdbsc[absolute_index+ii], self.output_chain)
                dic_translation['forward'][original_residue]    = translated_residue
                dic_translation['backward'][translated_residue] = original_residue
                dic_translation['forward'][original_residue_short]    = translated_residue
                dic_translation['backward'][translated_residue_short] = original_residue
                fout.write("%s\t%s\n" %(original_residue, translated_residue))
        fout.close()
        return dic_translation,path_pdb_renum_singlechain

    def generate_translation_table_and_pdb_for_docking_using_coevolmap(self):
        """
        Writes the pdb file used in docking with the format adapted to account for symmetries in frodock scoring in the context of a coevolmap:
            - True chains are written in the segids, chain is identical for every monomer.
            - Every monomer is renumbered to have the same residue indexes which will be matched by the coevolmap contacts
        Creates a file containing 2 columns, each line is for an AA,
        col1: AA index from the input (format <AAtype>.<resindex>.<chain>)
        col2: AA index in renumbered/single_chain pdb used in docking (format <AAtype>.<resindex>.<chain>)
        """
        fout = open(self.output_translation_table, "w")
        str_chains = "".join(self.list_chains)
        dic_translation = {}
        dic_translation['forward'] = {}
        dic_translation['backward'] = {}

        # To create the translation Table we generate the target pdb and then map to each other the residue list of the original and target file

        # 1 - Add the chainid into the segid column
        path_pdb_renum_chain2segid = os.path.join(self.workdir, os.path.basename(self.input_pdb) + '_ch2segi.pdb')
        tools.copy_chainID2segID(self.input_pdb, out_pdb=path_pdb_renum_chain2segid)

        # 2 - Renumber every chain from 1 to N. no TER within the same chain even in case of jumps
        pdb_renumbered = os.path.join(self.workdir, os.path.basename(self.input_pdb) + '_ren.pdb')
        tools.renumber_pdb_all_chains_from_one(path_pdb_renum_chain2segid, pdb_renumbered, ignore_hetatm=False)

        # 3 - Switch some chains to HETATM if required
        print("The pdb will be formatted for compatibility with %s" % (self.dockingmethod))
        if self.dockingmethod == 'zdock':
            set_chain_hetatm = False
        else:
            if len(self.set_chains_to_turn_hetatm) == 0:
                set_chain_hetatm = False
            else:
                set_chain_hetatm = self.set_chains_to_turn_hetatm
        tools.clean_subunit_pdb_for_docking(pdb_renumbered, pdb_renumbered, keepHOH=False,
                                            force_hetatm=set_chain_hetatm)
        # we use (keepHOH = False) so that it behaves the same as in the clean_subunit_pdb_for_docking function in one_click_InterEvDock2 for monomer


        # 4 - Convert the whole pdb into a single chain
        # print(self.list_chains)
        str_newchain = "%s" % (self.output_chain) * len(self.list_chains)
        chain_conversion = "%s:%s" % (str_chains, str_newchain)
        path_pdb_renum_singlechain = os.path.join(self.workdir, os.path.basename(self.input_pdb) + '_ren_sc.pdb')
        pdb_renumbered_singlechain = tools.change_chainID(pdb_renumbered, chain_conversion,
                                                          out_pdb=path_pdb_renum_singlechain)
        # 5 - Get the lists for the converted pdb
        # We can't use biopython because it doesn't know about segids.
        listAA_pdbsc, listresi_pdbsc = tools.get_chain_res_list_and_resid_list(path_pdb_renum_singlechain,
                                                                               given_chain=self.output_chain,
                                                                               ignore_hetatm=False, use_biopython=False)

        # Map the 2 residue lists together
        absolute_index = 0
        ii = -1
        for chain in self.list_chains:
            absolute_index += ii + 1
            for ii, AA in enumerate(self.dic_sequences[chain]["list_AA"]):
                res_index = self.dic_sequences[chain]["list_resi"][ii]
                original_residue = "%s.%s.%s" % (AA, res_index, chain)
                translated_residue = "%s.%s.%s" % (
                listAA_pdbsc[absolute_index + ii], listresi_pdbsc[absolute_index + ii], self.output_chain)
                original_residue_short = "%s.%s" % (res_index, chain)
                translated_residue_short = "%s.%s" % (listresi_pdbsc[absolute_index + ii], self.output_chain)
                dic_translation['forward'][original_residue] = translated_residue
                dic_translation['backward'][translated_residue] = original_residue
                dic_translation['forward'][original_residue_short] = translated_residue
                dic_translation['backward'][translated_residue_short] = original_residue
                fout.write("%s\t%s\n" % (original_residue, translated_residue))
        fout.close()
        return dic_translation, path_pdb_renum_singlechain

    def convert_pdb_back(self,inpdb,outpdb):
        """
        Major method of protocol 4 to reverse a single chain pdb back to its original multi-chain pdb
        """
        # change col 21->25 B%4s
        fin = open(inpdb).readlines()
        fout = open(outpdb,"w")
        l_previous_isTER = False
        previous_chain = None
        for l in fin:
            newl = ""
            if not l[0:4] == "ATOM" and not l[0:6] == "HETATM":
                newl += l
                if l[0:3] == "TER":
                    l_previous_isTER = True
                else:
                    l_previous_isTER = False
            else:
                chain = tools.get_chain_from_pdb_line(l)
                resid = tools.get_resid_from_pdb_line(l)
                AA    = tools.get_AA_from_pdb_line(l)
                if chain and resid:
                    if l[0:4] == "ATOM":
                        convert_in = "%s.%s.%s"%(AA,resid,chain)
                    else:
                        convert_in = "X.%s.%s"%(resid,chain)
                    convert_out = self.dic_translation["backward"][convert_in]
                    list_convert = convert_out.split('.')
                    
                    if l[0:4] == "ATOM" and AA != list_convert[0]:
                        print("ERROR: Problem in converting the docked decoys (chain and resid) as in the original input ==> Amino acids are not the same !!")
                    
                    # If no chain was excluded, then the chain is left unchanged. Otherwise a new chain not excluded is chosen.
                    chain_to_substitute = self.dic_substitute_chains[list_convert[2]]
                    
                    substitution = "%s%4s"%(chain_to_substitute,list_convert[1])
                    l_substituted = tools.change_columns_from_pdb_line(l, 22, substitution)
                    if previous_chain and chain_to_substitute != previous_chain:
                        # We are in a new chain so we check whether the TER was there or we add it.
                        if not l_previous_isTER:
                            newl += "TER\n"
                    previous_chain = chain_to_substitute
                    newl += l_substituted
                l_previous_isTER = False
            fout.write(newl)

    def convert_pdb_back_using_coevolmap(self, inpdb, outpdb):
        """
        Method of protocol 4 to reverse a single chain pdb back to its original multi-chain pdb
            in case a coevol_map was used.
        This means that if oligomeric the structure contains several time the same chain not separated by TER
            but distinguished by their segid
        """

        # change col 21->25 B%4s
        fin = open(inpdb).readlines()
        fout = open(outpdb, "w")
        previous_segid = None
        first_line_of_segi = False
        for l in fin:
            newl = ""
            if not l[0:4] == "ATOM" and not l[0:6] == "HETATM":
                newl += l
            else:
                chain = tools.get_chain_from_pdb_line(l)
                resid = tools.get_resid_from_pdb_line(l)
                AA = tools.get_AA_from_pdb_line(l)
                segi = tools.get_segi_from_pdb_line(l)
                if segi != previous_segid:
                    previous_segid = segi
                    new_chain = True
                    newl += "TER\n"
                    first_line_of_segi = True
                if chain and resid:
                    if l[0:4] == "ATOM":
                        convert_in = "%s.%s.%s" % (AA, resid, chain)
                    else:
                        convert_in = "X.%s.%s" % (resid, chain)
                    # !! Translation dictionary is correct for residue type and index but not for chain because
                    # the same chain encoded for several original chain and the last one was kept !!
                    # TODO: [RG] We should adapt dic_translation structure so that it incorporates a list of possible chain
                    # TODO:      value for the same key (chains are repeated in sym coevol map cases)
                    convert_out = self.dic_translation["backward"][convert_in]
                    list_convert = convert_out.split('.')

                    if l[0:4] == "ATOM" and AA != list_convert[0]:
                        print("ERROR: Problem in converting the docked decoys "
                              "(chain and resid) as in the original input ==> Amino acids are not the same !!")

                    # If no chain was excluded, then the chain is left unchanged. Otherwise a new chain not excluded is chosen.
                    # We use segi to know the identity of the original chain rather than the information contained in the dictionary
                    chain_to_substitute = self.dic_substitute_chains[segi]
                    if first_line_of_segi:
                        print('for segi {}, chain to substitute is {}'.format(segi, chain_to_substitute))
                        first_line_of_segi = False
                    substitution = "%s%4s" % (chain_to_substitute, list_convert[1])
                    # We replace the previous segid by the chain which should be either A or B
                    l_substituted = tools.change_columns_from_pdb_line(l, 73, chain)
                    l_substituted = tools.change_columns_from_pdb_line(l_substituted, 22, substitution)

                    newl += l_substituted
            fout.write(newl)

    def define_new_chains(self):
        """
        Modify the self.dic_substitute_chains so that excluded chains are substituted by the proper chain id (protocol 4)
        """
        possible_chains = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        for chain in self.list_chains:
            if chain in self.list_exclude:
                for chain_possible in possible_chains:
                    if chain_possible not in self.list_exclude:
                        self.dic_substitute_chains[chain] = chain_possible
                        self.list_exclude.append(chain_possible)
                        break
                        
            
    def translate_forward(self):
        """
        From a simple list of index (i.e. 10A,11A,12C,...) in the multi-chain pdb, 
          -> returns a list of correspondance in the single chain pdb
        """
        lres_forward = self.input_lres.split(",")
        lres_output = []
        fout = open(self.filename_resout, "w")
        for res in lres_forward:
            resi = res[:-1]
            chain = res[-1]
            inres = "%s.%s"%(resi,chain)
            outres = self.dic_translation['forward'][inres]
            lres_output.append(outres)
            fout.write("%s\t%s\n"%(res,outres))
        fout.close()
        return lres_output

    def translate_backward(self):
        """
        From a simple list of index (i.e. 10,11,12,...) in the single chain pdb, 
          -> returns a list of correspondance in the multi-chain pdb
        """
        lres_backward = self.output_lres.split(",")
        lres_output = []
        fout = open(self.filename_resout, "w")
        for res in lres_backward:
            resi = res
            chain = self.output_chain
            inres = "%s.%s"%(resi,chain)
            
            outres = self.dic_translation['backward'][inres]
            chain_to_substitute = self.dic_substitute_chains[outres.split('.')[2]]
            outres_with_possible_exlusion = "%s.%s.%s"%(outres.split('.')[0], outres.split('.')[1], chain_to_substitute)
            lres_output.append(outres)
            fout.write("%s\t%s\n"%(res,outres_with_possible_exlusion))
        fout.close()
        return lres_output





def Main():


    USAGE = "A generic pipeline to handle multimeric inputs/ouputs for docking\n"+\
            " 4 Protocols available: \n"+\
            "PROTOCOL 1. to convert a multi chain pdb into a renumbered single chain pdb:\n"+\
            "> ConvertMultimerForDocking.py -i input.pdb -o output.pdb -c chain_output (triggered by the -o and -c option)\n"+\
            "                            [-f fasta_rootname_output (default='pdbextract_chain')]\n"+\
            "                            [-T translation_table_output (default='residues_translation_table.out')]\n"+\
            "                            [-d workdir] (default = ./)\n"+\
            "                            [-z zdock/frodock ] (default = frodock)\n"+\
            "                            \n"+\
            "PROTOCOL2. case to translate a list of residues from the input multichain pdb into the corresponding residues in single chain pdb (triggered by the -l option) :\n"+\
            "> ConvertMultimerForDocking.py [EITHER -i input.pdb OR -t input_translation_table ] -l 12A,20A... -O file_list_residues_converted \n\n"+\
            "PROTOCOL 3. case to translate a list of residues from the docked singlechain pdb into the original input residues of the multichains pdb (triggered by the -L option):\n"+\
            "> ConvertMultimerForDocking.py [EITHER -i input.pdb OR -t input_translation_table ] -L 12,20,... -O file_list_residues_converted [-e A,B,... to exclude chains]  \n\n"+\
            "PROTOCOL 4. case to translate a docked chain of pdb into the original input residues (triggered by the -L option):\n"+\
            "> ConvertMultimerForDocking.py [EITHER -i input.pdb OR -t input_translation_table] -b input_docked.pdb -r reversed_pdb.pdb  [-e A,B,... to exclude chains] \n"+\
            "\n"
    parser = optparse.OptionParser(usage=USAGE)
    parser.add_option('-i','--ipdb',action="store",dest='input_pdb',type='string',
                      help="Input PDB with different chains labels for every chain to consider",default=None)
    parser.add_option('-c','--chain_out',action="store",dest='output_chain',type='string',default=None,
                      help="Name of the chain in the outpdb PDB")
    parser.add_option('-o','--opdb',action="store",dest='output_pdb',type='string',default=None,
                      help="Outpdb PDB with a single chain for docking")

    parser.add_option('-d','--dir',action="store",dest='workdir',type='string',
                      help="full path for files creation workdirectory",default=None)
    parser.add_option('-f','--fasout',action="store",dest='output_fasta',type='string',
                      help="Rootname of the fasta output files corresponding the individual chains and the fusion of the chains",default="pdbextract_chain")
    parser.add_option('-l','--lresin',action="store",dest='input_lres',type='string',
                      help="List of residues to convert written as <resindex1><chain1>,<resindex2><chain2>,...", default=None)
    parser.add_option('-L','--Lresout',action="store",dest='output_lres',type='string',
                      help="List of residues to convert backward from the docked structure written as <resindex1>,<resindex2>,...", default=None)
    parser.add_option('-t','--transtab',action="store",dest='input_translation_table',type='string',
                      help="Input Name of the file mapping the input residues to the output ones (do not need to provide the pdb)", default=None)
    parser.add_option('-T','--TransTab',action="store",dest='output_translation_table',type='string',
                      help="Output Name of the file mapping the input residues to the output ones", default="residues_translation_table.out")
    parser.add_option('-O','--transresout',action="store",dest='filename_resout',type='string',
                      help="Output Name of the file listing the converted residues requested", default=None)
    parser.add_option('-b','--backwardpdb',action="store",dest='input_backward',type='string',
                      help="Input Name of the pdbfile which has to be reverted backward with original chain and resids", default=None)
    parser.add_option('-r','--reversedpdb',action="store",dest='output_backward',type='string',
                      help="Output Name of the pdbfile once reverted backward with original chain and resids", default=None)
    parser.add_option('-e','--excludechain',action="store",dest='exclude_chain',type='string',
                      help="Chains written as A,B,C,... to exclude in reversion of docked pdb because redundant with other chains in the partner", default=None)
    parser.add_option('-s','--samesequences',action="store",dest='output_same_sequences',type='string',
                      help="Chains are reported in col1 and their representative chain also", default='representative_chain.out')
    parser.add_option('-z','--dockingmethod',action="store",dest='dockingmethod',type='choice',
                      help="Chains are reported in col1 and their representative chain also", default='frodock',choices=['zdock','frodock'])
    parser.add_option('-m','--coevolmap',action="store_true",dest='coevolmap',
                      help="Should the multimer be encoded for treatment of coevol map with segid encoding of the chains?", default=False)


    if len(sys.argv) == 1:
        print(USAGE)
        print("type -h or --help for more help information")
        sys.exit(1)

    (options, args) = parser.parse_args(sys.argv)

    if len(args) > 1: # sys.argv[0] is the name of this program
        print("Leftover arguments:" + str(args[1:]))

    if options.workdir is None:
        if options.input_pdb:
            options.workdir = os.path.dirname(options.input_pdb)
        elif options.input_translation_table:
            options.workdir = os.path.dirname(options.input_translation_table)
        else:
            options.workdir = os.getcwd()
    if options.workdir == '.' or options.workdir == '':
        options.workdir = os.getcwd()

    # Make sure the input is in workdir
    if options.input_pdb:
        input_pdb = add_path_to_filename_if_required(options.workdir, options.input_pdb)
        print(input_pdb)
        if not os.path.isfile(input_pdb):
            print("Could not find the input pdb file")
            print("type -h or --help for more help information")
            sys.exit(1)
    else:
        input_pdb = None


        
    # for all modes
    output_chain      = options.output_chain
    options.workdir   = options.workdir
    input_translation_table  = options.input_translation_table

    output_pdb        = add_path_to_filename_if_required(options.workdir, options.output_pdb)
    output_fasta      = add_path_to_filename_if_required(options.workdir, options.output_fasta)

    output_translation_table = add_path_to_filename_if_required(options.workdir, options.output_translation_table)
    filename_resout   = add_path_to_filename_if_required(options.workdir, options.filename_resout)
    output_same_sequences = add_path_to_filename_if_required(options.workdir, options.output_same_sequences)

    dockingmethod     = options.dockingmethod
    coevolmap         = options.coevolmap

    # If mode 4 activated => prepare reverse conversion of docked single chain pdbs 
    input_backward    = add_path_to_filename_if_required(options.workdir, options.input_backward)
    output_backward   = add_path_to_filename_if_required(options.workdir, options.output_backward)
    if options.exclude_chain:
        list_exclude = options.exclude_chain.split(',')
    else:
        list_exclude = None
    
    input_lres        = options.input_lres
    output_lres       = options.output_lres



    # Main class of the module
    cm = ConvertMultimer(input_pdb, output_pdb, output_chain, options.workdir, output_fasta,\
                          input_lres, output_lres, filename_resout,\
                          input_translation_table, output_translation_table,\
                          input_backward, output_backward, list_exclude, output_same_sequences, dockingmethod, coevolmap)


    test = 0


if __name__ == "__main__":
    Main()
