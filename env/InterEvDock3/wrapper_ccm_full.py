#!/usr/bin/env python3

import sys, argparse, os, glob, time, re
# import PyPDB.PyPDB as PDB

import cluster.cluster as cluster
import pkg_resources

DFLT_HOME = ""
DFLT_PROTEO3DNET_PATH = "/shared/webservices/mobyle/jobs/Proteo3Dnet"
# DAREUS_WRAPPER_PATH   = "/scratch/user/tuffery/ccm/COMPASS"
DAREUS_WRAPPER_PATH   = pkg_resources.resource_filename("InterEvDock3","")
DOCKER_CCM = "ccm/1.0-rpbs"
DOCKER_HHTOOLS = "hhtools/1.0-rpbs"
DOCKER_GROMACS = "gromacs/2020.4.gromacs-py-rpbs"

OLD_BANKS = True
if OLD_BANKS:
    HHCLUSTERBANK = "/home/databases/pdb70_clu_7158679.tsv" # HHsearch's pdb70_clu.tsv, with size appended
    MUSTANGBANK   = "/home/databases/MUSTANG"
    SEQRESBANK    = "/home/databases/SEQATM_PASS1.txt" # Only residues with 3D coordinates (rather than pdb_seqres.txt)
else:
    HHCLUSTERBANK = "/home/databases/pdb70_clu_7444274.tsv" # HHsearch's pdb70_clu.tsv, with size appended
    MUSTANGBANK   = "/home/databases/ali"
    SEQRESBANK    = "/home/databases/SEQATM.200916"
#
#IDMAPPINGUNIPROT = "/scratch/banks/MS2MODELS_banks/idmapping_selected.tab"

def rename_and_map_sequences(inputfasta, outputfasta): # GP2020
    dico = {}
    counter = 1
    with open(inputfasta, 'r') as infile:
        with open(outputfasta, 'w') as outfile:
            for line in infile:
                if line[0] == '>':
                    outfile.write('>sequence'+str(counter)+"\n")
                    dico['sequence'+str(counter)] = line.strip("\n")
                    counter+=1
                else:
                    outfile.write(line)

    with open('renaming.map', 'w') as outfile: # NOTE: hard-coded
        for key in sorted(dico):
            outfile.write(key+"\t"+dico[key]+"\n")

def sketch_multifasta(fasta_fn, prefix="sequence"):
    """
    read a multiple fasta. For each, change the identifier for a sketch 1 (sequenceX)
    output sketched fasta and sketch mapping
    """
    f = open(fasta_fn)
    lines = f.readlines()
    f.close()

    extension = os.path.splitext(fasta_fn)[1]
    base      = os.path.splitext(fasta_fn)[0]
    sketch_map_fn   = base+"-sketch.map"
    sketch_fasta_fn = base+"-sketch.fst" # +extension.replace(".","")
    sketch_map      = open(sketch_map_fn, "w")
    sketch_fasta    = open(sketch_fasta_fn, "w")

    rs = {}
    s_num = -1
    for l in lines:
        if l[0] == ">":
            if s_num >= 0:
                sketch_map.write("%s %s \"%s\"\n" % (s_sketch, s_id, s_info) )
                sketch_fasta.write(">%s\n%s\n" %  (s_sketch, rs[s_id]))
            s_num   += 1
            s_info   = l[1:].strip()
            s_id     = l[1:].split()[0]
            if ((s_id[:3] == "sp|") or (s_id[:3] == "tr|")):
                s_id = s_id.split("|")[1]
            s_sketch = "%s%d" % (prefix, s_num + 1)  # First sequence has number 1
            rs[s_id] = ""
        else:
            rs[s_id] = "%s%s" % (rs[s_id], l.strip())
    if s_num >= 0:
        sketch_map.write("%s %s \"%s\"\n" % (s_sketch, s_id, s_info) )
        sketch_fasta.write(">%s\n%s\n" %  (s_sketch, rs[s_id]))

    sketch_map.close()
    sketch_fasta.close()
    return sketch_fasta_fn

def multifasta_read_for_chunks(fasta_fn, prefix="sequence"):
    """
    read a multiple fasta. arrange it as a list of list, i.e. ready fo chunk identification
    """
    f = open(fasta_fn)
    lines = f.readlines()
    f.close()

    rs = []
    s_num = -1
    for l in lines:
        if l[0] == ">":
            if s_num >= 0:
                rs.append("%s\n%s\n" % (s_id,s_seq))
            s_num   += 1
            s_id     = l.strip().replace(" ","")
            s_seq = ""
        else:
            s_seq = "%s%s" % (s_seq, l.strip().replace(" ",""))
    if s_num >= 0:
        rs.append("%s\n%s\n" % (s_id,s_seq))
    return rs

def chunk_define(xs, n):
    ''' Split a list xs into n chunks
    '''
    # print("xs:", xs)
    L = len(xs)
    assert 0 < n <= L
    s = L//n
    return [xs[p:p+s] for p in range(0, L, s)]

def write_sublists_and_listoflists_ori(output_list_filename, prefix, chunks,  verbose = True): 
    if verbose:
        sys.stderr.write("output_list_filename: %s\n" % output_list_filename)
        sys.stderr.write("prefix              : %s\n" % prefix)
        sys.stderr.write("chunks              : %s\n" % str(chunks))
    f1 = open(output_list_filename, 'w')
    for i in range(len(chunks)):
        # list_name = str(i)+'_'+input_fasta_file
        list_name = "%s-chunk%d.fst" % (prefix, i+1)
        f1.write("%s\n" % list_name)
        f2 = open(list_name, 'w')
        for sequence in chunks[i]:
            sequence = '>'+sequence
            f2.write(sequence)
        f2.close()
    f1.close()

def write_chunks(chunks,  chunk_list_fn, prefix, suffix, verbose = False): 
    """
    chunks is list of list
    each secondary list written as on chunk
    """
    if verbose:
        sys.stderr.write("chunk_list_fn: %s\n" % chunk_list_fn)
        sys.stderr.write("prefix       : %s\n" % prefix)
        sys.stderr.write("suffix       : %s\n" % suffix)
        sys.stderr.write("chunks       : %s\n" % str(chunks))
        fn_list = []
    f1 = open(chunk_list_fn, 'w')
    for i in range(len(chunks)):
        # list_name = str(i)+'_'+input_fasta_file
        list_name = "%s-chunk%d.%s" % (prefix, i+1, suffix)
        fn_list.append(list_name)
        f1.write("%s\n" % list_name)
        f2 = open(list_name, 'w')
        for item in chunks[i]:
            f2.write(item)
        f2.close()
    f1.close()
    return chunk_list_fn, fn_list

def parallelize_analysis_of_hhout_files(hhsearch_dir, HHCLUSTERBANK, MUSTANGBANK, SEQRESBANK, label, tasks_per_chunk = 10, verbose = True):
    all_hhsearch_out_file = "%s.all_hhsearchout" % label
    cmd = 'ls ' + hhsearch_dir + '/*.out > %s' % all_hhsearch_out_file
    os.system(cmd)
    if verbose:
        print("current directory:", os.getcwd(), "cmd:", cmd)

    # number_of_out_files = os.popen('cat ALL_OUT | wc -l').read()
    f = open(all_hhsearch_out_file)
    lines = f.readlines()
    f.close()

    num_chunks = len(lines) // tasks_per_chunk
    if len(lines) % tasks_per_chunk > 0:
        num_chunks += 1
    chunks = chunk_define(lines, num_chunks)

    output_list_filename = "%s-hhsearchout-chunks.txt" % label # FIXME Arbitrary name

    chunk_list_fn, fn_list = write_chunks(chunks,  output_list_filename, label, "hhout", verbose = True)

    return chunk_list_fn, fn_list, num_chunks
    # sys.exit(0)

    # number_of_out_files = len(lines)
    # if verbose: 
    #   sys.stderr.write("number_of_out_files: %s\n" % number_of_out_files)
    # number_of_out_files_in_sublists = (int(number_of_out_files)//10)+1 # FIXME 10 //
    # cmd = 'split -l ' + str(number_of_out_files_in_sublists) + ' --additional-suffix=.sub all_hhsearch_outfiles.txt'
    # if verbose: 
    #   sys.stderr.write("number_of_out_files_in_sublists: %s\n" % number_of_out_files_in_sublists)
    # os.system(cmd)
    # # print('split -l ' + str(number_of_out_files_in_sublists) + ' --additional-suffix=.sub ALL_OUT')

    # arg2sadd = hhsearch_dir + ' ' + HHCLUSTERBANK + ' ' + MUSTANGBANK + ' ' + SEQRESBANK
    # all_subout = os.popen('ls *.sub').read()
    # all_subout_list = all_subout.split("\n")
    # all_subout_list.pop()

    # f1 = open('all_hhsearch_outfiles.sub', 'w')
    # for line in all_subout_list:
    #   f1.write("%s %s\n" % (line, arg2sadd))
    # f1.close()
    # number_of_extractions = int(os.popen('cat all_hhsearch_outfiles.sub | wc -l').read())

    # return number_of_extractions

def parseargs(args = None):

    # parser = optparse.OptionParser()
    parser = argparse.ArgumentParser()

    parser.add_argument("-q","--query",action="store",dest="query", default=None, required=True, 
                      help="Query full sequences")

    parser.add_argument("-l","--label",action="store",dest="label", default="ccm", 
                      help="label for job results")

    parser.add_argument("-a", "--alignments",action="store", dest="alignments", 
                        default=None, required=False, 
                        help="A series of pairwise alignments (one per chain), ordered as 1: query 2: template. \n \
                        Template and query identifiers MUST MATCH. Template chains on the form templateId_chainId (e.g. 3BXW_A)")

    parser.add_argument("--t_id",action="store",dest="template_id", default=None, 
                      help="template identifier in alignment file (e.g. 3BXW)")

    parser.add_argument("-c","--csv",action="store", dest="csv", default=None, required=False,
                        help="tsv/csv file describing template/query correspondance. Lines on the form: 6CHG A O60137, \n \
                        meaning chaing A of 6CHG is template for sequence Id O60137 in the alignment/query")

    parser.add_argument("-s","--session_id",action="store",dest="session_id",default=None, required=False, 
                      help="proteo3Dnet session Id")

    parser.add_argument("--p3dn_path",action="store",dest="proteo3Dnet_path",default=DFLT_PROTEO3DNET_PATH, required=False,
                      help="path to Proteo3Dnet mobyle jobs")

    parser.add_argument("--noProteo3Dnet",action="store_false",dest="doProteo3Dnet", default=True,
                      help="skip proteo3Dnet")

    parser.add_argument("--noPass1",action="store_false",dest="doPass1", default=True,
                      help="skip pass1")

    parser.add_argument("--noDareus",action="store_false",dest="doDareus", default=True,
                      help="skip DaReUS-Loop")

    parser.add_argument("--noPass2",action="store_false",dest="doPass2", default=True,
                      help="skip pass2")

    parser.add_argument("--noMinimization",action="store_false",dest="doMinimization", default=True,
                      help="skip final minimization")

    parser.add_argument("-v","--verbose",action="store_true",dest="verbose", default=False,
                      help="trigger verbose mode")

    parser.add_argument("--min_loop_sze",action="store",dest="min_loop_sze", default=None, type=int, 
                      help="Minimum loop size")

    parser.add_argument("--max_loop_sze",action="store",dest="max_loop_sze", default=None, type=int,
                      help="Maximum loop size")

    parser.add_argument("--max_loop_num",action="store",dest="max_loop_num", default=None, type=int,
                      help="Maximum number of loops")

    parser.add_argument("--master_progress",action="store",dest="master_progress", default=None,
                      help="master workdir path in order to write in the write .progress.txt (useful for integrating live progress reports into InterEvDock3)")

    options = parser.parse_args()
    
    return options

def analyse_proteo3Dnet_candidates(session_id, template_id = None, subdir = ".", proteo3Dnet_path = DFLT_PROTEO3DNET_PATH, master_progress=None):
    # Get files from session id
    # cmd = "cp %s/%s/%s/c00* %s/%s/%s/*-sketch.fst %s/%s/%s/*-sketch.map ." % \
    # (proteo3Dnet_path, session_id, subdir, proteo3Dnet_path, session_id, subdir, proteo3Dnet_path, session_id, subdir)

    print("got here")
    if template_id:
        # Identify which c00x we need
        # this will assign tsvfile  
        tsvfile = ""
        for t in sorted(glob.glob("%s/%s/%s/*tsv"%(proteo3Dnet_path, session_id, subdir))):
            with open(t,"r") as f:
                for l in f:
                    tid = l.strip().split()[0]
                    if tid == template_id:
                        tsvfile = os.path.basename(t)
                        break
        
        if tsvfile=="":
            # means we have not found the template
            cluster.progress("Sorry: Template %s was not found among possible templates from previous session id %s"%(template_id,session_id))
            if master_progress and os.path.exists(master_progress):
                cluster.progress("Sorry: Template %s was not found among possible templates from previous session id %s"%(template_id,session_id),wdir=master_progress)
            return None, None, None

        # Copy relevant files (tsv file, hhdb and *mafftout files)
        hhdb_filename = "%s.hhdb"%subdir
        label = subdir
        cmd = "cp %s/%s/%s/%s %s/%s/%s/%s %s/%s/%s/*/*mafftout ." % \
        (proteo3Dnet_path, session_id, subdir, tsvfile, proteo3Dnet_path, session_id, subdir, hhdb_filename, proteo3Dnet_path, session_id, subdir)
        os.system(cmd)

            # generate c00x.tsv
        cmd = "doFindComplex.py"
        args = ["-f", hhdb_filename,
                "-l", label,
                "-a", ".",
                "--tsv_file", tsvfile,
                                "-v"]
        cluster.progress("Processing template ...")
        cluster.runTasks(cmd, args, tasks = 1, environment_module = DOCKER_HHTOOLS, log_prefix = "find_complex")
        time.sleep(10)

    else:
        # Get files from session id
        cmd = "cp %s/%s/%s/c00* %s/%s/%s/*-sketch.fst %s/%s/%s/*-sketch.map ." % \
        (proteo3Dnet_path, session_id, subdir, proteo3Dnet_path, session_id, subdir, proteo3Dnet_path, session_id, subdir)
        os.system(cmd)

    # # Analyze the files
    # dsv        = glob.glob("cOO1*.dsv")
    # ref_seqs   = glob.glob("cOO1*.refseq")
    # alignments = glob.glob("cOO1*.fst")

    # return dsv, alignments, ref_seqs 
    return best_dsv_sorted(template_id)

def run_threader_pass1(correspondances = None, alignments = None, references = None, template_id = None, label = DOCKER_CCM, verbose = False):
    # if dsv is not None:
    #   drun ccm:1.0-dev raw_complex.py -d c001_6CHG_37.2_2.985.dsv  -q c001_6CHG_37.2_2.985.refseq -a c001_6CHG_37.2_2.985_60.9.fst -l csv_test
    # % (correspondances, references, alignments, label)
    # sudo docker run -it --rm -v $(pwd):$(pwd) -v /scratch/banks/:/scratch/banks/ -u $(id -u):$(id -g) -w $(pwd) docker-registry.rpbs.univ-paris-diderot.fr/hhtools:1.0-dev /bin/bash
    # import cluster

    cmd = "raw_complex.py"
    args = ["-d", " ".join(correspondances),
            "-q", " ".join(references),
            "-a", " ".join(alignments),
            "-l", label]
    if verbose:
        sys.stderr.write("run_threader_pass1:\n")
        sys.stderr.write("correspondances: %s\n" % correspondances)
        sys.stderr.write("alignments: %s\n" % alignments)
        sys.stderr.write("references: %s\n" % references)
        sys.stderr.write("label: %s\n" % label)
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))

    cluster.runTasks(cmd, args, tasks = 1, tasks_from = 1, job_opts = "", environment_module = DOCKER_CCM, log_prefix = "run_threader_pass1")

def run_dareus_loop(label = "ccm", verbose = False):
    """
    This produces a: "%s-dareus_final_models.pdb" % label
    """
    args = ["--mode", "Modeling",
            "--model_str", "%s_dareus_input_pdb.pdb" % label,
            "--model_seq", "%s_dareus_input_seq.fst" % label,
            "--l", "%s-dareus" % label,
            "--run_mod", "cluster"]

    cmd = "python %s/DaReUS-Loop-dev.py %s" % (DAREUS_WRAPPER_PATH, " ".join(args))

    if verbose:
        sys.stderr.write("%s\n" % (cmd))
        sys.stderr.flush()
    os.system(cmd)

def run_dareus_loop_sdv(label = "ccm", verbose = False, min_loop_sze=False, max_loop_sze=False, max_loop_num=False):
    """
    This produces a: "%s-dareus_final_models.pdb" % label
    """
    args = ["--mode", "Modeling",
            "--model_str", "%s_dareus_input_pdb.pdb" % label,
            "--model_seq", "%s_dareus_input_seq.fst" % label,
            "--l", "%s-dareus" % label,
            "--run_mod", "cluster",
            "--docker_img", "dareus-loop/1.0-rpbs"
            ]
    if min_loop_sze:
        args+=['--min_loop_sze {}'.format(min_loop_sze)] # min loop size default=2
    if max_loop_sze:
        args+=['--max_loop_sze {}'.format(max_loop_sze)] # max loop size default=30
    if max_loop_num:
        args+=['--max_loop_num {}'.format(max_loop_num)] # max number of loops default=150

    cmd = "python %s/Wrapper_DaReUS_all.py %s" % (DAREUS_WRAPPER_PATH, " ".join(args))

    if verbose:
        sys.stderr.write("%s\n" % (cmd))
        sys.stderr.flush()
    os.system(cmd)

def run_threader_pass2(correspondances = None, alignments = None, references = None, template_id = None, label = "ccm", verbose = False):
    # if dsv is not None:
    #   drun ccm:1.0-dev raw_complex.py -d c001_6CHG_37.2_2.985.dsv  -q c001_6CHG_37.2_2.985.refseq -a c001_6CHG_37.2_2.985_60.9.fst -l csv_test
    # % (correspondances, references, alignments, label)
    # drun ccm:1.0-dev raw_complex.py -d c005_3JC6_26.3_93.3_.dsv -q c005_3JC6_26.3_93.3_.refseq -a c005_3JC6_26.3_93.3_.aln -l test -e -p test-dareus_final_models.pdb
    # import cluster

    cmd = "raw_complex.py"
    args = ["-d", " ".join(correspondances),
            "-q", " ".join(references),
            "-a", " ".join(alignments),
            "-l",  label,
            "-e",
            "-p", "%s-dareus_final_models.pdb" % label]
    if verbose:
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))
        sys.stderr.flush()
    cluster.runTasks(cmd, args, tasks = 1, tasks_from = 1, job_opts = "", environment_module = DOCKER_CCM, log_prefix = "run_threader_pass2")

def run_final_minimization(label = "ccm", verbose = False):

    cmd = "minimize_pdb_and_cyclic_ter.py"
    args = ["-f", "%s-ccm-model_SC.pdb" % label,
            "-n", "%s-ccm-model_SC-min" % label,
            "-nt 6", "-keep_segid" ]
    if verbose:
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))
        sys.stderr.flush()

    count = 0
    while (count < 5) and (not os.path.exists("%s-ccm-model_SC-min.pdb" % label)):
        cluster.runTasks(cmd, args, tasks = 1, tasks_from = 1, job_opts = "-c 6", environment_module = DOCKER_GROMACS, log_prefix = "run_final_minization")
        count += 1

    gro_dir = "%s_final_minimization" % label
    os.mkdir (gro_dir)
    os.system("mv *gro* %s" % gro_dir)

def run_proteo3Dnet(references = None,label = "ccm", verbose = False):

    doHHSearch      = True
    doClusterExpand = True
    doFindComplex   = True
    # Read the input multi-FASTA file into a list, where each element is: sequence name + sequence itself (including all carriage returns)
    # input_fasta_str = os.popen('cat ' + references).read()
    # input_fasta_list = input_fasta_str.split('>')
    # input_fasta_list.pop(0)
    input_fasta_list = multifasta_read_for_chunks(references)

    cluster.progress(" Starting proteo3DNet ...")
    if verbose:
        sys.stderr.write("references: %s\n" % references)

    # If no number of tasks (chunks of the input list) is provided with option -n,
    # then one UniProt = one task
    number_of_tasks = len(input_fasta_list)
    if verbose:
        sys.stderr.write("sequence number: %d\n" % number_of_tasks)

    # Split the list into n files, where n is the input number of tasks
    # FILENAME.fasta => 0_FILENAME.fasta, 1_FILENAME.fasta, 2_FILENAME.fasta, ...
    chunks = chunk_define(input_fasta_list, number_of_tasks)
    output_list_filename = "%s-chunks.txt" % label # FIXME Arbitrary name

    if verbose:
        sys.stderr.write("number of chunks: %d\n" % len(chunks))
        sys.stderr.write("chunks          : %s\n" % str(chunks))
        sys.stderr.write("output_list_filename: %s\n" % output_list_filename)

    if doHHSearch:
        # write_sublists_and_listoflists(output_list_filename, references.replace(".fst",""), chunks, verbose = True)
        chunk_list_fn, fn_list = write_chunks(chunks,  output_list_filename, references.replace(".fst",""), "fst", verbose = True)

    hhsearch_dir = os.path.splitext(references)[0]
    hhsearch_dir = "%s-hhsearch" % label
    hhexpand_dir = "%s-hhexpand" % label
    hhdbfile     = os.path.splitext(references)[0]+'.hhdb'
    hhdbfile     = "%s.hhdb" % label
    mafft_dir    = os.path.splitext(references)[0]+'_MAFFT_OUTPUT'
    mafft_dir    = "%s-clusterExpand" % label

    if verbose:
        sys.stderr.write("hhsearch_dir: %s\n" % hhsearch_dir)
        sys.stderr.write("hhdbfile    : %s\n" % hhdbfile)
        sys.stderr.write("mafft_dir   : %s\n" % mafft_dir)

    # For each sublist produced, run a task on the prod or dev cluster
    if verbose:
        sys.stderr.write("starting step1 (hhsearch) ...\n")
    cmd = "eval doFindTemplate.py"
    args = [ "-f", '$(sed -n "${SLURM_ARRAY_TASK_ID}p" %s)' % output_list_filename,
                 "-l", '%s-sequence-chunk"${SLURM_ARRAY_TASK_ID}"' % label,
                 "--no_sketch",
                 "-v"] 
    # print(cmd, " ".join(args))
    if verbose:
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))

    # NOTE: number of CPU used (job_opts = "-c 8") is defined in findTemplate.ini
    cluster.progress(" Proteo3DNet: Running HHsearch... (this may take some time depending on server load)")
    if doHHSearch:
        cluster.runTasks(cmd, args, tasks = len(fn_list), environment_module = DOCKER_HHTOOLS, job_opts = "-c 8", log_prefix = "find_template")

    # Move the results into a directory (newly created, unless it already exists)
    # FILENAME.fasta => FILENAME/
    # print("hhsearch_dir: ", hhsearch_dir)
    if (not os.path.isdir(hhsearch_dir)):
        os.mkdir(hhsearch_dir)
    mv_target = '*'+hhsearch_dir+'*'
    mv_target = "*sequence-chunk*"
    # NOTE: redirecting STDERR to /dev/null to mute: cannot move "1_MS2MODELS_DeltaP73" to a subdirectory of itself
    cmd = 'mv %s %s 2> /dev/null' % (mv_target, hhsearch_dir)
    if verbose or True:
        sys.stderr.write("%s\n" % (cmd))

    if doHHSearch:
        os.system(cmd) 

    # Analyze the HHsearch results
    # (i) Search additional templates into the clusters
    # (ii) Annotate these templates
    chunk_list_fn, chunk_fns, num_chunks = parallelize_analysis_of_hhout_files(hhsearch_dir, HHCLUSTERBANK, MUSTANGBANK, SEQRESBANK, label)
    # print("number_of_extractions:", number_of_extractions)

    print("chunk_list_fn:", chunk_list_fn)
    print("chunk_fns    :", chunk_fns)
    print("num_chunks   :", num_chunks)
    cmd_args_fn = chunk_list_fn.replace(".txt",".cmds")
    f = open(cmd_args_fn, "w")
    for chunk in chunk_fns:
        f.write(" --list_file %s -l %s\n" % (chunk, chunk.replace(".hhout", "")))
    f.close()
    # print("starting step2 =======================================")
    if verbose:
        sys.stderr.write("starting step2 (hh cluster expansion / analysis) ...\n")
    cmd = "eval doClusterExpand.py"
    args = [ '$(sed -n "${SLURM_ARRAY_TASK_ID}p" %s)' % cmd_args_fn ]
    # print(cmd, " ".join(args))
    if verbose:
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))
    cluster.progress(" Proteo3DNet: Performing advanced cluster search ... (this may take some time depending on server load)")
    if doClusterExpand:
        cluster.runTasks(cmd, args, tasks = num_chunks, environment_module = DOCKER_HHTOOLS, log_prefix = "cluster_expand")
    time.sleep(3)

    if (not os.path.isdir(hhexpand_dir)):
        os.mkdir(hhexpand_dir)

    hhdb_filename = hhsearch_dir+'.hhdb'
    hhdb_filename = "%s/%s.hhdb" % (hhexpand_dir, label)
    # os.popen('cat *.hhdb > %s' % hhdb_filename)
    # os.popen('mv *.alntmp *.maffterr *.mafftout *.hhaln *.msa* *.multifasta *_MAFFT_OUTPUT/')
    os.system('cat *.hhdb > %s' % hhdb_filename)
    os.system("mv *.hhaln *.maffterr *.mafftout *.hhdb %s" % hhexpand_dir)
    os.system("cp %s ." % (hhdb_filename))

    # generate c00x.tsv
    if verbose:
        sys.stderr.write("starting final step: template complex identification ...\n")
    cmd = "doFindComplex.py"
    args = ["-f", hhdb_filename,
            "-l", label,
            "-a", hhexpand_dir]
    if verbose:
        sys.stderr.write("%s %s\n" % (cmd, " ".join(args)))
    cluster.progress(" Proteo3DNet: Performing template identification ...")
    if doFindComplex:
        cluster.runTasks(cmd, args, tasks = 1, environment_module = DOCKER_HHTOOLS, log_prefix = "find_complex")
    time.sleep(10)

    cluster.progress(" Proteo3DNet: done ...")
    return best_dsv_sorted(verbose = verbose)


def best_dsv(verbose = False):
    """
    dsv names in the form: c001-chloe_6DEX_2.1_80.5_43.5.dsv, i.e.
    c0xx-label-PDBid_resol_cov_id.dsv
    """
    # Identity c000x* for next modeling steps:
    dsv        = glob.glob("c001*.dsv")
    # dsv        = glob.glob("c00*.dsv")
    # dsv        = glob.glob("c00*.dsv")
    # print(dsv)
    index      = 0
    n_entities = 0
    # Number of entities
    for i, d in enumerate(dsv):
        n =  int(os.popen('wc -l %s' % d).read().split(  )[0])
        if n > n_entities:
            ndex = [i]
            n_entities = n
        elif n == n_entities:
            ndex .append(i)
        # if d.split("_")[2] > int(dsv[index].split("_")[2])
    if "ndex" not in locals():
        return None, None, None

    idkey   = -1
    ckey    = -2
    i30dex  = []
    id30    = []
    cov30   = []
    i10dex  = []
    covid10 = []
    for i in ndex:
        idy = float(dsv[i].replace(".dsv","").split("_")[idkey])
        cov = float(dsv[i].replace(".dsv","").split("_")[ckey])

        if idy >= 30.:
            i30dex.append(i)
            id30.append(idy)
            cov30.append(cov)
        else:
            i10dex.append(i)
            covid10.append(idy + cov)

    # If we have something at more than 30% sequence identity, consider best coverage
    if len(i30dex):
        # print("i30dex:", i30dex)
        # print("cov30:", cov30)
        # print("id30:", id30)
        # more than 30 % sequence identity, best coverage
        index   = i30dex[0]
        bestval = cov30[0]
        # print("bestval:", bestval)
        # for i in i30dex[1:]:
        for i in range(1, len(i30dex)):
            if cov30[i] > bestval:
                index = i30dex[i]
                bestval = cov30[i]
    else:
        # less than 30 % sequence identity, best coverage+id
        index   = i10dex[0]
        bestval = covid10[0]
        for i in i10dex[1:]:
            if covid10[i] > bestval:
                index = i10dex[i]
                bestval = covid10[i]

    t_name     = dsv[index].replace(".dsv","").split("_")[-4]
    c_name     = "_".join(dsv[index].replace(".dsv","").split("_")[:-4])

    alignments = glob.glob("%s_%s*.aln" % (c_name, t_name))
    sequences  = glob.glob("%s_%s*.refseq" % (c_name, t_name))

    if verbose:
        sys.stderr.write("hh dsv: %s alignments: %s sequences: %s\n" % ([dsv[index]], alignments, sequences))

    return [dsv[index]], alignments[:1], sequences[:1]

def best_dsv_sorted_30(template = None, verbose = True):
    """
    dsv names in the form: c001-chloe_6DEX_2.1_80.5_43.5.dsv, i.e.
    c0xx-label-PDBid_resol_cov_id.dsv

    This function searches the current directory for dsv files.
    Then it prioritize them according to sequence identity or identity + coverage
    If a template is specified (MUST BE A VALID PDB IDENTIFIER), then it selects the dsv corresponding to this template
    otherwise it returns a sorted list of templates
    Original version with a switch at 30% sequence identity
    """
    # Identity c000x* for next modeling steps:
    dsv        = glob.glob("c001*.dsv")
    # dsv        = glob.glob("c00*.dsv")
    # dsv        = glob.glob("c00*.dsv")
    if verbose:
        sys.stderr.write("%s\n" % str(dsv))

    index      = 0
    n_entities = 0
    # Number of entities
    for i, d in enumerate(dsv):
        n =  int(os.popen('wc -l %s' % d).read().split(  )[0])
        if n > n_entities:
            ndex = [i]
            n_entities = n
        elif n == n_entities:
            ndex .append(i)
        # if d.split("_")[2] > int(dsv[index].split("_")[2])
    if "ndex" not in locals():
        return None, None, None

    def id_percent(name):
        return float(name.replace(".dsv","").split("_")[-1])

    def coverage(name):
        return float(name.replace(".dsv","").split("_")[-2])
    
    # print(sorted(dsv, key=lambda name: (float(name.replace(".dsv","").split("_")[-2]), float(name.replace(".dsv","").split("_")[-1])), reverse=True))
    sorted_dsv = sorted(dsv, key=lambda name: (float(name.replace(".dsv","").split("_")[-1]), \
        float(name.replace(".dsv","").split("_")[-2])), reverse=True)

    # If we have something at more than 30% sequence identity, consider best coverage
    if float(sorted_dsv[0].replace(".dsv","").split("_")[-1]) < 30:
        sorted_dsv = sorted(dsv, key=lambda name: (float(name.replace(".dsv","").split("_")[-1]) + \
            float(name.replace(".dsv","").split("_")[-2])), reverse=True)

    if template is not None:
        template_dsv = [ x for x in sorted_dsv if x.count(template)]
        if template_dsv != []:
            sorted_dsv = template_dsv
        else:
            return [],[],[]

    sorted_aln = [name.replace(".dsv",".aln") for name in sorted_dsv]
    seqs = ["" for name in sorted_dsv]

    if verbose:
        sys.stderr.write("%s\n" % str(sorted_dsv))
        sys.stderr.write("%s\n" % str(sorted_aln))
        sys.stderr.write("%s\n" % str(seqs))

    return sorted_dsv, sorted_aln, seqs


def best_dsv_sorted(template = None, verbose = True):
    """
    dsv names in the form: c001-chloe_6DEX_2.1_80.5_43.5.dsv, i.e.
    c0xx-label-PDBid_resol_cov_id.dsv

    This function searches the current directory for dsv files.
    Then it prioritize them according to sequence identity or identity + coverage
    If a template is specified (MUST BE A VALID PDB IDENTIFIER), then it selects the dsv corresponding to this template
    otherwise it returns a sorted list of templates

    JA version of candidate sorting.
    """
    # Identity c000x* for next modeling steps:
    # dsv        = glob.glob("c001*.dsv")
    dsv        = glob.glob("c00*.dsv")
    # dsv        = glob.glob("c00*.dsv")
    if verbose:
            sys.stderr.write("%s\n" % str(dsv))
    index      = 0
    n_entities = 0
    # Number of entities
    for i, d in enumerate(dsv):
        n =  int(os.popen('wc -l %s' % d).read().split(  )[0])
        if n > n_entities:
            ndex = [i]
            n_entities = n
        elif n == n_entities:
            ndex .append(i)
        # if d.split("_")[2] > int(dsv[index].split("_")[2])
    if "ndex" not in locals():
            return None, None, None
    def id_percent(name):
            return float(name.replace(".dsv","").split("_")[-1])
    def coverage(name):
            return float(name.replace(".dsv","").split("_")[-2])
    # all coverage <25% get pushed to the end. Then sort by decreasing id first, then by decreasing coverage.
    sorted_dsv = sorted(dsv, key= lambda name:(coverage(name)<25,id_percent(name)<30,-coverage(name)))
    # then we have to re-sort the part of the list where id<30%, by decreasing (id+cov)
    idx=[i for (i,name) in enumerate(sorted_dsv) if ((id_percent(name)<30) & (coverage(name)>=25))]
    if idx:
        first = idx[0]
        last = idx[-1]
        # when cov is >=25%, second sorting criterion is identity
        sorted_dsv[first:last+1] = sorted(sorted_dsv[first:last+1],key = lambda name:(-(id_percent(name)+coverage(name)),-id_percent(name)))
        # when cov is <25%, second sorting criterion is coverage
        sorted_dsv[last+1:] = sorted(sorted_dsv[last+1:],key = lambda name:(-(id_percent(name)+coverage(name)),-coverage(name)))
    if template is not None:
        template_dsv = [ x for x in sorted_dsv if x.count(template)]
        if template_dsv != []:
            sorted_dsv = template_dsv
        else:
            return [],[],[]
    sorted_aln = [name.replace(".dsv",".aln") for name in sorted_dsv]
    seqs       = ["" for name in sorted_dsv]
    if verbose:
        sys.stderr.write("%s\n" % str(sorted_dsv))
        sys.stderr.write("%s\n" % str(sorted_aln))
        sys.stderr.write("%s\n" % str(seqs))
    return sorted_dsv, sorted_aln, seqs

def main(argv):

    options = parseargs(argv)

    check              = True
    do_proteo3Dnet     = options.doProteo3Dnet
    do_threading_pass1 = options.doPass1
    do_DaReUS_Loop     = options.doDareus
    do_threading_pass2 = options.doPass2
    do_Minimize        = options.doMinimization
    
    msg                = []

    if options.query is None:
        msg.append("Need a file specifying all sequences")
        check = False

    if (options.alignments is not None) and (options.csv is not None):
        do_proteo3Dnet = False

    dsv            = options.csv
    alignments     = options.alignments
    sequences      = options.query
    template_id    = options.template_id
    job_label      = options.label
    master_progress = options.master_progress

    # dsv        = ["c001_6CHG_37.2_-.dsv"]
    # alignments = ["c001_6CHG_37.2_-_60.9.fst"]
    # sequences  = ["c001_6CHG_37.2_-.refseq"]


    # outputfile = sequences+'_renamed' # GP2020
    # rename_and_map_sequences(sequences, outputfile)
    # sequences = outputfile
    sequences = sketch_multifasta(sequences)

    if do_proteo3Dnet:
        if master_progress and os.path.exists(master_progress):
            cluster.progress("... %s: running template search"%job_label,wdir=master_progress)

        cluster.progress("CCM: Will run Proteo3Dnet ...")
        dsv, alignments, r_sequences = run_proteo3Dnet(references = sequences, label = job_label, verbose = options.verbose)
        time.sleep(3)
        if dsv == None:
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: Error: no suitable template found"%(job_label),wdir=master_progress)
            sys.stderr.write("CCM: no template detected.\n")
            sys.exit(0)
    elif options.session_id is not None:
        dsv, alignments, r_sequences = analyse_proteo3Dnet_candidates(options.session_id, options.template_id, options.label, options.proteo3Dnet_path, master_progress)
        time.sleep(3)
    else: # Warm restart from dsv.
        cluster.progress("CCM: Warm restart after Proteo3Dnet ...")
        dsv, alignments, r_sequences = best_dsv_sorted(options.template_id, verbose = options.verbose)
    # print("dsv:", dsv)
    # print("sequences:", sequences)
    # sys.exit(0)
    if dsv == []:
        if options.template_id is not None:
            cluster.progress("Sorry: template %s does not seem valid.\nCannot proceed further." % options.template_id)
            sys.stderr.write("Sorry: template %s does not seem valid.\nCannot proceed further." % options.template_id)
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: Error: given template id %s does not seem valid"%(job_label,options.template_id),wdir=master_progress)
        else:
            cluster.progress("Sorry: no valid template identified.\nCannot proceed further.")
            sys.stderr.write("Sorry: no valid template found.\nCannot proceed further.")
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: Error: no suitable template found"%(job_label),wdir=master_progress)

        sys.exit(0)

    sys.stderr.write("dsv in use: %s\n" % dsv)

    target_tried = 1
    for a_dsv, a_aln in zip(dsv, alignments):
        print("Considering: %s" % a_dsv)
        if master_progress and os.path.exists(master_progress):
            cluster.progress("... %s: considering template %s"%(job_label,a_dsv.split("_")[-4]),wdir=master_progress)
        cluster.progress("Considering template %s" % a_dsv.split("_")[-4])
        if do_threading_pass1:
            cluster.progress("CCM: Threading sequence in template ...")
            # print("sequences:", sequences)
            try:
                run_threader_pass1(correspondances = [a_dsv], alignments = [a_aln], references = [sequences], \
                                template_id = template_id, label = job_label, verbose = options.verbose)
                time.sleep(3)
            except Exception as e:
                sys.stderr.write('Failed on threader: '+ str(e))
                sys.stdout.write(a_dsv.split("_")[-4] + ' Failed on threader: ' + str(e))
                os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])
                if master_progress and os.path.exists(master_progress):
                    cluster.progress("... %s: Warning: Threading step failed, will pass onto the next template"%job_label,wdir=master_progress)
                continue
            if not os.path.exists("%s_dareus_input_pdb.pdb" % job_label):
                os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])
                print("No %s_dareus_input_pdb.pdb. Trying using other template" % job_label)
                if master_progress and os.path.exists(master_progress):
                    cluster.progress("... %s: Warning: Threading step failed, will pass onto the next template"%job_label,wdir=master_progress)
                target_tried += 1
                continue

        if do_DaReUS_Loop:
            cluster.progress("CCM: Loop modeling using DaReUS-Loop ...")
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: modeling loops"%job_label,wdir=master_progress)
            try:
                run_dareus_loop_sdv(label = job_label, verbose = options.verbose, min_loop_sze=options.min_loop_sze, max_loop_sze=options.max_loop_sze, max_loop_num=options.max_loop_num)
                time.sleep(3)
            except Exception as e:
                sys.stderr.write('Failed on DaReUS-Loop: '+ str(e))
                sys.stdout.write(a_dsv.split("_")[-4]+' Failed on DaReUS-Loop: '+ str(e))
                os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])
                if master_progress and os.path.exists(master_progress):
                    cluster.progress("... %s: Warning: DaReUS-Loop failed to model loops, will pass onto the next template"%job_label,wdir=master_progress)
                continue
            if not os.path.exists("%s-dareus_final_models.pdb" % job_label):
                f=open(".progress.txt")
                supp_info = f.read()
                f.close()
                res = re.search(r'Error! There are more than ([\d]+) loops to be modeled',supp_info)
                if res and master_progress and os.path.exists(master_progress):
                    cluster.progress("... {}: Warning: Comparative modeling failed, too many loops to be modeled ({} loops) compared to given threshold.".format(job_label,res.group(1)),wdir=master_progress)
                if target_tried < 10:
                    print("Dareus failed for %s, will pass onto the next template"%a_dsv.split("_")[-4])
                    cmd = "mkdir previous_template%s/; mv *_template.pdb *dareus_input_pdb.pdb *dareus_input_seq.fst *_SC.pdb previous_template%s/"%(a_dsv.split("_")[-4],a_dsv.split("_")[-4])
                    #cmd = "\rm *_template.pdb *dareus_input_pdb.pdb *dareus_input_seq.fst *_SC.pdb"
                    os.system(cmd)
                    target_tried += 1
                    os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])
                    if master_progress and os.path.exists(master_progress):
                        cluster.progress("... %s: Warning: DaReUS-Loop failed to model loops, will pass onto next template"%job_label,wdir=master_progress)
                    continue
                else:
                    print("Dareus failed for %s, will ignore this step and continue with %s_dareus_input_pdb.pdb"%(a_dsv.split("_")[-4],job_label))
                    if master_progress and os.path.exists(master_progress):
                        cluster.progress("... %s: Warning: DaReUS-Loop failed to model loops, will ignore this step and continue"%job_label,wdir=master_progress)
                    cmd = "cp %s_dareus_input_pdb.pdb %s-dareus_final_models.pdb" % (job_label, job_label)
                    os.system(cmd)

        if do_threading_pass2:
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: cleaning up structure"%job_label,wdir=master_progress)

            cluster.progress("CCM: Structure cleanup (residue numbering, side chain positioning, etc) ...")
            # try:
            run_threader_pass2(correspondances = [a_dsv], alignments = [a_aln], references = [sequences], \
                template_id = template_id, label = job_label, verbose = options.verbose)
            time.sleep(3)
            # except:
            #   continue
            f=open(DOCKER_CCM+a_dsv+"..log")
            if "The size of the modeled protein is over limit!" in f.read() and master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: Warning: structure is too big for Oscar-star to process (this might cause problems down the line)"%job_label,wdir=master_progress)
                

        if do_Minimize:
            if master_progress and os.path.exists(master_progress):
                cluster.progress("... %s: final minimization"%job_label,wdir=master_progress)

            cluster.progress("CCM: Final minimization ...")
            try:
                run_final_minimization(label = job_label, verbose = options.verbose)
            except Exception as e:
                sys.stderr.write('Failed on DaReUS-Loop: '+ str(e))
                sys.stdout.write(a_dsv.split("_")[-4]+' Failed on DaReUS-Loop: '+ str(e))
                os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])
                cluster.progress("CCM: Warning: Failed to minimize ... Continuing from crude geometry ...")
                cmd = "cp %s-ccm-model_SC.pdb %s-ccm-model_SC-min.pdb" % (job_label, job_label)
                os.system(cmd)
                if master_progress and os.path.exists(master_progress):
                    cluster.progress("... %s: Warning: Failed to minimize..."%job_label,wdir=master_progress)
                continue

        if os.path.exists("%s-ccm-model_SC-min.pdb" % job_label):
            break
        else:
            print(a_dsv.split("_")[-4]+" Minimization failed, no output file generated, passing onto next template")
            cmd = "rm -fr %s_final_minimization/" % job_label
            os.system(cmd)
            os.system("mkdir log_old_jobs_"+a_dsv.split("_")[-4]+" ; cp *.log *.err log_old_jobs_"+a_dsv.split("_")[-4])

    #os.system("rm %s-dareus_Loop*.pdb"%job_label)
    cluster.progress("CCM: done ...")

if __name__ == "__main__":

    main(sys.argv)
