#! /usr/bin/env python

import os, sys
import shutil
import numpy as np
import tempfile
import time
import glob
import re
import InterEvDock3.ConvertMultimer
import pkg_resources

import InterEvDock3.tools as tools
# for python version compatibility
try:
    import ConfigParser
except:
    import configparser as ConfigParser

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))

CMD_GETPDBCLEAN = config.get("scripts","getpdbclean") # '/service/env/InterEvDock2/GetPDBClean.py'
SCRIPT_CONVERT_MULTIMER = ''

import PyPDB.PyPDB as PDB

PYTHON = config.get('scripts', 'python')
MAPCOEVOL2STRUCT = config.get('scripts', 'mapcoevol2struct')

import cluster.cluster as cluster

def run_mapCoevol2Struct_with_jobarray(d_coevol_map, chain_combination, workdir="",
                                       distance_threshold=8.0, threshold_coupling=0.0, jobarray=False):
    """
    Inputs:
    d_coevol_map : dict containing the path to the coevol associated files
    chain_combination : str as 'AA,BA:CB,DB' for heteromer of chain A and B as homomers (segid A) versus chains C and D homomers (segid B)
    workdir : directory where the directory *_models/ are with the pdb inside
    distance_threshold : CA-CA distance threshold used to consider a positive structural contact
    threshold_coupling : Consider couplings down to which threshold value
    jobarray : Should the script MapCoevolmap2Structures.py be run using jobarray on the cluster

    MapCoevolmap2Structures options:
   ('-p', '--pdb', help='Pdb input file', default=debug['pdb'])
   ('-f', '--sequence', default=debug['sequence'],
                    help='Sequence input file in fasta format. If heterodimer, use colons ":" between the sequence name of the two files')
   ('-l', '--coupling', default=debug['coupling'], help='DCA couplings input file')
   ('-w', '--working_dir', help='Which directory are running in ?', default=debug['working_dir'])
   ('-o', '--output', help='Output file', default=debug['output'])
   ('-d', '--distance_thresh', default=debug['distance_thresh'], type=float,
                    help='Distance thresholds for positive couplings')
    ('-c', '--chain_segid', default=debug['chain'], help='How to interpret the chains and segids in the pdb? \
                                                Automatically inferred for homomers but required for heterodimers with homomeric subunits, \
                                                Write 'AA,BA:CB,DB' for heteromer of chain A and B as homomers (segid A) versus chains C and D homomers (segid B)')
    ('-m', '--mode', default=debug['mode'], choices=['monomer', ' homomer', 'heteromer'],
                    help='What is the nature of the pdb input ?')
   ('-v', '--verbose', action='store_true', default=False)
   ('-a', '--add_redundancy', action='store_true', default=False)
   ('-t', '--threshold_coupling', action='store', default=debug['threshold_coupling'])
   ('-y', '--only_dump_pml_file', action='store_true', default=False)
    """
    START = time.time()
    curdir = os.path.abspath(os.getcwd())
    if not workdir:
        workdir = os.getcwd()

    path_sequence_a = d_coevol_map["path_sequence_a"]
    path_sequence_b = d_coevol_map["path_sequence_b"]
    path_cmap = d_coevol_map["path_cmap"]

    folders = glob.glob("*_models/")
    print("Models will be taken from : \n{}".format('\n'.join(folders)))
    model_complex = []
    for folder in folders:
        for model in glob.glob(folder + "/*"):
            model_complex.append(model)

    print (">>> List of models to process : \n{}".format('\n'.join(model_complex)))
    tmpdir = tempfile.mkdtemp(dir=workdir)
    os.chmod(tmpdir, 0o755)

    for i, mod in enumerate(model_complex):
        shutil.copy(mod, tmpdir)
        model_complex[i] = os.path.basename(model_complex[i])

    shutil.copy(path_sequence_a, tmpdir)
    shutil.copy(path_sequence_b, tmpdir)
    shutil.copy(path_cmap, tmpdir)

    os.chdir(tmpdir)

    if jobarray:
        for i, mod in enumerate(model_complex):
            args = ['-p {}'.format(mod),
                    '-f {}:{}'.format(path_sequence_a, path_sequence_b),
                    '-l {}'.format(path_cmap),
                    '-w {}'.format(tmpdir),
                    '-d {}'.format(distance_threshold),
                    '-c {}'.format(chain_combination),
                    '-t {}'.format(threshold_coupling),
                    '-y '
                    ]

            f = open("input_{}.txt".format(i + 1), "w")
            f.write(" ".join(args) + "\n")
            f.close()
        args = ["$(cat input_${SLURM_ARRAY_TASK_ID}.txt)"]
        print("jobarray " + PYTHON + " " + MAPCOEVOL2STRUCT + " " + " ".join(args))
        cluster.runTasks(PYTHON + " " + MAPCOEVOL2STRUCT, args, tasks = len(model_complex), tasks_from = 1, log_prefix = "map_coevol_2_struct")
    else:
        for i, mod in enumerate(model_complex):
            args = ['-p {}'.format(mod),
                    '-f {}:{}'.format(path_sequence_a, path_sequence_b),
                    '-l {}'.format(path_cmap),
                    '-w {}'.format(tmpdir),
                    '-d {}'.format(distance_threshold),
                    '-c {}'.format(chain_combination),
                    '-t {}'.format(threshold_coupling),
                    '-y '
                    ]
            print("sequentiel " + PYTHON + " " + MAPCOEVOL2STRUCT + " " + " ".join(args))
            cluster.runTasks(PYTHON + " " + MAPCOEVOL2STRUCT, args, log_prefix = "map_coevol_2_struct")


    for folder in folders:
        model_types = folder.split('_')[0]
        lst_pml = glob.glob("*{}*.pml".format(model_types))
        for pml in lst_pml:
            shutil.copy(os.path.join(tmpdir,pml), os.path.join('..', folder))
        if len(lst_pml) < 10:
            print(">> WARNING: Less than 10 pml files were generated for {} models. Check for potentiel issues".format(model_types))

    os.chdir(curdir)

    try:
        pass
        #shutil.rmtree(tmpdir)
    except:
        # this happens locally when we use quick_qsub.py and the quick_qsub.e / quick_qsub.o files take too long to arrive in the current tmpdir
        # OSError: [Errno 39] Directory not empty: /path/to/tmpdir
        time.sleep(2)
        shutil.rmtree(tmpdir)


class PrepPDBs():
    """
    """

    def __init__(self):
        """
        """
        pass


    def checkPDB(self,pdb):
        """
        pdb file
        """        
        # 1. check if the input is valid PDB
        status, msg = PDB.isPDB(pdb)
        if not status: # status = True/False
            return False, msg

        # 2. check if the number of residues is within 10-N
        p = PDB.PDB(pdb,hetSkip=2)
        nres = len(p)
        if nres < 10 or nres > 6000:
            return False, 'input PDB must have <10,6000> residues'

        # check for missing backbone/sidechain atoms --> JA: disabled on 19 July 2018 
        # (local tests seem to show it works even with missing bb atoms, to be confirmed on the web server)           

        return True, None


    def cleanPDB(self, input_pdb, chain_output_subdir):

        chain_output_subdir = os.path.abspath(chain_output_subdir)

        # 1. get chain list
        p = PDB.PDB(input_pdb)
        str_chain_input = ':'.join(p.chnList()) # CQ: replaced """chnlist = ':'.join(tools.get_chains_and_exclude_chains_only_solvent(pdb))""" with PyPDB module, should give the same results
        str_chain_input = str_chain_input.replace(' ','_') # blank chain names have to be replaced by _ for GetPDBClean
        
        # 2. make output subdir for this partner
        if os.path.isdir(chain_output_subdir):
            shutil.rmtree(chain_output_subdir)
        os.makedirs(chain_output_subdir)
        
        # 3. copy input pdb to subdir
        path_input_for_chaintreatment = os.path.join(chain_output_subdir,os.path.basename(input_pdb))
        shutil.copy(input_pdb,path_input_for_chaintreatment)
        
        ori_dir = os.getcwd()
        os.chdir(chain_output_subdir)
        
        # 4. execute GetPDBClean.py to clean pdb
        cmd = 'python %s -i %s -c %s -keep_chainname' % (CMD_GETPDBCLEAN, path_input_for_chaintreatment, str_chain_input)
        print("## Running:",cmd)
        os.system(cmd)

        os.chdir(ori_dir)
        # 5. return path to clean pdb file
        return os.path.join(chain_output_subdir, os.path.basename("%s_cleanedup.pdb" %(input_pdb)))


    def Oligo2Mono(self, chain_output_subdir, clean_input_pdb, output_pdb, chain_out, use_frodock=True, is_coevol_map=False):
        """
        Protocol 1 is used to convert a multimeric pdb into a single chain pdb + extraction of subunits fasta
        """
        ori_dir = os.getcwd()
        chain_output_subdir = os.path.abspath(chain_output_subdir)
        clean_input_pdb = os.path.abspath(clean_input_pdb)
        output_pdb = os.path.abspath(output_pdb)

        os.chdir(chain_output_subdir)

        if use_frodock:
            option_for_docking_method = 'frodock'
        else:
            option_for_docking_method = 'zdock'
        print("Converting Oligo to Mono (protocol1)")
        # ConvertMultimer.py -i clean_input_pdb -o output_pdb -c chain_out -f chainX_pdbextract -T chain_residues_translation_table.out -s chain_representative_chain.out -z frodock
        cm = ConvertMultimer.ConvertMultimer(input_pdb = clean_input_pdb, output_pdb = output_pdb, output_chain = chain_out, workdir = chain_output_subdir, \
                          output_fasta = "chain%s_pdbextract"%(chain_out), output_translation_table = "chain%s_residues_translation_table.out"%(chain_out),\
                          output_same_sequences = "chain%s_representative_chain.out" %(chain_out), dockingmethod=option_for_docking_method, coevolmap=is_coevol_map)

        os.chdir(ori_dir)
        return


    def Mono2Oligo(self, translation_file_for_A, translation_file_for_B, list_pdb_dir, consensus_residues, pymol_script, workdir, dockingmethod='frodock', is_coevol_map=False, d_coevol_map=False):
        """
        Call ConvertMultimer script to reformat the ouputs when oligomer protocol was used
        # We will convert every chain invidually
        # Chain A will be reverted as it was in input
        # Chain B might have its original chains converted if redundant with reverted A
        """
        curdir = os.getcwd()
        os.chdir(workdir)

        listtmp = []
        ### For model structures
        list_pdb2change = []
        for model_dir in list_pdb_dir:
            list_models = glob.glob("%s/*pdb"%(model_dir))
            for model in list_models:
                list_pdb2change.append(model)

        # Analyse the chains that will be present in A
        set_exclude_chains_B = set()
        f = open(translation_file_for_A)
        ftin = f.readlines()
        f.close()
        for l in ftin:
            s = l.split('\t')
            m = s[0].split('.')
            set_exclude_chains_B.add(m[2])
        set_exclude_chains_B = list(set_exclude_chains_B)

        # Analyse the chains originally in B
        ori_chains_for_B = []
        f = open(translation_file_for_B)
        ftin = f.readlines()
        f.close()
        for l in ftin:
            s = l.split('\t')
            m = s[0].split('.')
            if not m[2] in ori_chains_for_B:
                ori_chains_for_B.append(m[2])
        
        tmpfileA = tools.make_tempfile(prefix_str='chainA2reverse')
        tmpfileB = tools.make_tempfile(prefix_str='chainB2reverse')
        tmpfilerevA = tools.make_tempfile(prefix_str='chainAreversed')
        tmpfilerevB = tools.make_tempfile(prefix_str='chainBreversed')
        listtmp.extend([tmpfileA,tmpfileB,tmpfilerevA,tmpfilerevB])

        #PROTOCOL 4. case to translate a docked chain of pdb into the original input residues (triggered by the -L option):\n"+\
        #> ConvertMultimerForDocking.py -t input_translation_table -b input_docked.pdb -r reversed_pdb.pdb  [-e A,B,... to exclude chains] \n"+\
        print("Converting Mono to Oligo for all top 50 decoys (protocol4)")
        for model in list_pdb2change:
            tools.extract_pdb_chain(model,'A',tmpfileA)
            tools.extract_pdb_chain(model,'B',tmpfileB)
            # ConvertMultimer.py -t translation_file_for_A -b tmpfileA -r tmpfilerevA
            cm = ConvertMultimer.ConvertMultimer(input_translation_table=translation_file_for_A, input_backward=tmpfileA,
                                                       output_backward=tmpfilerevA, dockingmethod=dockingmethod, coevolmap=is_coevol_map)
            # ConvertMultimer.py -t translation_file_for_B -b tmpfileB -r tmpfilerevB -e set_exclude_chains_B
            cm = ConvertMultimer.ConvertMultimer(input_translation_table=translation_file_for_B, input_backward=tmpfileB,
                                                       output_backward=tmpfilerevB, list_exclude=set_exclude_chains_B[:],
                                                       dockingmethod=dockingmethod, coevolmap=is_coevol_map)
            tools.join_pdb_chain([tmpfilerevA,tmpfilerevB],model)
        new_chains_for_B = tools.get_chain_list(tmpfilerevB)
        chains_for_A = tools.get_chain_list(tmpfilerevA)
        dic_translate_newB2oriB = dict(zip(new_chains_for_B,ori_chains_for_B))

        if is_coevol_map:
            consensus_residues = False
            try:
                lst_chsegi_A = [ch+'A' for ch in chains_for_A]
                lst_chsegi_B = [ch + 'B' for ch in new_chains_for_B]
                chain_combination = "{}:{}".format(",".join(lst_chsegi_A),",".join(lst_chsegi_B))
                #print(chain_combination)
                run_mapCoevol2Struct_with_jobarray(d_coevol_map, chain_combination, workdir="",
                                       distance_threshold=8.0, threshold_coupling=0.0, jobarray=True)
            except Exception as e:
                print(e)
                print("Problem encountered running  run_mapCoevol2Struct_with_jobarray()")


        if consensus_residues:
            ### Consensus residues
            tmpfileresA = tools.make_tempfile(prefix_str='resAreversed')
            tmpfileresB = tools.make_tempfile(prefix_str='resBreversed')
            listtmp.extend([tmpfileresA, tmpfileresB])

            fin = open(consensus_residues).readlines()
            list_resA = []
            list_resB = []
            for l in fin:
                if l[0] == "#":
                    continue
                elif l[0] in ['1','2','3','4','5']:
                    s = l.split()
                    list_resA.append(s[1][1:-1])
                    list_resB.append(s[2][1:-1])
            str_resA = ",".join(list_resA)
            str_resB = ",".join(list_resB)

            print("Converting top 5 residue list (protocol3)")
            # ConvertMultimer.py -t translation_file_for_A -L str_resA -O tmpfileresA
            cm = ConvertMultimer.ConvertMultimer(output_lres=str_resA, filename_resout=tmpfileresA, input_translation_table=translation_file_for_A, dockingmethod=dockingmethod)
            # ConvertMultimer.py -t translation_file_for_B -L str_resB -O tmpfileresB -e set_exclude_chains_B
            cm = ConvertMultimer.ConvertMultimer(output_lres=str_resB, filename_resout=tmpfileresB, input_translation_table=translation_file_for_B, list_exclude=set_exclude_chains_B[:], dockingmethod=dockingmethod)

            resAconvert = {}
            resBconvert = {}
            f = open(tmpfileresA)
            fA = f.readlines()
            f.close()
            f = open(tmpfileresB)
            fB = f.readlines()
            f.close()
            for l in fA:
                s = l.strip('\n').split('\t')
                resAconvert[s[0]] = s[1]
            for l in fB:
                s = l.strip('\n').split('\t')
                resBconvert[s[0]] = s[1]

            newlines = []
            for ii,l in enumerate(fin):
                if ii == 0:
                    newlines.append(l)
                elif ii == 1:
                    newl = re.sub('chainA','inputA(AA.index.chain)',l)
                    newl = re.sub('chainB','inputB(AA.index.chain)',newl)
                    newlines.append(newl)
                elif l[0] in ['1','2','3','4','5']:
                    s = l.split()
                    # list_resA.append(s[1][1:-1])
                    # list_resB.append(s[2][1:-1])
                    new_chain_B = resBconvert[s[2][1:-1]].split('.')[2]
                    if new_chain_B == dic_translate_newB2oriB[new_chain_B]:
                        newl = "%s       %s                 %s\n"%(l[0],resAconvert[s[1][1:-1]],resBconvert[s[2][1:-1]])
                    else:
                        newl = "%s       %s                 %s (chain %s was chain %s in user inputB)\n"%(l[0],resAconvert[s[1][1:-1]],resBconvert[s[2][1:-1]],new_chain_B,dic_translate_newB2oriB[new_chain_B])
                    newlines.append(newl)

            fout = open(consensus_residues,"w")
            fout.writelines(newlines)
            fout.close()

        if pymol_script and not d_coevol_map:
            print("Converting pymol script")
            ### pymol files
            f = open(pymol_script)
            fpy = f.read()
            f.close()
            newlines = []

            #for l in fpy:
            newl = re.sub('/A','/%s'%("+".join(chains_for_A)),fpy)
            newl = re.sub('/B','/%s'%("+".join(new_chains_for_B)),newl)
            newl = re.sub('ch1=A','ch1=%s'%("+".join(chains_for_A)),newl)
            newl = re.sub('ch2=B','ch2=%s'%("+".join(new_chains_for_B)),newl)
            #newlines.append(newl)
            fout_pymol = open(pymol_script,"w")
            fout_pymol.writelines(newl)
            fout_pymol.close()
      
        ### Remove temp files !!
        for tmpfile in listtmp:
            os.system("rm %s"%(tmpfile))

        os.chdir(curdir)
