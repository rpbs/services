#!/usr/bin/env python

import os
import re
import sys
import glob
import math
import time
import shutil
import optparse
import tempfile
import numpy as np
import pkg_resources
# for python version compatibility
try:
    import commands
except:
    import subprocess as commands
try:
    import ConfigParser
except:
    import configparser as ConfigParser
import cluster.cluster as cluster
import InterEvDock3.tools as tools
config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))
DOCKER_ROSETTA = config.get("environments","rosetta")
ROSETTA_CMD = config.get("programs","rosetta")
ROSETTA_DB = config.get("programs","rosetta_db")

def div_list(lst, divisor):
    """Divide a list into "divisor" parts. ex [1,2,3,4,5,6,7] divided by 3: [1,2,3], [4,5], [6,7]
    Useful in a mpi program, when a complete list of tasks should be divided for each CPU,
    use div_list(lst, size)[rank]
    """
    if divisor == 0:
        print("Error: divisor cannot be zero")
        return None
    size = int(math.floor(float(len(lst))/divisor))
    if len(lst) == 0:
        my_list = []
        for i in range(divisor):
            my_list.append([])
    elif len(lst) < divisor:
        my_list = [[i] for i in lst]
        diff = divisor - len(my_list)# probably void sub-list should be added
        for i in range(diff):
            my_list.append([])
    else:
        lengths = [size for i in range(0, divisor*size, size)]
        for ite,i in enumerate(range(divisor*size,len(lst))):
            lengths[ite]+=1
        lims = np.cumsum([0]+lengths)
        my_list = [lst[lims[i-1]:lims[i]] for i in range(1,len(lims))]
    return my_list


class RosettaISC():


    def __init__(self,list_pdb=[],output="",workdir="./",parallelise=40,debug=False):
        """
        Class to run Rosetta ISC scoring on a list of pdbs
        list_pdb: [str], list of pdb files
        output: str, output npz file containing all rosetta scores (if not given, no file will be generated)
        workdir: str, working directory where tmpdir will be generated (default: cwd)
        parallelise: int, number of cpus to parallelise scoring on, this script uses job arrays to parallelise with cluster.runTasks()
        debug: bool, if True, tmpdir won't be removed but kept and moved to workdir

        out can access rosetta scores through the numpy output file or through the self variable final_scores (a numpy array)
        """
        if output:
            self.output = os.path.abspath(output)
        else:
            self.output = output
        self.workdir = os.path.abspath(workdir)
        self.parallelise = parallelise
        self.debug = debug
        self.list_pdb = list_pdb
        self.sublists = [] # will be filled in later
        self.final_scores = np.zeros((1,1)) # will be filled in later

        self.CURDIR = os.path.abspath(os.getcwd())

        ### If pdb list is empty, stop here
        if not self.list_pdb:
            print("No pdbs given! Consider revising your input values")
            raise Exception("No pdbs given! Consider revising your input values")

        ### Create tmpdir
        self.tmpdir = tools.make_tempdir(parent_dir=self.workdir)
        os.chdir(self.tmpdir)

        ### Check if some pdb files are called the same in which case we'll use the full name in the output table
        self.only_basename = False
        if len(set([os.path.basename(os.path.splitext(p)[0]) for p in self.list_pdb])) == len(self.list_pdb):
            self.only_basename = True

        ### Print message
        print('Scoring {} pdb(s).'.format(len(self.list_pdb)))

        self.run_rosetta()

        self.read_output()

        if self.debug:
            print('finale scores:')
            print(final_scores)
            try:
                shutil.move(self.tmpdir,self.CURDIR)
            except:
                pass
            print("Check out temp files in "+os.path.join(self.CURDIR,os.path.basename(self.tmpdir)))
        else:
            shutil.rmtree(self.tmpdir) 

        os.chdir(self.CURDIR)


    def run_rosetta(self):

        ### Divide list of pdbs into 'size' sublists
        if self.parallelise > 1:
            self.sublists = div_list(self.list_pdb,self.parallelise)
            self.sublists = [l for l in self.sublists if l] # remove empty sublists
            self.parallelise = len(self.sublists) # i.e. parallelise over X cpus with job arrays

            for i in range(1,self.parallelise+1):
                # we create a sublist file of pdbs for each list
                tmp_pdb_list_file = os.path.join(self.tmpdir,'pdblist_{}.txt'.format(i))
                f=open(tmp_pdb_list_file,"w")
                f.write("\n".join(self.sublists[i-1])+"\n")
                f.close() 

            ### run rosetta on each list independantly
            args = ["-in:file:l {}".format(os.path.join(self.tmpdir,'pdblist_${SLURM_ARRAY_TASK_ID}.txt')),
                    "-out:file:scorefile {}".format(os.path.join(self.tmpdir,'score_Isc_beta_nov15_${SLURM_ARRAY_TASK_ID}.fasc')),
                    "-out:file:silent decoys_out_${SLURM_ARRAY_TASK_ID}.silent",
                    "-in:path:database {}".format(ROSETTA_DB),
                    "-score:weights beta_nov15",
                    "-corrections::beta_nov15 true",
                    "-overwrite",
                    "-nstruct 1",
                    "-use_input_sc",
                    "-docking_local_refine",
                    "-out:no_nstruct_label",
                    "-ignore_zero_occupancy false", 
                    "-run:constant_seed",
                    "> rosetta_${SLURM_ARRAY_TASK_ID}.log 2>&1"] # keep log
            #print('Rank {}: Scoring {} pdbs with the following command: {} {}'.format(rank,len(sublist),ROSETTA_CMD," ".join(args)))
            cluster.runTasks(ROSETTA_CMD, args, job_opts = '-c 1', tasks = self.parallelise, tasks_from = 1, environment_module = DOCKER_ROSETTA, log_prefix = "rosetta_isc") # job_opts to specify other slurm commands

        else:
            self.sublists = [self.list_pdb[:]]
            tmp_pdb_list_file = os.path.join(self.tmpdir,'pdblist_1.txt')
            f=open(tmp_pdb_list_file,"w")
            f.write("\n".join(self.list_pdb)+"\n")
            f.close() 

            ### run rosetta on each list independantly
            args = ["-in:file:l {}".format(os.path.join(self.tmpdir,'pdblist_1.txt')),
                    "-out:file:scorefile {}".format(os.path.join(self.tmpdir,'score_Isc_beta_nov15_1.fasc')),
                    "-out:file:silent decoys_out_1.silent",
                    "-in:path:database {}".format(ROSETTA_DB),
                    "-score:weights beta_nov15",
                    "-corrections::beta_nov15 true",
                    "-overwrite",
                    "-nstruct 1",
                    "-use_input_sc",
                    "-docking_local_refine",
                    "-out:no_nstruct_label",
                    "-ignore_zero_occupancy false", 
                    "-run:constant_seed",
                    "> rosetta_1.log 2>&1"] # keep log
            cluster.runTasks(ROSETTA_CMD, args, environment_module = DOCKER_ROSETTA, log_prefix = "rosetta_isc") # job_opts to specify other slurm commands


    def read_output(self):

        ### Check if all scoring files are present
        checkfiles = sum([os.path.exists(os.path.join(self.tmpdir,'score_Isc_beta_nov15_{}.fasc'.format(r))) for r in range(1,self.parallelise+1)])
        if checkfiles != self.parallelise:
            print("Error: Scoring didn't work - output file not generated.\nPlease check what went wrong in {}".format(os.path.join(self.CURDIR,os.path.basename(self.tmpdir))))
            os.chdir(self.CURDIR)
            try:
                shutil.move(self.tmpdir,self.CURDIR)
            except:
                pass
            raise Exception("Error: Scoring didn't work - output file not generated.\nPlease check what went wrong in {}".format(os.path.join(self.CURDIR,os.path.basename(self.tmpdir))))

        ### Reduce pdb names if all basenames are different from one another
        if self.only_basename:
            for i in range(self.parallelise):
                self.sublists[i] = [os.path.splitext(os.path.basename(p))[0] for p in self.sublists[i]]
            self.list_pdb = [os.path.splitext(os.path.basename(p))[0] for p in self.list_pdb]

        ### Read output file
        pattern = re.compile('SCORE:'+'\s+([\-\.\d]+)'*26+'\s+(.+)')

        if self.debug:
            for i in range(1,self.parallelise+1):
                scoring_output = os.path.join(self.tmpdir,'score_Isc_beta_nov15_{}.fasc'.format(i))
                f=open(scoring_output)
                print(i,scoring_output)
                print(f.read())
                f.close()

        labels = ['total_score', 'score', 'rms', 'Fnat', 'I_sc', 'Irms', 'dslf_fa13', 'fa_atr', 'fa_dun', 
                  'fa_elec', 'fa_intra_rep', 'fa_intra_sol_xover4', 'fa_rep', 'fa_sol', 'hbond_bb_sc', 
                  'hbond_lr_bb', 'hbond_sc', 'hbond_sr_bb', 'lk_ball_wtd', 'omega', 'p_aa_pp', 'pro_close', 
                  'rama_prepro', 'ref', 'time', 'yhh_planarity', 'description']
        types  = ['f']*26+['|{}S'.format(max([len(p) for p in self.list_pdb]))]
        self.final_scores = np.zeros(len(self.list_pdb),dtype=list(zip(labels,types)))
        gathered_failed_jobs = []
        lg = 0
        for i in range(1,self.parallelise+1):
            scoring_output = os.path.join(self.tmpdir,'score_Isc_beta_nov15_{}.fasc'.format(i))
            f=open(scoring_output)
            data = [pattern.search(l).groups() for l in f if pattern.search(l)]
            f.close()
            it_data = 0
            for ii,p in enumerate(self.sublists[i-1]):
                # check if this pdb is in the output file and was scored by Rosetta without problem
                if it_data < len(data) and ((not self.only_basename and os.path.splitext(os.path.basename(p))[0] == data[it_data][-1]) or (self.only_basename and p==data[it_data][-1])):
                    self.final_scores[lg+ii] = data[it_data][:-1]+tuple([self.sublists[i-1][ii]])
                    it_data += 1
                else:
                    # Fill in scores to default values if not in Rosetta output
                    self.final_scores[lg+ii]['description'] = self.sublists[i-1][ii]
                    for l in labels[:-1]:
                        self.final_scores[lg+ii][l] = float('inf')
                    gathered_failed_jobs.append(self.sublists[i-1][ii])
            lg += len(self.sublists[i-1])

        if gathered_failed_jobs:
            print("Number of failed jobs: {} out of {}".format(len(gathered_failed_jobs),len(self.list_pdb)))
            print("\n".join(gathered_failed_jobs))

        if self.output:
            # save output scores in a compressed numpy array
            np.savez(self.output,RosettaScores=self.final_scores,Description="RosettaScores is a structured numpy "+\
                "array containing all 24 scores outputted by Rosetta (c.f. column names). Lines filled "+\
                "with float('inf') correspond to structures that weren't scored by Rosetta because it failed "+\
                "to converge after 10 iterations.")


def Main():


    ### Read options

    usage = "\n{} -p <pdb> [or -l <file list of pdbs> or -d <directory of pdbs>] -o <output.npz>\n".format(os.path.abspath(sys.argv[0]))+\
            "Description:\nScript to run Rosetta Scoring on a single or a list of pdb complexes. You can run it in parallel using jobarrays with the --parallelise option.\n"+\
            "\nExample:\npython {} --parallelise 40 -l file.txt\n".format(os.path.abspath(sys.argv[0]))

    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-p','--pdb',action="store",dest='pdb',type='string',
                      help="pdb complex structure to score")
    parser.add_option('-l','--pdblst',action="store",dest='pdblst',type='string',
                      help="file with a list of pdb complex structures to score")
    parser.add_option('-d','--pdbdir',action="store",dest='pdbdir',type='string',
                      help="directory of pdb complex structures to score")
    parser.add_option('-o','--output',action="store",dest='output',type='string',default='rosetta_sc.npz',
                      help="numpy .npz output file name that will contain all Rosetta scores [default: %default]")
    parser.add_option('--parallelise',action="store",dest='parallelise',type='int',default=1,
                      help="if you want to run of several cpus using job arrays, specify number of cpus with this options")
    parser.add_option('--debug',action="store_true",dest='debug',
                      help="if it's not running correctly and you would like to look at various variable values along "+\
                           "this script and access intermediate temporary files")
    parser.add_option('-w','--workdir',action="store",dest='workdir',default=os.getcwd(),type="string",
                      help="To specify working directory path [default: current working directory %default]")


    if len(sys.argv) == 1:
        print(usage)
        print("Type -h or --help for more help information")
        sys.exit(1)

    (options, args) = parser.parse_args(sys.argv)
    if len(args) > 1: # sys.argv[0] is the name of this script
        print("Leftover arguments:" + str(args[1:]))

    START = time.time()

    ### Get list of pdbs to score (a structure, a list of structure or a directory of structures)
    list_pdb = []
    if options.pdb and os.path.exists(options.pdb): # it's a single pdb
        list_pdb = [os.path.abspath(options.pdb)]
    elif options.pdblst and os.path.exists(options.pdblst): # it's a file with a list of pdbs
        f=open(options.pdblst)
        list_pdb = [os.path.abspath(l.strip()) for l in f if l.strip() and not l.startswith("#") and os.path.exists(l.strip())]
        f.close()
    elif options.pdbdir and os.path.exists(options.pdbdir) and os.path.isdir(options.pdbdir): # it's a directory with pdbs
        options.pdbdir = os.path.abspath(options.pdbdir)
        list_pdb = [os.path.join(options.pdbdir,l) for l in os.listdir(options.pdbdir) if l.endswith(".pdb")]

    options.output = os.path.abspath(options.output)

    ### If pdb list is empty, stop here
    if not list_pdb:
        print("No pdbs given! Consider revising your input values")
        print(" ".join(sys.argv))
        print(usage)
        sys.exit(1)

    oRos = RosettaISC(list_pdb=list_pdb,output=options.output,workdir=options.workdir,parallelise=options.parallelise,debug=options.debug)

    print("Total execution time in seconds: {:.0f}x{} seconds".format((time.time()-START),options.parallelise))

if __name__ == "__main__":
    Main()

