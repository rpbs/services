#! /usr/bin/env python


import os
import re
import sys
import math
import pprint
import copy

import argparse
import configparser
import fileinput
import tempfile
import shutil
import time
import pkg_resources

import InterEvDock3.tools as tools
from operator import itemgetter

#from sphinx.addnodes import desc_content

"""
ATOM     26  CB  PHE A 435      40.965  92.791  22.229  1.00 38.22      A    C
ATOM     27  CG  PHE A 435      40.607  91.832  21.134  1.00 37.33      A    C
ATOM     28  CD1 PHE A 435      39.330  91.839  20.577  1.00 37.86      A    C
ATOM     29  CD2 PHE A 435      41.542  90.917  20.661  1.00 34.55      A    C
ATOM     30  CE1 PHE A 435      38.984  90.941  19.566  1.00 40.16      A    C
ATOM     31  CE2 PHE A 435      41.210  90.017  19.652  1.00 35.88      A    C
ATOM     32  CZ  PHE A 435      39.925  90.029  19.102  1.00 36.78      A    C
ATOM     33  N   GLY A 436      42.591  95.589  23.138  1.00 40.85      A    N
ATOM     34  CA  GLY A 436      42.877  96.468  24.259  1.00 41.55      A    C
ATOM     35  C   GLY A 436      43.933  95.918  25.197  1.00 40.59      A    C
ATOM     36  O   GLY A 436      44.724  95.050  24.827  1.00 40.43      A    O
ATOM     37  N   ARG A 437      43.950  96.420  26.423  1.00 37.61      A    N

"""

d3to1 = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K', 'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F',
         'ASN': 'N',
         'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 'ALA': 'A', 'VAL': 'V', 'GLU': 'E', 'TYR': 'Y',
         'MET': 'M'}

try:
    import ConfigParser
except:
    import configparser as ConfigParser

if not os.name == 'nt':
    # We run under linux

    try:
        # Runs only on RPBS : Do we have a config file available.
        config = ConfigParser.ConfigParser()
        config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))
        MAFFT = config.get("programs", "mafft")
        DOCKER_MAFFT = config.get("environments", "mafft")
        PYTHON = config.get('scripts', 'python')
        MAPCOEVOL2STRUCT = config.get('scripts', 'mapcoevol2struct')
        import cluster.cluster as cluster
        # Will be used with : cluster.runTasks(MAFFT, args, docker_img=DOCKER_MAFFT)
    except:
        # We presume we are at i2bc
        MAFFT = "mafft "
        DOCKER_MAFFT = None
        PYTHON = 'python3 '
        MAPCOEVOL2STRUCT = "MapCoevolmap2Structures.py "

else:
    # Instruction to run on windows
    MAFFT = "cmd.exe /C C:\\Users\\rg152862\Documents\SCRIPTS\DIVERSE\diverse\mafft-win\mafft.bat --localpair"
    # "cmd.exe /C C:\Users\rg152862\Documents\SCRIPTS\DIVERSE\diverse\mafft-win\mafft.bat --localpair " #--out output.txt input.txt"
    DOCKER_MAFFT = None


def dist(coord1, coord2, do_sqrt=False):
    dist = 0
    for i, e in enumerate(coord1):
        dist += (coord1[i] - coord2[i]) * (coord1[i] - coord2[i])
    if do_sqrt:
        dist = math.sqrt(dist)
    return dist

def append_redundancy_group(coevmap, fcoupling_with_group_path, range_index_redundancy_group=2, is_value_proba=True):
    """
    Change the file only if the contact map is 3-columns.
    Grouping by equivalent contacts can be done with respect to the amplitude of  redundancy_grouping value
    @param coevmap : A coevolmap file from ComplexContact or trRosetta
    @param range_index_redundancy_group : number of positions before and after considered equivalent
    @param is_value_proba : True if value is a proba in [0,1] or a predicted distance

    @return: A global string which can be written to a file containing the updated contact map
    """
    list_contacts = []
    dic_groups = {}
    newlines = []
    coupling_group = 0
    # For every residue index resi, residues in the index range [resi-AMPLITUDE,resi+AMPLITUDE]  will be assigned the best value for the contact
    AMPLITUDE = range_index_redundancy_group
    for ll in fileinput.FileInput(coevmap):
        x = ll.split()
        try:
            list_contacts.append([eval(x[2]), int(x[0]), int(x[1])])
        except (NameError, ValueError):
            continue

    if is_value_proba:
        # typical case is when the 3rd column of the contact map file contains coupling intensity or contact probability (higher-> better)
        list_contacts.sort(reverse=True, key=itemgetter(0))
    else:
        # typical case is when the 3rd column of the contact map file contains distances (lower-> better)
        list_contacts.sort(key=itemgetter(0))

    for ii, sp in enumerate(list_contacts):
        # sp = ll.split()
        idxAi = sp[1]
        idxBj = sp[2]
        value_contact = sp[0]
        if "%d_%d" % (idxAi, idxBj) not in dic_groups:
            coupling_group += 1
            for add_i in range(-AMPLITUDE, AMPLITUDE + 1):
                for add_j in range(-AMPLITUDE, AMPLITUDE + 1):
                    elt = "%d_%d" % (idxAi + add_i, idxBj + add_j)
                    if elt not in dic_groups:
                        dic_groups[elt] = {}
                        dic_groups[elt]['value'] = value_contact
                        dic_groups[elt]['group_index'] = coupling_group

    for ii, sp in enumerate(list_contacts):
        idxAi = sp[1]
        idxBj = sp[2]
        value_contact = sp[0]
        key_residues = "%d_%d" % (idxAi, idxBj)
        newline = "%d %d %.6f %d %.6f" % (
            idxAi, idxBj, value_contact, dic_groups[key_residues]['group_index'], dic_groups[key_residues]['value'])
        newlines.append(newline)
    coevmap_with_group = "\n".join(newlines)

    fout = open(fcoupling_with_group_path, "w")
    fout.write(coevmap_with_group)
    fout.close()

def parse_pdb(fpdb):
    """
    Parse a pdb and extract sequence and coordinates

    @param fpdb: path to the pdb file
    @return: a dict dpdb such that
    dpdb[chain+segid]['seq'] -> sequence aa
    dpdb[chain+segid]['coord'][resid][at] -> coordinates as a list of 3 floats
    dpdb[chain+segid]['resi'] -> list of resid (resid is int)
    """
    dpdb = {}
    previous_resid = -100
    previous_segid = "XXX"
    previous_chain = "XXX"
    for ll in fileinput.FileInput(fpdb):
        if ll[:4] != 'ATOM':
            continue
        if len(ll) > 72:
            at = ll[13:17].strip()
            chain = ll[21]
            resid = int(ll[22:26])
            resn = ll[17:20]
            xyz = [eval(x) for x in ll[30:56].split()]
            segid = ll[72]

            if not previous_chain == chain:
                dpdb[chain + segid] = {}
                dpdb[chain + segid]['seq'] = ""
                dpdb[chain + segid]['resi'] = []
                dpdb[chain + segid]['coord'] = {}
                previous_chain = chain

            if not previous_segid == segid:
                dpdb[chain + segid] = {}
                dpdb[chain + segid]['seq'] = ""
                dpdb[chain + segid]['resi'] = []
                dpdb[chain + segid]['coord'] = {}
                previous_segid = segid

            try:
                aa = d3to1[resn]
            except KeyError:
                aa = "X"
            if not previous_resid == resid:
                dpdb[chain + segid]['seq'] += aa
                dpdb[chain + segid]['resi'].append(resid)
                dpdb[chain + segid]['coord'][resid] = {}
                previous_resid = resid

            # lpdb.append([chain,segid,resid,xyz])
            dpdb[chain + segid]['coord'][resid][at] = xyz

    return dpdb

def parse_fasta(ffasta):
    """
    Extract headers and sequences from a fasta file

    @param ffasta: path to the fasta file
    @return: 2 lists 1 of headers the other sequences
    """
    lheader = []
    lseq = []
    fin = open(ffasta)
    is_start_fasta = False
    for ll in fin.readlines():
        if len(ll) > 0:
            if ll[0] == ">":
                is_start_fasta = True
                lheader.append(ll)
                lseq.append("")
            elif is_start_fasta:
                lseq[-1] += ll
    return lheader, lseq

def map_pdb_seq(fastaref, pdbseq):
    """
    Maps the index in the pdb sequence and in the fasta sequence

    @param fastaref: string of AA corresponding to the sequence used to generate the contact map
    @param pdbseq: string of AA
    @return: a dict,  key=['s2p'][int index in sequence], val = index in structure (not the resid itself but alsolute index)
                      key=['p2s'][int abs index in structure], val = index in sequence
    """
    dmap = {}
    dmap['p2s'] = {}
    dmap['s2p'] = {}
    dmap['AA_seq'] = {}
    dmap['AA_pdb'] = {}

    ini_dir = os.getcwd()

    dtmp_dir = tools.make_tempdir()
    ftmpin_path = tools.make_tempfile(parent_dir=dtmp_dir)
    with open(ftmpin_path, 'w') as f:
        f.write(">seq1\n{}\n>seq2\n{}\n".format(fastaref, pdbseq))
    f.close()
    ftmpout_path = tools.make_tempfile(parent_dir=dtmp_dir)
    #os.close(ftmpout)
    os.chdir(dtmp_dir)

    if not os.name == 'nt':
        if DOCKER_MAFFT:
            #args = [ftmpin_path, '>', ftmpout_path]#, "2>", "/dev/null"]
            cmd = "{} {} > {} ".format(MAFFT, ftmpin_path,
                                       ftmpout_path)  # 2> /dev/null".format(MAFFT, ftmpin_path, ftmpout_path)
        else:
            cmd = "{} {} > {} ".format(MAFFT, ftmpin_path, ftmpout_path)#2> /dev/null".format(MAFFT, ftmpin_path, ftmpout_path)
    else:
        cmd = "{} --out {} {}".format(MAFFT, ftmpout_path, ftmpin_path)
    print('Running:')
    if DOCKER_MAFFT:
        #cluster.runTasks(MAFFT, args, docker_img=DOCKER_MAFFT)
        os.system("/usr/local/bin/drun {} {}".format(DOCKER_MAFFT, cmd))
    else:
        print(cmd)
        os.system(cmd)

    lheaderout, lseqout = parse_fasta(ftmpout_path)
    aliseq1 = lseqout[0].replace('\n', '')
    aliseq2 = lseqout[1].replace('\n', '')
    s1_index = 0
    s2_index = 0
    for ii in range(len(aliseq1)):
        if aliseq1[ii] == '-':
            s1_index += 1
        elif aliseq2[ii] == '-':
            s2_index += 1
        else:
            dmap['s2p'][s2_index] = s1_index
            dmap['p2s'][s1_index] = s2_index
            dmap['AA_seq'][s2_index] = aliseq2[ii]
            dmap['AA_pdb'][s1_index] = aliseq1[ii]
            s1_index += 1
            s2_index += 1
    #pprint.pprint(dmap)
    if os.path.isfile(ftmpin_path):
        os.remove(ftmpin_path)
    if os.path.isfile(ftmpout_path):
        os.remove(ftmpout_path)
    os.chdir(ini_dir)
    if os.path.isdir(dtmp_dir):
        os.removedirs(dtmp_dir)

    return dmap

def contact_map(dpdb, threshd, thresh_index=4, CA_CA_prefilter=20):
    """
    Analyse the contacts in a pdb between and within segids (inside the homomers and outside).
    Does not consider coevol map so far, only the pdb

    @param dpdb: Dictionary containing all the pdb data, output of parse_pdb(fpdb)
    @param threshd: Distance threshold below which a contact is validated
    @param thresh_index: Removes the sequence neighbours from the contact map up to Delta of thresh_index
                        (only when they are in the same segid)
    @return: 1) set of contacts written with the format : "{ch_seg1}_{resid1}_{ch_seg2}_{resid2}"
             2) dict of distances and of related chain_segi for every contacts d[contact as in 1)] = [[dist,chsg1,chsg2],...]
    """
    sq_threshd = threshd * threshd
    condition1 = CA_CA_prefilter * CA_CA_prefilter  # CA-CA distance above which no chance that they interact
    # set_contacts = set()
    d_contact_distance = {}
    problem_report = set()

    for ch_seg1 in dpdb:
        for resid1 in dpdb[ch_seg1]['resi']:
            for ch_seg2 in dpdb:
                for resid2 in dpdb[ch_seg2]['resi']:
                    if ch_seg1 == ch_seg2 and abs(resid2 - resid1) <= thresh_index:
                        continue
                    # print(ch_seg1,resid1,ch_seg2,resid2)
                    try:
                        CA1 = dpdb[ch_seg1]['coord'][resid1]['CA']
                        CA2 = dpdb[ch_seg2]['coord'][resid2]['CA']
                        dCA1_CA2 = dist(CA1, CA2)
                    except KeyError:
                        if not resid1 in problem_report and not resid2 in problem_report:
                            print('Could not map CA atom in {}_{} or in {}_{}'.format(ch_seg1,resid1,ch_seg2,resid2))
                            problem_report.add(resid1)
                            problem_report.add(resid2)
                        continue
                    if dCA1_CA2 > condition1:
                        continue
                    is_contact = False
                    for at1 in dpdb[ch_seg1]['coord'][resid1]:
                        if is_contact:
                            continue
                        for at2 in dpdb[ch_seg2]['coord'][resid2]:
                            if is_contact:
                                continue
                            coord1 = dpdb[ch_seg1]['coord'][resid1][at1]
                            coord2 = dpdb[ch_seg2]['coord'][resid2][at2]

                            if dist(coord1, coord2) <= sq_threshd:
                                is_contact = True
                                contact = "{}_{}_{}_{}".format(ch_seg1, resid1, ch_seg2, resid2)
                                # d_struct_contacts.add(contact)
                                if not contact in d_contact_distance:
                                    d_contact_distance[contact] = []
                                d_contact_distance[contact].append([dCA1_CA2, ch_seg1, ch_seg2])

    for struct_contact in d_contact_distance:
        d_contact_distance[contact].sort()
    return d_contact_distance

def analyze_fcoupling(coupling_file, diff_index_thresh, dset_struct_resid, set_intra, set_interhomo, set_interhetero,
                      dequiv_ch_segi, coevo_map_format, threshold_coupling=0.1):
    """
    1- Read the coupling file,
    2- Keep only the couplings between residues existing in the pdb
    3- Label every kept contact with a flag notifying whether it exists in the pdb:
                                                            - as intra contact (flag = 'intra')
                                                            - at the interface of homomers (flag = 'interhomo')
                                                            - at the interface of heteromers (flag = 'interhetero')

    @param coupling_file: file from ComplexContact or trRosetta
    @param diff_index_thresh: Difference in index below which the residue contacts are ignore
    @param dset_struct_resid: dict of set of residue sequence index (referenced with respect to fasta)
                              present in every chain_segi (restriction to those having an equivalent in the pdb)
    @param set_intra: set of structural intra contacts in a chain_segi
    @param set_interhomo: set of structural inter contacts between the chain_segi in equivalent mol systems (homomers)
    @param set_interhetero: set of structural inter contacts between the chain_segi in equivalent mol systems (homomers)
    @param dequiv_ch_segi: dict of chains equivalent in every molecular system considered (ex : homodimer)
                i.e : dequiv_ch_segi['equivalent_molsystem_0'] = set( all  ch_seg part of the first homomultimer),
                      dequiv_ch_segi['equivalent_molsystem_1'] = set( all  ch_seg part of the second homomultimer),
                      dequivalent_chain_segi[ch_seg] = 'equivalent_molsystem_0' or 'equivalent_molsystem_1'
    @return:
        -> list of contacts : [r1,r2,coupling,flag_intra_or_inter,redundancy_group,coupling_group]
        -> Number of contacts in every category : nb_interhomo, nb_interhetero, nb_intra, nb_false_positive
    """
    list_couplings = []
    nb_intra = 0
    nb_interhomo = 0
    nb_interhetero = 0
    nb_false = 0
    index_coupling = 0
    # If one molecular system (monomer) r1 and r2 belong to 'equivalent_molsystem_0'
    # If homomer, r1 and r2 might be intra or inter
    # If heteromer, r1 and r2 are inter from two entities 'equivalent_molsystem_0' and 'equivalent_molsystem_1', resp
    #               r1 may correspond to several chains in the pdb. Since they are equivalent we select one of them
    for ch_seg in dequiv_ch_segi['equivalent_molsystem_0']:  # Trick of a loop break to recover just one element of the set
        set_struct_resid_r1 = dset_struct_resid[ch_seg]  # residue sequence index (referenced with respect to fasta)
        break
    max_index_molsystem_0 = max(set_struct_resid_r1)
    if 'equivalent_molsystem_1' in dequiv_ch_segi:  # contains a heteromer
        for ch_seg in dequiv_ch_segi['equivalent_molsystem_1']:
            # Trick of a loop break to recover just one element of the set
            set_struct_resid_r2 = dset_struct_resid[ch_seg]  # residue sequence index (referenced with respect to fasta)
            break
    else:
        for ch_seg in dequiv_ch_segi['equivalent_molsystem_0']:  # is a monomer or a homomer
            set_struct_resid_r2 = dset_struct_resid[ch_seg]
            break
    print("coevo_map_format : ",coevo_map_format)
    #print("set_intra : ",set_intra)
    #print("set_interhomo : ",set_interhomo)
    #print("set_interhetero : ",set_interhetero)
    set_sym_couples_alreadyseen = set()
    for ll in fileinput.FileInput(coupling_file):
        sp = ll.split()
        try:
            r1 = int(sp[0]) - 1 # The map starts at position 1 for the first residue of fasta sequence while in our mapping first residue is 0
            r2 = int(sp[1]) - 1
        except ValueError:
            continue

        if coevo_map_format in ['mono_homomer', 'heteromer_full']:
            # In full maps r1 and r2 can belong to any of the same molecular system
            if r1 not in set_struct_resid_r1 and r1 not in set_struct_resid_r2:
                continue
            if r2 not in set_struct_resid_r2 and r2 not in set_struct_resid_r1:
                continue
            if "{}_{}".format(r2, r1) in set_sym_couples_alreadyseen:
                # Analyse half the symetry matrix only
                continue
        elif coevo_map_format in ['heteromer_focused']:
            if r1 not in set_struct_resid_r1:
                continue
            if r2 not in set_struct_resid_r2:
                continue

        if coevo_map_format in ['mono_homomer', 'heteromer_full'] and abs(r1 - r2) <= diff_index_thresh:
            # We remove only intramolecular local interactions along the diagonal
            continue

        coupling = eval(sp[2])
        if coupling <= threshold_coupling:
            continue

        index_coupling += 1
        #print(index_coupling)
        try:
            # Redundancy groups are defined in some cases (as in IED3 app) not to count several times similar contacts
            redundancy_group = sp[3]
            coupling_group = sp[4]
        except IndexError:
            redundancy_group = index_coupling
            coupling_group = coupling

        # Important step where contacts are assigned a label that will be used
        #  in pymol outputs, etc...
        cc = "{}_{}".format(r1, r2)
        set_sym_couples_alreadyseen.add(cc)
        if cc in set_intra and coevo_map_format in ['mono_homomer', 'heteromer_full']:
            # in heteromer_full, we see all the map including the intra
            nb_intra += 1
            flag = 'intra'
        elif cc in set_interhomo and coevo_map_format in ['mono_homomer', 'heteromer_full']:
            nb_interhomo += 1
            flag = 'interhomo'  # homomeric contacts are only counted when they can't be interpreted as intra
        elif cc in set_interhetero and coevo_map_format in ['heteromer_focused']:
            nb_interhetero += 1
            flag = 'interhetero'
        elif cc in set_interhetero and coevo_map_format in ['heteromer_full']:
            nb_interhetero += 1
            flag = 'interhetero'
        else:
            nb_false += 1
            if coevo_map_format in ['heteromer_focused']:
                # Map focused on the corner as in ComplexContact
                flag = 'false_hetero'
            elif coevo_map_format in ['mono_homomer'] and len(dequiv_ch_segi['equivalent_molsystem_0']) == 1:
                # Single Chain, no complex
                flag = 'false_mono'
            elif coevo_map_format in ['mono_homomer'] and len(dequiv_ch_segi['equivalent_molsystem_0']) > 1:
                # Single sequence repeated in several chain_segi couples
                flag = 'false_homo'
            elif coevo_map_format in ['heteromer_full']:
                # Heteromeric Assembly
                limit = max_index_molsystem_0
                if len(dequiv_ch_segi['equivalent_molsystem_0']) > 1 and (r1 <= limit + 1 and r2 <= limit + 1):
                    # First square in the diagonal of the contact map. Correspond to the first homomer
                    flag = 'false_homo1'
                elif len(dequiv_ch_segi['equivalent_molsystem_1']) > 1 and (r1 > limit + 1 and r2 > limit + 1):
                    # Second square in the diagonal. Correspond to the second homomer
                    flag = 'false_homo2'
                else:
                    # Square out of the diagonal of the contact map
                    flag = 'false_hetero'

        list_couplings.append([r1, r2, coupling, flag, redundancy_group, coupling_group])

    return list_couplings, nb_interhomo, nb_interhetero, nb_intra, nb_false

def define_set_contacts(d_struct_contacts, global_dmap, dequivalent_chain_segi, thresh):
    """
    From the contacts analyzed in structure,
        translate into contacts in the sequence space using the global_dmap and dequivalent_chain_segi
    @param d_struct_contacts: d["{}_{}_{}_{}".format(ch_seg1,resid1,ch_seg2,resid2)] = [[dist1, chsg1, chsg2],...]
    @param global_dmap: dglobal[ch_seg] {['s2r'] / ['s2p']} [seqi] to map sequence index and pdb index
    @param dequivalent_chain_segi: dequivalent_chain_segi['equivalent_molsystem_[0|1]'] = set('chainsegi')

    @return set_cc_seq_intra, set_cc_seq_interhomo, set_cc_seq_interhetero :
            Sets of contacts for every category of the form '{seq_index1}_{seq_index2}'

    """
    set_cc_seq_intra = set()
    set_cc_seq_interhomo = set()
    set_cc_seq_interhetero = set()
    print("Total number of contacts below {} A : {}".format(thresh, len(d_struct_contacts)))
    for cc in d_struct_contacts:  # d_struct_contacts contains structural contacts with structural indexes as seen in pdb
        cc = cc.split('_')
        try:  # Not all the Structural residues map in the sequence. Some contacts in pdb cannot not be seen in the sequences
            ch_segi1 = cc[0]
            ch_segi2 = cc[2]
            residpdb1 = cc[1]
            residpdb2 = cc[3]
            seq_index1 = global_dmap[ch_segi1]['r2s'][int(residpdb1)]  # index in the fasta input 1
            seq_index2 = global_dmap[ch_segi2]['r2s'][int(residpdb2)]  # index in the fasta input 2
            molsystem1 = dequivalent_chain_segi[ch_segi1]
            molsystem2 = dequivalent_chain_segi[ch_segi2]
            if cc[0] == cc[2]:
                # Contains the contacts inside subunits
                # In case of a fused coalignment it is reliable
                #   but for an output of ComplexContact, highly misleading
                set_cc_seq_intra.add('{}_{}'.format(seq_index1, seq_index2))
                set_cc_seq_intra.add('{}_{}'.format(seq_index2, seq_index1))
            elif molsystem1 == molsystem2:
                # Contains the contacts between subunits of homomers
                set_cc_seq_interhomo.add('{}_{}'.format(seq_index1, seq_index2))
                set_cc_seq_interhomo.add('{}_{}'.format(seq_index2, seq_index1))
            else:
                # Contains the contacts between two subunits of heteromer
                set_cc_seq_interhetero.add('{}_{}'.format(seq_index1, seq_index2))
                set_cc_seq_interhetero.add('{}_{}'.format(seq_index2, seq_index1))
        except KeyError:
            pass
    print("Total number of intra contacts in the pdb structure below {} A : {}".format(thresh, len(set_cc_seq_intra)))
    print("Total number of inter homomeric contacts in the pdb structure below {} A : {}".format(thresh, len(
        set_cc_seq_interhomo)))
    print("Total number of inter heteromeric contacts in the pdb structure below {} A : {}".format(thresh, len(
        set_cc_seq_interhetero)))

    return set_cc_seq_intra, set_cc_seq_interhomo, set_cc_seq_interhetero

def print_precision_file(out, list_couplings, nb_intra_all, nb_interhomo_all, nb_interhetero_all, sampling=0.01):
    """
    Write a file listing the statistics of contact satisfaction in structures depending on the coupling value
    @param out: name of the output file
    @param list_couplings: [r1,r2,coupling,flag,redundancy_group,coupling_group], r1 and r2 are the index in the sequence space
    @param nb_intra_all, nb_interhomo_all, nb_interhetero_all: number of contacts of each category in the input file in total
    @param sampling: the step of decreasing proba at which statistics are calculated

    @return: None, the file is closed in the end.
    """
    dnbcc = {'intra': 0, 'interhomo': 0, 'interhetero': 0, 'false': 0}
    coupling_level = 3  # initial thresh for the first coupling should be above max coupling
    previous_coupling_level = coupling_level

    fout_roc = open(out + "_coupling_vs_precision.out", "w")
    fout_roc.write("#evol_coupling \
    %correct_all \
    %intra_correct \
    %interhomo_correct \
    %interhetero_correct \
    %false \
    nb_contacts \
    %intra_vs_allintra \
    %interhomo_vs_allinterinthefile \
    %interhetero_vs_allinterinthefile\n")
    for ll in list_couplings:

        r1 = ll[0]
        r2 = ll[1]
        coupling = ll[2]
        cc_type = ll[3]
        #print(ll)
        if cc_type in ['false_mono', 'false_homo', 'false_homo1', 'false_homo2', 'false_hetero']:
            cc_type = 'false'
        dnbcc[cc_type] += 1
        while coupling <= coupling_level:
            coupling_level -= sampling
        if not coupling_level == previous_coupling_level:
            sum_all = sum(dnbcc.values())
            ratio_good_vs_all = (dnbcc['intra'] + dnbcc['interhomo'] + dnbcc['interhetero']) * 100. / sum_all
            ratio_intra_vs_all = dnbcc['intra'] * 100. / sum_all
            ratio_interhomo_vs_all = dnbcc['interhomo'] * 100. / sum_all
            ratio_interhetero_vs_all = dnbcc['interhetero'] * 100. / sum_all
            ratio_false_vs_all = dnbcc['false'] * 100. / sum_all
            if nb_intra_all > 0:
                ratio_intra_vs_allintra = dnbcc['intra'] * 100. / nb_intra_all
            else:
                ratio_intra_vs_allintra = -1
            if nb_interhomo_all > 0:
                ratio_interhomo_vs_allinter = dnbcc['interhomo'] * 100. / nb_interhomo_all
            else:
                ratio_interhomo_vs_allinter = -1
            if nb_interhetero_all > 0:
                ratio_interhetero_vs_allinter = dnbcc['interhetero'] * 100. / nb_interhetero_all
            else:
                ratio_interhetero_vs_allinter = -1

            fout_roc.write(
                "{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:d}\t{:.2f}\t{:.2f}\t{:.2f}\n".format(coupling_level,
                                                                                                        ratio_good_vs_all,
                                                                                                        ratio_intra_vs_all,
                                                                                                        ratio_interhomo_vs_all,
                                                                                                        ratio_interhetero_vs_all,
                                                                                                        ratio_false_vs_all,
                                                                                                        sum_all,
                                                                                                        ratio_intra_vs_allintra,
                                                                                                        ratio_interhomo_vs_allinter,
                                                                                                        ratio_interhetero_vs_allinter))

            previous_coupling_level = coupling_level
    fileinput.close()
    fout_roc.close()

def dump_global_dictionary(dglobal, dmolsystem, ftmp):
    """
    Write a file to check if the mapping between sequence and structures is correct.
    This method is run only when verbose mode is activated.
    @param dglobal: dglobal[ch_seg] {['s2r'] / ['s2p']} [seqi] to map sequence index and pdb index
    @param dmolsystem: dequivalent_chain_segi['equivalent_molsystem_[0|1]'] = set('chainsegi')
    @param ftmp: path to the output file

    @return : None, just the output file is closed
    """
    with open(ftmp, "w") as fout:
        fout.write("# col1 : absolute index of pdb residues as mapped in the fasta sequence starting from index 0 #\n")
        fout.write("# col3 : real residue index as observed in the pdb structure #\n")
        for index_molsystem in [0, 1]:
            k = 'equivalent_molsystem_{}'.format(index_molsystem)
            if not k in dmolsystem:
                continue
            set_chain_segi = dmolsystem[k]
            for ch_seg in set_chain_segi:
                fout.write("########################## {} #########################\n".format(ch_seg))
                print("reporting for {}".format(ch_seg))
                # pprint.pprint(dglobal[ch_seg])
                for seqi in dglobal[ch_seg]['s2p']:
                    # print(seqi)
                    AApdbi = dglobal[ch_seg]['AA_pdb'][dglobal[ch_seg]['s2p'][seqi]]
                    AAseqi = dglobal[ch_seg]['AA_seq'][seqi]
                    respdbi = dglobal[ch_seg]['s2r'][seqi]
                    fout.write("{}\t{}\t{}\t{}\n".format(seqi, AAseqi, respdbi, AApdbi))
    fout.close()

def convert_into_pymol_dist(contact, atom='CA', pdbname=""):
    """
    Convert constraints into formatted string to write in pymol pml
    """
    chsg1, res1, chsg2, res2 = contact.split('_')
    chain1 = chsg1[0]
    try:
        segi1 = chsg1[1]
    except IndexError:
        segi1 = ""
    chain2 = chsg2[0]
    try:
        segi2 = chsg2[1]
    except IndexError:
        segi2 = ""
    atom1 = "/{}/{}/{}/{}/{}".format(pdbname, segi1, chain1, res1, atom)
    atom2 = "/{}/{}/{}/{}/{}".format(pdbname, segi2, chain2, res2, atom)
    return atom1, atom2

def enumerate_all_chainsegi_couples(dequiv_chsegi):
    """
    Definition of all the combination of chain-segi which have to be considered for all the categories of contacts
    correct ones :  ['intra', 'interhomo', 'interhetero']
    and their associated negative ['false_mono','false_homo','false_homo1','false_homo2','false_hetero']
    """
    d_chseg_couples = {}  # for every contact type which couples of chains should be considered

    for cc_type in ['intra', 'interhomo', 'interhetero', 'false_mono', 'false_homo', 'false_homo1', 'false_homo2',
                    'false_hetero']:
        d_chseg_couples[cc_type] = []
    for cc_type in ['intra', 'interhomo',
                    'interhetero']:  # , 'false_mono','false_homo','false_homo1','false_homo2','false_hetero']:
        if cc_type == 'intra':
            for system in dequiv_chsegi:
                for chsg in dequiv_chsegi[system]:
                    d_chseg_couples['intra'].append([chsg, chsg])
                    d_chseg_couples['false_mono'].append([chsg, chsg])
        elif cc_type == 'interhomo':
            for system in dequiv_chsegi:
                for chsg1 in dequiv_chsegi[system]:
                    for chsg2 in dequiv_chsegi[system]:
                        if chsg1 != chsg2 and [chsg2, chsg1] not in d_chseg_couples['interhomo']:
                            d_chseg_couples['interhomo'].append([chsg1, chsg2])
                            d_chseg_couples['false_homo'].append([chsg1, chsg2])
        elif cc_type == 'interhetero':
            for chsg1 in dequiv_chsegi['equivalent_molsystem_0']:
                for chsg1b in dequiv_chsegi['equivalent_molsystem_0']:
                    if chsg1b != chsg1 and [chsg1b, chsg1] not in d_chseg_couples['false_homo1']:
                        d_chseg_couples['false_homo1'].append([chsg1, chsg1b])
                for chsg2 in dequiv_chsegi['equivalent_molsystem_1']:
                    for chsg2b in dequiv_chsegi['equivalent_molsystem_1']:
                        if chsg2b != chsg2 and [chsg2b, chsg2] not in d_chseg_couples['false_homo2']:
                            d_chseg_couples['false_homo2'].append([chsg2, chsg2b])
                    d_chseg_couples['interhetero'].append([chsg1, chsg2])
                    d_chseg_couples['false_hetero'].append([chsg1, chsg2])
    return d_chseg_couples

def calculate_list_of_distances(int_seqindex1, int_seqindex2, dpdb, dglobal, l_couples_chseg):
    """
    For a given contact between two sequence positions int_seqindex1,int_seqindex2
    For a list of combinations of 2 chain_segi,
    Calculate the list of distances between Ca-Ca and sort them to have the lowest first.
    """
    return_dlist = []
    for couple in l_couples_chseg:
        chsg1 = couple[0]
        chsg2 = couple[1]
        try:
            res1 = dglobal[chsg1]['s2r'][int_seqindex1]
            res2 = dglobal[chsg2]['s2r'][int_seqindex2]
            CA1 = dpdb[chsg1]['coord'][res1]['CA']
            CA2 = dpdb[chsg2]['coord'][res2]['CA']
        except KeyError:
            continue
        dCA1_CA2 = dist(CA1, CA2)
        return_dlist.append([dCA1_CA2, chsg1, chsg2])
    return_dlist.sort()
    return return_dlist

def map_pred_contacts_in_pdb(list_couplings, d_struct_contacts, dglobal, dequiv_chsegi, dpdb, pdbname=""):
    """
    Define the contacts to be shown in the visualisation interface such as pymol
    @param list_couplings : [r1,r2,coupling,flag,redundancy_group,coupling_group], r1 and r2 are the index in the sequence space
    @param d_struct_contacts   : d["{}_{}_{}_{}".format(ch_seg1,resid1,ch_seg2,resid2)] = [[dist1, chsg1, chsg2],...]
    @param dglobal        : dglobal[ch_seg] {['s2r'] / ['s2p']} [seqi] to map sequence index and pdb index
    @param dequiv_chsegi  : dequivalent_chain_segi['equivalent_molsystem_[0|1]'] = set('chainsegi')

    @return:
        d_contact2pymol['intra'], ['interhomo'], ['interhetero']
    """
    d_contact2pymol = {}
    set_validated_contacts = set()
    set_wrong_contacts = set()
    debugcc = False
    for cc_type in ['intra', 'interhomo', 'interhetero', 'false_mono', 'false_homo', 'false_homo1', 'false_homo2',
                    'false_hetero']:
        d_contact2pymol[cc_type] = []

    d_chseg_couples = enumerate_all_chainsegi_couples(dequiv_chsegi)

    # FIRST PASS
    # We first run through all the couplings to check if they are correct
    # For the correct ones retrieve their index.
    for ii, coupling in enumerate(list_couplings):

        # print("Checking: {}".format(coupling))

        cc_type = coupling[3]
        if cc_type.split("_")[0] == 'false':
            continue

        s1 = coupling[0]
        s2 = coupling[1]
        cc_val = coupling[2]
        try:
            cc_ref = int(coupling[4])
            cc_refval = coupling[5]

        except IndexError:
            cc_ref = ii + 1
            cc_refval = cc_val

        for couples in d_chseg_couples[cc_type]:
            # We go through all possible couples and keep only the one with the smallest distance.
            chsg1 = couples[0]
            chsg2 = couples[1]
            try:
                res1 = dglobal[chsg1]['s2r'][s1]
                res2 = dglobal[chsg2]['s2r'][s2]
            except KeyError:
                continue
            struct_contact = "{}_{}_{}_{}".format(chsg1, res1, chsg2, res2)

            if struct_contact in d_struct_contacts:
                closest_to_keep = d_struct_contacts[struct_contact][0]
                if chsg1 != closest_to_keep[1] or chsg2 != closest_to_keep[2]:
                    # We only keep the closest distance if there are two ways or more to satisfy the contact
                    continue
                atom1, atom2 = convert_into_pymol_dist(struct_contact, pdbname=pdbname)
                coupling_info = "{}_{:.2f}_{:.2f}".format(cc_ref, float(cc_val), float(cc_refval))
                contact2add = [atom1, atom2, coupling_info]

                if not cc_ref in set_validated_contacts:
                    d_contact2pymol[cc_type].append(contact2add)
                if cc_type in ['intra', 'interhomo', 'interhetero']:
                    set_validated_contacts.add(cc_ref)

    # SECOND PASS
    # We run a second time focusing on the false predictions not satisfied in the first pass
    #
    for ii, coupling in enumerate(list_couplings):

        # print("Checking: {}".format(coupling))

        cc_type = coupling[3]
        if cc_type.split("_")[0] != 'false':
            continue

        s1 = coupling[0]
        s2 = coupling[1]
        cc_val = coupling[2]
        try:
            cc_ref = int(coupling[4])
            cc_refval = coupling[5]
        except IndexError:
            cc_ref = ii + 1
            cc_refval = cc_val
        if cc_ref in set_validated_contacts and cc_type.split("_")[0] != 'false':
            print('{} was already treated by a member of its redundancy group was'.format(coupling))
            continue
        # contacts are not in the d_struct_contacts because they are ove the threshold.
        # Hence we recompute all of them and take the one with the smallest distance
        return_dlist = calculate_list_of_distances(s1, s2, dpdb, dglobal, d_chseg_couples[cc_type])
        try:
            smallest_dist = return_dlist[0]  # of the form [d_CA_CA,chsg1,chsg2]
        except IndexError:
            continue
        chsg1 = smallest_dist[1]
        chsg2 = smallest_dist[2]
        try:
            res1 = dglobal[chsg1]['s2r'][s1]
            res2 = dglobal[chsg2]['s2r'][s2]
        except KeyError:
            continue
        struct_contact = "{}_{}_{}_{}".format(chsg1, res1, chsg2, res2)
        atom1, atom2 = convert_into_pymol_dist(struct_contact, pdbname=pdbname)
        coupling_info = "{}_{:.2f}_{:.2f}".format(cc_ref, float(cc_val), float(cc_refval))
        contact2add = [atom1, atom2, coupling_info]
        d_contact2pymol[cc_type].append(contact2add)
        set_validated_contacts.add(cc_ref)
        if cc_type.split("_")[0] != 'false':
            continue
            #pprint.pprint(d_contact2pymol[cc_type])

    return d_contact2pymol

def print_pml_file_for_pymol(out, d_contact2pymol, type_contacts=['intra', 'interhomo', 'interhetero'], pdbname=None, use_pdbname=True):
    """
    @return:
    The pml file creates distance between well predicted contacts

    # first, create two named selections
    select a, ///A/501/02
    select b, ///B/229/N
    # calculate & show the distance from selection a to selection b.
    distance d, a, b
    # hide just the distance labels; the
    # dashed bars should still be shown
    hide labels, d

    set dash_gap, 0.5
    set dash_radius, 0.1
    """
    dic_type_name= dict()
    dic_type_name['intra']  = "i"
    dic_type_name['interhomo'] = "o"
    dic_type_name['interhetero'] = "c"
    for bad in ['false_mono', 'false_homo', 'false_homo1', 'false_homo2', 'false_hetero']:
        dic_type_name[bad] = "f"
    if pdbname:
        for key in dic_type_name:
            dic_type_name[key] += "_"+pdbname
    f = {}
    for cc_type in type_contacts:
        directory = os.path.dirname(out)
        filename  = os.path.basename(out)
        f[cc_type] = open(os.path.join(directory, "VisualizeContacts_{}_{}.pml".format(cc_type,filename)), "w")
        for ii, struct_contact in enumerate(d_contact2pymol[cc_type]):
            sel1 = struct_contact[0]
            sel2 = struct_contact[1]
            info = struct_contact[2]
            dname = "c{}_{}_{}".format(ii + 1, info,filename)
            f[cc_type].write("distance {}, {}, {}\n".format(dname, sel1, sel2))
            f[cc_type].write("hide labels, {}\n".format(dname))
            if use_pdbname:
                f[cc_type].write("group {},{}\n".format(dic_type_name[cc_type], dname))
            else:
                f[cc_type].write("group c{},{}\n".format(cc_type, dname))
        if use_pdbname:
            f[cc_type].write("set dash_gap, 0.5\nset dash_radius, 0.25\ncolor white, {}*\n".format(dic_type_name[cc_type]))
        else:
            f[cc_type].write("set dash_gap, 0.5\nset dash_radius, 0.25\ncolor white, c{}*\n".format(cc_type))
        f[cc_type].close()


def main(fpdb,
         fseqs,
         fcoupling,
         chain_segid,
         distance_threshold=8.0,
         wdir=None,
         out=None,
         mode="",
         verbose=False,
         add_redundancy=False,
         thresh_index=4,
         CA_CA_prefilter=20,
         only_dump_pml_file=False,
         threshold_coupling=0.1):

    CURDIR = os.getcwd()
    if not wdir:
        wdir = CURDIR
    os.chdir(wdir)
    
    if not out:
        out = os.path.splitext(fpdb)[0]

    # ===============================================================================
    # Read pdb and store coords
    # ===============================================================================

    dpdb = parse_pdb(fpdb)
    """
    # dpdb : dict key = <chain><segid>
    #                    ['seq'], value = 1-letter sequence
    #                    ['resi'], value = list of resid
    #                    ['coord'][resid][at] = xyz
    """
    pdb_rootname = os.path.splitext(os.path.basename(fpdb))[0]

    # ===========================================================================
    # Parse the fasta inputs
    # ===========================================================================

    list_fseq = fseqs.split(":")  # list of all fasta files
    list_hetero = chain_segid.split(":")  # list of every chain (corresponding to the fasta files) ['AA,AB', 'BA,BB'] 'chain_segid'
    parsed_seq = []
    dic_seqinfo = {}

    for fseq in list_fseq:
        header, seqref = parse_fasta(fseq)
        parsed_seq.append([header, seqref])
    """
    # header and seqref = lists
    """
    # ===========================================================================
    # Define the molecular system : what are the equivalent subunits, what are the ones forming an heteromer ?
    # ===========================================================================

    dequivalent_chain_segi = dict()  # used to discriminate inter-homomers and inter-heteromers
    for ii, elt in enumerate(list_hetero):
        dequivalent_chain_segi['equivalent_molsystem_{}'.format(ii)] = set()
        list_chain_segid = elt.split(",")
        for ch_seg in list_chain_segid:
            dic_seqinfo[ch_seg] = {}
            dic_seqinfo[ch_seg]['header'] = parsed_seq[ii][0]
            dic_seqinfo[ch_seg]['seqref'] = parsed_seq[ii][1]
            dequivalent_chain_segi['equivalent_molsystem_{}'.format(ii)].add(ch_seg)
            dequivalent_chain_segi[ch_seg] = 'equivalent_molsystem_{}'.format(ii)

    # ===========================================================================
    # Map sequence indexes to structure resid en reverse
    # ===========================================================================
    global_dmap = {}

    dset_existing_resid_seq = dict()
    min_seqindex_molsyst2 = -1
    max_seqindex_molsyst1 = -1

    for ch_seg in dpdb:
        print(ch_seg)
        set_existing_resid_seq = set()  # which residues in the sequence have a structural equivalent
        # Get the correspondances between index in seq and in pdb
        """ 
        # global_dmap[ch_seg]['s2p'][index in sequence]     , val = index in structure (not the resid itself but alsolute index)
        #                    ['p2s'][abs index in structure], val = index in sequence
        """
        global_dmap[ch_seg] = map_pdb_seq(dic_seqinfo[ch_seg]['seqref'][0], dpdb[ch_seg]['seq'])
        dmap = global_dmap[ch_seg]

        dmap['r2s'] = {}  # real resid in pdb not absolute index
        dmap['s2r'] = {}  # real resid in pdb not absolute index
        for elt in dmap['s2p']:
            absolute_index = dmap['s2p'][elt]  # absolute_index in the pdb sequence starts at index 0
            res_index_pdb = dpdb[ch_seg]['resi'][absolute_index]
            set_existing_resid_seq.add(elt)
            dmap['r2s'][res_index_pdb] = elt
            dmap['s2r'][elt] = res_index_pdb
            # print(ch_seg,elt,absolute_index,res_index_pdb)
        dset_existing_resid_seq[ch_seg] = copy.copy(set_existing_resid_seq)

        # We define min and max to determine the type of coevolution map, focused on inter or full on both partners
        if 'equivalent_molsystem_1' in dequivalent_chain_segi \
                and ch_seg in dequivalent_chain_segi['equivalent_molsystem_1']:
            min_seqindex_molsyst2 = min(list(global_dmap[ch_seg]['s2p'].keys()))
            #print(ch_seg,min_seqindex_molsyst2)
        if ch_seg in dequivalent_chain_segi['equivalent_molsystem_0']:
            max_seqindex_molsyst1 = max(list(global_dmap[ch_seg]['s2p'].keys()))
            #print(ch_seg,max_seqindex_molsyst1)

    # ===========================================================================
    # Automatic detection of the type of coevolution map, full or focused on the inter signals (as in ComplexContacts)
    # ============================================================================

    if min_seqindex_molsyst2 == -1:
        print("Coevolution map was understood as calculated for a monomer or a homomeric complex")
        coevo_map_format = 'monomer_homomer'
    elif max_seqindex_molsyst1 < min_seqindex_molsyst2:
        print("Coevolution map was understood as calculated for a heteromeric complex containing both intra and inter molecular signals")
        coevo_map_format = 'heteromer_full'
    else:
        print("Coevolution map was understood as calculated for a heteromeric complex containing only inter molecular signals")
        coevo_map_format = 'heteromer_focused'

    # ===========================================================================
    # For all 3D contacts, defines all the correct contacts in the sequence space as intra or inter
    # ===========================================================================
    d_struct_contacts = contact_map(dpdb, distance_threshold, CA_CA_prefilter=CA_CA_prefilter)

    # ===========================================================================
    # For all 3D contacts, defines the corresponding contact in the sequence space
    # ===========================================================================
    set_cc_seq_intra, set_cc_seq_interhomo, set_cc_seq_interhetero = define_set_contacts(d_struct_contacts, global_dmap, dequivalent_chain_segi, distance_threshold)

    # ===========================================================================
    # Dump a file showing the correspondance between sequence indexes (starting from 0) and pdb struct res index
    # ===========================================================================
    if verbose:
        print("Verbose : Dump the table showing the correspondence between sequence and structures")
        dump_global_dictionary(global_dmap, dequivalent_chain_segi, 'verbose.tmp')

    # ===========================================================================
    # Add a redundancy group if required. Save a new file with the redundancy group added
    # ===========================================================================
    f_coevolution_map_to_use = fcoupling
    if add_redundancy:
        fcoupling_with_group_path = os.path.splitext(fcoupling)[0] + "_rdgrp.txt"
        append_redundancy_group(fcoupling,fcoupling_with_group_path,range_index_redundancy_group=2, is_value_proba=True)
        f_coevolution_map_to_use = fcoupling_with_group_path

    # ===========================================================================
    # Recovers and analyze the couplings file to assign the flag element in list_couplings
    # ===========================================================================
    list_couplings, nb_interhomo_all, nb_interhetero_all, nb_intra_all, nb_false_all = analyze_fcoupling(
        f_coevolution_map_to_use,
        thresh_index,
        dset_existing_resid_seq,
        set_cc_seq_intra,
        set_cc_seq_interhomo,
        set_cc_seq_interhetero,
        dequivalent_chain_segi,
        coevo_map_format,
        threshold_coupling=threshold_coupling)

    #print(nb_interhomo_all, nb_interhetero_all, nb_intra_all, nb_false_all)

    # ===========================================================================
    # Print the values for the precision curve
    # ===========================================================================
    if not only_dump_pml_file:
        print_precision_file(out, list_couplings, nb_intra_all, nb_interhomo_all, nb_interhetero_all)

    # ===========================================================================
    # Recover a dictionnary of all contacts correctly and badly predicted
    # ===========================================================================
    d_contact_assign = map_pred_contacts_in_pdb(list_couplings, d_struct_contacts, global_dmap, dequivalent_chain_segi,
                                                dpdb, pdbname=pdb_rootname)

    # ===========================================================================
    # Print the macro for contact visualisation in pymol
    # ===========================================================================
    if only_dump_pml_file:
        print_pml_file_for_pymol(out, d_contact_assign,
                                 type_contacts=['false_hetero', 'interhetero'],
                                 pdbname=pdb_rootname)
    else:
        print_pml_file_for_pymol(out, d_contact_assign,
                                 type_contacts=['intra', 'interhomo', 'false_hetero', 'interhetero'],
                                 pdbname=pdb_rootname)


if __name__ == '__main__':

    example = """Example :
    MapCoevolmap2Structures.py -p receptor.pdb -f sequence1.fasta:sequence2.fasta -l couplings.list -w ./ -d 8.0 -c AA,AB:BA,BB -m heteromer\n
    MapCoevolmap2Structures.py -p receptor.pdb -f sequence1.fasta -l couplings.list -w ./ -d 8.0 -c AA,AB -m homomer\n"""
    parser = argparse.ArgumentParser(
        description="""Script to map DCA couplings and check whether they respect the distance threshold. Runs in python3""",
        epilog=example,
        prog='MapCoevolmap2Structures.py',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    debug = {
        'pdb': 'Complex_CMAPnb2m.pdb',
        'sequence': 'P43895.fasta:P60338.fasta',
        'coupling': 'ComplexContact_cont.txt',
        'working_dir': 'C:\\Users\\rg152862\Documents\WORK\InterEvDock3\CoevolutionBenchmark\EFTU_EFTS\\2b_P43895_P60338',
        'output': 'Complex_CMAPnb2m',
        'distance_thresh': 8.0,
        'chain': 'AA,BA:CB',
        'add_redundancy': True,
        'threshold_coupling': 0.01,
        'only_dump_pml_file': True,
        'mode': 'heteromer'
    }
    parser.add_argument('-p', '--pdb', help='Pdb input file')#, default=debug['pdb'])
    parser.add_argument('-f', '--sequence',help='Sequence input file in fasta format. If heterodimer, use colons ":" between the sequence name of the two files') #, default=debug['sequence'],
    parser.add_argument('-l', '--coupling', help='DCA couplings input file')#, default=debug['coupling'])
    parser.add_argument('-w', '--working_dir', help='Which directory are running in ?', default="./")#debug['working_dir'])
    parser.add_argument('-o', '--output', help='Output file', default=None)#debug['output'])
    parser.add_argument('-d', '--distance_thresh', default=debug['distance_thresh'], type=float,
                        help='Distance thresholds for positive couplings')
    parser.add_argument('-c', '--chain_segid', help='How to interpret the chains and segids in the pdb? \
                                                    Automatically inferred for homomers but required for heterodimers with homomeric subunits, \
                                                    Write AA,AB:BA,BB for heteromer of chain A homomer (segid A and B) versus chain B homomer (segid A and B)',
                        default=None)
    parser.add_argument('-m', '--mode', default=debug['mode'], choices=['monomer', ' homomer', 'heteromer'],
                        help='What is the nature of the pdb input ?')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)
    parser.add_argument('-a', '--add_redundancy', action='store_true', default=False)
    parser.add_argument('-t', '--threshold_coupling', action='store', type=float, default=debug['threshold_coupling'])
    parser.add_argument('-y', '--only_dump_pml_file', action='store_true', default=False)

    args = parser.parse_args()

    if args.pdb and args.sequence and args.coupling and args.chain_segid:
        main(
            args.pdb,
            args.sequence,
            args.coupling,
            args.chain_segid,
            distance_threshold=args.distance_thresh,
            out=args.output,
            wdir=args.working_dir,
            mode=args.mode,
            verbose=args.verbose,
            add_redundancy=args.add_redundancy,
            CA_CA_prefilter=20,
            threshold_coupling=args.threshold_coupling,
            only_dump_pml_file=args.only_dump_pml_file
        )
    else:
        parser.print_help()
