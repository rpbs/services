#! /usr/bin/env python
"""
Consensus calculation module
Last modified: CQ Nov. 2020
Based on initial scripts in InterEvDock
"""

import os,sys
import optparse
import numpy as np
import pickle as pk
import re
from collections import defaultdict
import itertools


class Pdb(object):
    """ Object that allows operations with protein files in PDB format. """

    def __init__(self, CHAINB, file_cont = [], pdb_code = ""):
        self.cont = []
        self.atom = []
        self.hetatm = []
        self.fileloc = ""
        self.CHAINB = CHAINB
        if isinstance(file_cont, list):
            self.cont = file_cont[:]
            
        elif isinstance(file_cont, str):
            try:
                FHi = open(file_cont, 'r')
                self.cont = [row.strip() for row in FHi.read().split('\n') if row.strip()]
                FHi.close()
            except IOError:
                print("FileNotFoundError")

        if self.cont:
            self.atom   = [row for row in self.cont if row.startswith('ATOM')]
            self.hetatm = [row for row in self.cont if row.startswith('HETATM')]
            self.conect = [row for row in self.cont if row.startswith('CONECT')]

    def calpha(self):
        return [row for row in self.cont if row.startswith('ATOM') and row[13:15]=='CA']
    
    def calpha_chainB(self):
        return [row for row in self.cont if row.startswith('ATOM') and row[13:15]=='CA' and row[21] in self.CHAINB]
    
    def rmsd(self, sec_molecule, ligand=False, atoms="no_h"):
        """
        Calculates the Root Mean Square Deviation (RMSD) between two
        protein or ligand molecules in PDB format.
        Requires that both molecules have the same number of atoms in the
        same numerical order.
        Keyword arguments:
            sec_molecule (PdbObj): the second molecule as PdbObj object.
            ligand (bool): If true, calculates the RMSD between two
                ligand molecules (based on HETATM entries), else RMSD
                between two protein molecules (ATOM entries) is calculated.
:            hydrogen (bool): If True, hydrogen atoms will be included in the
                    RMSD calculation.
            atoms (string) [all/c/no_h/ca]: "all" includes all atoms in the RMSD calculation,
                "c" only considers carbon atoms, "no_h" considers all but hydrogen atoms,
                and "ca" compares only C-alpha protein atoms.
        Returns:
            Calculated RMSD value as float or None if RMSD not be
            calculated.
        """
        rmsd = None

        if not ligand:
            coords1, coords2 = self.atom, sec_molecule.atom
        else:
            coords1, coords2 = self.hetatm, sec_molecule.hetatm

        if atoms == "c":
            coords1 = [row for row in coords1 if row[77:].startswith('C')]
            coords2 = [row for row in coords2 if row[77:].startswith('C')]
        elif atoms == "no_h":
            coords1 = [row for row in coords1 if not row[77:].startswith('H')]
            coords2 = [row for row in coords2 if not row[77:].startswith('H')]
        elif atoms == "ca":
            coords1 = self.calpha()
            coords2 = sec_molecule.calpha()
        elif atoms == "caB":
            coords1 = self.calpha_chainB()
            coords2 = sec_molecule.calpha_chainB()

        if coords1 and coords2 and len(coords1) == len(coords2):
            total = 0
            for (i, j) in zip(coords1, coords2):
                total += ( float(i[30:38]) - float(j[30:38]) )**2 +\
                         ( float(i[38:46]) - float(j[38:46]) )**2 +\
                         ( float(i[46:54]) - float(j[46:54]) )**2      
            rmsd = round(( total / len(coords1) )**0.5, 4)
        return rmsd

class MatrixRmsd:

    def __init__(self,pdb_lists,d_pdb2num,CHAINB,output=None):
        self.CHAINB = CHAINB
        self.pdb_lists = pdb_lists
        self.d_pdb2num = d_pdb2num

        self.list_pairs = []
        # top 10 x top 50
        for pdbs1 in pdb_lists:
            for pdbs2 in pdb_lists:
                for pdb1 in pdbs1[:10]:
                    self.list_pairs+=[tuple(sorted([self.d_pdb2num.get(pdb1,pdb1),self.d_pdb2num.get(pdb2,pdb2)])) for pdb2 in pdbs2]
        self.list_pairs = sorted(list(set(self.list_pairs)))

        self.d_num2pdb = {}
        for k,v in self.d_pdb2num.items():
            if v not in self.d_num2pdb:
                self.d_num2pdb[v] = []
            self.d_num2pdb[v].append(k)

        
    def computeRmsdMatrix(self):
        dmat = {}

        # regroup pairs
        for p1,p2 in self.list_pairs:
            if p1 in dmat:
                dmat[p1][p2] = 0
            elif p2 in dmat:
                dmat[p2][p1] = 0
            else:
                dmat[p1]={p2:0}

        # compute RMSD
        for p1 in dmat:
            pdb1 = Pdb(self.CHAINB,self.d_num2pdb.get(p1,[p1])[0])
            for p2 in dmat[p1]:
                if p1==p2:
                    dmat[p1][p2] = 0.
                else:
                    pdb2 = Pdb(self.CHAINB,self.d_num2pdb.get(p2,[p2])[0])
                    dmat[p1][p2] = pdb1.rmsd(sec_molecule=pdb2, ligand=False, atoms="caB")

        # complete missing pdbs and sym and revert to names
        dnewmat = {}
        for p1 in dmat:
            names1 = self.d_num2pdb.get(p1,[p1])
            for p2 in dmat[p1]:
                names2 = self.d_num2pdb.get(p2,[p2])
                for name1 in names1:
                    if name1 not in dnewmat:
                        dnewmat[name1] = {}
                    for name2 in names2:
                        if name2 not in dnewmat:
                            dnewmat[name2] = {}
                        dnewmat[name1][name2] = dmat[p1][p2]
                        dnewmat[name2][name1] = dmat[p1][p2]

        return dnewmat
    

class GetConsensus:
    
    def __init__(self,pdbfile_lists,fileout,CHAINB):

        ### Initialisation of useful lists and dictionnaries ###
        # Which scores to consider
        outputdir = os.path.dirname(fileout)
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)

        pdb_lists = []
        self.d_pdb2num = {}
        for it,pdbfile_list in enumerate(pdbfile_lists):
            f=open(pdbfile_list)
            pdbs=[l.split() for l in f if l.strip() and not l.startswith('#')]
            f.close()
            if len(pdbs[0]) == 2:
                # decoy number is specified
                self.d_pdb2num.update(dict([(p[0],p[1]) for p in pdbs]))
                pdbs=[p[0] for p in pdbs if os.path.exists(p[0])]
            else:
                pdbs=[p[0] for p in pdbs if os.path.exists(p[0])]
            pdb_lists.append(pdbs)

        MR = MatrixRmsd(pdb_lists,self.d_pdb2num,CHAINB)
        self.dmat = MR.computeRmsdMatrix()
        pk.dump(self.dmat,open(fileout+"_Top10Consensus_rmsd.p",'wb'))

        FHout_txt_all = open(fileout+"_Top10Consensus_all.txt","w")
        FHout_txt_all.close()
        FHout_txt = open(fileout+"_Top10Consensus.txt","w")
        FHout_txt.close()
        # FHout = open(fileout+"_Top10Consensus.html","w")
        # FHout.close()

        names = [os.path.splitext(os.path.basename(p))[0] for p in pdbfile_lists]
        d_results = {}
        d_results["_".join(names)] = self.calc_consensus(pdb_lists,fileout+"_Top10Consensus.txt"," ".join(names))
        # all permutation for first level with all scores
        for idx in itertools.permutations(range(len(pdb_lists))):
            # print('permutation '+" ".join([names[i] for i in idx]))
            d_results["_".join([names[i] for i in idx])] = self.calc_consensus([pdb_lists[i] for i in idx],fileout+"_Top10Consensus_all.txt"," ".join([names[i] for i in idx]))

        # all combinations of sub groups of min 3 scores
        for r in range(3,len(pdb_lists)+1):
            for idx in itertools.combinations(range(len(pdb_lists)),r):
                for idx2 in itertools.permutations(idx):
                    if "_".join([names[i] for i in idx2]) not in d_results:
                        # print('permutation '+" ".join([names[i] for i in idx2]))
                        d_results["_".join([names[i] for i in idx2])] = self.calc_consensus([pdb_lists[i] for i in idx2],fileout+"_Top10Consensus_all.txt"," ".join([names[i] for i in idx2]))

        if self.d_pdb2num:
            np.savez(fileout+"_Top10Consensus_decoynum.npz",**d_results)

    def calc_consensus(self,pdb_lists,fileout,order):

        # evenly divide 10 by the number of scores in the consensus
        # e.g. 3 scores -> top 4 + top 3 + top 3
        #      4 scores -> top 3 + top 3 + top 2 + top 2
        #      etc.
        q=10//len(pdb_lists)
        r=10%len(pdb_lists)
        lgs = [q+1 for i in range(r)]+[q for i in range(len(pdb_lists)-r)]
        selection_order = []
        for i in range(len(pdb_lists)):
            selection_order += pdb_lists[i][:lgs[i]]
        if r:
            lgs = [0 for i in range(r)]+[q for i in range(len(pdb_lists)-r)]
            for i in range(len(pdb_lists)):
                if lgs[i]:
                    try: # in case we have less than 10 decoys
                        selection_order += [pdb_lists[i][lgs[i]]]
                    except:
                        pass
        if q+1 <= len(pdb_lists[0]):
            for l in range(q+1,min(10,len(pdb_lists[0]))):
                for i in range(len(pdb_lists)):
                    selection_order += [pdb_lists[i][l]]

        # DISTANCE THRESHOLD 
        self.THRESH_LRMSD_ACC = 10.
        self.THRESH_LRMSD_MED = 5.
        self.LEN_FINAL_CONSENSUS = 10
        self.SCORE_INI = 20

        self.dic_consensus = dict([(p,self.SCORE_INI) for pl in pdb_lists for p in pl[:10]]) # Initial score that will be decreased for the most preferred models

        for pdb_list1 in pdb_lists:
            for pdb1 in pdb_list1[:10]:
                for pdb_list2 in pdb_lists:
                    for pdb2 in pdb_list2:
                        if pdb1==pdb2:
                            continue
                        if self.dmat.get(pdb1,{}).get(pdb2,0.) <= self.THRESH_LRMSD_MED :
                            if pdb_list1 == pdb_list2: 
                                self.dic_consensus[pdb1]+=0 # from same score, we don't reward
                            else:
                                self.dic_consensus[pdb1]+=-1 # different score, we reward
                        elif self.dmat.get(pdb1,{}).get(pdb2,0.) <= self.THRESH_LRMSD_ACC :
                            if pdb_list1 == pdb_list2: 
                                self.dic_consensus[pdb1]+=0 # from same score, we don't reward
                            else:
                                self.dic_consensus[pdb1]+=-1  # different score, we reward  

        d_priority_order = dict([(p,i+50*j) for j,pl in enumerate(pdb_lists) for i,p in enumerate(pl)]) 
        list_results = [(self.dic_consensus[k],k) for pl in pdb_lists for k in pl[:10]]

        list_results.sort(key=lambda k: (k[0],d_priority_order[k[1]]))

        remove=0
        count_score_above1 = 0
        
        best_consensus = []
        # First we recover up to 10 models that are interconnected between the different scoring systems
        for score,model in list_results:
            if model not in selection_order:
                remove+=1
            if score < self.SCORE_INI - 1 : # requires at least 2 connections to be processed
                count_score_above1 += 1
                if count_score_above1 > self.LEN_FINAL_CONSENSUS:
                    break
                add_model = True
                key1 = model
                for elt in best_consensus:
                    key2 = elt

                    if self.dmat.get(key1,{}).get(key2,0.) < self.THRESH_LRMSD_ACC :
                        add_model = False
                if add_model:
                    best_consensus.append(model)  
            else:
                break
        
        # We fill with the models 
        for best_model in selection_order:
            if len(best_consensus)>= self.LEN_FINAL_CONSENSUS:
                break
            if best_model not in best_consensus:
                add_model = True
                # Before adding a model to the selection, we check whether
                # that no other closely related model was previously selected
                for elt in best_consensus:
                    key1 = best_model
                    key2 = elt
                    if self.dmat.get(key1,{}).get(key2,0.) < self.THRESH_LRMSD_ACC:
                        add_model = False
                if add_model:
                    best_consensus.append(best_model)

        # We fill with the models in case len(best_consensus) < 10 because all top 10 models are too similar
        for best_model in selection_order:
            if len(best_consensus)>= self.LEN_FINAL_CONSENSUS:
                break
            if best_model not in best_consensus:
                add_model = True
                # Before adding a model to the selection, we check if
                # no other closely related model was previously selected
                for elt in best_consensus:
                    key1 = best_model
                    key2 = elt
                    if self.dmat.get(key1,{}).get(key2,0.) < 0.1: # probably the same decoy
                        add_model = False
                if add_model:
                    best_consensus.append(best_model)

        if self.d_pdb2num:
            list_decoynum_consensus = [self.d_pdb2num[elt] for elt in best_consensus]
        else:
            list_decoynum_consensus = [' ' for elt in best_consensus]

        ### print Output File ###
        FHout_txt = open(fileout,"a")
        FHout_txt.write("#{}-way consensus\n".format(len(order.split())))
        FHout_txt.write("#consensus_rank\tdecoy_name\n")
        for ii,elt in enumerate(best_consensus):
            FHout_txt.write("{}\t{}\n".format(ii+1,os.path.basename(elt)))
        FHout_txt.close()

        # Not used in IED3 server and confusing because residues aren't translated
        # FHout = open(fileout+"_Top10Consensus.html","a")
        # # Print opening HTML tags -------------------------
        # FHout.write("<html><body><table>\n")
        # # Print the content of the table, line by line ----
        # for ii,elt in enumerate(best_consensus):
        #    FHout.write("<tr><td>%d</td><td>%s</td></tr>\n"%(ii+1,os.path.basename(elt)))
        # # Print closing HTML tags -------------------------
        # FHout.write("</table></body></html>\n")
        # FHout.close()

        if self.d_pdb2num:
            return np.array([(ii+1,self.d_pdb2num[elt]) for ii,elt in enumerate(best_consensus)],dtype=[('rank', '<i4'), ('decoy_num', '<i4')])
            
        return None
        

class GetResInterface():

    def __init__(self,pdbfile_lists,output_res_score_file,chainA,chainB):

        self.one_letter = {}
        self.one_letter["ALA"]="A"
        self.one_letter["CYS"]="C";self.one_letter["CME"]="C";self.one_letter["CSE"]="C";self.one_letter["CSD"]="C";self.one_letter["CSO"]="C";self.one_letter["CSS"]="C";self.one_letter["CCS"]="C";self.one_letter["P1L"]="C";self.one_letter["CMT"]="C";self.one_letter["CSZ"]="C";self.one_letter["CAS"]="C"
        self.one_letter["ASP"]="D"
        self.one_letter["GLU"]="E";self.one_letter["PCA"]="E"
        self.one_letter["PHE"]="F"
        self.one_letter["GLY"]="G";self.one_letter["GLZ"]="G"
        self.one_letter["HIS"]="H";self.one_letter["DDE"]="H";self.one_letter["HIC"]="H" ;self.one_letter["NEP"]="H"
        self.one_letter["ILE"]="I"
        self.one_letter["LYS"]="K";self.one_letter["KCX"]="K";self.one_letter["MLY"]="K";self.one_letter["KCX"]="K";self.one_letter["LLP"]="K";self.one_letter["LYZ"]="K"
        self.one_letter["LEU"]="L"
        self.one_letter["MET"]="M";self.one_letter["MSE"]="M";self.one_letter["CXM"]="M";self.one_letter["FME"]="M";
        self.one_letter["ASN"]="N";self.one_letter["MEN"]="N"
        self.one_letter["PRO"]="P"
        self.one_letter["GLN"]="Q"
        self.one_letter["ARG"]="R";self.one_letter["ARO"]="R"
        self.one_letter["SER"]="S";self.one_letter["SEP"]="S";self.one_letter["PN2"]="S"
        self.one_letter["THR"]="T";self.one_letter["TPO"]="T";
        self.one_letter["VAL"]="V"
        self.one_letter["TRP"]="W";self.one_letter["TRF"]="W";self.one_letter["TRQ"]="W"
        self.one_letter["TYR"]="Y";self.one_letter["PTR"]="Y";self.one_letter["PAQ"]="Y"

        # Which scores to consider
        outputdir = os.path.dirname(output_res_score_file)
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)

        # get list of pdbs
        pdb_lists = []
        for it,pdbfile_list in enumerate(pdbfile_lists):
            f=open(pdbfile_list)
            pdbs=[l.split() for l in f if l.strip() and not l.startswith('#')]
            f.close()
            pdbs=[p[0] for p in pdbs]
            pdb_lists.append(pdbs)


        consensustop10 = {} # method (or all) -> chain -> residue -> consensus = percentage of models containing this residue
        consensustop10["all"] = dict([(chA,{}) for chA in chainA]+[(chB,{}) for chB in chainB]) #{chainA:{},chainB:{}}

        # Find consensus for each method and combined consensus
        for pdb_file_lst,method in zip(pdb_lists,pdbfile_lists):
            # for each method, find all decoys          
            consensustop10[method] = dict([(chA,{}) for chA in chainA]+[(chB,{}) for chB in chainB]) # chain -> residue -> consensus = percentage of models containing this residue

            for pdbfile in pdb_file_lst[:10]:
                # find the list of residues in contact for this PDB file
                lst_chainA_contacts, lst_chainB_contacts, res_types = self.find_res_in_contact(os.path.abspath(pdbfile), 5.0, chainA, chainB)

                # Integrate those results into the consensus
                for chA,rA in lst_chainA_contacts:
                    if rA not in consensustop10[method][chA]:
                        consensustop10[method][chA][rA] = 0.
                    if rA not in consensustop10["all"][chA]:
                        consensustop10["all"][chA][rA] = 0.
                    consensustop10[method][chA][rA] += 1./10
                    consensustop10["all"][chA][rA] += 1./(10*len(pdbfile_lists))
                        
                for chB,rB in lst_chainB_contacts:
                    if rB not in consensustop10[method][chB]:
                        consensustop10[method][chB][rB] = 0.
                    if rB not in consensustop10["all"][chB]:
                        consensustop10["all"][chB][rB] = 0.
                    consensustop10[method][chB][rB] += 1./10
                    consensustop10["all"][chB][rB] += 1./(10*len(pdbfile_lists))

        #########################################################################################################################################
        # Overwrite all decoy files, replacing the occupancy column with the occupancy calculated from consensustop10
        for pdb_file_lst in pdb_lists:
            for pdbfile in pdb_file_lst:
                # write output PDB file containing the consensus interface in the occupancy column
                self.replace_occupancy(pdbfile, consensustop10["all"])

        #########################################################################################################################################

        # Final consensus (from top10 from all consensus methods) to be displayed in a table
        # 5 residues on chain A and 5 residues on chain B
        top5resAintop10 = self.find_consensus(consensustop10, chainA, pdbfile_lists)
        top5resBintop10 = self.find_consensus(consensustop10, chainB, pdbfile_lists)

        print("Consensus top5 residues on each chain are written to %s_top5_residues.txt"%output_res_score_file)
        self.write_table(output_res_score_file, res_types, top5resAintop10, top5resBintop10, chainA, chainB)

       
    def squared_eucl_dist(self, lcoor1,lcoor2):
        """ find euclidean distance between coordinates in lcoor1 and coordinates in lcoor2
        return distance as a float
        """
        return (lcoor1[0] - lcoor2[0])**2 + (lcoor1[1] - lcoor2[1])**2 + (lcoor1[2] - lcoor2[2])**2
        

    def find_all_pdb_coor(self, input_PDB):
        """ Extract all coordinates from input_PDB
        """
        FHi = open(input_PDB, 'r')

        coor_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))
        res_types = defaultdict(lambda: defaultdict(lambda: "X"))

        for line in FHi:
            if line.startswith('ATOM'):
                try:
                    c = line[21]
                    r = int(line[22:26])
                    t = self.one_letter.get(line[17:20],'X')
                    a = re.sub(" ", "",line[12:16])
                    x, y, z = (float(line[30:38]),float(line[38:46]),float(line[46:54]))
                    coor_dict[c][r][a] = [x,y,z]
                    res_types[c][r] = t
                except:
                    print("Error: Not valid pdb coor line")
        FHi.close()

        return coor_dict, res_types

    def find_contact(self, coorA, coorB, thresh):
        """ Find if two residues are in contact (at least one atom in rA within thresh (distance) of one atom in rB)
        """
        # Prefilter for speed
        if "CA" in coorA and "CA" in coorB:
            if self.squared_eucl_dist(coorA["CA"], coorB["CA"]) >= 400.0:
                return False
        
        for aA in coorA:
            for aB in coorB:
                d = self.squared_eucl_dist(coorA[aA], coorB[aB])
                if d <= thresh:
                    return True

    def find_res_in_contact(self, input_PDB, thresh, chainA, chainB):
        """ Find lists of residues in contact
        """
        thresh = thresh**2

        coor_dict, res_types = self.find_all_pdb_coor(input_PDB)

        res_lst_A = set()
        res_lst_B = set()

        for chA in chainA:
            for rA in coor_dict[chA]:
                for chB in chainB:
                    for rB in coor_dict[chB]:
                        if self.find_contact(coor_dict[chA][rA], coor_dict[chB][rB], thresh):
                            res_lst_A.add((chA,str(rA)))
                            res_lst_B.add((chB,str(rB)))
                            
        return sorted(list(res_lst_A)), sorted(list(res_lst_B)), res_types

    def find_consensus(self, consensustop10, chain, priority_order):
        """ find the top5 consensus for a given chain from the dictionary of occupancy based on 10+10+10 best models
        If there is a tie, choose based on the best InterEvScore occupancy.
        If there is still a tie, choose based on the best soap_pp occupancy.
        """
        top5resAconsensus = [] # list where the top5 residues will be stored
        prevlist = [] # buffer list of residues with same "all" occupancy
        threshold = 0.0
        prevsc = 1.0
        prevscore = 1.0
        sorted_list = sorted([(score,rA,ch) for ch in chain for rA,score in consensustop10["all"][ch].items()], reverse=True)
        # for ite,rA in enumerate(sorted(consensustop10["all"][chain], key=consensustop10["all"][chain].get, reverse=True)):
        for ite,(score,rA,chA) in enumerate(sorted_list):
            # loop through residues in order of decreasing "all" occupancy
            score = consensustop10["all"][chA].get(rA,0.0)
            if score < threshold:
                # We have filled prevlist with all residues that have the same occupancy as the 5th best residue
                # Check how many residues are missing
                tofill = 5-len(top5resAconsensus)
                score1 = {}
                if len(prevlist)==tofill:
                    # we can just take those to fill the list
                    for r in prevlist:
                        top5resAconsensus.append(r)
                else:
                    # Choose remaining to fill residues based on the highest priority score occupancy
                    for r in prevlist:
                        score1[r] = (consensustop10[priority_order[0]][r[1]].get(r[0],0.0))
                    redlist = [] # buffer list of residues with same highest priority score occupancy
                    thresh = 0.0
                    for ii,rr in enumerate(sorted(score1, key=score1.get, reverse=True)):
                        # loop through residues in order of decreasing IvS occupancy
                        sc = consensustop10[priority_order[0]][rr[1]].get(rr[0],0.0)
                        if sc < thresh:
                            # Check how many residues are still missing and choose them on the basis of 2nd highest priority score occupancy
                            stilltofill = 5-len(top5resAconsensus)
                            score2 = {}
                            for rrr in redlist:
                                score2[rrr] = consensustop10[priority_order[1]][rrr[1]].get(rrr[0],0.0)
                            for rrr in sorted(score2, key=score2.get, reverse=True)[0:stilltofill]:
                                top5resAconsensus.append(rrr)
                            break
                        if ii >= 1:
                            if ii==tofill:
                                # we have reached the fifth residue
                                thresh = sc
                            if sc < prevsc:
                                for rrr in redlist:
                                    top5resAconsensus.append(rrr)
                                redlist = [rr]
                            else:
                                redlist.append(rr)
                        elif ii == 0:
                            # first time through loop
                            redlist.append(rr)
                        prevsc = sc

                    # flush the last residues that may remain in redlist (take the best occupancies of the 2nd highest priority score)
                    stilltofill = 5-len(top5resAconsensus)
                    score2 = {}
                    for rrr in redlist:
                        score2[rrr] = consensustop10[priority_order[1]][rrr[1]].get(rrr[0],0.0)
                    for rrr in sorted(score2, key=score2.get, reverse=True)[0:stilltofill]:
                        top5resAconsensus.append(rrr)
                        
                break
            if ite>=1:
                if ite==4:
                    # we have reached the fifth residue
                    threshold = score
                if score < prevscore:
                    for r in prevlist:
                        top5resAconsensus.append(r)
                    prevlist = [(rA,chA)]
                else:
                    prevlist.append((rA,chA))
            elif ite==0:
                # residue with best absolute occupancy
                prevlist.append((rA,chA))
            prevscore = score

        return top5resAconsensus

    def replace_occupancy(self, input_PDB, consensus):
        """ Reads input_PDB and writes output_PDB which is identical to input_PDB except for updated occupancy values.
        Occupancy values are based on the interface predictions for this method (InterEvScore, frodock or soap_pp).
        """
        
        FHi = open(input_PDB, 'r')
        lines = FHi.readlines()
        FHi.close()
        
        FHo = open(input_PDB, 'w')
        for line in lines:
            if line.startswith("ATOM"):
                # find chain and residue id and replace occupancy accordingly
                c = line[21]
                r = str(int(line[22:26]))
                occ = consensus.get(c,{}).get(r,0.0)
                FHo.write(line[0:54]+"  %4.2f"%occ+line[60:])
            elif line.startswith("HETATM"):
                # HETATM line, replace occupancy with 0
                FHo.write(line[0:54]+"  %4.2f"%(0.0)+line[60:])
            else:
                # keep all non-ATOM and non-HETATM lines unchanged
                FHo.write(line)
        FHo.close()


    def write_table(self, dir_target, res_types, top5resAintop10, top5resBintop10, chainA, chainB):
        """ Write final table with top5 residues for each chain
        """
        o = open(dir_target+'_top5_residues.txt', 'w')
        o.write("#Below are the top 5 residues (on each protein partner) predicted to be involved in contacts based on the consensus of top 10 models from each method.\n")
        o.write("#rank\tproteinA\tproteinB\n")
        for i in range(5):
            # this should not normally happen, but control it just in case
            if len(top5resAintop10) < (i+1):
                rA = " "
            else:
                rA = res_types[top5resAintop10[i][1]][int(top5resAintop10[i][0])]+top5resAintop10[i][0]+top5resAintop10[i][1]
            if len(top5resBintop10) < (i+1):
                rB = " "
            else:
                rB = res_types[top5resBintop10[i][1]][int(top5resBintop10[i][0])]+top5resBintop10[i][0]+top5resBintop10[i][1]
            o.write("%i\t%s\t%s\n"%(i+1, rA, rB))
        o.close()
        
        # o = open(dir_target+"_top5_residues.html", 'w')
        # o.write("<html><body><table>")
        # o.write("#Below are the top 5 residues (on each protein partner) predicted to be involved in contacts based on the consensus of top 10 models from each method.<br>\n")
        
        # o.write("<tr><td>rank</td><td>proteinA</td><td>proteinB</td></tr>\n")
        # for i in range(5):
        #     # this should not normally happen, but control it just in case
        #     if len(top5resAintop10) < (i+1):
        #         rA = " "
        #     else:
        #         rA = res_types[top5resAintop10[i][1]][int(top5resAintop10[i][0])]+top5resAintop10[i][0]+top5resAintop10[i][1]
        #     if len(top5resBintop10) < (i+1):
        #         rB = " "
        #     else:
        #         rB = res_types[top5resBintop10[i][1]][int(top5resBintop10[i][0])]+top5resBintop10[i][0]+top5resBintop10[i][1]
        #     o.write("<tr><td>%i</td><td>%s</td><td>%s</td></tr>\n"%(i+1, rA, rB))
            
        # o.write("</table></body></html>")
        # o.close()


if __name__=="__main__":

    if len(sys.argv)==1:
        sys.argv.append('-h')
    usage = """
            usage: %prog -l <comma separated file names containing paths to top 50 decoys for each score> -a <receptor chain name> -b <ligand chain name> -o <output file name without extention>
            """
    parser = optparse.OptionParser(usage = usage)         

    parser.add_option("-l", "--pdb_lists", 
                      dest="pdb_lists",
                      help="Path to the files specifying where top 50 models are stored in the right priority order e.g. top50SPP.txt,top50IES.txt,top50FD.txt. Each file should contain a list of the full path names for the top 50 decoys for that score in decreasing scoring order.")
    parser.add_option("-o", "--output_filename", 
                      dest="output",default="./Top10Consensus",
                      help="Basename of the outputfile (default:%default)")
    parser.add_option("-a", "--chain_receptor", 
                      dest="chainA",default="A",
                      help="Name of the receptor chain(s) e.g. A or AB (default:%default)")
    parser.add_option("-b", "--chain_ligand", 
                      dest="chainB",default="B",
                      help="Name of the ligand chain(s) e.g. C or CD (default:%default)")

    (options, args) = parser.parse_args()

    pdb_lists       = [os.path.abspath(pdb_list) for pdb_list in options.pdb_lists.split(',')]
    fileoutput      = os.path.abspath(options.output)
    chainA          = options.chainA
    chainB          = options.chainB

    GC = GetConsensus(pdb_lists,fileoutput,chainB)

    GC = GetResInterface(pdb_lists,fileoutput,chainA,chainB)

