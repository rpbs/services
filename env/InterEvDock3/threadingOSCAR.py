#! /usr/bin/env python3
"""
Threading tools
Last modified: CQ Nov. 2020
"""
import os, sys
import tempfile
import shutil
import re
import string
import math
import numpy as np
import pkg_resources
alphabet = string.ascii_uppercase + string.ascii_lowercase + string.digits
SEED = 9031

# for python version compatibility
try:
    import ConfigParser
except:
    import configparser as ConfigParser
# for python version compatibility
try:
    import commands
except:
    import subprocess as commands

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini")) # config file in the same folder as this script
OSCARBIN = config.get('programs','pyoscar')
PYTHON = config.get('scripts','python')
MAFFT = config.get('programs','mafft')

import PyPDB.PyPDB as PDB
import InterEvDock3.tools as tools
# defined docker file where OSCAR is installed
DOCKEROSCAR = config.get('environments','pyoscar')
DOCKERMAFFT = config.get('environments','mafft')

import cluster.cluster as cluster

# input 
# a set of 1 or several MSAs for 1 partner
# a set of 1 or several templates (1 for each chain in the partner or 1 with all the chains)

class runOSCAR():
    """
    class runOSCAR
    This runs OSCAR from sequence alignments and template structures alone
    """
    def __init__(self,output="",new_chains=[],unblock=False,renumber=False,realign=True,verbose=False,parallelise=True):
        """
        output         output pdb abs path base name (e.g. homolog_pdbs/homolog_chainA -> homolog_pdbs/homolog_chainA_1.pdb,homolog_pdbs/homolog_chainA_2.pdb,etc.)
        new_chains     list of new chain names if not the same as template chain names
        unblock        if you don't want to block template side chains from
                       being the default if AAs are the same as in the query
        realign        if you want to realign the template pdb sequence onto the template 
                       sequence in the alignment
        """
        if not os.path.exists(os.path.abspath(os.path.dirname(output))):
            os.makedirs(os.path.abspath(os.path.dirname(output)))

        self.output = output
        self.new_chains = new_chains
        self.unblock = unblock
        self.realign = realign
        self.renumber = renumber
        self.alig_head, self.alig_seq = [], []
        self.pdb_tpl = None
        self.oldchains=[]
        self.parallelise = parallelise
        self.verbose = verbose

    def load_inputs(self,in_alignment,in_template):
        """
        in_alignment   path to clean query-template alignment file(s) in fasta
                       format (first query, then template sequence). If
                       several files (one for each chain), list as comma-
                       separated string.
        in_template    path to clean template structure(s) in pdb format
                       (template sequences have to exactly match the
                       sequences given in the alignments). If several files
                       (one for each chain), list as comma-separated string.
                       All chains can be in a single template file.
        """
        if isinstance(in_alignment,list):
            lg = len(in_alignment)
        else:
            lg = 1
        if self.new_chains:
            if lg != len(self.new_chains):
                self.new_chains = alphabet[:lg]
                print("Warning: chains won't be renamed because too many or not enough were specified")
        elif lg != 1: 
            self.new_chains = alphabet[:lg]
        else:
            self.new_chains = None

        self.load_templates(in_template)
        self.load_alignments(in_alignment)

        # check if we're dealing with the same number of chains in the template and the given alignment
        if len(self.alig_seq) != len(self.pdb_tpl.chnList()):
            print('Error: not same number of chains in alignments and templates provided')
            sys.exit()


    def load_templates(self,in_template):

        if isinstance(in_template,str):
            in_template = [in_template]

        if isinstance(in_template,list):
            for t in in_template:
                if not os.path.exists(t):
                    print('Cannot locate template {}'.format(t))
                    sys.exit()
            for tpl in in_template:
                p = PDB.PDB(tpl,hetSkip=1,altCare=0,keepH=0)
                p.clean()
                chlst=p.chnList()
                if self.new_chains!=None:
                    newchlst=self.new_chains[len(self.oldchains):len(self.oldchains)+len(chlst)]
                else:
                    newchlst=chlst
                for ch,nch in zip(chlst,newchlst):
                    p2=p[ch]
                    p2.chnRename(ch+':'+nch)
                    if self.pdb_tpl == None:
                        self.pdb_tpl = p2
                    else:
                        self.pdb_tpl += p2
                self.oldchains += chlst
        else:
            print('Unknown template input type')
            sys.exit()


    def read_fasta(self,multi_fasta_file):
        """Read a multi fasta file (multi_fasta_file) and output list of headers and sequences
        return (headers,seqs)
        """
        headers = []
        seqs = []
        FHi = open(multi_fasta_file)
        lines = FHi.readlines()
        for line in lines:
            line = line.rstrip('\n')
            if line.startswith(">"):
                headers.append(line[1:].strip())
                seqs.append("")
            else:
                try:
                    seqs[-1] += re.sub("[^AERTYIPQSDFGHKLMWCVN]","-",re.sub("[\t ]","",line))
                except:
                    print(line)
                    print("Fasta format error in threadingOSCAR read_fasta.")
        FHi.close()
        if self.verbose:
            print("fasta sequence",headers,seqs)
        return headers, seqs

    def write_fasta(self, header_list,seq_list,outfile,ignore_gaps=True):
        """
        Write MSA to fasta file

        Input:
        header_list: [str], list of sequence headers
        seq_list:    [str], list of corresponding sequences
        ignore_gaps: bool, if True, positions in the MSA that have only gaps are 
               removed before writing the file

        Output:
        outfile: str, name of the output fasta file
        """
        def remove_extra_gaps(seq_list):
            num_seq = len(seq_list)
            new_seq_list = ["" for i in range(num_seq)]
            for i in range(len(seq_list[0])):
                if set([seq_list[j][i] for j in range(num_seq)])==set(["-"]):
                    continue
                for j in range(num_seq):
                    new_seq_list[j]+=seq_list[j][i]
            return new_seq_list

        if len(header_list) != len(seq_list):
            print("Error: cannot write fasta file. Header number: {:d}, "+\
                "seq number: {:d}".format(len(header_list),len(seq_list)))
            sys.exit(1)
        if ignore_gaps:
            seq_list = remove_extra_gaps(seq_list)
        FHo = open(outfile, 'w')
        for i in range(len(header_list)):
            FHo.write('>{}\n'.format(header_list[i]))
            FHo.write("{}\n".format(seq_list[i]))
        FHo.close()


    def load_alignments(self,in_alignment):
        if isinstance(in_alignment,list):
            for it,a in enumerate(in_alignment):
                if not os.path.exists(a):
                    print('Cannot locate alignment {}'.format(a))
                    sys.exit()
                h,s = [],[]
                if self.realign:
                    if self.new_chains!=None:
                        h,s = self.realign_seq(a,self.pdb_tpl[self.new_chains[it]].aaseq())
                    else:
                        h,s = self.realign_seq(a,self.pdb_tpl[self.oldchains[it]].aaseq())
                else:
                    h,s = self.read_fasta(a)
                self.alig_head.append(h)
                self.alig_seq.append(s)

        elif isinstance(in_alignment,str):
            if not os.path.exists(in_alignment):
                print('Cannot locate alignment {}'.format(in_alignment))
                sys.exit()
            h,s = [],[]
            if self.realign:
                if self.new_chains!=None:
                    h,s = self.realign_seq(in_alignment,self.pdb_tpl[self.new_chains[it]].aaseq())
                else:
                    h,s = self.realign_seq(in_alignment,self.pdb_tpl[self.oldchains[it]].aaseq())
            else:
                h,s = self.read_fasta(in_alignment)
            self.alig_head.append(h)
            self.alig_seq.append(s)

        else:
            print('Unknown alignment input type')
            sys.exit()


    def realign_seq(self,aligfile,seq):
        """
        aligfile:  str, fasta file
        seq:       str, a sequence

        To add a sequence to an alignment.
        Returns (header list, sequence list)
        """
        seqfile = tools.make_tempfile(parent_dir='./',suffix_str=".fst")
        self.write_fasta(['pdb'],[seq],seqfile)
        outfile = tools.make_tempfile(parent_dir='./',suffix_str=".fst")
        args = ["--auto",
                "--seed {}".format(aligfile),
                seqfile,
                " > {}".format(outfile),
                "2>/dev/null"]
        cluster.runTasks(MAFFT, args, environment_module = DOCKERMAFFT, log_prefix = "run_oscar")
        h,s = self.read_fasta(outfile)
        os.remove(seqfile)
        os.remove(outfile)
        return (h[-1:]+h[1:-1],s[-1:]+s[1:-1])


    def compare_alig(self,seq_tpl,seq_q):
        # -identify regions in template that are not covered by query to remove them
        # -write OSCAR's mask sequence to block residue side chains that don't change
        seq_mask_oscar = ""
        seq_mask_template = ""
        idx = 0
        for i in range(len(seq_tpl)):
            if seq_tpl[i] == '-':
                continue
            if seq_q[i] != '-':
                seq_mask_template+=seq_tpl[i]
                if seq_q[i] == seq_tpl[i] and not self.unblock:
                    seq_mask_oscar+=seq_q[i].lower()
                else:
                    seq_mask_oscar+=seq_q[i].upper()
            else:
                seq_mask_template+="X"
            idx += 1

        return seq_mask_template, seq_mask_oscar


    def run(self,output_homologs='output_homologs.txt'):

        curdir = os.path.abspath(os.getcwd())
        os.chdir(os.path.dirname(os.path.abspath(self.output)))

        tmpdir = tools.make_tempdir(prefix_str='oscargen',parent_dir='./')
        tmpmask = os.path.join(tmpdir,'mask{}.txt')
        tmptpl = os.path.join(tmpdir,'tpl{}.pdb')
        
        list_homologs = []
        nb_input = len(self.alig_seq[0])-1
        for id_homolog in range(1,nb_input+1):
            if self.verbose:
                print('homolog {} chains {}'.format(id_homolog,self.pdb_tpl.chnList()))

            masks = []
            newpdb = None
            for chid,ch in enumerate(self.pdb_tpl.chnList()):
                # extract pairwise alig between template and homolog
                seq_mask_template, seq_mask_oscar = self.compare_alig(self.alig_seq[chid][0],self.alig_seq[chid][id_homolog])
                masks.append(seq_mask_oscar)

                if self.verbose:
                    print(ch)
                    print('>template sequence in MSA')
                    print(self.alig_seq[chid][0])
                    print('>homolog sequence in MSA')
                    print(self.alig_seq[chid][id_homolog])
                    print('>homolog mask sequence for OSCAR')
                    print(masks[chid])
                    print('>template mask sequence for PyPDB (i.e. regions that will be removed from the pdb)')
                    print(seq_mask_template)
                    print('>template pdb sequence before edit')
                    print(self.pdb_tpl[ch].aaseq())

                # readjust template           
                if newpdb == None:
                    rs = self.pdb_tpl[ch].subPDBFromMask(seq_mask_template)
                    rs = rs.set_seq_straight(seq_mask_oscar.upper())
                    if self.renumber:
                        rs.renumber(1)
                    newpdb = rs
                else:
                    rs = self.pdb_tpl[ch].subPDBFromMask(seq_mask_template)
                    rs = rs.set_seq_straight(seq_mask_oscar.upper())
                    if self.renumber:
                        rs.renumber(1)
                    newpdb += rs

                if self.verbose:
                    print('>template pdb sequence after edit')
                    print(newpdb[ch].aaseq())

            # write fasta mask input
            self.write_fasta(newpdb.chnList(),masks,tmpmask.format(id_homolog),ignore_gaps=False)

            # write template
            newpdb.out(tmptpl.format(id_homolog),hetSkip=1)
            list_homologs.append(self.output+'_'+str(id_homolog)+"_SC.pdb")

        if self.parallelise:
            args = ['-i {}'.format(tmptpl.format("${SLURM_ARRAY_TASK_ID}")),
                    '-s {}'.format(tmpmask.format("${SLURM_ARRAY_TASK_ID}")),
                    '-l {}'.format(os.path.basename(self.output)+'_${SLURM_ARRAY_TASK_ID}'),
                    '--seed {}'.format(SEED)]
            print(OSCARBIN + " " + " ".join(args))
            cluster.runTasks(OSCARBIN, args, tasks = nb_input, tasks_from = 1, environment_module = DOCKEROSCAR, log_prefix = "run_oscar") # job_opts to specify other slurm commands

        else:
            for id_homolog in range(1,nb_input+1):
                args = ['-i {}'.format(tmptpl.format(id_homolog)),
                        '-s {}'.format(tmpmask.format(id_homolog)),
                        '-l {}'.format(os.path.basename(self.output)+'_'+str(id_homolog)),
                        '--seed {}'.format(SEED)]
                print(OSCARBIN + " " + " ".join(args))
                cluster.runTasks(OSCARBIN, args, environment_module = DOCKEROSCAR, log_prefix = "run_oscar") 

        os.chdir(curdir)
        f=open(output_homologs,"w")
        f.write("\n".join(sorted(list_homologs,key=lambda k: int(re.search("_(\d+)_SC.pdb$",k).group(1))))+'\n')
        f.close()
        shutil.rmtree(tmpdir)



def main():
    import optparse
    
    USAGE = "python {} -a <fasta_file> -t <template_structure> -o <output_basename> [-c <new_chains>] [--unblock] [--renumber]\n".format(os.path.abspath(__file__))+\
            "Wrapper script to thread a query sequence onto a template using a clean pairwise alignment and the template pdb.\n"+\
            "By default, residues that are the same."
    parser = optparse.OptionParser(usage=USAGE)
    parser.add_option('-a','--alignment',action="store",dest='alignment',type='string',
                      help="path to MSA files in fasta format (first sequence is template, then all query and homologs). If several files (one for each chain), list as comma-separated string")
    parser.add_option('-t','--template',action="store",dest='template',type='string',
                      help="path to template structure in pdb format (template sequences have to exactly match the sequences given in the alignments if option realign is not used). If several files (one for each chain), list as comma-separated string")
    parser.add_option('-o','--output',action="store",dest='output',type='string',
                      help="output pdb name")
    parser.add_option('-c','--chains',action="store",dest='new_chains',type='string',
                      help="if you want to specify new chain names for the output, list them as a comma-separated string (by default, chains are the same as the input templates",default="")
    parser.add_option('--unblock',action="store_true",dest='unblock',
                      help="if you don't want to block template side chains from being the default if AAs are the same as in the query")
    parser.add_option('--renumber',action="store_true",dest='renumber',
                      help="if you want output pdbs to be renumbered from 1 for each chain (by default, residues are given the template(s) numbering)")
    parser.add_option('--parallelise',action="store_true",dest='parallelise',
                      help="if you want to use job arrays to parallelise model generation")
    parser.add_option('--realign',action="store_true",dest='realign',
                      help="if you want to realign template sequences to the MSAs")
    parser.add_option('--verbose',action="store_true",dest='verbose',
                      help="verbose mode")
    parser.add_option('--output_homolog_list',action="store",dest='output_homologs',type='string',default="list_homologs.txt",
                      help="file with output homolog pdb names")

    if len(sys.argv) == 1:
        print(USAGE)
        print("type -h or --help for more help information")
        sys.exit(1)
    (options, args) = parser.parse_args(sys.argv)
    if len(args) > 1: # sys.argv[0] is the name of this program
        print("Leftover arguments:" + str(args[1:]))

    if options.new_chains.strip():
        options.new_chains = options.new_chains.split(',')
    else:
        options.new_chains = []

    obj = runOSCAR(output=os.path.abspath(options.output),new_chains=options.new_chains,unblock=options.unblock,renumber=options.renumber,realign=options.realign,verbose=options.verbose,parallelise=options.parallelise)
    obj.load_inputs(in_alignment=options.alignment.split(','),in_template=options.template.split(','))
    obj.run(output_homologs=os.path.abspath(options.output_homologs))

if __name__ == "__main__":
    main()



