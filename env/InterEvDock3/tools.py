#!/usr/bin/env python
'''
Created on 18 avr. 2014

@author: Jinchao Yu
'''
import re
import sys
import os
import shutil
import warnings
import math
import tempfile
import cluster.cluster as cluster

# for python version compatibility
try:
    import commands
except:
    import subprocess as commands

AA3 = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR","5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG", "HSE", "HSD","HSP"]
AA3new = ['PAQ', 'AGM', 'PR3', 'DOH', 'CCS', 'GSC', 'GHG', 'OAS', 'MIS', 'SIN', 'TPL', 'SAC', '4HT', 'FGP', 'HSO', 'LYZ', 'FGL', 'PRS', 'DCY', 'LYM', 'GPL', 'PYX', 'PCC', 'EHP', 'CHG', 'TPO', 'DAS', 'AYA', 'TYN', 'SVA', 'SCY', 'BNN', '5HP', 'HAR', 'IAS', 'SNC', 'AHB', 'PTR', 'PHI', 'NPH', 'PHL', 'SNN', 'A66', 'TYB', 'PHD', 'MAA', 'APN', 'TYY', 'TYT', 'TIH', 'TRG', 'CXM', 'DIV', 'TYS', 'DTH', 'MLE', 'CME', 'SHR', 'OCY', 'DTY', '2AS', 'AEI', 'DTR', 'OCS', 'CMT', 'BET', 'NLP', 'LLY', 'SCH', 'CEA', 'LLP', 'TRF', 'HMR', 'TYI', 'TRO', 'NLE', 'BMT', 'BUC', 'PEC', 'BUG', 'SCS', 'NLN', 'MHO', 'CSO', 'FTR', 'DLE', 'TRN', 'CSE', 'CSD', 'OMT', 'CSA', 'DSP', 'CSB', 'DSN', 'SHC', 'CSX', 'YCM', 'CSZ', 'TRQ', 'CSW', 'EFC', 'CSP', 'CSS', 'CSR', 'CZZ', 'MSO', 'BTR', 'HLU', 'MGN', 'HTI', 'TYQ', '4IN', 'M3L', 'C5C', 'HTR', 'MPQ', 'KCX', 'GLH', 'DIL', 'ACA', 'NEM', '5CS', 'LYX', 'DVA', 'ACL', 'GLX', 'MLZ', 'GLZ', 'SME', 'SMC', 'DLY', 'NEP', 'BCS', 'ASQ', 'SET', 'SEP', 'ASX', 'DGN', 'DGL', 'MHS', 'SEG', 'ASB', 'ASA', 'SEC', 'SEB', 'ASK', 'GGL', 'ASI', 'SEL', 'CGU', 'C6C', 'ASL', 'LTR', 'CLD', 'CLE', 'GMA', '1LU', 'CLB', 'MVA', 'S1H', 'DNP', 'SAR', 'FME', 'ALO', 'ALM', 'LEF', 'MEN', 'TPQ', 'NMC', 'SBD', 'ALY', 'MME', 'GL3', 'ALS', 'SBL', '2MR', 'CAY', '3AH', 'DPR', 'CAS', 'NC1', 'HYP', 'FLA', 'LCX', 'MSE', 'IYR', 'DPN', 'BAL', 'CAF', 'MSA', 'AIB', 'HIP', 'CYQ', 'PCA', 'DAL', 'BFD', 'DAH', 'HIC', 'CYG', 'DAR', 'CYD', 'IIL', 'CYM', 'CYL', 'CY3', 'CY1', 'HAC', '143', 'DHI', 'CY4', 'YOF', 'HPQ', 'SOC', 'DHA', '2LU', 'MLY', 'TRW', 'STY', 'MCL', 'BHD', 'NRQ', 'ARM', 'PRR', 'ARO', "5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG", "HSE","HSD","HSP"]

def is_atom_line(line):
    if line[0:4] == "ATOM":
        return True
    elif line[0:6] == "HETATM":
        return True
    else:
        return False

def get_chains_and_exclude_chains_only_solvent(pdbfile):
    """
    Returns the list of chains in the pdbfile excluding chains containing only solvent atoms
    i.e. "HOH","H2O","WAT" or "DOD" in the residue type column
    """
    SOLV = set(["HOH","H2O","WAT","DOD"])
    d_ch = {}
    chnorder = []

    f=open(pdbfile)
    for line in f:
        if line.startswith("ATOM") or line.startswith("HETATM"):
            atype = line[17:20].strip()
            chain = line[21]
            if chain not in d_ch:
                d_ch[chain]=set()
                chnorder.append(chain)
            d_ch[chain].add(atype)
    f.close()

    chnlst = []
    for chain in chnorder:
        if len(d_ch[chain]-SOLV) > 0: # if there's at least 1 atom in there that's not solvent, keep chain
            chnlst.append(chain)
    
    return chnlst

def change_chainID(input_pdb, chain_correspondance, out_pdb=None):
    """chain_correspondance (string): "AB_:C_D", which means A->C, B->' ', ' '->D
    if out_pdb is None, no file will created. However, out_pdb content will always be returned.
    Return out_pdb content
    """
    lst_chains = re.split(':',chain_correspondance)
    if len(lst_chains[0]) != len(lst_chains[1]):
        raise Exception("Error: chain_correspondance is not correct")
    dict_chain_correspondance = {}
    for i in range(len(lst_chains[0])):
        old_ID = lst_chains[0][i]
        new_ID = lst_chains[1][i]
        if old_ID == '_':
            old_ID = ' '
        if new_ID == '_':
            new_ID = ' '
        dict_chain_correspondance[old_ID] = new_ID
    FHi = open(input_pdb, 'r')
    content = ''
    for line in FHi:
        res_match_ATOM = re.match("ATOM  ",line)
        res_match_HETATM = re.match("HETATM",line)
        res_match_ANISOU = re.match("ANISOU",line)
        res_match_SIGUIJ = re.match("SIGUIJ",line)
        if res_match_ATOM or res_match_HETATM or res_match_ANISOU or res_match_SIGUIJ:
            if line[21] in dict_chain_correspondance.keys():
                content += "%s%s%s"%(line[:21],dict_chain_correspondance[line[21]],line[22:])
        else:
            res_match_TER = re.match("TER",line)
            if res_match_TER:
                content += "TER\n"
            else:
                content += line
    FHi.close()
    if out_pdb:
        FHo = open(out_pdb, 'w')
        FHo.write(content)
        FHo.close()
    return content

def copy_chainID2segID(input_pdb, out_pdb=None):
    """
    Cpies the current chain id in the segid column
    if out_pdb is None, no file will created. However, out_pdb content will always be returned.
    Return out_pdb content
    """
    with open(input_pdb, 'r') as FHi:
        content = ''
        for line in FHi:
            res_match_ATOM = re.match("ATOM  ",line)
            res_match_HETATM = re.match("HETATM",line)
            res_match_ANISOU = re.match("ANISOU",line)
            res_match_SIGUIJ = re.match("SIGUIJ",line)
            if res_match_ATOM or res_match_HETATM or res_match_ANISOU or res_match_SIGUIJ:
                chain_id = line[21]
                if len(line) > 73:
                    content += "%s%s%s" % (line[:72], chain_id, line[73:])
                else:
                    padding = " "*(80 - len(line))
                    newline = line[:-1] + padding + "\n"
                    content += "%s%s%s" % (newline[:72], chain_id, newline[73:])
            else:
                res_match_TER = re.match("TER",line)
                if res_match_TER:
                    content += "TER\n"
                else:
                    content += line
        FHi.close()
        if out_pdb:
            FHo = open(out_pdb, 'w')
            FHo.write(content)
            FHo.close()
        return content


def get_AA_from_pdb_line(line):
    d3to1 = {"ALA": "A", "CYS": "C", "ASP": "D", "GLU": "E", "PHE": "F", \
             "GLY": "G", "HIS": "H", "ILE": "I", "LYS": "K", "LEU": "L", \
             "MET": "M", "ASN": "N", "PRO": "P", "GLN": "Q", "ARG": "R", \
             "SER": "S", "THR": "T", "VAL": "V", "TRP": "W", "TYR": "Y"}

    try:
        AA3 = line[17:20]
        AA1 = d3to1[AA3]
        return AA1
    except:
        return None


def get_segi_from_pdb_line(line):
    try:
        segi = line[72]
        return segi
    except:
        return None


def get_chain_from_pdb_line(line):
    try:
        term = line[21]
        return term
    except:
        return None
    
def get_resid_from_pdb_line(line):
    term = line[23-1:23-1+4]
    term = re.sub(" ", "",term)
    res = re.match("^([-]*\d+)$",term)
    if res:
        return int(res.group(1))
    else:
        return None
    
def change_columns_from_pdb_line(line, column_start, string):
    lst_line = list(line)
    index = column_start-1
    if lst_line[-1] == '\n':
        lst_line.pop()
        n_spaces =  len(string) - (len(lst_line) - index)
        if  n_spaces > 0:
            lst_line.append(' '*n_spaces)
        lst_line[index:(index+len(string))] = list(string)
        lst_line.append('\n')
    else:
        n_spaces =  len(string) - (len(lst_line) - index)
        if  n_spaces > 0:
            lst_line.append(' '*n_spaces)
        lst_line[index:(index+len(string))] = list(string)
    return "".join(lst_line)
    
def get_Zdock_decoy_number(decoy_name):
    """Get decoy number from file name like (complex.123.pdb),
    useful for sorting the zdock decoys. Can also read xxx123.pdb format
    """
    res_search = re.search("\D(\d+)\.pdb",decoy_name)
    if res_search:
        return int(res_search.group(1))
    else:
        print( "Warning: unknown zdock decoy name " + decoy_name)
        return -1
    
def get_pdb_file_list(dir_abspath, return_path=False):
    """Get the list of files ending at ".pdb" in the given directory.
    if return_path is False (def), return the list of file name;
    if return_path is True, return the list of paths;
    """
    pdb_file_list = []
    dir_abspath = os.path.abspath(dir_abspath)
    for file_name in os.listdir(dir_abspath):
        res_search = re.search("\.pdb$",file_name)
        if res_search:
            if return_path:
                pdb_file_list.append(os.path.join(dir_abspath,file_name))
            else:
                pdb_file_list.append(file_name)
    return pdb_file_list

def extract_pdb_chain(pdb,chain,pdbout):
    """
    Extract a chain from a pdb and save it to pdbout
    """
    fin = open(pdb).readlines()
    file_lines = []
    fout = open(pdbout,"w")
    for l in fin:
        if is_atom_line(l):
            if l[21] == chain:
                fout.write(l)
    fout.close()
    return

def join_pdb_chain(list_pdb,pdbout):
    """
    Concatenate pdbs in a list and save it to pdbout
    pdbs are separated by a TER and last pdb is followed by END
    no header is kept
    """
    fout = open(pdbout,"w")
    for ii,pdb in enumerate(list_pdb):
        fin = open(pdb).readlines()
        atom_lines = []
        for l in fin:
            if is_atom_line(l):
                atom_lines.append(l)
        lines = "".join(atom_lines)
        fout.write(lines)
        if ii == (len(list_pdb) - 1):
            fout.write("END\n")
        else:
            fout.write("TER\n")
    fout.close()
    return
    
def comment_line(line):
    """return true if this line begins with #
    """
    res = re.match("#", line)
    if res:
        return True
    else:
        return False
    
def execute(cmd, mode=2, use_container=True, docker_img = 'interevdock'):
    """Execute a command
    mode:
    1.then return stat
    2.then print output and return stat
    3.print output if the returned status is not zero, and return stat
    """
    if use_container:
        #~ cmd = 'drun %s "%s"' % (docker_img, cmd)
        args = cmd.split()
        executable = args.pop(0)
        cluster.runTasks(executable, args, environment_module = docker_img)
        stat, output = 0, "Check container logs"
        
    else:
        stat, output = commands.getstatusoutput(cmd)

    if mode == 3:
        if stat != 0:
            print("status: " + str(stat))
            print("In the command:" + cmd)
            print("output:" + output)
    elif mode == 2:
        print("execute:" + cmd)
        print("output:" + output)
    return stat

def file_2_list(list_file):
    """Read a file containing a list, whose each term is located in a line
    Any line beginning with # and any line containing only spaces or \t will be ignored 
    """
    my_list = []
    try:
        FH = open(list_file)
    except:
        print("Input file not found.")
        return my_list
    for line in FH:
        res_match = re.match("#",line)
        if res_match:
            continue
        res_match = re.match("^[\s\t]*$",line)
        if res_match:
            continue
        my_list.append(line.rstrip()) 
    FH.close()
    return my_list

def file_column_2_list(list_file, column_number, delimiter="[\t ]+"):
    """Read a file containing a table and transfer one column to a list
    Any line beginning with # and any line containing only spaces and/or \t will be ignored 
    """
    my_list = []
    try:
        FH = open(list_file)
    except:
        print("Input file not found.")
        return my_list
    for line in FH:
        res_match = re.match("#",line)
        if res_match:
            continue
        res_match = re.match("^[\s\t]*$",line)
        if res_match:
            continue
        try:
            my_term = re.split(delimiter,line.rstrip())[column_number-1]
        except:
            print("Warning: this line is abnormal in the file %s"%list_file)
        my_list.append(my_term) 
    FH.close()
    return my_list

def div_list(lst, divisor):
    """Divide a list into "divisor" parts. ex [1,2,3,4,5] divided by 3: [1,2], [3,4], [5] 
    Useful in a mpi program, when a complete list of tasks should be divided for each CPU,
    use div_list(lst, size)[rank]
    """
    size = int(math.ceil(float(len(lst))/divisor)) 
    my_list = [lst[i:i+size] for i in range(0, len(lst), size)]
    diff = divisor - len(my_list)# probably void sub-list should be added
    for i in range(diff):
        my_list.append([])
    return my_list

def mzdock_decoys_add_pdb_suffix(decoys_dir):
    """the decoys that mzdock generates are named complex.xxx (xxx is the number)
    This function allows adding ".pdb" for each decoy (complex.xxx -> complex.xxx.pdb)
    in order to accord with the zdock naming convention
    """
    if not os.path.exists(decoys_dir):
        print("This folder does not exsits.")
    try:
        for file_name in os.listdir(decoys_dir):
            res = re.match("complex\.\d+$",file_name)
            if res:
                file_path = os.path.join(decoys_dir,file_name)
                new_file_path = file_path + ".pdb"
                os.rename(file_path, new_file_path)
    except:
        print("Error in mzdock_decoys_add_pdb_suffix")
        
def chain_2_segid(PDB_file, output_file):
    """Description: copy chainID to segID (if segID is void, segId will be "VOID")
    Any line which is shorter than standard line length (80) will be filled by spaces at the end
    Arguments:
    1. PDB_file; input PDB file path (absolute path recommended)
    2. output_file: resulting file path (absolute path recommended)
    """
    STANDARD_LINE_LENGTH = 80
    try:
        FH = open(PDB_file,"r")
    except:
        print("can not open the PDB file: " + PDB_file)
        return 1
    FH_out = open(output_file,"w")
    for line in FH:
        line = line.rstrip()
        res_match_atom = re.match("ATOM  ",line)
        res_match_hetatm = re.match("HETATM",line)
        if res_match_atom or res_match_hetatm:
            # Add spaces if a line is shorter than standard line length
            mssing_space = STANDARD_LINE_LENGTH - len(line)
            if mssing_space < 0:
                mssing_space = 0
            new_line = list(line + ' '*mssing_space)
            
            # chainID is 22 and segId is from 73 to 76
            chainID = new_line[21]
            if chainID == " ":
                new_line[72:76] = list("VOID")
            else:
                new_line[72] = chainID
            FH_out.write(''.join(new_line)+"\n")
        else:
            FH_out.write(line+"\n")
    FH.close()
    FH_out.close()
    return 0

def get_chain_list(PDB_file):
    """Description: get a chain list from a PDB file by reading the 21st letter in ATOM lines
    Arguments:
    1. PDB_file; input PDB file path (absolute path recommended)
    """
    try:
        FH = open(PDB_file,"r")
    except:
        print("can not open the PDB file: " + PDB_file)
        return 1
    chain_list = []
    for line in FH:
        line = line.rstrip()
        res_match_atom = re.match("ATOM  ",line)
        if res_match_atom:
            try:
                if line[21] not in chain_list:
                    chain_list.append(line[21])
            except:
                print("Abnormal PDB format detected")
                print(line)
    FH.close()
    return chain_list

def correct_TER_END(PDB_file, output_file):
    """Description: correct TER and END terms in a PDB file 
    Arguments:
    1. PDB_file; input PDB file path (absolute path recommended)
    2. output_file: resulting file path (absolute path recommended)
    """
    try:
        FH = open(PDB_file,"r")
    except:
        print("can not open the PDB file: " + PDB_file)
        return 1
    FH_out = open(output_file,"w")
    chain = "not_loaded"
    ter_line = None
    model_begin = False
    i = 0
    while True:
        line = FH.readline()
        if len(line) == 0:
            break
        i += 1 
        res_match_atom = re.match("ATOM  ",line)
        res_match_ter = re.match("TER",line)
        res_match_end = re.match("END[\s\t\n]*$",line)
        res_match_model = re.match("MODEL",line)
        res_match_endmdl = re.match("ENDMDL",line)
        res_void_line = re.match("[\s\t\n]*$", line)
        if res_void_line:
            pass
        elif res_match_atom:
            if chain == "not_loaded":
                chain = line[21]
                FH_out.write(line)
            else:
                if chain != line[21]:
                    chain = line[21]
                    if ter_line != None:# the precedent line is TER, so changing chain is normal
                        FH_out.write(ter_line)
                        FH_out.write(line)
                        ter_line = None
                    else:# the precedent line is not TER, so TER should be added now
                        FH_out.write("TER\n")
                        FH_out.write(line)
                else:
                    if ter_line != None:
                        print("Inadequate TER is found in line " + str(i))
                        ter_line = None
                    FH_out.write(line)
        elif res_match_ter:
            ter_line = line
        elif res_match_end: # ignore the "END" line. An "End" line will be added at the last line after the loop finishes
            pass
        elif res_match_model:# new model begins
            chain = "not_loaded"
            if ter_line:
                FH_out.write(ter_line)
                ter_line = None
            else:
                FH_out.write("TER\n")
            if model_begin:# The model above does not end with ENDMDL
                FH_out.write("ENDMDL\n")
            model_begin = True
            FH_out.write(line)
        elif res_match_endmdl:# a model ends with ENDMDL
            if model_begin == False:#
                print("Error: ENDMDL found at line " + str(i) + " with no corresponding MODEL head")
            else:
                model_begin = False
                if ter_line:
                    FH_out.write(ter_line)
                    ter_line = None
                else:
                    FH_out.write("TER\n")
                FH_out.write(line)
        else:
            FH_out.write(line)

    if ter_line:
        FH_out.write(ter_line)
        ter_line = None
    else:
        FH_out.write("TER\n")
    if model_begin:
        FH_out.write("ENDMDL\n")
    FH_out.write("END\n")
        
    FH.close()
    FH_out.close()

def pdb_delete_HETATM(in_pdb, out_pdb):
    """Remove HETATM lines,
    If out_pdb and in_pdb are the same path, in_pdb will be overwritten
    """
    text = ""
    FHi = open(in_pdb, 'r')
    model_keyword_found = False
    for line in FHi:
        res_match_het = re.match("HETATM",line)
        if not res_match_het:
            text += line
    FHi.close()
    
    FHo = open(out_pdb, 'w')
    FHo.write(text)
    FHo.close()
    
def clean_subunit_pdb_for_docking(in_pdb, out_pdb,keepHOH = True, force_hetatm = False):
    """Keep only ATOM and HETATM lines, only the part before END keyword, and only the first model if
    there are more than one.

    If out_pdb and in_pdb are the same path, in_pdb will be overwritten
    
    The force_hetatm was added for InterEvDock2 Oligomeric options to allow DNA to be included 
        use a set() specifying the chains to be switched to HETATM such as force_hetatm = set(["I","J"])
    """
    text = ""
    FHi = open(in_pdb, 'r')
    model_keyword_found = False
    for line in FHi:
        res_match_atom = re.match("ATOM  ",line)
        res_match_het = re.match("HETATM",line)
        res_match_end = re.match("END[\s\t\n]*$",line)
        res_match_model = re.match("MODEL",line)
        res_match_endmdl = re.match("ENDMDL",line)

        if res_match_model:
            model_keyword_found = True
        elif res_match_endmdl:
            if model_keyword_found:
                break
        elif res_match_end:
            break
        elif res_match_atom:
            chain = line[21]
            resname = line[17:20].strip()
            if not keepHOH:
                res_water = re.search("HOH|H2O|WAT|DOD",line)
                if res_water:
                    continue
            if resname not in AA3 and resname not in AA3new:
                text += "HETATM " + line[7:] # we do this to be sure that HETATM10000 cannot be generated for large files
                continue
            if force_hetatm:
                if chain in force_hetatm:
                    text += "HETATM " + line[7:] # we do this to be sure that HETATM10000 cannot be generated for large files
                else:
                    text += line
            else:
                text += line
        elif res_match_het:
            if not keepHOH:
                res_water = re.search("HOH|H2O|WAT|DOD",line)
                if not res_water:
                    text += "HETATM " + line[7:]#line
            else:
                text += "HETATM " + line[7:] #line -> we do this to be sure that HETATM10000 cannot be generated for large files
    FHi.close()
    
    FHo = open(out_pdb, 'w')
    FHo.write(text)
    FHo.close()
    
def PDB_2_fasta(PDB_file,out_file,chain=None,localradius=None):
    """Description: Extract a chain in a PDB file and output a fasta file
    Biopython is used if it exists, otherwise the local script getFastaFromCoords.pl is used
    Arguments:
    1. PDB_file; input PDB file path (absolute path recommended)
    2. out_file: resulting fasta file path (absolute path recommended)
    3. chain (optional): indicate the chain ID (a underscore means a chain without ID); 
                         by default, we consider there is only one chain in the PDB file and this chain
                         will be extracted automatically; if there are more than one chains and this argument
                         is not set, program terminate with an error code returned
    """
    if chain == None or len(chain)>1:
        try:
            chain_list = get_chain_list(PDB_file)
        except:
            return 1
        if len(chain_list) > 1:
            print("There is multiple chains in this PDB file.")
            return 1
        chain_ID = chain_list[0]
    else:
        chain_ID = chain
    name = os.path.basename(PDB_file)
    try:
        from Bio.PDB import PDBParser
        from Bio.PDB import PPBuilder
        from Bio import SeqIO
        from Bio.SeqRecord import SeqRecord
        from Bio.PDB.PDBExceptions import PDBConstructionWarning
    except ImportError:
        #print "Biopython not found. A local script getFastaFromCoords.pl will be used."
        GET_FASTA = "/service/env/InterEvDock/getFastaFromCoords.pl"
        if not os.path.exists(GET_FASTA):
            print("getFastaFromCoords.pl not found")
            return 1
        else:
            if chain_ID == " ":
                chain_ID = "_"
            cmd = GET_FASTA + " -pdbfile " + PDB_file + " -chain " + chain_ID + " > " + out_file
            os.system(cmd)
            return 0
    # if Biopython has been found
    import warnings
    warnings.filterwarnings("ignore", category=PDBConstructionWarning)
    if chain_ID == "_":
        chain_ID = " "
    pp = PDBParser()
    model = pp.get_structure("my_struct", PDB_file)[0] # the first model
    chain = model[chain_ID]
    if localradius:
        ppb = PPBuilder(radius=localradius)
    else:
        ppb = PPBuilder()
    my_seq_record = SeqRecord(id=name,seq="")
    for pp in ppb.build_peptides(chain):# usually there is only one poly-peptide 
        my_seq_record.seq += pp.get_sequence()
    FH_out= open(out_file,"w")
    SeqIO.write(my_seq_record, FH_out, "fasta")
    FH_out.close()
    return 0
    
def divide_fasta(in_fasta, out_fasta, n):
    """Description: divide a single fasta sequence by n and choose the first part
    This is used for obtaining a real fasta sequence from 
    a merged fasta sequence composed of n identical parts.
    Arguments:
    in_fasta: input fasta file, absolute path recommended
    out_fasta: output fasta file, absolute path recommended
    n: number of exactly the same merged subunits in one chain,
       if this number n is greater than 1 (1 by default),
       the fasta sequence will divide into n identical parts and only the first part will be kept       
    """
    FHi = open(in_fasta, "r")
    header = ""
    seq = ""
    for line in FHi:
        res = re.match("(^#)|(^[ \t\n]*$)",line)
        if not res:
            line = line.rstrip()
            res_header = re.match("^>",line)
            if res_header:
                header = line
            else:
                seq += line
    FHi.close()
    seq_length = int(len(seq)/n)
    seq = seq[:seq_length]
    
    size = 60 # In output, each line contains at most 60 residues
    seq_list = [seq[i:i+size] for i in range(0, len(seq), size)]
    seq_text = "\n".join(seq_list)+"\n"    
    FHo = open(out_fasta,"w")
    FHo.write(header+"\n")
    FHo.write(seq_text)
    FHo.close()

def get_chain_res_list_and_resid_list(target_pdb, given_chain=None, ignore_hetatm=True, use_biopython=True):
    """ if given_chain is not given, the first chain will be reported. The same index in 
    the two returned lists corresponds to the same residue. 
    ignore_hetatm: True by default, means that chains with only hetatm are ignored
    
    Return: (res_list, resid_list) 
    PS: resid is in string format
    """
    try:
        from Bio.PDB import PDBParser
        from Bio.Data.SCOPData import protein_letters_3to1
        from Bio.PDB.PDBExceptions import PDBConstructionWarning
        warnings.filterwarnings("ignore", category=PDBConstructionWarning)
        USE_BIOPYTHON = True
    except:
        USE_BIOPYTHON = False
    PRINT_WARNING = 0

    if not use_biopython:
        USE_BIOPYTHON = False

    if USE_BIOPYTHON:
        print("Using BioPython for the recovery of residues and index ids.")

    if ignore_hetatm:
        chain_list = get_chain_list(target_pdb)
    else:
        chain_list = get_chains_and_exclude_chains_only_solvent(target_pdb)
        
    # no chain (no residue) in the pdb file
    if len(chain_list) == 0:
        return ([],[])
    if given_chain is None:
        given_chain = chain_list[0]
    if given_chain == '_':
        given_chain = ' '
    if given_chain not in chain_list:
        print("Error: no chain found as %s"%given_chain)
    lst_res = []
    lst_resid = []
    
    if USE_BIOPYTHON:
        pdb_parser = PDBParser()
        struct = pdb_parser.get_structure('my_struct',target_pdb)
        set_warning = set()
        for res in struct[0][given_chain]:
    #         print res, res.id
            if res.id[0] == ' ' and res.resname in protein_letters_3to1.keys():
                lst_res.append(protein_letters_3to1[res.resname])
                lst_resid.append(str(res.id[1]))
            else:
                if not given_chain in set_warning:
                    print("Warning: Some residues are not recognized as ATOM in chain %s"%(given_chain))
                    set_warning.add(given_chain)
                lst_res.append('X')
                lst_resid.append(str(res.id[1]))
    else:
        previous_res = -100000
        fpdb = open(target_pdb).readlines()
        for l in fpdb:
            if len(l)>6 and l[:6] == "ENDMDL":
                # We keep only the first model
                break
            if not is_atom_line(l):
                continue
            if get_chain_from_pdb_line(l) != given_chain:
                continue
            resi = get_resid_from_pdb_line(l)
            if resi != previous_res:
                resn = get_AA_from_pdb_line(l)
                if resn:
                    lst_res.append(resn)
                else:
                    lst_res.append("X")
                    if PRINT_WARNING == 0:
                        print("Warning: Some residues are not recognized as ATOM in chain %s"%(given_chain))
                        PRINT_WARNING += 1
                lst_resid.append(str(resi))
                previous_res = resi

    return(lst_res, lst_resid)


def renumber_pdb(inpdb, chains_by_comma_in_string, begins_by_comma_in_string, outpdb):
    """renumber each chain in input pdb file from each given starting resnum; write into outpdb file
    outpdb can be the same as inpdb so that inpdb will be overwritten.
    chains_by_comma_in_string: A,B,_
    begins_by_comma_in_string: 1,1,1
    """
    def remap_resnum_for_line(line, mapping ):
        if line[0:3] == "TER": return "TER\n" 
        if line[0:4] != "ATOM" and line[0:6]!= "HETATM": return line
        chain = line[21];
        if chain not in mapping: return line
        resstring = line[22:27]
        resnum, lastresstring = mapping[chain]
        if lastresstring == "" or resstring != lastresstring :
            if lastresstring != "" : resnum += 1
            mapping[chain] = (resnum, resstring )
        newresstring = str(resnum) + " "
        if len(newresstring) == 2: newresstring = "   " + newresstring
        elif len(newresstring) == 3: newresstring = "  " + newresstring
        elif len(newresstring) == 4: newresstring = " " + newresstring
        return line[0:22] + newresstring + line[27:]
    chain_lst = re.split(',',re.sub('_','',chains_by_comma_in_string))
    begins_lst = re.split(',',begins_by_comma_in_string)
    if len(chain_lst) != len(begins_lst):
        print("Error: the length of the list of chains differs with that of resnum start list")
        return
    mapping = {}
    for i in range(len(chain_lst)):
        mapping[chain_lst[i]] = (int(begins_lst[i]), '')
    content = ""
    FHi = open(inpdb, 'r')
    
    for line in FHi:
        content += remap_resnum_for_line(line, mapping)
    FHi.close()
    
    FHo = open(outpdb, 'w')
    FHo.write(content)
    FHo.close()

def renumber_pdb_all_chains_from_one(inpdb, outpdb, ignore_hetatm=True):
    """renumber each chain in input pdb file from one; write into outpdb file.
    outpdb can be the same as inpdb so that inpdb will be overwritten.
    """
    if ignore_hetatm:
        chain_lst = get_chain_list(inpdb)
    else:
        chain_lst = get_chains_and_exclude_chains_only_solvent(inpdb)
    ch_string = ''
    resnum_start_string = ''
    for ch in chain_lst:
        ch_string += ch+','
        resnum_start_string += '1,'
    renumber_pdb(inpdb, ch_string[:-1], resnum_start_string[:-1], outpdb)

def renumber_pdb_from_one_to_last_res(inpdb, outpdb, ignore_hetatm=True):
    """renumber each chain in input pdb file from one; write into outpdb file.
    outpdb can be the same as inpdb so that inpdb will be overwritten.
    """
    if ignore_hetatm:
        chain_lst = get_chain_list(inpdb)
    else:
        chain_lst = get_chains_and_exclude_chains_only_solvent(inpdb)
    
    ch_string = ''
    resnum_start_string = ''
    start_resnum = 1
    for ch in chain_lst:
        ch_string += ch+','
        resnum_start_string += '%d,'%start_resnum
        ch_length = len(get_chain_res_list_and_resid_list(inpdb, ch, ignore_hetatm)[0])
        start_resnum = start_resnum + ch_length
    renumber_pdb(inpdb, ch_string[:-1], resnum_start_string[:-1], outpdb)

def change_chainID(input_pdb, chain_correspondance, out_pdb=None):
    """chain_correspondance (string): "AB_:C_D", which means A->C, B->' ', ' '->D
    if a pdb has chains AB and chain_corresp is "A:C", chain B does not change
    if out_pdb is None, no file will created. out_pdb can be input_pdb so that input pdb is overwritten.
    out_pdb content will always be returned.
    """
    lst_chains = re.split(':',chain_correspondance)
    if len(lst_chains[0]) != len(lst_chains[1]):
        raise Exception("Error: chain_correspondance is not correct")
    dict_chain_correspondance = {}
    for i in range(len(lst_chains[0])):
        old_ID = lst_chains[0][i]
        new_ID = lst_chains[1][i]
        if old_ID == '_':
            old_ID = ' '
        if new_ID == '_':
            new_ID = ' '
        dict_chain_correspondance[old_ID] = new_ID
    FHi = open(input_pdb, 'r')
    content = ''
    for line in FHi:
        res_match_ATOM = re.match("ATOM  ",line)
        res_match_HETATM = re.match("HETATM",line)
        res_match_ANISOU = re.match("ANISOU",line)
        res_match_SIGUIJ = re.match("SIGUIJ",line)
        if res_match_ATOM or res_match_HETATM or res_match_ANISOU or res_match_SIGUIJ:
            if line[21] in dict_chain_correspondance.keys():
                content += "%s%s%s"%(line[:21],dict_chain_correspondance[line[21]],line[22:])
            else:
                content += line
        else:
            res_match_TER = re.match("TER",line)
            if res_match_TER:
                content += "TER\n"
            else:
                content += line
    FHi.close()
    if out_pdb:
        FHo = open(out_pdb, 'w')
        FHo.write(content)
        FHo.close()
    return content
        
def extract_best_clusters(dir_docking):
    """read *_best_clusters.txt and copy the 1st pdb of each cluster into folders *_models
    Argument:
    1. dir_docking: docking directory which contains decoys folder and *_best_clusters.txt results
    """
    for file_name in os.listdir(dir_docking):
        res = re.search("(\w+)_best_clusters.txt",file_name)
        if res:
            print("Reading %s"%os.path.join(dir_docking,file_name))
            print("Creating %s"%os.path.join(dir_docking,res.group(1)+"_models"))
            new_models_dir = os.path.join(dir_docking,res.group(1)+"_models")
            if os.path.exists(new_models_dir):
                shutil.rmtree(new_models_dir)
            os.mkdir(new_models_dir)
            FHi = open(os.path.join(dir_docking,file_name),"r")
            while True:
                line = FHi.readline()
                if len(line) == 0:
                    break
                res_comment_cluster = re.match("#(\d+) --> cluster(\d+) ([\d+\-\.]+):",line)
                if res_comment_cluster:
                    rank = res_comment_cluster.group(1)
                    cluster = res_comment_cluster.group(2)
                    score = res_comment_cluster.group(3)
                    old_pdb_name = None
                    complexindex = None
                    nextline = FHi.readline()
                    res_pdb_name = re.match("complex\.(\d+)\.pdb",nextline)
                    if res_pdb_name:
                        complexindex = res_pdb_name.group(1)
                        old_pdb_name = "complex."+ complexindex + ".pdb"
                    else:
                        res_pdb_name = re.match("(.+)\.pdb",nextline)
                        if res_pdb_name:
                            complexindex = res_pdb_name.group(1)
                            old_pdb_name = complexindex + ".pdb"
                        else:
                            print("Error in this line: %s"%nextline)
                    if complexindex != None:
                        new_pdb_name = "%s_c.%s_cl%s_%s.pdb"%(rank,complexindex,cluster,score)
                        shutil.copy(os.path.join(dir_docking,"decoys",old_pdb_name), 
                                    os.path.join(new_models_dir,new_pdb_name))



################################################################
# tempfile

LOCAL_TMP_DIR_PATH = os.getcwd()
if not os.path.exists(LOCAL_TMP_DIR_PATH):
    try:
        os.mkdir(LOCAL_TMP_DIR_PATH)
    except:
        print("The program needs to create a tmp dir at $HOME/tmp/.")
        print("Creation failed.")
        raise IOError
if not os.access(LOCAL_TMP_DIR_PATH, os.W_OK):
    print("No writing right in %s"% LOCAL_TMP_DIR_PATH)
    raise IOError  

def make_tempdir(suffix_str='',prefix_str='',parent_dir=LOCAL_TMP_DIR_PATH):
    """ create a tempdir in parent_dir (def = $HOME/tmp). Return the created tempdir absolute path.
    Arguments:
    1. suffix_str: string for tempdir name's suffix part (def = '')
    2. prefix_str: string for tempdir name's prefix part (def = '')
    3. parent_dir: parent directory path (def = $HOME/tmp)
    Return:
    the created tempdir absolute path
    """
    tmpfile = tempfile.mkdtemp(suffix=suffix_str,prefix=prefix_str,dir=parent_dir)
    os.chmod(tmpfile, 0o755)
    return os.path.abspath(tmpfile)

def make_tempfile(suffix_str='',prefix_str='',parent_dir=LOCAL_TMP_DIR_PATH):
    """ create a tempfile in parent_dir (def = $HOME/tmp). Return the created tempfile absolute path.
    File handle problem has been solved.
    Arguments:
    1. suffix_str: string for tempfile name's suffix part (def = '')
    2. prefix_str: string for tempfile name's prefix part (def = '')
    3. parent_dir: parent directory path (def = $HOME/tmp)
    Return:
    the created tempdir absolute absolute path
    """
    fd, tmpfile = tempfile.mkstemp(suffix=suffix_str,prefix=prefix_str,dir=parent_dir)
    os.close(fd)
    os.chmod(tmpfile, 0o755)
    return os.path.abspath(tmpfile)


################################################################
# FASTA TOOLS

def write_fasta(header_list,seq_list,outfile,ignore_gaps=True):
    """
    Write MSA to fasta file

    Input:
    header_list: [str], list of sequence headers
    seq_list:    [str], list of corresponding sequences
    ignore_gaps: bool, if True, positions in the MSA that have only gaps are 
           removed before writing the file

    Output:
    outfile: str, name of the output fasta file
    """

    def remove_extra_gaps(seq_list):
        """
        Remove columns in the MSA that only have gaps

        Input:
        seq_list: [str], list of sequences

        Input:
        new_seq_list: [str], list of sequences without useless gaps
        """
        num_seq = len(seq_list)
        new_seq_list = ["" for i in range(num_seq)]
        for i in range(len(seq_list[0])):
            if set([seq_list[j][i] for j in range(num_seq)])==set(["-"]):
                continue
            for j in range(num_seq):
                new_seq_list[j]+=seq_list[j][i]
        return new_seq_list

    if len(header_list) != len(seq_list):
        print("Error: cannot write fasta file. Header number: {:d}, "+\
            "seq number: {:d}".format(len(header_list),len(seq_list)))
        sys.exit(1)
    if ignore_gaps:
        seq_list = remove_extra_gaps(seq_list)
    FHo = open(outfile, 'w')
    for i in range(len(header_list)):
        FHo.write('>{}\n'.format(header_list[i]))
        FHo.write("{}\n".format(seq_list[i]))
    FHo.close()


def read_fasta(fasta_file):
    """
    Read a fasta file

    Input:
    fasta_file: str, name of the fasta file

    Output:
    (headers,seqs): ([str],[str]), a tuple with a list of the sequence headers 
            and a list of corresponding sequences
    """
    headers = []
    seqs = []
    FHi = open(fasta_file)
    index_seq = -1
    for line in FHi:
        line = line.rstrip('\n')
        res = re.match(">\s*(.*)",line)
        if res:
            headers.append(res.group(1))
            seqs.append("")
        else:
            try:
                seqs[-1] += re.sub("[\t ]","",line)
            except:
                print("Fasta format error in tools read_fasta.")
    FHi.close()
    return (headers,seqs)


def combine_MSAs(lst_msas,fastaout,add_gapped_lg=False,ignore_gaps=False):
    """
    Paste 2 MSAs files together
    
    Input:
    lst_msas:  [([str],[str])], list of tuples with a list of the sequence headers 
               and a list of corresponding sequences for the first coMSA
               or 
               [str], list of fasta file names
    fastaout: str, name of the output input fasta file

    Output:
    None
    """
    list_headers = []
    list_seqs    = []
    set_lengths = set()    
    for msa in lst_msas:
        if isinstance(msa,list):
          h,s = msa
        elif isinstance(msa,str):
          h,s = read_fasta(msa)
        else:
          raise Exception("Error: Could not recognise msa format in combine_MSAs")
        list_headers.append(h)
        list_seqs.append(s)
        set_lengths.add(len(h))

    if len(set_lengths) > 1:
        raise Exception("Error: cannot combine. MSA sizes differ.")
    set_lengths = list(set_lengths)

    if add_gapped_lg:
      write_fasta([",".join([list_headers[j][i]+' (gapped length %d)'%(len(list_seqs[j][i])) for j in range(len(list_headers))]) for i in range(set_lengths[0])],
      ["".join([list_seqs[j][i] for j in range(len(list_seqs))]) for i in range(set_lengths[0])],fastaout,ignore_gaps=ignore_gaps)
    else:
      write_fasta(["|||".join([list_headers[j][i] for j in range(len(list_headers))]) for i in range(set_lengths[0])],
      ["".join([list_seqs[j][i] for j in range(len(list_seqs))]) for i in range(set_lengths[0])],fastaout,ignore_gaps=ignore_gaps)


def get_fasta_from_ali(in_ali, fasta):
    """ Get fasta sequence from query-template if not query fasta is provided (AUTOPDB mode).
    """
    FHi = open(in_ali, 'r')
    header = ""
    seq = ""
    for line in FHi:
        if line[0]==">":
            if header == "":
                header = line
            else:
                break
        else:
            seq+=line.strip()
    FHi.close()
    
    if header != "" and seq != "":
        seq = seq.replace("/","")
        seq = seq.replace("-","")
        
        FHo = open(fasta, 'w')
        FHo.write(header)
        FHo.write("%s\n"%seq)
        FHo.close()
        return True
    return False
