#!/usr/bin/env python
"""
This script is used to check if the residues given as constraints for
interface selection are on the surface of the protein structures given.
It also generates the frodock-friendly constraints file and renumbers 
residues if the numbering given follows the pdb numbering in stead of
being sequential.

HELP
Type 'python checkResOnSurface.py' for usage info
Refer to paper and/or help file for more details
"""

### LOADING LIBRARIES
try:
    # Standard libraries
    import sys
    import os
    import re
    import pkg_resources

    from collections import defaultdict

    # for python version compatibility
    try:
        import ConfigParser
    except:
        import configparser as ConfigParser
except:
    print("ERROR LIBRARY IMPORTATION")
    print("CHECK YOUR PATHS AND THE PROGRAMS REQUIRED")
    sys.exit(1)

config = ConfigParser.ConfigParser()
config.read(pkg_resources.resource_filename("InterEvDock3","config/config.ini"))

NACCESS = config.get("programs","naccess")
DOCKER_NACCESS = config.get("environments","naccess")

import PyPDB.PyPDB as PDB

import cluster.cluster as cluster

class checkResConstraints():

    def __init__(self, pdbA="", pdbB="", constraintfile="", conversionfileA="", conversionfileB="", output='', user_output='', table_output='', renumber=False, threshold = 1.0, default_dist_single = 8.0, default_dist_pair = 11.0):
        """
        This class will take user inputed constraints and reformat them into a frodock-compatible constraint file
        Constraints that involve residues that are not in the structure or not on the protein surface are excluded
        """

        self.pdbA = pdbA                         # protein A
        self.pdbB = pdbB                         # protein B
        self.constraintfile = constraintfile     # constraints as given by the user in the InterEvDock3 server
        self.conversionfileA = conversionfileA   # translation table created by Raphael's ConvertMultimer class for partner A
        self.conversionfileB = conversionfileB   # same for partner B
        self.output = output                     # frodock-compatible txt output ready for docking
        self.user_output = user_output           # user-friendly txt output explaining what constraints were retained
        self.table_output = table_output         # output txt for InterEvDock3 results page: a table with constraints that were kept or not kept and why
        self.threshold = threshold               # accessibility threshold to define an accessible residue
        self.default_dist_single = default_dist_single  # default distance if none given by the user for single constraints
        self.default_dist_pair = default_dist_pair      # default distance if none given by the user for constraint pairs
        # a bit of a useless option I think?
        self.renumber = renumber                 # if constraints have to be renumbered according to sequence
        self.accessibilitiesA = {}               # [chain][resnum] -> (restype,accessibility) for protein A
        self.accessibilitiesB = {}               # [chain][resnum] -> (restype,accessibility) for protein B

        if not os.path.exists(self.pdbA) or not os.path.exists(self.pdbB):
            print("\nERROR: checkConstraints: Cannot read input pdbs {} and {}\n".format(self.pdbA,self.pdbB))
            raise Exception("\nERROR: checkConstraints: Cannot read input pdbs {} and {}\n".format(self.pdbA,self.pdbB))

        if self.conversionfileA and self.conversionfileB:
            print(">> checkConstraints: Oligomer docking dectected, conversion of user constraints necessary")
            trad_input2renamed, trad_renamed2input = self.read_conversion_table()
        else:
            print(">> checkConstraints: Monomer/monomer docking dectected, no conversion of user constraints necessary")
            trad_input2renamed, trad_renamed2input = {}, {}

        # read contraint file
        constraints, all_constraints, corres_constraints, list_constraints = self.get_contraints(trad_input2renamed)

        # execute naccess on both pdb files
        rsaA, asaA, logA = self.executeNaccess(self.pdbA)
        print(">> checkConstraints: naccess worked on",self.pdbA,rsaA)
        rsaB, asaB, logB = self.executeNaccess(self.pdbB)
        print(">> checkConstraints: naccess worked on",self.pdbB,rsaB)

        # check if files were generated
        if not os.path.exists(rsaA) or not os.path.exists(rsaB):
            print("\nERROR: checkConstraints: naccess didn't generate .rsa files\n")
            raise Exception("\nERROR: checkConstraints: naccess didn't generate .rsa files\n")

        # get accessibilities
        self.accessibilitiesA = self.read_naccess_rsa(rsaA)
        self.accessibilitiesB = self.read_naccess_rsa(rsaB)

        # clean up
        os.remove(rsaA) 
        os.remove(asaA) 
        os.remove(logA)
        os.remove(rsaB)
        os.remove(asaB) 
        os.remove(logB)

        new_constraints, trad_renamed2input, all_constraints = self.filter_constraints(constraints, threshold, trad_renamed2input, all_constraints, corres_constraints)

        # check if there are any constraints left
        if self.if_constraints_empty(new_constraints):
            #print("\nThere are no useable constraints - proceeding without constraints\n")

            self.output = None
            self.write_table_constraints(all_constraints, list_constraints)
            f=open(self.user_output,'w')
            f.write("No constraints were used during docking whether because they were submitted in the wrong format \n")
            f.write("or because the residues indicated aren't on the surface of the protein (i.e. a relative \n")
            f.write("accessibility surface of at least 1 A^2) or because they aren't in the protein structure \n")
            f.write("given or modelled.")
            f.close()

        else:
            if renumber:
                # residue id's given in constraints file don't match the residue sequence
                new_constraints, self.accessibilitiesA, self.accessibilitiesB = self.renumber_constraints(new_constraints)        

            # write frodock friendly constraints file
            self.write_frodock_contraints_v21(new_constraints)
            self.write_user_constraints(new_constraints, trad_renamed2input)
            self.write_table_constraints(all_constraints, list_constraints)

            print(">> checkConstraints: Constraint file created under: %s"%self.output)

    def executeNaccess(self,pdbfile):
        """
        To execute naccess on a pdb file
        INPUT 
        a pdb file
        naccess executable
        OUTPUT
        a rsa, asa and log file as returned by naccess
        """
        currwd = os.getcwd()
        folder = os.path.dirname(pdbfile)
        os.chdir(folder)
        args = [pdbfile,
                '> /dev/null']
        cluster.runTasks(NACCESS, args, environment_module=DOCKER_NACCESS, log_prefix = "naccess")
        os.chdir(currwd) 

        prefix = os.path.basename(pdbfile.split(".pdb")[0])
        return os.path.join(folder,"%s.rsa"%prefix), os.path.join(folder,"%s.asa"%prefix), os.path.join(folder,"%s.log"%prefix)


    def read_naccess_rsa(self,rsafile):
        """
        To read the output rsa files from naccess
        INPUT
        a rsa file as returned by naccess
        OUTPUT
        a dictionary of accessibilities [chain][resnum] -> (restype,accessibility) 
        for the first chain in the file
        """  
        accessibilities = defaultdict(lambda: dict()) 

        f=open(rsafile,"r")
        lines=f.readlines()
        f.close()

        chains = set() # save chains
        for line in lines:
            if line[:3] == "RES":           
                restype = line[4:7].strip()
                chain   = line[8]
                resnum  = int(line[9:13])
                access  = float(line[22:28])
                accessibilities[chain][resnum]=(restype,access)

        return accessibilities


    def check_if_number(self,residues,d,constraint):
        # warnings if first part of constraint element contains something else than a number
        # e.g. F26A:
        res1, res2 = residues
        try:
            if res1:
                int(res1[:-1])
            if res2:
                int(res2[:-1])
        except:
            if res1 and res2:
                # constraint pair
                print("Warning: checkConstraints: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. 11A:20B or 11A:20B:12.0)"%(constraint))
                cluster.progress("Warning: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. 11A:20B or 11A:20B:12.0)"%(constraint))
            elif not res2:
                # constraint on input A
                print("Warning: checkConstraints: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. 11A: or 11A::12.0)"%(constraint))
                cluster.progress("Warning: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. 11A: or 11A::12.0)"%(constraint))
            elif not res1:
                # constraint on intput B
                print("Warning: checkConstraints: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. :20B or :20B:12.0)"%(constraint))
                cluster.progress("Warning: Constraint %s isn't written in proper format - it will be ignored (format should be e.g. :20B or :20B:12.0)"%(constraint))
            return False, False
        return [(res1, res2),d]


    def check_constraint_format(self, constraint):
        """
        Check if constraint given is in the correct format i.e. <resi><chain>:<dist> for 
        a single residue or <resiA><chainA>:<resiB><chainB>:<dist> for a residue pair and 
        limiting chain identifiers to 'A' and 'B'. <dist> is optional and is set to 
        default_dist by default.
        INPUT
        constaint string
        OUTPUT
        the constraint as a list or False if constraint is in wrong format
        """
        res_pair_dist = re.match(r"([0-9A-Za-z ]{2,}):([0-9A-Za-z ]{2,}):([0-9\.]+)",constraint)
        res_single_distA = re.match(r"([0-9A-Za-z ]{2,}):[\-]*:([0-9\.]+)",constraint)
        res_single_distB = re.match(r"[\-]*:([0-9A-Za-z ]{2,}):([0-9\.]+)",constraint)   
        res_pair = re.match(r"([0-9A-Za-z ]{2,}):([0-9A-Za-z ]{2,})",constraint)
        res_singleA = re.match(r"([0-9A-Za-z ]{2,}):[\-]*",constraint)
        res_singleB = re.match(r"[\-]*:([0-9A-Za-z ]{2,})",constraint)

        if res_pair_dist:
            # constraint pair with specified distance
            res1 = res_pair_dist.group(1)
            res2 = res_pair_dist.group(2)
            d = float(res_pair_dist.group(3))
            return self.check_if_number((res1, res2), d,constraint)       

        elif res_pair:
            # constaint pair
            res1 = res_pair.group(1)
            res2 = res_pair.group(2)
            return self.check_if_number((res1, res2), self.default_dist_pair,constraint)       

        elif res_single_distA:
            # single constraint with specified distance
            res = res_single_distA.group(1)
            d = float(res_single_distA.group(2))
            return self.check_if_number((res, False), d,constraint)       

        elif res_single_distB:
            # single constraint with specified distance
            res = res_single_distB.group(1)
            d = float(res_single_distB.group(2))
            return self.check_if_number((False,res), d,constraint)       

        elif res_singleA:
            # single constraint
            # check if user intended to provide distance and forgot the double colon
            res_dist = re.match(r"([0-9A-Za-z ]{2,}):([0-9\.]+)",constraint)
            res = res_singleA.group(1)
            if res_dist:
                print("Warning: checkConstraints: Constraint %s isn't properly formatted and will be interpreted as %s: - please use a double colon if you want to specify a distance"%(constraint,res_singleA.group(1)))
                cluster.progress("Warning: Constraint %s isn't properly formatted and will be interpreted as %s: - please use a double colon if you want to specify a distance"%(constraint,res_singleA.group(1)))
            return self.check_if_number((res,False), self.default_dist_single, constraint)       

        elif res_singleB:
            # single constraint
            res = res_singleB.group(1)
            return self.check_if_number((False,res), self.default_dist_single,constraint)       

        else:
            print("Warning: checkConstraints: Constraint %s isn't written in proper format - it will be ignored (check help for a description of the required format)"%(constraint))
            cluster.progress("Warning: Constraint %s isn't written in proper format - it will be ignored (check help for a description of the required format)"%(constraint))
            return False, False

        print("Warning: checkConstraints: Constraint %s isn't written in proper format - it will be ignored (check help for a description of the required format)"%(constraint))
        cluster.progress("Warning: Constraint %s isn't written in proper format - it will be ignored (check help for a description of the required format)"%(constraint))
        return False, False


    def get_contraints(self, trad_input2renamed):
        """
        Read file with list of residues supposedly at interface, returns a dict 
        with keys 'A', 'B' and 'AB'
        e.g.
        ['A'] -> set('125','2567',...)
        ['AB'] -> set(('125','86'),...)
        INPUT
        constaints file name
        OUTPUT
        constaints dictionary
        """
        all_res = {} # dictionary to keep track whether each user-provided constraint was used or not
        corres_res = defaultdict(lambda: defaultdict(lambda: dict())) # dictionary to keep track of original name of all constraints retained at this step
        list_of_res = [] # to keep user input order
        if trad_input2renamed:
            # oligomer state, need to take care of renumbering
            f=open(self.constraintfile,'r')
            inter_res = defaultdict(lambda: defaultdict(lambda: dict()))
            for res in re.split(r"[^a-zA-Z0-9\:\.\-]",f.read()):
                if not res:
                    continue
                list_of_res.append(res)
                residue, distance = self.check_constraint_format(res)
                if residue:
                    if not residue[0]:
                        # protein B
                        if residue[1][-1] in trad_input2renamed["proteinB"] and residue[1][:-1] in trad_input2renamed["proteinB"][residue[1][-1]]:
                            new_chain, new_res = trad_input2renamed["proteinB"][residue[1][-1]][residue[1][:-1]] # translate
                            if "proteinB" in corres_res and new_chain in corres_res["proteinB"] and new_res in corres_res["proteinB"][new_chain]:
                                oldres = corres_res["proteinB"][new_chain][new_res]
                                all_res[oldres][2] = "not used"
                                all_res[oldres][3] = "superseded by %s"%res
                            inter_res["proteinB"][new_chain][new_res] = distance
                            corres_res["proteinB"][new_chain][new_res] = res
                            all_res[res] = ["input B","%f"%distance,"used","residue %s on chain %s with distance %.1f A"%(residue[1][:-1],residue[1][-1],distance)]
                        else:
                            print("Warning: checkConstraints: Residue %s of ligand isn't in structure - omitting constraint %s"%(residue[1],res))
                            cluster.progress("Warning: Residue %s of ligand isn't in structure - omitting constraint %s"%(residue[1],res))
                            all_res[res] = ["input B","%f"%distance,"not used","%s not in structure"%(residue[1])]
                            
                    elif not residue[1]:
                        # protein A
                        if residue[0][-1] in trad_input2renamed["proteinA"] and residue[0][:-1] in trad_input2renamed["proteinA"][residue[0][-1]]:
                            new_chain, new_res = trad_input2renamed["proteinA"][residue[0][-1]][residue[0][:-1]] # translate
                            if "proteinA" in corres_res and new_chain in corres_res["proteinA"] and new_res in corres_res["proteinA"][new_chain]:
                                oldres = corres_res["proteinA"][new_chain][new_res]
                                all_res[oldres][2] = "not used"
                                all_res[oldres][3] = "superseded by %s"%res
                            inter_res["proteinA"][new_chain][new_res] = distance
                            corres_res["proteinA"][new_chain][new_res] = res
                            all_res[res] = ["input A","%f"%distance,"used","residue %s on chain %s with distance %.1f A"%(residue[0][:-1],residue[0][-1],distance)]
                        else:
                            print("Warning: checkConstraints: Residue %s of receptor isn't in structure - omitting constraint %s"%(residue[0],res))
                            cluster.progress("Warning: Residue %s of receptor isn't in structure - omitting constraint %s"%(residue[0],res))
                            all_res[res] = ["input A","%f"%distance,"not used","%s not in structure"%(residue[0])]

                    else:
                        # it's a pair of residues
                        if residue[1][-1] in trad_input2renamed["proteinB"] and residue[1][:-1] in trad_input2renamed["proteinB"][residue[1][-1]] and residue[0][-1] in trad_input2renamed["proteinA"] and residue[0][:-1] in trad_input2renamed["proteinA"][residue[0][-1]]:
                            new_chainA, new_resA = trad_input2renamed["proteinA"][residue[0][-1]][residue[0][:-1]] # translate
                            new_chainB, new_resB = trad_input2renamed["proteinB"][residue[1][-1]][residue[1][:-1]] # translate
                            if "pair" in corres_res and new_chainA+new_chainB in corres_res["pair"] and (new_resA,new_resB) in corres_res["pair"][new_chainA+new_chainB]:
                                oldres = corres_res["pair"][new_chainA+new_chainB][(new_resA,new_resB)]
                                all_res[oldres][2] = "not used"
                                all_res[oldres][3] = "superseded by %s"%res
                            inter_res["pair"][new_chainA+new_chainB][(new_resA,new_resB)] = distance
                            corres_res["pair"][new_chainA+new_chainB][(new_resA,new_resB)] = res
                            all_res[res] = ["pair","%f"%distance,"used","residue pair %s on chain %s (in receptor) and %s on chain %s (in ligand) with distance %.1f A"%(residue[0][:-1],residue[0][-1],residue[1][:-1],residue[1][-1],distance)]
                        else:
                            print("Warning: checkConstraints: Constraint %s isn't written in proper format or contains residues that aren't in the structures - it will be ignored"%res)
                            cluster.progress("Warning: Constraint %s isn't written in proper format or contains residues that aren't in the structures - it will be ignored"%res)
                            all_res[res] = ["pair","%f"%distance,"not used","bad format (see help for details) or residue(s) not in structure"]
                else:
                    all_res[res] = ["","","not used", "bad format (see help for details)"]
            
            f.close()

        else:    
            # monomer/monomer docking, no need for renumbering
            f=open(self.constraintfile,'r')
            inter_res = defaultdict(lambda: defaultdict(lambda: dict()))
            for res in re.split(r"[^a-zA-Z0-9\:\.\-]",f.read()):
                if not res:
                    continue
                list_of_res.append(res)
                residue, distance = self.check_constraint_format(res)
                if residue:
                    if not residue[0]:
                        # protein B
                        if "proteinB" in corres_res and residue[1][-1] in corres_res["proteinB"] and residue[1][:-1] in corres_res["proteinB"][residue[1][-1]]:
                            oldres = corres_res["proteinB"][residue[1][-1]][residue[1][:-1]]
                            all_res[oldres][2] = "not used"
                            all_res[oldres][3] = "superseded by %s"%res
                        inter_res["proteinB"][residue[1][-1]][residue[1][:-1]] = distance
                        corres_res["proteinB"][residue[1][-1]][residue[1][:-1]] = res
                        all_res[res] = ["input B","%f"%distance,"used","residue %s on chain %s with distance %.1f A"%(residue[1][:-1],residue[1][-1],distance)]
                    elif not residue[1]:
                        # protein A
                        if "proteinA" in corres_res and residue[0][-1] in corres_res["proteinA"] and residue[0][:-1] in corres_res["proteinA"][residue[0][-1]]:
                            oldres = corres_res["proteinA"][residue[0][-1]][residue[0][:-1]]
                            all_res[oldres][2] = "not used"
                            all_res[oldres][3] = "superseded by %s"%res
                        inter_res["proteinA"][residue[0][-1]][residue[0][:-1]] = distance
                        corres_res["proteinA"][residue[0][-1]][residue[0][:-1]] = res
                        all_res[res] = ["input A","%f"%distance,"used","residue %s on chain %s with distance %.1f A"%(residue[0][:-1],residue[0][-1],distance)]
                    else:
                        # it's a pair of residues
                        res1, res2 = residue
                        if "pair" in corres_res and res1[-1]+res2[-1] in corres_res["pair"] and (res1[:-1],res2[:-1]) in corres_res["pair"][res1[-1]+res2[-1]]:
                            oldres = corres_res["pair"][res1[-1]+res2[-1]][(res1[:-1],res2[:-1])]
                            all_res[oldres][2] = "not used"
                            all_res[oldres][3] = "superseded by %s"%res
                        inter_res["pair"][res1[-1]+res2[-1]][(res1[:-1],res2[:-1])] = distance
                        corres_res["pair"][res1[-1]+res2[-1]][(res1[:-1],res2[:-1])] = res
                        all_res[res] = ["pair","%f"%distance,"used","residue pair %s on chain %s (in receptor) and %s on chain %s (in ligand) with distance %.1f A"%(res1[:-1],res1[-1],res2[:-1],res2[-1],distance)]
                else:
                    all_res[res] = ["","","not used", "bad format (see help for details)"]

            f.close()

        return inter_res, all_res, corres_res, list_of_res


    def filter_constraints(self, constraints, threshold, trad_renamed2input, all_res, corres_constraints):
        """
        Filters the given constraints i.e. removes those that are not on protein surface or not
        in protein structure altogether
        INPUT
        constraints: dictionary with user constraints
        accessibilitiesA & accessibilitiesB: dictionary with accessibility values for each residue
        on each chain in proteins A & B
        threshold: accessibility above which a residue is considered on the surface of the protein
        trad_input2renamed: dictionary which is empty if in monomeric case
        OUTPUT
        new_constraints: filtered constraint dictionary
        """
        # define new constraints
        new_constraints = defaultdict(lambda: defaultdict(lambda: dict()))

        if trad_renamed2input:
            # oligomeric case
            # for protein A
            for chain in constraints['proteinA']:
                for resA in constraints['proteinA'][chain]:
                    input_chainA = trad_renamed2input['proteinA'][chain][resA][0]
                    input_resA = trad_renamed2input['proteinA'][chain][resA][1]
                    if int(resA) in self.accessibilitiesA[chain]:
                        if self.accessibilitiesA[chain][int(resA)][1] >= threshold:
                            # ok, on surface of chain A
                            new_constraints['proteinA'][chain][resA] = constraints['proteinA'][chain][resA]
                        else:
                            print("Warning: checkConstraints: Residue %s%s %s of receptor isn't exposed on surface - omitting constraint %s%s::%s"%(input_resA,input_chainA,self.accessibilitiesA[chain][int(resA)][0],input_resA,input_chainA,constraints['proteinA'][chain][resA]))
                            cluster.progress("Warning: Residue %s%s %s of receptor isn't exposed on surface - omitting constraint %s%s::%s"%(input_resA,input_chainA,self.accessibilitiesA[chain][int(resA)][0],input_resA,input_chainA,constraints['proteinA'][chain][resA]))
                            c = corres_constraints['proteinA'][chain][resA]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%s%s %s of receptor not exposed on surface"%(input_resA,input_chainA,self.accessibilitiesA[chain][int(resA)][0])
                    else:
                        print("Warning: checkConstraints: Residue %s%s of receptor isn't in structure - omitting constraint %s%s::%s"%(input_resA,input_chainA,input_resA,input_chainA,constraints['proteinA'][chain][resA]))
                        cluster.progress("Warning: Residue %s%s of receptor isn't in structure - omitting constraint %s%s::%s"%(input_resA,input_chainA,input_resA,input_chainA,constraints['proteinA'][chain][resA]))
                        c = corres_constraints['proteinA'][chain][resA]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%s%s of receptor not in structure"%(input_resA,input_chainA)

            # for protein B
            for chain in constraints['proteinB']:
                for resB in constraints['proteinB'][chain]:
                    input_chainB = trad_renamed2input['proteinB'][chain][resB][0]
                    input_resB = trad_renamed2input['proteinB'][chain][resB][1]
                    if int(resB) in self.accessibilitiesB[chain]:
                        if self.accessibilitiesB[chain][int(resB)][1] >= threshold:
                            # ok, on surface of chain B
                            new_constraints['proteinB'][chain][resB] = constraints['proteinB'][chain][resB]
                        else:
                            print("Warning: checkConstraints: Residue %s%s %s of ligand isn't exposed on surface - omitting constraint :%s%s:%s"%(input_resB,input_chainB,self.accessibilitiesB[chain][int(resB)][0],input_resB,input_chainB,constraints['proteinB'][chain][resB]))
                            cluster.progress("Warning: Residue %s%s %s of ligand isn't exposed on surface - omitting constraint :%s%s:%s"%(input_resB,input_chainB,self.accessibilitiesB[chain][int(resB)][0],input_resB,input_chainB,constraints['proteinB'][chain][resB]))
                            c = corres_constraints['proteinB'][chain][resB]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%s%s %s of ligand not exposed on surface"%(input_resB,input_chainB,self.accessibilitiesB[chain][int(resB)][0])
                    else:
                        print("Warning: checkConstraints: Residue %s%s of ligand isn't in structure - omitting constraint :%s%s:%s"%(input_resB,input_chainB,input_resB,input_chainB,constraints['proteinB'][chain][resB]))
                        cluster.progress("Warning: Residue %s%s of ligand isn't in structure - omitting constraint :%s%s:%s"%(input_resB,input_chainB,input_resB,input_chainB,constraints['proteinB'][chain][resB]))
                        c = corres_constraints['proteinB'][chain][resB]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%s%s of ligand not in structure"%(input_resB,input_chainB)

            # pairwise constraints
            for chainsAB in constraints["pair"]:
                for resA,resB in constraints["pair"][chainsAB]:
                    input_chainA = trad_renamed2input['proteinA'][chainsAB[0]][resA][0]
                    input_resA = trad_renamed2input['proteinA'][chainsAB[0]][resA][1]
                    input_chainB = trad_renamed2input['proteinB'][chainsAB[1]][resB][0]
                    input_resB = trad_renamed2input['proteinB'][chainsAB[1]][resB][1]
                    if int(resA) in self.accessibilitiesA[chainsAB[0]]:
                        if int(resB) in self.accessibilitiesB[chainsAB[1]]:
                            if self.accessibilitiesA[chainsAB[0]][int(resA)][1] >= threshold:
                                # ok, on surface of chain A
                                if self.accessibilitiesB[chainsAB[1]][int(resB)][1] >= threshold:
                                    # ok, on surface of chain B
                                    new_constraints['pair'][chainsAB][(resA,resB)] = constraints["pair"][chainsAB][(resA,resB)]
                                else:
                                    print("Warning: checkConstraints: Residue %s%s %s of ligand isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(input_resB,input_chainB,self.accessibilitiesB[chainsAB[1]][int(resB)][0],input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                                    cluster.progress("Warning: Residue %s%s %s of ligand isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(input_resB,input_chainB,self.accessibilitiesB[chainsAB[1]][int(resB)][0],input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                                    c = corres_constraints['pair'][chainsAB][(resA,resB)]
                                    all_res[c][2] = "not used"
                                    all_res[c][3] = "%s%s %s of ligand not exposed on surface"%(input_resB,input_chainB,self.accessibilitiesB[chainsAB[1]][int(resB)][0])
                            else:
                                print("Warning: checkConstraints: Residue %s%s %s of receptor isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(input_resA,input_chainA,self.accessibilitiesA[chainsAB[0]][int(resA)][0],input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                                cluster.progress("Warning: Residue %s%s %s of receptor isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(input_resA,input_chainA,self.accessibilitiesA[chainsAB[0]][int(resA)][0],input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                                c = corres_constraints['pair'][chainsAB][(resA,resB)]
                                all_res[c][2] = "not used"
                                all_res[c][3] = "%s%s %s of receptor not exposed on surface"%(input_resA,input_chainA,self.accessibilitiesA[chainsAB[0]][int(resA)][0])
                        else:
                            print("Warning: checkConstraints: Residue %s%s of ligand isn't in structure - omitting constraint %s%s:%s%s:%s"%(input_resB,input_chainB,input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                            cluster.progress("Warning: Residue %s%s of ligand isn't in structure - omitting constraint %s%s:%s%s:%s"%(input_resB,input_chainB,input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                            c = corres_constraints['pair'][chainsAB][(resA,resB)]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%s%s of ligand not in structure"%(input_resB,input_chainB)
                    else:
                        print("Warning: checkConstraints: Residue %s%s of receptor isn't in structure - omitting constraint %s%s:%s%s:%s"%(input_resA,input_chainA,input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                        cluster.progress("Warning: Residue %s%s of receptor isn't in structure - omitting constraint %s%s:%s%s:%s"%(input_resA,input_chainA,input_resA,input_chainA,input_resB,input_chainB,constraints["pair"][chainsAB][(resA,resB)]))
                        c = corres_constraints['pair'][chainsAB][(resA,resB)]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%s%s of receptor not in structure"%(input_resA,input_chainA)

        else:
            trad_renamed2input = defaultdict(lambda:defaultdict(lambda:{}))
            # monomeric case -> override chain names: protein A is always A and protein B is always B in proteins given as input in zdock_InterEvDock2.py
            # for protein A
            if constraints['proteinA']:
                # here there is a problem if constraints are given for partner A using 2 different chain ids
                # chain = constraints['proteinA'].keys()[0]
                chain = "A"
                for resA in constraints['proteinA'][chain]:
                    if int(resA) in self.accessibilitiesA["A"]:
                        if self.accessibilitiesA["A"][int(resA)][1] >= threshold:
                            # ok, on surface of chain A
                            new_constraints['proteinA']["A"][resA] = constraints['proteinA'][chain][resA]
                            trad_renamed2input['proteinA']["A"][resA] = (chain,resA)
                        else:
                            print("Warning: checkConstraints: Residue %sA %s of receptor isn't exposed on surface - omitting constraint %s%s::%s"%(resA,self.accessibilitiesA["A"][int(resA)][0],resA,"A",constraints['proteinA'][chain][resA]))
                            cluster.progress("Warning: Residue %sA %s of receptor isn't exposed on surface - omitting constraint %s%s::%s"%(resA,self.accessibilitiesA["A"][int(resA)][0],resA,"A",constraints['proteinA'][chain][resA]))
                            c = corres_constraints['proteinA'][chain][resA]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%sA %s of receptor not exposed on surface"%(resA,self.accessibilitiesA["A"][int(resA)][0])
                    else:
                        print("Warning: checkConstraints: Residue %sA of receptor isn't in structure - omitting constraint %s%s::%s"%(resA,resA,"A",constraints['proteinA'][chain][resA]))
                        cluster.progress("Warning: Residue %sA of receptor isn't in structure - omitting constraint %s%s::%s"%(resA,resA,"A",constraints['proteinA'][chain][resA]))
                        c = corres_constraints['proteinA'][chain][resA]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%sA of receptor not in structure"%(resA)
                for chain in constraints['proteinA']:
                    if chain != "A":
                        for resA in corres_constraints['proteinA'][chain]:
                            c = corres_constraints['proteinA'][chain][resA]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "wrong chain: should be A for receptor (first partner)"

            # for protein B
            if constraints['proteinB']:
                # here there is a problem if constraints are given for partner B using 2 different chain ids
                # chain = constraints['proteinB'].keys()[0]
                chain = "B"
                for resB in constraints['proteinB'][chain]:
                    if int(resB) in self.accessibilitiesB["B"]:
                        if self.accessibilitiesB["B"][int(resB)][1] >= threshold:
                            # ok, on surface of chain B
                            new_constraints['proteinB']["B"][resB] = constraints['proteinB'][chain][resB]
                            trad_renamed2input['proteinB']["B"][resB] = (chain,resB)
                        else:
                            print("Warning: checkConstraints: Residue %sB %s of ligand isn't exposed on surface - omitting constraint :%s%s:%s"%(resB,self.accessibilitiesB["B"][int(resB)][0],resB,"B",constraints['proteinB'][chain][resB]))
                            cluster.progress("Warning: Residue %sB %s of ligand isn't exposed on surface - omitting constraint :%s%s:%s"%(resB,self.accessibilitiesB["B"][int(resB)][0],resB,"B",constraints['proteinB'][chain][resB]))
                            c = corres_constraints['proteinB'][chain][resB]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%sB %s of ligand not exposed on surface"%(resB,self.accessibilitiesB["B"][int(resB)][0])
                    else:
                        print("Warning: checkConstraints: Residue %sB of ligand isn't in structure - omitting constraint :%s%s:%s"%(resB,resB,"B",constraints['proteinB'][chain][resB]))
                        cluster.progress("Warning: Residue %sB of ligand isn't in structure - omitting constraint :%s%s:%s"%(resB,resB,"B",constraints['proteinB'][chain][resB]))
                        c = corres_constraints['proteinB'][chain][resB]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%sB of ligand not in structure"%(resB)
                for chain in constraints['proteinB']:
                    if chain != "B":
                        for resB in corres_constraints['proteinB'][chain]:
                            c = corres_constraints['proteinB'][chain][resB]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "wrong chain: should be B for ligand (second partner)"

            if constraints['pair']:
                # here there is a problem if constraints are given for partner A or B using 2 different chain ids
                # chainsAB = constraints['pair'].keys()[0]
                chainsAB = "AB"
                # pairwise constraints
                for resA,resB in constraints["pair"][chainsAB]:
                    if int(resA) in self.accessibilitiesA["A"]:
                        if int(resB) in self.accessibilitiesB["B"]:
                            if self.accessibilitiesA["A"][int(resA)][1] >= threshold:
                                # ok, on surface of chain A
                                if self.accessibilitiesB["B"][int(resB)][1] >= threshold:
                                    # ok, on surface of chain B
                                    new_constraints['pair']["AB"][(resA,resB)] = constraints["pair"][chainsAB][(resA,resB)]
                                    trad_renamed2input['proteinA']["A"][resA] = (chainsAB[0],resA)
                                    trad_renamed2input['proteinB']["B"][resB] = (chainsAB[1],resB)
                                else:
                                    print("Warning: checkConstraints: Residue %sB %s of ligand isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(resB,self.accessibilitiesB["B"][int(resB)][0],resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                                    cluster.progress("Warning: Residue %sB %s of ligand isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(resB,self.accessibilitiesB["B"][int(resB)][0],resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                                    c = corres_constraints["pair"][chainsAB][(resA,resB)]
                                    all_res[c][2] = "not used"
                                    all_res[c][3] = "%sB %s of ligand not exposed on surface"%(resB,self.accessibilitiesB["B"][int(resB)][0])
                            else:
                                print("Warning: checkConstraints: Residue %sA %s of receptor isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(resA,self.accessibilitiesA["A"][int(resA)][0],resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                                cluster.progress("Warning: Residue %sA %s of receptor isn't exposed on surface - omitting constraint %s%s:%s%s:%s"%(resA,self.accessibilitiesA["A"][int(resA)][0],resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                                c = corres_constraints["pair"][chainsAB][(resA,resB)]
                                all_res[c][2] = "not used"
                                all_res[c][3] = "%sA %s of receptor not exposed on surface"%(resA,self.accessibilitiesA["A"][int(resA)][0])
                        else:
                            print("Warning: checkConstraints: Residue %sB of ligand isn't in structure - omitting constraint %s%s:%s%s:%s"%(resB,resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                            cluster.progress("Warning: Residue %sB of ligand isn't in structure - omitting constraint %s%s:%s%s:%s"%(resB,resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                            c = corres_constraints["pair"][chainsAB][(resA,resB)]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "%sB of ligand not in structure"%(resB)
                    else:
                        print("Warning: checkConstraints: Residue %sA of receptor isn't in structure - omitting constraint %s%s:%s%s:%s"%(resA,resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                        cluster.progress("Warning: Residue %sA of receptor isn't in structure - omitting constraint %s%s:%s%s:%s"%(resA,resA,"A",resB,"B",constraints["pair"][chainsAB][(resA,resB)]))
                        c = corres_constraints["pair"][chainsAB][(resA,resB)]
                        all_res[c][2] = "not used"
                        all_res[c][3] = "%sA of receptor not in structure"%(resA)
                for chainsAB in constraints['pair']:
                    if chainsAB != "AB":
                        for pair in constraints['pair'][chainsAB]:
                            c = corres_constraints['pair'][chainsAB][pair]
                            all_res[c][2] = "not used"
                            all_res[c][3] = "wrong chain(s): should be A for receptor (first partner) and B for ligand (second partner)"
                        
        return new_constraints, trad_renamed2input, all_res


    def write_frodock_contraints_v21(self, constraints):
        """
        Writes a frodock-friendly constraints file
        INPUT
        name of the output file
        constraints dictionary
        max distance that defines a contact between 2 residues
        OUTPUT
        None
        
        e.g. contact file input for fd constraints
        RECEPT_____     LIGAND_____ D__
        -------------------------------
        THR B   31      GLY A    46 10
        ASP C  102      GLU A    45  5
        """
        f=open(self.output,"w")
        f.write("RECEPT_____     LIGAND_____ D__\n")
        f.write("-------------------------------\n")

        for chainA in constraints["proteinA"]:
            for resnumA in constraints["proteinA"][chainA]:
                restypeA, _ = self.accessibilitiesA[chainA][int(resnumA)]
                f.write("{:<3} {:<1}{:>5}      {:<3} {:<1}{:>5}  {:>3.3}\n".format(restypeA,chainA,resnumA, "---","-","-1", constraints["proteinA"][chainA][resnumA]))

        for chainB in constraints["proteinB"]:
            for resnumB in constraints["proteinB"][chainB]:
                restypeB, _ = self.accessibilitiesB[chainB][int(resnumB)]
                f.write("{:<3} {:<1}{:>5}      {:<3} {:<1}{:>5}  {:>3.3}\n".format("---","-","-1",restypeB,chainB,resnumB, constraints["proteinB"][chainB][resnumB]))

        for chainsAB in constraints["pair"]:
            chainA = chainsAB[0]
            chainB = chainsAB[1]
            for resnumA, resnumB in constraints["pair"][chainsAB]:
                restypeA, _ = self.accessibilitiesA[chainA][int(resnumA)]
                restypeB, _ = self.accessibilitiesB[chainB][int(resnumB)]
                f.write("{:<3} {:<1}{:>5}      {:<3} {:<1}{:>5}  {:>3.3}\n".format(restypeA,chainA,resnumA, restypeB,chainB,resnumB, constraints["pair"][chainsAB][(resnumA,resnumB)]))

        f.close()
        

    def write_user_constraints(self, constraints, trad_renamed2input):
        """
        Write user-friendly output file with constraints that were retained 
        with same numbering as given by the user
        INPUT
        user_output: name of output file
        constraints: filtered constraint dictionary
        trad_renamed2input: translation dictionary in order to match renumbered
        numbering with input numbering
        OUTPUT
        None - it creates a file
        """
        txt = ''
        if trad_renamed2input:
            f=open(self.user_output,'w')
            f.write("Constraints that will be used:\n")
            txt+="Constraints that will be used:\n"
            string = ""
            for chain, dic in constraints["proteinA"].items():
                for res,dist in dic.items():
                    old_chain, old_res = trad_renamed2input["proteinA"][chain][res]
                    string += "- Residue {} on chain {} with distance {} A\n".format(old_res,old_chain,dist)
            if string:
                f.write("Receptor (input A):\n")
                f.write(string)
                txt+="Receptor (input A):\n"
                txt+=string

            string = ""
            for chain, dic in constraints["proteinB"].items():
                for res,dist in dic.items():
                    old_chain, old_res = trad_renamed2input["proteinB"][chain][res]
                    string += "- Residue {} on chain {} with distance {} A\n".format(old_res,old_chain,dist)
            if string:
                f.write("Ligand (input B):\n")
                f.write(string)
                txt+="Ligand (input B):\n"
                txt+=string

            string = ""
            for chainsAB, dic in constraints["pair"].items():
                for (resA,resB),dist in dic.items():
                    old_chainA, old_resA = trad_renamed2input["proteinA"][chainsAB[0]][resA]
                    old_chainB, old_resB = trad_renamed2input["proteinB"][chainsAB[1]][resB]
                    string += "- Residue pair {} on chain {} in receptor and {} on chain {} in ligand with distance {} A\n".format(old_resA,old_chainA,old_resB,old_chainB,dist)
            if string:
                f.write("Residue pairs:\n")
                f.write(string)
                txt+="Residue pairs:\n"
                txt+=string
            f.close()

        else:
            f=open(self.user_output,'w')
            f.write("Constraints that will be used:\n")
            txt+="Constraints that will be used:\n"
            string = ""
            for chain, dic in constraints["proteinA"].items():
                for res,dist in dic.items():
                    string += "- Residue {} on chain {} with distance {} A to any atom in protein B\n".format(res,chain,dist)
            if string:
                f.write("Receptor (protein A):\n")
                f.write(string)
                txt+="Receptor (input A):\n"
                txt+=string

            string = ""
            for chain, dic in constraints["proteinB"].items():
                for res,dist in dic.items():
                    string += "- Residue {} on chain {} with distance {} A to any atom in protein A\n".format(res,chain,dist)
            if string:
                f.write("Ligand (protein B):\n")
                f.write(string)
                txt+="Ligand (input B):\n"
                txt+=string

            string = ""
            for chainsAB, dic in constraints["pair"].items():
                for (resA,resB),dist in dic.items():
                    string += "- Residue pair {} on chain {} in receptor and {} on chain {} in ligand with distance {} A\n".format(resA,chainsAB[0],resB,chainsAB[1],dist)
            if string:
                f.write("Residue pairs:\n")
                f.write(string)
                txt+="Residue pairs:\n"
                txt+=string
            f.close()
        cluster.progress(txt)


    def write_table_constraints(self, all_constraints, list_constraints):
        # write a simple tab-separated file giving:
        # original constraint, constraint type, constraint distance, used/not used, if not used why
        f = open(self.table_output,'w')
        for res in list_constraints:
            # print(res)
            s = all_constraints[res]
            f.write("%20s\t%10s\t%s\n"%(res,s[2],s[3]))
        f.close()
            

    def readPDB(self, address):
        """
        Read pdb file and get translation dictionary from pdb residue id's 
        to sequence residue id's
        INPUT
        a pdb file
        OUTPUT
        a dictionary [<pdb residue id>] -> <sequence residue id>
        """
        f = open(address,'r')
        flines = f.readlines()
        f.close()

        oldres = -10000000
        residue_counter = 0 # counter for renumbering residues
        d_pdbid2seqid = defaultdict( lambda : {})
        prev_chain = "-1"

        for line in flines:
            if line[0:4]!="ATOM":
                # Keep only ATOM lines
                continue
            if line[13]=="H" or (len(line)>=78 and line[77]=="H"):
                # Ignore all hydrogen atoms
                continue

            index_res    = line[22:26].strip()  # residue index
            chain        = line[21]             # chain
            mult_density = line[16]             # column for atoms with multiple alternate locations
            
            if chain != prev_chain:
                prev_chain = chain
                residue_counter = 0
                oldres = -10000000

            if mult_density not in [" ","A"]:
                # keep only the first location for each atom
                continue
            
            if int(index_res)!=int(oldres):
                # starting with a new residue
                residue_counter += 1
                oldres = int(index_res)
            else:
                continue

            # New number for residue
            d_pdbid2seqid[chain][index_res] = str(residue_counter)

        return d_pdbid2seqid


    def renumber_constraints(self, constraints):
        """
        To renumber constraints according to their position in the residue
        sequence in stead of the pdb numbering
        INPUT
        constraints dictionary with a list of residue id's for each chain or residue pairs
        OUTPUT
        a new constraints dictionary 
        """
        new_constraints = defaultdict(lambda: defaultdict(lambda: dict()))
        new_accessibilitiesA = defaultdict(lambda: dict())
        new_accessibilitiesB = defaultdict(lambda: dict())

        # get dictionary to translate pdb id's to seq id's
        d_pdbid2seqid = {}
        d_pdbid2seqid['proteinA'] = self.readPDB(self.pdbA)
        d_pdbid2seqid['proteinB'] = self.readPDB(self.pdbB)

        for chain in constraints['proteinA']:
            for resA in constraints['proteinA'][chain]:
                new_constraints['proteinA'][chain][d_pdbid2seqid['proteinA'][chain][resA]] = constraints['proteinA'][chain][resA]
                new_accessibilitiesA[chain][int(d_pdbid2seqid['proteinA'][chain][resA])] = self.accessibilitiesA[chain][int(resA)]

        for chain in constraints['proteinB']:
            for resB in constraints['proteinB'][chain]:
                new_constraints['proteinB'][chain][d_pdbid2seqid['proteinB'][chain][resB]] = constraints['proteinB'][chain][resB]
                new_accessibilitiesB[chain][int(d_pdbid2seqid['proteinB'][chain][resB])] = self.accessibilitiesB[chain][int(resB)]

        for chainsAB, dic in constraints['pair'].items():
            for resA,resB in dic:
                new_constraints['pair'][chainsAB][(d_pdbid2seqid['proteinA'][chainsAB[0]][resA],d_pdbid2seqid['proteinB'][chainsAB[1]][resB])] = constraints['pair'][chainsAB][(resA,resB)]
                new_accessibilitiesA[chainsAB[0]][int(d_pdbid2seqid['proteinA'][chainsAB[0]][resA])] = self.accessibilitiesA[chainsAB[0]][int(resA)]
                new_accessibilitiesB[chainsAB[1]][int(d_pdbid2seqid['proteinB'][chainsAB[1]][resB])] = self.accessibilitiesB[chainsAB[1]][int(resB)]
                
        return new_constraints, new_accessibilitiesA, new_accessibilitiesB  


    def if_constraints_empty(self,new_constraints):
        """
        Check if there are any constraints left in the dictionary
        INPUT
        constraint dictionary
        OUTPUT
        boolean - true if the dictionary is empty
        """
        for chain in new_constraints["proteinA"]:
            # if dictionary isn't empty
            if new_constraints["proteinA"][chain]:
                return False
        for chain in new_constraints["proteinB"]:
            # if dictionary isn't empty
            if new_constraints["proteinB"][chain]:
                return False
        for chains in new_constraints["pair"]:
            # if dictionary isn't empty
            if new_constraints["pair"][chains]:
                return False

        return True


    def read_conversion_table(self):
        """
        Read oligomeric conversion table in order to match user constraints
        with renamed residues and chains in pdb files used for docking
        INPUT
        conversion file
        OUTPUT
        2 dictionaries: ones that converts input residue numbering to renumbered 
        ones and the other to do the reverse translation
        [proteinA/B] -> [chain] -> [res] -> (chain, res)
        """
        trad_renamed2input = defaultdict(lambda:defaultdict(lambda:{}))
        trad_input2renamed = defaultdict(lambda:defaultdict(lambda:{}))
        for prot,conversionfile in zip(["proteinA","proteinB"],[self.conversionfileA,self.conversionfileB]):
            f=open(conversionfile)
            for line in f:
                res_input, res_renamed = line.split()
                res_input = res_input.split(".")
                res_renamed = res_renamed.split(".")
                trad_renamed2input[prot][res_renamed[2]][res_renamed[1]] = (res_input[2],res_input[1])
                trad_input2renamed[prot][res_input[2]][res_input[1]] = (res_renamed[2],res_renamed[1])
            f.close()

        return trad_input2renamed, trad_renamed2input


def usage():
    print("""checkResOnSurface.py
   USAGE : python checkResOnSurface.py
                                 -rec <receptor pdb file (chain A)>
                                 -lig <ligand pdb file (chain B)>
                                 -con <constraints file>
                                 [-str]
                                 [-out <output file>]
                                 
            N.B. constraints file should be a space-, tab- or new line-delimited file 
            with constaints in the format <resid><chain> for single residues or 
            <resid1><chainA>:<resid2><chainB> for pairs. The receptor is always chain A
            and the ligand always chain B. However, chain id's within the pdb files 
            themselves don't have to be named A and B. Make sure there's only one chain
            per pdb file as the programme will ignore the others if there are any.

            If pdb structures were provided and the residues in the constraint file are 
            numbered according to the pdb file, add option -str to renumber them 
            according to the residue sequence.

          """)
    sys.exit(1)
    return True


def main():

    threshold = 1.0 # percentage relative accessibility threshold, residues above it are on the surface (20% seems to be commonly used)
    default_dist_single = 8.0   # maximum all-atom distance between 2 residues for them to be considered in contact /!\ has to be a float
    default_dist_pair = 11.0   # maximum all-atom distance between 2 residue pairs for them to be considered in contact /!\ has to be a float

    # get arguments
    try:
        pdbA = os.path.abspath(sys.argv[sys.argv.index('-rec')+1])
        pdbB = os.path.abspath(sys.argv[sys.argv.index('-lig')+1])
    except:
        print("\nERROR: Receptor or ligand pdb file missing\n")
        usage()
        sys.exit(-1)

    try:
        constraintfile = os.path.abspath(sys.argv[sys.argv.index('-con')+1])
    except:
        print("\nERROR: constraint file missing\n")
        usage()
        sys.exit(-1)

    try:
        output = os.path.abspath(sys.argv[sys.argv.index('-out')+1])
    except:
        output = os.path.join(os.path.dirname(constraintfile),"frodock_constraints.txt")

    try:
        user_output = os.path.abspath(sys.argv[sys.argv.index('-usr')+1])
    except:
        user_output = os.path.join(os.path.dirname(constraintfile),"user_constraints.txt")

    try:
        table_output = os.path.abspath(sys.argv[sys.argv.index('-table')+1])
    except:
        table_output = os.path.join(os.path.dirname(constraintfile),"table_constraints.txt")

    try:
        conversionfileA = os.path.abspath(sys.argv[sys.argv.index('-tradA')+1])
        conversionfileB = os.path.abspath(sys.argv[sys.argv.index('-tradB')+1])
    except:
        conversionfileA = False
        conversionfileB = False

    if conversionfileA and conversionfileB and not os.path.exists(conversionfileA) and not os.path.exists(conversionfileB):
        conversionfileA = False
        conversionfileB = False

    checkResConstraints(pdbA=pdbA, pdbB=pdbB, constraintfile=constraintfile, conversionfileA=conversionfileA, conversionfileB=conversionfileB, output=output, user_output=user_output, table_output=table_output, renumber=('-str' in sys.argv), threshold = threshold, default_dist_single = default_dist_single, default_dist_pair = default_dist_pair)



""" MAIN """

if __name__=="__main__":

    main()



