import glob
import os
import re
import sys
import time
import subprocess
import signal
import threading
import drmaa

TIME_START = time.time()


if not 'MODULEPATH' in os.environ:
    f = open(os.environ['MODULESHOME'] + "/init/.modulespath", "r")
    path = []
    for line in f.readlines():
        line = re.sub("#.*$", '', line)
        if line is not '':
            path.append(line)
    os.environ['MODULEPATH'] = ':'.join(path)


if not 'LOADEDMODULES' in os.environ:
    os.environ['LOADEDMODULES'] = ''


def module(*args):
    if type(args[0]) == type([]):
        args = args[0]
    else:
        args = list(args)
    (output, error) = subprocess.Popen(['/usr/bin/modulecmd', 'python'] + 
            args, stdout=subprocess.PIPE).communicate()
    exec(output)


def progress_time(line, wdir = "./"):
    ''' Write progress log line '''
    line= time.strftime('[%H:%M:%S] ', time.gmtime(time.time() - TIME_START)) + line 
    with open(wdir + ".progress.txt", "a") as logfile:
        logfile.write(line + "\n")


def progress(line, wdir = "./"):
    ''' Write progress log line '''
    return progress_time(line, wdir)


def signal_handler(function):
    def wrapper(*args, **kwargs):

        s = args[0]
        job_list = args[1]

        def killTasks(signum, frame):

            for i, jobid in enumerate(job_list):
                 arrayid = int(jobid.split("_")[0])
                 if (i != len(job_list) - 1):
                     real_jobid = str(arrayid + i + 1)
                 else:
                     real_jobid = jobid
                 if s.jobStatus(real_jobid) not in ("done", "failed"):
                     print("cluster: Caught %s signal! Cancelling job %s" % (signum, jobid))
                     s.control(real_jobid, drmaa.JobControlAction.TERMINATE)

        signal.signal(signal.SIGTERM, killTasks)
        signal.signal(signal.SIGINT, killTasks)

        return function(*args, **kwargs)

    return wrapper


@signal_handler
def waitTasks(s, job_list, show_progress = True):
    """
    Wait for cluster job(s)
    """

    # This function can be interrupted with SIGTERM
    def wait(target, args):
        thread = threading.Thread(target = target, args = args)
        thread.start()
        while thread.isAlive():
            time.sleep(1)

    collected = 0
    percents = 0
    tasks = len(job_list)

    # dirty fix to avoid drmaa bug
    while (s.jobStatus(job_list[0])=="undetermined"):
        time.sleep(1)

    if show_progress and tasks > 9:
        for jobid in job_list:
            #~ s.wait(jobid, drmaa.Session.TIMEOUT_WAIT_FOREVER)
            while (s.jobStatus(jobid)=="running" or s.jobStatus(jobid)=="queued_active"):
                time.sleep(1)
            collected += 1/float(tasks)*100
        # Print percentage milestones
            if collected >= percents and percents < 90:
                percents += 10
                with open(".progress.txt", "a") as logfile:
                    logfile.write('%d%%..' % percents)
        # Final progress
        if collected >= 90:
            with open(".progress.txt", "a") as logfile:
                logfile.write('100%\n')
    else:
        #~ s.synchronize(job_list, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
        for jobid in job_list:
            while (s.jobStatus(jobid)=="running" or s.jobStatus(jobid)=="queued_active"):
                time.sleep(1)

    return 0


def runTasks(command, args, tasks = 1, tasks_from = 1, environment_module = None, map_list = None, log_prefix = None, job_name = None, job_opts = '', join_files = True, progress = True, partition = None, account = None, qos = None, wait = True):
    """
    Run a job array
    """

    # Start the DRMAA session

    s = drmaa.Session()
    s.initialize()
    jt = s.createJobTemplate()
    jt.nativeSpecification = job_opts

    # Set the partition to the current one unless specified otherwise

    if partition is not None:
        jt.nativeSpecification += " -p %s" % partition
    elif "SLURM_JOB_PARTITION" in os.environ:
        partition = os.environ["SLURM_JOB_PARTITION"]
        jt.nativeSpecification += " -p %s" % partition

    # Set the account to the current one unless specified otherwise (mobyle only)

    if account is not None:
        jt.nativeSpecification += " -A %s" % account
    elif "SLURM_ACCOUNT" in os.environ:
        account = os.environ["SLURM_ACCOUNT"]
        jt.nativeSpecification += " -A %s" % account

    # Set the qos to the current one unless specified otherwise (mobyle only)

    if qos is not None:
        jt.nativeSpecification += " --qos=%s" % qos

    # Set the environment to load

    if environment_module is not None:
        try:
            if isinstance(environment_module, str):
                module('load', environment_module)
            elif isinstance(environment_module, list):
                for environment in environment_module:
                    module('load', environment)
        except:
            raise
    jt.jobEnvironment = os.environ

    # Set the command to run

    if map_list is not None:
        command = "eval " + command
        with open("map_file.txt", "wt") as map_file:
            map_file.write("\n".join(map_list))
        args = [ arg.replace("map_item", '$(sed -n "${SLURM_ARRAY_TASK_ID}p" map_file.txt)') for arg in args ]
        tasks = len(map_list)
    jt.remoteCommand = command
    jt.args = args

    # Set job name

    if job_name is not None:
        jt.jobName = job_name
    else:
        if "SLURM_JOB_NAME" in os.environ:
            jt.jobName = os.environ["SLURM_JOB_NAME"]
        else:
            if environment_module is not None:
                jt.jobName = "%s:%s" % (environment_module, command)
            else:
                jt.jobName = "%s" % command

    # Set prefix for log file

    if log_prefix is None:
        log_prefix = "slurm"
    jt.outputPath = ":%s." % log_prefix
    jt.errorPath = ":%s." % log_prefix
    if log_prefix[0] != '/':
        if tasks > 1:
            jt.outputPath += "%a"
            jt.errorPath += "%a"
        jt.outputPath += ".log"
        jt.errorPath += ".err"
    else:
        join_files = False # Disable
    try:
        with open('%s.log' % log_prefix, 'a') as outfile:
            outfile.write("""# cluster.runTasks:
--------------------
submit host : %s
job name : %s
specifications : %s
tasks : %d
command : %s %s
environment module : %s
--------------------
""" % (os.environ['SLURMD_NODENAME'], jt.jobName, jt.nativeSpecification, tasks, jt.remoteCommand, ' '.join(jt.args), environment_module))
    except:
        pass

    # Collect results

    ret = 0
    job_list = []
    try:
        job_list = s.runBulkJobs(jt, tasks_from, tasks_from + tasks - 1, 1)
        if wait:
            ret = waitTasks(s, job_list, progress)
    except:
        # Terminate all tasks
        progress_time('... an error occured, terminating tasks')
        raise

    # Reduce logs

    if wait and join_files:
        with open('%s.log' % log_prefix, 'a') as outfile:
            for file_name in glob.glob('%s*.log' % log_prefix):
                if file_name == '%s.log' % log_prefix:
                    continue
                with open(file_name) as infile:
                    outfile.write(infile.read())
                os.remove(file_name)

    if wait and join_files:
        with open('%s.err' % log_prefix, 'a') as outfile:
            for file_name in glob.glob('%s*.err' % log_prefix):
                if file_name == '%s.err' % log_prefix:
                    continue
                with open(file_name) as infile:
                    outfile.write(infile.read())
                os.remove(file_name)

    s.deleteJobTemplate(jt)
    s.exit()

    # Sync buffer cache and reenter
    subprocess.check_output('sync')

    return ret


def runMPITasks(command, args, environment_module = None, log_prefix = None, job_name = None, job_opts = '', partition = None, account = None, qos = None):
    """
    Start MPI job(s)
    """

    # Set the command to run

    slurm_cmd = ["salloc", "--exclusive"]

    # Set the partition to the current one unless specified otherwise

    if partition is not None:
        slurm_cmd.append("-p %s" % partition)
    elif "SLURM_JOB_PARTITION" in os.environ:
        partition = os.environ["SLURM_JOB_PARTITION"]
        slurm_cmd.append("-p %s" % partition)

    # Set the account to the current one unless specified otherwise (mobyle only)

    if account is not None:
        slurm_cmd.append("-A %s" % account)
    elif "SLURM_ACCOUNT" in os.environ:
        account = os.environ["SLURM_ACCOUNT"]
        slurm_cmd.append("-A %s" % account)

    # Set the qos to the current one unless specified otherwise (mobyle only)

    if qos is not None:
        slurm_cmd.append("--qos=%s" % qos)

    # Set the environment to load

    if environment_module is not None:
        if "openmpi/1.10.7-rpbs" in environment_module:
            slurm_cmd.append("mpirun --mca oob_tcp_if_include 10.0.1.0/24")
        else:
            slurm_cmd.append("mpirun --mca btl_tcp_if_include 10.0.1.0/24")
        try:
            if isinstance(environment_module, str):
                module('load', environment_module)
            elif isinstance(environment_module, list):
                for environment in environment_module:
                    module('load', environment)
        except:
            raise

    # Set job name

    if job_name is not None:
        slurm_cmd.append("-J %s" % job_name)
    else:
        if "SLURM_JOB_NAME" in os.environ:
            job_name = os.environ["SLURM_JOB_NAME"]
        else:
            if environment_module is not None:
                job_name = "%s:%s" % (environment_module, command)
            else:
                job_name = "%s" % command

    # Set the command to run

    slurm_cmd.append(command)
    slurm_cmd.extend(args)

    # Log prefix
    if log_prefix is None:
        log_prefix = "slurm"
    try:
        with open('%s.log' % log_prefix, 'a') as outfile:
            outfile.write("""# cluster.runMPITasks:
--------------------
submit host : %s
job name : %s
specifications : %s
command : %s
environment module : %s
--------------------
""" % (os.environ['SLURMD_NODENAME'], job_name, job_opts, " ".join(slurm_cmd), environment_module))
    except:
        pass

    # Run jobs

    try:
        with open('%s.log' % log_prefix, 'a') as log_file, open('%s.err' % log_prefix, 'a') as err_file:
            os.system(" ".join(slurm_cmd))
    except:
        raise

    # Sync buffer cache and reenter
    subprocess.check_output('sync')
