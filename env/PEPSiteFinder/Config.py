#!/usr/bin/env python

import pkg_resources

# ------------------------- Globals
VERSION = "1.0"
# -------------------------

PEPFOLD           = "/usr/local/bin/CppBuilder"

PEPDOCK_ROOT      = "/service/env/PEPSiteFinder"

PEPDOCK_DEMO      = pkg_resources.resource_filename("PEPSiteFinder", "demo")

DOCKER_IMG        = "pep-sitefinder/1.0-rpbs"

BABEL_DOCKER_IMG  = "opendocking/1.0-rpbs"

DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"

PEPDOCK_PROGRESS_FILE = ".progress.txt"

REMOTE_SERVER     = False  # If True, you can specify the remote python interpreter by setting the IPYTHON value
REMOTE_PYTHON     = "/usr/bin/env python"
SYNCHRONOUS       = False # If true, the process will not exit after sge submit, but wait cfor completion (required for Mobyle)

# The fst and pdb files should be copied into the PEPDOCK_ROOT
# DFLT_DEMO_PDB      = "1A1M_prot.pdb"
# DFLT_DEMO_PEPTIDE  = "1A1M_lig.pdb"
# DFLT_DEMO_LABEL    = "1A1M"
DFLT_DEMO_PDB      = "1N7F.pdb"
DFLT_DEMO_PEPSEQ   = "1N7F_lig.fst"
DFLT_DEMO_PEPPDB   = "1N7F_lig.pdb"
DFLT_DEMO_PEPPDB   = "1n7eA_lig.pdb"
DFLT_DEMO_LABEL    = "1N7F"

# Library
PTOOLS_PATH = "/usr/local/ptools" #path inside the docker container
PARAMETERS_PATH = pkg_resources.resource_filename("PEPSiteFinder", "templates")
NBESTMODELS = 10

DFLT_WPATH        = "."
PEPDOCK_DFLT_LABEL = "PEPDOCK"
DFLT_BIN_PATH     = "%s/bin" % PEPDOCK_ROOT
DFLT_PRG          = ""
DFLT_NCLUSTERS    = 200
PEPDOCK_DFLT_RADIUS = 15.

DFLT_MAX_PEPFOLD_CENTROIDS   = 20
DFLT_PEP_SITEFINDER_DENSITY  = 10.
DFLT_PEP_SITEFINDER_DENSITY  = None
DFLT_PEP_SITEFINDER_DISTANCE = "3.0x"

DFLT_MAX_PROPENSITY_MODELS = 50 # Max models used to analyze patch propensities

DFLT_PEPTIDE_MAX_SIZE        = 36
DFLT_PEPTIDE_MIN_SIZE        = 4

# Oscar-star library (if using oscar-star)
MULUL="/usr/local/OSCAR/library/z/" # oscar-star library path

# JMOL APPLET CONFIG ...
JMOLPATH = "/portal/applets/jmol"

# Pymol color palette
PML_COL = ["marine","magenta","limegreen","dash","br8",
            "lightblue","cyan","lightpink","purpleblue","sulfur",
            "oxygen","deepteal","chocolate","br4","lightteal"]

DFLT_NPROC = 8
