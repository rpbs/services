#!/usr/bin/env python
"""
Combine Apollo / CS RMS data
./doCSRMSdAnalysis.py Apollo-results.txt TMCSResultsHA.tmscore  TMCSResultsHN.tmscore 

./doCSRMSdAnalysis.py Apollo/2zaj-clusters.txt 2zaj/TMCSP_HA.data 2zaj/TMCSP_HN.data target
"""
import sys
import os, subprocess

# import PyGreedy.PyGreedy as Greedy
# import PyBestFit.PyBestFit as PyBestFit
import PyPDB.PyPDB as PDB


def linkToBestModels(clusterFile, maxBest = 10, verbose = False):
    f = open(clusterFile)
    lines = f.readlines()
    f.close()

    isOpened = False
    models = []
    for l in lines:
        if l.count("# cluster "):
            clusterNumber = int(l.split()[2]) + 1
            isOpened = True
            continue
        if isOpened:
            isOpened = False
            fname = l.split()[0]
            os.system("ln %s model%d.pdb" % (fname, clusterNumber))
            maxBest = maxBest - 1
            if maxBest <= 0:
                break
            continue

def clusterAppendRMSd(lines, ref, refZones = None, verbose = 0):
    """
    Append to lines in the clusters format the RMSd value
    @param lines   : the clusters.txt lines
    @param ref     : the reference PDB
    @param refZones: the correspondance between model and ref if needed
    """
    if verbose:
        sys.stderr.write("Will append RMSd information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   RMSd\n" % (l[:-1]))
            else:
                rs.append(l)
        elif not l.count("-SC-min.pdb"):
            pass # The reference should not be in the clusters
            rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            # rmsd = pairRMSd(ref, mfile, mobiZones = [[4,30]], verbose = 0)
            if not refZones:
                rmsd = pairRMSd(ref, mfile, verbose = 0)
            else:
                sZones = PyBestFit.parseUsrDefZn(refZones, verbose = 0)
                rmsd = pairRMSd(ref, mfile, refZones = sZones['ref'], mobiZones = sZones['mobi'], verbose = 0)
            rs.append("%s %6.2f\n" % (l[:-1],float(rmsd)))
    return rs

def clusterAppendTMSPRMSd(lines, ref, refZones = None, verbose = 0):
    """
    Append to lines in the clusters format the RMSd value
    @param lines   : the clusters.txt lines
    @param ref     : the reference PDB
    @param refZones: the correspondance between model and ref if needed
    """
    import TMScore
    if verbose:
        sys.stderr.write("Will append RMSd information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   RMSd TMscore SPscore GDT_TS   BCscore\n" % (l[:-1]))
            else:
                rs.append(l)
        elif not l.count("-SC-min.pdb"):
            pass # The reference should not be in the clusters
            rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            # rmsd = pairRMSd(ref, mfile, mobiZones = [[4,30]], verbose = 0)
            if not refZones:
                tmscore, rmsd, gdtts, spa, bc = TMScore.run(ref, mfile)
            else:
                sZones = PyBestFit.parseUsrDefZn(refZones, verbose = 0)
                # print sZones['ref'], sZones['mobi']
                x = PDB.PDB(ref, altCare = 1)
                y = PDB.PDB(mfile, altCare = 1)
                xz = PyBestFit.PDBZones(x, sZones['ref'])
                yz = PyBestFit.PDBZones(y, sZones['mobi'])
                xz.renumber(1)
                yz.renumber(1)
                xz.out("%s.zone" % ref)
                yz.out("%s.zone" % mfile)
                # sys.exit(0)
                tmscore, rmsd, gdtts, spa, bc = TMScore.run("%s.zone" % ref, "%s.zone" % mfile, verbose = verbose)
            rs.append("%s %6.2f %7.2f %7.2f %6.2f %8.6f\n" % (l[:-1],float(rmsd),float(tmscore),float(spa),float(gdtts),float(bc) ))
    return rs


def clusterAppendBC(lines, ref, refZones = None, verbose = 0):
    """
    Append to lines in the clusters format the RMSd value
    @param lines   : the clusters.txt lines
    @param ref     : the reference PDB
    @param refZones: the correspondance between model and ref if needed
    """
    import TMScore
    if verbose:
        sys.stderr.write("Will append RMSd information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   BCscore\n" % (l[:-1]))
            else:
                rs.append(l)
        elif not l.count("-SC-min.pdb"):
            pass # The reference should not be in the clusters
            rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            # rmsd = pairRMSd(ref, mfile, mobiZones = [[4,30]], verbose = 0)
            if not refZones:
                tmscore, rmsd, gdtts, spa, bc = TMScore.run(ref, mfile, verbose = verbose)
            else:
                sZones = PyBestFit.parseUsrDefZn(refZones, verbose = 0)
                # print sZones['ref'], sZones['mobi']
                x = PDB.PDB(ref, altCare = 1)
                y = PDB.PDB(mfile, altCare = 1)
                xz = PyBestFit.PDBZones(x, sZones['ref'])
                yz = PyBestFit.PDBZones(y, sZones['mobi'])
                xz.renumber(1)
                yz.renumber(1)
                xz.out("%s.zone" % ref)
                yz.out("%s.zone" % mfile)
                # sys.exit(0)
                tmscore, rmsd, gdtts, spa, bc = TMScore.run("%s.zone" % ref, "%s.zone" % mfile, verbose = verbose)
            rs.append("%s %8.6f\n" % (l[:-1],float(bc) ))
    return rs

def greedyEne(PDBFN, ffrom = 0, tto = -1, SSBonds = None, Neighbours = None, verbose = False):
    i_options = {"i3DPDB" : PDBFN, "modAna": True, "wPath": ".", "ffrom" : ffrom, "tto": tto, "verbose": True, "SSBonds": SSBonds, "neighbours": Neighbours}
    g = Greedy.Greedy(args = ["Greedy","-i",PDBFN], options = i_options)
    g.start()
    # print g
    out, log = g.greedy_run(verbose=verbose)
    # print out
    # print log
    ene = g.modAnaParse( out )
    return ene['Etot'], ene["EConstr"]

def clusterAppendEne(lines, SSBonds = None, Neighbours = None, verbose = 0):
    """
    Append to lines in the clusters format the energy value
    @param SSBonds: specify SSbonds if any (PEPFOLD format)
    @param Neighbours: specify Neighbours if any
    """
    if verbose:
        sys.stderr.write("Will append sOPEP energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s  sOPEP  CGCNS\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            rmsd, cnstr = greedyEne(mfile, SSBonds = SSBonds, Neighbours = Neighbours, verbose = verbose)
            rs.append("%s %6.2f  %5s\n" % (l[:-1],float(rmsd), cnstr))
    return rs

def cppBuilderEne(PDBFN, ffrom = 0, tto = -1, SSBonds = None, Neighbours = None, verbose = False):
    """
cppBuilderEne("silk4-3-4-00007_bestene1-mc-min.pdb")
    """
    popened = subprocess.Popen(["/home/tuffery/Work/prgs/Src/S4-dev/CppBuilder", "--objective", "sopep", "--action", "ecalc", "--iW", "NONE", "--sOPEP1", "--noTabEne", "--i3D" , PDBFN], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    comm = popened.communicate()
    out = comm[0]
    err = comm[1]
    # print(out)
    # print(err)
    isopen = 0
    for l in err.split("\n"):
        # print(l)
        if l.count("Detailed full energy"):
            isopen=1
        elif isopen == 2:
            # print(l)
            return float(l.split()[-1])
        elif isopen == 1:
            isopen=2
    return 0.

def clusterAppendCppBuilderEne(lines, label= "cppsOPEP", SSBonds = None, Neighbours = None, verbose = 0):
    """
    Append to lines in the clusters format the energy value
    @param SSBonds: specify SSbonds if any (PEPFOLD format)
    @param Neighbours: specify Neighbours if any


python
import clusterTools as ct

f = open("silk4-3-4_clusters.txt")
lines = f.readlines()
f.close()

ll = ct.clusterAppendCppBuilderEne(lines)
ll2 = clustersSortBy(ll, "cppsOPEP")
    """
    if verbose:
        sys.stderr.write("Will append sOPEP energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s  %s\n" % (l[:-1], label))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0].replace(".pdb","-min.pdb")
            ene = cppBuilderEne(mfile, SSBonds = SSBonds, Neighbours = Neighbours, verbose = verbose)
            rs.append("%s %6.2f \n" % ( l[:-1],float(ene) ) )
    return rs

def clusterAppendCppBuilderEneFromEneFile(lines, label= "minsOPEP", SSBonds = None, Neighbours = None, verbose = 0):
    """
    Append to lines in the clusters format the energy value
    @param SSBonds: specify SSbonds if any (PEPFOLD format)
    @param Neighbours: specify Neighbours if any


python
import clusterTools as ct

f = open("silk4-3-4_clusters.txt")
lines = f.readlines()
f.close()

ll = ct.clusterAppendCppBuilderEne(lines)
ll2 = clustersSortBy(ll, "cppsOPEP")
    """
    if verbose:
        sys.stderr.write("Will append minsOPEP energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s  %s\n" % (l[:-1], label))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0].replace(".pdb","_SC-min.ene") # ene file
            f=open(mfile)
            flines = f.readlines()
            f.close()
            for fl in flines:
                if fl.count("sOPEP Energy:"):
                    ene = fl.split()[2]
                    rs.append("%s %s \n" % ( l[:-1].replace(".pdb","_SC-min.pdb"), ene ) )
                    break
    return rs

def clusterAppendLikelihood(lines, verbose = 0):
    """
    Append likelihood to a PDB file
    """
    import PySAProbs.PySAProbs as PySAProbs
    import PySAProbs.PySATrj as PySATrj

    if verbose:
        sys.stderr.write("Will append CSRMS energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s    Lkl  \n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            trjfile = mfile.replace("-SC-min.pdb",".trj")
            probfile = "%s.svmi8.27.prob" % mfile.split("-")[0]
            args = ["SAProbs","-i",probfile]
            x = PySAProbs.SAProbs(args = args)
            x.numProbs()
            t = PySATrj.Trj(trjfile)
            L = t.likelihood(x.np)
            rs.append("%s %f\n" % (l[:-1],L))
    return rs

def clustersAppendCSRMS(lines, verbose = 0):
    """
    We append HNCSRMS to the end of lines
    """
    if verbose:
        sys.stderr.write("Will append CSRMS energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s  HNCS  HACS\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            mfile = l.split()[0]
            spfile = mfile.replace(".pdb",".spartap")
            # print "Appending to %s" % mfile.replace(".pdb",".spartap")
            f = open(spfile)
            cslines = f.readlines()
            f.close()
            csrms = "100."
            csharms = "100."
            for csl in cslines:
                if csl.count("DATA CS_STAT_INFO  HN RMS:"):
                    csrms = csl.split()[4]
                if csl.count("DATA CS_STAT_INFO  HA RMS:"):
                    csharms = csl.split()[4]
            #csrms = spartaCSRMS(mfile.replace(".pdb",".spartap"))
            rs.append("%s %s %s\n" % (l[:-1],csrms, csharms))
    return rs
    

def clustersAppendCSSIGMA(lines, verbose = 0):
    """
    We append HNCS sigma inforamtion to the end of lines
    """
    if verbose:
        sys.stderr.write("Will append CSRMS energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   M1    M2   MA1   MA2\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            model = it[0].split(".")[0]
            mfile = l.split()[0]
            spfile = mfile.replace(".pdb",".spartap")
            hnlines = parseSpartapSigmas("%s.spartap" % (model),  verbose = verbose)
            halines = parseSpartapSigmas("%s.spartap" % (model), atmNames = [" HA "], verbose = verbose)
            # q1, q2, m1, m2 = sigmaQuality(hnlines, seuil=0.5)
            # qa1, qa2, ma1, ma2 = sigmaQuality(halines, seuil=0.28)
            q1, q2, m1, m2 = sigmaQuality(hnlines, seuil=0.48)
            qa1, qa2, ma1, ma2 = sigmaQuality(halines, seuil=0.26)
            rs.append("%s %.3f %.3f %.3f %3.f\n" % (l[:-1], m1, m2, ma1, ma2))
    return rs
    
def clustersAppendFSTM(lines, rclines, verbose = 0):
    """
    We append Full Structure TMscore at the end of lines
    """
    if verbose:
        sys.stderr.write("Will append FS TMscore information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   FSTM\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            model = it[0].split(".")[0]
            mfile = l.split()[0]
            for rcl in rclines:
                if rcl.count(model):
                    itt = rcl.split()
                    RCTM = itt[1]
                    break
            rs.append("%s %s\n" % (l[:-1], RCTM))
    return rs
    
def clustersAppendRCTM(lines, rclines, verbose = 0):
    """
    We append RC TMscore at the end of lines
    """
    if verbose:
        sys.stderr.write("Will append CSRMS energy information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   RCTM\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            model = it[0].split(".")[0]
            mfile = l.split()[0]
            for rcl in rclines:
                if rcl.count(model):
                    itt = rcl.split()
                    RCTM = itt[1]
                    break
            rs.append("%s %s\n" % (l[:-1], RCTM))
    return rs
    

def clustersAppendS2TM(lines, rclines, verbose = 0):
    """
    We append S2 TMscore at the end of lines
    """
    if verbose:
        sys.stderr.write("Will append S2TM information\n")
    rs = []
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                rs.append("%s   S2TM\n" % (l[:-1]))
            else:
                rs.append(l)
        else:
            it = l.split()
            if not len(it):
                rs.append(l)
                continue
            model = it[0].split(".")[0]
            mfile = l.split()[0]
            for rcl in rclines:
                if rcl.count(model):
                    itt = rcl.split()
                    RCTM = itt[1]
                    break
            rs.append("%s %s\n" % (l[:-1], RCTM))
    return rs
    

def cmpFunc(a,b):
    return a["lowestE"] - b["lowestE"]

def keyIndex(lines, sortKey):
    """
    return the index of the information associated with the sKey
    """
    for l in lines:
        if l.count("# file "):
            # Incorrect key: do nothing
            if sortKey not in l.split():
                return None
            else:
                it = l.split()
                sortKeyPos = l.split().index(sortKey) - 1  # minus 1 since "#"at beginning of line  
                return sortKeyPos
    return None

def clustersSortBy(lines, sortKey, verbose = False):
    """
    Sort cluster lines using a valid key (String)
    @param lines: the linbes of the cluster file
    @param sortKey: the key to use for sorting
    @return : the lines sorted 

    # file    TMalign      avg      gdt      max        q       tm    RMSd  sOPEP
    1jbl.pdb    0.268    0.561    0.742    0.676    0.517    0.308    3.08  -8.38
f = open("clusters.txt")
lines = f.readlines()
f.close()
lrs = clustersSortBy(lines, "sOPEP")
print "".join(lrs)
from operator import itemgetter
rs2 = sorted(rs, key=itemgetter("lowestE")) 
for i in rs2:
    print i["lowestE"]

    Allowed keys are:
    RMSd  sOPEP TMScore
    """
    header = []
    tailer = []
    isOpened = False
    isTailer = False
    clusters = []
    cluster = None
    reverse = False
    if sortKey in ("TMalign", "tm", "gdt", "q", "RCTM", "FSTM", "S2TM", "BCscore", "r_BCscore", "ExpBCscore", "TMscore","GDT_TS", "SPscore"):
        reverse = True
    weight = 1.
    if sortKey == "Lkl":
        weight = -1.
    for l in lines:
        if not isOpened:
            if l.count("# file  "):
                header.append(l)
                # Incorrect key: do nothing
                if sortKey not in l.split():
                    return lines
                else:
                    it = l.split()
                    sortKeyPos = l.split().index(sortKey) - 1  # minus 1 since "#"at beginning of line  
                    if verbose:
                        sys.stderr.write("clustersSortBy: key %s index %d reverse %s\n" % (sortKey, sortKeyPos,str(reverse)))
                isOpened = True
                continue
            else:
                header.append(l)
        else: # isOpened
            if l.count("# cluster"):
                if cluster:
                    clusters.append(cluster)
                it = l.split()
                clsName = it[2]
                isTailer = False
                cluster = {"header": l, 
                           "lines" : [], 
                           "lowestE" : 100000000., 
                           # "lowestRMSd" : 100000., 
                           # "lowestConstrainE": 10000000000.,
                           }
                if reverse:
                    cluster["lowestE"] = 0.
            else:
                if not len(l.split()):
                    isTailer = True
                    tailer.append(l)
                    continue
                if (l[0] == "#") and (not l.count("# cluster")):
                    tailer.append(l)
                    isTailer = True
                    continue
                if isTailer:
                    tailer.append(l)
                    continue
                cluster["lines"].append(l)
                it = l.split()
                if not reverse:
                    if weight * float(it[sortKeyPos]) < cluster["lowestE"]:
                        cluster["lowestE"] = weight * float(it[sortKeyPos])
                else:
                    if weight * float(it[sortKeyPos]) > cluster["lowestE"]:
                        cluster["lowestE"] = weight * float(it[sortKeyPos])
    clusters.append(cluster)
    from operator import itemgetter
    rs = sorted(clusters, key=itemgetter("lowestE"), reverse = reverse) 
    
    # for r in rs:
    #     print r["lowestE"]

    lrs = header
    for i,r in enumerate(rs):
        lrs.append("# cluster %d - Size: %d\n" % (i, len(r["lines"])))
        # We sort the lines either
        if len(r["lines"]) > 1:
            r["lines"] = sorted(r["lines"], key=lambda l: float(l.split()[sortKeyPos]), reverse = reverse)
        # lrs.append("".join(r["lines"]))
        lrs = lrs + r["lines"]
    lrs = lrs + tailer
    # Now we reset the lines
    return lrs

def nBestClustersModels_old(aplines, maxCluster = 10, maxModelsPerCluster = 5, maxModels = 30, minScannedModels = 20):
    """
    @param aplines             : Apolo clusters lines
    @param maxModels           : the maximum number of models returned
    @param maxModelsPerCluster : the maximum number of models returned  per cluster
    @param minScannedModels    : the minimum number of models scanned
    @return                    : list of models within the N best clusters
    """
    cluster = 0
    rs = []
    nScanned = 0
    for l in aplines:
        it = l.split()
        if not len(it):
            continue
        if l.count("cluster"):
            nmodel = 0
            cluster = it[2]
            if int(cluster) >= maxCluster and (nScanned >= minScannedModels):
                break
            continue
        if l[0] == "#":
            continue
        # 2zaj-47_bestene1mc-SC-min.pdb     0.431    0.274    0.362    0.266    0.217    0.250
        model = it[0].split(".")[0]
        nmodel += 1
        nScanned += 1
        # print model, nmodel, maxModelsPerCluster
        if nmodel <= maxModelsPerCluster:
            rs.append(model)
        if (len(rs) == maxModels) and (nScanned >= minScannedModels):
            break
    return rs

def nBestClusters(aplines, maxCluster = 10):
    """
    @param aplines : Apolo clusters lines
    @param maxModels : the maximum number of models returned
    @return        : lines for the n best clusters
    """
    cluster = 0
    rs = []
    for l in aplines:
        if l.count("cluster"):
            it = l.split()
            cluster = int(it[2])
            if cluster == maxCluster:
                break
        rs.append(l)
    return rs


def nBestClustersModels(aplines, maxCluster = 10, maxModelsPerCluster=1):
    """
    @param aplines : Apolo clusters lines
    @param maxCluster : the maximum number of clusters returned.
    @param maxModels : the maximum number of models returned per cluster
    @return        : lines for the n best clusters
    """
    cluster = 0
    rs = []
    for l in aplines:
        if l.count("cluster"):
            nModels = 0
            it = l.split()
            cluster = int(it[2])
            if cluster == maxCluster:
                break
        if l[0] == "#":
            rs.append(l)
            continue
        nModels += 1
        if nModels <= maxModelsPerCluster:
            rs.append(l)
    return rs



def countModels(aplines):
    """
    return a subset of models matching condition that what CS RMS less than seuil
    @param aplines: liens off the clusters.txt of Apollo
    @return       : number of clusters and number of models in lines
    """
    clusters = models = 0
    for l in aplines:
        it = l.split()
        if not len(it):
            continue
        if l.count("cluster"):
            clusters += 1
            continue
        if l[0] == "#":
            continue
        # 2zaj-47_bestene1mc-SC-min.pdb     0.431    0.274    0.362    0.266    0.217    0.250
        models += 1
    return clusters, models

def modelLines(lines, gHNSeuil):
    rs = []
    for l in lines:
        it = l.split()
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        model  = it[0].split(".")[0]
        target = model.split("-")[0]
        hn = float(it[7])
        ha = float(it[8])
        m1 = float(it[9])
        m2 = float(it[10])
        tm    = it[1]
        ptm   = it[6]
        RCTM  = it[13]

        # Original formulation
        # rs.append("%s %s %s %s %s %s %s %s %f %s %s" % (target, model, tm, ptm, ha, hn, m1, m2, float(hn) - gHNSeuil + float(m1), RCTM, cluster))

        # Normalized formulation
        rs.append("%s %s %s %s %s %s %s %s %f %s %s" % (target, model, tm, ptm, (ha - 0.31) / 0.27, (hn - 0.48) / 0.27, m1 / 0.072, m2 / (1.41 *0.072), ((hn - 0.48) / 0.27) + (m1 / 0.072) + (m2 / (1.41 *0.072)), RCTM, cluster))
        # rs.append(l)
    return rs

def clusterRank(lines, sortKey="sOPEP", verbose = 0):
    """
    Find the rank of the solution with best criterion 
    """
    rs = []
    keyPos = None
    for l in lines:
        if l[0] == '#':
            if l.count("# file  "):
                keyPos = l.split().index(sortKey) - 1
                if verbose:
                    sys.stderr.write("Key %s is column %d\n" % (sortKey, keyPos))
                break

    # Sort and find the best model according to sort
    slines = clustersSortBy(lines, sortKey)
    for l in slines:
        it = l.split()
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l
        model  = it[0].split(".")[0]
        value  = it[keyPos]
        break
    if verbose:
        sys.sterr.write( "best model according to %s is %s" % (sortKey, model))

    # Locate best model into original ranking
    for l in lines:
        it = l.split()
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        if l.count(model):
            return model, cluster, value, it[-1], it[-2]
            break
    return None, None, None, None, None


def main2(argv):
    """
    return best RCTM in x first clusters
    """
    target = argv[1]
    zone   = argv[2]
    verbose = True

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    # Append sOPEP, RMSd, RCTM
    aplines = clusterAppendEne(aplines)
    aplines = clustersAppendFSTM(aplines, fslines)
    aplines = clustersAppendRCTM(aplines, rclines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1])
    aplines = nBestClusters(aplines, maxCluster = 10)
    aplines = clustersSortBy(aplines, "RCTM")
    # print "".join(aplines)
    # aplines = nBestClusters(aplines, maxCluster = 10)
    for l in aplines:
        it = l.split()
        if l.count("RCTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        model  = it[0].split(".")[0]
        # print model, it[-1]
        
        itt = zone.replace("::",":").replace("|",":").split(":")
        tmmodelFile = "%s-%s-%s.pdb" % (model, itt[2],itt[-1])
        # Get information about coverage
        cmd = "grep 'Number of residues in common=' %s.tmscore  | cut -d'=' -f2 "  % tmmodelFile
        # print cmd
        nmatch = os.popen(cmd).readlines()
        nmatch = nmatch[0].split()[0]
        cmd = "grep 'Superposition in the TM-score' %s.tmscore | cut -d'=' -f2 | cut -d'R' -f1" % tmmodelFile
        # print cmd
        naln = os.popen(cmd).readlines()
        naln = naln[0].split()[0]
        # print model, nmatch, naln
        print(model, it[-1], float(naln) / float(nmatch))
        break
    return

def main2FS(argv):
    """
    return best FSTM in x first clusters
    """
    target = argv[1]
    verbose = True

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    # Append sOPEP, RMSd, RCTM
    aplines = clusterAppendEne(aplines)
    aplines = clustersAppendRCTM(aplines, rclines)
    aplines = clustersAppendFSTM(aplines, fslines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1])
    aplines = nBestClusters(aplines, maxCluster = 10)
    aplines = clustersSortBy(aplines, "FSTM")
    # print "".join(aplines)
    # aplines = nBestClusters(aplines, maxCluster = 10)
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        model  = it[0].split(".")[0]
        
        # Get information about coverage
        cmd = "grep 'Number of residues in common=' %s-aafrg.pdb.tmscore  | cut -d'=' -f2 "  % model
        nmatch = os.popen(cmd).readlines()
        nmatch = nmatch[0].split()[0]
        # print cmd
        cmd = "grep 'Superposition in the TM-score' %s-aafrg.pdb.tmscore | cut -d'=' -f2 | cut -d'R' -f1" % model
        # print cmd
        naln = os.popen(cmd).readlines()
        naln = naln[0].split()[0]
        # print model, nmatch, naln
        print(model, it[-1], float(naln) / float(nmatch))
        
        break
    return

def main2RCFS(argv):
    """
    return best RCTM and best FSTM in x first clusters
    """
    target = argv[1]
    verbose = True
    nClusters = int(argv[2])

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    # Append sOPEP, RMSd, RCTM
    # aplines = clusterAppendEne(aplines)
    aplines = clustersAppendRCTM(aplines, rclines)
    aplines = clustersAppendFSTM(aplines, fslines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1])
    aplines = nBestClusters(aplines, maxCluster = nClusters)
    aplines = clustersSortBy(aplines, "FSTM")
    # print "".join(aplines)
    # aplines = nBestClusters(aplines, maxCluster = 10)
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        FSmodel  = it[0].split(".")[0]
        FS       = it[-1]
        break

    aplines = clustersSortBy(aplines, "RCTM")
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        RCmodel  = it[0].split(".")[0]
        RC       = it[-2]
        break
    print(RCmodel, RC, FSmodel, FS)
    return

def main3(argv):
    """
    rank of lowest Ene
    """
    target = argv[1]
    verbose = True

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    f = open("TMS2BFResults.TMscore")
    s2lines = f.readlines()
    f.close()

    # Append sOPEP, RMSd, RCTM
    aplines = clusterAppendEne(aplines)
    aplines = clustersAppendFSTM(aplines, fslines)
    aplines = clustersAppendRCTM(aplines, rclines)
    aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1], refZones = argv[2])
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = nBestClusters(aplines, maxCluster = 200)
    # aplines = clustersSortBy(aplines, "sOPEP")
    # aplines = nBestClusters(aplines, maxCluster = 10)
    model, cluster, ene, RMSd, RCTM = clusterRank(aplines, sortKey="sOPEP")
    # clusterRank(aplines, sortKey="tm")
    # Get information about coverage
    cmd = "grep 'Number of residues in common=' %s-aafrg.pdb.tmscore  | cut -d'=' -f2 "  % model
    nmatch = os.popen(cmd).readlines()
    nmatch = nmatch[0].split()[0]
    # print cmd
    cmd = "grep 'Superposition in the TM-score' %s-aafrg.pdb.tmscore | cut -d'=' -f2 | cut -d'R' -f1" % model
    # print cmd
    naln = os.popen(cmd).readlines()
    naln = naln[0].split()[0]
    # print model, nmatch, naln
    print(model, cluster, ene, RMSd, RCTM, float(naln) / float(nmatch))

def main4(argv):
    """
    return the RCTM and FSTM of the N best models representative of the N best clusters (as in blind CASP prediction).
    """
    target    = argv[1]
    verbose   = True
    nClusters = int(argv[2])
    nModels   = int(argv[3])
    sKey      = argv[4]
    refZones  = argv[5]

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    # Append sOPEP, RMSd, RCTM
    # aplines = clusterAppendEne(aplines)
    aplines = nBestClusters(aplines, maxCluster = 10)
    aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1], refZones = refZones)
    aplines = clustersAppendRCTM(aplines, rclines)
    aplines = clustersAppendFSTM(aplines, fslines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    if sKey != "None":
        # aplines = clustersSortBy(aplines, "sOPEP")
        aplines = clustersSortBy(aplines, sKey)

    # print "".join(aplines)    # aplines = nBestClusters(aplines, maxCluster = 10)
    # models = nBestClustersModels_old(aplines, maxCluster = 10, maxModelsPerCluster = 1, maxModels = 30, minScannedModels = 0)
    aplines = nBestClustersModels(aplines, maxCluster = nClusters, maxModelsPerCluster = nModels)
    # print "".join(models)
    cmd = "cd %s; pymol %s.pdb " % (target, target)
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        model  = it[0].split(".")[0]
        
        # print model, nmatch, naln
        print(model, it[-3], it[-2], it[-1])
        
        cmd = "%s %s.pdb " % (cmd, model)
        # break
    sys.stderr.write("%s\n" % cmd)
    return

def mainValueinNBest(argv):
    """
    return the best criterion value (e.g. BCscore) of the N best models representative of the N best clusters (as in blind CASP prediction).
    implies the clusters.txt compatible file already has the information supplied.
    1. Sort clusters according to sort key
    2. Truncate to n best clusters
    3. Get the best value in the N best clusters, up to max rank
    Format is:
    clusterTools clusters.txt sOPEP 5 200 BCscore # 5 is n clusters, 200 is maxRank
    """
    target    = argv[1]
    verbose   = True
    s1Key      = argv[2]
    nClusters = int(argv[3])
    maxRank   = int(argv[4])
    s2Key      = argv[5]

    # We load the clusters
    f = open("%s" % target)
    aplines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    if s1Key != "None":
        # aplines = clustersSortBy(aplines, "sOPEP")
        aplines = clustersSortBy(aplines, s1Key)

    # Append sOPEP, RMSd, RCTM
    # aplines = clusterAppendEne(aplines)
    aplines = nBestClusters(aplines, maxCluster = nClusters)
    # aplines = clustersAppendS2TM(aplines, s2lines)

    # print "".join(aplines)    # aplines = nBestClusters(aplines, maxCluster = 10)
    # models = nBestClustersModels_old(aplines, maxCluster = 10, maxModelsPerCluster = 1, maxModels = 30, minScannedModels = 0)
    aplines = nBestClustersModels(aplines, maxCluster = nClusters, maxModelsPerCluster = maxRank)
    aplines = clustersSortBy(aplines, s2Key)
    # print "".join(aplines)
    keyPos      = keyIndex(aplines, s2Key)
    mainKeyPos  = keyIndex(aplines, s1Key)
    isOpen  = False
    for l in aplines:
        it = l.split()
        if l.count("# file"):
            pass
        if l.count("cluster"):
            isOpen = True
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        if isOpen:
            # print l,
            # print l.split()[keyPos]
            print(l.split()[0], l.split()[mainKeyPos], l.split()[keyPos])
            return
    return

def mainL(argv):
    target = argv[1]
    verbose = True
    nClusters = int(argv[2])

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    # f = open("TMS2BFResults.TMscore")
    # s2lines = f.readlines()
    # f.close()

    # Append sOPEP, RMSd, RCTM
    # aplines = clusterAppendEne(aplines)
    aplines = clustersAppendRCTM(aplines, rclines)
    aplines = clustersAppendFSTM(aplines, fslines)
    aplines = clusterAppendLikelihood(aplines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1])
    # aplines = nBestClusters(aplines, maxCluster = nClusters)
    aplines = clustersSortBy(aplines, "Lkl")
    print("".join(aplines))
    # print "".join(aplines)
    # aplines = nBestClusters(aplines, maxCluster = 10)
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        FSmodel  = it[0].split(".")[0]
        FS       = it[-1]
        break

    aplines = clustersSortBy(aplines, "RCTM")
    for l in aplines:
        it = l.split()
        if l.count("FSTM"):
            # print l,
            pass
        if l.count("cluster"):
            cluster = it[2]
            nViewedPerClust = 0
            continue
        if l[0] == '#':
            continue
        if not len(it):
            continue
        # print it
        # print l,
        RCmodel  = it[0].split(".")[0]
        RC       = it[-2]
        break
    print(RCmodel, RC, FSmodel, FS)
    return

def main(argv):
    target = argv[1]
    verbose = True

    # We load the clusters
    f = open("../Apollo/%s-clusters.txt" % argv[1])
    aplines = f.readlines()
    f.close()

    # FSTM information
    f = open("TMFULLResults.TMscore")
    fslines = f.readlines()
    f.close()

    # RCTM information
    f = open("TMRCBFResults.TMscore")
    rclines = f.readlines()
    f.close()

    # S2TM information
    f = open("TMS2BFResults.TMscore")
    s2lines = f.readlines()
    f.close()

    # Append sOPEP, RMSd, RCTM
    aplines = clusterAppendEne(aplines)
    aplines = clustersAppendFSTM(aplines, fslines)
    aplines = clustersAppendRCTM(aplines, rclines)
    # aplines = clustersAppendS2TM(aplines, s2lines)
    # aplines = clusterAppendRMSd(aplines, "%s.pdb" % argv[1])
    # aplines = nBestClusters(aplines, maxCluster = 200)
    aplines = clustersSortBy(aplines, "sOPEP")
    # aplines = nBestClusters(aplines, maxCluster = 10)
    clusterRank(aplines, sortKey="RCTM")
    # clusterRank(aplines, sortKey="tm")

    f = open("clusters.allinfo.txt","w")
    for l in aplines:
        f.write("%s" % l)
    f.close()
    return

    # We append CSRMS information to maxClusters
    aplines = clustersAppendCSRMS(aplines)
    aplines = clustersAppendCSSIGMA(aplines)

    # Now we filter the lines iteratively
    gM1Seuil = 0.21
    gM2Seuil = 0.27
    M1Seuil  = 0.21
    M2Seuil  = 0.27
    gHASeuil = 0.32
    gHNSeuil = 0.48
    HASeuil  = 0.50
    HNSeuil  = 0.48

    aplines = modelLines(aplines, gHNSeuil)
    print("\n".join(aplines))
    return


def comboAddInformation(clusterFile, refFile = None, refZones = None, verbose = False):

    # We load the clusters
    f = open("%s" % clusterFile)
    aplines = f.readlines()
    f.close()

    refZones = None
    try:
        refZones = argv[3]
    except:
        pass

    # Append sOPEP, RMSd, TMscore, SPscore, GDT_TS
    aplines = clusterAppendEne(aplines)
    if refFile is not None:
        aplines = clusterAppendTMSPRMSd(aplines, argv[2], refZones = refZones, verbose = False) # Now includes BCscore as well

    # Append BCscore
    # aplines = clusterAppendBC(aplines, argv[2], refZones = refZones, verbose = True)

    f = open("%s" % clusterFile, "w")
    f.write("%s" % "".join(aplines))
    f.close()
    return

    
def mainCombo(argv):
    target = argv[1]
    verbose = True

    # We load the clusters
    f = open("%s" % argv[1])
    aplines = f.readlines()
    f.close()

    refZones = None
    try:
        refZones = argv[3]
    except:
        pass

    # Append sOPEP, RMSd, TMscore, SPscore, GDT_TS
    aplines = clusterAppendEne(aplines)
    aplines = clusterAppendTMSPRMSd(aplines, argv[2], refZones = refZones, verbose = False) # Now includes BCscore as well

    # Append BCscore
    # aplines = clusterAppendBC(aplines, argv[2], refZones = refZones, verbose = True)

    f = open("%s" % argv[1], "w")
    f.write("%s" % "".join(aplines))
    f.close()
    return

def mainSortBy(argv):
    target = argv[1]
    verbose = True

    # We load the clusters
    f = open("%s" % argv[2])
    aplines = f.readlines()
    f.close()

    sKey = argv[3]
    # Append sOPEP, RMSd, TMscore, SPscore, GDT_TS
    aplines = clustersSortBy(aplines, sKey)

    # Append BCscore
    # aplines = clusterAppendBC(aplines, argv[2], refZones = refZones, verbose = True)

    print("".join(aplines))
    # f = open("%s.%s.sort" % (argv[1], sKey, "w")
    # f.write("%s" % "".join(aplines))
    # f.close()
    return

def getNBestCentroids(clusterFile, maxCluster = 20, maxRank = 1, verbose = False):
    # We load the clusters
    f = open(clusterFile)
    aplines = f.readlines()
    f.close()

    s1Key      = "sOPEP"

    if s1Key != "None":
        # aplines = clustersSortBy(aplines, "sOPEP")
        aplines = clustersSortBy(aplines, s1Key)

    aplines = nBestClustersModels(aplines, maxCluster = maxCluster, maxModelsPerCluster = maxRank)
    print("".join(aplines))

    f = open("%s.%s-%s-best.Ids" % (clusterFile, str(maxCluster),str(maxRank)), "w")
    for l in aplines:
        if l[0] == "#":
            continue
        f.write("%s\n" % l.split()[0])
    f.close()

def mainNBest(argv):
    """
    argv: Contains the prog name and switch (clusterTools -nbest). 
    """
    target = argv[2]
    verbose = True

    # We load the clusters
    f = open("%s" % argv[2])
    aplines = f.readlines()
    f.close()

    maxCluster = int(sys.argv[3])
    maxRank    = int(sys.argv[4])
    s1Key      = "sOPEP"

    if s1Key != "None":
        # aplines = clustersSortBy(aplines, "sOPEP")
        aplines = clustersSortBy(aplines, s1Key)

    aplines = nBestClustersModels(aplines, maxCluster = maxCluster, maxModelsPerCluster = maxRank)
    print("".join(aplines))

    f = open("%s.%s-%s-best.Ids" % (target, str(maxCluster),str(maxRank)), "w")
    for l in aplines:
        if l[0] == "#":
            continue
        f.write("%s\n" % l.split()[0])
    f.close()

if __name__ == "__main__":
    # main2RCFS(sys.argv)
    # mainL(sys.argv)
    # main4(sys.argv)
    if sys.argv[1] == "-sort":
        mainSortBy(sys.argv)
    elif sys.argv[1] == "-best":
        mainValueinNBest(sys.argv[1:])
    elif sys.argv[1] == "-nbest":
        mainNBest(sys.argv)
    else:
        mainCombo(sys.argv)
