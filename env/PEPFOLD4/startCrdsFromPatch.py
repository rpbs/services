#!/usr/bin/env python3
"""
This procedure to generate starting points from the protein and residue list.

./startCrdsFromPatch.py protein.pdb patch.txt
residues are specified using the convention:
_ChId_rName_rNum_ICode_ 
"""
import sys
import PyPDB.PyPDB as PDB
from PyPDB.Geo3DUtils import *
import random, math

def randomDirection():
    N     = 1000.
    count = 0
    while (N > 1.) and (count < 1000):
        x = -1+random.uniform(0.,1.)*2
        y = -1+random.uniform(0.,1.)*2
        z = -1+random.uniform(0.,1.)*2
        N = x*x + y*y + z*z
        count += 1
    N = math.sqrt(N)
    return x/N,y/N,z/N

def BC(dots):
    (x,y,z) = (0.,0.,0.)
    nAtm = 0.
    for aDot in dots:
        x = x + aDot[0]
        y = y + aDot[1]
        z = z + aDot[2]
        nAtm = nAtm + 1.
    x = x / nAtm
    y = y / nAtm
    z = z / nAtm
    return (x,y,z)

def has_clash(pdb, lx, ly, lz, minDist = 2.2):
    for aRes in pdb:
        for atm1 in aRes.atms:
            (x,y,z) = atm1.xyz()
            r = distance(x,y,z,lx, ly, lz)
            if r > 15.:
                continue
            if r < minDist:
                return True
    return False

def check_external(pdb, dirs, lx, ly, lz,  scale = 2.5, minDist = 3.7):
    nOK = 0
    for adir in dirs:
        dx = lx + (adir[0]*scale)
        dy = ly + (adir[1]*scale)
        dz = lz + (adir[2]*scale)
        if has_clash(pdb, dx, dy, dz, 3.7):
            continue
        nOK += 1
    if nOK < (len(dirs) / 3):
        return False
    # print "external: %d over %d" % (nOK, len(dirs))
    # for adir in dirs:
    #     dx = lx + (adir[0]*scale)
    #     dy = ly + (adir[1]*scale)
    #     dz = lz + (adir[2]*scale)
    #     print "ATOM     80  DU  CUX     6    %8.3f%8.3f%8.3f  1.00  0.00      LIGA" % (dx, dy, dz)
    return True
        
def minDistToList(out, lx, ly, lz, minDist = 5.):
    # Minimal distance to previous starting points accepted.
    for atm1 in out:
        if distance(lx,ly,lz,atm1[0],atm1[1],atm1[2]) < minDist:
            return False
    return True

def startCrdsFromPatchResidues(PDBFile, rList, minDist2Prot = 4.76, maxDist2Prot = 6., minDist2Point = 3., maxPoints = 10., BCscale = 2.5, extMinDist = 3.7, useBC =  True):
    
    out = []
    outlines = []

    # random directions
    dirs = []
    for atry in range(0,100):
        dx, dy, dz = randomDirection()
        if not minDistToList(dirs, dx, dy, dz, 0.55):
            continue
        dirs.append([dx, dy, dz])
        # outlines.append("ATOM     80  CU  CUX     6    %8.3f%8.3f%8.3f  1.00  0.00      LIGA" % (dx, dy, dz) )

    # return out, outlines

    # receptor
    pdb = PDB.PDB(PDBFile)  # The protein coordinates in the complex
    # CAx = x.select(awhat=["CA"])
    CAy = pdb.select(awhat=["CA"])

    patchCrds = []
    for l in rList:
        it = l.split("_")
        # print it
        chId = it[1]
        rName = it[2]
        rNum = it[3]
        rIcode = it[4]
        # print chId, rName, rNum, rIcode
        try:
            patchCrds.append( pdb.findAtm(chId, rName, rNum, rIcode).xyz()) # CA crds
        except:
            sys.stderr.write("Could not find information for %s" % l)
            sys.exit(0)
        # print pdb.findRes(chId, rName, rNum, rIcode, verbose = 1)
    bcx, bcy, bcz = BC(patchCrds)

    if len(patchCrds) == 0:
        sys.stderr.write("Sorry: No residue in %s\n" % sys.argv[2])
        sys.exit(0)

    # Now we generate coordinates to start PEPFOLD simulation.
    for i in range(0,1000):

        # Position in patch
        pos = random.randint(0, len(patchCrds)-1)
        # print "Pos:", pos, CAx[pos], dir(CAx[pos][0]), CAx[pos][0].xyz()
        px, py, pz = patchCrds[pos]
        dx, dy, dz = randomDirection()
        scaling = random.uniform(minDist2Prot, maxDist2Prot)
        if useBC:
            # from BC
            lx = bcx + (dx * scaling)
            ly = bcy + (dy * scaling)
            lz = bcz + (dz * scaling)
        else:
            # random
            lx = px + (dx * scaling)
            ly = py + (dy * scaling)
            lz = pz + (dz * scaling)
        # lx = px + random.uniform(2.,14.) - 8.   # BC  [-6 .. +6. ] each coordinate!
        # ly = py + random.uniform(2.,14.) - 8.
        # lz = pz + random.uniform(2.,14.) - 8.
        
        if not minDistToList(out, lx, ly, lz):
            continue

        if has_clash(pdb, lx, ly, lz):
            continue
        
        if not check_external(pdb, dirs, lx, ly, lz, scale = BCscale, minDist = extMinDist):
            continue
        
        outlines.append("ATOM     80  CU  CUX     6    %8.3f%8.3f%8.3f  1.00  0.00      LIGA" % (lx, ly, lz) )
        out.append([lx,ly,lz])
        
        if len(out) >= maxPoints:
            break
        
    return out, outlines


    

if __name__ == "__main__":
    # for i in range(0,10):
    #     print randomDirection()
        
    # sys.exit(0)
    
    # receptor
    y = PDB.PDB(sys.argv[1])  # The protein coordinates in the complex
    # CAx = x.select(awhat=["CA"])
    CAy = y.select(awhat=["CA"])


    # Patch
    f = open(sys.argv[2])
    lines = f.readlines()
    f.close()


    # out, outlines = startCrdsFromPatchResidues(sys.argv[1], lines, 4.6, 6., 3.)
    # out, outlines = startCrdsFromPatchResidues(sys.argv[1], lines, 5.5, 7., 5.)
    out, outlines = startCrdsFromPatchResidues(sys.argv[1], lines, 4.6, 7., 6.)
    if len(out) < 1:
         out, outlines = startCrdsFromPatchResidues(sys.argv[1], lines, 4.6, 7., 6., BCscale = 0.1, extMinDist = 2.2, useBC = True)
    if len(out) < 1:
         out, outlines = startCrdsFromPatchResidues(sys.argv[1], lines, 4.6, 7., 6., useBC = False)
    print("\n".join(outlines))
    sys.exit(0)
    
