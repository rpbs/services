#!/usr/bin/env python3

import pkg_resources

# ------------------------- Globals
VERSION = "4.0"
# -------------------------

PEPFOLD_DEMO      = pkg_resources.resource_filename("PEPFOLD4", "demo")

DFLT_MOBYLE_JOBS_PATH = "/data/jobs/PEP-FOLD4"

PEPFOLD_PROGRESS_FILE = ".progress.txt"

REMOTE_SERVER     = True  # If True, you can specify the remote python interpreter by setting the IPYTHON value

# The fst and pdb files should be copied into the PEPFOLD_ROOT
DFLT_DEMO_SEQUENCE = "1jbl.fst"
DFLT_DEMO_PDB      = "1jbl.pdb"
DFLT_DEMO_LABEL    = "1jbl"
#~ DFLT_DEMO_SSBONDS  = "3-11"
DFLT_DEMO_SSBONDS  = None

DFLT_DEMO_SEQUENCE = "3e21.fst"
DFLT_DEMO_PDB      = "3e21.pdb"
DFLT_DEMO_LABEL    = "3e21"


EXT_BIAS_VALUE     = 0.1

# 1AWR_apo-clean.pdb
# HAGPIA
#__ARG_54__,__PHE_59__,__MET_60__,__GLN_62__,__GLY_71__,__THR_72__,__ALA_100__,__ASN_101__,__ALA_102__,__PHE_112__,__TRP_120__,__LEU_121__,__HIS_125__


DFLT_DEMO_SHAKE_SEQUENCE  = "3e21.fst"
DFLT_DEMO_SHAKE_REFPDB    = "3e21.pdb"
DFLT_DEMO_SHAKE_LABEL     = "3e21"
DFLT_DEMO_SHAKE_MODEL     = "3e21.pdb"
DFLT_DEMO_SHAKE_SEQMASK   = "3e21.mask"

DFLT_DEMO_COMPLEX_SEQUENCE  = "1AWR_lig.fst"
DFLT_DEMO_COMPLEX_RECEPTOR  = "1AWR_apo-clean.pdb"
DFLT_DEMO_COMPLEX_PATCH     = "1AWR_interface.txt"
DFLT_DEMO_COMPLEX_LABEL     = "1AWR"

DFLT_WPATH        = "."
DFLT_MAX_LETTERS  = 8
PEPFOLD_DFLT_LABEL = "PEPFOLD"

DFLT_MIN_SEQ_SIZE = 5
DFLT_MAX_SEQ_SIZE = 50 # 50
DFLT_MAX_SIM_SIZE = 50 # 36
DFLT_MAX_PEPFOLD_CENTROIDS = 10

DFLT_PEPFOLD_CPPNCORES = 32
DFLT_PEPFOLD_PPPNCORES = 8


DFLT_MODEL_NUMBER = 100

DFLT_KEYS         = ["sOPEP", "RMSd","TMalign"]
DFLT_SORT_KEY     = "sOPEP" # one of "sOPEP", or, if reference specified, "RMSd" or "TMalign"

# Pymol color palette
PML_COL = ["marine","magenta","limegreen","dash","br8",
            "lightblue","cyan","lightpink","purpleblue","sulfur",
            "oxygen","deepteal","chocolate","br4","lightteal"]

