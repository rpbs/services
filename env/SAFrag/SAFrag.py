#!/usr/bin/env python
1
# @author(s): Yimin Shen, Frederic Guyon, Pierre Tuffery
# Yimin SHEN, 2012, core developper of the SA-Prof program.
# Version 1.7 (20/07/2012)
# Wrapping into a python class using the start, check_options, cmdLine_options formalism by P. Tuffery, 2012 
# PrfScore.c: written by P. Tuffery and F.Guyon, 2011

import sys
import os
import re
import fnmatch
import SGE
import time
import MethodeBasic
from datetime import datetime
from time import localtime,strftime

# Dependencies
from PyOptions import *

# Modifier ici !!!
from Config import *

import PyPDB.PyPDB as PDB
import Fasta
import Matches2Pir
import Pir2Rose
import resSAprofToHtml
import Matches2Trj
import shutil

GLOB_sgeIds = None
import signal

#forYimin=True
forYimin=False # superseded by genThresholdData option since October 2012.

def sgehandler(signum, frame, verbose = False):
    """
    We do not know if sge jobs are finished.
    We try each.
    """

    if verbose:
        f = open("ilog","a")
        f.write("SGEHANDLER ON REMOTE KILL\n")
        f.write("Ids : %s\n" % str(GLOB_sgeIds))
    
    if GLOB_sgeIds:
        try:
            for aId in GLOB_sgeIds:
                os.system("qdel %s" % aId)
                f.write("qdel %s\n" % aId)
        except:
            pass
    if verbose:
        f.close()
    sys.exit(0)

class SAprof(Options):

    def __init__(self, args = ["PostTreatment"], options = None, version=None, verbose = False):
        """
        @param args    : a command line argument list (first is program name)
        @param options : a dictionary of the options among which 
                         some are valid.
        @param version : the version of the class (can be superseeded from external classes)
        """
        if version == None:
            version = "SA-Frag"+ " v%s"%VERSION
        if not hasattr(self,"clp"):
            Options.__init__(self, args = args, options = options, version=version, verbose = verbose)
        self.__cmdLine_options()


    def setremotehandler(self, verbose = False):
        """
        This to ensure qdel on Mobyle user removal.
        """
	# f = open("ilog","a")
	# f.write("Installing signal handler\n")
	# f.close()
        signal.signal(signal.SIGTERM, sgehandler) 

    def progressLog(self, msg = None, mode = "a"):
        """
        Mobyle progress log 
        """
        f = open(self.options.progressFile,mode)
        f.write("%s\n" % msg)
        f.close()


    def start(self, parse = True, check = True, verbose = False):
        """
        Effective options parsing
        on return, PostTreament ready to run.
        """
        # This for demo mode
        if parse:
            self.parse()
        if verbose:
            self.options.verbose = verbose
        if check:
            if self.options.isMain:
                self.progressLog("SA-Frag (1/6): Checking options.", "a")
            self.cmd_line_status = self.__check_options(verbose = verbose)
            if verbose:
                sys.stderr.write("SAProf Initial start status: %s\n" % self.cmd_line_status)
        return

    def __repr__(self):
        """
        @return: a string describing the options setup for the Blast instance
        """

        try:
            rs = self.version
            for option in self.options.__dict__.keys():
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""

    def __cmdLine_options(self, version = None):
        """
        Command line definition.
        These options are extra options over inherited PPP options.
        @return: command line options and arguments (optionParser object)
        """

        self.clp.set_usage("usage: %prog [options] <sequence file> ")

        # OK from here
        self.clp.add_option("-s", "--iSeq", action="store",        dest="seqFile",   type="string",
                       help="amino acid sequence (fasta file)",    default = None)

        self.clp.add_option("-3", "--iPDB", action="store",        dest="pdb",   type="string",
                       help="PDB file",                            default = None)

        self.clp.add_option("-r", "--iRefPDB", action="store",     dest="refPDB",   type="string",
                       help="reference PDB structure (for RMSd)",  default = None)

        self.clp.add_option("--iRefxyz", action="store",              dest="refXYZ",   type="string",
                       help="XYZ file",                            default = None)

        self.clp.add_option("--iProb", action="store",             dest="prob",   type="string",
                       help="SA profile file",                     default = None)

        self.clp.add_option("--iSeqComplet", action="store",       dest="seqFileComplet",   type="string",
                       help="amino acid complete sequence (fasta file)", default = None)

        self.clp.add_option("--bank", action="store",              dest="bank",   type="string",
                       help="Bank to mine. One of %s" % (str(LISTS.keys())),                     
                                                                   default = None)

        self.clp.add_option("--probList", action="store",          dest="probList",        type="string",
                       help="the list of the proteins to scan (.prob)",
                                                                   default = DFLT_PROB_FILE_LIST)

        self.clp.add_option("--probDbPath", action="store",        dest="probDbPath",    
                       help="path to .prob files",                 default = DFLT_PROB_FILE_PATH)

        self.clp.add_option("--xyzList", action="store",           dest="xyzList",        type="string",
                       help="the list of the proteins  3D (.xyz) to calculate ccRMSd",
                                                                   default = DFLT_XYZ_FILE_LIST)

        self.clp.add_option("--xyzDbPath", action="store",         dest="xyzDbPath",    
                       help="path to .xyz files",                  default = DFLT_XYZ_FILE_PATH)

        self.clp.add_option("--mode", action="store",              dest="mode",   type="string",
                       help="running mode one of %s, default is %s" % (",".join(SAPROF_MODES), DFLT_SAPROF_MODE),             
                                                                   default = DFLT_SAPROF_MODE)

        self.clp.add_option("--maxSeqSze", action="store",                   dest="maxSeqSze",   type="int",
                       help="maximal input sequence size  (%s)" % str(DFLT_MAX_SEQ_SIZE),   default = DFLT_MAX_SEQ_SIZE)

        self.clp.add_option("--genThresholdData", action="store_true",    dest="genThresholdData",
                            help="Switch to systematic output for threshold learning",  default = False),

        self.clp.add_option("--from", action="store",                dest="ffrom",      type="int",
                       help="Search fragments starting at given position (from 1) (Default : %d)" % DFLT_SAPROF_FROM,     
                                                                   default = DFLT_SAPROF_FROM) 

        self.clp.add_option("--to", action="store",                dest="tto",      type="int",
                       help="Search fragments up to given position (included, from 1, -1 is last) (Default : %d)" % DFLT_SAPROF_TO,     
                                                                   default = DFLT_SAPROF_TO) 
        
        self.clp.add_option("--thresholdJS", action="store",       dest="thresholdDisJS",    type="string",
                            help="threshold DisJS with %s errors, default is %s"%(",".join(DISJS_THRESHOLDS), DFLT_DISJS_THRESHOLD),
                                                                   default = DFLT_DISJS_THRESHOLD)       

        self.clp.add_option("-p", "--wPATH", action="store",       dest="wPath",    type="string",
                            help="working path",                   default = DFLT_WPATH)

        self.clp.add_option("--tmpPATH", action="store",           dest="tmpPath",    type="string",
                       help="temporary working path for cluster (e.g. /scratch/tmpCLS)", 
                                                                   default = DFLT_TMPPATH)

        self.clp.add_option("--progressFile", action="store",      dest="progressFile",      type="string",
                       help="Log file to put information about SA-Frag progress (%s)" % DFLT_PROGRESS_FILE,
                                                                   default = DFLT_PROGRESS_FILE)

        self.clp.add_option("-l", "--label", action="store",       dest="label",      type="string",
                       help="job label",                           default = DFLT_SAPROF_LABEL) 

        self.clp.add_option("--upperPrecisionThreshold", action="store",    dest="inthresholdPreciAccept",      type="float", 
                       help="minimal cluster acceptance precision (former thresholdPreciAccept) [0.6-1.0] with (%5.3f) [higher favorable precision]" % DFLT_PRECI_ACCEPT,     
                                                                   default = None) 
        
        self.clp.add_option("--lowerPrecisionThreshold", action="store",    dest="inthresholdPreciReject",      type="float",
                       help="maximal cluster rejection precision (former threholdPreciReject) [0.6-1.] with (%5.3f) [lower favorable coverage]" % DFLT_PRECI_REJECT,     
                                                                   default = None) 

        self.clp.add_option("--coverageTimes", action="store", dest="incoverageTimes", type="int",
                            help="coverage times [1,2,3 or 4] for the medium precision fragments (%d)[higher favorable coverage-Ok approach coverage-all, but adverse precision]" % DFLT_COVERAGE_TIMES,
                            default = None)

        self.clp.add_option("--searchMode", action="store",                dest="searchMode",      type="string",
                       help="search mode is one of %s" % SEARCH_MODES,     
                                                           default = DFLT_SEARCH_MODE) 
        
        self.clp.add_option("--np", action="store",                dest="np",      type="int",
                       help="number of concurrent processing units to use (%d)" % DFLT_SAPROF_NP,     
                                                           default = DFLT_SAPROF_NP) 

        self.clp.add_option("--outFmt", action="store",            dest="outFmt",      type="string",
                       help="output format",                       default = DFLT_SAPROF_OUTPUT) 

        self.clp.add_option("--child", action="store_false",      dest="isMain",   
                       help="denotes a child of a main job (avoid mutlitple archives, etc). DO NOT USE",    default = True)

        self.clp.add_option("--noPPP", action="store_false",       dest="ppp",   
                       help="switch profile generation off (saves time tor tests once generated)",          default = True)


    def __check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy
        
        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """

        # Check input data exists
        if (not self.options.seqFile) and (not self.options.pdb) and (not self.options.prob):
            if verbose:
                sys.stderr.write("SA-Frag : must specify either sequence, PDB, or SA profile. --help for option detail.\n")
            self.progressLog("SA-Frag : unspecified input sequence, PDB or SA profile file.", "a")
            return False

        self.aaSeq = None

        if self.options.pdb:
            try:
                x = PDB.PDB(self.options.pdb)
            except:
                if verbose:
                    sys.stderr.write("SA-Frag: Could not read PDB input.\n")
                self.progressLog("SA-Frag : Could not read PDB input.", "a")
                return False

            if not len(x):
                if verbose:
                    sys.stderr.write("SA-Frag: Void PDB input.\n")
                self.progressLog("SA-Frag : Void PDB input.", "a")
                return False

            self.aaSeq = x.aaseq()

        elif self.options.seqFile:
            # Now we get the sequence to treat (if possible)
            seq = Fasta.fasta(self.options.seqFile)
            self.aaSeq = seq[seq.data.keys()[0]].data["s"]
            if len(self.aaSeq) > self.options.maxSeqSze:
                if verbose:
                    sys.stderr.write("SA-Frag: Too long sequence (%d > %d).\n" % (len(self.aaSeq), self.options.maxSeqSze))
                self.progressLog("SA-Frag : Too long sequence (%d > %d).\n" % (len(self.aaSeq), self.options.maxSeqSze), "a")
                return False
                
        if self.options.refPDB:
            try:
                x = PDB.PDB(self.options.refPDB)
            except:
                if verbose:
                    sys.stderr.write("SA-Frag: Could not read reference PDB input.\n")
                self.progressLog("SA-Frag : Could not read reference PDB input.", "a")
                return False

            if not len(x):
                if verbose:
                    sys.stderr.write("SA-Frag: Void or incorrect reference PDB input.\n")
                self.progressLog("SA-Frag : Void or incorrect reference PDB input.", "a")
                return False
                
            # Check sequence is similar to input
            if self.options.pdb or self.options.seqFile:
                aaRefSeq = x.aaseq()
                if self.aaSeq != aaRefSeq:
                    if verbose:
                        sys.stderr.write("SA-Frag: Reference PDB sequence does not match query sequence.\n")
                    self.progressLog("SA-Frag : Reference PDB sequence does not match query sequence.", "a")
                    return False

        if self.options.bank:
            if self.options.bank in LISTS.keys():
                self.options.probDbPath = LISTS[self.options.bank]["path"]
                self.options.probList   = LISTS[self.options.bank]["list"]
                self.options.xyzList    = LISTS[self.options.bank]["xyzlist"]
                self.options.xyzDbPath  = LISTS[self.options.bank]["xyzpath"]
            else:
                if verbose:
                    sys.stderr.write("SA-PROF: Incorrect bank specified.\n")
                self.progressLog("SA-PROF : Incorrect bank specified.", "a")
                return False
            if  self.options.bank == "25":
                if self.options.searchMode == "Dflt":  # Former maxPrec
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.82 # 0.92
                    self.options.coverageTimes        = 4 # 3
                elif self.options.searchMode == "maxPrec":
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.95
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxCov":
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 5
            if  self.options.bank == "30":
                if self.options.searchMode == "Dflt":  # Former maxPrec
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.82
                    self.options.coverageTimes        = 4
                elif self.options.searchMode == "maxPrec":
                    self.options.thresholdPreciAccept = 0.995
                    self.options.thresholdPreciReject = 0.92
                    self.options.coverageTimes        = 5
                elif self.options.searchMode == "maxCov":
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.66
                    self.options.coverageTimes        = 5
            if  self.options.bank == "50":
                if self.options.searchMode == "Dflt": # former maxPrec
                    self.options.thresholdPreciAccept = 0.995
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxPrec":
                    self.options.thresholdPreciAccept = 0.995
                    self.options.thresholdPreciReject = 0.5
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxCov":
                    self.options.thresholdPreciAccept = 0.97
                    self.options.thresholdPreciReject = 0.66
                    self.options.coverageTimes        = 5
            if  self.options.bank == "70":
                if self.options.searchMode == "Dflt": # former maxPrec
                    self.options.thresholdPreciAccept = 0.995
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxPrec":
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.95
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxCov":
                    self.options.thresholdPreciAccept = 0.8
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 8
            if  self.options.bank == "90":
                if self.options.searchMode == "Dflt": # former maxPrec
                    self.options.thresholdPreciAccept = 0.995
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxPrec":
                    self.options.thresholdPreciAccept = 0.99
                    self.options.thresholdPreciReject = 0.95
                    self.options.coverageTimes        = 3
                elif self.options.searchMode == "maxCov":
                    self.options.thresholdPreciAccept = 0.8
                    self.options.thresholdPreciReject = 0.65
                    self.options.coverageTimes        = 8


        if self.options.inthresholdPreciAccept:
            self.options.thresholdPreciAccept = self.options.inthresholdPreciAccept
        if self.options.inthresholdPreciReject:
            self.options.thresholdPreciReject = self.options.inthresholdPreciReject
        if self.options.incoverageTimes:
            self.options.coverageTimes = self.options.incoverageTimes
        return True

    def run(self, verbose = 0):
        """
        The core search function:
        1. If necessary, will generate the .prob
        2. then will scan a bank to search for matches, for a series of fragment lengths.
        3. Finally, will assemble the results
        """
	global GLOB_sgeIds

        print datetime.now()
        (s1,a1)=compteurTime()
        # 1. If necessary: generate the .prob from sequence or PDB
        cmd = ""
        self.progressLog("SA-Frag (2/6): SA profile generation", "a")

        self.setremotehandler()

        if self.options.seqFile:
            if self.options.seqFileComplet:
                cmd = "PyPPPExec -s %s --SVMmodel %s --blastDbPath %s -l %s\n"%(self.options.seqFileComplet,SVM_MODEL,BLAST,self.options.label+".complet")
                probFileComplet = "%s.complet.svmi8.27.prob" % self.options.label
                self.probFile = "%s.svmi8.27.prob" % self.options.label
#                CutProbTempleFASTA.Run(self.options.seqFileComplet,self.options.seqFile,probFileComplet,self.probFile)
                cmd += "%s/CutProbTempleFASTA.py %s %s %s %s\n"%(PATH_SCRIPTS,self.options.seqFileComplet,self.options.seqFile,probFileComplet,self.probFile)
            else :
                cmd = "PyPPPExec -s %s --SVMmodel %s --blastDbPath %s -l %s\n"%(self.options.seqFile,SVM_MODEL,BLAST,self.options.label)
                self.probFile = "%s.svmi8.27.prob" % self.options.label
            fileName_= self.options.label
        elif self.options.pdb:
            # Write the PDB sequence, then run on sequence. 
            f = open("%s.fst" % self.options.label,"w")
            f.write("> %s\n" % self.options.label)
            f.write("%s\n" % self.aaSeq)
            f.close()
            cmd = "PyPPPExec -s %s.fst --SVMmodel %s --blastDbPath %s -l %s\n"%(self.options.label,SVM_MODEL,BLAST,self.options.label)
            self.probFile = "%s.svmi8.27.prob" % self.options.label
            fileName_= self.options.label
            # ATTENTION: IL Y A UNE DIFFERENCE ENTRE LES LIGNES PDB ET FST POUR SVM ET BLAST !!
        else:
            self.probFile = self.options.prob
        
        if not self.options.ppp:
            id=self.options.seqFile.replace(".fst","").replace(".fasta","").replace(".seq","")
            if not os.path.isfile(self.options.label+".svmi8.27.prob") and os.path.isfile(id+".svmi8.27.prob"):
                print ">cp %s %s"%(id+".svmi8.27.prob",self.options.label+".svmi8.27.prob")
                shutil.copyfile(id+".svmi8.27.prob",self.options.label+".svmi8.27.prob")

        if cmd!="" and self.options.ppp:        
            jobf = SGE.SGE_script_creat(cmd,"PPP_%s.sh"%self.options.label,"all.q", self.options.label, array_size=1, array_from=1,OutErrFile=True)
            jobId = SGE.SGE_submit(jobf,waitId = None)
            GLOB_sgeIds = [jobId]
            # f = open("ilog","a")
            # f.write("Ids : %s\n" % str(GLOB_sgeIds))
            # f.close()

            print "Submitted PPP_%s.sh. JobId: %s"% (self.options.label,jobId) 
            SGE.SGE_wait(jobId)

        # 1.5 For .xyz management:
        if self.options.refPDB:
            cmd="PDBSAExec -3 %s -l %s\n"%(self.options.refPDB,self.options.label)
            jobf = SGE.SGE_script_creat(cmd,"PDBSA_%s.sh"%self.options.label,"all.q", self.options.label, array_size=1, array_from=1,OutErrFile=True)
            jobId = SGE.SGE_submit(jobf,waitId = None)
            print "Submitted PDBSA_%s.sh. JobId: %s"% (self.options.label,jobId)            
            GLOB_sgeIds = [jobId]
            # f = open("ilog","a")
            # f.write("Ids : %s\n" % str(GLOB_sgeIds))
            # f.close()

            SGE.SGE_wait(jobId)
            
            # List files of the current directory
            filesL = os.listdir( "." )

            # PDB files to clusterize
            xyzlist = []
            pattern = "%s*.xyz" % self.options.label

            # List them
            for aFile in filesL:
                if fnmatch.fnmatch( aFile, pattern ):
                    xyzlist.append( aFile )
            if len(xyzlist) > 1:
                sys.stderr.write("SAFrag: Sorry, cannot handle PDB reference structure with more than one chain.\n")
                sys.exit(0)
            if len(xyzlist) < 1:
                sys.stderr.write("SAFrag: Sorry, could not process correctly PDB reference structure.\n")
                sys.exit(0)

            # self.options.refXYZ = "%s.xyz" % self.options.label
            self.options.refXYZ = xyzlist[0]

        # 2. Now, we run the search
        if   self.options.mode in ["FiltComplet","FiltFast"]: strFilt="filt"   #if filter
        elif self.options.mode in ["AllComplet","AllFast"]:   strFilt="nofilt" # get all fragment
        else: 
            sys.stderr.write("SAFrag: Sorry, Incorrect mode (%s) !\n" % self.options.mode)
            sys.exit(0)

        # estimate the number of runs (each size, each position)
        time.sleep(5) # NFS

        tabProb=MethodeBasic.inputProb(self.probFile)
        lenProb=len(tabProb)
        compte=0
        tabFiles=[]
        tabJobIds=[]
        compteTotal=0
        # Setup range ffrom tto
        self.options.ffrom -= 1
        if self.options.tto == -1:
            self.options.tto = lenProb
        else:
            self.options.tto -= 3
        if self.options.tto > lenProb:
            self.options.tto = lenProb
        
        # Maybe we could optimize this ?
        for lenFragHMM in TAB_LENFRAG:
            for posi in range(self.options.ffrom, self.options.tto - lenFragHMM + 1):
                compteTotal+=1

        print "Total jobs to submit: %d\n" % compteTotal
        self.progressLog("SA-Frag (3/6): Searching bank ...", "a")

        # Now we launch the jobs using at max n slots ... Could be preferable to use SGE_TASK_ID (HOW) ??
        nbJobLancer_=0
        GLOB_sgeIds = []
        for lenFragHMM in TAB_LENFRAG:
            for posi in range(self.options.ffrom, self.options.tto - lenFragHMM+1):
#                cmd="%s/analyseFrag1_Clust.sh T0643.svmi8.27.prob.newModelSVM T0643.xyz 1 20 /nfs/freetown/user/shen/PP-Search/Data/PDB25_Err/ok49Id.fin.prob /nfs/freetown/user/shen/PP-Search/Data/PDB25_Err/ok49Id.fin /nfs/freetown/user/shen/PP-Search/Data/PDB25_Err/Err/ filtFast"
                compte+=1
                posi_deb=posi+1
                posi_fin=posi_deb+lenFragHMM-1
#                cmd="%s/analyseFrag1_Clust.sh %s %s %d %d %s %s %s %s"%(PATH_SCRIPTS,probFile,xyzFile,posi_deb,lenFragHMM,IDsProb,IDsCRMSD,pathCRMSD,mode)
                fileName="%s-%d-%d"%(self.options.label,posi_deb,posi_fin)
                tmpDir = "%s/SAPROF-%s-$RANDOM-$RANDOM" % (self.options.tmpPath, fileName)
                cmd="temp=%s\n"%(tmpDir)
                cmd+="mkdir $temp\n"
                cmd+="node=`hostname | cut -d\".\" -f1`\n"
                cmd+="echo [RunPath: $node /scratch/$temp/]\n"
                cmd+="echo INFO[Bank:%s  SVMmodel:%s  Blast:%s]\n"%(self.options.bank,SVM_MODEL,BLAST)
                cmd+="cp %s %s/%s $temp/\n"%(self.probFile,SAPROF_LIB,self.options.probList)
                cmd+="probFile=`basename %s`\nIDsProb=`basename %s`\npathProb=%s\n"%(self.probFile, self.options.probList,self.options.probDbPath)
#                cmd+="a=`cat /scratch/$temp/$probFile | wc -l`\n b=`cat /scratch/$temp/$IDsProb | wc -l`\n"
#                cmd+="if [ $a = 0 -o $b = 0 ]; then cp %s %s /scratch/$temp/; echo RECOPY $a $b; fi\n"%(probFile,IDsProb)
                
                if self.options.refXYZ:
                    cmd+="cp %s %s/%s $temp/\n"%(self.options.refXYZ,SAPROF_LIB,self.options.xyzList)
                    cmd+="xyzFile=`basename %s`\nIDsCRMSD=`basename %s`\npathCRMSD=\"%s\"\n"%(self.options.refXYZ,self.options.xyzList,self.options.xyzDbPath)
#                    cmd+="a=`cat /scratch/$temp/$xyzFile | wc -l`\n b=`cat /scratch/$temp/$IDsCRMSD | wc -l`\n"
#                    cmd+="if [ $a = 0 -o $b = 0 ]; then cp %s %s /scratch/$temp/; echo RECOPY $a $b; fi\n"%(xyzFile,IDsCRMSD)
                else: cmd+="xyzFile=\"NA\"\nIDsCRMSD=\"NA\"\npathCRMSD=\"NA\"\n"
                cmd+="path=$PWD\n"
                cmd+="cd $temp\n\n"
#                cmd+="export SAPROF_LIB=%s\n" % SAPROF_LIB
#                cmd+="export SAPROF_PROB_PATH=%s\n" % self.options.probDbPath

                cmd+="export SAPROF_BIN_PATH=%s\n" % SAPROF_BIN_PATH
                cmd+="export SAPROF_SCRIPT_PATH=%s\n" % SAPROF_SCRIPT_PATH
                if self.options.mode in ["FiltComplet","FiltFast"]: #analyseFrag1_Clust.sh: methode est efficase pour une petite nombre des fragments.
                    nMax = 500
                    if self.options.genThresholdData:
                        nMax = 10000
                    cmd+="echo \"%s/analyseFrag1_Clust.sh $probFile $xyzFile %d %d $IDsProb $pathProb $IDsCRMSD $pathCRMSD %s %s %s %d\"\n"%(PATH_SCRIPTS,posi_deb,lenFragHMM,self.options.mode,self.options.label,self.options.thresholdDisJS, nMax)
                    cmd+="%s/analyseFrag1_Clust.sh $probFile $xyzFile %d %d $IDsProb $pathProb $IDsCRMSD $pathCRMSD %s %s %s %d\n\n"%(PATH_SCRIPTS,posi_deb,lenFragHMM,self.options.mode,self.options.label,self.options.thresholdDisJS, nMax)
                    cmd+="# if found match fragment copy results back, else make sure not exist the same name file in source dossier.\n"
                    if forYimin or self.options.genThresholdData:
                        cmd+="if [ -f %s.%s.allCl ];then cp %s.%s.allCl $path;else if [ -f $path/%s.%s.allCl ];then rm $path/%s.%s.allCl;fi;fi\n"%(fileName,strFilt,fileName,strFilt,fileName,strFilt,fileName,strFilt) # dflt pas besoin
                        cmd+="if [ -f %s.%s.refCl ];then cp %s.%s.refCl $path;else if [ -f $path/%s.%s.refCl ];then rm $path/%s.%s.refCl;fi;fi\n"%(fileName,strFilt,fileName,strFilt,fileName,strFilt,fileName,strFilt) # dflt pas besoin
                    cmd+="if [ -f %s.%s.bestCl ];then cp %s.%s.bestCl $path;else if [ -f $path/%s.%s.bestCl ];then rm $path/%s.%s.bestCl;fi;fi\n"%(fileName,strFilt,fileName,strFilt,fileName,strFilt,fileName,strFilt)
                else: #analyseFrag2_Clust.sh: pour une tres grand nombre des fragments, il faut utilise ce methode.
                    cmd+="echo \"%s/analyseFrag2_Clust.sh $probFile $xyzFile %d %d $IDsProb $pathProb $IDsCRMSD $pathCRMSD %s %s %s\"\n"%(PATH_SCRIPTS,posi_deb,lenFragHMM,self.options.mode,self.options.label,self.options.thresholdDisJS)
                    cmd+="%s/analyseFrag2_Clust.sh $probFile $xyzFile %d %d $IDsProb $pathProb $IDsCRMSD $pathCRMSD %s %s %s\n\n"%(PATH_SCRIPTS,posi_deb,lenFragHMM,self.options.mode,self.options.label,self.options.thresholdDisJS)
                    cmd+="# if found match fragment copy results back, else make sure not exist the same name file in source dossier.\n"
                    cmd+="if [ -f %s.%s ];then cp %s.%s $path;else if [ -f $path/%s.%s ];then rm $path/%s.%s;fi;fi\n"%(fileName,strFilt,fileName,strFilt,fileName,strFilt,fileName,strFilt) # dflt pas besoin
#                cmd+="cp * $path\n"
                cmd+="rm -fr $temp\n"

#                continue
                jobf = SGE.SGE_script_creat(cmd,"SAFRG1_%s.sge"%fileName,"all.q", fileName, array_size=1, array_from=1,OutErrFile=True)
                if len(tabJobIds)>=self.options.np and nbJobLancer_<=0:(tabJobIds,nbJobLancer_) = SGE.SGE_wait_maxRunList(tabJobIds,self.options.np,wait_delay=3) # pour ne lancer pas tops de jobs en meme temps.

                nbJobLancer_-=1
                jobId = SGE.SGE_submit(jobf,waitId = None)
                print "  %d-%d\t%s\tLen%d\tJobId: %s"%(compte,compteTotal,fileName,lenFragHMM,jobId) 
                if ((float(compte) / float(compteTotal)) * 100) % 10 == 0.:
                    self.progressLog("SA-Frag : Launched %d %%..." % int((float(compte) / float(compteTotal)) * 100), "a")
                    
                tabFiles.append("%s.%s"%(fileName,strFilt))
                tabJobIds.append(jobId)
                GLOB_sgeIds = tabJobIds
                # f = open("ilog","a")
                # f.write("Ids : %s\n" % str(GLOB_sgeIds))
                # f.close()
                time.sleep(0.1)
        
        print "All the %d Jobs have been submitted. Please wait ..."%compteTotal
        SGE.SGE_wait_list(tabJobIds)
        (s2,a2)=compteurTime()
        if a1==a2: usedTime="[Used %d min %d sec]"%((s2-s1)/60,(s2-s1)%60)
        else: usedTime=""
        print "1st step has finished %s Continue ..."%usedTime
        time.sleep(5) # NFS

        # Now, we merge all the results, clusterize, etc
        self.progressLog("SA-Frag (4/6): Clustering ...", "a")
        
        print "------[Fusion files]--------"
        fileName2=self.options.label+"."+strFilt+".bestCl"
        if self.options.mode in ["FiltComplet","FiltFast"]:
            if forYimin or self.options.genThresholdData:
                self.fusionFiles(tabFiles,(self.options.label+"."+strFilt+".allCl"),".allCl") #Fusion the allCl files
                self.fusionFiles(tabFiles,(self.options.label+"."+strFilt+".refCl"),".refCl") #Fusion the refCl files
            self.fusionFiles(tabFiles,fileName2,".bestCl") #Fusion the bestCl files

#            cmd="%s/FusionFiles.py %s >%s\n"%(PATH_SCRIPTS,files,fileName2) # A cause de nombreux files, risque de passer MAX-Line-Command
#            usage:FusionFiles.py [PATH] [Regex] [OutputFile]
#            
#            cmd="if [ -f %s ];then \\rm %s;fi\n"%(fileName2,fileName2)
#            cmd+="%s/FusionFilesRegex.py . %s %s\n"%(PATH_SCRIPTS,"\.%s\.clBest1\.0A\\\Z"%strFilt,fileName2)
#            if forYimin : 
#                cmd+="%s/FusionFilesRegex.py . %s %s\n"%(PATH_SCRIPTS,"\.%s\.clRef1\.0A\\\Z"%strFilt,self.options.label+"."+strFilt+".refCl") # dflt pas besoin
#                cmd+="%s/FusionFilesRegex.py . %s %s\n"%(PATH_SCRIPTS,"\.%s\.clAll1\.0A\\\Z"%strFilt,self.options.label+"."+strFilt+".allCl") # dflt pas besoin
            
            #AnalyseDATA_V2.py [1.FileName:T0471-99-101-DisJSNA-rmsdNA.data.fin] [2.seuilScore:Auto/0.x/NA] [3.seuilCRMSD:Auto/x.x/NA] [4.SVMmodel:new/newSure/old/oldExact] [5.OutFile:fileName/NA/No] [6.Rapide:Yes/No] 
            #[7.OutInfo:Yes/No] [8.NumColPoids:NA/12] [9.seuilEff/Poids: NA/2/...][10.NBest-JS/CRMSD:NA/1/2/...] [11.DataHHfrag:Yes/No] [12.ReplaceJSMaxToMean_puisFilte:Yes/No] [13.AllFragAjouter( or BestFrag&FixCoverage): Yes/No] 
            #[14.SeuilConfianceEff:NA/Auto/2/3...]
            # A cause de AnalyseDATA_V2.py en mode Rapide (ne calculer pas %cover, donc on n'a pas besoin de length de seq)

            self.progressLog("SA-Frag (5/6): Filtering ...", "a")

            cmd="echo INFO[Bank:%s  SVMmodel:%s  Blast:%s]\n"%(self.options.bank,SVM_MODEL,BLAST)
#            cmd+="%s/AnalyseDATA_V2.py %s NA NA %s %s Yes No 0 NA NA No No No Auto\n"%(PATH_SCRIPTS,fileName2,self.options.thresholdDisJS,fileName2+".SAprof")
            cmd+="%s/AjouterPrecision_2.py %s %s/Ref_Preci.500Hits-PC5.txt %s\n"%(PATH_SCRIPTS,fileName2,PATH_SCRIPTS,fileName2+".preci_")
            cmd+="%s/DelRedondResuFrag.py %s %s\n"%(PATH_SCRIPTS,fileName2+".preci_",fileName2+".preci")
            cmd+="%s/AnalyseDATA_V3.py %s NA NA %s %s Yes No -1 NA No NA No No %f %f 0 %d\n"%(PATH_SCRIPTS,fileName2+".preci",self.options.thresholdDisJS,fileName2+".SAFrag",self.options.thresholdPreciAccept,self.options.thresholdPreciReject,self.options.coverageTimes)
            jobf = SGE.SGE_script_creat(cmd,"SAFRG2_%s.sge"%fileName2,"all.q", fileName2, array_size=1, array_from=1,OutErrFile=True)
            jobId = SGE.SGE_submit(jobf,waitId = None)
            print "%s\tJobId: %s"%(fileName2,jobId) 
            GLOB_sgeIds = [jobId]
            # f = open("ilog","a")
            # f.write("Ids : %s\n" % str(GLOB_sgeIds))
            # f.close()
            SGE.SGE_wait(jobId)
#            print fileName2+".thresholdPreciReject.err"
            time.sleep(5) # NFS

            self.progressLog("SA-Frag (6/6): Formatting results ...", "a")

            if not os.path.isfile(fileName2+".SAFrag") and os.path.isfile(fileName2+".preci.thresholdPreciReject.err"): 
                self.report="Result after filter is empty(We can't find the fragments with the precision >%5.3f). So you cans :\n"%self.options.thresholdPreciReject
                self.report+="\t 1. Restart with decrease threshold precision reject[< %5.3f] with add option (--thresholdPreciReject)\n"%self.options.thresholdPreciReject
                self.report+="\t 2. Use the result of the file bestCl [%s.preci]\n"%fileName2
                self.report+="\t 3. If your sequence is a part of fragment, and you have the complete sequence, restart with option (--iSeqComplet)\n"
                print self.report
                sys.stderr.write("We can't find the fragments with the precision >%5.3f \n"%self.options.thresholdPreciReject)
                sys.exit(0)
            if (self.aaSeq is not None) and (not self.options.genThresholdData):
                Matches2Pir.matches2pir(fileName2+".SAFrag", fileName2+".pir", self.options.label, self.aaSeq, dbPATH = self.options.xyzDbPath, verbose = 0)
                Pir2Rose.main(fileName2+".pir", self.options.label, self.options.probDbPath)
                Matches2Trj.matches2trj(fileName2+".SAFrag", fileName2+".safrag.pir", fileName2+".safrag.trj", self.options.label, self.aaSeq, dbPATH = self.options.xyzDbPath,verbose = 0)
                if self.options.refPDB:
                    resSAprofToHtml.main(["resSAprofToHtml",self.options.label+".saprof.09", self.aaSeq, self.options.refPDB])
                else:
                    resSAprofToHtml.main(["resSAprofToHtml",self.options.label+".saprof.09", self.aaSeq])

        (s3,a3)=compteurTime()
        if a1==a3: usedTime="[Used %d min %d sec]"%((s3-s1)/60,(s3-s1)%60)
        else: usedTime=""
        print datetime.now()
        print "------[End]--------%s"%usedTime

        self.progressLog("SA-Frag : Done ...", "a")

        return


    def fusionFiles(self,listFiles,outFile,extention=""): 
        strOut=""
        comptLines=0
        for file in listFiles:
            if os.path.isfile(file+extention):
                f = open(file+extention)
                lines = f.readlines()
                f.close()
                
                for i in range(len(lines)):
                    l=lines[i].replace("\n","")
                    if len(l)>0:
                        strOut+=l+"\n"
                        comptLines+=1
                    else : sys.stderr.write( "Attention: [file:%s] delete empty line[ %d ]  =>%s<=\n"%(file,i,l))
        
        if os.path.isfile(outFile): os.remove(outFile) # del le file s'il exist
        if strOut!="":
            outPut(strOut,outFile,comptLines)
        else : 
            print "Input files is empty! Can't creat %s file."%(outFile)
#            sys.exit("Input files is empty! Can't creat %s file."%(outFile)) 

def compteurTime():
    """
    return les second cumuler de cette annee 
    """
    a=localtime().tm_year
    d=localtime().tm_yday
    h=localtime().tm_hour
    m=localtime().tm_min
    s=localtime().tm_sec
    return (s+60*m+3600*h+3600*24*d),a

def outPut(text, where = None,i=0,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file :"+where+"]\t["+str(i)+" lines]"
    if where:
        f.close()

optionStr="====================================[OPTION]=============================================\n"
optionStr+="<Target_PROB/FASTA_FileName> : PROB file (or FASTA file) of the target\n"
optionStr+="<Target_XYZ/PDB_FileName/NA> : XYZ file (or PDB file) of the target. \"NA\" for file absence\n"
optionStr+="<BD_IDsProb/Dflt>            : list ids for the PROB data base. \"Dflt\" for SA-PROF.DFLT_PROB_FILE_LIST\n"
optionStr+="<BD_IDsCRMSD:NA/Dflt>        : list ids for the PDB data base. \"NA\" for PDB files absence, \"Dflt\" for SA-PROF.DFLT_XYZ_FILE_LIST\n"
optionStr+="<BD_PathIDsCRMSD:NA/Dflt>    : path for the PDB files data base. \"NA\" for data base absence, \"Dflt\" for SA-PROF.DFLT_XYZ_FILE_PATH\n"
optionStr+="<Mode:AllFast/AllComplet/FiltComplet/FiltFast>:\n"
optionStr+="    FiltFast     : Filtre DisJS  manquer DisJS par position.\n"
optionStr+="    FiltComplet  : Filtre DisJS, ajouter DisJS par position.\n"
#optionStr+="    AllFast   [Test]: All the Data, manquer DisJS par position, mode for the test\n"
#optionStr+="    AllComplet[Test]: All the Data, ajouter DisJS par position, mode for the test\n"
optionStr+="<MaxJobsRun:300/500/.../Dflt> : max jobs run on the same time in the cluster\n"
optionStr+="Attention*: before 1st run,check for SA-PROF.PATH_SCRIPTS, SA-PROF.SVM_MODEL, and SA-PROF.BLAST\n"
optionStr+="=========================================================================================\n"

def main(args):
    job = SAprof(args = args)
    job.start(check = True, verbose = True)


    if not job.cmd_line_status:
        sys.stderr.write("SA-Frag: Sorry, incorrect command line.\n")
        job.progressLog("SA-Frag : failed when verifying input data. Please check carefully.", "a")
        sys.exit(-1)

    # job.parse_specific()
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)

    job.run(job.options.verbose)


if __name__ == "__main__":

    main(sys.argv)
    sys.exit(0)


        
