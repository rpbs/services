#!/usr/bin/env python

# -- IMPORTS --

# Python standards modules
import sys
import os
import os.path
import shutil
import subprocess
import copy
import fnmatch
import random
import tarfile
import time
import stat
from optparse import OptionParser


# DRMAA context
try:
    import glob, time, pickle
    import drmaa

    sess = drmaa.Session()
    sess.initialize()

    # DRMAA wrappers
    # @note I don't really want to rewrite this, I'm sorry. -mv

    class JobFile:
        def __init__(self, fname, array_from, array_size):
            self.fname = fname
            self.array_from = array_from
            self.array_to = array_from + array_size - 1
        def __str__(self):
            return str(self.fname)

    class JobId:
        def __init__(self, jobfile):
            self.jobfile = jobfile
            self.jt = sess.createJobTemplate()
            self.jt.nativeSpecification = "-J safrag"
            self.jt.remoteCommand = "/usr/local/bin/drun"
            self.jt.args = ['safrag', self.jobfile.fname]
            self.joblist = []
        def run(self):
            self.joblist = sess.runBulkJobs(self.jt, self.jobfile.array_from, self.jobfile.array_to, 1)
            return self.joblist
        def wait(self):
            return sess.synchronize(self.joblist, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
        def __str__(self):
            return str(self.joblist)
except:
    pass

DFLT_WPATH          = "." #pas utile
DFLT_SGECPUS        = 10 #pas utile
DFLT_SGEQUEUE       = "all.q"
DFLT_SGEJOBLBL      = "PepFoldJob" #pas utile
SGE_NODE_NCPU       = 8 # pas utile
SGE_CHECK_SLEEP     = 10  # how many seconds do we have to wait between status checking ?
try:
    SGE_PATH        = "%s/bin/lx24-amd64" % os.environ["SGE_ROOT"]
except:
    # Default value
    SGE_PATH="/usr/bin"
    pass
    # sys.exit("No SGE environment detected. Is SGE installed ?\n")

def shellExec( cmd, stdinl = [], ignerr = False, verbose = 0 ):
    """
    Execute a shell command, with pipelines to stdin, stdout, and stderr.
    
    @param cmd    : the command line
    @param stdinl : string list to pipe as input
    @param ignerr : ignore stderr process content
    @param verbose: verbose mode

    @return: a tuple as (stdout lines list,stderr lines list)
    """

    if verbose:
        print >> sys.stderr, "ShellExec command: %s" % cmd

    if len( stdinl ):
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdin=subprocess.PIPE, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
        # Prepare stdin
        iStr = ""
        for aIn in stdinl:
            iStr += "%s\n" % aIn
        # Write to stdin
        p.stdin.write( "%s" % iStr )
        # Close pipe
        p.stdin.close()
    else:
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
    
    # Read stdout/err
    stdoutl = []
    stderrl = []
    while True:
        aStdout = p.stdout.readline()
        aStderr = p.stderr.readline()
        if verbose >1:
            print >> sys.stderr, aStdout,
        if verbose >1:
            print >> sys.stderr, aStderr,
        if aStdout:
            stdoutl.append( aStdout )
        if aStderr:
            stderrl.append( aStderr )
        if not aStdout and not aStderr:
            break

    if len(stderrl) and not ignerr:
        writeList( sys.stderr, stderrl )

    if verbose:
        print >> sys.stderr, ""

    return (stdoutl,stderrl)


def writeList( handle, lines, ret = 0, verbose = 0 ):
    """
    Write a string (list) to an open file handle

    @param handle : the file handle
    @param lines  : line(s) (list) to write
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """

    if isinstance( lines, str ):
        if ret:
            aStr = '%s\n' % lines
        else:
            aStr = '%s' % lines
        try:
            handle.write( aStr )
        except:
            print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % lines
            sys.exit(0)
    elif isinstance( lines, list ):
        for line in lines:
            if ret:
                aStr = '%s\n' % line
            else:
                aStr = '%s' % line
            try:
                handle.write( aStr )
            except:
                print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % line
                sys.exit(0)
    try:
        handle.flush()
    except:
        pass
    return

def SGE_job_status( jobId, verbose = 0 ):
    """
    Get SGE job state, given an id

    @param jobId  : SGE job identifier
    @param verbose: verbose mode
    """

    # Get queue info
    if verbose:
        print >> sys.stderr, "Will check jobid %s status :" % jobId,
        
    # cmd = "%s/qstat -j %s" % ( SGE_PATH, jobId)
    cmd = "%s/qstat" % ( SGE_PATH)
    (qstat, stderrl) = shellExec( cmd, verbose = 0 )
    # print qstat

    # Jobs is already finished
    if len(stderrl) :
        # Jobs no longer exists, finished
        if 'qstat: Unknown Job Id' in stderrl[0]:
            return 'E'
        else:
            # This is a real error
            print >> sys.stderr, "Error: an error occured while querying jobs status."
            sys.exit(1)
    
    jobs = []

    # Parse qstat
    for l in qstat:

        if l.count("Following jobs do not exist"):
            return None
        if l.count("job-ID"):
            continue
        if l.count("-----"):
            continue

        sp = l.split()

        if sp[0].strip() == jobId:
            state = sp[4].strip()
            if verbose:
                print >> sys.stderr, state
            return '%s' % state

    return None

def SGE_submit(jobFile, waitId = None, verbose = False):
    """
    SGESubmit: submit a jobFile to SGE
    
    @param jobFile: SGE script to submit
    @return: jobId
    """
    jobId = JobId(jobFile)
    jobId.run()
    return jobId
    # # Submit PBS script
    # if not waitId:
    #     cmd = "%s/qsub %s " % ( SGE_PATH, jobFile )
    # else:
    #     cmd = "%s/qsub -hold_jid %s %s " % ( SGE_PATH, waitId, jobFile )        

    # (stdoutl, stderrl) = shellExec( cmd, verbose = verbose )
    # jobId = None
    # for l in stdoutl:
    #     if l.count("Your job-array"):
    #         jobId = l.split()[2].split(".")[0]
    # # print stdoutl
    # if not jobId:
    #     sys.stderr.write("Could not submit %s. Is SGE active ?\n" % jobFile)
    # return jobId


def SGE_wait(jobId, wait_delay=SGE_CHECK_SLEEP, verbose = False):
    # status = SGE_job_status(jobId)
    # while (status != None):  
    #     if verbose:
    #         sys.stderr.write("Id: %s\t%s\n" % (jobId, status))
    #     time.sleep( wait_delay )
    #     status = SGE_job_status(jobId, verbose = verbose)
    return jobId.wait()

def SGE_wait_list(jobIds, wait_delay=SGE_CHECK_SLEEP, verbose = False):
    """
    jobIdList: type String, separator ",", For example: "465465,464554,679849,465454" 
    """
    jobIdList=jobIds
    if isinstance(jobIds, JobId):
        jobIdList = [jobIds]
    status="initialise"
    for jobId in jobIdList:
        SGE_wait(jobId, wait_delay, verbose)
    # while (status != None):  
    #     time.sleep( wait_delay )
    #     for jobId in jobIdList:
    #         status = SGE_job_status(jobId, verbose = verbose)
    #         if verbose:sys.stderr.write("Id: %s\t%s\n" % (jobId, status))
    #         if status != None: break
    return

def SGE_wait_maxRunList(jobIds,maxRun, wait_delay=SGE_CHECK_SLEEP, verbose = False):
    """
    jobIdList: type String, separator ",", For example: "465465,464554,679849,465454" 
    return: tabJobsRun: IDs des jobs pas encore fini
            nbJobLancer: nombre de jobs peut etre lancer, sans re-examine  
    """
    jobIdList=jobIds
    if isinstance(jobIds, JobId):
        jobIdList = [jobIds]
    SGE_wait_list(jobIdList, wait_delay, verbose)
    return [], len(jobIdList)
#     nbRun=100000
#     aff=True
#     while nbRun>=maxRun:
#         nbRun=0
#         listJobsRun=[]
#         for jobId in jobIdList:
#             status = SGE_job_status(jobId, verbose = verbose)
#             if verbose:sys.stderr.write("Id: %s\t%s\n" % (jobId, status))
#             if status != None: listJobsRun.append(jobId);nbRun+=1
#             if nbRun>=maxRun: 
# #                if aff: print "[Already %d Jobs running], please wait..."%nbRun,;aff=False
# #                else : print ".",
#                 if aff: sys.stdout.write("[Already %d Jobs running], please wait ..."%nbRun);aff=False
#                 else :  sys.stdout.write(".")
#                 time.sleep( wait_delay )
#                 break
#     if not aff: print        
#     print "[Only %d Jobs running], will submit %d Jobs, then ..."%(nbRun,maxRun-nbRun)
#     return listJobsRun,(maxRun-nbRun)


def SGE_script(script, queue = DFLT_SGEQUEUE, error = "SGE", array_size = None, array_from = 1, verbose = False,OutErrFile=True):
    """
    Wrap script for SGE submission, return lines
    @param script: a string of the script to run
    @param queue : SGE queue to submit script to
    @param array_size : specify job array and its size
    
    @return : script with correct headers
    """

    return JobFile(script, array_from, array_size)
    # if OutErrFile : rs = "#!/bin/bash\n#$ -q %s\n#$ -cwd\n#$ -V\n#$ -o %s.out\n#$ -e %s.err\n#$ -N %s" % (queue,error,error,error)
    # else : rs = "#!/bin/bash\n#$ -q %s\n#$ -cwd\n#$ -V\n#$ -o %s.out\n#$ -e /dev/null\n#$ -N %s" % (queue,error,error)
    # if array_size:
    #     rs = "%s\n#$ -t %d-%d\n" % (rs, array_from, array_from + array_size - 1)
    # rs = "%s\n\n%s" % (rs, script)
    # return rs


def SGE_script_creat(script,fileName,queue = DFLT_SGEQUEUE, error = "SGE", array_size = None, array_from = 1, verbose = False,OutErrFile=True):
    """
    same as SGE_script, but save in file
    """  
    script = SGE_script(script, queue, error, array_size, array_from, verbose,OutErrFile)
    f = open(fileName,"w")
    f.write("%s" % script)
    f.close()
    st = os.stat(fileName)
    os.chmod(fileName, st.st_mode | stat.S_IEXEC)
    return JobFile(os.path.abspath(fileName), array_from, array_size)


if __name__ == "__main__":
    """
    " "=@
    SGE.py cat@aa.txt@|grep@a
    """
    cmd = " ".join(sys.argv[1].split("@"))
#    cmd = " ".join("echo@aa".split("@"))
    print cmd
    SGE_script_creat(cmd,"submit.sh","all.q")
    sys.exit()
    jobId = SGE_submit("submit.sh")
    
#    f = open(sys.argv[1])
#    lines = f.readlines()
#    f.close()
#    script = reduce( lambda x,y: x+y, lines)
#    script = SGE_script(script, array_size=10)
#    f = open(sys.argv[1]+".sge","w")
#    f.write("%s" % script)
#    f.close()
#    jobId = SGE_submit(sys.argv[1]+".sge")
#    # print jobId
#    SGE_wait(jobId, verbose = True)
