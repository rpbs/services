#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "Types.h"
#include "argstr.h"



double W1[27][27] = {
// A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a [dflt]
  { 5.32551808595549 , 1.91279782483781 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.81383972280479 , 2.88886300258078 , 0 , 0 , 1.01439766211418 , 2.33384938282058                                                                                 }, // A 
  { 1.91279782483781 , 7.782113815988 , 0 , 0 , 3.38250987331947 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.36184241787187 , 2.11252095557752 , 0 , 0 , 1.79947328025614 , 2.71611622095099									}, // B 
  { 0 , 0 , 12.3952706417414 , 0 , 0 , 1.72499473792429 , 0 , 0 , 2.28224589546139 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.63794973524835 , 0 , 0 , 0 , 0 , 3.94685871303005 , 0												}, // C 
  { 0 , 0 , 0 , 12.9077641189144 , 0 , 2.43565569610535 , 0 , 1.13707480026868 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.31458496520646 , 0 , 1.34296922209498 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0												}, // D 
  { 0 , 3.38250987331947 , 0 , 0 , 11.8283122397808 , 1.80319483509555 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 3.13108315312427 , 0 , 2.06246939432202 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.50770857762712 , 0											}, // E 
  { 0 , 0 , 1.72499473792429 , 2.43565569610535 , 1.80319483509555 , 10.8995336073158 , 1.96748484773206 , 1.07207711961455 , 1.14989657091997 , 2.66916582899404 , 0 , 0 , 0 , 0 , 2.01258708213395 , 1.70443725408535 , 0 , 2.48143246430247 , 1.21702213255669 , 0 , 3.4625073698925 , 0 , 0 , 0 , 2.01890644305902 , 1.31842652624668 , 0   }, // F 
  { 0 , 0 , 0 , 0 , 0 , 1.96748484773206 , 9.92238186664221 , 0 , 0 , 0 , 0 , 0 , 1.49879282385661 , 0 , 1.34506857919094 , 2.51995992847980 , 0 , 1.92972981328449 , 1.3993110383103 , 1.50316366942219 , 0 , 0 , 0 , 0 , 0 , 0 , 0													        }, // G 
  { 0 , 0 , 0 , 1.13707480026868 , 0 , 1.07207711961455 , 0 , 11.0259325192397 , 0 , 0 , 1.53697008326788 , 0 , 0 , 0 , 0 , 2.74282112943847 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 3.00386862623149 , 0 , 0																	        }, // H 
  { 0 , 0 , 2.28224589546139 , 0 , 0 , 1.14989657091997 , 0 , 0 , 10.7975364324167 , 1.75418787407276 , 0 , 0 , 0 , 0 , 0 , 0 , 2.30263242254611 , 1.42361868536533 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // I 
  { 0 , 0 , 0 , 0 , 0 , 2.66916582899404 , 0 , 0 , 1.75418787407276 , 11.6887167950258 , 1.77849766588680 , 1.54267357680553 , 1.06892556593766 , 1.02578892555064 , 0 , 0 , 1.46574405366631 , 1.24034181637860 , 0 , 1.07923342775410 , 1.78640460616007 , 0 , 0 , 1.87734022280706 , 3.35557452441505 , 0 , 0			        }, // J 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.53697008326788 , 0 , 1.77849766588680 , 8.99465039450872 , 3.57326591598027 , 0 , 0 , 0 , 0 , 0 , 1.49145029663145 , 0 , 0 , 0 , 0 , 0 , 0 , 3.63837080350681 , 0 , 0																	        }, // K 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.54267357680553 , 3.57326591598027 , 7.79039060130642 , 2.57209051959539 , 2.14110917321361 , 0 , 1.75148324933766 , 0 , 1.75728850691133 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // L 
  { 0 , 0 , 0 , 0 , 0 , 0 , 1.49879282385661 , 0 , 0 , 1.06892556593766 , 0 , 2.57209051959539 , 7.51696901352156 , 2.894568355765 , 0 , 1.82227186479836 , 0 , 0 , 0 , 3.34900738853154 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // M 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.02578892555064 , 0 , 2.14110917321361 , 2.894568355765 , 7.85689114835425 , 0 , 0 , 0 , 2.72898296638236 , 0 , 3.64420414479284 , 0 , 0 , 0 , 2.43350109436619 , 0 , 0 , 0															        }, // N 
  { 0 , 0 , 0 , 0 , 3.13108315312427 , 2.01258708213395 , 1.34506857919094 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 13.5336340752259 , 0 , 1.89134459111515 , 0 , 2.95902848519303 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // O 
  { 0 , 0 , 0 , 1.31458496520646 , 0 , 1.70443725408535 , 2.51995992847980 , 2.74282112943847 , 0 , 0 , 0 , 1.75148324933766 , 1.82227186479836 , 0 , 0 , 8.83327640865417 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // P 
  { 0 , 0 , 0 , 0 , 2.06246939432202 , 0 , 0 , 0 , 2.30263242254611 , 1.46574405366631 , 0 , 0 , 0 , 0 , 1.89134459111515 , 0 , 9.11562523633216 , 0 , 3.09327376209149 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // Q 
  { 0 , 0 , 0 , 1.34296922209498 , 0 , 2.48143246430247 , 1.92972981328449 , 0 , 1.42361868536533 , 1.24034181637860 , 1.49145029663145 , 1.75728850691133 , 0 , 2.72898296638236 , 0 , 0 , 0 , 12.5968905278827 , 2.32799411047197 , 0 , 0 , 0 , 0 , 1.98156908888022 , 0 , 0 , 0							        }, // R 
  { 0 , 0 , 0 , 0 , 0 , 1.21702213255669 , 1.3993110383103 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.95902848519303 , 0 , 3.09327376209149 , 2.32799411047197 , 9.9178483741111 , 0 , 0 , 0 , 0 , 1.52303491683071 , 0 , 0 , 0															        }, // S 
  { 0 , 0 , 0 , 0 , 0 , 0 , 1.50316366942219 , 0 , 0 , 1.07923342775410 , 0 , 0 , 3.34900738853154 , 3.64420414479284 , 0 , 0 , 0 , 0 , 0 , 8.11922738486984 , 0 , 0 , 0 , 3.25847729879289 , 0 , 0 , 0																	        }, // T 
  { 0 , 0 , 2.63794973524835 , 0 , 0 , 3.4625073698925 , 0 , 0 , 0 , 1.78640460616007 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 12.2006594066014 , 0 , 0 , 0 , 0 , 1.06476806915121 , 0																			        }, // U 
  { 2.81383972280479 , 2.36184241787187 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 6.65173019150024 , 2.3728695420887 , 0 , 0 , 3.24358435262505 , 2.55346688871063																	        }, // V 
  { 2.88886300258078 , 2.11252095557752 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.3728695420887 , 6.96537885593415 , 0 , 0 , 2.43819497207371 , 2.65238702843616																	        }, // W 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.87734022280706 , 0 , 0 , 0 , 2.43350109436619 , 0 , 0 , 0 , 1.98156908888022 , 1.52303491683071 , 3.25847729879289 , 0 , 0 , 0 , 10.1285584186043 , 0 , 0 , 0																	        }, // X 
  { 0 , 0 , 0 , 0 , 0 , 2.01890644305902 , 0 , 3.00386862623149 , 0 , 3.35557452441505 , 3.63837080350681 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 11.8231798660744 , 0 , 0																		        }, // Y 
  { 1.01439766211418 , 1.79947328025614 , 3.94685871303005 , 0 , 1.50770857762712 , 1.31842652624668 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.06476806915121 , 3.24358435262505 , 2.43819497207371 , 0 , 0 , 7.70354028140104 , 2.27562331615745									        }, // Z 
  { 2.33384938282058 , 2.71611622095099 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.55346688871063 , 2.65238702843616 , 0 , 0 , 2.27562331615745 , 8.96886378761276																	        }  // a 
};

double W2[27][27] = {
// A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a
  { 5.32551808595549 , 1.91279782483781 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.81383972280479 , 2.88886300258078 , 0 , 0 , 1.01439766211418 , 2.33384938282058                                                                                 }, // A 
  { 1.91279782483781 , 7.782113815988 , 0 , 0 , 3.38250987331947 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.36184241787187 , 2.11252095557752 , 0 , 0 , 1.79947328025614 , 2.71611622095099									}, // B 
  { 0 , 0 , 12.3952706417414 , 0 , 0 , 1.72499473792429 , 0 , 0 , 2.28224589546139 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.63794973524835 , 0 , 0 , 0 , 0 , 3.94685871303005 , 0												}, // C 
  { 0 , 0 , 0 , 12.9077641189144 , 0 , 2.43565569610535 , 0 , 1.13707480026868 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.31458496520646 , 10 , 1.34296922209498 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0												}, // D 
  { 0 , 3.38250987331947 , 0 , 0 , 11.8283122397808 , 1.80319483509555 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 3.13108315312427 , 0 , 2.06246939432202 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.50770857762712 , 0											}, // E 
  { 0 , 0 , 1.72499473792429 , 2.43565569610535 , 1.80319483509555 , 10.8995336073158 , 1.96748484773206 , 1.07207711961455 , 1.14989657091997 , 2.66916582899404 , 0 , 0 , 0 , 0 , 2.01258708213395 , 1.70443725408535 , 0 , 2.48143246430247 , 1.21702213255669 , 0 , 3.4625073698925 , 0 , 0 , 0 , 2.01890644305902 , 1.31842652624668 , 0   }, // F 
  { 0 , 0 , 0 , 0 , 0 , 1.96748484773206 , 9.92238186664221 , 0 , 0 , 0 , 0 , 0 , 1.49879282385661 , 0 , 1.34506857919094 , 2.51995992847980 , 0 , 1.92972981328449 , 1.3993110383103 , 1.50316366942219 , 0 , 0 , 0 , 0 , 0 , 0 , 0													        }, // G 
  { 0 , 0 , 0 , 1.13707480026868 , 0 , 1.07207711961455 , 0 , 11.0259325192397 , 0 , 0 , 1.53697008326788 , 0 , 0 , 0 , 0 , 2.74282112943847 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 3.00386862623149 , 0 , 0																	        }, // H 
  { 0 , 0 , 2.28224589546139 , 0 , 0 , 1.14989657091997 , 0 , 0 , 10.7975364324167 , 1.75418787407276 , 0 , 0 , 0 , 0 , 0 , 0 , 2.30263242254611 , 1.42361868536533 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // I 
  { 0 , 0 , 0 , 0 , 0 , 2.66916582899404 , 0 , 0 , 1.75418787407276 , 11.6887167950258 , 1.77849766588680 , 1.54267357680553 , 1.06892556593766 , 1.02578892555064 , 0 , 0 , 1.46574405366631 , 1.24034181637860 , 0 , 1.07923342775410 , 1.78640460616007 , 0 , 0 , 1.87734022280706 , 3.35557452441505 , 0 , 0			        }, // J 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.53697008326788 , 0 , 1.77849766588680 , 8.99465039450872 , 3.57326591598027 , 0 , 0 , 0 , 0 , 0 , 1.49145029663145 , 0 , 0 , 0 , 0 , 0 , 0 , 3.63837080350681 , 0 , 0																	        }, // K 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.54267357680553 , 3.57326591598027 , 7.79039060130642 , 2.57209051959539 , 2.14110917321361 , 0 , 1.75148324933766 , 0 , 1.75728850691133 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // L 
  { 0 , 0 , 0 , 0 , 0 , 0 , 1.49879282385661 , 0 , 0 , 1.06892556593766 , 0 , 2.57209051959539 , 7.51696901352156 , 2.894568355765 , 0 , 1.82227186479836 , 0 , 0 , 0 , 3.34900738853154 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // M 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.02578892555064 , 0 , 2.14110917321361 , 2.894568355765 , 7.85689114835425 , 0 , 0 , 0 , 2.72898296638236 , 0 , 3.64420414479284 , 0 , 0 , 0 , 2.43350109436619 , 0 , 0 , 0															        }, // N 
  { 0 , 0 , 0 , 0 , 3.13108315312427 , 2.01258708213395 , 1.34506857919094 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 13.5336340752259 , 0 , 1.89134459111515 , 0 , 2.95902848519303 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // O 
  { 0 , 0 , 0 , 1.31458496520646 , 0 , 1.70443725408535 , 2.51995992847980 , 2.74282112943847 , 0 , 0 , 0 , 1.75148324933766 , 1.82227186479836 , 0 , 0 , 8.83327640865417 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0															        }, // P 
  { 0 , 0 , 0 , 10 , 2.06246939432202 , 0 , 0 , 0 , 2.30263242254611 , 1.46574405366631 , 0 , 0 , 0 , 0 , 1.89134459111515 , 0 , 9.11562523633216 , 0 , 3.09327376209149 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0																	        }, // Q 
  { 0 , 0 , 0 , 1.34296922209498 , 0 , 2.48143246430247 , 1.92972981328449 , 0 , 1.42361868536533 , 1.24034181637860 , 1.49145029663145 , 1.75728850691133 , 0 , 2.72898296638236 , 0 , 0 , 0 , 12.5968905278827 , 2.32799411047197 , 0 , 0 , 0 , 0 , 1.98156908888022 , 0 , 0 , 0							        }, // R 
  { 0 , 0 , 0 , 0 , 0 , 1.21702213255669 , 1.3993110383103 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.95902848519303 , 0 , 3.09327376209149 , 2.32799411047197 , 9.9178483741111 , 0 , 0 , 0 , 0 , 1.52303491683071 , 0 , 0 , 0															        }, // S 
  { 0 , 0 , 0 , 0 , 0 , 0 , 1.50316366942219 , 0 , 0 , 1.07923342775410 , 0 , 0 , 3.34900738853154 , 3.64420414479284 , 0 , 0 , 0 , 0 , 0 , 8.11922738486984 , 0 , 0 , 0 , 3.25847729879289 , 0 , 0 , 0																	        }, // T 
  { 0 , 0 , 2.63794973524835 , 0 , 0 , 3.4625073698925 , 0 , 0 , 0 , 1.78640460616007 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 12.2006594066014 , 0 , 0 , 0 , 0 , 1.06476806915121 , 0																			        }, // U 
  { 2.81383972280479 , 2.36184241787187 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 6.65173019150024 , 2.3728695420887 , 0 , 0 , 3.24358435262505 , 2.55346688871063																	        }, // V 
  { 2.88886300258078 , 2.11252095557752 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.3728695420887 , 6.96537885593415 , 0 , 0 , 2.43819497207371 , 2.65238702843616																	        }, // W 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.87734022280706 , 0 , 0 , 0 , 2.43350109436619 , 0 , 0 , 0 , 1.98156908888022 , 1.52303491683071 , 3.25847729879289 , 0 , 0 , 0 , 10.1285584186043 , 0 , 0 , 0																	        }, // X 
  { 0 , 0 , 0 , 0 , 0 , 2.01890644305902 , 0 , 3.00386862623149 , 0 , 3.35557452441505 , 3.63837080350681 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 11.8231798660744 , 0 , 0																		        }, // Y 
  { 1.01439766211418 , 1.79947328025614 , 3.94685871303005 , 0 , 1.50770857762712 , 1.31842652624668 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1.06476806915121 , 3.24358435262505 , 2.43819497207371 , 0 , 0 , 7.70354028140104 , 2.27562331615745									        }, // Z 
  { 2.33384938282058 , 2.71611622095099 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 2.55346688871063 , 2.65238702843616 , 0 , 0 , 2.27562331615745 , 8.96886378761276																	        }  // a 
};

double W[27][27] = {
// A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a
  { 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // A 
  { 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // B
  { 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // C
  { 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0.4 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // D
  { 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // E
  { 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // F
  { 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // G
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // H
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // I
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // J
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // K
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // L
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // M
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // N
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // O
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // P
  { 0 , 0 , 0 , 0.4 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0.6 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // Q
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // R 
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // S
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0  },  // T
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0  },  // U
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0  },  // V
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0  },  // W
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0  },  // X
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 0  },  // Y
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0  },  // Z
  { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1  }   // a
};

/* ------------------------------------------------------------
 * Un petit utilitaire ...
 * ------------------------------------------------------------
 */
void splitLine(char *line,  char **tok, int *nTok)
{
  *nTok = 0;
  tok[0] = strtok(line," ");
  while ((tok[++(*nTok)] = strtok(NULL," ")) != NULL);
}


/* =====================================
 * input probas from file
 * return a DtProb pointer
 * =====================================
 */
DtProb *probInput(char *fname, int prfSze)
{
  DtProb *p = NULL;
  FILE   *fp;
  DtStr   buff;
  int     nLines;
  char   *pC;
  char   *tok[27 + 30];
  int     nTok;
  int     aState;
  int     aPos;


  p = (DtProb *) calloc(1, sizeof(DtProb));
  p->sze = prfSze;

  if (!strcmp(fname,"stdin")) {
    fp = stdin;
  } else {
    if ((fp = fopen(fname,"r")) == NULL) {
      fprintf(stderr,"Sorry: could not open file %s\n",fname);
      exit(0);
    }
    /* fprintf(stderr,"OK1: could open file %s\n",fname); */
  }

  aPos = 0;
  nLines = 0;
  while (fgets(buff,BUFSIZ,fp) != NULL) {
    nLines++;
  }
  rewind(fp);

  p->p = calloc(nLines, sizeof(DtVec27));

  while (fgets(buff,BUFSIZ,fp) != NULL) {
    if ((pC = strchr(buff,'\n')) != NULL) *pC = '\000';
    splitLine(buff,  tok, &nTok);
    if (!nTok) continue;

    if (nTok != 27) {
      fprintf(stderr,"Sorry: line %d does not has %d probabilities (found %d)\n", aPos, 27, nTok);
      fflush(stderr);
      exit(0);
    }
    for (aState = 0; aState < 27; aState++) {
      sscanf(tok[aState],"%lf",&p->p[aPos][aState]);
    }
    aPos ++;
  }
  p->n = aPos;

  fclose(fp);

  /* 1. How many lines */

  /* 2. Alloc memory */

  /* 3. Read data */

  /* 4. return */
  return p;
}

void probOutput(char *fname, DtProb *p)
{
  FILE *fp;
  int aPos, aBit;

  if (!strcmp(fname,"stderr")) {
    fp = stderr;
  } else if (!strcmp(fname,"stdout")) {
    fp = stdout;
  } else {
    fp = fopen(fname,"w");
  }
  for (aPos = 0; aPos < p->n; aPos ++) {
    fprintf(fp, "%d : ", aPos);
    for (aBit = 0; aBit < p->sze; aBit ++) {
      fprintf(fp, "%.6lf ",p->p[aPos][aBit]);
    }
    fprintf(fp, "\n");
  }
  if ((fp != stderr) && (fp != stdout)) fclose(fp);
}

/* ===================================
 * Return a DtProb, subset of Prob
 * tto = -1 implies up to end
 * ===================================
 */
DtProb *probFromTo(DtProb *p, int ffrom, int tto)
{
  DtProb *rs = NULL;
  int     aPos;


  rs = (DtProb *) calloc(1, sizeof(DtProb));
  if (tto == -1) tto = p->n;
  fprintf(stderr, "probFromTo %d %d\n", ffrom, tto);
  rs->n = tto - ffrom;
  rs->sze = p->sze;
  rs->p = calloc(rs->n, sizeof(DtVec27));
  for (aPos = ffrom; aPos < tto; aPos ++) {
    memcpy(&rs->p[aPos-ffrom], &p->p[aPos], sizeof(DtVec27));
  }

  return rs;
}

/* ============================================
 * get nbest probs.
 * setup other to 0
 * normalize
 * ============================================
 */
int compar(void *a1, void *b1) {
  double *a, *b;
  a = (double *) a1;
  b = (double *) b1;
  if (a[0] < b[0]) return 1;
  if (a[0] > b[0]) return -1;
  return 0;
}

void nBestProbs(DtProb *p, int nBest, DtPoint2 *ps)
{
  int aPos, aBit, i;
  double sum;

  for (aPos = 0; aPos < p->n; aPos++) {
    for (aBit = 0; aBit < p->sze; aBit++) {
      ps[aBit][0] = p->p[aPos][aBit];
      ps[aBit][1] = (double) aBit;
    }
    qsort(ps, p->sze, sizeof(DtPoint2), compar);
    for (aBit = nBest; aBit < p->sze; aBit++) {
      i = (int) ps[aBit][1];
      p->p[aPos][i] = 0.;
    }
#if 0
    for (aBit = 0; aBit < p->sze; aBit++) {
      fprintf(stderr,"%.0lf %lf\n", ps[aBit][1], ps[aBit][0]);
    }
#endif

    sum = 0.;
    for (aBit = 0; aBit < p->sze; aBit++) {
      sum += p->p[aPos][aBit];
    }
    for (aBit = 0; aBit < p->sze; aBit++) {
      p->p[aPos][aBit] /= sum;
    }
#if 0
    for (aBit = 0; aBit < p->sze; aBit++) {
      fprintf(stderr,"%lf ", p->p[aPos][aBit]);
    }
    fprintf(stderr,"\n");

    exit(0);
#endif
  }

}

void Dirichet(double *p, int sze, double K)
{
  double wght;
  int i;

  wght = 1. + (float) sze * K;
  // fprintf(stderr,"Dirichlet weight %lf\n", wght);

  for (i=0; i<sze; i++) {
    // fprintf(stderr,"before %lf ", p[i]);
    p[i] = (p[i] + K) / (wght);
    // fprintf(stderr,"after %lf\n", p[i]);
  }
}

void PrfDirichlet(DtProb *p, double K)
{
  int aPos;
  int i;
  for (aPos = 0; aPos < p->n; aPos++) {
    Dirichet(p->p[aPos], p->sze, K);
#if 0
    for (i=0; i<p->sze; i++) {
      fprintf(stderr,"%lf ", p->p[aPos][i]);
    }
    fprintf(stderr,"\n");
    // exit(0);
#endif
  }
}

/* =======================================
 * Yimin motifier prob 
 * =======================================
 */

// double min(double a, double b)
// {
//  if(a<b)
//   return a;
//  else 
//   return b;
// }
// 
// 
// DtVec27 ConfuProb(DtVec27 p1,DtVec27 p2)
// {
//   double move1,move2;
//   int i,j;
//   for (i=0; i<27; i++) {
//     if (p1[i]>p2[i]){
//       for (j=0; j<27; j++) {
// 	  if (W[i][j]>0 && p1[j]<p2[j]){
// 	      move1=min((p1[i]-p2[i]),(p2[j]-p1[j]));
// 	      p2[i]=p2[i]+move1;p2[j]=p2[j]-move1;
// 	      if (p1[i]==p2[i]) break;
// 	  }
//       }
//     }
//     if (p1[i]<p2[i]){
//       for (j=0; j<27; j++) {
// 	  if (W[i][j]>0 && p1[j]>p2[j]){
// 	      move2=min((p2[i]-p1[i]),(p1[j]-p2[j]));
// 	      p2[i]=p2[i]-move2;p2[j]=p2[j]+move2;
// 	      if (p1[i]==p2[i]) break;
// 	  }
//       }
//     }
//   }
//   return p2;
// }


/* =======================================
 * Jensen Shannon score for 2 vectors
 * better to in put pseudo counts to order 0 values ...
 * but this one does not manage it.
 * Not tested yet.
 * =======================================
 */
double JSscore(DtVec27 p1,
		DtVec27 p2)
{
  double  rs = 0.;
  double  s1, s2;
  DtVec27 m;
  int     i;

//   p2=ConfuProb(p1, p2);
  
  for (i=0; i<27; i++) {
    m[i] = (p1[i] + p2[i]) / 2.;
    //printf("m: %lf\n",i,m[i]);
  }
  
  s1 = 0.;
  for (i=0; i<27; i++) {
    s1 += p1[i]*log2(p1[i]/m[i]);
    //printf("s1: %lf\n",(p1[i]*log2(p1[i]/m[i])));
  }

  s2 = 0.;
  for (i=0; i<27; i++) {
    s2 += p2[i]*log2(p2[i]/m[i]);
    //printf("s2: %lf\n",(p2[i]*log2(p2[i]/m[i])));
  }
  return (s1+s2) / (2.);
}





void normalizeW(double W[27][27])
{
  double s;
  int i, j;

  for (i=0; i<27; i++) {
    s = 0.;
    for (j=0; j<27; j++) {
      s+=W[i][j];
    }
    for (j=0; j<27; j++) {
      W[i][j] /= s;
    }
  }
}

/* =====================================================
 * weighted JS
 * 
 * Sum_i (Sum_j pi ln (2. pi / pi+pj) + Sum_j pj ln (2. pj / pj + pi))
 * =====================================================
 */
double WJSscore(DtVec27 p1, 
		DtVec27 p2)
{
  
  double  rs = 0.;
  double  s1, s2;
  int     i, j;

  
  s1 = 0.;
  for (i=0; i<27; i++) {
    for (j=0; j<27; j++) {
      s1 += W[i][j] * p1[i]*log2(2.*p1[i]/(p1[i] + p2[j]));
    }
  }
  s2 = 0.;
  for (i=0; i<27; i++) {
    for (j=0; j<27; j++) {
      s2 += W[i][j] * p2[i]*log2(2.*p2[i]/(p1[j] + p2[j]));
    }
  }
  return (s1+s2) / (2.);
}

/* Soding like score for 2 vectors */
double LOscore(DtVec27 p1, 
	       DtVec27 p2)
{
  double rs = 0.;
  
  return rs;
}

/* Sliding scan for seed against whole profile */
double sliceScore(DtProb *seed, DtProb *target, int tPos)
{
  int aPos;
  double lrs;
  double un;

  lrs = 0.;
  for (aPos = 0; aPos < seed->n; aPos++ ) {
    
    un = JSscore(seed->p[aPos], target->p[aPos+tPos]);
//     un = WJSscore(seed->p[aPos], target->p[aPos+tPos]);
    lrs += un;
    printf("%lf ",un);
  }
  printf("\n");
  lrs /= (float) seed->n;

  return lrs;
}

double sliceMaxScore(DtProb *seed, DtProb *target, int tPos)
{
  int aPos;
  double rs, lrs;

  rs = 0.;
  for (aPos = 0; aPos < seed->n; aPos++ ) {
    lrs = JSscore(seed->p[aPos], target->p[aPos+tPos]);
//     lrs = WJSscore(seed->p[aPos], target->p[aPos+tPos]);
    printf("%lf ",lrs);
    if (lrs > rs) rs = lrs;
  }
  printf("\n");
  return rs;
}

double sliceMaxScore2(DtProb *seed, DtProb *target, int tPos, double maxValue, double *detail)
{
  int aPos;
  double rs, lrs;

  rs = 0.;
  for (aPos = 0; aPos < seed->n; aPos++ ) {
    lrs = JSscore(seed->p[aPos], target->p[aPos+tPos]);
//     lrs = WJSscore(seed->p[aPos], target->p[aPos+tPos]);
    if (lrs > maxValue) return maxValue + 0.1;
    detail[aPos] = lrs;
    // if (outdetail) printf("%lf ",lrs);
    if (lrs > rs) rs = lrs;
  }
  // if (outdetail) printf("\n");
  return rs;
}

int main(int argc,char *argv[])
{
  FILE    * logfp;
  FILE    * ofp;
  DtProb  * pSeedPrf;
  DtProb  * pRegionPrf;
  DtProb  * pTargetPrf;
  DtStr     ITargetFichName;
  char   ** Ids;
  double    score;
  double    JSVect[MAXFRGLEN];
  DtPoint2 *ps;
  int       nSearch;
  int       prfSze = 27;
  int       nIds;
  int       aId;
  int       aPos;
  int       lPos;
  int       nHits;
  /* int       maxHits = 100; */
  int       over = 0;

  /* 1. Parse command line args */
  /* seed, ffrom, tto */
  /* List of files to search */
  /* Data directory */

  /* 1: On lit la ligne de commande */
  parseArgstr(argc, argv);

  if (!strcmp(OLstFichName,"stderr")) {
    logfp = stderr;
  } else if  (!strcmp(OLstFichName,"stdout")){
    logfp = stdout;
  } else {
    logfp = fopen(OLstFichName,"w");
  }

  if (!strcmp(OFichName,"stderr")) {
    ofp = stderr;
  } else if  (!strcmp(OFichName,"stdout")){
    ofp = stdout;
  } else {
    ofp = fopen(OFichName,"w");
  }
  if (gVerbose) {
    prmtrsSumary(logfp);
  }

  /* 2. Input data */
  normalizeW(W);
  
  /* 2.1 input seed */
  if (IPrfFichName[0] == '\000') {
    fprintf(stderr, "Sorry, not seed profile specified. (-iPrf)\n");
    exit(0);
  }
  pSeedPrf = probInput(IPrfFichName, prfSze);
  // probOutput("stderr", pSeedPrf);

  /* 2.2 extract region of interest */
  pRegionPrf = probFromTo(pSeedPrf, gFrom, gTo);
  // probOutput("stderr", pRegionPrf);
  ps = (DtPoint2 *) calloc(pRegionPrf->sze, sizeof(DtPoint2));
  nBestProbs(pRegionPrf, gNBest, ps);
  /* Take nbest, apply Dirichlet */
  PrfDirichlet(pRegionPrf, gKDirich);
  // probOutput("stderr", pRegionPrf);
  // exit(0);

  /* 2.2 input list of files to search */
  if (IIdsFichName[0] == '\000') {
    fprintf(stderr, "Sorry, not Id list specified. (-iIds)\n");
    exit(0);
  }

  Ids = txtFileRead(IIdsFichName, &nIds);

  /* 3. perform search against bank, iteratively */
  // fprintf(ofp,"seed target sfrom sto tfrom tto\n");
  nHits = 0;
  for (aId = 0; aId < nIds; aId++) {
    // fprintf(stderr, "%s\n", Ids[aId]);

    /* 3.1 input target */
    sprintf(ITargetFichName,"%s/%s", gDBPATH, Ids[aId]);
    // sprintf(ITargetFichName,"%s", Ids[aId]);
    pTargetPrf = probInput(ITargetFichName, prfSze);
    nBestProbs(pTargetPrf, gNBest, ps);

    PrfDirichlet(pTargetPrf, gKDirich);
    // probOutput("stderr", pTargetPrf);

    /* 3.2 search matches */
    nSearch = pTargetPrf->n - pRegionPrf->n + 1;
    if (nSearch < 0) {
      fprintf(stderr,"%s: Target is smaller than seed (%d / %d)\n", Ids[aId], pTargetPrf->n, pRegionPrf->n);
      continue;
    }
    for (aPos = 0; aPos < nSearch; aPos++) {
      if (gUseMean) {
	score = sliceScore(pRegionPrf, pTargetPrf, aPos);
      } else {
	/* score = sliceMaxScore(pRegionPrf, pTargetPrf, aPos); */
	score = sliceMaxScore2(pRegionPrf, pTargetPrf, aPos, gMaxScore, JSVect);
      }
      //  fprintf(stderr,"%s %d : %lf\n", Ids[aId], aPos, score);
      /* 3.3 output results */
      // fprintf(logfp,"%s %s %d %d %d %d %lf\n", IPrfFichName, Ids[aId], gFrom + 1, gTo, aPos + 1, aPos + pRegionPrf->n, score);
      if (score < gMaxScore) {
	// fprintf(logfp,"%s %s %d %d %d %d %lf\n", IPrfFichName, Ids[aId], gFrom + 1, gTo, aPos + 1, aPos + pRegionPrf->n, score);
	if  (gOutJS) {
	  for (lPos = 0; lPos < pRegionPrf->n; lPos++) {
	    fprintf(stdout, "%lf ",JSVect[lPos]);
	  }
	  fprintf(stdout, "\n");
	}
	fprintf(ofp,"%s %s %d %d %d %d %lf\n", IPrfFichName, Ids[aId], gFrom + 1, gTo, aPos + 1, aPos + pRegionPrf->n, score);
	nHits ++;
	if (nHits > gMaxHits) { // avoid to find all helices from PDB ...
	  over = 1;
	  break;
	}
      }
    }

    if (over) break;
  }
  /* 4. Termination */
  if ((logfp != stderr) && (logfp != stdout)) fclose(logfp);
  if ((ofp != stderr) && (ofp != stdout)) fclose(ofp);
}


