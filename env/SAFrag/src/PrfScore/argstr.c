#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "Types.h"

int DEBUG = 0;

int gPreFilter  = 1;
int gClusterize = 0;

int gFrom       = 0;
int gTo         = -1;

int gUseMean    = 0;

int gHeapSze = 3000;
int gFltrRndFrom = 100;
int gFltrHeapSze = 300;

int gMaxHits     = 1000;

int gNDECOYS = 0;
DtStr ODecoysName = "";

DtStr IPrfFichName    = "";
DtStr IIdsFichName    = "";
DtStr OLstFichName    = "stderr";
DtStr OFichName       = "stdout";

/* ************************************************* */
/* JM - 05/09/05 - Added Backbone rebuilding support */
/* ************************************************* */
DtStr ICATraceFichName = "";
/* ************************************************* */
/*        ####### JM - 05/09/05 - end ######         */
/* ************************************************* */

int   gCstSeed = 0;

#define DcDFLTDBPATH "/home/raid5/HMM/SBBDB/"

DtStr gDBPATH = ".";

double gKDirich  = 0.000001;
double gMaxScore = 1.;

int gOutJS       = 0;

int    gNBest    = 13;

int gVerbose     = 0;

void delimiter(FILE *f)
{
  fprintf(f,"----------------------------------------------------\n");
}

/* ------------------------------------------------------------
 * Possibles arguments de la ligne de commande. --------------- 
 * ------------------------------------------------------------
 */
void printUsage()
{
  fprintf(stderr,"CABuilder: a program to reconstruct a 3D structure from SA + objective alpha Carbons description.\n");
  fprintf(stderr,"  usage: CABuilder <args>\n");
  fprintf(stderr,"\nGeneric parameters:\n");
  /*  fprintf(stderr,"         -iSBBs <SBBFich>  : Fichier sequence SBB proteine\n"); */
  fprintf(stderr,"         -iPrf   <PrfFich>  : Fichier profil seed (stdin)\n");
  fprintf(stderr,"         -iIds   <ListFich> : List of Ids to scan \n");
  fprintf(stderr,"         -o      <OutFich>  : Results (stdout)\n");
  fprintf(stderr,"         -olog    <logFich>  : logs (stderr)\n");
  fprintf(stderr,"         -from   <-1>       : build from\n");
  fprintf(stderr,"         -to     <-1>       : build up to\n");
  fprintf(stderr,"         -dbPath <.>        : path to files to scan\n");
  fprintf(stderr,"         -pcK    <10-6>     : pseudo count value\n");
  fprintf(stderr,"         -maxScore <1.>     : max distance value to accept match\n");
  fprintf(stderr,"         -nBest <13>        : compare profiles up to n best values. Others are ignored (set to 0.)\n");
  fprintf(stderr,"         -mean              : use mean sccore instead of max. \n");
  fprintf(stderr,"         -maxHits <1000>    : return up to maxHits matches. \n");
  fprintf(stderr,"         -outJS             : output JS per fragment position. \n");

  fprintf(stderr,"\nmisc. parameters:\n");
  fprintf(stderr,"         -h                 : To get this information\n");
  fprintf(stderr,"         -dbg               : To get debug information\n");
  fprintf(stderr,"         -v                 : verbose\n");
}

/*
 * Modif 09/05/05 JM:
 *  Added backbone rebuilding support 
 *    - DcBBREBFWD, DcBBREBBCK
 *    - input ca trace file name (ICATraceFichName)
 *
 */

void parseArgstr(int argc,char *argv[])
{
  int i;

  if (argc == 1) {
    printUsage();
    exit(0);
  }

  for (i=0;i<argc;i++) {
    
    if (!strcmp(argv[i],"-h")) { /* print usage */
      printUsage();
      exit(0);
    }
    
    /* Le fichier profile seed (input) --------------------- */
    if (!strcmp(argv[i],"-iPrf")) {
      strcpy(IPrfFichName,argv[i+1]);
      i++;
    } 
    /* Le fichier liste de profils a scanner (input) --------------------- */
    if (!strcmp(argv[i],"-iIds")) {
      strcpy(IIdsFichName,argv[i+1]);
      i++;
    } 
    /* Le fichier results (output) --------------------- */
    if (!strcmp(argv[i],"-o")) {
      strcpy(OFichName,argv[i+1]);
      i++;
    } 
    /* Le fichier log (output) --------------------- */
    if (!strcmp(argv[i],"-olog")) {
      strcpy(OLstFichName,argv[i+1]);
      i++;
    } 
    /* DB location ---------------------------------------- */
    if (!strcmp(argv[i],"-dbPath")) {
      strcpy(gDBPATH,argv[i+1]);
      i++;
    } 

    /* build trj from ----------------------------------- */
    if (!strcmp(argv[i],"-from")) {
      gFrom = atoi(argv[i+1]);
      i++;
    } 
    /* build trj up to ---------------------------------- */
    if (!strcmp(argv[i],"-to")) {
      gTo = atoi(argv[i+1]);
      i++;
    } 

    /* maxHits up to ---------------------------------- */
    if (!strcmp(argv[i],"-maxHits")) {
      gMaxHits = atoi(argv[i+1]);
      i++;
    } 

    /* Dirichlet wieght (as effectives) ----------------- */
    if (!strcmp(argv[i],"-pcK")) {
      gKDirich = atof(argv[i+1]);
      i++;
    } 
    /* MAx score ---------------------------------------- */
    if (!strcmp(argv[i],"-maxScore")) {
      gMaxScore = atof(argv[i+1]);
      i++;
    } 
    /* nBest ---------------------------------------- */
    if (!strcmp(argv[i],"-nBest")) {
      gNBest = atoi(argv[i+1]);
      i++;
    } 

    /* mean instead of max --------------------------------------- */
    if (!strcmp(argv[i],"-mean")) {
      gUseMean = 1;
    } 
    /* output JS per position --------------------------------------- */
    if (!strcmp(argv[i],"-outJS")) {
      gOutJS = 1;
    } 
    /* mode DEBUG --------------------------------------- */
    if (!strcmp(argv[i],"-dbg")) {
      DEBUG = 1;
    } 
    /* mode DEBUG --------------------------------------- */
    if (!strcmp(argv[i],"-v")) {
      gVerbose = 1;
    } 

  }

}


/* =====================================================================
 * Global parameters information
 * =====================================================================
 */
void prmtrsSumary(FILE *f)
{
  delimiter(f);
  fprintf(f,"PrfScore (P. Tuffery, 2010)\n");
  delimiter(f);
  fprintf(f,"\nSearch parameters\n");
  /*  fprintf(stderr,"         -iSBBs <SBBFich>  : Fichier sequence SBB proteine\n"); */
  fprintf(stderr,"  seed profile file     : %s\n", IPrfFichName);
  fprintf(stderr,"  seed from             : %d\n", gFrom);
  fprintf(stderr,"  seed to               : %d\n", gTo);
  fprintf(stderr,"  List of files to scan : %s\n", IIdsFichName);
  fprintf(stderr,"  PATH to files         : %s\n", gDBPATH);
  fprintf(stderr,"  pseudo count value    : %lf\n", gKDirich);
  fprintf(stderr,"  use mean (not max)    : %d\n",  gUseMean);
  fprintf(stderr,"  max score value       : %lf\n", gMaxScore);
  fprintf(stderr,"  n best probas         : %d\n",  gNBest);
  fprintf(stderr,"  max hits returned     : %d\n",  gMaxHits);
  fprintf(stderr,"  result file           : %s\n",  OFichName);
  fprintf(stderr,"  out JS detail         : %d\n",  gOutJS);
  delimiter(f);

  delimiter(f);

  fflush(f);
}

char ** txtFileRead(char *fname, int *lSze)
{
  struct stat buf;
  char *tmp, *p;
  char **line;
  int fd;
  int fSze, done;
  int nLines;
  int i,j;

  if ((fd = open(fname,O_RDONLY)) < 0) {
    return NULL;
  }

  if (fstat(fd,&buf)) {
    close(fd);
    return NULL;
  }

  if (!S_ISREG (buf.st_mode)) {
    close(fd);
    return NULL;
  }

  if (!(fSze = buf.st_size)) {
    close(fd);
    return NULL;
  }

  if (!(tmp = malloc((fSze+1) * sizeof(char)))) {
    close(fd);
    return NULL;
  }

  done = 0;
  while (done < fSze) {
    int r = read(fd, &(tmp[done]), fSze - done);
    if (r < 0) {
      free(tmp);
      close(fd);
      return NULL;
    }
    done += r;
  }
  close(fd);

  nLines = 0;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') nLines++;
  }
  if (tmp[fSze-1] != '\n') nLines++;

  if (!(line = malloc((nLines+2) * sizeof(char *)))) {
      free(tmp);
      return NULL;
  }

  line[0] = tmp;
  j = 1;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') {
      line[j++] = &tmp[i+1];
      tmp[i] = '\000';
    }
  }
 
  line[j] = NULL;
  *lSze = nLines;

  return (line);
}


char ** txtFileFree(char **l, int lSze)
{
  int i;

  /* Attention: on ne libere que la premiere ligne,
     car c'est un seul malloc pour l'ensemble des lignes
  */
  free(l[0]);
  free(l);
  return (NULL);
}


