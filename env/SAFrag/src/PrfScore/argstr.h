#ifndef __ARGSTR_H__
#define __ARGSTR_H__

#include "Types.h"

extern DtStr IPrfFichName;
extern DtStr IIdsFichName;
extern DtStr OLstFichName;
extern DtStr OFichName;

extern DtStr gDBPATH;
extern int   gFrom;
extern int   gTo;
extern int   gNBest;
extern int   gUseMean;
extern int   gVerbose;

extern double gKDirich;
extern double gMaxScore;
extern int    gMaxHits;
extern int    gOutJS;
 
extern void parseArgstr(int argc,char *argv[]);
extern void printUsage();
extern void prmtrsSumary(FILE *f);
extern char ** txtFileRead(char *fname, int *lSze);
extern char ** txtFileFree(char **l, int lSze);


#endif
