#ifndef __TYPE_H__
#define __TYPE_H__


#define MAXFRGLEN 50

typedef char   DtStr[BUFSIZ];
typedef double DtVec27[27];
typedef double DtPoint2[2];

typedef struct sprob {
  int n;
  int sze;
  DtVec27 *p; 
} DtProb;

typedef struct sresults {
  int n;
  double *rs; 
} DtResults;

#endif
