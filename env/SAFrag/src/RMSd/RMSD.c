#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#define INIT_SIZE 64
#define INC_SIZE 64
#define MAX_SIZE 1024
#define MAXLINE 256
#define MAXROW 128
#define EPS 1.e-8
#define min(a,b) (a<b?a:b)
#define max(a,b) (a<b?b:a)

typedef  struct {
  double *values;
  int size;
} DoubleVector;

void skipLine (FILE *file) {
char c;
  c=fgetc(file);
  while (c!=EOF && c!='\n') {
     c=fgetc(file);
  }
}

int readLine(FILE *file, char *buffer) {
int n;
char c;
  n=0;
  c=fgetc(file);
  while (c!=EOF && c!='\n' && c!='>' && n<MAXLINE) {
    buffer[n++]=c;
    c=fgetc(file);
  }
  buffer[n]='\0';
  if (c==EOF || c=='\n') return n;
  if (n>=MAXLINE) return -MAXLINE;
  if (c=='>') { fseek(file, -1, SEEK_CUR);return n;}
}

int readCommentLine (FILE *file, char **id) {
char c;
int n=0;

  c=fgetc(file);
  if (c=='>') {
    while (c!=EOF && c!='\n') {
      c=fgetc(file);
      (*id)[n++]=c;
    }
    (*id)[n]='\0';
  }
  return n;
}
/*
 * dirty parsing of strange files
 */
int parsePDBLine (char *buffer, double *x) {
  char dest[9];
  dest[9]='\0';
  x[0]=atof(strncpy(dest, buffer, 8));
  x[1]=atof(strncpy(dest, buffer+8, 8));
  x[2]=atof(strncpy(dest, buffer+16, 8));
  return 3;
}

int parseDoubleVector(char *buffer, double *x) {
  char *pstr, *str;
  int n;

  pstr=buffer;
  n=0;
  while ((str=strsep(&pstr, " "))!=NULL && n<MAXROW){
    if (strlen(str)>0) x[n++]=atof(str);
  }
  return n;
}

DoubleVector *newDoubleVector(int n) {
  DoubleVector *X;
  double *x;
  
  X=(DoubleVector *)calloc(1, sizeof(DoubleVector));
  x=(double *)calloc(n, sizeof(double));
  X->values=x;
  X->size=n;
  return X;
}

void freeDoubleVector(DoubleVector *X) {
  free(X->values);
  free(X);
}

DoubleVector *reallocDoubleVector(DoubleVector *X, int n) {
  X->values = (double *) realloc(X->values, n*sizeof(double));
  X->size=n;
  return X;
}

/*
 * si X!= NULL : append to X new read values
 * si id!=NULL : read first line as a comment line
 */
DoubleVector *readDoubleVector(FILE *file, DoubleVector *X, int *status, char *id) {
  char buffer[MAXLINE];
  double x[MAXROW];
  int i, j, n,  size;
  int c;
  if (X==NULL) {
    X=newDoubleVector(INIT_SIZE);
  }
  if (id!=NULL) readCommentLine(file, &id);
  size=0;
  c=readLine(file, buffer);
  while (c>0) {
     //  n=parseDoubleVector(buffer, x);
     n=parsePDBLine(buffer, x);
    if (size+n>X->size) {
      X=reallocDoubleVector(X, X->size+INC_SIZE);
    }
    for (i=0; i<n; i++) { 
      X->values[size+i]=x[i];
    }
    size+=n;
    c=readLine(file, buffer);
  }
  X->size=size;
  *status=X->size;
  return X;
}
  
void printDoubleVector(DoubleVector *X, int ncol) {
  int i;

  for (i=0; i<X->size; i++) {
    if (i%ncol==0) printf("\n");
    printf("%d %lf ", i/ncol+1, X->values[i]);
  }
  printf("\n");
}
/*
 *
 * RMSD STUFF
 *
 */

double *random_vect(int n) {
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++) 
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

double inner(double *x, double *y, int n) {
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;
    
}

void product(double *result, double *A, double *x, int n) {
  int i, j;
  double sum;

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i+n*j]*x[j];
    result[i]=sum;
  }
}

double l2(double *x, int n) {
  int i, sum=0;
  for (i=0; i<n; i++) sum+=x[i]*x[i];
  return sqrt(sum);
}

/*
 * det(K): determinant de matrice 3x3
 */
double det3x3(double K[3][3]) {
  return K[0][0]*(K[1][1]*K[2][2]-K[2][1]*K[1][2])+K[1][0]*(K[2][1]*K[0][2]-K[0][1]*K[2][2])+K[2][0]*(K[0][1]*K[1][2]-K[1][1]*K[0][2]);
}


void rotation(double *e, double R[3][3]) {
  R[0][0]=e[0]*e[0]+e[1]*e[1]-e[2]*e[2]-e[3]*e[3];
  R[1][1]=e[0]*e[0]-e[1]*e[1]+e[2]*e[2]-e[3]*e[3];
  R[2][2]=e[0]*e[0]-e[1]*e[1]-e[2]*e[2]+e[3]*e[3];
  R[0][1]=2*(e[1]*e[2]+e[0]*e[3]);
  R[0][2]=2*(e[1]*e[3]-e[0]*e[2]);
  R[1][0]=2*(e[1]*e[2]-e[0]*e[3]);
  R[1][2]=2*(e[2]*e[3]+e[0]*e[1]);
  R[2][0]=2*(e[1]*e[3]+e[0]*e[2]);
  R[2][1]=2*(e[2]*e[3]-e[0]*e[1]);
}


int power(double *a, int n, int maxiter, double eps, double *v, double *w0, int new) {
  int niter,i;
  double *y;
  double sum, l, normy, d, w[4];

  y=w0;
  if (new) y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    product(y, a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } //while (d>eps*fabs(l) && niter<maxiter); 
  while (d>eps*fabs(l) && niter<10000);
  if (new) free(y);
  *v=l;
  for (i=0; i<4; i++) w0[i]=w[i];
 return niter;
}

double best_shift(double *a, int n) {
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0];
  for (i=1; i<n; i++) t=max(t, a[i+n*i]);
  M=t;
  t=a[0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i+n*j]);
    t=min(t, a[i+n*i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++) 
    a[i+n*i]=a[i+n*i]+s;
  return s;
}


int shift_power(double *a, int n, int maxiter, double eps, double *v, double *w, int new) {  
  double sh;
  int niter;
  sh=best_shift(a, n);   

  niter=power(a, n, maxiter, eps, v, w, new);
  *v=*v-sh;
  return niter;
}

/*
 * calculs des coefficients d'Euler
 *
 */
void euler(double *X,  double *Y, int n, double *v, double *w, int new) {
  double Xm[3], Ym[3], K[3][3], A[16];
  double x, y, sum;
  int i,j,k;



  for (i=0; i<3;i++)
    for (j=0; j<3; j++) {
      sum=0;
      for (k=0; k<n;k++)
	sum+=Y[i+3*k]*X[j+3*k];
      K[i][j]=sum;
    }

  A[0+4*0]=K[0][0]+K[1][1]+K[2][2];
  A[1+4*1]=K[0][0]-K[1][1]-K[2][2];
  A[2+4*2]=-K[0][0]+K[1][1]-K[2][2];
  A[3+4*3]=-K[0][0]-K[1][1]+K[2][2];
  A[0+4*1]=A[1+4*0]=K[1][2]-K[2][1];
  A[0+4*2]=A[2+4*0]=K[2][0]-K[0][2];
  A[0+4*3]=A[3+4*0]=K[0][1]-K[1][0];
  A[1+4*2]=A[2+4*1]=K[0][1]+K[1][0];
  A[1+4*3]=A[3+4*1]=K[0][2]+K[2][0];
  A[2+4*3]=A[3+4*2]=K[1][2]+K[2][1];
  shift_power(A, 4, 10000, EPS, v, w, new);
}

/*
 * rmsd  entre X:3xN et Y:3xN stockés dans des vecteurs
 * par colonnes mises bout à bout.
 * effet de bord: centrage de X et Y
 */

void rmsd(double *X,  double *Y, int n, double *r, double *w, int new, int sym) {
  double Xm[3], Ym[3],XX[3*n],YY[3*n];
  double x, y, sum, v;
  int i,j,k;

  for (i=0; i<3;i++) {
    sum=0;
    for (j=0; j<n;j++)
      sum+=X[i+j*3]; 
    Xm[i]=sum/n;
  }
  for (i=0; i<3;i++) {
    sum=0;
    for (j=0; j<n;j++)
      sum+=Y[i+j*3]; 
    Ym[i]=sum/n;
  }

  sum=0;
  for (i=0; i<3; i++) 
    for (j=0; j<n; j++) {
      x=X[i+j*3]-Xm[i];
      y=Y[i+3*j]-Ym[i];
      sum+=x*x+y*y;
      XX[i+j*3]=x;
      YY[i+3*j]=y;
      if (sym<0) YY[i+3*j]=-y;
      
    }
  euler(XX, YY, n, &v, w, new);
  //printf("v=%lf\n", v); 
  *r=sqrt(fabs(sum-2*v)/n);
}

/*
 * MAIN
 */
int main(int argc, char *argv[]) {
  DoubleVector *X1=NULL, *X2=NULL;
  int len1, len2;
  FILE *file1, *file2;
  int status;
  char id1[MAXLINE], id2[MAXLINE];
  double  r; 
  double *results1, *results2; 
  int n, i, j, k, from1, to1, from2, to2;
  double *X, *Y;
  double *w;
  clock_t start, end;
  double cpu_time_used;
     
  start = clock();

  if (argc<6) {
    fprintf(stderr, "usage: %s file1.xyz  from1 to1 file2.xyz from2 to2\n", argv[0]);
    return -1;
  }
  n=1;
  if ((file1=fopen(argv[n], "r"))==NULL) {
    fprintf (stderr, "cannot open %s\n", argv[n]);
    return -1;
  }
  n++;
  from1 = atoi(argv[n]);
  n++;
  to1  = atoi(argv[n]);
  n++;
  if ((file2=fopen(argv[n], "r"))==NULL) {
    fprintf (stderr, "cannot open %s\n", argv[n]);
    return -1;
  }
  n++;
  from2 = atoi(argv[n]);
  n++;
  to2  = atoi(argv[n]);
  n++;
  X1=readDoubleVector (file1, X1, &status, id1);
  fclose(file1);
  //printDoubleVector(X1, 3);
  len1=X1->size/3;
  if (from1 <1 || to1 > len1) {
    fprintf (stderr, "change something : from1=%d, to1=%d\n", from1, to1);
    return -1;
  }
  X=&((X1->values)[3*(from1-1)]);

  X2=readDoubleVector (file2, X2, &status, id2);
  fclose(file2);
  //printDoubleVector(X2, 3);
  len2=X2->size/3;
  if (from2 <1 || to2 > len2) {
    fprintf (stderr, "change something : from2=%d, to2=%d\n", from2, to2);
    return -1;
  }
  Y=&((X2->values)[3*(from2-1)]);

  printf("> file1 file2 from1 to1 from2 to2 rmsd\n");
  w=random_vect(4);
  rmsd(X, Y, to1-from1+1, &r, w, 0, 1);
  printf("%s %s %d  %d  %d  %d %lf\n", argv[1], argv[4], from1, to1, from2, to2, r);
  free(w);
  freeDoubleVector(X1);
  freeDoubleVector(X2);
}

