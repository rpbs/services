#!/usr/bin/env python

SAPROF_ROOT="/nfs/freetown/user/tuffery/prgs/SAProf"
# SAPROF_ROOT="/usr/local/SAProf/"
# SAPROF_ROOT="/nfs/freetown/user/shen/PP-Search/scripts/ProfAnalyseV4"

SAPROF_BIN_PATH="%s/bin/" % SAPROF_ROOT
# Chemin vers les scripts python
#PATH_SCRIPTS="/nfs/freetown/user/shen/PP-Search/scripts/ProfAnalyseV2/"
PATH_SCRIPTS="%s/SAProf3" % SAPROF_ROOT
PATH_SCRIPTS="%s/scripts" % SAPROF_ROOT
SAPROF_SCRIPT_PATH=PATH_SCRIPTS
SAPROF_LIB="%s/data" % SAPROF_ROOT

LISTS = {
     "25" : { "path": "%s/PDB25ok/" % SAPROF_LIB,
              "list" : 
              "PDB25_HHfragBD_4649ids_5972Frag_newSVMBlast.prob.ids2",
              "xyzpath": "%s/PDB25ok/" % SAPROF_LIB,
              "xyzlist": "PDB25_HHfragBD_4649ids_5972Frag.ids2"
              },
 
     "50" : { "path": "%s/PDB50/" % SAPROF_LIB,
              "list" : 
"cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13580.novSVMBlast.prob.ids",
              "xyzpath": "%s/PDB50/" % SAPROF_LIB,
              "xyzlist":  "cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13580.ids"
              },

#      "25" : { "path": "%s/PDB25ok/" % SAPROF_LIB,
#               "list" : 
#               "PDB25_HHfragBD_4649ids_5972Frag_newSVMBlast.prob.ids2",
#               "xyzpath": "%s/PDB25ok/" % SAPROF_LIB,
#               "xyzlist": "PDB25_HHfragBD_4649ids_5972Frag.ids2"
#               },
 
#      "50" : { "path": "%s/PDB50/" % SAPROF_LIB,
#               "list" : 
# "cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13580.novSVMBlast.prob.ids",
#               "xyzpath": "%s/PDB50/" % SAPROF_LIB,
#               "xyzlist":  "cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13580.ids"
#               },
}
 
DFLT_PROB_FILE_LIST = "PDB25_HHfragBD_4649ids_5972Frag_newSVMBlast.prob.ids2" 
DFLT_PROB_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB
DFLT_XYZ_FILE_LIST = "PDB25_HHfragBD_4649ids_5972Frag.ids2"
DFLT_XYZ_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB

DFLT_SAPROF_FROM   = 1
DFLT_SAPROF_TO     = -1

IDS_PROB  = "%s/PDB25_HHfragBD_4649ids_5972Frag_newSVMBlast.prob.ids2" % SAPROF_LIB
IDS_CRMSD = "%s/PDB25_HHfragBD_4649ids_5972Frag.ids2" % SAPROF_LIB
PATH_CRMSD= "%s/PDB25ok/" % SAPROF_LIB

DFLT_WPATH         = "."
DFLT_TMPPATH       = "/scratch"   #!!!
DFLT_PROGRESS_FILE = ".progress.txt"
DFLT_SAPROF_LABEL  = "SA-PROF"
DFLT_SAPROF_NP     = 50
DFLT_SAPROF_OUTPUT = ".pir"

# mode       : AllFast/AllComplet/FiltComplet/FiltFast/Dflt
#       AllFast : sort toutes les valeurs de tous les calculs (NE PAS UTILISER: pour l'evaluation de la methode)
#              Query Match qfrom qto mfrom mto JS (RMSd) LFrag ClusterNumber ClusterEffective 
#              LFrag: (to-from + 1 + 3)  
#    AllComplet : Idem AllFst, apres RMSd: ajoute JS par position (1 chaine separee par des tirets)
#    FiltComplet: Idem AllComplet, mais le resultat apres le filtre
#      FiltFast : Comme AllFast, mais filtree
#          Dflt : Pour definir le mode par defaut (FiltFast ou FiltComplet)

SAPROF_MODES       = ["AllFast","AllComplet","FiltComplet","FiltFast"]
DFLT_SAPROF_MODE   = "FiltComplet"

# si utilise fst comme entree.(not prob file), choisir le version de blast et svmModel
SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #Dflt novSVMmodel
#SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl0Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #novNovNovSVMmodel
BLAST="/nfs/freetown/banks/biomaj/blastdb"  #novBlast
#BLAST="" #pour blast2006 ??? c'est quoi?

# Do not modify 
PPP_SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" # ??? n'est pas utilise?
TAB_LENFRAG=[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
MODE="FiltFast"
MAXRUN=500

# C'est quoi? 
REMOTE_SERVER=False
