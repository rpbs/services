#!/bin/bash

#file input:
#T05213 16pkA 2 4 1 3 0.268273
#T05213 16pkA 2 4 2 4 0.806592
#....

#../../scripts/ProfAnalyseV4/scripts/ajouterCRMSD.sh aa ./ ../../scripts/ProfAnalyseV4/data/PDB50/ ../../scripts/ProfAnalyseV4/bin/RMSD

#file output:                 CRMSD(ajouter)   
#T05213 16pkA 2 4 1 3 0.268273 0.309598
#T05213 16pkA 2 4 2 4 0.806592 0.366754
#....


if [ $# == 5 ] ; then
	fileName=$1
	pathOrFileRef=$2
	pathXYZ=$3
	scriptCRMSD=$4
	outFile=$5
else
	echo "Usage:  $0 <results inputfile> <file or path RefXYZ> <pathBDxyz> <scriptCRMSD_used> <outFile>"
	echo "InputFile format [the line]:  T05213.svmi8.27.prob 16pkA.novSVMBlast.svmi8.27.prob 2 4 1 3 0.268273"
	echo "Attention: this scripts clean extention \".svmi8.27.prob ...\""
	exit 1
fi

if [ -f $outFile ]; then \rm $outFile;fi  #del outFile, if exist

IDREF="" #pour que message err affichier seulement une fois
compt=0
for i in `cat $fileName|sed s/'\.svmi8\.27\.prob'//g | sed s/'.newModelSVM'//g | sed s/'\.prob'//g | sed s/'\.new'//g| sed s/'\.novNovNovSVMBlast'//g| sed s/'\.novNovSVMBlast'//g| sed s/'\.novSVMBlast'//g|sed s/'\.novBlast'//g| sed s/'\.novNovSVM'//g| sed s/'\.novSVM'//g | sed s/'\t'/' '/g | tr -s ' '| sed s/" "/"#"/g`;do
#echo $i
	((compt++))
	id1=`echo $i| cut -d"#" -f1`
	id2=`echo $i| cut -d"#" -f2`
	posi1a=`echo $i| cut -d"#" -f3`
	posi1b=`echo $i| cut -d"#" -f4`
	((posi1b+=3))
	posi2a=`echo $i| cut -d"#" -f5`
	posi2b=`echo $i| cut -d"#" -f6`
	((posi2b+=3))
	if [ -f $pathOrFileRef ]; then # If "pathOrFileRef" is a file, use this file.
		idRef=`basename $pathOrFileRef| sed s/'\.xyz$'//`
		if [ ! "$id1" = "$idRef" -a ! "$IDREF" = "$id1" ];then 
			echo "Warning: Lines $compt ResultsInput_ID[$id1] diff XYZinput_ID[$idRef]"
			IDREF=$id1
		fi
	#	echo "$scriptCRMSD $pathOrFileRef $posi1a $posi1b $pathXYZ/$id2.xyz $posi2a $posi2b | grep -v \>| awk '{print $7}'"
		crmsd=`$scriptCRMSD $pathOrFileRef $posi1a $posi1b $pathXYZ/$id2.xyz $posi2a $posi2b | grep -v \>| awk '{print $7}'`
	elif [ -d $pathOrFileRef ]; then # If "pathOrFileRef" is a path, will use ID of InputResultsFile 
	#	echo "$scriptCRMSD $pathOrFileRef/$id1.xyz $posi1a $posi1b $pathXYZ/$id2.xyz $posi2a $posi2b | grep -v \>| awk '{print $7}'"
		crmsd=`$scriptCRMSD $pathOrFileRef/$id1.xyz $posi1a $posi1b $pathXYZ/$id2.xyz $posi2a $posi2b | grep -v \>| awk '{print $7}'`
	else
		echo "Not exist this path or file [$pathOrFileRef]"
		exit 1
	fi
	if [ "$crmsd" = "" ]; then crmsd=-1; echo 'Err: '$i'#'$crmsd| sed s/'#'/' '/g; fi
	echo $i'#'$crmsd| sed s/'#'/' '/g >>$outFile
done
if [ $compt -eq 0 ]; then echo "Input file ["$fileName"] is empty.";fi
