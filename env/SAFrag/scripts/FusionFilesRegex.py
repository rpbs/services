#!/usr/bin/python
import sys
import os
import re

class FusionFilesRegex:
    
    def __init__(self,pathFiles,regexFile,Aff=False):
        strOut=""
        comptLines=0
        listFiles=self.findFiles(regexFile,pathFiles)
        
        for file in listFiles:
            f = open(file)
            lines = f.readlines()
            f.close()
            
            for i in range(len(lines)):
                l=lines[i].replace("\n","")
                if len(l)>0:
#                    print l
                    strOut+=l+"\n"
                    comptLines+=1
                else : sys.stderr.write( "Attention: [file:%s] delete empty line[ %d ]  =>%s<=\n"%(file,i,l))
        if strOut!="":
            outPut(strOut,outFile,comptLines)
        else : 
            print "Input files[%s] is empty! Can't creat %s file."%(regex,outFile)
            sys.exit("Input files[%s] is empty! Can't creat %s file."%(regex,outFile))

    def findFiles(self,regex, base=".", circle=False):
        """
        Find all the files with character "regex"
        Circle: chercher aussi les sous repertoires.
        """
        if base == ".":
            base = os.getcwd()
        final_file_list = []
        print "[ Research :",base,"]"
        for item in os.listdir(base):
            full_path = os.path.join(base, item)
        #            print full_path," -isFile->  ",os.path.isfile(full_path)
            if os.path.isfile(full_path):
                if re.search(regex,full_path):# and full_path.endswith(".py"):
                    print full_path,"  ",regex
                    final_file_list.append(full_path)
            else:
                if circle: final_file_list += self.findFiles(regex, full_path)
        return final_file_list



def outPut(text, where = None,i=0,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file :"+where+"] ["+str(i)+" lines]"
        
    if where:
        f.close()

  

if __name__ == "__main__":
    
    if len(sys.argv) != 4:
        sys.exit("usage:FusionFiles.py [PATH] [Regex] [OutputFile]\n***Attention: lines empty will be deleted!***")
     
    pathFiles=sys.argv[1]
    regexFiles=sys.argv[2] #"-DisJSNA-rmsdNA\.data\.fin\Z"
    outFile=sys.argv[3]
    
    
    FusionFilesRegex(pathFiles,regexFiles,outFile)
    
