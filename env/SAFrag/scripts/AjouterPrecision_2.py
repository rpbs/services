#!/usr/bin/python
'''
Created on June, 2011

@author: shen
'''
import sys
import copy
import os
import re


class Run:
    
    def __init__(self,FileName, precisionFile=None):
        """
        read file *.results.final
        calculer pourcentage couvert
        """

        tabInfo=self.inputFile(FileName)  #input Score et CRMSD infos
        (dictLengthFrg,tabLenFrag)=self.inputPreciFile(precisionFile)#input Classment infos class2 et Class3 contient les names incomplete. Ex: dictClass "d1dlwa_",dictClass2"d1dlwa",dictClass3 "d1dlw"
        
        print "-----[Sort tab DisJS]-------"
        col_poidCl_tabPreci=1
        col_MJS_tabPreci=5
        col_preci_tabPreci=8 # precision_lissage=8(dflt); precision=7
        dictMinMaxMJSPoid={}
        tabLenFrag.sort()
        for i in tabLenFrag:
            dictLengthFrg[i].sort(key=lambda k:float(k[col_MJS_tabPreci-1]))
            minMJS=float(dictLengthFrg[i][0][col_MJS_tabPreci-1])
            maxMJS=float(dictLengthFrg[i][len(dictLengthFrg[i])-1][col_MJS_tabPreci-1])
            
            dictLengthFrg[i].sort(key=lambda k:int(k[col_poidCl_tabPreci-1]))
            minPoid=int(dictLengthFrg[i][0][col_poidCl_tabPreci-1])
            maxPoid=int(dictLengthFrg[i][len(dictLengthFrg[i])-1][col_poidCl_tabPreci-1])
           
            dictLengthFrg[i].sort(key=lambda k:float(k[col_preci_tabPreci-1]))
            minPrec=float(dictLengthFrg[i][0][col_preci_tabPreci-1])
            maxPrec=float(dictLengthFrg[i][len(dictLengthFrg[i])-1][col_preci_tabPreci-1])

            print "Len%d\tMinMJS:%5.3f\tMaxMJS:%5.3f\tMinPoids:%d\tMaxPoids:%d\tMinPrec:%5.3f\tMaxPrec:%5.3f"%(i,minMJS,maxMJS,minPoid,maxPoid,minPrec,maxPrec)
            dictMinMaxMJSPoid[i]=[minMJS,maxMJS,minPoid,maxPoid,minPrec,maxPrec]
#        print dictMinMaxMJSPoid
#        sys.exit()
#        print tabInfo[0]
#        print dictLengthFrg
        (self.tabInfo,self.tabInfo_str)=self.ajouterPrecision(tabInfo,dictLengthFrg,dictMinMaxMJSPoid,-2,0,7,col_preci_tabPreci,col_poidCl_tabPreci,col_MJS_tabPreci)
#        print self.tabInfo_str
      
    def inputFile(self,fname):
        """
        read un file plusieurs colonne sep=" "
        
        T05151    2yxxA     11    17     19    25    0.014191    0.517723    10    T05151-11-17-Cl1      495
        T05151    3bjdC1   101    107    35    41    0.046399    1.191462    10    T05151-101-107-Cl1    485
        T05151    3ck2A    100    106    91    97    0.049969    1.506331    10    T05151-100-106-Cl1    479

        """
        f = open(fname)
        print "[Load ScoreFile:%s]"%fname
        l = f.readlines()
        f.close()
        tabInfo = []
        for i in range(0,len(l)):
            tempTab=[]
            if len(l)!=0:   
                if l[i]!="\n" and l[i][0]!="#" and l[i].split()[0]!="file1":
                    tempTab=l[i].replace("\n","").split()
                    tabInfo.append(tempTab)
                    file1name=tempTab[0].split('.')[0] # Create dictionary {name_file1, length_file1}
            else :
                print "Errors empty line No:",i
                sys.exit();
        return tabInfo
    
    def inputPreciFile(self,fname,col_len=12):
        """
            ex : /home/freetown/shen/Data/astral-scopdom-seqres-gd-all-1.75.fa.scopIDs
            
            #poid_from poid_to poid_moy precision crmsd    eff     len 
            389    498    443.50    0.960839    0.868320    715    17
            390    498    444.00    0.960784    0.868706    714    17
            391    498    444.50    0.960784    0.868706    714    17

            #Poid_fm        Poid_to Poids   MJS_fm  MJS_to  MJS     Precision       Preci_liss      CRMSD   NbFrag  OkFrag  LenFrag
            1       500     250.5   0.003   0.185   0.0938  0.8187  0.8187  1.9647  5180    4241    21
            1       500     250.5   0.003   0.184   0.0933  0.8191  0.8191  1.9624  5164    4230    21
            1       500     250.5   0.003   0.183   0.0928  0.8225  0.8225  1.9454  5110    4203    21
            ...       ...
        """
        f = open(fname)
        dictLengthFrg={}
        tabLenFrag=[]
        print "[Load PreciFile:%s]"%fname
        l = f.readlines()
        f.close()
        for i in range(0,len(l)):
            tempTab=[]
            if len(l)!=0:   
                if l[i]!="\n" and l[i][0]!="#":
                    tempTab=l[i].replace("\n","").split()
                    lenFrag=int(tempTab[col_len-1])                
                    if lenFrag<6 or lenFrag>27: print "Err: LenFragAA[6-27] is",lenFrag;sys.exit("Err: LenFragAA[6-27] is %d."%lenFrag) 
                    if dictLengthFrg.has_key(lenFrag):
#                        print dictLengthFrg[lenFrag]
                        dictLengthFrg[lenFrag].append(tempTab)
                    else :  
                        dictLengthFrg[lenFrag]=[tempTab]
                        tabLenFrag.append(lenFrag)
            else :
                print "Errors empty line No:",i
                sys.exit();
        return dictLengthFrg,tabLenFrag

    def ajouterPrecision(self,tabInfo,dictLengthFrg,dictMinMaxMJSPoid,col_len=-2,col_poidCl=0,col_MJS=7,col_preci_tabPreci=8,col_poidCl_tabPreci=1,col_MJS_tabPreci=4):
        """
        id1       id2    from1    to1    from2  to2   disJS        crmsd      len      id_Cluster     poids_Cl   preci
        T05151    3bjdC1    01    107    35    41    0.046399    1.191462    10    T05151-101-107-Cl1    485     xx
        T05151    3ck2A    100    106    91    97    0.049969    1.506331    10    T05151-100-106-Cl1    479     xx
        """
        tabInfo_str=""
        message_str=""
        for i in range(len(tabInfo)):
                
            if i==0: sys.stdout.write("Running: |0% ")
            if i==int(len(tabInfo)/5):   sys.stdout.write("==>20% ")
            if i==int(len(tabInfo)*2/5): sys.stdout.write("==>40% ")
            if i==int(len(tabInfo)*3/5): sys.stdout.write("==>60% ")
            if i==int(len(tabInfo)*4/5): sys.stdout.write("==>80% ")
            if i==len(tabInfo)-1:sys.stdout.write("==>100%|")
            
            lenFrag=int(tabInfo[i][col_len-1])

            trouverPreci=False
            if lenFrag<6 or lenFrag>27: print "Err: LenFragAA[6-27] is",lenFrag;sys.exit("Err: LenFragAA[6-27] is %d."%lenFrag) 
            poidCl=int(tabInfo[i][col_poidCl-1])
            MJS="%5.3f"%(float(tabInfo[i][col_MJS-1])+0.0005)

            if float(MJS)>dictMinMaxMJSPoid[lenFrag][1]:
                message_str+="Attention[line:%d]: MJS[%s] > maxMJS[%5.3f], used minPreci[%5.3f]\n"%(i,MJS,dictMinMaxMJSPoid[lenFrag][1],dictMinMaxMJSPoid[lenFrag][4])
                minORmaxPreci=dictMinMaxMJSPoid[lenFrag][4]
                trouverPreci=True
            if poidCl<dictMinMaxMJSPoid[lenFrag][2]:
                message_str+="Attention[line:%d]: Poid[%d] < minPoid[%d], used minPreci[%5.3f]\n"%(i,poidCl,dictMinMaxMJSPoid[lenFrag][2],dictMinMaxMJSPoid[lenFrag][4])
                minORmaxPreci=dictMinMaxMJSPoid[lenFrag][4]
                trouverPreci=True

            if float(MJS)< dictMinMaxMJSPoid[lenFrag][0]: 
                message_str+="Attention[line:%d]: MJS[%s] < minMJS[%5.3f], used maxPreci[%5.3f]\n"%(i,MJS,dictMinMaxMJSPoid[lenFrag][0],dictMinMaxMJSPoid[lenFrag][5])
                minORmaxPreci=dictMinMaxMJSPoid[lenFrag][5]
                trouverPreci=True
            if poidCl>dictMinMaxMJSPoid[lenFrag][3]:
                message_str+="Attention[line:%d]: Poid[%d] > maxPoid[%d], used maxPreci[%5.3f]\n"%(i,poidCl,dictMinMaxMJSPoid[lenFrag][3],dictMinMaxMJSPoid[lenFrag][5])
                minORmaxPreci=dictMinMaxMJSPoid[lenFrag][5]
                trouverPreci=True


            if trouverPreci:
                tabInfo[i]=tabInfo[i]+["%5.3f"%minORmaxPreci]
                un="\t".join(tabInfo[i])+"\n"
                tabInfo_str+=un
            else:
                BestPreci=-1
                for j in range(len(dictLengthFrg[lenFrag])):
#                    print j
#                    print dictLengthFrg[lenFrag][j]
                    poidCl_tabPreci=int(dictLengthFrg[lenFrag][j][col_poidCl_tabPreci-1])
                    MJS_tabPreci=dictLengthFrg[lenFrag][j][col_MJS_tabPreci-1]
                    preci_tabPreci=float(dictLengthFrg[lenFrag][j][col_preci_tabPreci-1])
#                    print poidCl_tabPreci,poidCl, MJS_tabPreci,MJS
                    if poidCl_tabPreci==poidCl and MJS_tabPreci==MJS:
                        tabInfo[i]=tabInfo[i]+["%5.3f"%preci_tabPreci]
                        un="\t".join(tabInfo[i])+"\n"
                        tabInfo_str+=un
                        trouverPreci=True
                        break
                    if poidCl_tabPreci==poidCl or MJS_tabPreci==MJS:
                        if BestPreci<poidCl_tabPreci:BestPreci=preci_tabPreci

            if not trouverPreci and BestPreci!=-1:
                tabInfo[i]=tabInfo[i]+["%5.3f"%BestPreci]
                un="\t".join(tabInfo[i])+"\n"
                tabInfo_str+=un
                print "\nUsed Preci. similaire (A case d'absent ref Preci.): %5.3f"%BestPreci
                print "line[%d] len[%d] clusterWeight=[%d] MJS=[%s] PreciSimil=[%5.3f]"%(i,lenFrag,poidCl,MJS,BestPreci)
                trouverPreci=True

            if not trouverPreci:
                if BestPreci!=-1:
                    tabInfo[i]=tabInfo[i]+["%5.3f"%minORmaxPreci]
                    un="\t".join(tabInfo[i])+"\n"
                    tabInfo_str+=un
                    print "\nUsed Preci. similaire (A case d'absent ref Preci.)"
                    print "line[%d] len[%d] clusterWeight=[%d] MJS=[%s]"%(i,lenFrag,poidCl,MJS)
                    trouverPreci=True
                print "\nErr: Can't find Precision, line[%d] len[%d] clusterWeight=[%d] MJS=[%s]"%(i,lenFrag,poidCl,MJS)
                print tabInfo[i]
                sys.exit("Err: Can't find Precision! line[%d] len[%d] clusterWeight=[%d] MJS=[%s]"%(i,lenFrag,poidCl,MJS))
        print "\n%s"%message_str
        return tabInfo,tabInfo_str
            
                   
       
def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file : "+where+"]"
    if where:
        f.close()

if __name__ == "__main__":
    
    if len(sys.argv) == 4:
        FileName=sys.argv[1]
        PrecisionFile=sys.argv[2]
        outFile=sys.argv[3]
    else:  sys.exit("Usage: "+sys.argv[0]+" [FILE] [PRECI_REF_FILE] [OUTFILE]")


#    FileName="/home/freetown/shen/PP-Search/Analyse7/SAfrag-HHfrag/allData.filt.bestCl"
#    FileName="/home/freetown/shen/PP-Search/Analyse7/SAfrag-HHfrag/aa"
#    PrecisionFile="/home/freetown/shen/PP-Search/Analyse7/SAfrag-HHfrag/allData.filt.bestCl.PreciComplet"
#    outFile="%s.Preci"%(FileName)
    
    x=Run(FileName,PrecisionFile)
    print "------FIN------"
    outPut(x.tabInfo_str,outFile)
        


