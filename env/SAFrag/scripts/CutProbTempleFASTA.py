#!/usr/bin/python
import sys
import os
import re
import MethodeBasic


class Run:
    
    def __init__(self,FstCompletFile,FstFragFile,ProbCompletFile,OutFile):
        """
        Aligne 2 fasta seq, couper correctement Prob file.
        """
        fstComplet=MethodeBasic.inputFst(FstCompletFile)
        fstFrag=MethodeBasic.inputFst(FstFragFile)
        
#        print fstComplet
#        print fstFrag
        nbMatch=fstComplet.count(fstFrag)
        if len(fstComplet)<len(fstFrag): 
            print "Err[%s]: length FragSeq[%d] > CompletSeq[%d]"%(FstFragFile,len(fstFrag),len(fstComplet))
            sys.exit("Err[%s]: length FragSeq[%d] > CompletSeq[%d]"%(FstFragFile,len(fstFrag),len(fstComplet)))
        if nbMatch!=0: 
            deb=fstComplet.index(fstFrag)
            if nbMatch >1: 
                print "Attention[%s]: Find %d fragment sequence [L=%d] in %s"%(FstFragFile,nbMatch,len(fstFrag),FstCompletFile)
#                sys.exit("Attention[%s]: Find %d fragment sequence [L=%d] in %s"%(FstFragFile,nbMatch,len(fstFrag),FstCompletFile))
        else : 
            print "Err[%s]: Can't find the fragment sequence in sequence complete."%(FstFragFile)
            sys.exit("Err[%s]: Can't find the fragment sequence in sequence complete."%(FstFragFile))
        
        
        ProbComplet=MethodeBasic.inputProb(ProbCompletFile)
        if len(ProbComplet)!=len(fstComplet)-3:
            print "Err[%s]: length ProbComplet[%d] diff CompletSeq[%d-3]"%(FstFragFile,len(ProbComplet),len(fstComplet))
            sys.exit("Err[%s]: length ProbComplet[%d] diff CompletSeq[%d-3]"%(FstFragFile,len(ProbComplet),len(fstComplet)))
            
        
        ProbOut=ProbComplet[deb:deb+len(fstFrag)-3]
        strOut=""
        for i in ProbOut:
            strOut+="%s\n"%(" ".join(i))
            
        outPut(strOut,len(ProbOut),OutFile)
        

def outPut(text, i, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print "[Create file: "+where+"][L:"+str(i)+"]"
    if where:
        f.close()

if __name__ == "__main__":
    
    if len(sys.argv) != 5:
        print "Aligne 2 fasta seq, couper correctement Prob file."
        sys.exit("usage: "+sys.argv[0]+" [FstCompletFile] [FstFragFile] [ProbCompletFile] [OutPut]")
    FstCompletFile=sys.argv[1]
    FstFragFile=sys.argv[2]
    ProbCompletFile=sys.argv[3]
    OutFile=sys.argv[4]
    
#    FstCompletFile="./16pkA.fasta"
#    FstFragFile="./16pkA.fas"
#    ProbCompletFile="./16pkA.novSVMBlast.svmi8.27.prob"
#    OutFile="./aa"
    
    x=Run(FstCompletFile,FstFragFile,ProbCompletFile,OutFile)
    

        