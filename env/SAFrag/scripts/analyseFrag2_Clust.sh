#!/bin/bash
# ce programe calculer RMSD et DisJS, pour un frag donnee.
# il faut lancer dans le cluster master2.

#Mode
#(manque xyz/PDB file, ne calcule pas CRMSD)
# FiltFast    : Filtre DisJS par "PrfScore",                 manquer DisJS par position.
# AllFast     : Ne filtre pas DisJS, All the Data,           manquer DisJS par position, ne fait pas 10000bestDisJS and cluster
# FiltComplet : Filtre DisJS par "fusionSelection2Files.py", ajouter DisJS par position.
# AllComplet  : Ne filtre pas DisJS, All the Data,           ajouter DisJS par position, ne fait pas 10000bestDisJS and cluster

# $1 : probFile
# $2 : xyzFile
# $3 : ffrom
# $4 : fLen
# $5 : List of Ids to mine (.prob)
# $6 : Path to prob files
# $7 : List of Ids for RMSd (.xyz)
# $8 : Path to xyz files
# $9 : Mode
# $10: Label (for outFile)
# $11: Threshold DisJS (3PC,4PC,5PC,8PC,10PC for 3%,4%,... Err)


binPATH="$SAPROF_BIN_PATH"
scriptPATH="$SAPROF_SCRIPT_PATH"
echo $binPATH $scriptPATH


if [ $# == 11 ] ; then

	probFile=$1
	xyzFile=$2
	deb=$3
	len=$4
	idJS=$5
	pathJS=$6
	idCRMSD=$7
	pathCRMSD=$8
	mode=$9 #;mode="AllComplet"
	outFileID=${10} 
	seuilDisJS=${11}
	idR=`echo $1 | sed s/'\.svmi8\.27\.prob$'//g | sed s/'\.svmi8\.27\.prob\.new$'//g| sed s/'\.svmi8\.27\.prob\.newModelSVM$'//g| sed s/'\.prob$'//g| sed s/'\.novNovNovSVMBlast'//g|sed s/'\.novNovNovSVM'//g|sed s/'\.novNovSVMBlast'//g| sed s/'\.novSVMBlast'//g|sed s/'\.novBlast'//g| sed s/'\.novNovSVM'//g| sed s/'\.novSVM'//g`
        echo "RefID: $idR  OutFileID: $outFileID"
	if [ $xyzFile = "NA" -o $idCRMSD = "NA" -o $pathCRMSD = "NA" ];then 
		echo "Will only do DisJS.[no CRMSD]"
		calCRMSD=false
	else 
	       lenProb=`cat $probFile | wc -l `
	       lenXYZ=`cat $xyzFile | wc -l `
	       ((i=$lenProb+4))
		if [ $i -ne $lenXYZ ]; then echo "Err [analyseFrag1_Clust.sh]: $probFile($lenProb)+4 diff $xyzFile($lenXYZ)";exit 1;fi
		echo "Will add DisJS and cRMSd"
		calCRMSD=true
	fi
else
	echo "USAGE[10]: $0 <Target_ProbFileName> <Target_xyzFileName:NA> <Target_PosiStar_HMM> <Target_lengthFrag_HMM> <BD_IDsProb> <BD_PathIDsProb> <BD_IDsCRMSD:NA> <BD_PathIDsCRMSD:NA> <Mode:AllFast/AllComplet/FiltComplet/FiltFast*> <OutFileID>" 
	echo -e "[ Ex[10] : $0 T0455.svmi8.27.prob T0455.xyz 1 17 $SAPROFROOT/data/PDB25_HHfragBD_4649ids_5972Frag_newSVM.prob.ids2 $SAPROFROOT/data/PDB25ok/ $SAPROFROOT/data/PDB25_HHfragBD_4649ids_5972Frag.ids2 $SAPROFROOT/data/PDB25ok/ FiltComplet T0455 5PC ]\n"	
	exit 1
fi 


((lenAA=$len+3))
((fin=$deb+$len-1))
((deb2=$deb-1))
((finAA=$deb+$len+3-1))

#seuil DisJS evaluer a partir de newModelSVM : Attention pas pour oldSVMModel.

if false; then #seuil grand pour tester
	if   [ $len -eq 3 ] ; then maxScore=0.16 #0.075 #0.05 
	elif [ $len -eq 4 ] ; then maxScore=0.17 #0.09 #0.06 
	elif [ $len -eq 5 ] ; then maxScore=0.18 #0.105 #0.075 
	elif [ $len -eq 6 ] ; then maxScore=0.19 #0.12 #0.09 
	elif [ $len -eq 7 ] ; then maxScore=0.20 #0.13 #0.1 
	elif [ $len -eq 8 ] ; then maxScore=0.21 #0.14 #0.11 
	elif [ $len -eq 9 ] ; then maxScore=0.22 #0.15 #0.115 
	elif [ $len -eq 10 ] ;then maxScore=0.23 #0.16 #0.125 
	elif [ $len -eq 11 ] ;then maxScore=0.23 #0.17 #0.13 
	elif [ $len -eq 12 ] ;then maxScore=0.23 #0.18 #0.15 
	elif [ $len -eq 13 ] ;then maxScore=0.23 #0.19 #0.155 
	elif [ $len -eq 14 ] ;then maxScore=0.23 #0.20  #0.17 
	elif [ $len -eq 15 ] ;then maxScore=0.23 #0.21 #0.18 
	elif [ $len -eq 16 ] ;then maxScore=0.23 #0.21 
	elif [ $len -eq 17 ] ;then maxScore=0.23 #0.21 
	elif [ $len -eq 18 ] ;then maxScore=0.24 #0.215 
	elif [ $len -eq 19 ] ;then maxScore=0.24 #0.21 
	elif [ $len -eq 20 ] ;then maxScore=0.24 #0.215 
	elif [ $len -eq 21 ] ;then maxScore=0.24 #0.2 
	elif [ $len -eq 22 ] ;then maxScore=0.24 #0.186 
	elif [ $len -eq 23 ] ;then maxScore=0.24 #0.19
	elif [ $len -eq 24 ] ;then maxScore=0.24 #0.19 
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi
fi

#dflt 1er version,environ 4-8% err
if [ "$seuilDisJS" == "old" ]; then
	if   [ $len -eq 3 ] ;then maxScore=0.05 
	elif [ $len -eq 4 ] ;then maxScore=0.06 
	elif [ $len -eq 5 ] ;then maxScore=0.075 
	elif [ $len -eq 6 ] ;then maxScore=0.09 
	elif [ $len -eq 7 ] ;then maxScore=0.1 
	elif [ $len -eq 8 ] ;then maxScore=0.11 
	elif [ $len -eq 9 ] ;then maxScore=0.115 
	elif [ $len -eq 10 ] ;then maxScore=0.125 
	elif [ $len -eq 11 ] ;then maxScore=0.13 
	elif [ $len -eq 12 ] ;then maxScore=0.15 
	elif [ $len -eq 13 ] ;then maxScore=0.155 
	elif [ $len -eq 14 ] ;then maxScore=0.17 
	elif [ $len -eq 15 ] ;then maxScore=0.18 
	elif [ $len -eq 16 ] ;then maxScore=0.21 
	elif [ $len -eq 17 ] ;then maxScore=0.21 
	elif [ $len -eq 18 ] ;then maxScore=0.215 
	elif [ $len -eq 19 ] ;then maxScore=0.21 
	elif [ $len -eq 20 ] ;then maxScore=0.215 
	elif [ $len -eq 21 ] ;then maxScore=0.2 
	elif [ $len -eq 22 ] ;then maxScore=0.186 
	elif [ $len -eq 23 ] ;then maxScore=0.19
	elif [ $len -eq 24 ] ;then maxScore=0.19 
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi 


#3%err clAll
elif [ "$seuilDisJS" == "3PC" ]; then
	if   [ $len -eq 3 ]  ;then maxScore=0.0488 
	elif [ $len -eq 4 ]  ;then maxScore=0.0643 
	elif [ $len -eq 5 ]  ;then maxScore=0.0776 
	elif [ $len -eq 6 ]  ;then maxScore=0.0819 
	elif [ $len -eq 7 ]  ;then maxScore=0.0845
	elif [ $len -eq 8 ]  ;then maxScore=0.0944
	elif [ $len -eq 9 ]  ;then maxScore=0.1146 
	elif [ $len -eq 10 ] ;then maxScore=0.1203 
	elif [ $len -eq 11 ] ;then maxScore=0.1230
	elif [ $len -eq 12 ] ;then maxScore=0.1278
	elif [ $len -eq 13 ] ;then maxScore=0.1371 
	elif [ $len -eq 14 ] ;then maxScore=0.1458
	elif [ $len -eq 15 ] ;then maxScore=0.1487
	elif [ $len -eq 16 ] ;then maxScore=0.1521
	elif [ $len -eq 17 ] ;then maxScore=0.1474
	elif [ $len -eq 18 ] ;then maxScore=0.1535 
	elif [ $len -eq 19 ] ;then maxScore=0.1516
	elif [ $len -eq 20 ] ;then maxScore=0.1516 
	elif [ $len -eq 21 ] ;then maxScore=0.1462
	elif [ $len -eq 22 ] ;then maxScore=0.1438 
	elif [ $len -eq 23 ] ;then maxScore=0.1275
	elif [ $len -eq 24 ] ;then maxScore=0.1228
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi

#4%err clAll
elif [ "$seuilDisJS" == "4PC" ]; then
	if   [ $len -eq 3 ]  ;then maxScore=0.0617 
	elif [ $len -eq 4 ]  ;then maxScore=0.0791 
	elif [ $len -eq 5 ]  ;then maxScore=0.0955 
	elif [ $len -eq 6 ]  ;then maxScore=0.1041 
	elif [ $len -eq 7 ]  ;then maxScore=0.1090
	elif [ $len -eq 8 ]  ;then maxScore=0.1197
	elif [ $len -eq 9 ]  ;then maxScore=0.1362 
	elif [ $len -eq 10 ] ;then maxScore=0.1423 
	elif [ $len -eq 11 ] ;then maxScore=0.1470
	elif [ $len -eq 12 ] ;then maxScore=0.1507
	elif [ $len -eq 13 ] ;then maxScore=0.1581 
	elif [ $len -eq 14 ] ;then maxScore=0.1645
	elif [ $len -eq 15 ] ;then maxScore=0.1668
	elif [ $len -eq 16 ] ;then maxScore=0.1702
	elif [ $len -eq 17 ] ;then maxScore=0.1668
	elif [ $len -eq 18 ] ;then maxScore=0.1703 
	elif [ $len -eq 19 ] ;then maxScore=0.1691
	elif [ $len -eq 20 ] ;then maxScore=0.1680 
	elif [ $len -eq 21 ] ;then maxScore=0.1627
	elif [ $len -eq 22 ] ;then maxScore=0.1592 
	elif [ $len -eq 23 ] ;then maxScore=0.1533
	elif [ $len -eq 24 ] ;then maxScore=0.1452
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi

#5%err clAll
elif [ "$seuilDisJS" == "5PC" -o "$seuilDisJS" == "5PC2" ]; then
	if   [ $len -eq 3 ]  ;then maxScore=0.0706 #0.05 
	elif [ $len -eq 4 ]  ;then maxScore=0.0895 #0.06 
	elif [ $len -eq 5 ]  ;then maxScore=0.105 #>0.105 ----avec %50data(86seq) obtien 0.1044
	elif [ $len -eq 6 ]  ;then maxScore=0.1171 #0.09 
	elif [ $len -eq 7 ]  ;then maxScore=0.1246 #0.1 
	elif [ $len -eq 8 ]  ;then maxScore=0.1355 #0.11 
	elif [ $len -eq 9 ]  ;then maxScore=0.15 #0.15 -----avec %50data(86seq) obtien 0.1489
	elif [ $len -eq 10 ] ;then maxScore=0.1573 #0.125 
	elif [ $len -eq 11 ] ;then maxScore=0.1626 #0.13 
	elif [ $len -eq 12 ] ;then maxScore=0.1668 #0.15 
	elif [ $len -eq 13 ] ;then maxScore=0.1726 #0.155 
	elif [ $len -eq 14 ] ;then maxScore=0.1780  #0.17 
	elif [ $len -eq 15 ] ;then maxScore=0.1799 #0.18 
	elif [ $len -eq 16 ] ;then maxScore=0.1837 #0.21 
	elif [ $len -eq 17 ] ;then maxScore=0.1804 #0.21 
	elif [ $len -eq 18 ] ;then maxScore=0.1843 #0.215 
	elif [ $len -eq 19 ] ;then maxScore=0.1835 #0.21 
	elif [ $len -eq 20 ] ;then maxScore=0.1830 #0.215 
	elif [ $len -eq 21 ] ;then maxScore=0.1779 #0.2 
	elif [ $len -eq 22 ] ;then maxScore=0.1757 #0.186 
	elif [ $len -eq 23 ] ;then maxScore=0.1713 #0.19
	elif [ $len -eq 24 ] ;then maxScore=0.1622 #0.19 
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi

#8%err clAll
elif [ "$seuilDisJS" == "8PC" ]; then
	if   [ $len -eq 3 ]  ;then maxScore=0.0883 #0.05 
	elif [ $len -eq 4 ]  ;then maxScore=0.1046 #0.06 
	elif [ $len -eq 5 ]  ;then maxScore=0.1285 #0.075 
	elif [ $len -eq 6 ]  ;then maxScore=0.1416 #0.09 
	elif [ $len -eq 7 ]  ;then maxScore=0.1507 #0.1 
	elif [ $len -eq 8 ]  ;then maxScore=0.1634 #0.11 
	elif [ $len -eq 9 ]  ;then maxScore=0.1772 #0.115 
	elif [ $len -eq 10 ] ;then maxScore=0.1845 #0.125 
	elif [ $len -eq 11 ] ;then maxScore=0.19   #0.13 
	elif [ $len -eq 12 ] ;then maxScore=0.1936 #0.15 
	elif [ $len -eq 13 ] ;then maxScore=0.1986 #0.155 
	elif [ $len -eq 14 ] ;then maxScore=0.2029  #0.17 
	elif [ $len -eq 15 ] ;then maxScore=0.2070 #0.18 
	elif [ $len -eq 16 ] ;then maxScore=0.2102 #0.21 
	elif [ $len -eq 17 ] ;then maxScore=0.2075 #0.21 
	elif [ $len -eq 18 ] ;then maxScore=0.2104 #0.215 
	elif [ $len -eq 19 ] ;then maxScore=0.2096 #0.21 
	elif [ $len -eq 20 ] ;then maxScore=0.2088 #0.215 
	elif [ $len -eq 21 ] ;then maxScore=0.2052 #0.2 
	elif [ $len -eq 22 ] ;then maxScore=0.2043 #0.186 
	elif [ $len -eq 23 ] ;then maxScore=0.2028 #0.19
	elif [ $len -eq 24 ] ;then maxScore=0.1982 #0.19 
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi

#10%err clAll
elif [ "$seuilDisJS" == "10PC" ]; then
	if   [ $len -eq 3 ]  ;then maxScore=0.0941 #0.05 
	elif [ $len -eq 4 ]  ;then maxScore=0.1142 #0.06 
	elif [ $len -eq 5 ]  ;then maxScore=0.1391 #0.075 
	elif [ $len -eq 6 ]  ;then maxScore=0.1531 #0.09 
	elif [ $len -eq 7 ]  ;then maxScore=0.1636 #0.1 
	elif [ $len -eq 8 ]  ;then maxScore=0.1767 #0.11 
	elif [ $len -eq 9 ]  ;then maxScore=0.1902 #0.115 
	elif [ $len -eq 10 ] ;then maxScore=0.1981 #0.125 
	elif [ $len -eq 11 ] ;then maxScore=0.2039 #0.13 
	elif [ $len -eq 12 ] ;then maxScore=0.2086 #0.15 
	elif [ $len -eq 13 ] ;then maxScore=0.2142 #0.155 
	elif [ $len -eq 14 ] ;then maxScore=0.2179 #0.17 
	elif [ $len -eq 15 ] ;then maxScore=0.2184 #0.18 
	elif [ $len -eq 16 ] ;then maxScore=0.2232 #0.21 
	elif [ $len -eq 17 ] ;then maxScore=0.2204 #0.21 
	elif [ $len -eq 18 ] ;then maxScore=0.2231 #0.215 
	elif [ $len -eq 19 ] ;then maxScore=0.2220 #0.21 
	elif [ $len -eq 20 ] ;then maxScore=0.2209 #0.215 
	elif [ $len -eq 21 ] ;then maxScore=0.2172 #0.2 
	elif [ $len -eq 22 ] ;then maxScore=0.2161 #0.186 
	elif [ $len -eq 23 ] ;then maxScore=0.2138 #0.19
	elif [ $len -eq 24 ] ;then maxScore=0.2080 #0.19 
	else echo "Length of Fragment [=$len] must between 3 and 24!"
	fi
else
	echo "Err: Can't find your threshold DisJS: $seuilDisJS(last input),please check it. <Threshold DisJS:old/3PC/4PC/5PC/8Pc/10PC>"
	exit 1
fi

#Ray for Cluster analysis
if true; then # auto ray
#if false; then # ray cluster = 1 
	if   [ $len -eq 3 ]  ;then RayCRMSD=1 
	elif [ $len -eq 4 ]  ;then RayCRMSD=1.2 
	elif [ $len -eq 5 ]  ;then RayCRMSD=1.4 
	elif [ $len -eq 6 ]  ;then RayCRMSD=1.5 
	elif [ $len -eq 7 ]  ;then RayCRMSD=1.6 
	elif [ $len -eq 8 ]  ;then RayCRMSD=1.75 
	elif [ $len -eq 9 ]  ;then RayCRMSD=1.9 
	elif [ $len -eq 10 ] ;then RayCRMSD=2
	elif [ $len -eq 11 ] ;then RayCRMSD=2.1 
	elif [ $len -eq 12 ] ;then RayCRMSD=2.2 
	elif [ $len -eq 13 ] ;then RayCRMSD=2.35 
	elif [ $len -eq 14 ] ;then RayCRMSD=2.5 
	elif [ $len -eq 15 ] ;then RayCRMSD=2.6 
	elif [ $len -eq 16 ] ;then RayCRMSD=2.75 
	elif [ $len -eq 17 ] ;then RayCRMSD=2.8 
	elif [ $len -eq 18 ] ;then RayCRMSD=3 
	elif [ $len -eq 19 ] ;then RayCRMSD=3.1 
	elif [ $len -eq 20 ] ;then RayCRMSD=3.2 
	elif [ $len -eq 21 ] ;then RayCRMSD=3.25 
	elif [ $len -eq 22 ] ;then RayCRMSD=3.35 
	elif [ $len -eq 23 ] ;then RayCRMSD=3.45
	elif [ $len -eq 24 ] ;then RayCRMSD=3.5 
	else 
		echo "Err: Length of Fragment [=$len] must between 3 and 24! "
		exit 1
	fi
else
	RayCRMSD=1
fi

if [ "$mode" = "AllComplet" -o "$mode" = "AllFast" ];then
	maxScore1=10000
	maxScore2="NA"
	fileNameOutput=$outFileID'-'$deb'-'$fin'.nofilt'
elif [ $mode = "FiltComplet" -o $mode = "FiltFast" ];then
	if $calCRMSD ;then
		maxScore1=10000
		maxScore2=$maxScore
	else
		if [ $mode = "FiltFast" ];   then maxScore1=$maxScore;fi
		if [ $mode = "FiltComplet" ];then maxScore1=10000;fi		
		maxScore2=$maxScore
	fi
	fileNameOutput=$outFileID'-'$deb'-'$fin'.filt'
else
	echo "Err: Please used Mode: AllComplet, AllFast, FiltComplet or FiltFast[Dflt]."
	exit 1
fi


delTempfiles=true #[Del tempfiles - Econimize disc]
#delTempfiles=false

if [ -f $outFileID-$deb-$fin.results ];then \rm $outFileID-$deb-$fin.results; fi 
if [ -f $outFileID-$deb-$fin.js ]     ;then \rm $outFileID-$deb-$fin.js     ; fi
if [ -f $outFileID-$deb-$fin.rmsd ]   ;then \rm $outFileID-$deb-$fin.rmsd   ; fi

echo "--------------------[DisJS]---------------------------"
if [ $mode = "FiltComplet" -o $mode = "AllComplet" ];then outJSperPosition="-outJS";else outJSperPosition="";fi

echo "$binPATH/PrfScore -iPrf $probFile -iIds $idJS -dbPath $pathJS -maxHits 100000000000000000 -nBest 27 -maxScore $maxScore1 -o $outFileID-$deb-$fin.results -from $deb2 -to $fin $outJSperPosition>$outFileID-$deb-$fin.js2"
$binPATH/PrfScore -iPrf $probFile -iIds $idJS -dbPath $pathJS -maxHits 100000000000000000 -nBest 27 -maxScore $maxScore1 -o $outFileID-$deb-$fin.results -from $deb2 -to $fin $outJSperPosition>$outFileID-$deb-$fin.js2

if [ $mode = "FiltComplet" -o $mode = "AllComplet" ];then cat $outFileID-$deb-$fin.js2 | sed s/" "/"-"/g |sed s/"-$"/""/> $outFileID-$deb-$fin.js;  \rm $outFileID-$deb-$fin.js2; fi

if ! $calCRMSD;then     #[ne calcule pas RMSD] mode: FiltFast AllFast
	if [ $mode = "FiltComplet" -o $mode = "AllComplet" ];then
		$scriptPATH/fusionSelection2Files.py $outFileID-$deb-$fin.results $outFileID-$deb-$fin.js $maxScore2 NA $fileNameOutput
	else
		mv $outFileID-$deb-$fin.results $fileNameOutput
		# echo fileNameOutput $fileNameOutput
		\rm $outFileID-$deb-$fin.js2 #il droit contient 0 ligne.
	fi
	# exit;
else    #calcule RMSD

	echo "--------------------[crmsd]-----------------------"
	echo "/usr/local/GLSearch/bin/GLProtSearch -ipdbxyz $xyzFile  -len $lenAA -iIds $idCRMSD  -lhw 3 -gsc -1 -lsc -1  -bcl 0 -dbPath $pathCRMSD -np 1 -maxHits 100000000000000000 -maxRMSd 1000 -from $deb -to $finAA -o stdout | cut -d" " -f1-10 | sed s/'\.xyz'// > $outFileID-$deb-$fin.rmsd"
	/usr/local/GLSearch/bin/GLProtSearch -ipdbxyz $xyzFile  -len $lenAA -iIds $idCRMSD  -lhw 3 -gsc -1 -lsc -1  -bcl 0 -dbPath $pathCRMSD -np 1 -maxHits 100000000000000000 -maxRMSd 1000 -from $deb -to $finAA -o stdout | cut -d" " -f1-10 | sed s/'\.xyz'// > $outFileID-$deb-$fin.rmsd

	echo "-----------------[fusion file]--------------------"
	if [ $mode = "FiltComplet" -o $mode = "AllComplet" ];then 
		$scriptPATH/fusion2FilesParLigne.py $outFileID-$deb-$fin.rmsd $outFileID-$deb-$fin.js -t $fileNameOutput'_'
		$scriptPATH/fusionSelection2Files.py $outFileID-$deb-$fin.results $fileNameOutput'_' $maxScore2 NA $fileNameOutput
	else             #mode: FiltFast AllFast
		$scriptPATH/fusionSelection2Files.py $outFileID-$deb-$fin.results $outFileID-$deb-$fin.rmsd $maxScore2 NA $fileNameOutput
	fi

	if $delTempfiles; then \rm $outFileID-$deb-$fin.js $outFileID-$deb-$fin.rmsd $outFileID-$deb-$fin.results;fi
fi

cat $fileNameOutput | sed s/'\.svmi8\.27\.prob'//g | sed s/'.newModelSVM'//g | sed s/'\.prob'//g | sed s/'\.new'//g| sed s/'\.novNovNovSVMBlast'//g| sed s/'\.novNovNovSVM'//g|sed s/'\.novNovSVMBlast'//g| sed s/'\.novSVMBlast'//g|sed s/'\.novBlast'//g| sed s/'\.novNovSVM'//g| sed s/'\.novSVM'//g > $fileNameOutput'2'

#--------------------[ATTENTION STOP]-----------------------------
#mv $fileNameOutput'2' $fileNameOutput.allCl
#exit 1

mv $fileNameOutput'2' $fileNameOutput

#mode fast ne filt rien,donc arreter ici.
if [ $mode = "AllComplet" -o $mode = "AllFast" ];then exit 1;fi



echo "---------------------[max 500 BestDisJS]------------------------"          
#Apres filtre par seuil DistJS,des fois les fragments trouves (qui <Seuil) sont encore tres nombreuse.
#ici filtre pour assurer le nombre de fragment <10000. A cause de l'etap prochain en fonction de ~ nb^2, consomme trop de temps 
$scriptPATH/CutNBest.py $fileNameOutput 7 500 $fileNameOutput

#----------------------------[Cluster]--------------[Rest 1 seul frag]-------------------
BestParPosi="Yes" #chaque len et posi choisi un best fragment
echo "---------------------[Cluster]------------------------: RayCRMSD="$RayCRMSD
#PATH_BD=`dirname $delStrPath_`'/'
PATH_BD=$pathJS
echo PATH_BD $PATH_BD
echo "$scriptPATH/AnalyseDATACluster.py $fileNameOutput $RayCRMSD $fileNameOutput $PATH_BD $binPATH/RMSD $BestParPosi"
$scriptPATH/AnalyseDATACluster.py $fileNameOutput $RayCRMSD $fileNameOutput $PATH_BD $binPATH/RMSD $BestParPosi


#Mode
#(manque xyz/PDB file, ne calcule pas CRMSD)
# FiltFast    : Filtre DisJS par "PrfScore",                 manquer DisJS par position.
# AllFast     : Ne filtre pas DisJS, All the Data,           manquer DisJS par position, ne fait pas 10000bestDisJS and cluster
# FiltComplet : Filtre DisJS par "fusionSelection2Files.py", ajouter DisJS par position.
# AllComplet  : Ne filtre pas DisJS, All the Data,           ajouter DisJS par position, ne fait pas 10000bestDisJS and cluster
