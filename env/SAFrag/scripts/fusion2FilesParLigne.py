#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Fusion 2 files par ligne
"""
import sys
import string
import time


def outPut(text, where = None,i=0, mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file : "+where+"]["+str(i)+"]"
    if where:
        f.close()
        
if __name__ == "__main__":
    cmd=" ".join(sys.argv)
    if len(sys.argv) !=5:
        print "%s [Err: arg != 4] "%cmd
        sys.exit("usage: fusion2FilesParLigne.py <file 1> <file 2> <separator -e[space],-t[tab] or -n[line break]> <OutputFile>")
    print cmd
    file1=sys.argv[1]
    file2=sys.argv[2]
    sep=sys.argv[3]
    outFile=sys.argv[4]
    print "Loading file1 [%s] ... ..."%(file1)
    f  = open(file1)
    l1 = f.readlines()
    f.close()
    len1=len(l1)
#    print "%s%s"%(time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!")
        
    print  "Loading file2 [%s] ... ..."%(file2)
    g  = open(file2)
    l2 = g.readlines()
    g.close()
    len2=len(l2)
#    print "%s%s"%(time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!")
    if len1!=len2:print "Err : length File1[%d] diff File2[%d]!"%(len1,len2);sys.exit("Err : length File1[%d] diff File2[%d]!"%(len1,len2))
    if len1==0:print "Err : %s %s are empty!"%(file1,file2);sys.exit("Err : %s %s are empty!"%(file1,file2)) 
   
    
    print  "Fusion 2 files ... ..."
    selectOut=""
    for i in range(len2):
        if sep=="-e": selectOut+="%s %s\n"%(l1[i].replace("\n",""),l2[i].replace("\n",""))
        elif sep=="-t" : selectOut+="%s\t%s\n"%(l1[i].replace("\n",""),l2[i].replace("\n",""))
        elif sep=="-n" : selectOut+="%s\n%s\n"%(l1[i].replace("\n",""),l2[i].replace("\n",""))
        else : print "Separator is not correct [-e, -t or -n] ";sys.exit("Separator is not correct [-e, -t or -n] ")
    outPut(selectOut,outFile,len1)
    print "%s%s"%(time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!")
