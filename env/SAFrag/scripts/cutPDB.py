#!/usr/bin/env python
import sys
#sys.path.append("/data/PyTools/Classes")
#import PDB
mypath = [
	"/data/PyTools/Classes",
	"/home/ionesco/yimin/scripts/PyTools/Classes",
        "/home/freetown/shen/scripts/PyTools/Classes",
        "/nfs/freetown/user/shen/scripts/PyTools/Classes",
	"/nfs/eurydice/PyTools/Classes"
	] 
sys.path += mypath
# import PDB6_7 as PDB
import PyPDB.PyPDB as PDB

if len(sys.argv) != 4:
	sys.exit("usage: argv[0] [PDB FILE] [posi_start] [posi_end] #Attention : posi start with 1. is not the posi in PDB file.")

x = PDB.PDB(sys.argv[1])
y =x[(int(sys.argv[2])-1):int(sys.argv[3])] # commencer a position 1 (pas 0)
y.out()
