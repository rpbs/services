#!/bin/bash

SAPROFROOT="/nfs/freetown/user/tuffery/prgs/SAProf"
PATH_SCRIPTS="$SAPROFROOT/SAProf3"


if [ $# != 4 ] ; then
echo "USAGE: $0 <RefPDB:FST_complet> <TargetPDB:PDB_fragment> <ProbFile_PDBcomplet> <Path_Output>"
echo "Ce programme est surtout pour couper les ficheirs Prob[qui vienent de seqence complet]."
echo "Aussi pour couper des PDB imcomplet aux fragments, pour eviter l'analyser sur seq_casse(aaaa__aa)"
exit 1
fi 

# resoudre les problemes des PDB files deb!=1 [ex: HHfrag/pdbs25/1ootA.pdb]
# resoudre les problemes des PDB files deb!=1 & mi-seq incomplet [ex: HHfrag/pdbs25/1fm0E.pdb posi:40-46]

completFST=$1
fragPDB=$2
probFile=$3
pathOut=$4

debug=false
#debug=true

if false ; then
debug=true
completFST="/home/freetown/shen/PP-Search/Data/Target/CASP9ok/T0620/T0620.fst"
fragPDB="/home/freetown/shen/PP-Search/Data/Target/CASP9resu/cgi/T0620.pdb"
probFile="/home/freetown/shen/PP-Search/Data/Target/CASP9ok/T0620/T0620.svmi8.27.prob"
pathOut="/home/freetown/shen/PP-Search/Data/Target/Test/"

completFST="/nfs/freetown/user/shen/PP-Search/Data/PDB25/1hdhA.fasta"
fragPDB="/nfs/freetown/user/shen/PP-Search/Data/PDB25/1hdhA.pdb"
probFile="/nfs/freetown/user/shen/PP-Search/Data/PDB25/1hdhA.svmi8.27.prob"
pathOut="/nfs/freetown/user/shen/PP-Search/Data/"
fi

posiFSTcomplet=""
posiPDBfrag=""
#--------------------------------------------------------------
#a cause de certain PDB file ne contient pas CA, mais que N (ex:1f8vD posi:18)
#for j in `cat $fragPDB| grep '^ATOM' | grep ' CA '| cut -c 18-26 | sed s/" "/"-"/g | uniq`; do


fstFragPDB_c="";fstFragPDB="";seqFragPDB_c="";seqFragPDB=""

for j in `cat $fragPDB| grep '^ATOM' | cut -c 18-26 | sed s/" "/"-"/g | uniq`; do
seqFragPDB_c=$seqFragPDB_c" "$j
aa3=`echo $j|cut -d"-" -f1`
fstFragPDB_c=$fstFragPDB_c`$PATH_SCRIPTS/AAnameBetween1L3L.py $aa3`
done
#a cause de certain PDB file ne contient pas CA, mais que N (ex:1f8vD posi:18)
#for j in `cat $fragPDB| grep '^ATOM' | grep ' CA '| cut -c 18-26 | sed s/" "/"-"/g | uniq`; do

for j in `cat $fragPDB| grep '^ATOM' | grep ' CA '| grep -v "UNK"| cut -c 18-26 | sed s/" "/"-"/g | uniq`; do
seqFragPDB=$seqFragPDB" "$j
aa3=`echo $j|cut -d"-" -f1`
fstFragPDB=$fstFragPDB`$PATH_SCRIPTS/AAnameBetween1L3L.py $aa3`
done


endPosi=`echo $j| cut -c 6-9 | sed s/'-'//g`
fstCompletPDB="";for l in `cat $completFST | grep -v '>'`; do fstCompletPDB+=$l;done
lenFstFrag_c=${#fstFragPDB_c}
lenFstFrag=${#fstFragPDB}
lenFstComplet=${#fstCompletPDB}
if $debug;then echo "seqFragPDB_c: "$seqFragPDB_c; echo "seqFragPDB  : "$seqFragPDB;fi
if $debug;then echo -e "seqFrag_c["$lenFstFrag_c"]:"$fstFragPDB_c"\nseqFrag  ["$lenFstFrag"]:"$fstFragPDB"\nseqCompl ["$lenFstComplet"]:"$fstCompletPDB;fi

#Verif si X in seq_complet (X possible in 2 cote, pas milieu)
seq_verif=`echo $fstCompletPDB | sed s/"X*$"// |sed s/"^X*"//`
seq_verif2=`echo $seq_verif | sed s/"X"//g` 
if [ "$seq_verif" != "$seq_verif2" ]; then echo "False: X in FST_Complet! Quit...";exit 1;fi

#--------------------------------------------------------------
posiNext=-1;posiStart_compl=-1;posiStart_frag=-1;nbFrag=0

for((i=1;i <= $lenFstFrag;i++)); do
((compteComplet++)) ; ((i2++))
posi=`echo $seqFragPDB| cut -d" " -f $i | cut -c 6-9 | sed s/'-'//g`

aaFrag=`echo $fstFragPDB | cut -c $i`
aaComplet=`echo $fstCompletPDB | cut -c $posi`
if $debug;then  echo $i"-"$posi $aaFrag"-"$aaComplet; fi

if [ $aaFrag != $aaComplet ]; then echo "Err:" $posi $aaFrag" diff "$aaComplet;exit 1;fi
if [ $posiStart_compl -eq -1 ]; then posiStart_compl=$posi;posiStart_frag=$i;fi
#echo  $posiNext  $posi
if [ $posiNext -eq $posi  -o $posiNext -eq -1 ]; then
  posiEnd_compl=$posi
  posiEnd_frag=$i
  if [ $i -eq $lenFstFrag ]; then 
    if [ $posiEnd_compl -lt $posiStart_compl ]; then posiEnd_frag=$posiStart_frag;posiEnd_compl=$posiStart_compl;fi
    echo "Last [posiStart:"$posiStart "posiEnd:"$posiEnd"]"
    posiFSTcomplet=$posiFSTcomplet' '$posiStart_compl'-'$posiEnd_compl
    posiPDBfrag=$posiPDBfrag' '$posiStart_frag'-'$posiEnd_frag;((nbFrag++))
  fi
else
  if [ $posiEnd_compl -lt $posiStart_compl ]; then posiEnd_frag=$posiStart_frag;posiEnd_compl=$posiStart_compl;fi
  echo "[posiStart:"$posiStart "posiEnd:"$posiEnd"]"
  posiFSTcomplet=$posiFSTcomplet' '$posiStart_compl'-'$posiEnd_compl
  posiPDBfrag=$posiPDBfrag' '$posiStart_frag'-'$posiEnd_frag;((nbFrag++))
  posiStart_compl=$posi
  posiStart_frag=$i
fi
((posiNext=$posi+1))
done

echo "FSTCompl[L:"$lenFstComplet"]  {"$posiFSTcomplet" }" $nbFrag
echo "PDBFrag_[L:"$lenFstFrag"]  {"$posiPDBfrag" }" $nbFrag

if [ $lenFstFrag_c -ne $lenFstFrag ];then
  posiPDBfrag_c=""
  same=true
  for j in `echo $posiPDBfrag | sed s/"-"/" "/g`;do
    info=`echo $seqFragPDB| cut -d" " -f $j`
    for((i=1;i <= $lenFstFrag_c;i++)); do
      info_c=`echo $seqFragPDB_c| cut -d" " -f $i`
      if [ "$info_c" = "$info" ];then
     	if $same ; then 
		  posiPDBfrag_c=$posiPDBfrag_c" "$i;same=false
        else 
		  posiPDBfrag_c=$posiPDBfrag_c"-"$i;same=true
        fi
   	    break
      fi  
    done
  done
  echo "PDBFrag [L:"$lenFstFrag_c"]  {"$posiPDBfrag_c" }" $nbFrag
else
  posiPDBfrag_c=$posiPDBfrag
fi

#--------------------------------------------------------------
id=`basename $fragPDB | sed s/'\.pdb'/''/ | sed s/'.hmmpdb'/''/`

for ((i=1;i<=$nbFrag;i++));do
posi1=`echo $posiFSTcomplet| cut -d" " -f $i`
aa1=`echo $fstCompletPDB | cut -c $posi1`; lenAA1=`echo ${#aa1}`
posi2=`echo $posiPDBfrag_c| cut -d" " -f $i`
aa2=`echo $fstFragPDB_c | cut -c $posi2`;
if [ $nbFrag -eq 1 ]; then 
  outFilePath=$pathOut$id
  outFile=$pathOut$id'/'$id
else
  outFilePath=$pathOut$id$i
  outFile=$pathOut$id$i'/'$id$i
fi
mkdir $outFilePath
#----------[Verif et Create FST]-------------------
if [ "$aa1" = "$aa2"  ]; then
  echo "Verif SeqAA ["$posi1" "$posi2"] L:"$lenAA1" -- ok"
  echo '>'$id' '$posi1' L:'$lenAA1 >$outFile'.fst'
  echo $aa1 >>$outFile'.fst'
  echo 'Creat: '$outFile'.fst'
else 
  echo "Err : (not same seq) "
  echo "PDBcomplete: "$aa1
  echo "PDBfragment: "$aa2
  exit 1
fi
#-----------------[Cut PDB]-----------------------
posiPDB=`echo $posi2 | sed s/'-'/' '/`
$PATH_SCRIPTS/cutPDB.py $fragPDB $posiPDB >$outFile'.pdb'
echo 'Creat: '$outFile'.pdb'

#-------------[Verif et create Prob]------------------
probExten=`basename $probFile | cut -d"." -f2-`
lenProb=`wc -l $probFile | cut -d" " -f1`
lenAA=`echo ${#fstCompletPDB}`
((temp=$lenProb+3))
if [ $temp -eq $lenAA ]; then
  #echo "Verif Length Prob ["$lenProb"] -- ok"
  fromProb=`echo $posi1| cut -d"-" -f1`
  toProb_=`echo $posi1| cut -d"-" -f2`
  ((toProb=$toProb_-3))
  cat $probFile | sed -n  $fromProb','$toProb'p' >$outFile'.'$probExten
  echo 'Creat: '$outFile'.'$probExten
else
  echo "Err : (length same seq)"
  echo 'L_AA:' $lenAA'  L_Prob:'$lenProb  
  exit 1
fi

done

