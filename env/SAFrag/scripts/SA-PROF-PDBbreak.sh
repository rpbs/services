#!/bin/bash

# ne utilise pas. pas besoin pour instant
# ce script est pour le cas : fasta complet mais pdb n'est pas complet(avec les trous)
# position de fst seq droit correspond que pdb file position. 
#Exemple: si pdb commence du 3, fst seq droit aussi contient posi 1 et posi 2 lettres.(sinon completer par X) 

SAPROFROOT="/nfs/freetown/user/tuffery/prgs/SAProf"
PATH_SCRIPTS="$SAPROFROOT/SAProf4"

if [ $# == 4 ] ; then
	fst=$1
	pdb=$2
	SVMmodel=$3
	blastPath=$4
elif [ $# == 2 ] ; then
	fst=$1
	pdb=$2
	SVMmodel="pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #Dflt model (novSVMmodel)
#	SVMmodel="pc50_res2.5_R0.25_chains7059.random.Cl0Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #pas encore tester (novNovNovSVMModel)
	blastPath="/nfs/freetown/banks/biomaj/blastdb"
else
	echo "Usage:  $0 <FST file> <PDB file> <SVM model> <Blast path> "
	echo "Usage:  $0 <FST file> <PDB file>"
	echo "Attention: fst file position must correspond PDB file position."
	exit 1
fi


# PATH_SCRIPTS="/nfs/freetown/user/shen/PP-Search/scripts/ProfAnalyseV3"
if [ ! -d $PATH_SCRIPTS ];then echo "Err: Before 1st run, please check PATH_SCRIPTS";exit 1;fi

#option dflt : [PDB25]
IDS_PROB="$SAPROFROOT/data/PDB25_HHfragBD_4649ids_5972Frag_newSVM.prob.ids2"
IDS_CRMSD="$SAPROFROOT/data/PDB25_HHfragBD_4649ids_5972Frag.ids2"
PATH_CRMSD="$SAPROFROOT/data/PDB25ok/"
MODE="FiltFast"
MAXRUN=400

#id="sym"
id=`echo $fst| sed s/"\.fst"//|sed s/"\.fasta"//`

\cp $fst $id.fasta   #PyPPPExec risque de ecraser le fst complet file
#-------------------1. [fasta complet => Prob complet]------------------
PyPPPExec -s $fst --SVMmodel $SVMmodel --blastDbPath $blastPath -l $id
#PyPPPExec -s $fst --SVMmodel $SVMmodel -l $id


#-------------------2. [PDB incomplet => fragment fst hmmpdb xyz]------------------
PDBSAExec -3 $pdb -l $id
#exit 1


#==========================[ pour chaque fragment :
for i in `ls $id*.hmmpdb| sed s/"\.hmmpdb"/""/`;do
	echo $i
#-------------------3. [FST complet+PDB incomplet => fragment PROB and fst hmmpdb xyz]------
	$PATH_SCRIPTS/CutPDBtemplFST.sh $id.fasta $i.hmmpdb $id.svmi8.27.prob ./
	mv $i.xyz $i/
	#SA-PROF
	cd $i
	pwd
#-------------------4. [fragment PROB and XYZ => Analyse fragment]------------------
	$PATH_SCRIPTS/SA-PROF.py $i.svmi8.27.prob $i.xyz $IDS_PROB $IDS_CRMSD $PATH_CRMSD $MODE $MAXRUN
	cd ..
done
