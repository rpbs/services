#!/usr/bin/python
'''
Created on Dec, 2012

@author: shen
'''
import sys
import copy
import os
import re


class Run:

    def __init__(self,FileName):
        """
        read file *.results
        Delete redundancy for the same hits with difference length in same position.
        """

        (dictResuFrag,tabInfo,strInput,nbIn)=self.inputFile(FileName)
#        print strInput
#        print "----------------------------------------"

        self.tabInfo=self.suppRedondFrag(dictResuFrag)
        self.tabStr=""
        for i in self.tabInfo:
            self.tabStr+="\t".join(i)+"\n"
        print "----lines[%d]----------->[Delete Redundancy]---------->lines[%d]"%(nbIn,len(self.tabInfo))


    def suppRedondFrag(self,dictResuFrag):
        tabSelect=[]
        for idx in dictResuFrag.keys():
            nbFrag=len(dictResuFrag[idx])
            if nbFrag >1:
                tabPosi=[]
                unTab=self.sortList(dictResuFrag[idx],-1)
                for n in range(len(unTab)):
                    posiFrom=int(unTab[n][2])
                    posiTo=int(unTab[n][3])
#                    print posiFrom,"------>",posiTo
                    AjouteFrg=False
                    for k in range(posiFrom,(posiTo+1)):
                            if k not in tabPosi: AjouteFrg=True
                            tabPosi.append(k)
                    if AjouteFrg:tabSelect.append(unTab[n])
#                    print tabSelect
                    tabPosi=sorted(set(tabPosi))
#                    print tabPosi
            else : tabSelect+=dictResuFrag[idx]

        return tabSelect

    def sortList(self,tabIn,numCol1=-1,numCol2=-4):
        """
        ex: T05611    2xseA_1    69    92    74    97    0.131771    0.063258    27    T05611-69-92-Cl1    1    0.962
        regle: 1)Precision col=  [-1]
               2)Len       col= [-4]
        """
        for x in tabIn:
            x[numCol1]=float(x[numCol1])   #Eff Cluster
            x[numCol2]=int(x[numCol2]) #DisJS
#        print tabIn
        tabIn=sorted(tabIn,key=lambda k:(-k[numCol1],-k[numCol2])) #EJSL
        tabIn= [map(str,x) for x in tabIn]
#        print tabIn
        return tabIn

    def inputFile(self,fname,col_id2=2):
        """
        read un file plusieurs colonne sep=" "
        
        T05151    2yxxA     11    17     19    25    0.014191    0.517723    10    T05151-11-17-Cl1      495
        T05151    3bjdC1   101    107    35    41    0.046399    1.191462    10    T05151-101-107-Cl1    485
        T05151    3ck2A    100    106    91    97    0.049969    1.506331    10    T05151-100-106-Cl1    479

        """
        nb=0
        f = open(fname)
        dictResuFrag={}
        tabInfo=[]
        strInput=""
        print "[Load File:%s]"%fname
        l = f.readlines()
        f.close()
        for i in range(0,len(l)):
            tempTab=[]
            if len(l)!=0:
                if l[i]!="\n" and l[i][0]!="#" and l[i].split()[0]!="file1":
                    strInput+=l[i]
                    nb+=1
                    tempTab=l[i].replace("\n","").split()
                    id2=tempTab[col_id2-1]
                    if dictResuFrag.has_key(id2):
#                        print dictResuFrag[id2]
                        dictResuFrag[id2].append(tempTab)
                    else :
                        dictResuFrag[id2]=[tempTab]
                        tabInfo.append(id2)
            else :
                print "Errors empty line No:",i
                sys.exit();
        return dictResuFrag,tabInfo,strInput,nb

def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file : "+where+"]"
    if where:
        f.close()

if __name__ == "__main__":

    if len(sys.argv) == 3:
        FileName=sys.argv[1]
        outFile=sys.argv[2]
    else:
        print "Delete redundancy for the same hits with difference length in same position."
        sys.exit("Usage:"+sys.argv[0]+"[fileIn] [Output]")
        
    x=Run(FileName)
    outPut(x.tabStr,outFile)
    sys.exit()



#    FileName="/home/shen/workspace/INSERM/PPresearch/T05611.filt.bestCl.SAFrag"
#    outFile="/home/shen/aas"
#    x=Run(FileName)
#    print x.tabStr
#    outPut(x.tabStr,outFile)


