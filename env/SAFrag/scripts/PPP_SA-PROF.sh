#!/bin/bash
#$ -q all.q
#$ -cwd
#$ -V
#$ -o SA-PROF.out
#$ -e SA-PROF.err
#$ -t 1-1


PyPPPExec -s ../test/T05213.fst --SVMmodel pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model --blastDbPath --blastDbPath /nfs/freetown/banks/biomaj/blastdb -l SA-PROF
