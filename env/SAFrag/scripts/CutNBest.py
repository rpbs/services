#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Fusion 2 files par ligne
"""
import sys
import string
import time

def inputFile(fname):
    """
    read a *.results.final
    'file1', 'file2', 'length', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS', 'cRMSd_f', 'TMScore', 'cRMSd', 'GDT_TS', 'len_fstFile1'
    'file1', 'file2', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS','RMSd', 'DisJSperSite'
    """
    f = open(fname)
    print " [Load ScoreFile:%s]"%fname
#        self.AffText+="[Load ScoreFile:%s]\n"%fname
    l = f.readlines()
    f.close()
    tabData = []
    for i in range(0,len(l)):
        tempTab=[]
        if len(l)!=0:   
            if l[i]!="\n" and l[i][0]!="#":
                tempTab=l[i].replace("\n","").split()
                tabData.append(tempTab)
        else :
            print "Errors";
            sys.exit();
    return tabData

def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file :"+where+"]"
    if where:
        f.close()
        
if __name__ == "__main__":
    cmd=" ".join(sys.argv)
    if len(sys.argv) !=5:
        print "%s [Err: arg != 4] "%cmd
        sys.exit("usage: CutNBest.py <fileName> <numCol:7> <BestNombre:10000> <Output>")
    print cmd
    
    file=sys.argv[1]
    numCol=int(sys.argv[2])
    nb=int(sys.argv[3])
    outFile=sys.argv[4]
    
    tabData=inputFile(file)
#    print tabData
    if len(tabData)<nb: nb=len(tabData)
    
#	tabData.sort(key=lambda k:k[numCol-1])
    tabData=sorted(tabData,key=lambda k:k[numCol-1]) #num col DisJS=7
    tabData= [map(str,x) for x in tabData]
    
    outstr=""    
    for i in range(nb):
        outstr+="%s\n"%("\t".join(tabData[i]))
    print "%s[%d] --(%dbestDisJS)--> %s[%d]"%(file,len(tabData),nb,outFile,nb)
    if outstr!="": outPut(outstr,outFile)
