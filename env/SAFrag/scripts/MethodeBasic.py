'''
Created on May 27, 2010

@author: shen
'''
import sys
import re
import os
#CouperNbsite2cotes=8
CouperNbsite2cotes=0
    
def inputFst(fname):
    """
    read a Fst file 
    saved in a str 
    """
    f = open(fname)
    Seq=""
    next=True
    while next:
        l=f.readline()
#            print l
        if len(l)!=0: 
            if l!="\n" and l[0]!=">":
                Seq+=l.replace("\n","")
        else : next=False
    f.close()
#    return Seq.replace(".", "").replace(" ","")
    seq=Seq.replace(".", "").replace(" ","")
    return seq[CouperNbsite2cotes:(len(seq)-CouperNbsite2cotes)]
    
def inputHmm(fname):
    return inputFst(fname)

def inputStride(fname):
    return inputFst(fname)

def inputPsea(fname):
    """
    read a P-sea(*.sea) file 
    saved in a str 
    """
    f = open(fname)
    Seq=""
    next=True
    while next:
        if f.readline()=="secondary structure:p-sea\n":
            while next:
                l=f.readline()
                if len(l)!=0 and l!="*\n": 
                    Seq+=l.replace("\n","")
                else: next=False
    f.close()
#    return Seq.replace("-", "")
    seq=Seq.replace("-", "")
    return seq[CouperNbsite2cotes:(len(seq)-CouperNbsite2cotes)]

def converPseaStridToSS(seqPseaStride):
    seqSS=""
    for i in range(len(seqPseaStride)-3):
        if seqPseaStride[i:i+4].count("H")>=3:
            seqSS+="H"
        elif seqPseaStride[i:i+4].count("E")>=3:
            seqSS+="E"
        else : seqSS+="C"
    return seqSS

def inputProb(fname,nbSA=27):
    """
    read a probability file (27 probs per line)
    saved in a table 
    """
    f = open(fname)
    l = f.readlines()
    f.close()
    tabProb = []
    for i in range(0,len(l)):
        tempTab=[]
        tempTab=l[i].replace("\n","").split()
        if len(l[i].split()) != 0:
            if len(tempTab)!=nbSA: 
                print "Check Err [line %d]: length is not equal to %d! line"%(i,nbSA)
                sys.exit()
            tabProb.append(tempTab)
#    return tabProb  
    return tabProb[CouperNbsite2cotes:(len(tabProb)-CouperNbsite2cotes)]   
       
def inputProb2(fname,nbSA=27):
    """
    read a probability file (27 probs per line)
    saved in a table 
    """
    f = open(fname)
    l = f.readlines()
    f.close()
    tabProb = []
    for i in range(0,len(l)):
        tempTab=[]
        tempTab=l[i].replace("\n","").split()
        if len(l[i].split()) != 0:
            if len(tempTab)!=nbSA: 
                print "Check Err [line %d]: length is not equal to %d! line"%(i,nbSA)
                sys.exit()
            tabProb.append(" ".join(tempTab))
#    return tabProb   
    return tabProb[CouperNbsite2cotes:(len(tabProb)-CouperNbsite2cotes)] 

def inputRosetta(fname):
    """
    read a Rosetta file
    saved in a table 
    tabData[9]: ['8', '3ew0', 'A', '45', '52', 'RCWRSKKF']
    tabData[10]:['7', '2k04', 'D', '28', '35', 'EPAFREEN']
    """
    f = open(fname)
    l = f.readlines()
    f.close()
    tabData= []
    for i in range(0,len(l)):
        tempTab=l[i].replace("\n","").split()
        if len(l[i].split()) != 0 and tempTab[0]=="position:": 
#            print tempTab
            posiRef=tempTab[1];neighbors=tempTab[3]
            saveUn_=False
            continue
        if len(l[i].split()) == 0:
            if saveUn_ and name!=None:
                tabData.append([int(posiRef),name,chain,int(deb),int(fin),aa])
#                print "tabData:",[int(posiRef),name,chain,int(deb),int(fin),aa]
            aa="";name=None;chain=None;deb=None;fin=None
            compte=0
            saveUn_=True
            continue
        if saveUn_:
            compte+=1
            if compte==1: 
                name=tempTab[0]
                chain=tempTab[1]
                deb=tempTab[2]
            aa+=tempTab[3]
            fin=tempTab[2]
            saveUn_=True
    return tabData          

 
def inputPhmm(fname):
    """
    read a Phmm file 
    saved in a table 
    """
    f = open(fname)
    Phmm=[]
    next=True
    while next:
        l=f.readline().replace("\n","")
        if len(l)!=0: 
#                Phmm.append(l)
            Phmm.append(" ".join(l.split()[1:]))
        else : next=False
    f.close()
#    return Phmm
    length=len(Phmm) 
    return Phmm[CouperNbsite2cotes:(length-CouperNbsite2cotes)] 
          
def inputSvmi8(fname):
    """
    For R-SVM 
    read a Svmi8 file 
    saved in a table 
    """
    f = open(fname)
    Svmi8=[]
    next=True
    while next:
        l=f.readline()
        if len(l)!=0: 
            if l!="\n":
#                    Svmi8.append(l)
                l=re.compile("\s+").sub(" ", l) # pas obligatoire
                Svmi8.append(re.compile("^\s+").sub("", l))# ????
        else : next=False
    f.close()
#    return Svmi8
    length=len(Svmi8) 
    return Svmi8[CouperNbsite2cotes:(length-CouperNbsite2cotes)] 

def examin(fstFileName,seqFst,hmmFileName,seqHmm,strideFileName,seqStride,phmmFileName,seqPhmm,probFileName,seqProb,pdbFileName,seqPdb):
    if fstFileName!=None and hmmFileName!=None:
        if(len(seqFst)!=len(seqHmm)+3):
            print "FST File: "+fstFileName
            print "HMM File : "+hmmFileName
            print "Attention ! Fst[%d] length is not equal to Hmm[%d] length+3!"%(len(seqFst),len(seqHmm))
            sys.exit()
    if fstFileName!=None and strideFileName!=None:
        if(len(seqFst)!=len(seqStride)):
            print "FST File: "+fstFileName
            print "Stride File : "+strideFileName
            print "Attention ! Fst[%d] length is not equal to Stride[%d] length!"%(len(seqFst),len(seqStride))
            sys.exit()
    if fstFileName!=None and phmmFileName!=None:
        if(len(seqFst)!=len(seqPhmm)+3):
            print "FST File: "+fstFileName
            print "PHMM File : "+phmmFileName
            print "Attention ! Fst[%d] length is not equal to Phmm[%d] length+3!"%(len(seqFst),len(seqPhmm))
            sys.exit()
    if fstFileName!=None and probFileName!=None:
        if(len(seqFst)!=len(seqProb)+3):
            print "FST File: "+fstFileName
            print "PROB File : "+probFileName
            print "Attention ! Fst[%d] length is not equal to Prob[%d] length+3!"%(len(seqFst),len(seqProb))
            sys.exit()    
    if fstFileName!=None and pdbFileName!=None:
        if(len(seqFst)!=len(seqPdb)):
            print "FST File: "+fstFileName
            print "PDB File : "+pdbFileName
            print "Attention ! Pdb[%d] length is not equal to Fst[%d] length!"%(len(seqPdb),len(seqFst))
            sys.exit()       

def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file :"+where+"]"
    if where:
        f.close()

def pathExam(pathOut):
    if  (os.path.exists(pathOut)  ==  False):
        os.makedirs(pathOut)
        print "Creation:"+pathOut+"......ok!"
               
def toStringTab1(tab):
    l=""
    for i in range(len(tab)):
        l+=str(tab[i])+"\t"
    return l

def toStringTab2(tab):
    l=""
    
    for i in range(len(tab)):
        for j in range(len(tab[i])):
            l+=str(tab[i][j])+"\t"
        l+="\n"
    return l

def toStringTab11(tab):
    l=""
    for i in range(len(tab)):
        l+=str(tab[i])+"\n"
    return l

def toStringTab22(tab):
    l=""
    
    for i in range(len(tab)):
        for j in range(len(tab[i])):
            l+="%5.4f\t"%float(tab[i][j])
        l+="\n"
    return str(l)        
        
        