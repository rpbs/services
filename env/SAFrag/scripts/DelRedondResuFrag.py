#!/usr/bin/python
'''
Created on Dec, 2012

@author: shen
'''
import sys
import copy
import os
import re


class Run:

    def __init__(self,FileName):
        """
        read file *.results
        Delete redundancy for the same hits with difference length in same position.
        """

        (dictResuFrag,tabInfo,strInput,nbIn)=self.inputFile(FileName)
#        print strInput
#        print "----------------------------------------"

        self.tabInfo=self.suppRedondFragV2(dictResuFrag)
        self.tabStr=""
        for i in self.tabInfo:
            self.tabStr+="\t".join(i)+"\n"
        print "----lines[%d]----------->[Delete Redundancy]---------->lines[%d]"%(nbIn,len(self.tabInfo))


    def suppRedondFrag(self,dictResuFrag):
        tabSelect=[]
        nn=0
        for idx in dictResuFrag.keys():
            nbFrag=len(dictResuFrag[idx])
            if nbFrag >1: 
#                print dictResuFrag[idx]
                tabPosi=[]
                unTab=self.sortList(dictResuFrag[idx],-1)
                n1=0; n2=0
                for n in range(len(unTab)):
                    posiFrom=int(unTab[n][2])
                    posiTo=int(unTab[n][3])
#                    print posiFrom,"------>",posiTo
                    AjouteFrg=False
                    for k in range(posiFrom,(posiTo+1)):
                        if k not in tabPosi: AjouteFrg=True
#                        tabPosi.append(k)
                    if AjouteFrg:
                        for k in range(posiFrom,(posiTo+1)):
                            tabPosi.append(k)
                        tabSelect.append(unTab[n])
                        n1+=1
                    else: n2+=1;nn+=1
                    tabPosi=sorted(set(tabPosi))
                
                print  "Target:\t%s\t\tNb_All:\t%d\t\tAdd:\t%d\t\tDel:\t%d"%(idx,nbFrag,n1,n2)   
            else : tabSelect+=dictResuFrag[idx]
        print "del:",nn
        return tabSelect
    

    def suppRedondFragV2(self,dictResuFrag):
        """
        Version2
        si nb frag issu meme sequence superieur que 3, on verifier son redondance sur le meme position.
          1) accepter les fragments pour que tous les position coverage au mois 3 fois.
          2) accepter les fragments avec la precision>=0.98
          3) accepter les fragments avec longueur>=15   
        Dans quelconque position,le nombre maxmal redundance de meme sequence ne va pas redundant plus que 5 fois.
        
        """
        preciMin=0.98# accepte precision >0.98 
        lenMin=15 #accepte len frag >=15 
        suppMin=3 # couvrir au-moins 3fois partout
        suppMax=5 # la plus nombreux couverture parmi tous les position est 5
        
        tabSelect=[]
        nn=0
        for idx in dictResuFrag.keys():
            nbFrag=len(dictResuFrag[idx])
            if nbFrag >3:  
#                print dictResuFrag[idx]
                tabPosi=[]
                unTab=self.sortList(dictResuFrag[idx],-1)
                n1=0; n2=0
                for n in range(len(unTab)):
                    posiFrom=int(unTab[n][2])
                    posiTo=int(unTab[n][3])
                    preci=float(unTab[n][-1])
                    lenSeq=posiTo-posiFrom+1
#                    print posiFrom,"------>",posiTo
                    AjouteFrg=False
                    for k in range(posiFrom,(posiTo+1)):
                        if tabPosi.count(k)<=suppMin: AjouteFrg=True # couvrir au-moins 3fois partout
                        if preci>=preciMin:   AjouteFrg=True # precision >0.98
                        if lenSeq>=lenMin:    AjouteFrg=True #len frag >=15        
                        if tabPosi.count(k)>suppMax : AjouteFrg=False # la plus nombreux couverture parmi tous les position est 5
                    if AjouteFrg:
                        for k in range(posiFrom,(posiTo+1)):
                            if tabPosi.count(k)<=(suppMax+1): tabPosi.append(k)
                        tabSelect.append(unTab[n])
                        n1+=1
                    else: n2+=1;nn+=1
                print  "Target:\t%s\t\tNb_All:\t%d\t\tAdd:\t%d\t\tDel:\t%d"%(idx,nbFrag,n1,n2)   
            else : tabSelect+=dictResuFrag[idx]
        return tabSelect

    def sortList(self,tabIn,numCol1=-1,numCol2=-4):
        """
        ex: T05611    2xseA_1    69    92    74    97    0.131771    0.063258    27    T05611-69-92-Cl1    1    0.962
        regle: 1)Precision col=  [-1]
               2)Len       col= [-4]
        """
        for x in tabIn:
            x[numCol1]=float(x[numCol1])   #Eff Cluster
            x[numCol2]=int(x[numCol2]) #DisJS
#        print tabIn
        tabIn=sorted(tabIn,key=lambda k:(-k[numCol1],-k[numCol2])) #EJSL
        tabIn= [map(str,x) for x in tabIn]
#        print tabIn
        return tabIn

    def inputFile(self,fname,col_id2=2):
        """
        read un file plusieurs colonne sep=" "
        
        T05151    2yxxA     11    17     19    25    0.014191    0.517723    10    T05151-11-17-Cl1      495
        T05151    3bjdC1   101    107    35    41    0.046399    1.191462    10    T05151-101-107-Cl1    485
        T05151    3ck2A    100    106    91    97    0.049969    1.506331    10    T05151-100-106-Cl1    479

        """
        nb=0
        f = open(fname)
        dictResuFrag={}
        tabInfo=[]
        strInput=""
        print "[Load File:%s]"%fname
        l = f.readlines()
        f.close()
        for i in range(0,len(l)):
            tempTab=[]
            if len(l)!=0:
                if l[i]!="\n" and l[i][0]!="#" and l[i].split()[0]!="file1":
                    strInput+=l[i]
                    nb+=1
                    tempTab=l[i].replace("\n","").split()
                    id2=tempTab[col_id2-1]
                    if dictResuFrag.has_key(id2):
#                        print dictResuFrag[id2]
                        dictResuFrag[id2].append(tempTab)
                    else :
                        dictResuFrag[id2]=[tempTab]
                        tabInfo.append(id2)
            else :
                print "Errors empty line No:",i
                sys.exit();
        return dictResuFrag,tabInfo,strInput,nb

def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file : "+where+"]"
    if where:
        f.close()

if __name__ == "__main__":

    if len(sys.argv) == 3:
        FileName=sys.argv[1]
        outFile=sys.argv[2]
    else:
        print "Delete redundancy for the same hits with difference length in same position."
        sys.exit("Usage:"+sys.argv[0]+"[fileIn] [Output]")
        
    x=Run(FileName)
    outPut(x.tabStr,outFile)
    sys.exit()



#    FileName="/home/shen/workspace/INSERM/PPresearch/T05611.filt.bestCl.SAFrag"
#    outFile="/home/shen/aas"
#    x=Run(FileName)
#    print x.tabStr
#    outPut(x.tabStr,outFile)


