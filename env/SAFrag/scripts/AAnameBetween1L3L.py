#!/usr/bin/env python

import sys
import string

if len(sys.argv) != 2:
    print "Attention: Traduit aussi les FGL,BHD,HTR,MSE ... en Lettres S,X,W,M ..."
    sys.exit("usage: %s [Name_AA: AA3/AA1 ex:HIS/H]"%sys.argv[0])

inputAA=sys.argv[1]

AA1 = "ACDEFGHIKLMNPQRSTVWY"
AA3 = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR","5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG"]
AA1seq = "ACDEFGHIKLMNPQRSTVWYXXXSXWMCXWYMDPECXXYAMMSCMA"
AA3STRICT = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"]

def resType(aName):
    if aName == "":
	        return "Error"
    if string.count(AA3, aName) > 0:
        return string.index(AA3, aName)
    else:
        return "X"

def aa3Type(aName):
    if aName == "":
        return "Error"
    if string.count(AA3, aName) > 0 :
        return AA1seq[string.index(AA3, aName)]
    else:
        return "X"

def aa1Type(aName):
    if aName == "":
        return "Error"
    if string.count(AA1, aName) > 0:
       	return AA3[string.index(AA1, aName)]
    else:
        return "X"


if len(inputAA)==3 : print aa3Type(inputAA)
elif len(inputAA)==1:  print aa1Type(inputAA) 
else : print "Err AA Name: AA3/AA1 ex: HIS or H"
