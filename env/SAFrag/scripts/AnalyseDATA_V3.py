#!/usr/local/python2.6/bin/python
'''
Created on June, 2011

@author: shen
'''
import sys
import copy
import os
import re
import gc
from collections import defaultdict
from operator import itemgetter#, attrgetter
import MethodeBasic

#import logging

# newSVM model 34 target CASP8 [mieux que oldSVM model]   
#lenFrag SeuilJS RMSDprevu

#dflt 1er version,environ 4-8% err
seuilJStab_newSVMmodel=[[ 3, 0.05 , 1.1 ],
            [ 4, 0.06 , 1.1 ],
            [ 5, 0.075, 1.2 ],
            [ 6, 0.09 , 1.25],
            [ 7, 0.1  , 1.2 ],
            [ 8, 0.11 , 1.3 ],
            [ 9, 0.115, 1.25],
            [10, 0.125, 1.3 ],
            [11, 0.13 , 1.5 ],
            [12, 0.15 , 2   ],
            [13, 0.155, 2.1 ],
            [14, 0.17 , 2.3 ],
            [15, 0.18 , 2.3 ],
            [16, 0.21 , 3   ],
            [17, 0.21 , 3.1 ],
            [18, 0.215, 3.1 ],
            [19, 0.21 , 3.5 ],
            [20, 0.215, 4   ],
            [21, 0.2  , 2.6 ],
            [22, 0.186, 2.75],
            [23, 0.19 , 3.3 ],
            [24, 0.19 , 3.4 ]]
#3%err clAll
seuilJStab_newSVMmodel_3PCErr=[[ 3, 0.0488, 1.1],
            [ 4, 0.0643, 1.1],
            [ 5, 0.0776, 1.2],
            [ 6, 0.0819, 1.25],
            [ 7, 0.0845, 1.2],
            [ 8, 0.0944, 1.3],
            [ 9, 0.1146, 1.25],
            [10, 0.1203, 1.3],
            [11, 0.1230, 1.5],
            [12, 0.1278, 2  ],
            [13, 0.1371, 2.1],
            [14, 0.1458, 2.3],
            [15, 0.1487, 2.3],
            [16, 0.1521, 3  ],
            [17, 0.1474, 2.7],
            [18, 0.1535, 2.8],
            [19, 0.1516, 2.5],
            [20, 0.1516, 2.6],
            [21, 0.1462, 2.5],
            [22, 0.1438, 2.5],
            [23, 0.1275, 2.5],
            [24, 0.1228, 2.7]]

#4%err clAll
seuilJStab_newSVMmodel_4PCErr=[[ 3, 0.0617, 1.1],
            [ 4, 0.0791, 1.1],
            [ 5, 0.0955, 1.2],
            [ 6, 0.1041, 1.25],
            [ 7, 0.1090, 1.2],
            [ 8, 0.1197, 1.3],
            [ 9, 0.1362, 1.25],
            [10, 0.1423, 1.3],
            [11, 0.1470, 1.5],
            [12, 0.1507, 2  ],
            [13, 0.1581, 2.1],
            [14, 0.1645, 2.3],
            [15, 0.1668, 2.3],
            [16, 0.1702, 3  ],
            [17, 0.1668, 2.7],
            [18, 0.1703, 2.8],
            [19, 0.1691, 2.5],
            [20, 0.1680, 2.6],
            [21, 0.1627, 2.5],
            [22, 0.1592, 2.5],
            [23, 0.1533, 2.5],
            [24, 0.1452, 2.7]]

#5%err clAll
seuilJStab_newSVMmodel_5PCErr=[[ 3, 0.0706, 1.1],
            [ 4, 0.0895, 1.1],
            [ 5, 0.105 , 1.2],
            [ 6, 0.1171, 1.25],
            [ 7, 0.1246, 1.2],
            [ 8, 0.1355, 1.3],
            [ 9, 0.15  , 1.25],
            [10, 0.1573, 1.3],
            [11, 0.1626, 1.5],
            [12, 0.1668, 2  ],
            [13, 0.1726, 2.1],
            [14, 0.1780, 2.3],
            [15, 0.1799, 2.3],
            [16, 0.1837, 3  ],
            [17, 0.1804, 2.7],
            [18, 0.1843, 2.8],
            [19, 0.1835, 2.5],
            [20, 0.1830, 2.6],
            [21, 0.1779, 2.5],
            [22, 0.1757, 2.5],
            [23, 0.1713, 2.5],
            [24, 0.1622, 2.7]]
                    

#3-8%err clAll  yimin
seuilJStab_newSVMmodel_autoPCErr=[[ 3, 0.04108, 1.1],
            [ 4, 0.05825, 1.1],
            [ 5, 0.0738, 1.2],
            [ 6, 0.08845, 1.25],
            [ 7, 0.1022, 1.2],
            [ 8, 0.11505, 1.3],
            [ 9, 0.127, 1.25],
            [10, 0.13805, 1.3],
            [11, 0.1482, 1.5],
            [12, 0.15745, 2  ],
            [13, 0.1658, 2.1],
            [14, 0.17325, 2.3],
            [15, 0.1798, 2.3],
            [16, 0.18545, 3  ],
            [17, 0.1902, 2.7],
            [18, 0.19405, 2.8],
            [19, 0.197, 2.5],
            [20, 0.19905, 2.6],
            [21, 0.2002, 2.5],
            [22, 0.20045, 2.5],
            [23, 0.1998, 2.5],
            [24, .19825, 2.7]]

#8%err clAll
seuilJStab_newSVMmodel_8PCErr=[
            [ 3, 0.0883, 1.1],
            [ 4, 0.1046, 1.1],
            [ 5, 0.1285, 1.2],
            [ 6, 0.1416, 1.25],
            [ 7, 0.1507, 1.2],
            [ 8, 0.1634, 1.3],
            [ 9, 0.1772, 1.25],
            [10, 0.1845, 1.3],
            [11, 0.19  , 1.5],
            [12, 0.1936, 2  ],
            [13, 0.1986, 2.1],
            [14, 0.2029, 2.3],
            [15, 0.207 , 2.3],
            [16, 0.2102, 3  ],
            [17, 0.2075, 2.7],
            [18, 0.2104, 2.8],
            [19, 0.2096, 2.5],
            [20, 0.2088, 2.6],
            [21, 0.2052, 2.5],
            [22, 0.2043, 2.5],
            [23, 0.2028, 2.5],
            [24, 0.1982, 2.7]]
            
            
#10%err clAll
seuilJStab_newSVMmodel_10PCErr=[[ 3, 0.0941, 1.1],
            [ 4, 0.1142, 1.1],
            [ 5, 0.1391, 1.2],
            [ 6, 0.1531, 1.25],
            [ 7, 0.1636, 1.2],
            [ 8, 0.1767, 1.3],
            [ 9, 0.1902, 1.25],
            [10, 0.1981, 1.3],
            [11, 0.2039, 1.5],
            [12, 0.2086, 2  ],
            [13, 0.2142, 2.1],
            [14, 0.2179, 2.3],
            [15, 0.2184, 2.3],
            [16, 0.2232, 3  ],
            [17, 0.2204, 2.7],
            [18, 0.2231, 2.8],
            [19, 0.222 , 2.5],
            [20, 0.2209, 2.6],
            [21, 0.2172, 2.5],
            [22, 0.2161, 2.5],
            [23, 0.2138, 2.5],
            [24, 0.208, 2.7]]


seuilCRMSDtab=[[ 0, 0.4, 1.1],#lenAA=3
            [ 1, 0.55, 1.1],#lenAA=4
            [ 2, 0.75, 0.6],#lenAA=5
            [ 3, 1   , 1.1],
            [ 4, 1.2 , 1.1],
            [ 5, 1.4 , 1.2],
            [ 6, 1.5 , 1.25],
            [ 7, 1.6 , 1.1],
            [ 8, 1.75, 1.3],
            [ 9, 1.9 , 0.9],
            [10, 2.0 , 1.1],            
            [11, 2.1 , 1.7],
            [12, 2.2 , 1.95],
            [13, 2.35, 2],
            [14, 2.5 , 2.2],
            [15, 2.6 , 2.2],
            [16, 2.75, 2.5],
            [17, 2.8 , 2.5],
            [18, 3.0 , 2.9],
            [19, 3.1 , 3.4],
            [20, 3.2 , 3.5],
            [21, 3.25, 2.6],
            [22, 3.35, 2.75],
            [23, 3.45, 3.3],
            [24, 3.5 , 3.4]]



#dflt ne plus utiliser (obtenu par CASP8)
seuilConfienceEffTab_old=[[ 3,  19,  40],
                [ 4, 430],#
                [ 5, 326, 539],
                [ 6, 497],
                [ 7,1347],
                [ 8, 818],
                [ 9,3518],
                [10,1168],            
                [11,1217],
                [12, 128],
                [13, 128],
                [14, 138],
                [15, 117],
                [16,   8],
                [17,   8,  72],
                [18,   8,  71],
                [19,   8],
                [20,   4],
                [21,   6],
                [22,   4, 292],
                [23,   4, 251],
                [24,   3]]

#Max500Hit bestCRMSD auto
seuilConfienceEffTab_5PCErr_bestCRMSD=[[ 3, 79], 
                [ 4, 454],
                [ 5, 462],
                [ 6, 472],
                [ 7, 489],
                [ 8, 488],
                [ 9, 482],
                [10, 484],   
                [11, 480], 
                [12, 482], 
                [13, 482], 
                [14, 478],
                [15, 476],
                [16, 484],
                [17, 490],
                [18, 491], 
                [19, 495],
                [20, 496],
                [21, 495],
                [22, 493],
                [23, 449],
                [24, 281]] 
# > 95% precision
seuilConfienceEffTab_5PCErr_95pc=[[ 3, 271], 
                [ 4, 200],
                [ 5, 171],
                [ 6, 180],
                [ 7, 157],
                [ 8, 98],
                [ 9, 106],
                [10, 130],   
                [11, 130], 
                [12, 133], 
                [13, 82], 
                [14, 73],
                [15, 77],
                [16, 34],
                [17, 24],
                [18, 17], 
                [19, 9],
                [20, 7],
                [21, 7],
                [22, 5],
                [23, 4],
                [24, 4]] 

# > 97% precision
seuilConfienceEffTab_5PCErr_97pc=[[ 3, 462], 
                [ 4, 460],
                [ 5, 445],
                [ 6, 453],
                [ 7, 449],
                [ 8, 441],
                [ 9, 456],
                [10, 461],   
                [11, 460], 
                [12, 456], 
                [13, 461], 
                [14, 468],
                [15, 443],
                [16, 79],
                [17, 323],
                [18, 93], 
                [19, 65],
                [20, 12],
                [21, 9],
                [22, 8],
                [23, 7],
                [24, 6]] 

# > 98% precision 
seuilConfienceEffTab_5PCErr_98pc=[[ 3, 477], 
                [ 4, 477],
                [ 5, 480],
                [ 6, 482],
                [ 7, 479],
                [ 8, 474],
                [ 9, 473],
                [10, 475],   
                [11, 473], 
                [12, 475], 
                [13, 477], 
                [14, 476],
                [15, 473],
                [16, 442],
                [17, 363],
                [18, 344], 
                [19, 122],
                [20, 36],
                [21, 27],
                [22, 30],
                [23, 8],
                [24, 6]] 

seuilConfienceEffTab_5PCErr=seuilConfienceEffTab_5PCErr_98pc
seuilConfienceEffTab_5PCErr2=seuilConfienceEffTab_5PCErr_bestCRMSD
# > 99% precision  (ne marche pas bien)
seuilConfienceEffTab_5PCErr_99pc=[[ 3, 495], 
                [ 4, 496],
                [ 5, 498],
                [ 6, 499],
                [ 7, 498],
                [ 8, 496],
                [ 9, 493],
                [10, 492],   
                [11, 489], 
                [12, 487], 
                [13, 490], 
                [14, 489],
                [15, 497],
                [16, 471],
                [17, 467],
                [18, 432], 
                [19, 255],
                [20, 197],
                [21, 292],
                [22, 444],
                [23, 288],
                [24, 142]] 

seuilConfienceEffTab_3PCErr=seuilConfienceEffTab_5PCErr
seuilConfienceEffTab_4PCErr=seuilConfienceEffTab_5PCErr
seuilConfienceEffTab_8PCErr=seuilConfienceEffTab_5PCErr
seuilConfienceEffTab_10PCErr=seuilConfienceEffTab_5PCErr
seuilConfienceEffTab_autoPCErr=seuilConfienceEffTab_5PCErr

class Analyse:
    
    def __init__(self,FileName,seuilScore,seuilCRMSD,seuilScore_NbPercentFrag=None,classementFileName=None,rapide=False,AjouterFragLength=True,seuilDisJS="old",NumColPoids=None,HHfrag=False,remplacerDisJSparMeanJS=False,allFragAjouter=True,SeuilConfianceEff=2,seuilPreciConserver=0.99,seuilPreciEliminer=0.9,NumColPreci=0,CovNfois=4):
        """
        read file *.results.final
        calculer pourcentage couvert
        """
        self.Aff=True
        self.AjouterFragLength=AjouterFragLength
        self.AffText="***\n"
        self.FileName=FileName
        self.NumColallDisJS=9 #numero de colone "0.004737-0.005330-0.005482..." dans le fichier
        
        self.HHfrag=HHfrag#Dflt True pour analyser HHfrag
        self.rapide=rapide#si Rapide, ne calcule pas coverage 
        if self.HHfrag:seuilScore=None;seuilScore_NbPercentFrag=None #HHfrag ne contient pas Distance JS
        
        # only add the MJS < xxx fragments
        if seuilScore==None or seuilScore=="Auto": self.seuilScore=seuilScore
        else :self.seuilScore=float(seuilScore) 
        
        # only add the correct (or RMSD< xx) fragments 
        if seuilCRMSD==None or seuilCRMSD=="Auto": self.seuilCRMSD=seuilCRMSD 
        else : self.seuilCRMSD=float(seuilCRMSD)
        
        # add all the strong precision Fragments
        if seuilPreciConserver!=None:self.seuilPreciConserver=float(seuilPreciConserver)
        else: self.seuilPreciConserver=None
        
        # del all the weak precision Fragments
        if seuilPreciEliminer!=None: self.seuilPreciEliminer=float(seuilPreciEliminer)
        else : self.seuilPreciEliminer=None
        
        #at least, have N time coverage.
        if CovNfois!=None: self.CovNfois=int(CovNfois)
        else : self.CovNfois=None 
        
        #Pour les Eff<SeuilConfianceEff, supprimer les redondance des fragments
        if SeuilConfianceEff==None or SeuilConfianceEff=="Auto": self.SeuilConfianceEff=SeuilConfianceEff 
        else : self.SeuilConfianceEff=int(SeuilConfianceEff)
        
        if NumColPoids!=None : self.NumColPoids=int(NumColPoids)
        else :self.NumColPoids=None
        
        if NumColPreci!=None : self.NumColPreci=int(NumColPreci)
        else :self.NumColPreci=None
        
#        plotWithNumColPoids=self.NumColPoids #if exist "NumColPoids!=None", plot compte les effectif des fragment 
        plotWithNumColPoids=None# plot et precision ne calculer pas les effet des Fragment
        
        if SeuilConfianceEff!=None and (NumColPoids==None or allFragAjouter==True):print "If exist SeuilConfianceEff(supprimer le redondance sur les fragments,si SonEff<EeuilConfianceEff), need numColPoids is exist, et allFragAjouter=No.";sys.exit()
        if (seuilPreciConserver!=None or seuilPreciEliminer!=None) and (NumColPreci==None or allFragAjouter==True):print "If exist seuil GoodPreciConserver or BadPreciEliminer, need NumColPreci is exist, et allFragAjouter=No.";sys.exit()
        
        if seuilScore_NbPercentFrag==None:self.seuilScore_NbPercentFrag=seuilScore_NbPercentFrag
        else : self.seuilScore_NbPercentFrag=float(seuilScore_NbPercentFrag)
        
        if   seuilDisJS=="old" or seuilDisJS=="new": 
            self.seuilJStab=seuilJStab_newSVMmodel
            self.seuilConfienceEffTab=seuilConfienceEffTab_old
        elif seuilDisJS=="3PC": 
            self.seuilJStab=seuilJStab_newSVMmodel_3PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_3PCErr
        elif seuilDisJS=="4PC": 
            self.seuilJStab=seuilJStab_newSVMmodel_4PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_4PCErr
        elif seuilDisJS=="5PC": 
            self.seuilJStab=seuilJStab_newSVMmodel_5PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_5PCErr
        elif seuilDisJS=="5PC2":
            self.seuilJStab=seuilJStab_newSVMmodel_5PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_5PCErr2
        elif seuilDisJS=="8PC": 
            self.seuilJStab=seuilJStab_newSVMmodel_8PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_8PCErr
        elif seuilDisJS=="10PC":
            self.seuilJStab=seuilJStab_newSVMmodel_10PCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_10PCErr
        elif seuilDisJS=="autoPC":
            self.seuilJStab=seuilJStab_newSVMmodel_autoPCErr
            self.seuilConfienceEffTab=seuilConfienceEffTab_autoPCErr
        else : sys.exit("Err sur seuilDisJS, please used old/3PC/4PC/5PC/8PC/10PC/autoPC")
        self.seuilDisJS=seuilDisJS
        self.seuilCRMSDtab=seuilCRMSDtab
        self.classementFileName=classementFileName
        self.remplacerDisJSparMeanJS=remplacerDisJSparMeanJS
        
        self.allFragAjouter=allFragAjouter # ajouter tous les fragement/ False: n'ajouter que les fragment qui peut augmenter le covrage. (il faut commencer par les fragment plus probable)  
        if self.allFragAjouter==False and (NumColPoids==None and NumColPreci==None):print "If ajouter certaine fragment plus possible, need numColPoids or NumColPreci is not NA.";sys.exit()
        #length aa seq  'T0596b3':43 'T06382':50 'T05963':44, 
        self.dictFileLength={'T0388': 174, 'T0389': 134, 'T0507': 123, 'T0483': 285, 'T0482': 120, 'T0471': 133, 'T0473': 68, 'T0474': 80, 'T0475': 121, 'T0498': 56, 'T0499': 56, 'T0478': 264, 'T0494': 347, 'T0492': 73,
 'T0490': 366, 'T0458': 79, 'T0514': 144, 'T0437': 99, 'T0432': 130, 'T0455': 139, 'T0392': 104, 'T0395': 304, 'T0488': 95, 'T0466': 108, 'T0464': 89, 'T0462': 154, 'T0460': 111, 'T0484': 62, 'T0486': 256, 'T0469': 65, 'T0468': 109, 'T0496': 175, 'T0504': 208, 'T0428': 229, 'T0502': 98,'T05151':123, 'T05152':113, 'T05153':112, 'T0516':227, 'T0517':159, 'T0518':256, 'T0520':173, 'T05211':104, 'T05212':47,
 'T05213':17, 'T0521a1':34, 'T0521a2':47, 'T0521a3':17, 'T0521b':70, 'T0522':131, 'T0523':111, 'T05241':116, 'T05242':205, 'T0525':205, 'T0526':290, 'T05271':35, 'T05272':64, 'T0528':371, 'T0528a1':121,
 'T0528a2':83, 'T0528b1':130, 'T0528b2':30, 'T05291':140, 'T05292':182, 'T05293':154, 'T05294':24, 'T05295':15, 'T0529a1':140, 'T0529a2':182, 'T0529b1':154, 'T0529b2':24, 'T0529b3':15, 'T0530':80, 'T0531':65,
 'T05321':208, 'T05322':262, 'T0534':354, 'T0534a1':50, 'T0534a2':128, 'T0534b':176, 'T05371':7, 'T05372':332, 'T0537a':286, 'T0537b':31, 'T0538':53, 'T0539':68, 'T0540':90, 'T05411':17, 'T05412':53,
 'T05413':32, 'T05421':491, 'T05422':77, 'T0542a':301, 'T0542b1':188, 'T0542b2':77, 'T05431':342, 'T05432':56, 'T05433':102, 'T05434':15, 'T05435':268, 'T0543a':40, 'T0543b':45, 'T0543c1':257, 'T0543c2':56,
 'T0543c3':73, 'T0543d1':29, 'T0543d2':15, 'T0543d3':268, 'T0544':135, 'T05451':31, 'T05452':41, 'T05453':49, 'T05454':10, 'T05471':189, 'T05472':219, 'T05473':175, 'T0547a1':51, 'T0547a2':13, 'T0547a3':119,
 'T0547b1':138, 'T0547b2':127, 'T0547c':79, 'T0547d':56, 'T0548':95, 'T0548a':35, 'T0548b':60, 'T05501':130, 'T05502':175, 'T0550a1':130, 'T0550a2':13, 'T0550b':162, 'T0551':63, 'T05521':7, 'T05522':13,
 'T05523':7, 'T05524':12, 'T05525':17, 'T05526':9, 'T05527':8, 'T0553':134, 'T0553a':63, 'T0553b':71, 'T0555':134, 'T05571':13, 'T05572':107, 'T0558':272, 'T0559':67, 'T0560':64, 'T05611':109, 'T05612':17,
 'T05613':25, 'T0562':123, 'T05631':60, 'T05633':71, 'T05634':92, 'T05641':14, 'T05642':47, 'T05651':181, 'T05652':123, 'T0566':143, 'T0567':135, 'T05681':18, 'T05682':102, 'T0569':78, 'T0570':233,
 'T0571':315, 'T0571a':165, 'T0571b':135, 'T0572':86, 'T05731':78, 'T05732':79, 'T05733':63, 'T05734':34, 'T0574':102, 'T0575':216, 'T0575a':63, 'T0575b1':11, 'T0575b2':95, 'T0575b3':21, 'T0576':133,
 'T05781':48, 'T05782':100, 'T0579':124, 'T0579a1':29, 'T0579a2':31, 'T0579b':64, 'T0580':104, 'T0581':105, 'T0582a':221, 'T0582b':99, 'T0584':338, 'T05851':125, 'T05852':87, 'T0586':119, 'T0586a':80,
 'T0586b':39, 'T0588':381, 'T05891':57, 'T05892':10, 'T05893':40, 'T05894':313, 'T05895':11, 'T0589a1':42, 'T0589a2':38, 'T0589a3':49, 'T0589a4':99, 'T0589b':82, 'T0589c1':83, 'T0589c2':11, 'T05901':43,
 'T05902':29, 'T0591':380, 'T0592':137, 'T0593':195, 'T0594':140, 'T05961':106, 'T05962':24, 'T05963':44, 'T0596a':53, 'T0596b1':53, 'T0596b2':24, 'T0596b3':44, 'T05971':165, 'T05972':61, 'T05973':92,
 'T05974':41, 'T05981':77, 'T05982':37, 'T05983':13, 'T05991':90, 'T05992':70, 'T05993':207, 'T0601':442, 'T0602':55, 'T06031':150, 'T06032':110, 'T06041':46, 'T06042':488, 'T0604a1':46, 'T0604a2':34,
 'T0604b1':197, 'T0604b2':52, 'T0604c':205, 'T0605':49, 'T0606':120, 'T0607':469, 'T0608':250, 'T0608a':89, 'T0608b':161, 'T0609':335, 'T0610':176, 'T06111':167, 'T06112':36, 'T0611a':53, 'T0611b1':114,
 'T0611b2':35, 'T06131':126, 'T06132':149, 'T06141':11, 'T06142':12, 'T06143':16, 'T06144':6, 'T06145':26, 'T0615':174, 'T0616':97, 'T0617':136, 'T06181':90, 'T06182':68, 'T0619':101, 'T06201':111, 'T06202':43,
 'T06203':78, 'T06204':49, 'T0621':169, 'T0622':122, 'T0623':187, 'T0624':69, 'T0625':221, 'T0626':282, 'T0627':244, 'T0628':295, 'T0628a1':127, 'T0628a2':17, 'T0628b':146, 'T0629':216, 'T0629a1':49,
 'T0629a2':8, 'T0629b':159, 'T06301':32, 'T06302':67, 'T06321':14, 'T06322':100, 'T06341':54, 'T06342':21, 'T06343':32, 'T0635':161, 'T0636':318, 'T06381':51, 'T06382':52, 'T06383':13, 'T06384':20, 'T06385':76,
 'T0639':124, 'T06401':137, 'T06402':40, 'T06403':30, 'T0641':295, 'T0643':73 }
        
        if self.seuilScore_NbPercentFrag and self.seuilScore : sys.exit("Err: chose one of them (seuilScore_NbPercentFrag and seuilCRMSD)")
        #------------Input file -------------
        (self.tabInfo,self.tabScore,self.tabLengthFrg)=self.inputFile(FileName)  #input Score et CRMSD infos
#        print "DictFile1_Len:",self.dictFileLength,"\nTabFragLength:",self.tabLengthFrg
        self.nbData=len(self.tabInfo)
        
        #------------Input Classement file--------------------------
        if classementFileName: 
            (self.dictClass,self.dictClass2,self.dictClass3)=self.inputClassementFile(classementFileName)
            (self.tabInfo,self.outputSourceText)=self.ajouterClass(self.tabInfo,self.dictClass,self.dictClass2,self.dictClass3)
        # [OUTPUT]: self.outputSourceText: tous les data avec class
#        sys.exit()
        #---------- Analyse scoreJS Threshold ----------
        if (not rapide) or (self.seuilScore_NbPercentFrag!=None and self.seuilScore==None):
            if not self.HHfrag: (self.nb5pc,self.nb10pc,self.nb20pc,self.nb25pc,self.seuil_5Pc,self.seuil_10Pc,self.seuil_20Pc,self.seuil_25Pc,self.seuil_CalcWithPcFrag)=self.seuilScoreAnalyse(self.tabScore,seuilScore_NbPercentFrag)
        if self.seuilScore_NbPercentFrag!=None and self.seuilScore==None:self.seuilScore=self.seuil_CalcWithPcFrag
        
        #----------- Plot ROC ---------------
#        if self.seuilCRMSD!=None and (self.seuilScore=="Auto" or self.seuilScore!=None or self.seuilScore_NbPercentFrag!=None):
        if self.seuilScore=="Auto" or self.seuilScore==None or (self.seuilScore!=None or self.seuilScore_NbPercentFrag!=None):

            #if not self.HHfrag:
            (self.rocTP,self.rocTN,self.rocFP,self.rocFN,self.sensitivity,self.specificity)=self.plotROC(self.tabInfo,self.seuilScore,self.seuilCRMSD,plotWithNumColPoids)
        
        #-----------Filter & Analyse % Couverture----------------
        file1name=None;lengthFrg=None
        (self.tabFilte,self.dictFile1Matrix,self.tabFilteStr)=self.filte(self.tabInfo,self.seuilScore,self.seuilCRMSD,file1name,lengthFrg,self.seuilPreciConserver,self.seuilPreciEliminer,self.NumColPreci,self.NumColPoids,self.remplacerDisJSparMeanJS,self.NumColallDisJS,self.allFragAjouter,self.SeuilConfianceEff,self.CovNfois) # possible de filtre certains length de frag
        #print self.dictFile1Matrix
        if os.path.isfile(FileName+".thresholdPreciReject.err"):os.remove(FileName+".thresholdPreciReject.err") 
        if len(self.tabFilte)==0:
            self.report="Result after filter is empty(We can't find the fragments with the precision >%5.3f). So you cans :\n"%self.seuilPreciEliminer
            self.report+="\t 1. Restart with decrease threshold precision reject[< %5.3f] with add option (--thresholdPreciReject)\n"%self.seuilPreciEliminer
            self.report+="\t 2. Use the result of the file bestCl [*.filt.bestCl.preci]\n"
            self.report+="\t 3. If your sequence is a part of fragment, and you have the complete sequence, restart with option (--iSeqComplet)\n"
            print self.report
            outPut(self.report,FileName+".thresholdPreciReject.err")
            sys.exit("We can't find the fragments with the precision >%5.3f "%self.seuilPreciEliminer)
        
        if (not rapide):
            [self.tabCover,infoCover]=compteCover(self.dictFile1Matrix,self.Aff)
            self.AffText+=infoCover

#        print "---------------------------------------------------------------------------[End]"
        if self.allFragAjouter==False: 
            self.plotROC(self.tabFilte,self.seuilScore,self.seuilCRMSD,plotWithNumColPoids)
      
    def inputFile(self,fname):
        """
        read a *.results.final
        'file1', 'file2', 'length', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS', 'cRMSd_f', 'TMScore', 'cRMSd', 'GDT_TS', 'len_fstFile1'
        'file1', 'file2', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS','RMSd', 'DisJSperSite'
        """
        f = open(fname)
        print "[Load ScoreFile:%s]"%fname
        self.AffText+="[Load ScoreFile:%s]\n"%fname
        l = f.readlines()
        f.close()
        if len(l)==0: print "Warning: [%s] is empty!"%fname;sys.exit("Warning: AnalyseDATA_V3.py [Input: %s] is empty!"%fname)
        tabInfo = []
        tabScore=[]
        tabLengthFrg=[]
        for i in xrange(0,len(l)):
            tempTab=[]
            if len(l)!=0:   
                if l[i]!="\n" and l[i][0]!="#":
                    tempTab=l[i].replace("\n","").split()
                    tabInfo.append(tempTab)
                    if not self.HHfrag : tabScore.append(float(tempTab[6])) # ajouter les score
                    file1name=tempTab[0].replace("../","").split('.')[0] # Create dictionary {name_file1, length_file1}
                    if not self.dictFileLength.has_key(file1name):
                        print "Can't find id_name[%s] in length_Dict, will read prob file"%file1name
                        if os.path.isfile("%s.svmi8.27.prob"%file1name): 
                            tabProb=MethodeBasic.inputProb("%s.svmi8.27.prob"%file1name)
                            self.dictFileLength[file1name]=len(tabProb)+3
                            print "Add length: %s - %d"%(file1name,self.dictFileLength[file1name]) 
                        else:
                            self.dictFileLength[file1name]=1000
                            print "Attention add ErrLen: %s - %d"%(file1name,self.dictFileLength[file1name])
#                        sys.exit("Err: Can't find id_name[%s] in length_Dict[AnalyseDATA_V2.dictFileLength]"%(file1name))
#                    else:    
#                        print file1name," len:",self.dictFileLength[file1name]
                    lengthFrag= (int(tempTab[3])-int(tempTab[2])+1)
                    tabLengthFrg.append(lengthFrag) # length frag hmm
#                print tempTab
            else :
                print "Errors: %s is empty"%fname;
                sys.exit();
        tabScore=sorted(tabScore)
        tabLengthFrg=sorted(set(tabLengthFrg))
        return tabInfo,tabScore,tabLengthFrg
    
    def sortList(self,tabIn,numCol1,numCol2,numCol3,numCol4):
        """
        ex : T06321    1vh5A    7    11    23    27    0.050758[6]    0.991378    0.050758-0.011727-0.030073-0.009287-0.010009    5[9]    T06321-7-11-Cl1   3[11]
        regle: 1)Effectif Cluster  n...1  col=12[11]
               2) DistJS 0...1            col=7 [6]
               3) Length Fragment 27...6  col=10[9]
        """
        for x in tabIn:
            if float(x[numCol1])>1 or float(x[numCol4])>1 or float(x[numCol2])<3 or float(x[numCol3])<1:
                print "ErrType: Preci[%4.3f]>1 or MJS[%4.3f]>1 or len[%2.2f]<3 or PoidClust[%2.2f]<1"%(float(x[numCol1]),float(x[numCol4]),float(x[numCol2]),float(x[numCol3]))
                print "Modify: ColPreci=%d or ColMJS=%d or ColLen=%d or ColPoidCl=%d"%((numCol1+1),(numCol4+1),(numCol2+1),(numCol3+1))
                sys.exit("ErrType: Preci[%4.3f]>1 or MJS[%4.3f]>1 or len[%2.2f]<3 or PoidClust[%2.2f]<1"%(float(x[numCol1]),float(x[numCol4]),float(x[numCol2]),float(x[numCol3])))
            x[numCol1]=float(x[numCol1])   #preci
            x[numCol2]=int(x[numCol2])     #len
            x[numCol3]=int(x[numCol3])     #PoidCluster
            x[numCol4]=float(x[numCol4])   #MJS
            
#            x[numCol3]=float(x[numCol3]) #MJS    
#            x[numCol4]=int(x[numCol4])   #PoidCluster

#        print tabIn
        tabIn=sorted(tabIn,key=lambda k:(-k[numCol1],-k[numCol2],-k[numCol3],k[numCol4])) #EJSL
#       tabIn=sorted(tabIn,key=itemgetter(numCol1,numCol2,numCol3)) # ordre unique (descendant croissant)
        tabIn= [map(str,x) for x in tabIn]
#        print tabIn
        return tabIn 
    
    def inputClassementFile(self,fname):
        """
            ex : /home/freetown/shen/Data/astral-scopdom-seqres-gd-all-1.75.fa.scopIDs
            
            #id       class
            d1dlwa_   a.1.1.1
            d1uvya_   a.1.1.1
            d1dlya_   a.1.1.1
            ...       ...
        """
        f = open(fname)
        print "[Load ClassFile:%s]"%fname
        self.AffText+="[Load ClassFile:%s]\n"%fname
        l = f.readlines()
        f.close()
        tabClass = []
        tabClass2 = [] # pour certaine nom dehor tabClass
        tabClass3 = [] # pour certaine nom dehor tabClass et tabClass2
        for i in xrange(0,len(l)):
            tempTab=[];tempTab2=[];tempTab3=[]
            if len(l)!=0:   
                if l[i]!="\n" and l[i][0]!="#":
                    tempTab=l[i].replace("\n","").split()
                    tempTab2=l[i].replace("\n","").split()
                    tempTab3=l[i].replace("\n","").split()
                    tabClass.append(tempTab)     # ex:"d1tm6a_"
                    tempTab2[0]=tempTab2[0][0:6] # ex:"d1tm6a_"  cut key to "ditm6a"
                    tabClass2.append(tempTab2)
                    tempTab3[0]=tempTab3[0][0:5] # ex:"d1tm6a_"  cut key to "ditm6"
                    tabClass3.append(tempTab3)
            else :
                print "Errors";
                sys.exit();
        return dict(tabClass),dict(tabClass2),dict(tabClass3)

    def ajouterClass(self,tabInfo,dictClass,dictClass2,dictClass3):
        """
        Ajouter les class vers la fin de Data.
        InType:
        # 'file1    file2    file1_from    file1_to    file2_from    file2_to    distJS    RMSd    DisJSperSite'
        outType:
        # file1 file2  file1_from    file1_to    file2_from    file2_to    distJS    cRMSd  DisJSperSite  file2_class1    file2_class1-2    file2_class

        """
        out="file1\tfile1_len\tfile2\tHMM_length\tfile1_from\tfile1_to\tfile2_from\tfile2_to\tdistJS\tcRMSd\tclass1\tclass1-2\tclass\n"
        for i in xrange(len(tabInfo)):
            file2=tabInfo[i][1].split("/")[-1].replace(".hmmpdb",""); 
            if dictClass.has_key(file2) : className=dictClass[file2]
            elif dictClass2.has_key(file2[0:6]) : className="%s*"%dictClass2[file2[0:6]]
            elif dictClass3.has_key(file2[0:5]) :className="%s*"%dictClass3[file2[0:5]]
            else : className="NA";print "Warning!!! Can't find class, seqName[%s] in ClassFile[%s]"%(file2,self.classementFileName) ;sys.exit()
            unClass=className.split(".")
            tabInfo[i]=tabInfo[i]+[unClass[0],"%s.%s"%(unClass[0],unClass[1]),className]
            out+="%s\n"%("\t".join(tabInfo[i]))
#            print "\t".join(tabInfo[i])
        return tabInfo,out

    def seuilScoreAnalyse(self,tabScore,NbPercentFrag=None):
        """
        Analyser un certaine nombre de points, 
        Sortir les seuils de score pour 5% 10% 20% 25% des points. 
        """
        Info=""
        pc5=int(len(tabScore)*0.05); seuil_5Pc=tabScore[pc5-1]
        pc10=int(len(tabScore)*0.1); seuil_10Pc=tabScore[pc10-1] 
        pc20=int(len(tabScore)*0.2); seuil_20Pc=tabScore[pc20-1]
        pc25=int(len(tabScore)*0.25);seuil_25Pc=tabScore[pc25-1]
        if NbPercentFrag : pcInput=int(len(tabScore)*NbPercentFrag);seuil_InPc=tabScore[pcInput-1]
        else : seuil_InPc=None
        Info+= "================[ Score Threshold ]===========Min[%f - %f]Max\n"%(min(tabScore),max(tabScore))
        Info+= "--> FragHMM_length %s\n"%(str(self.tabLengthFrg))
        Info+= "  Agree with  5.00%% Best Score : Nb_frg=%d/%d\tThreshold_Score=%f\n"%(pc5,len(tabScore),seuil_5Pc)
        Info+= "  Agree with 10.00%% Best Score : Nb_frg=%d/%d\tThreshold_Score=%f\n"%(pc10,len(tabScore),seuil_10Pc)
        Info+= "  Agree with 20.00%% Best Score : Nb_frg=%d/%d\tThreshold_Score=%f\n"%(pc20,len(tabScore),seuil_20Pc)
        Info+= "  Agree with 25.00%% Best Score : Nb_frg=%d/%d\tThreshold_Score=%f\n"%(pc25,len(tabScore),seuil_25Pc)
        if NbPercentFrag :
            Info+= "Calcul. with %5.2f%% Best Score : Nb_frg=%d/%d\tThreshold_Score=%f\n"%((NbPercentFrag*100),pcInput,len(tabScore),seuil_InPc)
        Info+= "\n-------------------------------------------------------------\n"
        print Info;self.AffText+=Info
        return pc5,pc10,pc20,pc25,seuil_5Pc,seuil_10Pc,seuil_20Pc,seuil_25Pc,seuil_InPc
            
    def filte(self, tabInfo, seuilScore_=None,seuilCRMSD_=None,file1name=None,lengthFrg=None,seuilPreciConserver=None,seuilPreciEliminer=None,NumColPreci=0,NumColPoids=12,remplacerDisJSparMeanJS=False,NumColallDisJS=9,allFragAjouter=True,SeuilConfianceEff_=2,CovNfois=1):
        """
        InType:
        # 'file1    file2    file1_from    file1_to    file2_from    file2_to    distJS(7)    RMSd(8)  JSperSite lenFragHHM nameClust effClust(12) '
        
        tabOut: 'file1', 'file2',  'file1_from', 'file1_to', 'file2_from', 'file2_to', 'score', 'cRMSd' .... 'length'
        """
        dictFile1Matrix={}
        tabFilt=[]
        strFilt=""
        compt=0
        
        if not allFragAjouter and not self.HHfrag :
            numColDisJS=6;numColFragLength=-4
            tabInfo=self.sortList(tabInfo, (NumColPreci-1),numColFragLength,(NumColPoids-1), numColDisJS) # Preci-Len-Poids-MJS (Best ordre)
#            tabInfo=self.sortList(tabInfo,(NumColPreci-1),(NumColPoids-1), numColDisJS,numColFragLength) # Preci-Poids-MJS-Len Attention: il faut aussi modifier sortList()
            
        for i in xrange(len(tabInfo)):
            File1Name=tabInfo[i][0].replace("../","").split('.')[0]
            lengthFrag=(int(tabInfo[i][3])-int(tabInfo[i][2])+1)
            if not dictFile1Matrix.has_key(File1Name):dictFile1Matrix[File1Name]=[0]*self.dictFileLength[File1Name]
            if seuilScore_=="Auto" : 
                for j in xrange(len(self.seuilJStab)):
                    if self.seuilJStab[j][0]==lengthFrag:
                        seuilScore=self.seuilJStab[j][1]
            else: seuilScore=seuilScore_

            if seuilCRMSD_=="Auto" :
                if self.HHfrag: lengthFrag=lengthFrag-3
                for j in xrange(len(self.seuilCRMSDtab)):
                    if self.seuilCRMSDtab[j][0]==lengthFrag:
                        seuilCRMSD=self.seuilCRMSDtab[j][1]
            else: seuilCRMSD=seuilCRMSD_
            
            if SeuilConfianceEff_=="Auto" : 
                for j in xrange(len(self.seuilConfienceEffTab)):
                    if self.seuilConfienceEffTab[j][0]==lengthFrag:
                        SeuilConfianceEff=self.seuilConfienceEffTab[j][1]
            else: SeuilConfianceEff=SeuilConfianceEff_
            
            if remplacerDisJSparMeanJS : #replace maxDisJS a MeanDisJS 
#                print tabInfo[i][6],tabInfo[i][8]
                tabDistJS=tabInfo[i][NumColallDisJS-1].split("-")
                tabDistJS = [float(x) for x in tabDistJS]
                tabInfo[i][6]=str(sum(tabDistJS)/len(tabDistJS))
#                print tabInfo[i][6]
#                if i==10: sys.exit()
            
            if (seuilScore_==None or float(tabInfo[i][6])<=seuilScore) and (seuilCRMSD==None or float(tabInfo[i][7])<=seuilCRMSD) and (file1name==None or file1name==tabInfo[i][0]) and (lengthFrg==None or lengthFrg==lengthFrag):
#                print "seuilScore:",seuilScore," seuilCRMSD:",seuilCRMSD," ReelEff:",tabInfo[i][NumColPoids-1]," NumColPoids:",NumColPoids
                compt +=1
                if False:#self.Aff:
                    print "%d\t%s-%s \t"%(compt,tabInfo[i][2],tabInfo[i][3]),tabInfo[i][6],"< SCORE-Max[",seuilScore,"] \tAND\t",tabInfo[i][7],"< CRMSD-Max[",seuilCRMSD,"]" ;
                    print "File1_Name:",tabInfo[i][0],"[ref:",file1name,"]\tAND\t","length_frag:",lengthFrag,"[ref:]",lengthFrg
#                print tabInfo[i]
#                tabFilt.append(tabInfo[i])
#                Accepte tous les fragment avec eff >=XX
                if SeuilConfianceEff==None or int(tabInfo[i][NumColPoids-1])<SeuilConfianceEff:BigEffAjouter=False
                else: BigEffAjouter=True
                
                #accepter tous les fragments avec son precision>seuilPreciEliminer
                if seuilPreciConserver==None or float(tabInfo[i][NumColPreci-1])<seuilPreciConserver: GoodPreciAjouter=False
                else : GoodPreciAjouter=True
                
                # supprimer tous les fragments avec son precision<seuilPreciEliminer
                if seuilPreciEliminer==None or float(tabInfo[i][NumColPreci-1])>=float(seuilPreciEliminer): BadPreciDel=False
                else: BadPreciDel=True
                
                if not self.HHfrag : (dictFile1Matrix[File1Name],AjouterFrag)=coverMatrix(dictFile1Matrix[File1Name],int(tabInfo[i][2]),int(tabInfo[i][3])+3,allFragAjouter,BigEffAjouter,GoodPreciAjouter,BadPreciDel,CovNfois)
                else : (dictFile1Matrix[File1Name],AjouterFrag)=coverMatrix(dictFile1Matrix[File1Name],int(tabInfo[i][2]),int(tabInfo[i][3]),True) # Attention Ici +3 c'est length de AA seq [AS+3=AA]
                if AjouterFrag: 
                    if self.AjouterFragLength: tabInfo[i].append(str(lengthFrag))
                    tabFilt.append(tabInfo[i])
                    strFilt+="%s\n"%("\t".join(map(str,tabInfo[i])))
        if remplacerDisJSparMeanJS: print "Attention: Replace MaxJS -> MeanJS, Probablement Seuil-DisJS ai aussi besoin de changer!"
        return tabFilt,dictFile1Matrix,strFilt
    
    def plotROC(self,tabInfo,seuilScore_,seuilCRMSD_,NumColPoids=None):

        Info=""
        compte=0
        rocTP=0;rocFP=0;rocFN=0;rocTN=0
        sumTPscore=0;sumFPscore=0;sumFNscore=0;sumTNscore=0;sumAllscore=0
        sumTPcrmsd=0;sumFPcrmsd=0;sumFNcrmsd=0;sumTNcrmsd=0;sumAllcrmsd=0

        dictSeuilJSAuto={};dictSeuilCRMSDAuto={}
#        if seuilScore_!="Auto":seuilScore=seuilScore_
#        if seuilCRMSD_!="Auto": seuilCRMSD=seuilCRMSD_
        
        for i in xrange(len(tabInfo)):
            if self.HHfrag:
                score=-1
                lengthFrag=(int(tabInfo[i][3])-int(tabInfo[i][2])+1-3) #Attention HHfrag LenAA=LenHMM+3
                if lengthFrag<0: continue #if <0: len<3AA <3:len<6AA 
            else :
                score=float(tabInfo[i][6])
                lengthFrag= (int(tabInfo[i][3])-int(tabInfo[i][2])+1)
#                if lengthFrag>18: continue #if <0: len<3AA <3:len<6AA
            if seuilCRMSD_!=None: crmsd=float(tabInfo[i][7])
            if NumColPoids==None: 
                sumAllscore+=score
                if seuilCRMSD_!=None: sumAllcrmsd+=crmsd
                compte+=1
            else:
                poids=int(tabInfo[i][NumColPoids-1])
                sumAllscore+=score*poids
                if seuilCRMSD_!=None: sumAllcrmsd+=crmsd*poids
                compte+=poids
            if seuilScore_=="Auto" : 
                for i in xrange(len(self.seuilJStab)):
                    if self.seuilJStab[i][0]==lengthFrag:
                        seuilScore=self.seuilJStab[i][1]
                        if not dictSeuilJSAuto.has_key(lengthFrag):
                            dictSeuilJSAuto[lengthFrag]=self.seuilJStab[i][1]
            else:  seuilScore=seuilScore_
            if seuilCRMSD_=="Auto" : 
                for i in xrange(len(self.seuilCRMSDtab)):
                    if self.seuilCRMSDtab[i][0]==lengthFrag:
                        seuilCRMSD=self.seuilCRMSDtab[i][1]
                        if not dictSeuilCRMSDAuto.has_key(lengthFrag):
                            dictSeuilCRMSDAuto[lengthFrag]=self.seuilCRMSDtab[i][1]
            else:  seuilCRMSD=seuilCRMSD_                        
#                        seuilCRMSD=None
            
            if NumColPoids==None:
                if (score<=seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd<=seuilCRMSD ): 
                    rocTP+=1;sumTPscore+=score
                    if seuilCRMSD_!=None:sumTPcrmsd+=crmsd
                if (score<=seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd>seuilCRMSD ) : 
                    rocFP+=1;sumFPscore+=score
                    if seuilCRMSD_!=None:sumFPcrmsd+=crmsd
                if (score>seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd<=seuilCRMSD ) : 
                    rocFN+=1;sumFNscore+=score
                    if seuilCRMSD_!=None:sumFNcrmsd+=crmsd
                if (score>seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd>seuilCRMSD )  : 
                    rocTN+=1;sumTNscore+=score
                    if seuilCRMSD_!=None:sumTNcrmsd+=crmsd
            else:    
                if (score<=seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd<=seuilCRMSD ): 
                    rocTP+=1*poids;sumTPscore+=score*poids
                    if seuilCRMSD_!=None:sumTPcrmsd+=crmsd*poids
                if (score<=seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd>seuilCRMSD ) : 
                    rocFP+=1*poids;sumFPscore+=score*poids
                    if seuilCRMSD_!=None:sumFPcrmsd+=crmsd*poids
                if (score>seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd<=seuilCRMSD ) : 
                    rocFN+=1*poids;sumFNscore+=score*poids
                    if seuilCRMSD_!=None:sumFNcrmsd+=crmsd*poids
                if (score>seuilScore or seuilScore==None) and (seuilCRMSD==None or crmsd>seuilCRMSD )  : 
                    rocTN+=1*poids;sumTNscore+=score*poids
                    if seuilCRMSD_!=None:sumTNcrmsd+=crmsd*poids
        
        self.moyTPscore=0;self.moyFPscore=0;self.moyFNscore=0;self.moyTNscore=0;self.moyAllscore=0
        self.moyTPcrmsd=0;self.moyFPcrmsd=0;self.moyFNcrmsd=0;self.moyTNcrmsd=0;self.moyAllcrmsd=0
        if rocTP!=0: self.moyTPscore=sumTPscore/rocTP;self.moyTPcrmsd=sumTPcrmsd/rocTP
        if rocFP!=0: self.moyFPscore=sumFPscore/rocFP;self.moyFPcrmsd=sumFPcrmsd/rocFP
        if rocFN!=0: self.moyFNscore=sumFNscore/rocFN;self.moyFNcrmsd=sumFNcrmsd/rocFN
        if rocTN!=0: self.moyTNscore=sumTNscore/rocTN;self.moyTNcrmsd=sumTNcrmsd/rocTN
        if len(tabInfo)!=0: self.moyAllscore=sumAllscore/compte; self.moyAllcrmsd=sumAllcrmsd/compte
        if (rocTP+rocFN)!=0:sensitivity=float(rocTP)/float(rocTP+rocFN)
        else :sensitivity=0
        if (rocFP+rocTN)!=0:specificity=float(rocTN)/float(rocFP+rocTN)
        else : specificity=0
        if (rocTP+rocFP)!=0:precision=float(rocTP)/float(rocTP+rocFP)
        else : precision=0
        if seuilScore==None or seuilCRMSD==None:sensitivity=-1;specificity=-1
        if seuilCRMSD==None:precision=-1
        if NumColPoids==None : Info+= "          < Data ROC >  nb:%d\n"%len(tabInfo)
        else : Info+= "          < Data ROC >  nb:%d  +poids\n"%len(tabInfo)
        Info+= " *[cRMSd]\n | FP=%4d\t|\tTN=%4d\n"%(rocFP,rocTN)
        
        if seuilScore_==None :     seuilScore=" NA "
        elif seuilScore_=="Auto" : seuilScore="Auto"
        else :seuilScore_="%4.2f"%seuilScore
        
        if seuilCRMSD_==None :     seuilCRMSD=" NA "
        elif seuilCRMSD_=="Auto" : seuilCRMSD="Auto"
        else :seuilCRMSD_="%4.2f"%seuilCRMSD    
        
        Info+= "%s ---------- + -----------\n"%seuilCRMSD_
        Info+= " | TP=%4d\t|\tFN=%4d\n"%(rocTP,rocFN)
        Info+= " |_____________%s____________________*[scoreJS]\n"%seuilScore_
        Info+= "  - Sensitivity[TP/(TP+FN)]: %5.4f\n"%sensitivity
        Info+= "  - Specificity[TN/(TN+FP)]: %5.4f\n"%specificity
        Info+= "  - Precision  [TP/(TP+FP)]: %5.4f\n"%precision
        Info+= "  [Seuil_scoreJS:%s  Seuil_cRMSd:%s]\n"%(seuilScore_,seuilCRMSD_)
        if seuilScore_=="Auto" :
            strDict=""
            for k in dictSeuilJSAuto:
                strDict+="len%d:%5.3f "%(int(k)+3,dictSeuilJSAuto[k])
            Info+= "* Seuil_scoreJS (Auto)[%s]: [ %s]\n"%(self.seuilDisJS,strDict)
            
        if seuilCRMSD_=="Auto" :
            strCRMSD=""
            for k in dictSeuilCRMSDAuto:
                strCRMSD+="len%d:%5.3f "%(int(k)+3,dictSeuilCRMSDAuto[k])
            Info+= "* Seuil_CRMSD (Auto): [ %s]\n"%strCRMSD
            
        Info+="[MoyJS:%6.4f MoyCRMSD:%4.2f]\t[MoyJS-TP:%6.4f MoyCRMSD-TP:%4.2f]"%(self.moyAllscore,self.moyAllcrmsd,self.moyTPscore,self.moyTPcrmsd)
        if self.Aff: print Info
        self.AffText+=Info
        return rocTP,rocTN,rocFP,rocFN,sensitivity,specificity
        
def outPut(text, where = None,mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file: "+where+"]"
    if where:
        f.close()

def compteCover(dictFile1Matrix,Aff=True): 
    """
            Name    cover  len  %Cover
    output: [['All', 162, 716, 0.22625698324022347], ['T0388', 39, 174, 0.22413793103448276], ['T0389', 36, 134, 0.26865671641791045], ... ....
    """
    tabCover=[]
    NbSiteCover=0
    totalSite=0
    Info=""   
    if Aff: print "\n===========[ % Cover ]==========< __0..10--20''30`` >"
    Info+="\n===========[ % Cover ]==========< __0..10--20''30`` >\n"
    listName=dictFile1Matrix.keys()
    listName.sort()
    for n in listName:
#        if n=="T05523" or n=="T05371": continue
        compte=0
        tabMatrix=dictFile1Matrix[n]
        grap=""
        for j in xrange(len(tabMatrix)):
            if tabMatrix[j]!=0: 
                compte+=1
                if tabMatrix[j]<10:grap+="."
                elif tabMatrix[j]<20:grap+="-"
                elif tabMatrix[j]<30:grap+="'"
                else :grap+="`"
            else : grap+="_"    
        pourcentageCover = float(compte)/float(len(tabMatrix))
        tabCover.append([n,compte,len(tabMatrix),pourcentageCover])
        if Aff:print " %s\t%5.2f%%\t[%2d/%3d]\t0%s%d"%(n,pourcentageCover*100,compte,len(tabMatrix),grap,len(tabMatrix))
        Info+=" %s\t%5.2f%%\t[%2d/%3d]\t0%s%d\n"%(n,pourcentageCover*100,compte,len(tabMatrix),grap,len(tabMatrix))
        NbSiteCover+=compte
        totalSite+=len(tabMatrix)
    pourcentageCoverAll=float(NbSiteCover)/float(totalSite)
    
    if Aff: print "++< All\t%4.2f%%\t[%d/%d] >+++++[%d files]\n"%(pourcentageCoverAll*100,NbSiteCover,totalSite,len(listName))
    Info+="++< All\t%4.2f%%\t[%d/%d] >+++++[%d files]\n\n"%(pourcentageCoverAll*100,NbSiteCover,totalSite,len(listName))
    tabCover=[["All",NbSiteCover,totalSite,pourcentageCoverAll]]+tabCover
    return tabCover,Info

def coverMatrix(tabIn,deb,fin,allAjouter,BigEffAjouter=False,GoodPreciAjouter=False,BadPreciDel=False,CovNfois=1): #BadPreciDel
    Ajouter=False
    if not allAjouter:
        for i in xrange((deb-1),fin): 
            if tabIn[i]<CovNfois or BigEffAjouter or GoodPreciAjouter:  Ajouter=True #dflt
#            if tabIn[i]==1 or tabIn[i]==0 or BigEffAjouter :  Ajouter=True
#            if tabIn[i]==2 or tabIn[i]==1 or tabIn[i]==0 or BigEffAjouter :  Ajouter=True
#            if tabIn[i]==3 or tabIn[i]==2 or tabIn[i]==1 or tabIn[i]==0 or BigEffAjouter :  Ajouter=True
#            if tabIn[i]==4 or tabIn[i]==3 or tabIn[i]==2 or tabIn[i]==1 or tabIn[i]==0 or BigEffAjouter :  Ajouter=True

#            if tabIn[i]==0 and BigEffAjouter :  Ajouter=True #n'ajouter que les effectif > certaine nombre
            if BadPreciDel:Ajouter=False
            
    if Ajouter or allAjouter:
        for i in xrange((deb-1),fin): 
            tabIn[i]+=1
            Ajouter=True
    return tabIn,Ajouter

def SelectNBestSeqChaquePosiFromAllData(tabData,dictFileLength,nbBest=5,numJSCol=6,numRMSDCol=7,numTragetCol=0,numStartCol=2,allFragAjouter=True,BigEffAjouter=False,GoodPreciAjouter=False,BadPreciDel=False,CovNfois=1):
    """
    pour chaque seq de chaque position select 5 meilleur frag
    """
    dictFileMatirx_js={}
    dictFileMatirx_rmsd={}
    strOut_js=""
    strOut_rmsd=""
    tabFilte_js=[]
    tabFilte_rmsd=[]
    dictDataFilt_js={}
    dictDataFilt_rmsd={}
    
    dictData_rmsd={}
    dictData_js={}
    dictData={}
#    ----------[Load]-----------
    for unFrag in tabData:
#       info : 
        FileName  =unFrag[numTragetCol].split('.')[0]
        posiStart =int(unFrag[numStartCol])
        rmsd      =float(unFrag[numRMSDCol])
        js        =float(unFrag[numJSCol])
        lengthFrag=(int(unFrag[3])-int(unFrag[2])+1)
        
        if not dictData.has_key(FileName):
            dictFileMatirx_js[FileName]={}
            dictFileMatirx_rmsd[FileName]={}
            dictData[FileName]={} #Data
            dictData_js[FileName]={} #Data
            dictData_rmsd[FileName]={} #Data
            dictDataFilt_js[FileName]={} #Data
            dictDataFilt_rmsd[FileName]={} #Data
            print "FileName",FileName

        if not dictData[FileName].has_key(lengthFrag):
            dictData[FileName][lengthFrag]= {}#Data
            dictData_js[FileName][lengthFrag]= {}
            dictData_rmsd[FileName][lengthFrag]= {}
            dictDataFilt_js[FileName][lengthFrag]= {}
            dictDataFilt_rmsd[FileName][lengthFrag]= {}
            
        if not dictData[FileName][lengthFrag].has_key(posiStart):
            dictData[FileName][lengthFrag][posiStart]=[]
            dictData_js[FileName][lengthFrag][posiStart]=[]
            dictData_rmsd[FileName][lengthFrag][posiStart]=[]
            dictDataFilt_js[FileName][lengthFrag][posiStart]=[]
            dictDataFilt_rmsd[FileName][lengthFrag][posiStart]=[]
            
        dictData[FileName][lengthFrag][posiStart].append(unFrag)
        dictData_js[FileName][lengthFrag][posiStart].append(js)
        dictData_rmsd[FileName][lengthFrag][posiStart].append(rmsd)
        
#    print dictData['T0643'][7][1:5]
#     ---------[Filt]----------
    for FileName in dictData.keys():
        dictFileMatirx_rmsd[FileName]=[0]*dictFileLength[FileName]
        dictFileMatirx_js[FileName]  =[0]*dictFileLength[FileName]
        for lengthFrag in dictData[FileName].keys():
            for posi in dictData_rmsd[FileName][lengthFrag].keys():
#                print len(dictData[FileName][lengthFrag][posi])
#                print dictData_rmsd[FileName][lengthFrag][posi]
                if nbBest<=len(dictData[FileName][lengthFrag][posi]):
                    seuilRMSD=sorted(dictData_rmsd[FileName][lengthFrag][posi])[nbBest-1]
                    seuilJS  =sorted(dictData_js[FileName][lengthFrag][posi])[nbBest-1]
                else : seuilRMSD=None;seuilJS=None
#                print posi,"seuilRMSD",seuilRMSD,min(dictData_rmsd[FileName][lengthFrag][posi]) 
                for unFrag in dictData[FileName][lengthFrag][posi]:
                    unMatrix=[0]*dictFileLength[FileName]
                    rmsd      =float(unFrag[numRMSDCol])
                    js        =float(unFrag[numJSCol])
                    posiStart =int(unFrag[numStartCol])
                    posiEnd   =int(unFrag[numStartCol+1])
                    
                    if rmsd<=seuilRMSD or seuilRMSD==None:
#                        print "1"
                        dictDataFilt_rmsd[FileName][lengthFrag][posi].append(unFrag)
                        tabFilte_rmsd.append(unFrag)
                        strOut_rmsd+="%s\n"%"\t".join(unFrag)
                        dictFileMatirx_rmsd[FileName]=coverMatrix(dictFileMatirx_rmsd[FileName],posiStart,posiEnd+3,allFragAjouter,BigEffAjouter) # Attention Ici +3 c'est length de AA seq [AS+3=AA]
                    if js<=seuilJS or seuilJS==None:
#                        print "2"
                        dictDataFilt_js[FileName][lengthFrag][posi].append(unFrag)
                        tabFilte_js.append(unFrag)
                        strOut_js+="%s\n"%"\t".join(unFrag)
                        dictFileMatirx_js[FileName]=coverMatrix(dictFileMatirx_js[FileName],posiStart,posiEnd+3,allFragAjouter,BigEffAjouter) # Attention Ici +3 c'est length de AA seq [AS+3=AA]
#                print FileName,lengthFrag," lenRMSD",len(dictDataFilt_rmsd[FileName][lengthFrag][posi])," lenJS",len(dictDataFilt_js[FileName][lengthFrag][posi])
    return     strOut_js,tabFilte_js,dictDataFilt_js,dictFileMatirx_js,strOut_rmsd,tabFilte_rmsd,dictDataFilt_rmsd,dictFileMatirx_rmsd
        

if __name__ == "__main__":
    
    if len(sys.argv) != 18:
        print "Ex: /home/freetown/shen/PP-Search/scripts/ProfAnalyse/AnalyseDATA_V2.py file Auto NA 5PC fileOut Yes No -1 NA NA No NA No No 0.99 0.9 0 4"
        print "filt priority : (cRMSD and MJS) > AllFragAjouter > BadPreciDel > (GoodPreciAjouter or BigEffAjouter)"
        print "Line 693 (Dflt): NumColumn_MJS=7(6) NumColumn_LenFrag=-3(-4), if not must modify it."
#        [1.FileName] [2.seuilMJS] [3.seuilCRMSD] [4.typeSeuilMJS] [5.OutFile] [6.Rapide] [7.OutInfo] [8.NumColPoids] [9.SeuilConfianceEff] [10.DataHHfrag] [11.NBest-JS/CRMSD]  [12.ReplaceJSMaxToMean] [13.AllFragAjouter] [14.seuilGoodPreciAjouter] [15.seuilBadPreciDel],[16.NumColPreci],[17.CovNfois]
        # num                                                 1                                             2                   3                                4                                            5                             6                7                 8                        9                            10                                 11                      12                                       13                                    14                                                     15                                        16                          17
        sys.exit("usage: "+sys.argv[0]+" [1.FileName:T0471-99-101-DisJSNA-rmsdNA.data.fin] [2.seuilMJS:Auto/0.x/NA] [3.seuilCRMSD:Auto/x.x/NA] [4.typeSeuilMJS(pcErr):old/3PC/4PC/5PC/8PC/10PC] [5.OutFile:fileName/NA/No] [6.Rapide:Yes/No] [7.OutInfo:Yes/No] [8.NumColPoids:NA/12] [9.SeuilConfianceEff:NA/Auto/2/3...] [10.DataHHfrag:Yes/No]   [11.NBest-JS/CRMSD:NA/1/2/...]  [12.ReplaceJSMaxToMean:Yes/No] [13.AllFragAjouter( or BestFrag&FixCoverage): Yes/No] [14.seuilGoodPreciAjouter:NA/0.99/0.98/0.985/...] [15.seuilBadPreciDel: NA/0.9/0.85/0.8/...],[16.NumColPreci:NA/0/11/...],[17.CovNfois:NA/1/2/3/4]")

    FileName=sys.argv[1]
    
    seuilScore=sys.argv[2];seuilScore_NbPercentFrag=None
    if seuilScore=="NA":seuilScore=None
    
    seuilCRMSD=sys.argv[3]
    if seuilCRMSD=="NA":seuilCRMSD=None
    
    seuilDisJS=sys.argv[4]
    
    if sys.argv[6]=="Yes" or sys.argv[6]=="yes" or sys.argv[6]=="y" :rapide=True
    else : rapide=False
    
    if sys.argv[8]=="NA":NumColPoids=None
    else :NumColPoids=sys.argv[8]

    if sys.argv[9]=="NA":SeuilConfianceEff=None
    else :SeuilConfianceEff=sys.argv[9]
    
    if sys.argv[10]=="Yes" or sys.argv[10]=="yes" or sys.argv[10]=="y" :HHfrag=True
    else : HHfrag=False
    
    if sys.argv[12]=="Yes" or sys.argv[12]=="yes" or sys.argv[12]=="y" :remplacerDisJSparMeanJS=True
    else : remplacerDisJSparMeanJS=False
    
    if sys.argv[13]=="Yes" or sys.argv[13]=="yes" or sys.argv[13]=="y" :allFragAjouter=True 
    else : allFragAjouter=False
    
    if sys.argv[14]=="NA":GoodPreciAjouter=None
    else :GoodPreciAjouter=sys.argv[14]
    
    if sys.argv[15]=="NA":BadPreciDel=None
    else :BadPreciDel=sys.argv[15]

    if sys.argv[16]=="NA":NumColPreci=None
    else :NumColPreci=sys.argv[16]
    
    if sys.argv[17]=="NA":CovNfois=None
    else :CovNfois=sys.argv[17]
    
    classementFile=None
    AjouterFragLength=False
#    allFragAjouter=True 

    x=Analyse(FileName, seuilScore, seuilCRMSD,seuilScore_NbPercentFrag,classementFile,rapide,AjouterFragLength,seuilDisJS,NumColPoids,HHfrag,remplacerDisJSparMeanJS,allFragAjouter,SeuilConfianceEff,GoodPreciAjouter,BadPreciDel,NumColPreci,CovNfois)
#              FileName, seuilScore, seuilCRMSD,seuilScore_NbPercentFrag,classementFile,rapide,AjouterFragLength,seuilDisJS,NumColPreci,NumColPoids,HHfrag,remplacerDisJSparMeanJS,allFragAjouter,SeuilConfianceEff,seuilPreciConserver,seuilPreciEliminer,NumColPreci,CovNfois):
    OutFile=sys.argv[5]
    FileNameOut=FileName
    if x.tabFilteStr!="" and OutFile!="No":
        if OutFile =="NA":
            if remplacerDisJSparMeanJS:FileNameOut=FileName+".JSMEAN"
            if seuilCRMSD!=None:FileNameOut=FileNameOut+".filt-%sA"%str(seuilCRMSD)
            if allFragAjouter==False:  FileNameOut=FileNameOut+"-Filt"
            if SeuilConfianceEff!=None:  FileNameOut=FileNameOut+".PoidSup"+str(SeuilConfianceEff)
            if GoodPreciAjouter!=None:  FileNameOut=FileNameOut+".PreciAjouter"+str(GoodPreciAjouter)
            if BadPreciDel!=None:  FileNameOut=FileNameOut+".PreciDel"+str(BadPreciDel)
            if CovNfois!=None:  FileNameOut=FileNameOut+".Cov"+str(CovNfois)
        else : FileNameOut=OutFile
        if FileNameOut==FileName: print "Attention : FileName=FileOutName, Verifier bien si ce filte est utile. (sauf si besoin NBestJS-RMSD)"
        else :  outPut(x.tabFilteStr,FileNameOut)
    OutInfo=sys.argv[7]
    if OutInfo=="Yes" or OutInfo=="yes" or OutInfo=="y": outPut(x.AffText,FileNameOut+".info")
    
#    """
#    Pour chaque position de chaque filte les n meilleur JS and RMSD 
#    """
    if sys.argv[11]=="NA":nbBest=-1
    else : nbBest=int(sys.argv[11])
    if nbBest>0 :
        [strOut_js,tabFilte_js,dictDataFilt_js,dictFileMatirx_js,strOut_rmsd,tabFilte_rmsd,dictDataFilt_rmsd,dictFileMatirx_rmsd]=SelectNBestSeqChaquePosiFromAllData(x.tabFilte,x.dictFileLength,nbBest,BigEffAjouter,GoodPreciAjouter,BadPreciDel,CovNfois)
        print "JS nb:%d\n"%len(tabFilte_js);x.AffText+="JS nb:%d\n"%len(tabFilte_js)
        #print dictFileMatirx_js;            x.AffText+=str(dictFileMatirx_js)+"\n"
        #compteCover(dictFileMatirx_js)
        print "RMSD nb:%d\n"%len(tabFilte_rmsd);x.AffText+="RMSD nb:%d\n"%len(tabFilte_rmsd)
        #print dictFileMatirx_rmsd;              x.AffText+=str(dictFileMatirx_rmsd)+"\n"
        #compteCover(dictFileMatirx_rmsd)
    
        outPut(strOut_js,FileNameOut+".%dBestJS-%sA"%(nbBest,str(seuilCRMSD)))
        outPut(strOut_rmsd,FileNameOut+".%dBestRMSD-%sA"%(nbBest,str(seuilCRMSD)))
    
    sys.exit()
    
    
