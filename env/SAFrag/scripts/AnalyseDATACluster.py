#!/usr/bin/env python
'''
Created on June, 2011

@author: shen
'''
import sys
import subprocess
from datetime import datetime

class Cluster:
    
    def __init__(self,FileName,ClusterScript,pathBD,seuilCRMSD,BestFragParPosi,Aff=False):
        """
        read file *.results.final
        calculer pourcentage couvert
        """
        self.Aff=Aff
#        self.AffText=""
        self.verbose=0
        
        print "Start -> ",datetime.now()
        #------------Input file -------------
        (self.tabData,self.tabRefTarget,self.tabLengthFrg)=self.inputFile(FileName)  #input Score et CRMSD infos
#        print "DictFile1_Len:",self.dictFileLength,"\nTabFragLength:",self.tabLengthFrg
        self.nbData=len(self.tabData)
        print "Load ok ->  ",datetime.now()
        (self.refCluster,self.elementCluster,self.outStrAll)=self.ajouterCluster(self.tabData,ClusterScript,pathBD,seuilCRMSD)
        #print len(self.refCluster),self.refCluster
        #print len(self.elementCluster),self.elementCluster
        if len(self.elementCluster)!=len(self.refCluster): print "Err L-ele:",len(self.elementCluster),"diff L-ref:",len(self.refCluster); sys.exit()
        self.outStrRef="";self.tabRefCl=[]
        for i in range(len(self.refCluster)):
#            print self.refCluster[i],len(self.elementCluster[i]),len(self.refCluster)
            self.outStrRef+="%s\t%d\n"%("\t".join(self.refCluster[i]),len(self.elementCluster[i]))
            self.tabRefCl.append(self.refCluster[i]+[str(len(self.elementCluster[i]))])
        
#------------[Filter Best Fragment par length par position]---------------------        
        if BestFragParPosi:
            self.outStrBest=""
            print "Filter BestFragPerPosi ->  ",datetime.now()        
            numColClustName=-1;numColClustPoids=0
            (self.dictBestCluster,self.tabBestCluster)=self.BestCluster(self.tabRefCl, numColClustName, numColClustPoids)
            print "Filter Nb[ %d -> %d ]"%(len(self.refCluster),len(self.dictBestCluster))
            tabNameCluster=self.dictBestCluster.keys()
            tabNameCluster.sort()
            for i in tabNameCluster:
                self.outStrBest+="%s\n"%("\t".join(self.dictBestCluster[i]))
        
        print "End -> ",datetime.now()


    def inputFile(self,fname):
        """
        read a *.results.final
        'file1', 'file2', 'length', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS', 'cRMSd_f', 'TMScore', 'cRMSd', 'GDT_TS', 'len_fstFile1'
        'file1', 'file2', 'file1_from', 'file1_to', 'file2_from', 'file2_to', 'distJS','RMSd', 'DisJSperSite'
        """
        f = open(fname)
        print "[Load ScoreFile:%s]"%fname
#        self.AffText+="[Load ScoreFile:%s]\n"%fname
        l = f.readlines()
        f.close()
        tabData = []
        tabRefTarget=[]
        tabLengthFrg=[]
        for i in range(0,len(l)):
            tempTab=[]
            if len(l)!=0:   
                if l[i]!="\n" and l[i][0]!="#":
                    tempTab=l[i].replace("\n","").split()
                    tabData.append(tempTab)
                    nameRefTarget=tempTab[0].split('.')[0] 
                    tabRefTarget.append(nameRefTarget)
                    lengthFrag= (int(tempTab[3])-int(tempTab[2])+1)
                    tabLengthFrg.append(lengthFrag) # length frag hmm
#                print tempTab
            else :
                print "Errors";
                sys.exit();
        tabRefTarget=sorted(set(tabRefTarget))
        tabLengthFrg=sorted(set(tabLengthFrg))
        return tabData,tabRefTarget,tabLengthFrg
    
    def ajouterCluster(self,tabData,scriptCluster,pathBD,seuilCRMSD=1, Aff = False):
        refCluster=[]# refCluster
        elementCluster=[]
        outStr=""
        print "seuilCRMSD",seuilCRMSD
        for i in range(len(tabData)):
            if i==int(len(tabData)*0.2) : print "%20 -> ok"
            if i==int(len(tabData)*0.4) : print "%40 -> ok"
            if i==int(len(tabData)*0.6) : print "%60 -> ok"
            if i==int(len(tabData)*0.8) : print "%80 -> ok"
            name=tabData[i][1]; deb=tabData[i][4];fin=str(int(tabData[i][5])+3);DistJS=float(tabData[i][6])
            id=tabData[i][0]+"-"+tabData[i][2]+"-"+tabData[i][3]
            lenfrag=int(tabData[i][3])-int(tabData[i][2])+1+3
            if refCluster==[]:
                cl=tabData[i][0]+"-"+tabData[i][2]+"-"+tabData[i][3]+"-Cl1"
                refCluster.append(tabData[i]+[str(lenfrag),id+"-Cl1"])
                elementCluster.append([tabData[i]+[str(lenfrag),id+"-Cl1"]])
                outStr+="%s\t%s\t%s\n"%("\t".join(tabData[i]),str(lenfrag),id+"-Cl1")
                continue
#            j=-1
            creatCluster=True
            for j in range(len(refCluster)):
                nameRef=refCluster[j][1];debRef=refCluster[j][4];finRef=str(int(refCluster[j][5])+3);DistJSRef=float(refCluster[j][6])
                idRef=refCluster[j][0]+"-"+refCluster[j][2]+"-"+refCluster[j][3]
                crmsd=-1
                if idRef==id :
                    cmd=scriptCluster+" "+pathBD+"/"+name+".xyz "+deb+" "+fin+" "+pathBD+"/"+nameRef+".xyz "+debRef+" "+finRef+" | grep -v \>"
#                    print cmd
                    (stdoutl, stderrl) = shellExec( cmd, ignerr = True,  verbose = self.verbose )#fake = self.fake
    #                if stderrl!=[]: print "".join(stderrl)
#                    print stdoutl
                    crmsd=float(stdoutl[0].replace("\n","").split()[-1])
                    if Aff : print "  [%s/%s]>%d\tcl[%d/%d]\tcrmsd: %4.2f"%(idRef,id,i,j+1,len(refCluster),crmsd)
                    if crmsd<seuilCRMSD : 
                        if Aff :print "              --> ok"
                        elementCluster[j].append(tabData[i]+[str(lenfrag),id+"-Cl%d"%(j+1)])
                        outStr+="%s\t%s\t%s\n"%("\t".join(tabData[i]),str(lenfrag),id+"-Cl%d"%(j+1))
                        if DistJSRef>DistJS:
                            if Aff :print refCluster[j]
                            refCluster[j]=tabData[i]+[str(lenfrag),id+"-Cl%d"%(j+1)]
                            if Aff :print "Replace Ref :RefDisJS[",DistJSRef,"]>thisDisJS[",DistJS,"]"; print refCluster[j]
                        creatCluster=False
                        break
            if creatCluster:
                refCluster.append(tabData[i]+[str(lenfrag),id+"-Cl%d"%(j+2)])
                elementCluster.append([])
                elementCluster[j+1].append(tabData[i]+[str(lenfrag),id+"-Cl%d"%(j+2)])
                outStr+="%s\t%s\t%s\n"%("\t".join(tabData[i]),str(lenfrag),id+"-Cl%d"%(j+2))
                if Aff :print "* %4.2f>%4.2f -> Create Cluster [%s-Cl%d]"%(crmsd,seuilCRMSD,id,(j+2))
#                if Aff :print "%s %s"%(" ".join(tabData[i]),id+"-Cl%d"%(j+2))
        return refCluster,elementCluster,outStr


    def BestCluster(self,tabData,numColClustName=11, numColClustPoids=12):
        """
        chaque position, chaque longueur fragment, choisi le best fragment
        """
        dictBestCluster={}
        tabDataOut=[]
        for i in xrange(len(tabData)):
            if i==int(len(tabData)*0.2) : print "%20 -> ok"
            if i==int(len(tabData)*0.4) : print "%40 -> ok"
            if i==int(len(tabData)*0.6) : print "%60 -> ok"
            if i==int(len(tabData)*0.8) : print "%80 -> ok"
            if i==int(len(tabData))     : print "%100 -> ok"
            className="-".join(tabData[i][numColClustName-1].split("-")[:3])
            classPoids=int(tabData[i][numColClustPoids-1])
            distJS=float(tabData[i][6])
            if not dictBestCluster.has_key(className):
                dictBestCluster[className]=tabData[i];#print dictBestCluster
                tabDataOut.append(tabData[i])
            else :
                distJSRef=float(dictBestCluster[className][6]);classPoidsRef=int(dictBestCluster[className][numColClustPoids-1])
                if classPoidsRef<classPoids : 
                    dictBestCluster[className]=tabData[i];#print classPoidsRef,"<",classPoids
                    tabDataOut.append(dictBestCluster[className])
                if classPoidsRef==classPoids and distJSRef>distJS: 
                    dictBestCluster[className]=tabData[i];#print classPoidsRef,"=",classPoids
                    tabDataOut.append(dictBestCluster[className])
#                if distJS<0.02 and distJSRef>distJS and classPoidsRef<3:  dictBestCluster[className]=tabData[i];
        return dictBestCluster,tabDataOut

        
def shellExec( cmd, stdinl = [], ignerr = False, fake = False, verbose = 1 ):
     """
     Execute a shell command, with pipelines to stdin, stdout, and stderr.
 
     @param cmd    : the command line
     @param stdinl : string list to pipe as input
     @param ignerr : ignore stderr process content
     @param fake   : only prints the command line
     @param verbose: verbose mode
 
     @return: a tuple as (stdout lines list,stderr lines list)
     """
 
     if verbose:
         print >> sys.stderr, "ShellExec command: %s" % cmd
 
     if fake:
         print >> sys.stdout, "[FAKE]\n%s" % cmd
 
     p = subprocess.Popen(cmd,
                          shell=True,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          close_fds=True)
     if len( stdinl ):
         # Prepare stdin
         iStr = ""
         for aIn in stdinl:
             iStr += "%s\n" % aIn
     else:
         iStr = None
 
     (stdoutl,stderrl) = p.communicate( input=iStr )
 
     if verbose:
         print >> sys.stderr, "Command done."
 
     return (stdoutl.splitlines(),stderrl.splitlines())     



    
def outPut(text, where = None,i="",mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file: "+where+"]\t["+str(i)+"]"
    if where:
        f.close()

if __name__ == "__main__":
    
    if len(sys.argv) != 7:
        print "If Cluster, please use: \n   /nfs/freetown/user/shen/scripts/RMSD\n   /nfs/freetown/user/shen/PP-Search/Data/PDB25ok/"
        print "  or BD-Astral1.75 use: \n   /nfs/freetown/user/shen/scripts/RMSD\n   /nfs/freetown/user/shen/PP-Search/Data/Astral1.75/Astral1.75_res2.5_R0.25/"
        sys.exit("usage: "+sys.argv[0]+" [FileName] [SeuilCRMSD] [OutPut:NA] [pathBD:NA] [scriptCluster:NA] [Filt_BestFragPerPosi: Yes/No]")

    Aff=False
    fileName=sys.argv[1]
    seuilCRMSD=float(sys.argv[2])
    if sys.argv[3]=="NA" : outfile=fileName
    else :  outfile=sys.argv[3]
    if sys.argv[4]=="NA" : pathBD="/home/freetown/shen/PP-Search/Data/PDB25ok/"
    else : pathBD=sys.argv[4]
    if sys.argv[5]=="NA" : scriptCluster="/home/freetown/shen/scripts/RMSD"
    else : scriptCluster=sys.argv[5]
    if sys.argv[6]=="Yes" or sys.argv[6]=="yes" or sys.argv[6]=="y" :BestFragParPosi=True
    else : BestFragParPosi=False

    x=Cluster(fileName,scriptCluster,pathBD,seuilCRMSD,BestFragParPosi,Aff)
    print "Total:",x.nbData
    print "Clust:",len(x.refCluster)
    
    if x.outStrAll != "":
#        outPut(x.outStrAll,outfile+".clAll"+str(seuilCRMSD)+"A",x.nbData)
#        outPut(x.outStrRef,outfile+".clRef"+str(seuilCRMSD)+"A",len(x.refCluster))
#        if BestFragParPosi: outPut(x.outStrBest,outfile+".clBest"+str(seuilCRMSD)+"A",len(x.dictBestCluster))
        outPut(x.outStrAll,outfile+".allCl",x.nbData)
        outPut(x.outStrRef,outfile+".refCl",len(x.refCluster))
        if BestFragParPosi: outPut(x.outStrBest,outfile+".bestCl",len(x.dictBestCluster))
    else : print "[Input file: %s] is empty."%fileName
    
    sys.exit()

    Aff=False
    scriptCluster="/home/freetown/shen/scripts/RMSD"
    pathBD="/home/freetown/shen/PP-Search/Data/PDB25ok/"
    pathBD="/home/freetown/shen/PP-Search/Data/PDB25_Err/Err/"
    seuilCRMSD=1
    
    
#    fileName="/home/freetown/shen/PP-Search/Analyse4/CASP9/len12/AllFilteData-DisJSAuto-rmsdNA.allData"
#    x=Cluster(fileName,scriptCluster,pathBD,seuilCRMSD,Aff)
#    outPut(x.outStrAll,fileName+".cl")
#    outPut(x.outStrRef,fileName+".clRef")
#    sys.exit()
    
#    fileName="/home/freetown/shen/PP-Search/Analyse4/CASP9/len7/T0593/aa"
#    for i in [24,20,16,12,8]:
    for i in [12,13,14,15,16,17,18,19,20,21,22,23,24]:
        fileName="/home/freetown/shen/PP-Search/Analyse4/CASP9/len%d/T0628a1/AllFilteData-DisJSAuto-rmsdNA.allData"%i
        fileName="/home/freetown/shen/PP-Search/Analyse3/CASP8/len17/T0478/AllFilteData-DisJSAuto-rmsdNA.allData"
        fileName="/home/freetown/shen/PP-Search/Analyse2/CASP9/T0643/len20/T0643/T0643-1-20-DisJSNA-rmsdNA.data.fin"
        outfile=fileName
        x=Cluster(fileName,scriptCluster,pathBD,seuilCRMSD,Aff)
        print x.nbData
        print len(x.refCluster)
        #outPut(x.outStrAll,outfile+".clAll"+str(seuilCRMSD)+"A")
        #outPut(x.outStrRef,outfile+".clRef"+str(seuilCRMSD)+"A")
        break
    
#    cmd="/home/freetown/shen/scripts/RMSD /home/freetown/shen/PP-Search/Data/PDB25ok/2yxxA.xyz 30 56 /home/freetown/shen/PP-Search/Data/PDB25ok/2yxxA.xyz 17 43"
#    (a,b)=shellExec( cmd, ignerr = True,fake = False,   verbose = 1 )
#    print "".join(a)
#    print "".join(b)
