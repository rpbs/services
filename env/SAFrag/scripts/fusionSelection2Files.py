#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
pour analyser PP research,fusioner les 2 files obtenues par scanProb.py(DistJS) et GLsearch(RMSD)
"""
import sys
import string
import time
import os.path

def outPut(text, where = None,i=0, mode="w"):
    if not where:
        f = sys.stdout
        print text
    else:
        f = open(where,mode)
        f.write(text)
        print " [Create file : "+where+"]["+str(i)+"]"
    if where:
        f.close()
        
if __name__ == "__main__":
    print "Object : Fusion DisJS and RMSD files, select set (DisJS<seuilJS and RMSD<seuilRMSD)"
    cmd=" ".join(sys.argv)
    if len(sys.argv) !=6:
        print " ".join(sys.argv)," [Err: arg != 5] "
        sys.exit("usage: fusionSelection2Files.py <file DisJS> <file RMSD> <Seuil DisJS (NA)> <Seuil RMSD (NA)> <OutputFile(NA)>")
    print cmd
#    print time.strftime("%Y-%m-%d %A %X %Z", time.localtime())
    file1=sys.argv[1]
    file2=sys.argv[2]
    seuilScore=sys.argv[3]
    seuilCRMSD=sys.argv[4]
    outputFile=sys.argv[5]
    
#    file1="/home/freetown/shen/PP-Search/Analyse2/CASP8/Nov_svm_Model_BDPc50/len17_MaxSc1_nBest27/T0507/resuDistJS.final.txt"
#    file2="/home/freetown/shen/PP-Search/Analyse2/CASP8/Nov_svm_Model_BDPc50/len17_MaxSc1_nBest27/T0507/resuGLS.final.txt"
#    seuilScore="NA" #Seuil DisJS 0.3
#    seuilCRMSD="3"#seuil CRMSD 3

    path=os.path.dirname(file1)
    if path=="": path="."
    
    if seuilScore!="NA":seuilScore=float(seuilScore)
    if seuilCRMSD!="NA":seuilCRMSD=float(seuilCRMSD)
    
    if seuilScore!="NA" and (seuilScore<0 or seuilScore >1) : sys.exit("Err : seuil distJS est entre 0 et 1")
    # Input lists
    print "Loading file1 [%s] ... ..."%(file1)
    f  = open(file1)
    l1 = f.readlines()
    f.close()
    len1=len(l1)
#    print time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!"
    print "Loading file2 [%s] ... ..."%(file2)
    g  = open(file2)
    l2 = g.readlines()
    g.close()
    len2=len(l2)
#    print time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!"
    if len1!=len2:print "Err : length File1[%d] diff File2[%d]!"%(len1,len2);sys.exit("Err : length File1[%d] diff File2[%d]!"%(len1,len2))
    if len1==0:print "Err : %s %s are empty!"%(file1,file2);sys.exit("Err : %s %s are empty!"%(file1,file2))
   
    
    #file1   file1_len  file2   HMM_length  file1_from    file1_to    file2_from   file2_to distJS  cRMSd   class1  class1-2  class
    print "Fusion 2 files ... ..."
#    tabData=[]
#    selectData=[]
#    dataOut="file1\tfile2\tfile1_from\tfile1_to\tfile2_from\tfile2_to\tdistJS\tcRMSd\n"
#    selectOut="file1\tfile2\tfile1_from\tfile1_to\tfile2_from\tfile2_to\tdistJS\tcRMSd\n"
    selectOut=""
    Info=""
    rocTP=0;rocFP=0;rocFN=0;rocTN=0
    for i in range(len2):
        if len(l2[i].split())>6 and (l1[i].split()[2]!=l2[i].split()[2] or (int(l1[i].split()[3])+3)!=int(l2[i].split()[3]) or l1[i].split()[4]!=l2[i].split()[4] or (int(l1[i].split()[5])+3)!=int(l2[i].split()[5])):
            sys.exit("Verifier line %d js != crmsd"%(i+1)) 
        if len(l2[i].split())>6: un=l1[i].split()+l2[i].split()[6:] #numcol RMSD = 4
        else : un=l1[i].split()+l2[i].split()
#        tabData.append(un)
#        dataOut+="\t".join(un)+"\n"
#        print un
        if seuilScore!="NA": score=float(un[6])
        if seuilCRMSD!="NA": crmsd=float(un[7])
        if (seuilScore=="NA" or score<seuilScore) and (seuilCRMSD=="NA" or crmsd<seuilCRMSD ): 
            rocTP+=1
            selectOut+="\t".join(un)+"\n"
        if (seuilScore=="NA" or score<seuilScore)  and (seuilCRMSD=="NA" or crmsd>=seuilCRMSD ): rocFP+=1
        if (seuilScore=="NA" or score>=seuilScore) and (seuilCRMSD=="NA" or crmsd<seuilCRMSD  ): rocFN+=1
        if (seuilScore=="NA" or score>=seuilScore) and (seuilCRMSD=="NA" or crmsd>=seuilCRMSD ): rocTN+=1
        
    if (rocTP+rocFN)!=0 :sensitivity=float(rocTP)/float(rocTP+rocFN)
    else :sensitivity=-1
    if (rocFP+rocTN)!=0:specificity=float(rocTN)/float(rocFP+rocTN)
    else : specificity=-1
    if seuilScore=="NA" or  seuilCRMSD=="NA": sensitivity=-1;specificity=-1
    
    Info+= "          < Data ROC >  nb:%d\n"%len1
    Info+= " *[cRMSd]\n | FP=%4d\t|\tTN=%4d\n"%(rocFP,rocTN)
    Info+= "%3s  ---------- + -----------\n"%str(seuilCRMSD)
    Info+= " | TP=%4d\t|\tFN=%4d\n"%(rocTP,rocFN)
    Info+= " |_____________%s____________________*[scoreJS]\n"%str(seuilScore)
    Info+= "  - Sensitivity:%5.4f\n"%sensitivity
    Info+= "  - Specificity:%5.4f\n"%specificity
    Info+= "  [Seuil_scoreJS:%s  Seuil_cRMSd:%s]\n"%(str(seuilScore),str(seuilCRMSD))
    if True: print Info
#    print time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!"
    
    print "Creation output files ... ..."
 #   outPut(dataOut,path+"/select.info")
    if outputFile=="NA":  outPut(selectOut,path+"/select-DisJS%s-rmsd%s.data"%(seuilScore,seuilCRMSD),len2)
    else:  outPut(selectOut,path+"/%s"%outputFile,rocTP)
#    outPut(dataOut,path+"/all.data")
    print time.strftime("         %Y-%m-%d %A %X %Z", time.localtime())," ... Ok!"

