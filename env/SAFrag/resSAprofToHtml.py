#!/usr/bin/env python

######
# PICORD Geraldine
# 06/08/2012
#
# Create HTML file for SAprof results
######

## Imports
import sys, os
import glob
import PyPDB.PyPDB as PDB

## Fonctions
def parseRosFile( rFile = "" ):
    fIN   = open( rFile, "rt" )
    lines = fIN.readlines()
    fIN.close()
    
    dico = {}
    
    n = 0
    cpt = 0
    while cpt < len(lines):
        if lines[cpt][:11] == " position: ":
            lSplit = lines[cpt].split()
            n = lSplit[1]
            dico[n] = {}
            dico[n]["nb_vois"] = lSplit[-1]
            dico[n]["pdb"] = []
            dico[n]["seq"] = []
        else:
            if lines[cpt] != "\n":
                lSplitC = lines[cpt].split()
                dico[n]["pdb"].append(lSplitC[0] + lSplitC[1])
                seq = lSplitC[3]
                cpt += 1
                while lines[cpt] != "\n":
                    lSplitS = lines[cpt].split()
                    seq += lSplitS[3]
                    cpt += 1
                dico[n]["seq"].append(seq)
        cpt += 1
    
    return dico


def create_Rscript( seq, dico ):
    
    ### nombre de fragment par position (de debut))
    cles = dico.keys()
    #cles = sorted( cles , key=lambda x: int(x) )
    posT    = []
    nb_hitT = []
    
    for i in range(1, len(seq)+1 ): # seq[:35]
        posT.append(str(i))
        if str(i) in cles:
            #print i, "::", dico[str(i)]["nb_vois"]
            nb_hitT.append(dico[str(i)]["nb_vois"])
        else:
            #print i, "::", 0
            nb_hitT.append("0")
    
    #print 
    #print posT
    #print nb_hitT
    
    pos    = ",".join(posT)
    nb_hit = ",".join(nb_hitT)
    
    
    ### nombre de residus a la position x
    nb_res = [0 for i in range(len(seq))]
    pos_x = [str(i+1) for i in range(len(seq))]
    
    for i in range(len(seq)):
        if str(i+1) in cles:
            for s in dico[str(i+1)]["seq"]:
                for j in range(len(s)):
                    nb_res[i+j] += 1
    
    pos    = ",".join(pos_x)
    nb_hit = ",".join([ str(i) for i in nb_res])
    #print nb_res
    #print pos_x
    
    script = '\
x = c(%s)\n\
y = c(%s)\n\
\n\
pal = colorRampPalette(c("blue", "cyan" ,"green", "yellow", "orange", "red"), space = "rgb")\n\
allCol = pal(max(y)+1)\n\
\n\
colH = NULL\n\
for (i in 1:length(y)){\n\
  colH = c(colH, allCol[y[i]+1])\n\
}\n\
\n\
tSeq = unlist(strsplit("%s", ""))\n\
ti = c(1,seq(10,length(tSeq), 10),length(tSeq))\n\
tS = tSeq[ti]\n\
tx = x[ti]\n\
png("Graphe_hits.png", 1600, 1100, units="px", pointsize=16)\n\
plot(x,y, type="h", lwd=4, col=colH, xlab="position", ylab="number of residues", bty="n", xaxt="n", yaxt="n", cex.lab=1.5)\n\
axis(1, ti, ti, cex=0.8, lwd=1.5, font=2)\n\
axis(2,cex=1.2, lwd=1.5, font=2)\n\
axis(3, ti, tS, tick=F, cex=0.8, lwd=1.5, font=2)\n\
dev.off()' %(pos,nb_hit,seq)
    
    f = open("graphe.R", "wt")
    f.write(script)
    f.close()
    
    os.system("drun safrag Rscript graphe.R ")
    
    #return ( posT, nb_hitT )
    return ( pos_x, [ str(i) for i in nb_res] )


def add_col_by_Bfact( npdb = "", nb_hits = [] ):
    try:
        pdb = PDB.PDB( npdb )
    except:
        print " !! ERROR : cannot open pdb file %s !!" %(pdb)
        sys.exit()
    
    if len(pdb) == len(nb_hits):
        for i, res in enumerate( pdb ):
            for atm in res:
                atm.tfac(float(nb_hits[i]))
        pdb.out( npdb + 'Bfact')
    else:
        print " !! ERROR pdb file and sequence have different lengths !! "
        sys.exit()


def aliToHtml( dico, seq, nb_hits):
    
    lSeq = len(seq)
    cles = dico.keys()
    cles = sorted( cles , key=lambda x: int(x) )
    
    # calcule les positions pour plus de lisibilite
    pos = ""
    i = 0
    while i < len(seq):
        if i == 0:
            pos += "<a href='#p1' onmouseover=\"this.style.fontSize='14px'\" onmouseout=\"this.style.fontSize='11px'\">1</a>"
            i += 1
            continue
    
        if (i+1) % 5 == 0:
            if (i+1) % 10 == 0:
                if str(i+1) in cles:
                    pos += "<span onclick=\"bouge('p" + str(i+1) +"')\" style=\"cursor:pointer;\" onmouseover=\"this.style.fontSize='14px'\" onmouseout=\"this.style.fontSize='11px'\">" + str(i+1) + "</span>" #+ " "*(len(str(i+1))-1)
                else:
                    pos += str(i+1)
                i += len(" "*(len(str(i+1))))
            if (i+1) % 5 == 0:
                pos += "."
                i += 1
        else:
            pos += " "
            i += 1
    
    
    # calcule le coverage
    tCov = [ i for i in nb_hits if i != "0"]
    cov = (float(len(tCov)) / float(len(seq)))*100
    
    # seq query coverage
    res ='\
     <div id="title">\n\
      <h2>Alignment (coverage = %.2f%%)</h2>\n\
     </div>\n\
     <div id="div0" style="background:#eeeeee; overflow:hidden;width:95%%; padding: 1px 20px; margin-right:30px;display:inline-block;display:-moz-inline-box;font-weight:bold; ">\n\
      <pre>%s</pre>\n\
     </div>\n\
     <div id="div1" style="background:#eeeeee; overflow:hidden;width:95%%; padding: 1px 20px; margin-right:30px;display:inline-block;display:-moz-inline-box;font-weight:bold; ">\n\
      %s\n\
     </div>\n\n' %(cov,pos,seq)
     
    # alignement
    res += '\
      <div id="div2" class="alignment">\n\
       <pre>\n'
    prem = 0
    for c in cles:
        tag = 0
        for cpt, s in enumerate(dico[c]['seq']):
            av   = "-" * (int(c) - 1 )
            ap   = "-" * (len(seq) - int(c) - len(s) + 1 )
            ali  = av+s+ap
            pdbA = dico[c]['pdb'][cpt]
            pdb  = pdbA[:-1]
            if prem == 0:
                res += '<a name="p1" class=\"blo\" href=\'http://pdb.rcsb.org/pdb/explore.do?structureId='+pdb+'\' target=\"_blank\">>'+pdbA+'</a>\n'
                prem = 1
            else:
                res += '<a class=\"blo\" href=\'http://pdb.rcsb.org/pdb/explore.do?structureId='+pdb+'\' target=\"_blank\">>'+pdbA+'</a>\n'
            #res += "<span class=\"blo\">>" + dico[c]['pdb'][cpt]+"</span>\n"
            for i,let in enumerate(ali):
                if let != seq[i]:
                    ali = ali[:i] + ali[i].lower() + ali[i+1:]
            
            if int(c)%10 == 0 and tag == 0:
                tag = 1
                sRef = ali.replace("-", '')
                res += ali.replace(sRef, '<span id="p'+ c + '">' + sRef + '</span>') + "\n"
            else:
                res += ali +"\n"

    res += '\n\
       </pre>\n\
      </div>'

    return res

def resHtml( div_align = "", pdb = "" ):
    entete = '\
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n\
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >\n\
  <head>\n\n\
  <title> SAProf results</title>\n\n\
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />\n\n\
   <script src="/portal/applets/jmol/Jmol.js"></script>\n\
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>\n\
   <script language="javascript" type="text/javascript">\n\
\n\
function getPositionLeft (obj) {\n\
		var curleft = 0;\n\
		if (obj.offsetParent) {\n\
			curleft = obj.offsetLeft;\n\
			while (obj = obj.offsetParent) {curleft += obj.offsetLeft;}\n\
		}\n\
		return curleft;\n\
	}\n\
\n\
	function getPositionTop (obj) {\n\
		var curtop = 0;\n\
		if (obj.offsetParent) {\n\
			curtop = obj.offsetTop;\n\
			while (obj = obj.offsetParent) {curtop += obj.offsetTop;}\n\
		}\n\
		return curtop;\n\
	}\n\
\n\
function bouge(id_elem){\n\
\n\
    var t = getPositionTop(document.getElementById(id_elem));\n\
    var l = getPositionLeft(document.getElementById(id_elem));\n\
\n\
    var t2 = getPositionTop(document.getElementById("div2"));\n\
    var l2 = getPositionLeft(document.getElementById("div2"));\n\
\n\
    var t3 = t - t2 - 25;\n\
    var l3 = l - l2 - 25;\n\
    //alert( t3 + " " + l3); \n\
    $("#div2").scrollTop(t3);\n\
    $("#div2").scrollLeft(l3);\n\
}\n\
\n\
$(document).ready(function()\n\
{\n\
$("#div0").scroll(function () { \n\
        $("#div1").scrollLeft($("#div0").scrollLeft());\n\
    });\n\
$("#div1").scroll(function () { \n\
        $("#div0").scrollLeft($("#div1").scrollLeft());\n\
    });\n\
$("#div0").scroll(function () { \n\
        $("#div2").scrollLeft($("#div0").scrollLeft());\n\
    }); \n\
$("#div2").scroll(function () { \n\
        $("#div0").scrollLeft($("#div2").scrollLeft());\n\
    });\n\
$("#div1").scroll(function () { \n\
        $("#div2").scrollLeft($("#div1").scrollLeft());\n\
    });\n\
$("#div2").scroll(function () { \n\
        $("#div1").scrollLeft($("#div2").scrollLeft());\n\
    });\n\
});\n\n\n\
$(document).ready(function($){\n\
    rocket     = $("a.blo");\n\
\n\
    fixedLimit = rocket.offset().left;\n\
\n\
    $("#div2").trigger("scrollLeft");\n\
\n\
    $("#div2").scroll(function(){\n\
        windowScroll = $("#div2").scrollLeft() + fixedLimit;\n\
\n\
        if( windowScroll > fixedLimit){\n\
            rocket.css("marginLeft", $("#div2").scrollLeft());\n\
        }else{\n\
            rocket.css("marginLeft", 0);\n\
        }\n\
    });\n\
});\n\
var wireframe="off";\n\
var spacefill="off";\n\
var trace="off";\n\
var cartoons="on";\n\
var wireframe="off";\n\
var ribbons="off";\n\
var backbone="off";\n\
\n\
function loadMol(File) {\n\
  document.jmol.script("load "+File+";select all; wireframe "+wireframe+";spacefill "+spacefill+";cartoons "+cartoons+";trace "+trace+";ribbons "+ribbons+";backbone "+backbone);\n\
  document.jmol.script("select all; color property temperature \'bgyor\'");\n\
}\n\
\n\
function setRepMode(Cartoons, Spacefill, Wireframe, Ribbons, Backbone) {\n\
  cartoons=Cartoons;\n\
  spacefill=Spacefill;\n\
  wireframe=Wireframe;\n\
  ribbons=Ribbons;\n\
  backbone=Backbone;\n\
  document.jmol.script("select all; wireframe "+wireframe+"; spacefill "+spacefill+"; ribbons "+ribbons+"; cartoons "+cartoons+"; backbone "+backbone);\n\
}\n\
   </script>\n\n\
   <style type="text/css">\n\
     <!-- \n\
         a {\n\
            color:#222222;\n\
            text-decoration:none;\n\
            font-weight: bold;\n\
           }\n\
         a:hover {\n\
            color:#874C4;\n\
            text-decoration:none;\n\
                 }\n\
         #container {\n\
            position: absolute;\n\
            top: 25px;\n\
            left: 15%;\n\
            width: 70%;\n\
            #height: 95% ;\n\
            padding: 20px;\n\
            background: #ffffff;\n\
                    }\n\
         .alignment{\n\
            width: 95%;\n\
            height: 600px;#40%;\n\
            padding: 1px 20px;\n\
            background: #eeeeee;\n\
            overflow: auto;\n\
                   }\n\
         #title{\n\
             background:#eeeeee;\n\
             width:95%;\n\
             padding:1px 20px;\n\
               }\n\
         .blo{\n\
             ;\n\
         }\n\
         .ct1 {\n\
            position:relative;\n\
            width: 70%;\n\
            padding-left: 20px;\n\
            padding-top: 30px;\n\
            margin: 5 px;\n\
            font-weight: bold;\n\
         .ct2 {\n\
            position:relative;\n\
            width: 70%;\n\
            padding: 20px;\n\
            margin: 5 px;\n\
            background: #ffffff;\n\
         }\n\
         pre, p { \n\
            margin:0;\n\
            padding:0;\n\
           }\n\
     --> \n\
   </style>\n\n\
  </head>\n\n\
   <body style="font-family:monospace;font-size:11px;line-height:18px;">\n\n\
    <div id="container">\n\
     <div class="ct1">\n\
      <h1> SAProf Results </h1>\n\
     </div>\n\n' 
    
    fin = '\n\
    </div>\n\
   </body>\n\n\
 </html>'
    
    # image nombre de hits par position dans la sequence
    imgHits = '\n\
     <div id="title">\n\
      <h2>Number of residues for each position</h2>\n\
     </div>\n\
     <div style="background:#eeeeee;width:95%;padding:5px 20px;">\n\
      <img src="./Graphe_hits.png" alt="Graphe_imgHits" style="border: 2px solid white;height:40%;width:90%;margin:0 auto;display:block;">\n\
     </div>\n\
     <br />\n\n'
    
    # si on a un pdb on lance jmol
    if pdb != "":
        imgPdbToHtml = '\n\
     <div id="title">\n\
      <h2>PDB '+os.path.basename(pdb)+'</h2>\n\
     </div>\n\
     <div style="background:#eeeeee;width:95%;padding:5px 20px;">\n\
      <table style="font-family:monospace;font-size:11px;line-height:18px;margin:auto;">\n\
       <tr>\n\
        <td>\n\
         <applet name="jmol" code="JmolApplet" archive="JmolApplet.jar" codebase="/portal/applets/jmol" width="650" height="500">\n\
          <param name="progressbar" value="true"/>\n\
          <param name="load" value="'+os.path.basename(pdb)+'Bfact"/>\n\
          <param name="script" value="select all; wireframe off; spacefill off; ribbons off; cartoons on; backbone off; color property temperature \'bgyor\'"/>\n\
         </applet>\n\
        </td>\n\
\n\
        <td>\n\
         <form autocomplete="off">\n\
          <table style="font-family:monospace;font-size:11px;line-height:18px;">\n\
           <tr>\n\
            <td>  \n\
             Display:<br/> \n\
             <input type="radio" name="display" value="Cartoon" onClick=\'setRepMode("on", "off","off","off","off")\' id="b_cartoon" CHECKED><label for="b_cartoon">Cartoon</label></input><br/> \n\
             <input type="radio" name="display" value="Balls & sticks" onClick=\'setRepMode("off",150,75,"off","off")\' id="b_ballsticks"><label for="b_ballsticks">Balls & Sticks</label></input><br/>\n\
             <input type="radio" name="display" value="Spheres" onClick=\'setRepMode("off", "on","off","off","off")\' id="b_spheres"><label for="b_spheres">Spheres</label></input><br/> \n\
             <input type="radio" name="display" value="Wireframe" onClick=\'setRepMode("off", "off","0.15","off","off")\' id="b_wireframe"><label for="b_wireframe">Wireframe</label></input><br/> \n\
             <input type="radio" name="display" value="Ribbons" onClick=\'setRepMode("off", "off","off","on","off")\' id="b_ribbons"><label for="b_ribbons">Ribbons</label></input><br/> \n\
             <input type="radio" name="display" value="Backbone" onClick=\'setRepMode("off", "off","off","off","0.25")\' id="b_backbone"><label for="b_backbone">Backbone</label></input><br/> \n\
             <br/>\n\
            </td>\n\
           </tr>\n\
          </table>\n\
         </form>\n\
        </td>\n\
\n\
       </tr>\n\
      </table>\n\
     </div>\n\
\n\
     <br />\n\n'#+os.path.basename(pdb)+'Bfact"/>\n\
        to_write = entete + imgPdbToHtml + imgHits + div_align + fin
    else:
        to_write = entete + imgHits + div_align + fin
    
    f = open( "saprof.html", "wt" )
    f.write( to_write )
    f.close()

def main( argv = [] ):
    
    # Arguments
    rosettaFile = argv[1]
    seq         = argv[2]
    pdb         = ""
    if len(argv) > 3 :
        pdb     = argv[3]
    
    print rosettaFile
    print seq
    print pdb
    print 

    resDict      = parseRosFile( rFile = rosettaFile )
    
    pos, nb_hits = create_Rscript( seq, resDict )
    
    if pdb != "":
        add_col_by_Bfact( npdb = pdb, nb_hits = nb_hits )
    
    divAli = aliToHtml( dico = resDict, seq = seq, nb_hits = nb_hits )
    resHtml( divAli, pdb )


## Main
if __name__ == "__main__":
    
    main(sys.argv)
    sys.exit()
    
