#!/usr/bin/env python
"""
../bin/pval 3h8vA.xyz ../calibration.Ids 10 20 3 0.0005 3h8vA.cal

../PyGLSearch/GLSearch.py --ipdb 3h8vA --from 10 --to 20 --idList list-1.75.combo  --ipval 3h8vA.cal --dbPath /nfs/freetown/banks/GLSearch/Astral-1-75 --np 8 --binPath ../src/GLSearchP --engine GLSearchP --gsc 0.0001 --lsc 0.0001 --len 20 --out GLSearch.out --olog GLSearch.log  -v 2> /dev/null > 3h8vA-0.0001.res
../Tools/PileUpMatches.py 3h8vA-0.0001.res

../PyGLSearch/GLSearch.py --ipdb 3h8vA --from 10 --to 20 --idList list-1.75.combo  --ipval 3h8vA.cal --dbPath /nfs/freetown/banks/GLSearch/Astral-1-75 --np 8 --binPath ../src/GLSearchP --engine GLSearchP --gsc 0.001 --lsc 0.001 --len 20 --out GLSearch.out --olog GLSearch.log  -v 2> /dev/null > 3h8vA-0.001.res
../Tools/PileUpMatches.py 3h8vA-0.001.res

../PyGLSearch/GLSearch.py --ipdb 3h8vA --from 10 --to 20 --idList list-1.75.combo  --ipval 3h8vA.cal --dbPath /nfs/freetown/banks/GLSearch/Astral-1-75 --np 8 --binPath ../src/GLSearchP --engine GLSearchP --gsc 0.01 --lsc 0.01 --len 20 --out GLSearch.out --olog GLSearch.log  -v 2> /dev/null > 3h8vA-0.01.res
../Tools/PileUpMatches.py 3h8vA-0.01.res

target=2lzmA
ffrom=30
tto=40
../PyGLSearch/GLSearch.py --ipdb $target --from $ffrom --to $tto --lhw 2 --idList Astral-1.75-clusters70.clean.exp.X-RAY-2.0_0.2.Ids  --dbPath /nfs/freetown/banks/GLSearch/Astral-1-75 --cal_idList Astral-1.75-clusters70.clean.exp.X-RAY-2.0_0.2.Ids.cal --np 8 --binPath ../src/GLSearchP --engine GLSearchP --gsc 0.0001 --lsc 0.0001 --out GLSearch.out --olog GLSearch.log  -v --addScopIds --ipval $target-$ffrom-$tto.cal 2> /dev/null > $target-$ffrom-$tto.res # --nopval 

export target=7ahlA
ffrom=40
tto=55
../PyGLSearch/GLSearch.py --ipdb $target --from $ffrom --to $tto --lhw 2 --idList Astral-1.75-clusters70.clean.exp.X-RAY-2.0_0.2.Ids  --dbPath /nfs/freetown/banks/GLSearch/Astral-1-75 --cal_idList Astral-1.75-clusters70.clean.exp.X-RAY-2.0_0.2.Ids.cal --np 8 --binPath ../src/GLSearchP --engine GLSearchU --gsc 0.75 --lsc 0.5 --out $target-$ffrom-$tto.res --olog $target-$ffrom-$tto.log  -v --addScopIds --ipval /nfs/freetown/user/tuffery/prgs/PyGLSearch/src/GLSearchP/PNdist
../Tools/PileUpMatches.py  $target-$ffrom-$tto.res


../Tools/PileUpMatches.py $target-$ffrom-$tto.res

Out is:
name1 name2 qfrom qto tfrom tto RMSd Gsc GPVal LSc LPval bc bcd

"""
import sys, os

# Modifier ici !!!
from Config import *
import Fasta


import PyPDB.PyPDB as PDB
from PyBestFit.PyBestFit import *

def superimpose(ref, mob):
    """
    @param ref: a PDB instance
    @param mod: a PDB instance (same length as ref)
    @return   : mod superimposed on ref
    """
    bf = BestFit.bestFit()
    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(ref,mob, outFit = False)
    rs = bf.PDBTM(mob, TM)
    return rs



def processLineForPir(line, qLen, dbPATH = DFLT_XYZ_FILE_PATH, step = None, verbose = 0):
    """
    @param line: a line on the usual GLSearch, SAProf format: query, match, qfrom, qto, mfrom, mto, etc
                 NOTE: matches are in SA positions, hence qTo and mTo are + 3 for amino acids
    @param qLen: length of the query sequence.
    @param dbPATH : the path to the ddirectory where the .xyz, .fst, .hmmpdb are stored.
    @param step   : if specified split sequence each step chars (80) otherwise full sequence on one line
    @return    : a list of pir formatted 3 lines, with the match aligned to the query (ungapped).
    """
    it = line.split()
    if not len(it):
        return
    qfrom = int(it[2])
    qto   = int(it[3]) + 3
    match = dbPATH+"/"+it[1]+".fst"
    name  = os.path.basename(it[1])
    score = float(it[-1])
    try:
        pos = namee.index(".")
        name = name[:pos]
    except:
        pass
    mfrom = int(it[4])
    mto   = int(it[5]) + 3
    seq   = Fasta.fasta(match)
    aaSeq = seq[seq.data.keys()[0]].data["s"]
    aaSeq = aaSeq.replace(".","")
    matchSeq = aaSeq[mfrom-1:mto]
    alnSeq = ("-"*(qfrom-1))+matchSeq+("-"*(qLen - qto))
    # Now we must format as PIR.
    rs = [">P1;%s" % name]
    rs.append("structure:%s:%s : %s :%s : %s :Unknown :SAFrag:%.3f :" % (name, mfrom, name[-1], mto, name[-1], score))
    if not step:
        rs.append("%s" % alnSeq)
    else:
        while len(alnSeq) > 0:
            rs.append("%s" % alnSeq[:step] )
            alnSeq = alnSeq[step:]
    rs.append("*")
    return rs

def processLines(lines, qName, qSeq, dbPATH = DFLT_XYZ_FILE_PATH, step = None, verbose = 0):
    """
    Process a series of lines in the SAProf or GLSearch format
    Return a pir alignment

    @param lines: the matches in thhe QAProf format.
    @param qName: the name of the query
    @param qSeq : the query sequence (full, ungapped), a string.
    @return : a list of lines PIR formatted
    """
    qLen = len(qSeq)
    rs = [">P1;%s" % qName]
    rs.append("sequence:%s:%s :  :%s :  :Unknown :: :" % (qName, 1, qLen))
    if not step:
        rs.append("%s" % qSeq)
    else:
        while len(qSeq) > 0:
            rs.append("%s" % qSeq[:step] )
            qSeq = qSeq[step:]
    rs.append("*")
    for line in lines:
        rs = rs + processLineForPir(line, qLen, dbPATH = dbPATH, verbose = verbose)
    return rs
                  
def matches2pir(matches, opir, qName, qSeq, dbPATH = DFLT_XYZ_FILE_PATH, verbose = 0):
    """
    Process a file of SAProf matches.
    Write a .pir corresponding to the pir alignment of the matches

    @param matches: the file of the matches
    @param opir   : the output pir file name
    @param qLabel : the name of the query
    @param qSeq   : the sequence of the query
    @param step   : if specified split sequence each step chars (80) otherwise full sequence on one line

    @return       : the pir lines
    """
    f = open(matches)
    lines = f.readlines()
    f.close()

    # Sort the lines according to start position
    lines = sorted(lines, key=lambda x: int(x.split()[2]))

    rs = processLines(lines, qName, qSeq, dbPATH = dbPATH, verbose = verbose)
    f= open(opir,"w")
    f.write("\n".join(rs))
    f.write("\n")
    f.close()
    return rs
    
def processLines_old(lines, maxLines = 20, maxSamePos = 2):
    """
    OLD STUFF TO SUPERIMPOSE ON QUERY AS WELL. LEFT FOR INFO.
    """
    isStart = True
    counter = 0
    pml = "pymol query.pdb "
    oqfrom = None
    oqto   = None
    gCounter = 0
    for l in lines[1:]:
        gCounter += 1
        if not (gCounter % 100):
            print gCounter, counter
        it = l.split()
        if not len(it):
            continue
        qfrom = int(it[2])
        qto   = int(it[3])
        # Avoid piling up too many fragments at same position
        if (qfrom != oqfrom) or (qto != oqto):
            oqfrom = qfrom
            oqto = qto
            lCounter = 1
        else:
            lCounter+=1
            if lCounter > maxSamePos:
                continue
        if isStart:
            pdb = it[0].replace(".xyz", ".hmmpdb")
            # pdb = it[0]+".xyz"
            # pdb = pdb.replace(".xyz", ".hmmpdb")
            x = PDB.PDB(pdb)
            ref  =x[qfrom-1: qto]
            ref.out("query.pdb")
            # pml = "pymol query.pdb "
            # isStart = False
        match = DBPATH+"/"+it[1]+".xyz"
        name  = os.path.basename(match)
        mfrom = int(it[4])
        mto   = int(it[5])
    # print match, mfrom, mto
        fst = match.replace(".xyz",".fst")
        pdb = match.replace(".xyz",".hmmpdb")
        f = open(fst)
        lines = f.readlines()
        f.close()
        sclean = lines[1].replace(".","")
        s      = sclean[mfrom-1:mto]
        # print s
        x = PDB.PDB(pdb)
        aaseq = x.aaseq()
        
        # print aaseq
        try:
            aPos=aaseq.index(s)
        except:
            continue
        mob  =x[aPos: aPos+len(s)]
        mob = superimpose(ref, mob)
        oname = "%s.%d-%d.supp.pdb" % (name, mfrom, mto)
        mob.out(oname)
        pml = "%s %s" % (pml, oname)
        counter += 1
        if counter > maxLines:
            break
    return pml

if __name__ == "__main__":
    """
    This takes one match line of GLSearch and superimposes match on query 
    """
    f = open(sys.argv[1])

    matches = sys.argv[1]
    qName = sys.argv[2]
    qSeq  = sys.argv[3]

    opir  = sys.argv[4]
    
    rs = matches2pir(matches, opir, qName, qSeq, verbose = 0)
    print "\n".join(rs)
    print
