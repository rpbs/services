#!/usr/bin/env python
"""
A set of functions to generate a .trj from matches
"""
import sys, os

# Modifier ici !!!
from Config import *
import Fasta


import PyPDB.PyPDB as PDB
from PyBestFit.PyBestFit import *

def superimpose(ref, mob):
    """
    @param ref: a PDB instance
    @param mod: a PDB instance (same length as ref)
    @return   : mod superimposed on ref
    """
    bf = BestFit.bestFit()
    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(ref,mob, outFit = False)
    rs = bf.PDBTM(mob, TM)
    return rs



def processLineForSAPir(line, qLen, dbPATH = DFLT_XYZ_FILE_PATH, step = None, verbose = 0):
    """
    @param line: a line on the usual GLSearch, SAProf format: query, match, qfrom, qto, mfrom, mto, etc
                 NOTE: matches are in SA positions, hence qTo and mTo are + 3 for amino acids
    @param qLen: length of the query sequence.
    @param dbPATH : the path to the ddirectory where the .xyz, .fst, .hmmpdb are stored.
    @param step   : if specified split sequence each step chars (80) otherwise full sequence on one line
    @return    : a list of pir formatted 3 lines, with the SA encoding of the match aligned to the query (ungapped).
    """

    it = line.split()
    if not len(it):
        return
    qfrom = int(it[2])
    qto   = int(it[3]) # Do not add 3 here since SA.
    match = dbPATH+"/"+it[1]+".fst"
    samatch = dbPATH+"/"+it[1]+".hmm27-2"
    name  = os.path.basename(it[1])
    try:
        pos = namee.index(".")
        name = name[:pos]
    except:
        pass
    mfrom = int(it[4])
    mto   = int(it[5]) # Do not add 3 here since SA.
    seq   = Fasta.fasta(match)
    aaSeq = seq[seq.data.keys()[0]].data["s"]
    aaSeq = aaSeq.replace(".","")
    matchSeq = aaSeq[mfrom-1:mto]
    alnSeq = ("-"*(qfrom-1))+matchSeq+("-"*(qLen - qto))

    saseq   = Fasta.fasta(samatch)
    SASeq = saseq[seq.data.keys()[0]].data["s"]
    SASeq = SASeq.replace(".","")
    SAFrom = qfrom - 1 # From 0 for internal representation
    SAmatchSeq = SASeq[mfrom-1:mto]
    alnSASeq = ("-"*(qfrom-1))+SAmatchSeq+("-"*(qLen - qto))
    # Now we must format as PIR.
    rs = [">P1;%s" % name]
    rs.append("structure:%s:%s : %s :%s : %s :Unknown :: :" % (name, mfrom, name[-1], mto, name[-1]))
    if not step:
        rs.append("%s" % alnSASeq)
    else:
        while len(alnSeq) > 0:
            rs.append("%s" % alnSeq[:step] )
            alnSeq = alnSeq[step:]
    rs.append("*")
    print line
    print SAmatchSeq, SAFrom
    return rs, SAmatchSeq, SAFrom

def processLines(lines, qName, qSeq, dbPATH = DFLT_XYZ_FILE_PATH, step = None, verbose = 0):
    """
    Process a series of lines in the SAProf or GLSearch format
    Return a pir alignment

    @param lines: the matches in thhe QAProf format.
    @param qName: the name of the query
    @param qSeq : the query sequence (full, ungapped), a string.
    @return : a list of lines PIR formatted
    """
    
    qLen = len(qSeq)
    trj = []
    for i in range(0,qLen-3):
        trj.append([])
    rs = [">P1;%s" % qName]
    rs.append("sequence:%s:%s :  :%s :  :Unknown :: :" % (qName, 1, qLen))
    if not step:
        rs.append("%s" % qSeq)
    else:
        while len(qSeq) > 0:
            rs.append("%s" % qSeq[:step] )
            qSeq = qSeq[step:]
    rs.append("*")
    for line in lines:
        lrs, SASeq, SAFrom = processLineForSAPir(line, qLen, dbPATH = dbPATH, verbose = verbose)
        for i, letter in enumerate(SASeq):
            if letter not in trj[i+SAFrom]:
                trj[i+SAFrom].append(letter)
        rs = rs + lrs
    return rs, trj
                  
def matches2trj(matches, opir, otrj, qName, qSeq, dbPATH = DFLT_XYZ_FILE_PATH, verbose = 0):
    """
    Process a file of SAProf matches.
    Write a .pir corresponding to the pir alignment of the matches

    @param matches: the file of the matches
    @param opir   : the output pir file name
    @param qLabel : the name of the query
    @param qSeq   : the sequence of the query
    @param step   : if specified split sequence each step chars (80) otherwise full sequence on one line

    @return       : the pir lines
    """
    f = open(matches)
    lines = f.readlines()
    f.close()

    # Sort the lines according to start position
    lines = sorted(lines, key=lambda x: int(x.split()[2]))

    rs, trj = processLines(lines, qName, qSeq, dbPATH = dbPATH, verbose = verbose)
    f= open(opir,"w")
    f.write("\n".join(rs))
    f.write("\n")
    f.close()

    f= open(otrj,"w")
    for i, lset in enumerate(trj):
        f.write("%s %s\n" % ( i+1," ".join(lset)))
    f.close()
    
    return rs, trj

if __name__ == "__main__":
    """
    This takes one match line of GLSearch and superimposes match on query 
    """
    f = open(sys.argv[1])

    matches = sys.argv[1]
    qName   = sys.argv[2]
    qSeq    = sys.argv[3]
    dbPATH  = sys.argv[4]
    opir    = sys.argv[5]
    otrj    = sys.argv[6]

    
    rs, trj = matches2trj(matches, opir, otrj, qName, qSeq, dbPATH = dbPATH, verbose = 0)
    print "\n".join(rs)
    print
    for i, lset in enumerate(trj):
        print i," ".join(lset),"\n" 
