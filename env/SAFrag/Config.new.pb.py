#!/usr/bin/env python

SAPROF_ROOT="/nfs/freetown/user/tuffery/prgs/SAProf"
# SAPROF_ROOT="/usr/local/SAProf/"
# SAPROF_ROOT="/nfs/freetown/user/shen/PP-Search/scripts/ProfAnalyseV4/"

SAPROF_BIN_PATH="%s/bin/" % SAPROF_ROOT
# Chemin vers les scripts python
PATH_SCRIPTS="%s/SAProf-cluster2/SAProf/SAProf" % SAPROF_ROOT
PATH_SCRIPTS="%s/SAProf-cluster2/SAProf/scripts" % SAPROF_ROOT
PATH_SCRIPTS="%s/SAProf/scripts" % SAPROF_ROOT
SAPROF_SCRIPT_PATH=PATH_SCRIPTS
SAPROF_LIB="%s/data" % SAPROF_ROOT

LISTS = {
     "25" : { "path": "%s/PDB25ok/" % SAPROF_LIB,
              "list" : 
              "PDB25_HHfragBD_chains4649_frag5964.novSVMBlast.prob.ids",
              "xyzpath": "%s/PDB25ok/" % SAPROF_LIB,
              "xyzlist": "PDB25_HHfragBD_chains4649_frag5964.ids"
              },

     "50" : { "path": "%s/PDB50/" % SAPROF_LIB,
              "list" :
"cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13575.novSVMBlast.prob.ids",
              "xyzpath": "%s/PDB50/" % SAPROF_LIB,
              "xyzlist":  "cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13575.ids"
              },

# pour novNovNovSVMmodel 5/5 Jeux (currant est sur 4/5 Jeux)             
#     "25svm" : { "path": "%s/PDB25ok/" % SAPROF_LIB,
#              "list" :
#"PDB25_HHfragBD_chains4649_frag5964.novNovNovSVMBlast.prob.ids",
#              "xyzpath": "%s/PDB25ok/" % SAPROF_LIB,
#              "xyzlist": "PDB25_HHfragBD_chains4649_frag5964.ids"
#              },
#
#     "50svm" : { "path": "%s/PDB50/" % SAPROF_LIB,
#              "list" :
#"cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13575.novNovNovSVMBlast.prob.ids",
#              "xyzpath": "%s/PDB50/" % SAPROF_LIB,
#              "xyzlist":  "cullpdb_pc50_res2.0_R0.25_d120413_chains10114_frag13575.ids"
#              },
}

DFLT_PROB_FILE_LIST = "PDB25_HHfragBD_4649ids_5972Frag_newSVMBlast.prob.ids2" 
DFLT_PROB_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB
DFLT_XYZ_FILE_LIST = "PDB25_HHfragBD_4649ids_5972Frag.ids2"
DFLT_XYZ_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB

# DFLT_PROB_FILE_LIST = "PDB25_HHfragBD_chains4649_frag5964.novSVMBlast.prob.ids" 
# DFLT_PROB_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB
# DFLT_XYZ_FILE_LIST = "PDB25_HHfragBD_chains4649_frag5964.ids"
# DFLT_XYZ_FILE_PATH = "%s/PDB25ok/" % SAPROF_LIB

DFLT_SAPROF_FROM   = 1
DFLT_SAPROF_TO     = -1


DFLT_WPATH         = "."
DFLT_TMPPATH       = "/scratch"   #!!!
DFLT_PROGRESS_FILE = ".progress.txt"
DFLT_SAPROF_LABEL  = "SA-PROF"
DFLT_SAPROF_NP     = 100
DFLT_SAPROF_OUTPUT = ".pir"


SAPROF_MODES       = ["AllFast","AllComplet","FiltComplet","FiltFast"]
DFLT_SAPROF_MODE   = "FiltFast"

# threshold DisJS: old(3~8% Err), 3PC(3% Err), ...
DISJS_THRESHOLDS     = ["old","3PC","4PC","5PC","5PC2","8PC","10PC"]
DFLT_DISJS_THRESHOLD = "5PC"


# si utilise fst comme entree.(not prob file), choisir le version de blast et svmModel
SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #Dflt novSVMmodel
#SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl0Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" #novNovNovSVMmodel

BLAST="/nfs/freetown/banks/biomaj/blastdb"  #novBlast
#BLAST="" #pour blast2006 ??? c'est quoi?

# Do not modify 
#PPP_SVM_MODEL="pc50_res2.5_R0.25_chains7059.random.Cl1Cl2Cl3Cl4.svmi8.unbias8000.model" # ??? n'est pas utilise?
TAB_LENFRAG=[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
MODE="FiltFast"
MAXRUN=500


# C'est quoi? 
REMOTE_SERVER=False
