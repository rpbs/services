#!/usr/bin/env python

######
# PICORD Geraldine
# 06/08/2012
#
# Translate pir file into hhfrag result file
######


## Imports
import sys, os, time, shutil
import glob
import PyPDB.PyPDB as PDB


## Fonctions
def removeD( dirName ):
    """
    Clean remove a directory.

    @param dirName: input directory name
    """
    
    if os.path.isdir( dirName ):
        files = glob.glob( dirName + "/*")
        if files != []:
            for f in files:
                os.remove(f)
        os.rmdir( dirName )

def createD( dirName, where="." ):
    """
    Clean create a directory.

    @param dirName: input directory name
    @param where  : path in which to create a new directory
    """
    
    path = where + "/" + dirName

    if not os.path.isdir(path):
        try:
            os.mkdir(path)
        except Exception, err:
            print err

def readPir(fName):
    f = open(fName)
    lines = f.readlines()
    f.close()
    
    pirs = []
    lseq = None
    
    for l in lines:
        if l[0] == ">":
            if lseq is not None:
                lpir.append(lseq)
                pirs.append(lpir)
            id = l[4:].split()[0]
            lpir = [l[:-1]]
            lseq = ""
            status = 0
        elif not status:
            status = 1
            lpir.append(l[:-1])
        else:
            lseq = "%s%s" % (lseq,"".join(l.split()))
    
    lpir.append(lseq)
    pirs.append(lpir)
    return pirs


def sortPirs(pirs):
   spirs = sorted(pirs, key=lambda x: x[2].index(x[2].replace("-","")[0]))
   return spirs


def nb_gap( seq ):
    nb = 0
    for i in seq:
        if i == "-":
            nb += 1
        else:
            break
    
    return nb

def recup_pos( Npdb, Spdb, bank ):
    try:
        mon_pdb = PDB.PDB( bank + "/" + Npdb + ".hmmpdb" )
    except:
        print "%s not found in our database. Removed from final results..." %Npdb
        return [None,None]
    
    seq_pdb = mon_pdb.aaseq()
    
    try:
        deb = seq_pdb.index( Spdb )
        fin = deb + len( Spdb )
        return (deb, fin)
    except:
        return [None,None]

def recup_angle( Npdb, deb, fin, bank ):
    
    createD( "Temp", "." )
    createD( "PDB" , "." )
    createD( "PPX" , "." )

    try:
        mon_pdb = PDB.PDB( bank + "/" + Npdb + ".hmmpdb" )
    except:
        print "%s not found in our database. Removed from final results..." %Npdb
        return None
    
    # gestion des bords de pdb
    ref_deb = deb
    ref_fin = fin
    tagF = 0
    tagD = 0
    
    if fin >= len(mon_pdb) or not mon_pdb[fin].findAtm(atmName = "CA"):
        tagF = 1
        fin = fin - 1
    if (deb-1) < 1 or not mon_pdb[deb-1].findAtm(atmName = "CA"):
        tagD = 1
        deb = deb + 1
        
    mon_pdb.clean()
    mon_pdb[deb-1:fin+1].out("Temp/%s_%d_%d.tpdb" %(Npdb,ref_deb,ref_fin))
    mon_pdb[ref_deb:ref_fin].out("PDB/%s_%d_%d.pdb" %(Npdb,ref_deb,ref_fin))

    cmd = "drun safrag PDBMac -ipdb Temp/%s_%d_%d.tpdb -oppx PPX/%s%d_%d.ppx -noH -pdbnum 2>> Pir2Rose.log" %(Npdb,ref_deb,ref_fin,Npdb,ref_deb,ref_fin)

    os.system(cmd)

    if os.path.isfile("PPX/%s%d_%d.ppx" %(Npdb,ref_deb,ref_fin)):
        f = open("PPX/%s%d_%d.ppx" %(Npdb,ref_deb,ref_fin), "rt")
        lines = f.readlines()
        if not tagD and not tagF:
            lines = lines[1:-1]
        elif tagF and not tagD:
            lines = lines[1:]
        elif tagD and not tagF:
            lines = lines[:-1]
    
        f.close()
    
        res = [ (l[12:18].strip(), l[19:25].strip(), l[26:32].strip()) for l in lines if (l[12:18].strip() != "----" and  l[19:25].strip() != "----" and l[26:32].strip() != "----")]

        removeD( "Temp/" )
        removeD( "PPX/" )

        if len(res) == (ref_fin - ref_deb):
            return res
        else:
            return None
    else:
        return None
    

def recup_infos( label, tab_pir = [], seq = "", bank = "" ):
    
    dico = {}
    
    for s in range(len(seq)):
        dico[s + 1] = {}
        dico[s + 1]["nom"] = []
        dico[s + 1]["seq"] = []
        dico[s + 1]["angle"] = []
        dico[s + 1]["score"] = []
        dico[s + 1]["pos"] = []
    
    # permet de compter les sequence eliminees
    for elem in tab_pir:
        deb, fin = recup_pos( elem[0][4:].rstrip(), elem[-1].replace("-", "").replace("*",""), bank )
        if deb != None:
            angle = recup_angle(elem[0][4:].rstrip() , deb, fin, bank )
            if angle != None:
                dico[nb_gap( elem[-1] ) + 1]["nom"].append(elem[0][4:].rstrip())
                dico[nb_gap( elem[-1] ) + 1]["seq"].append(elem[-1].replace("-", "").replace("*",""))
                dico[nb_gap( elem[-1] ) + 1]["pos"].append(deb+1)
                dico[nb_gap( elem[-1] ) + 1]["angle"].append(angle)
                dico[nb_gap( elem[-1] ) + 1]["score"].append(float((elem[1].split(":"))[-2]))
    
    cles = dico.keys()
    
    # generation du fichier au format rosetta
    f = open(label + ".saprof.09", "wt")
    for i in cles:
        if len(dico[i]["nom"]) > 0:
            f.write( " position: %11d neighbors: %11d\n\n" %( i, len(dico[i]["nom"])) )
            for cptn, n in enumerate(dico[i]["nom"]):
                for cpt, s in enumerate(dico[i]["seq"][cptn]):
                    if cpt == 0:
                        f.write(" %s %s %5d %s L %8.3f %8.3f %8.3f %8.3f\n" %( n[:4], n[4], dico[i]["pos"][cptn], s, float(dico[i]["angle"][cptn][cpt][0]), float(dico[i]["angle"][cptn][cpt][1]), float(dico[i]["angle"][cptn][cpt][2]), dico[i]["score"][cptn]) )
                    else:
                        f.write(" %s %s %5d %s L %8.3f %8.3f %8.3f %8.3f\n" %( n[:4], n[4], dico[i]["pos"][cptn]+cpt, s, float(dico[i]["angle"][cptn][cpt][0]), float(dico[i]["angle"][cptn][cpt][1]), float(dico[i]["angle"][cptn][cpt][2]), dico[i]["score"][cptn]) )
                    
                f.write("\n")
    f.close()
    
    if os.path.isdir("PDB/"):
        cmd = "tar -czf PDB.tar.gz PDB/"
        os.system(cmd)

def main(pirF="", label="", bank=""):
    # Arguments
    #pirF = sys.argv[1]
    #label = sys.argv[2]
    
    # ouverture du fichier pir, on enleve la sequence query
    all_pirs = readPir( pirF )
    Qpir = all_pirs[0]
    tab_pirs = all_pirs[1:]
    tab_pirs = sortPirs( tab_pirs )

    # creation du fichier saprof.09 compatible avec rosetta
    recup_infos( label, tab_pirs, Qpir[-1].replace("*",""), bank )
    
########################################################################
#
#
#
########################################################################
## MAIN
if __name__ == "__main__":
    
    main(sys.argv[1],sys.argv[2], sys.argv[3])
    
