#!/usr/bin/env python

# import PyGreedy.PyGreedy

# ------------------------- Globals
VERSION = "1.0"
# -------------------------

DOCKER_IMG         = "ligcsr"
BABEL_DOCKER_IMG  = "opendocking-dev"
CSRRE_ROOT         = "/usr/local/LigCSRre"
LIGCSR_DEMO        = "/services/env/PyLigCSR/demo"
DFLT_DEMO_MOL2     = "CDK2.xray.inh1.1E9H.mol2"

# LigCSRre parameters
DFLT_CSR_NITER     = 1500
DFLT_CSR_NBEST     = 1000
DFLT_CSR_RULE_FILE = "/usr/local/LigCSRre/lib/ligrules2.txt"
DFLT_CSR_MINAT     = 5
DFLT_CSR_DMAX      = 2.
DFLT_CSR_RMSD      = 1.
DFLT_CSR_TOL       = 1.8
DFLT_CSR_LABEL     = "CSRre"
DFLT_CSR_PDB_FILE  = "%s.pdb" % DFLT_CSR_LABEL


BABEL_BIN          = "babel" # "/usr/bin/babel"

DFLT_CSR_BANK_PATH = "/scratch/banks/Drugs"


# The name of the splits of the banks. We do not append .mol2 here. We have to add on command line
splitBanks = {
    "diverse-testset" : ["diverse-testset-X50-part1", "diverse-testset-X50-part2", "diverse-testset-X50-part3", "diverse-testset-X50-part4", "diverse-testset-X50-part5", "diverse-testset-X50-part6", "diverse-testset-X50-part7", "diverse-testset-X50-part8", "diverse-testset-X50-part9", "diverse-testset-X50-part10"],
    "dvs-50" : ["dvs_u_ID_1.3D.X50.cpb", "dvs_u_ID_2.3D.X50.cpb", "dvs_u_ID_3.3D.X50.cpb", "dvs_u_ID_4.3D.X50.cpb", "dvs_u_ID_5.3D.X50.cpb", "dvs_u_ID_6.3D.X50.cpb", "dvs_u_ID_7.3D.X50.cpb", "dvs_u_ID_8.3D.X50.cpb", "dvs_u_ID_9.3D.X50.cpb", "dvs_u_ID_10.3D.X50.cpb", "actives.3D.denovo.X50", "actives_Xray_with_correct_H"],
    "dvs-50-2" : ["actives.3D.denovo.X50", "actives_Xray_with_correct_H"],
    "dvs-50-da" : ["dvs_u_ID_1.3D.X50.cpb.dah", "dvs_u_ID_2.3D.X50.cpb.dah", "dvs_u_ID_3.3D.X50.cpb.dah", "dvs_u_ID_4.3D.X50.cpb.dah", "dvs_u_ID_5.3D.X50.cpb.dah", "dvs_u_ID_6.3D.X50.cpb.dah", "dvs_u_ID_7.3D.X50.cpb.dah", "dvs_u_ID_8.3D.X50.cpb.dah", "dvs_u_ID_9.3D.X50.cpb.dah", "dvs_u_ID_10.3D.X50.cpb.dah", "actives.3D.denovo.X50.dah", "actives_Xray_with_correct_H.dah"],
#    "dvs-100" : ["dvs_u_ID_1.3D.X100.cpb", "dvs_u_ID_2.3D.X100.cpb", "dvs_u_ID_3.3D.X100.cpb", "dvs_u_ID_4.3D.X100.cpb", "dvs_u_ID_5.3D.X100.cpb", "dvs_u_ID_6.3D.X100.cpb", "dvs_u_ID_7.3D.X100.cpb", "dvs_u_ID_8.3D.X100.cpb", "dvs_u_ID_9.3D.X100.cpb", "dvs_u_ID_10.3D.X100.cpb", "actives.3D.denovo.X100", "actives_Xray_with_correct_H"],
    "dvs-100" : ["dvs_u_ID_1.3D.X100.cpb_1","dvs_u_ID_1.3D.X100.cpb_2","dvs_u_ID_2.3D.X100.cpb_1","dvs_u_ID_2.3D.X100.cpb_2","dvs_u_ID_3.3D.X100.cpb_1","dvs_u_ID_3.3D.X100.cpb_2","dvs_u_ID_4.3D.X100.cpb_1","dvs_u_ID_4.3D.X100.cpb_2","dvs_u_ID_5.3D.X100.cpb_1","dvs_u_ID_5.3D.X100.cpb_2","dvs_u_ID_6.3D.X100.cpb_1","dvs_u_ID_6.3D.X100.cpb_2","dvs_u_ID_7.3D.X100.cpb_1","dvs_u_ID_7.3D.X100.cpb_2","dvs_u_ID_8.3D.X100.cpb_1","dvs_u_ID_8.3D.X100.cpb_2","dvs_u_ID_9.3D.X100.cpb_1","dvs_u_ID_9.3D.X100.cpb_2","dvs_u_ID_10.3D.X100.cpb_1","dvs_u_ID_10.3D.X100.cpb_2", "actives.3D.denovo.X100", "actives_Xray_with_correct_H.dah"],
    "dvs-100-2" : ["actives.3D.denovo.X100", "actives_Xray_with_correct_H"],
    "dud-50" : ["dud_cdk2.X50.cpb_1","dud_cdk2.X50.cpb_2", "dud_cdk2.X50.cpb_3", "dud_cdk2.X50.cpb_4", "dud_cdk2.X50.cpb_5", "dud_cdk2.X50.cpb_6", "dud_cdk2.X50.cpb_7", "dud_cdk2.X50.cpb_8", "dud_cdk2.X50.cpb_9","dud_cdk2.X50.cpb_10", "dud_cdk2.X50.cpb_11",  "dud_cdk2.X50.cpb_12"],

# These adopt pbsdsh formalism (symbolic links) NUMBERS MUST START FROM 1
    "dvs-50-2"   : ["actives.3D.denovo.part1", "actives.3D.denovo.part2"],
    "dvs2880"    : ["dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb.part1","dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb.part2","dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb.part3","dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb.part4","dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb.part5"],
    "CPDBAS"     : ["CPDBAS_v5d_1547_20Nov2008.babel_formatted.X50.cpb.part1", "CPDBAS_v5d_1547_20Nov2008.babel_formatted.X50.cpb.part2","CPDBAS_v5d_1547_20Nov2008.babel_formatted.X50.cpb.part3"],
    "DrugBank"   : ["small_molecule.non-toxic.X50.cpb.part1", "small_molecule.non-toxic.X50.cpb.part2"],
    "Withdrawn"  : ["withdrawn_formatted.babel.FAF_soft.X50.cpb.part1"],
    "FDAApproved": ["approved_formatted.babel.FAF_soft.X50.cpb.part1"],
    "Analgesic"  : ["Anagesic.part1","Anagesic.part2","Anagesic.part3","Anagesic.part4","Anagesic.part5"],
    "Antibacterial" : ["Antibacterial.part1","Antibacterial.part2","Antibacterial.part3","Antibacterial.part4","Antibacterial.part5"],
    "Anticancer" : ["Anticancer.part1","Anticancer.part2","Anticancer.part3","Anticancer.part4","Anticancer.part5"],
    "CNS"        : ["CNS.part1","CNS.part2","CNS.part3","CNS.part4","CNS.part5"],
    "GPCR"       : ["GPCR.part1","GPCR.part2","GPCR.part3","GPCR.part4","GPCR.part5"],
    "Ion-Channels": ["Ion-Channel.part1","Ion-Channel.part2","Ion-Channel.part3","Ion-Channel.part4","Ion-Channel.part5"],
    "Kinase"     : ["Kinase.part1","Kinase.part2","Kinase.part3","Kinase.part4","Kinase.part5"],
    "Chamberlain": ["Chamberlain.part1","Chamberlain.part2","Chamberlain.part3","Chamberlain.part4","Chamberlain.part5"],
    # MJ Formation
    "formation": ["10-CDK2-Actives-dvs.2880.filtered.3D.X50.part1"],
    }

# Prefix does not include .mol2  (must add to bank name in LigCSRre call)
arrayBankPrefix = {
    "dvs-50-2"     : "actives.3D.denovo",
    "dvs2880"      : "dvs_u_sdf.filtered_bruno_formatted.subset76.X50.cpb",
    "CPDBAS"       : "CPDBAS_v5d_1547_20Nov2008.babel_formatted.X50.cpb",
    "DrugBank"     : "small_molecule.non-toxic.X50.cpb",
    "Withdrawn"    : "withdrawn_formatted.babel.FAF_soft.X50.cpb",
    "FDAApproved"  : "approved_formatted.babel.FAF_soft.X50.cpb",
    "Analgesic"    : "Anagesic",
    "Antibacterial": "Antibacterial",
    "Anticancer"   : "Anticancer",
    "CNS"          : "CNS",
    "GPCR"         : "GPCR",
    "Ion-Channels" : "Ion-Channels",
    "Kinase"       : "Kinase",
    "Chamberlain"  : "Chamberlain",
    # MJ Formation
    "formation"    : "10-CDK2-Actives-dvs.2880.filtered.3D.X50"
}
