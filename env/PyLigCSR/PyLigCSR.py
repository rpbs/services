#!/usr/bin/env python

import sys, os
import time

# Configuration 
from Config import *

# Dependencies
from PyOptions import *
from FileTools import *
import Mol2
import cluster

import signal


class CSRre(Options):


    def __init__(self, args = ["CSRre"], options = None, version = None, verbose = False):
        """
        @param options : a dictionary of the options among which 
                         some are valid.
        """
        if version == None:
            version = "CSRre"+ " v%s"%VERSION
        if not hasattr(self,"clp"):
            Options.__init__(self, args = args, options = options, version=version, verbose = verbose)
        self.__cmdLine_options()


    def start(self, parse = True, verbose = False):
        """
        Effective options parsing
        on return, CSRre ready to run.
        """
        # This for demo mode
        if parse:
            self.parse()
        if verbose:
            self.options.verbose = verbose
        self.cmd_line_status = self.__check_options(verbose = True)
        if verbose:
            sys.stderr.write("CSRre Initial start: %s\n" % self.cmd_line_status)
        return


    def setremotehandler(self, verbose = False):
        """
        This to ensure qdel on Mobyle user removal.
        """
  # f = open("ilog","a")
  # f.write("Installing signal handler\n")
  # f.close()
        signal.signal(signal.SIGTERM, sgehandler) 


    def __repr__(self):
        """
        @return: a string describing the options setup for the Blast instance
        """

        try:
            rs = self.version
            for option in self.options.__dict__.keys():
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""


    def __cmdLine_options(self, version = None):
        """
        Command line definition.
        These options are extra options over inherited PPP options.
        @return: command line options and arguments (optionParser object)
        """

        self.clp.set_usage("usage: %prog [options] <sequence file> ")

    # OK from here
        self.clp.add_option("--script", action="store_true",                 dest="script",   
                       help="do not run, but generate a script to run",              default = False)

        self.clp.add_option("--posttreatonly", action="store_true",                dest="posttreatonly",   
                       help="Run posttreatment only.",              default = False)

        self.clp.add_option("--ibmol2", action="store",             dest="bank",      type="string",
                       help="bank to screen (mol2)",              default = None)

        self.clp.add_option("--bankPath", action="store",             dest="bankPath",      type="string",
                       help="path to the installed banks to screen (mol2)",              default = DFLT_CSR_BANK_PATH)

        self.clp.add_option("--igbmol2", action="store",             dest="gbank",      type="string",
                       help="gbank to screen (mol2)",              default = None)

        self.clp.add_option("--itmol2", action="store",             dest="target",      type="string",
                       help="target (mol2)",              default = None)

        self.clp.add_option("-o", "--olog", action="store",             dest="logFile",      type="string",
                       help="logFile",              default = None)

        self.clp.add_option("-l", "--label", action="store",             dest="label",      type="string",
                       help="a label for your run and the files generated",              default = DFLT_CSR_LABEL)

        self.clp.add_option("--rules", action="store",             dest="ruleFile",      type="string",
                       help="the file containing the re rules",              default = DFLT_CSR_RULE_FILE)

        self.clp.add_option("--noRules", action="store_false",                dest="useRules",   
                       help="do not use rules to prune the search (3D only)",              default = True)

        self.clp.add_option("--noH", action="store_false",                dest="withH",   
                       help="do not use hydrogens in the search. Consider only heavy atoms.",              default = True)

        self.clp.add_option("-n", "--nIter", action="store",                 dest="niter",   type="int",
                       help="number of CSR iterations",                   default = DFLT_CSR_NITER)                      

        self.clp.add_option("--nBest", action="store",                 dest="nBest",   type="int",
                       help="number of compounds returned",                default = DFLT_CSR_NBEST)                      

        self.clp.add_option("--minAt", action="store",                 dest="minat",   type="int",
                       help="number of atom pairs for a match to be accepted",   default = DFLT_CSR_MINAT)  

        self.clp.add_option("--dmax", action="store", dest="dmax",   type="float",
                            help="maximal distance between atoms.", default = DFLT_CSR_DMAX)
        
        self.clp.add_option("--rmsd", action="store", dest="rmsd",   type="float",
                            help="maximal RMSd.", default = DFLT_CSR_RMSD)
        
        self.clp.add_option("--tol", action="store", dest="tol",   type="float",
                            help="maximal tolerance.", default = DFLT_CSR_TOL)

        
    def __check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy

        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """

        # Check input data exists
        if (not self.options.target):
            if verbose:
                sys.stderr.write("CSRre: must specify a target to search similar compounds. --help for option detail.\n")
            return False

        if (not self.options.bank) and (not self.options.gbank):
            if verbose:
                sys.stderr.write("CSRre: must specify a bank to search within. --help for option detail.\n")
            return False

        # Check target is valid
        try:
            print "%s" % self.options.target
            x = Mol2.mol2_indexed_set(self.options.target)
            IdList = x.compoundOrderedIds
            if len(IdList) < 1:
                sys.stderr.write("Sorry: No compound detected in the query. This might be due to an incorrect format (mol2 expected).\n");
                return False
            if len(IdList) > 1:
                sys.stderr.write("Sorry: More than one compound detected in the query. Only one accepted.\n");
                return False
        except:
            sys.stderr.write("Sorry: Some error occurred while parsing your query. This might be due to an incorrect format (mol2 expected)\n");
            return False

        # We test the bank size. Possibly we truncate (NOT DONE YET).
        if self.options.bank:
            try:
                # print bank
                x = Mol2.mol2_indexed_set(self.options.bank)
                IdList =  x.compoundOrderedIds
                if len(IdList) > 10000:
                    sys.stderr.write("Sorry: Your bank is too large (%d conformers). A maximum of 10 000 conformers is presently accepted.\n" % (len(IdList)));
                    return False
                if len(IdList) < 1:
                    sys.stderr.write("Sorry: No compound detected in the uploaded bank. This might be due to an incorrect format (mol2 expected)\n");
                    return False
                uniqueIds = []
                for id in IdList:
                    it = "_".join(id.split("_")[:-1])
                    it = id.split("_")[0]
                    if it not in uniqueIds:
                        uniqueIds.append(it)
                # print len(uniqueIds)
                if len(uniqueIds) > 500:
                    sys.stderr.write("Sorry: Your bank is too large (%d compounds). A maximum of 500 compounds is presently accepted.\n" % (len(uniqueIds)));
                    return False
            except:
                sys.stderr.write("Sorry: Some error occurred while parsing your bank. This might be due to an incorrect format (mol2 expected)\n");
                return False
                
        return True


    def generate_CSRre_cmd_line(self, verbose = False):
        """
        Generate the call to LigCSR binary
        Propagate proper command line options
        This has been adapted to sge array
        """

        self.cmd_ligcsrre = "LigCSRre_service"

        if self.options.bank:
            self.mode = 1
        elif self.options.gbank:
            self.mode = 2
            self.options.label = arrayBankPrefix[self.options.gbank]

        self.args_ligcsrre = [ "%s" % self.options.target,
                               "%d" % self.options.niter,
                               "%f" % self.options.dmax,
                               "%d" % self.options.minat,
                               "%f" % self.options.rmsd,
                               "%f" % self.options.tol,
                               "%s" % self.options.label,
                               "%s" % self.mode,
                               "%s" % self.options.bank,
                               "%s" % self.options.bankPath ]

        self.cmd_ligcsrresort = "LigCSRreSort_service"
        
        self.args_ligcsrresort = [ "%s" % self.options.label,
                              "%s" % self.mode ]

        if self.options.useRules:
            self.args_ligcsrre.append("-irules %s " % (self.options.ruleFile))
            
        if self.options.withH == False:
            self.args_ligcsrre.append("-noH ")

        self.scoreFileName           = "%s.scores" % self.options.label

        if self.options.bank:
            self.ntasks              = 1 
            self.logFiles            = ["%s.log" % self.options.label]
            self.unsortscoreFileName = "%s.unsortscores" % self.options.label
            self.scoreFiles          = ["%s.scores" % self.options.label]
            self.bestPDBFileName     = "%s-bestCompounds.pdb" % self.options.label
            self.bestMol2FileName    = "%s-bestCompounds.mol2" % self.options.label
            self.tenBestPDBFileName  = "%s-tenBestCompounds.pdb" % self.options.label
            self.tenBestMol2FileName = "%s-tenBestCompounds.mol2" % self.options.label
            self.bankList            = [self.options.label]

        elif self.options.gbank:           
            # These settings depend on Slurm array
            self.ntasks              = len(splitBanks[self.options.gbank])
            self.pbsIds              = []
            self.logFiles            = []
            self.scoreFiles          = []
            self.bankList            = splitBanks[self.options.gbank]
            # Individual log / scores per bank chunk.
            for i in splitBanks[self.options.gbank]:
                self.logFiles.append("%s.log" % i)
                self.scoreFiles.append("%s.unsortscores" % i)
            # These names come after the reduce step
            self.scoreFileName       = "%s.scores" % self.options.gbank
            self.bestPDBFileName     = "%s-bestCompounds.pdb" % self.options.gbank
            self.bestMol2FileName    = "%s-bestCompounds.mol2" % self.options.gbank
            self.tenBestPDBFileName  = "%s-tenBestCompounds.pdb" % self.options.gbank
            self.tenBestMol2FileName = "%s-tenBestCompounds.mol2" % self.options.gbank
            self.unsortscoreFileName = "%s.unsortscores" % self.options.gbank
        
        # depending on the command line, several options have to been setup.
        

    def run(self, verbose = False):
        self.generate_CSRre_cmd_line(verbose = verbose)
        if not self.options.posttreatonly:
            cluster.runTasks(self.cmd_ligcsrre, self.args_ligcsrre, tasks = self.ntasks, docker_img = DOCKER_IMG)
            cluster.runTasks(self.cmd_ligcsrresort, self.args_ligcsrresort, tasks = self.ntasks, docker_img = DOCKER_IMG)
            # Needed to reste some variable values
        self.posttreat(verbose = verbose)
            
    def posttreat(self, verbose = False):
        """
        posttreat
        This method will format results, assemble best results, etc

        @param verbose: switch verbose 
        @return: None
        """
        
        # This is a one CPU task.
        # now we must reduce the results into one file
        for i in self.scoreFiles:
            # sys.stderr.write("%s\n" % self.scoreFiles)
            cmd = "cat %s >> %s" % (i, self.unsortscoreFileName)
            # sys.stderr.write("%s\n" % cmd)
            os.system(cmd)
        # now we sort the results
        #~ cmd = "resort2.py"
        #~ 
        #~ args = [ "-c %s" % self.unsortscoreFileName,
                 #~ "> %s" % self.scoreFileName ]
#~ 
        #~ cluster.runTasks(cmd, args, docker_img = DOCKER_IMG)
 
        os.system("drun ligcsr resort2.py -c %s > %s" % ( self.unsortscoreFileName, self.scoreFileName))
 
        # Internal representation of PDB generated
        cmpnds = []
        for i in self.bankList:
            try:
                cmpnds.append( quickMultiPDB("%s.pdb" % i) )
            except:
                sys.stderr.write("PostTreatment: Sorry. Some error occurred while loading results.\n")
                return

        # Load the scores
        f = open(self.scoreFileName)
        lines = f.readlines()
        f.close()

        # auto switch for all results
        if self.options.nBest > len(lines):
            self.options.nBest = len(lines)

        f = open(self.bestPDBFileName, "w")

        ocmpnds = []
        lnBest = self.options.nBest
        if self.options.nBest > len(lines):
            lnBest = len(lines)
        rmlist = []
        for i in range(0,lnBest):
            aCmpnd = lines[i].split()[0]
            ocmpnds.append(aCmpnd)
            for oneSeries in cmpnds:
                if oneSeries.hasCmpnd(aCmpnd):
                    info = oneSeries.getCmpnd(aCmpnd)
                    f.write("%s" % info)
                    f1 = open("%s.pdb" % aCmpnd,"w")
                    f1.write("%s" % info)
                    f1.close()
                    rmlist.append("%s.pdb %s.mol2" % (aCmpnd, aCmpnd))
                    cmd = "babel"
                    args = [ "%s.pdb" % aCmpnd,
                             "%s.mol2" % aCmpnd ]
                    cluster.runTasks(cmd, args, docker_img = BABEL_DOCKER_IMG)
                    os.system("sed 's/.pdb//' %s.mol2 >> %s" % (aCmpnd, self.bestMol2FileName))
                    break
        f.close()
        os.system("rm -f %s" % " ".join(rmlist))

        # create output file for 10 best
        f = open(self.tenBestPDBFileName, "w")
        nBest = 10
        lnBest = nBest
        if nBest > len(lines):
            lnBest = len(lines)

        # output nBest compounds
        rmlist = []
        for i in range(0,lnBest):
            aCmpnd = lines[i].split()[0]
            # print aCmpnd, self.tenBestMol2FileName
            for oneSeries in  cmpnds:
                if oneSeries.hasCmpnd(aCmpnd):
                    # oneSeries.pileupCmpnd(aCmpnd, bestPDBFileName)
                    info = oneSeries.getCmpnd(aCmpnd)
                    f.write("%s" % info)
                    f1 = open("%s.pdb" % aCmpnd,"w")
                    f1.write("%s" % info)
                    f1.close()
                    rmlist.append("%s.pdb %s.mol2" % (aCmpnd, aCmpnd))
                    cmd = "babel"
                    args = [ "%s.pdb" % aCmpnd,
                             "%s.mol2" % aCmpnd ]
                    cluster.runTasks(cmd, args, docker_img = BABEL_DOCKER_IMG)
                    os.system("sed 's/.pdb//' %s.mol2 >> %s" % (aCmpnd, self.tenBestMol2FileName))
                    break
        f.close()
        os.system("rm -f %s" % " ".join(rmlist))


class quickMultiPDB:
    def __init__(self, fname):
        """
        import sys
        sys.path.append("/data/bin")
        from LigCSR import *
        d = quickMultiPDB2("diverse-testset-X50-part9.mol2.pdb")
        d.outCmpnd("9047176_1_24", "stdout")

        Note: we do not load file into memory: too huge !
        """

        self.fname = fname
        cmd = "grep -b HEADER %s" % fname
        positions = os.popen(cmd).readlines()
        
        rs = {}
#        count = 0
        for p in positions:
            # print p
            it = p.split(":")
            start = int(it[0])
            if len(rs):
                rs[Id]["to"] = start-1
                rs[Id]["len"] = start - rs[Id]["from"]
                # print Id, rs[Id]
            Id = it[1].split()[1]
            rs[Id] = {"from": start}
#             count += 1
#             if count == 10:
#                 break
        rs[Id]["to"] = -1 # EOF
        rs[Id]["len"] = -1 # EOF
        # print Id, rs[Id]
        self.data = rs

    def getCmpnd_lines(self, Id):
        """
        From line indices
        """
        if self.data[Id]["len"] == -1:
            cmd = "tail -n +%d %s 2> /dev/null" % (self.data[Id]["from"], self.fname)
        else:
            cmd = "tail -n +%d %s 2> /dev/null | head -n %d" % (self.data[Id]["from"], self.fname, self.data[Id]["len"])
        # print cmd
        lines = os.popen(cmd).readlines()
        return lines

    def getCmpnd(self, Id):
        """
        From byte-offset indices
        """
        f = open(self.fname)
        f.seek(self.data[Id]["from"])
        info = f.read(self.data[Id]["len"])
        f.close()
        return info

    def pileupCmpnd(self, Id, ofname):
        if self.data[Id]["len"] == -1:
            cmd = "tail -n +%d %s 2> /dev/null >> %s" % (self.data[Id]["from"], self.fname, ofname)
        else:
            cmd = "tail -n +%d %s 2> /dev/null | head -n %d >> %s" % (self.data[Id]["from"], self.fname, self.data[Id]["len"], ofname)
        # print cmd
        os.system(cmd)

    def writeCmpnd(self, Id, fname, mode = "a"):
        """
        output one compound in a file, manage file opening and closing
        """
        if not self.data.has_key(Id):
            return
        if fname == "stdout":
            f = sys.stdout
        else:
            f = open(fname, mode)
        self.outCmpnd(Id, f)
        if f != sys.stdout:
            f.close()

    def outCmpnd(self, Id, f):
        """
        output in file, do not manage file opening
        do not check if compound is there.
        """
        for l in self.getCmpnd(Id):
            f.write("%s" % l)

    def hasCmpnd(self, Id):
        if Id in self.data.keys():
            return True
        return False

def shellExec( cmd, stdinl = [], ignerr = False, verbose = 0 ):
    """
    Execute a shell command, with pipelines to stdin, stdout, and stderr.

    @param cmd    : the command line
    @param stdinl : string list to pipe as input
    @param ignerr : ignore stderr process content
    @param verbose: verbose mode

    @return: a tuple as (stdout lines list,stderr lines list)
    """
    import subprocess

    if verbose:
        print >> sys.stderr, "ShellExec command: %s" % cmd

    if len( stdinl ):
        p = subprocess.Popen(cmd,
                             shell=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)
        # Prepare stdin
        iStr = ""
        for aIn in stdinl:
            iStr += "%s\n" % aIn
        # Write to stdin
        p.stdin.write( "%s" % iStr )
        # Close pipe
        p.stdin.close()
    else:
        p = subprocess.Popen(cmd,
                             shell=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)

    # Read stdout/err
    stdoutl = []
    stderrl = []
    while True:
        aStdout = p.stdout.readline()
        aStderr = p.stderr.readline()
        if verbose >1:
            print >> sys.stderr, aStdout,
        if verbose >1:
            print >> sys.stderr, aStderr,
        if aStdout:
            stdoutl.append( aStdout )
        if aStderr:
            stderrl.append( aStderr )
        if not aStdout and not aStderr:
            break

    if len(stderrl) and not ignerr:
        writeList( sys.stderr, stderrl )

    if verbose:
        print >> sys.stderr, ""

    return (stdoutl,stderrl)


def main( args ):
    """
    Launch the main purpose:
    - Parse command line
    - Launch predictions
    """
    
    # Parse arguments
    job = CSRre(args = args)
    job.start(verbose = True)

    # Manage demo before checking options validity

    # Check options are OK.
    if not job.cmd_line_status:
        sys.stderr.write("CSRre: Sorry, incorrect command line.\n")
        return

    # job.parse_specific()
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)

    job.run(job.options.verbose)
    return 

# GO !
if __name__ == "__main__":
    main(sys.argv)
# -------------------------
                                
