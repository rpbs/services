#!/usr/bin/env python

import argparse
import re
import os
import sys
import shutil
import glob
import json
import string
import csv
from collections import defaultdict

import cluster.cluster as cluster
import PyPDB.PyPDB as PDB

class myPDB(PDB.PDB):

    def compound(self):
        """
        PDB.compound
        @return: the nature of the file (string)
        """
        title=''
        for Line in self.info:
          if Line[:6]=='COMPND':
            items = string.split(Line[6:])
            for aItem in items:
              if title != '':
                title = title + " "
              title = title + aItem
        return title

#Constants
DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"
DFLT_PDB_BANK = "/shared/banks/pdb/data/structures/all/pdb/"
DFLT_PDBQT_BANK = "/shared/banks/pdbqt"
DFLT_SURF_BANK = "/shared/banks/pdb_surfaces"
ENVIRONMENT_MODULE = "patchsearch/1.0-rpbs"

jobId = os.path.basename(os.getcwd())
serviceName = os.path.split(os.path.split(os.getcwd())[0])[1]   # can be patchsearch or patchidentification
wdir = os.path.join(DFLT_MOBYLE_JOBS_PATH, serviceName, jobId)

# Initialize templates
from jinja2 import Environment, PackageLoader
jinja_env = Environment(loader = PackageLoader('PatchSearch','templates'))


def cmdLine():

    parser = argparse.ArgumentParser(description=__doc__)
    
    parser.add_argument("-p", "--pdb", dest="pdb_file",
                        action="store", default=None,
                        help="PDB file from which extract patch around ligand.")
    
    parser.add_argument("-l", "--ligand", dest="ligand_name",
                        action="store", default=None,
                        help="Ligand to define patch around.")
    
    parser.add_argument("-r", "--reslist", dest="reslist_file",
                        action="store", default=None,
                        help="List of residues to define patch.")
    
    parser.add_argument("-a", "--accessibility", dest="accessibility",
                        action="store", default=0.01,
                        help="Accessibility.") 
 
    parser.add_argument("-c", "--calpha", dest="calpha",
                        action="store_true", default=False,
                        help="Keep the alpha carbons of residue involved in the patch (not necessary).")  

    parser.add_argument("-e", "--extract", dest="extract_patches",
                        action="store_true", default=False,
                        help="Just extract the list of patches from ligands.")

    parser.add_argument("-s", "--surflist", dest="surflist_file",
                        action="store", default=None,
                        help="List of surfaces to browse (pdb ids).")
                        
    parser.add_argument("-f", "--surffile", dest="surface_file",
                        action="store", default=None,
                        help="Surface file to browse (pdb file).")

    parser.add_argument("-m", "--max_hits", dest="max_hits",
                        action="store", default=5, type=int,
                        help="Number of returned hits on a target.")
                        
    parser.add_argument("-b", "--nr_best_hits", dest="nr_best_hits",
                        action="store", default=50, type=int,
                        help="Number of best hits.")
                        
    parser.add_argument("-g", "--scoring_method", dest="scoring_method",
                        action="store", default="vinardo", choices=["dkoes_scoring", "vinardo"],
                        help="Scoring method.")                    

    return parser


def checkArgValues(args):

    # Input complex file is PDB

    if not args.pdb_file:
        msg = "No pdb from which to extract patch file specified."
        return False, msg

    status, msg = PDB.isPDB(args.pdb_file)
    if not status:
        return False, msg

    pdb_file = os.path.splitext(os.path.basename(args.pdb_file))[0] + ".pdb"

    if args.pdb_file != pdb_file:
        try:
            shutil.copy2(args.pdb_file, pdb_file)
        except IOError, e:
            msg = "Could not copy input pdb file to current directory."
            return False, msg
        args.pdb_file = pdb_file

    if not args.extract_patches:
      
        if not args.ligand_name and not args.reslist_file:
            msg = "No residues list nor ligand to define patch specified."
            return False, msg

        if not args.surflist_file and not args.surface_file:
            msg = "No pdb ids list nor PDB file to search against provided."
            return False, msg
            
        if args.surflist_file:
            if os.path.split(args.surflist_file)[0]:
                try:
                    shutil.copy2(args.surflist_file, "./" + os.path.basename(args.surflist_file))
                    args.surflist_file = os.path.basename(args.surflist_file)
                except IOError, e:
                    msg = "Could not copy pdb ids list to current directory."
                    return False, msg
                
        if args.surface_file:
            status, msg = PDB.isPDB(args.surface_file)
            if not status:
                return False, msg
            surface_file = os.path.splitext(os.path.basename(args.surface_file))[0] + ".pdb"
            if args.surface_file != surface_file:
                try:
                    shutil.copy2(args.surface_file, surface_file)
                except IOError, e:
                    msg = "Could not copy surface pdb file to current directory."
                    return False, msg
                args.surface_file = surface_file

    return True, ""


def readAligFile(alig_file, ligand_num):

    alig_num = 0

    scores = {}

    with open(alig_file, "r") as alig_file_handle:
        alig_file_content = alig_file_handle.readlines()
        for i, line in enumerate(alig_file_content):
            if line.startswith(">"):
                alig_num += 1
                if alig_num == ligand_num:
                    score_line = line.split()[3:]
                    scores["coverage"] = score_line[0]
                    scores["rmsd"] = "%0.3f" % float(score_line[1])
                    scores["bcscore"] = "%0.3f" % float(score_line[2])
                    query_atom_list = [ "%s:%s.%s" % tuple([atom.split(":")[j] for j in (0,3,2)]) for atom in alig_file_content[i+1].split() ]
                    hit_atom_list = [ "%s:%s.%s" % tuple([atom.split(":")[j] for j in (0,3,2)]) for atom in alig_file_content[i+2].split() ]
                    hit_residue_list = [ atom.split(".")[0] for atom in hit_atom_list ]
                    hit_chain_list = list(set([ residue.split(":")[1] for residue in hit_residue_list ]))
                    scores["query_atom_list"] = query_atom_list
                    scores["hit_atom_list"] = hit_atom_list
                    scores["hit_residue_list"] = hit_residue_list
                    scores["hit_chain_list"] = hit_chain_list

    return scores
    

def readPDBInfo(pdb_file, chains):

    pdb = myPDB(pdb_file)

    compound_lines = pdb.compound()
    source_lines = pdb.source()

    match_molecule = re.findall("MOL_ID:\s+(\d+);\s+\d+\s+MOLECULE:\s+([^;]*);\s+\d+\s+CHAIN:\s+([A-Z, ]+)", compound_lines)
    match_organism = re.findall("MOL_ID:\s+(\d+);\s+\d+\s+ORGANISM_SCIENTIFIC:\s([^;]*);", source_lines)

    mol_ids = defaultdict(dict)

    if len(match_molecule):
        for m in match_molecule:
            mol_ids[m[0]].update({
                                  "molecule" : m[1], 
                                  "chain" : [x.strip() for x in m[2].split(",")]
                                 })
    if len(match_organism):
        for m in match_organism:
            mol_ids[m[0]].update({
                                  "organism" : m[1]
                                 })

    molecules = []
    organisms = []
    
    if bool(mol_ids):
        for mol_id in mol_ids.keys():
            if "chain" in mol_ids[mol_id].keys() and len(list(set(mol_ids[mol_id]["chain"]).intersection(set(chains)))):
                if "molecule" in mol_ids[mol_id].keys():
                    molecules.append(mol_ids[mol_id]["molecule"])
                if "organism" in mol_ids[mol_id].keys():
                    organisms.append(mol_ids[mol_id]["organism"])

    if not len(molecules):
        molecules.append("N/A")
    if not len(organisms):
        organisms.append("N/A")

    return molecules, organisms


def formatScoresTable(data, output_file, wdir="./"):

    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('patchsearch.table.html')
        sink.write(template.render(data = data))


def formatViewer(data, output_file, wdir = "./"):
        
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('patchsearch.vis.html')
        sink.write(template.render(data = json.dumps(data), wdir = wdir))


def formatPatchesViewer(model_file, patches, output_file, wdir = "./"):
  
    with open(output_file, 'wt') as sink:
        template = jinja_env.get_template('patchidentification.vis.html')
        sink.write(template.render(model_file = model_file, patches = patches, wdir = wdir))    


def main():

    parser = cmdLine()
    p_args = parser.parse_args()
    status, msg = checkArgValues(p_args)

    if not status:
        cluster.progress(msg)
        sys.exit(0)

    # we extract the patch(es) around the ligand(s)

    patch_file = os.path.splitext(p_args.pdb_file)[0] + "-patch.pdb"

    cmd = "PatchExtract.R"
    s_args = [ "%s" % p_args.pdb_file,
               "--outfile %s" % patch_file,
               "--centroid"
             ]

    if p_args.ligand_name:
        s_args.append("--ligand %s" % p_args.ligand_name)
        cluster.progress("Extracting patch.")

    if p_args.reslist_file:
        s_args.append("--list %s" % p_args.reslist_file)
        cluster.progress("Extracting patch from list of residues.")
              
    if p_args.calpha:
        s_args.append("--calpha")

    cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "patchextract")


    # we search for homologous patches in a pdb or a list of pdbs

    if p_args.surflist_file:

        pdbid_list = []
        with open(p_args.surflist_file, "r") as surflist_handle:
            for line in surflist_handle:
                currentline = line.rstrip().lower().replace(" ", "").split(",")
                pdbid_list += currentline
        
        pdbid_list = list(set(pdbid_list))                              # we remove duplicates

        for pdbid in pdbid_list:
            if not re.match("\d[a-z0-9]{3}", pdbid):
                msg = "Invalid pdb id : %s" % pdbid
                cluster.progress(msg)
                sys.exit(0)
            if not os.path.isfile(os.path.join(DFLT_PDB_BANK, "pdb%s.ent.gz" % pdbid)) or not os.path.isfile(os.path.join(DFLT_PDBQT_BANK, pdbid[1:3], "%s.pdbqt" % pdbid)):
                msg = "the pdb structure could not be processed, because of problems with the coordinates of the protein: %s" % pdbid
                cluster.progress(msg)
             # we must add something here to check that the pdb surface exists and the pdb itself   
        
        if not os.path.isfile(patch_file):
            msg = "Problem with extracting patch from input pdb. %s does not exist." % patch_file
            cluster.progress(msg)
            sys.exit(0)

        cmd = "PatchSearch_service"
        s_args = [ patch_file,
                   "map_item",
                   "%s" % p_args.max_hits,
                   "%s" % DFLT_SURF_BANK ]

        cluster.progress("Patch searching.")
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, map_list = pdbid_list, log_prefix = "patchsearch")

    elif p_args.surface_file:
        
        cmd = "PatchSearchScript.R"
        s_args = [ "%s" % patch_file,
                   "--surface %s" % p_args.surface_file,
                   "--outfile %s.score.txt" % os.path.splitext(p_args.surface_file)[0],
                   "--alfile %s.alig.txt" % os.path.splitext(p_args.surface_file)[0],
                   "--maxhits %d" % p_args.max_hits
                 ]
                  
        cluster.progress("Patch searching.")
        cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, log_prefix = "patchsearch")


    # We align query ligand with hits

    alig_files = glob.glob("*.alig.txt")
    pdbid_list = []

    with open("alignments.txt", "w") as alig_outfile:
        for alig_file in alig_files:
            size = 0
            with open(alig_file) as alig_filehandle:
                for line in alig_filehandle:
                    size += 1
                    alig_outfile.write(line)
            if size > 1:
                pdbid_list.append(alig_file.split(".alig.txt")[0])
       
    cmd = "PatchAlign_service"
    
    s_args = [ patch_file,
               "%s" % p_args.max_hits,
               "map_item",
               "%s" % p_args.scoring_method,
               "%s" % DFLT_PDB_BANK,
               "%s" % DFLT_PDBQT_BANK,
             ]

    cluster.progress("Patch scoring.")
    cluster.runTasks(cmd, s_args, environment_module = ENVIRONMENT_MODULE, map_list = pdbid_list, log_prefix = "patchalign")

    # We read the energy of the rescoring

    ligand_files = glob.glob("*-ligand-*.pdb")
    
    scores_tab = {}

    for ligand_file in ligand_files:
        m = re.match("(\S+)-ligand-(\d+)\.pdb", ligand_file)
        pdbid, ligandnum = m.groups()
        ligandnum = int(ligandnum)
        alig_file = "%s.alig.txt" % pdbid
        
        with open(ligand_file) as ligand_filehandle:
            affinity = ligand_filehandle.readline().split()[2]
            scores = readAligFile(alig_file, ligandnum)
            scores["ligand_file"] = ligand_file
            scores["affinity"] = "%0.3f" % float(affinity)
            
        scores_tab[pdbid] = scores
        
    # We put all the scores in a csv table
    
    with open("scores.csv", "w") as csv_file:
        writer = csv.writer(csv_file, delimiter = "\t")
        writer.writerow(["PDB Id", "Chains", "Hit Patch Coverage (# atoms)", "RMSD", "PatchSearch Score", "Ligand Affinity (kcal/mol)"])
        for pdbid, scores in scores_tab.items():
            writer.writerow([pdbid, ",".join(scores["hit_chain_list"]), scores["coverage"], scores["rmsd"], scores["bcscore"], scores["affinity"]])
      
    # We read the info in the PDB files and put them in the best scores table

    cluster.progress("Results analysis.")
    
    best_hits = dict(sorted(scores_tab.items(), key=lambda x: x[1]["affinity"])[:1000])

    for pdbid, scores in best_hits.items():

        pdbfile = "%s.pdb.gz" % pdbid
        molecules, organisms = readPDBInfo(pdbfile, scores["hit_chain_list"])

        scores["molecules"] = " + ".join(molecules)
        scores["organisms"] = " + ".join(organisms)

    # Format html table and ngl viewer

    cluster.progress("Formatting results.")

    formatScoresTable(best_hits, "patchsearch.table.html", wdir)    
    formatViewer(best_hits, "patchsearch.vis.html", wdir)

if __name__ == "__main__":
    
    main()
    sys.exit(0)
